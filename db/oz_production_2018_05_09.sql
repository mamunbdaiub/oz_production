-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 09, 2018 at 02:02 PM
-- Server version: 5.6.17
-- PHP Version: 5.6.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `oz_production`
--

-- --------------------------------------------------------

--
-- Table structure for table `ec_address`
--

CREATE TABLE IF NOT EXISTS `ec_address` (
  `address_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `first_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `last_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `address_line_1` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `address_line_2` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT '',
  `city` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `state` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `zip` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `country` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `phone` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `company_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`address_id`),
  UNIQUE KEY `address_id` (`address_id`),
  KEY `ec_address_idx1` (`address_id`),
  KEY `ec_address_idx2` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=63 ;

--
-- Dumping data for table `ec_address`
--

INSERT INTO `ec_address` (`address_id`, `user_id`, `first_name`, `last_name`, `address_line_1`, `address_line_2`, `city`, `state`, `zip`, `country`, `phone`, `company_name`) VALUES
(1, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'KS', '10991', 'US', '43535353', ''),
(2, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'KS', '10991', 'US', '43535353', ''),
(3, 0, 'Test', 'Abdul', 'NY, Newyork, USA', '', 'Kansas', 'KS', '10991', 'US', '43535353', ''),
(4, 0, 'Test', 'Abdul', 'NY, Newyork, USA', '', 'Kansas', 'KS', '10991', 'US', '43535353', ''),
(5, 0, 'Mamun', 'Abdullah', 'NY, Newyork, USA', '', 'Kansas', 'IN', '10991', 'US', '43535353', ''),
(6, 0, 'Mamun', 'Abdullah', 'NY, Newyork, USA', '', 'Kansas', 'IN', '10991', 'US', '43535353', ''),
(7, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'IA', '10991', 'US', '43535353', ''),
(8, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'IA', '10991', 'US', '43535353', ''),
(9, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'IL', '10991', 'US', '43535353', ''),
(10, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'IL', '10991', 'US', '43535353', ''),
(11, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'KS', '10991', 'US', '43535353', ''),
(12, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'KS', '10991', 'US', '43535353', ''),
(13, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'AL', '10991', 'US', '43535353', ''),
(14, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'AL', '10991', 'US', '43535353', ''),
(15, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'KS', '10991', 'US', '43535353', 'Instalogic'),
(16, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'KS', '10991', 'US', '43535353', 'Instalogic'),
(17, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'KS', '10991', 'US', '43535353', 'Instalogic'),
(18, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'KS', '10991', 'US', '43535353', 'Instalogic'),
(19, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'KS', '10991', 'US', '43535353', 'Instalogic'),
(20, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'KS', '10991', 'US', '43535353', 'Instalogic'),
(21, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'KS', '10991', 'US', '43535353', 'Instalogic'),
(22, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'KS', '10991', 'US', '43535353', 'Instalogic'),
(23, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'KS', '10991', 'US', '43535353', 'Instalogic'),
(24, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'KS', '10991', 'US', '43535353', 'Instalogic'),
(25, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'KS', '10991', 'US', '43535353', 'Instalogic'),
(26, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'KS', '10991', 'US', '43535353', 'Instalogic'),
(27, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'KS', '10991', 'US', '43535353', 'Instalogic'),
(28, 0, '05/02/2018 3:26 PM', '05/23/2018 3:26 PM', 'NY, Newyork, USA', '', 'Kansas', 'KS', '10991', 'US', '43535353', 'Instalogic'),
(29, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'AK', '10991', 'US', '43535353', ''),
(30, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'AK', '10991', 'US', '43535353', ''),
(31, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'KS', '10991', 'US', '43535353', ''),
(32, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'KS', '10991', 'US', '43535353', ''),
(33, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'AL', '10991', 'US', '43535353', ''),
(34, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'AL', '10991', 'US', '43535353', ''),
(35, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'AL', '10991', 'US', '43535353', 'Instalogic'),
(36, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'AL', '10991', 'US', '43535353', 'Instalogic'),
(37, 0, 'Jodu modu', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'AL', '10991', 'US', '43535353', 'Instalogic'),
(38, 0, 'Jodu modu', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'AL', '10991', 'US', '43535353', 'Instalogic'),
(39, 0, 'Jodu modu', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'AK', '10991', 'US', '43535353', ''),
(40, 0, 'Jodu modu', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'AK', '10991', 'US', '43535353', ''),
(41, 0, '05/02/2018 3:41 PM', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'AK', '10991', 'US', '43535353', ''),
(42, 0, '05/02/2018 3:41 PM', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'AK', '10991', 'US', '43535353', ''),
(43, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'AK', '10991', 'US', '43535353', ''),
(44, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'AK', '10991', 'US', '43535353', ''),
(45, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'AL', '10991', 'US', '43535353', ''),
(46, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'AL', '10991', 'US', '43535353', ''),
(47, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'AZ', '10991', 'US', '43535353', ''),
(48, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'AZ', '10991', 'US', '43535353', ''),
(49, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'AZ', '10991', 'US', '43535353', ''),
(50, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'AZ', '10991', 'US', '43535353', ''),
(51, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'TN', '10991', 'US', '43535353', ''),
(52, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'TN', '10991', 'US', '43535353', ''),
(53, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'KS', '10991', 'US', '43535353', ''),
(54, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'KS', '10991', 'US', '43535353', ''),
(55, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'AR', '10991', 'US', '43535353', ''),
(56, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'AR', '10991', 'US', '43535353', ''),
(57, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'AK', '10991', 'US', '43535353', ''),
(58, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'AK', '10991', 'US', '43535353', ''),
(59, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'DE', '10991', 'US', '43535353', ''),
(60, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'DE', '10991', 'US', '43535353', ''),
(61, 1, 'Rajibul', 'Islam', 'NY, Newyork, USA', '', 'Kansas', 'KS', '10991', 'US', '43535353', 'Metabu Limited'),
(62, 1, 'Rajibul', 'Islam', 'NY, Newyork, USA', '', 'Kansas', 'KS', '10991', 'US', '43535353', 'Metabu Limited');

-- --------------------------------------------------------

--
-- Table structure for table `ec_affiliate_rule`
--

CREATE TABLE IF NOT EXISTS `ec_affiliate_rule` (
  `affiliate_rule_id` int(11) NOT NULL AUTO_INCREMENT,
  `rule_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `rule_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `rule_amount` float(15,3) NOT NULL DEFAULT '0.000',
  `rule_limit` int(11) NOT NULL DEFAULT '0',
  `rule_active` tinyint(1) NOT NULL DEFAULT '1',
  `rule_recurring` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`affiliate_rule_id`),
  UNIQUE KEY `affiliate_rule_id` (`affiliate_rule_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_affiliate_rule_to_affiliate`
--

CREATE TABLE IF NOT EXISTS `ec_affiliate_rule_to_affiliate` (
  `rule_to_account_id` int(11) NOT NULL AUTO_INCREMENT,
  `affiliate_rule_id` int(11) NOT NULL DEFAULT '0',
  `affiliate_id` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`rule_to_account_id`),
  UNIQUE KEY `rule_to_account_id` (`rule_to_account_id`),
  KEY `affiliate_rule_id` (`affiliate_rule_id`),
  KEY `affiliate_id` (`affiliate_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_affiliate_rule_to_product`
--

CREATE TABLE IF NOT EXISTS `ec_affiliate_rule_to_product` (
  `rule_to_product_id` int(11) NOT NULL AUTO_INCREMENT,
  `affiliate_rule_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`rule_to_product_id`),
  UNIQUE KEY `rule_to_product_id` (`rule_to_product_id`),
  KEY `affiliate_rule_id` (`affiliate_rule_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_bundle`
--

CREATE TABLE IF NOT EXISTS `ec_bundle` (
  `bundle_id` int(11) NOT NULL AUTO_INCREMENT,
  `key_product_id` int(11) NOT NULL DEFAULT '0',
  `bundled_product_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`bundle_id`),
  UNIQUE KEY `bundle_id` (`bundle_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_category`
--

CREATE TABLE IF NOT EXISTS `ec_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_demo_item` tinyint(1) NOT NULL DEFAULT '0',
  `category_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_id` int(11) NOT NULL DEFAULT '0',
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `short_description` text COLLATE utf8mb4_unicode_520_ci,
  `image` text COLLATE utf8mb4_unicode_520_ci,
  `featured_category` tinyint(1) NOT NULL DEFAULT '0',
  `priority` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `category_id` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_categoryitem`
--

CREATE TABLE IF NOT EXISTS `ec_categoryitem` (
  `categoryitem_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`categoryitem_id`),
  UNIQUE KEY `categoryitem_id` (`categoryitem_id`),
  KEY `product_id` (`product_id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_code`
--

CREATE TABLE IF NOT EXISTS `ec_code` (
  `code_id` int(11) NOT NULL AUTO_INCREMENT,
  `code_val` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT '',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `orderdetail_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`code_id`),
  UNIQUE KEY `code_id` (`code_id`),
  KEY `product_id` (`product_id`),
  KEY `orderdetail_id` (`orderdetail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_country`
--

CREATE TABLE IF NOT EXISTS `ec_country` (
  `id_cnt` int(11) NOT NULL AUTO_INCREMENT,
  `name_cnt` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `iso2_cnt` varchar(10) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `iso3_cnt` varchar(10) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `sort_order` int(11) NOT NULL,
  `vat_rate_cnt` float(9,3) NOT NULL DEFAULT '0.000',
  `ship_to_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_cnt`),
  KEY `iso2_cnt` (`iso2_cnt`),
  KEY `iso3_cnt` (`iso3_cnt`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=241 ;

--
-- Dumping data for table `ec_country`
--

INSERT INTO `ec_country` (`id_cnt`, `name_cnt`, `iso2_cnt`, `iso3_cnt`, `sort_order`, `vat_rate_cnt`, `ship_to_active`) VALUES
(1, 'Afghanistan', 'AF', 'AFG', 10, 0.000, 1),
(2, 'Albania', 'AL', 'ALB', 11, 0.000, 1),
(3, 'Algeria', 'DZ', 'DZA', 12, 0.000, 1),
(4, 'American Samoa', 'AS', 'ASM', 13, 0.000, 1),
(5, 'Andorra', 'AD', 'AND', 14, 0.000, 1),
(6, 'Angola', 'AO', 'AGO', 15, 0.000, 1),
(7, 'Anguilla', 'AI', 'AIA', 16, 0.000, 1),
(8, 'Antarctica', 'AQ', 'ATA', 17, 0.000, 1),
(9, 'Antigua and Barbuda', 'AG', 'ATG', 18, 0.000, 1),
(10, 'Argentina', 'AR', 'ARG', 19, 0.000, 1),
(11, 'Armenia', 'AM', 'ARM', 20, 0.000, 1),
(12, 'Aruba', 'AW', 'ABW', 21, 0.000, 1),
(13, 'Australia', 'AU', 'AUS', 3, 0.000, 1),
(14, 'Austria', 'AT', 'AUT', 23, 0.000, 1),
(15, 'Azerbaijan', 'AZ', 'AZE', 24, 0.000, 1),
(16, 'Bahamas', 'BS', 'BHS', 25, 0.000, 1),
(17, 'Bahrain', 'BH', 'BHR', 26, 0.000, 1),
(18, 'Bangladesh', 'BD', 'BGD', 27, 0.000, 1),
(19, 'Barbados', 'BB', 'BRB', 28, 0.000, 1),
(20, 'Belarus', 'BY', 'BLR', 29, 0.000, 1),
(21, 'Belgium', 'BE', 'BEL', 30, 0.000, 1),
(22, 'Belize', 'BZ', 'BLZ', 31, 0.000, 1),
(23, 'Benin', 'BJ', 'BEN', 32, 0.000, 1),
(24, 'Bermuda', 'BM', 'BMU', 33, 0.000, 1),
(25, 'Bhutan', 'BT', 'BTN', 34, 0.000, 1),
(26, 'Bolivia', 'BO', 'BOL', 35, 0.000, 1),
(28, 'Botswana', 'BW', 'BWA', 36, 0.000, 1),
(29, 'Bouvet Island', 'BV', 'BVT', 37, 0.000, 1),
(30, 'Brazil', 'BR', 'BRA', 38, 0.000, 1),
(32, 'Brunei Darussalam', 'BN', 'BRN', 39, 0.000, 1),
(33, 'Bulgaria', 'BG', 'BGR', 40, 0.000, 1),
(34, 'Burkina Faso', 'BF', 'BFA', 41, 0.000, 1),
(35, 'Burundi', 'BI', 'BDI', 42, 0.000, 1),
(36, 'Cambodia', 'KH', 'KHM', 43, 0.000, 1),
(37, 'Cameroon', 'CM', 'CMR', 44, 0.000, 1),
(38, 'Canada', 'CA', 'CAN', 2, 0.000, 1),
(39, 'Cape Verde', 'CV', 'CPV', 46, 0.000, 1),
(40, 'Cayman Islands', 'KY', 'CYM', 47, 0.000, 1),
(42, 'Chad', 'TD', 'TCD', 48, 0.000, 1),
(43, 'Chile', 'CL', 'CHL', 49, 0.000, 1),
(44, 'China', 'CN', 'CHN', 50, 0.000, 1),
(45, 'Christmas Island', 'CX', 'CXR', 51, 0.000, 1),
(47, 'Colombia', 'CO', 'COL', 52, 0.000, 1),
(48, 'Comoros', 'KM', 'COM', 53, 0.000, 1),
(49, 'Congo', 'CG', 'COG', 54, 0.000, 1),
(50, 'Cook Islands', 'CK', 'COK', 55, 0.000, 1),
(51, 'Costa Rica', 'CR', 'CRI', 56, 0.000, 1),
(52, 'Cote D''''Ivoire', 'CI', 'CIV', 57, 0.000, 1),
(53, 'Croatia', 'HR', 'HRV', 58, 0.000, 1),
(54, 'Cuba', 'CU', 'CUB', 59, 0.000, 1),
(55, 'Cyprus', 'CY', 'CYP', 60, 0.000, 1),
(56, 'Czech Republic', 'CZ', 'CZE', 61, 0.000, 1),
(57, 'Denmark', 'DK', 'DNK', 62, 0.000, 1),
(58, 'Djibouti', 'DJ', 'DJI', 63, 0.000, 1),
(59, 'Dominica', 'DM', 'DMA', 64, 0.000, 1),
(60, 'Dominican Republic', 'DO', 'DOM', 65, 0.000, 1),
(61, 'East Timor', 'TP', 'TMP', 66, 0.000, 1),
(62, 'Ecuador', 'EC', 'ECU', 67, 0.000, 1),
(63, 'Egypt', 'EG', 'EGY', 68, 0.000, 1),
(64, 'El Salvador', 'SV', 'SLV', 69, 0.000, 1),
(65, 'Equatorial Guinea', 'GQ', 'GNQ', 70, 0.000, 1),
(66, 'Eritrea', 'ER', 'ERI', 71, 0.000, 1),
(67, 'Estonia', 'EE', 'EST', 72, 0.000, 1),
(68, 'Ethiopia', 'ET', 'ETH', 73, 0.000, 1),
(70, 'Faroe Islands', 'FO', 'FRO', 74, 0.000, 1),
(71, 'Fiji', 'FJ', 'FJI', 75, 0.000, 1),
(72, 'Finland', 'FI', 'FIN', 76, 0.000, 1),
(73, 'France', 'FR', 'FRA', 77, 0.000, 1),
(74, 'France, Metropolitan', 'FX', 'FXX', 78, 0.000, 1),
(75, 'French Guiana', 'GF', 'GUF', 79, 0.000, 1),
(76, 'French Polynesia', 'PF', 'PYF', 80, 0.000, 1),
(78, 'Gabon', 'GA', 'GAB', 81, 0.000, 1),
(79, 'Gambia', 'GM', 'GMB', 82, 0.000, 1),
(80, 'Georgia', 'GE', 'GEO', 83, 0.000, 1),
(81, 'Germany', 'DE', 'DEU', 84, 0.000, 1),
(82, 'Ghana', 'GH', 'GHA', 85, 0.000, 1),
(83, 'Gibraltar', 'GI', 'GIB', 86, 0.000, 1),
(84, 'Greece', 'GR', 'GRC', 87, 0.000, 1),
(85, 'Greenland', 'GL', 'GRL', 88, 0.000, 1),
(86, 'Grenada', 'GD', 'GRD', 89, 0.000, 1),
(87, 'Guadeloupe', 'GP', 'GLP', 90, 0.000, 1),
(88, 'Guam', 'GU', 'GUM', 91, 0.000, 1),
(89, 'Guatemala', 'GT', 'GTM', 92, 0.000, 1),
(90, 'Guinea', 'GN', 'GIN', 93, 0.000, 1),
(91, 'Guinea-bissau', 'GW', 'GNB', 94, 0.000, 1),
(92, 'Guyana', 'GY', 'GUY', 95, 0.000, 1),
(93, 'Haiti', 'HT', 'HTI', 96, 0.000, 1),
(95, 'Honduras', 'HN', 'HND', 97, 0.000, 1),
(96, 'Hong Kong', 'HK', 'HKG', 98, 0.000, 1),
(97, 'Hungary', 'HU', 'HUN', 99, 0.000, 1),
(98, 'Iceland', 'IS', 'ISL', 100, 0.000, 1),
(99, 'India', 'IN', 'IND', 101, 0.000, 1),
(100, 'Indonesia', 'ID', 'IDN', 102, 0.000, 1),
(102, 'Iraq', 'IQ', 'IRQ', 103, 0.000, 1),
(103, 'Ireland', 'IE', 'IRL', 104, 0.000, 1),
(104, 'Israel', 'IL', 'ISR', 105, 0.000, 1),
(105, 'Italy', 'IT', 'ITA', 106, 0.000, 1),
(106, 'Jamaica', 'JM', 'JAM', 107, 0.000, 1),
(107, 'Japan', 'JP', 'JPN', 108, 0.000, 1),
(108, 'Jordan', 'JO', 'JOR', 109, 0.000, 1),
(109, 'Kazakhstan', 'KZ', 'KAZ', 110, 0.000, 1),
(110, 'Kenya', 'KE', 'KEN', 111, 0.000, 1),
(111, 'Kiribati', 'KI', 'KIR', 112, 0.000, 1),
(113, 'Korea, Republic of', 'KR', 'KOR', 113, 0.000, 1),
(114, 'Kuwait', 'KW', 'KWT', 114, 0.000, 1),
(115, 'Kyrgyzstan', 'KG', 'KGZ', 115, 0.000, 1),
(117, 'Latvia', 'LV', 'LVA', 116, 0.000, 1),
(118, 'Lebanon', 'LB', 'LBN', 117, 0.000, 1),
(119, 'Lesotho', 'LS', 'LSO', 118, 0.000, 1),
(120, 'Liberia', 'LR', 'LBR', 119, 0.000, 1),
(122, 'Liechtenstein', 'LI', 'LIE', 120, 0.000, 1),
(123, 'Lithuania', 'LT', 'LTU', 121, 0.000, 1),
(124, 'Luxembourg', 'LU', 'LUX', 122, 0.000, 1),
(125, 'Macau', 'MO', 'MAC', 123, 0.000, 1),
(127, 'Madagascar', 'MG', 'MDG', 124, 0.000, 1),
(128, 'Malawi', 'MW', 'MWI', 125, 0.000, 1),
(129, 'Malaysia', 'MY', 'MYS', 126, 0.000, 1),
(130, 'Maldives', 'MV', 'MDV', 127, 0.000, 1),
(131, 'Mali', 'ML', 'MLI', 128, 0.000, 1),
(132, 'Malta', 'MT', 'MLT', 129, 0.000, 1),
(133, 'Marshall Islands', 'MH', 'MHL', 130, 0.000, 1),
(134, 'Martinique', 'MQ', 'MTQ', 131, 0.000, 1),
(135, 'Mauritania', 'MR', 'MRT', 132, 0.000, 1),
(136, 'Mauritius', 'MU', 'MUS', 133, 0.000, 1),
(137, 'Mayotte', 'YT', 'MYT', 134, 0.000, 1),
(138, 'Mexico', 'MX', 'MEX', 135, 0.000, 1),
(141, 'Monaco', 'MC', 'MCO', 136, 0.000, 1),
(142, 'Mongolia', 'MN', 'MNG', 137, 0.000, 1),
(143, 'Montserrat', 'MS', 'MSR', 138, 0.000, 1),
(144, 'Morocco', 'MA', 'MAR', 139, 0.000, 1),
(145, 'Mozambique', 'MZ', 'MOZ', 140, 0.000, 1),
(146, 'Myanmar', 'MM', 'MMR', 141, 0.000, 1),
(147, 'Namibia', 'NA', 'NAM', 142, 0.000, 1),
(148, 'Nauru', 'NR', 'NRU', 143, 0.000, 1),
(149, 'Nepal', 'NP', 'NPL', 144, 0.000, 1),
(150, 'Netherlands', 'NL', 'NLD', 145, 0.000, 1),
(151, 'Netherlands Antilles', 'AN', 'ANT', 146, 0.000, 1),
(152, 'New Caledonia', 'NC', 'NCL', 147, 0.000, 1),
(153, 'New Zealand', 'NZ', 'NZL', 148, 0.000, 1),
(154, 'Nicaragua', 'NI', 'NIC', 149, 0.000, 1),
(155, 'Niger', 'NE', 'NER', 150, 0.000, 1),
(156, 'Nigeria', 'NG', 'NGA', 151, 0.000, 1),
(157, 'Niue', 'NU', 'NIU', 152, 0.000, 1),
(158, 'Norfolk Island', 'NF', 'NFK', 153, 0.000, 1),
(160, 'Norway', 'NO', 'NOR', 154, 0.000, 1),
(161, 'Oman', 'OM', 'OMN', 155, 0.000, 1),
(162, 'Pakistan', 'PK', 'PAK', 156, 0.000, 1),
(163, 'Palau', 'PW', 'PLW', 157, 0.000, 1),
(164, 'Panama', 'PA', 'PAN', 158, 0.000, 1),
(165, 'Papua New Guinea', 'PG', 'PNG', 159, 0.000, 1),
(166, 'Paraguay', 'PY', 'PRY', 160, 0.000, 1),
(167, 'Peru', 'PE', 'PER', 161, 0.000, 1),
(168, 'Philippines', 'PH', 'PHL', 162, 0.000, 1),
(169, 'Pitcairn', 'PN', 'PCN', 163, 0.000, 1),
(170, 'Poland', 'PL', 'POL', 164, 0.000, 1),
(171, 'Portugal', 'PT', 'PRT', 165, 0.000, 1),
(172, 'Puerto Rico', 'PR', 'PRI', 166, 0.000, 1),
(173, 'Qatar', 'QA', 'QAT', 167, 0.000, 1),
(174, 'Reunion', 'RE', 'REU', 168, 0.000, 1),
(175, 'Romania', 'RO', 'ROM', 169, 0.000, 1),
(176, 'Russian Federation', 'RU', 'RUS', 170, 0.000, 1),
(177, 'Rwanda', 'RW', 'RWA', 171, 0.000, 1),
(178, 'Saint Kitts and Nevis', 'KN', 'KNA', 172, 0.000, 1),
(179, 'Saint Lucia', 'LC', 'LCA', 173, 0.000, 1),
(181, 'Samoa', 'WS', 'WSM', 174, 0.000, 1),
(182, 'San Marino', 'SM', 'SMR', 175, 0.000, 1),
(183, 'Sao Tome and Principe', 'ST', 'STP', 176, 0.000, 1),
(184, 'Saudi Arabia', 'SA', 'SAU', 177, 0.000, 1),
(185, 'Senegal', 'SN', 'SEN', 178, 0.000, 1),
(186, 'Seychelles', 'SC', 'SYC', 179, 0.000, 1),
(187, 'Sierra Leone', 'SL', 'SLE', 180, 0.000, 1),
(188, 'Singapore', 'SG', 'SGP', 181, 0.000, 1),
(190, 'Slovenia', 'SI', 'SVN', 182, 0.000, 1),
(191, 'Solomon Islands', 'SB', 'SLB', 183, 0.000, 1),
(192, 'Somalia', 'SO', 'SOM', 184, 0.000, 1),
(193, 'South Africa', 'ZA', 'ZAF', 185, 0.000, 1),
(195, 'Spain', 'ES', 'ESP', 186, 0.000, 1),
(196, 'Sri Lanka', 'LK', 'LKA', 187, 0.000, 1),
(197, 'St. Helena', 'SH', 'SHN', 188, 0.000, 1),
(198, 'St. Pierre and Miquelon', 'PM', 'SPM', 189, 0.000, 1),
(199, 'Sudan', 'SD', 'SDN', 190, 0.000, 1),
(200, 'Suriname', 'SR', 'SUR', 191, 0.000, 1),
(202, 'Swaziland', 'SZ', 'SWZ', 192, 0.000, 1),
(203, 'Sweden', 'SE', 'SWE', 193, 0.000, 1),
(204, 'Switzerland', 'CH', 'CHE', 194, 0.000, 1),
(205, 'Syrian Arab Republic', 'SY', 'SYR', 195, 0.000, 1),
(206, 'Taiwan', 'TW', 'TWN', 196, 0.000, 1),
(207, 'Tajikistan', 'TJ', 'TJK', 197, 0.000, 1),
(209, 'Thailand', 'TH', 'THA', 198, 0.000, 1),
(210, 'Togo', 'TG', 'TGO', 199, 0.000, 1),
(211, 'Tokelau', 'TK', 'TKL', 200, 0.000, 1),
(212, 'Tonga', 'TO', 'TON', 201, 0.000, 1),
(213, 'Trinidad and Tobago', 'TT', 'TTO', 202, 0.000, 1),
(214, 'Tunisia', 'TN', 'TUN', 203, 0.000, 1),
(215, 'Turkey', 'TR', 'TUR', 204, 0.000, 1),
(216, 'Turkmenistan', 'TM', 'TKM', 205, 0.000, 1),
(217, 'Turks and Caicos Islands', 'TC', 'TCA', 206, 0.000, 1),
(218, 'Tuvalu', 'TV', 'TUV', 207, 0.000, 1),
(219, 'Uganda', 'UG', 'UGA', 208, 0.000, 1),
(220, 'Ukraine', 'UA', 'UKR', 209, 0.000, 1),
(221, 'United Arab Emirates', 'AE', 'ARE', 210, 0.000, 1),
(222, 'United Kingdom', 'GB', 'GBR', 211, 0.000, 1),
(223, 'United States', 'US', 'USA', 1, 0.000, 1),
(224, 'US Minor Outlying Islands', 'UM', 'UMI', 213, 0.000, 1),
(225, 'Uruguay', 'UY', 'URY', 214, 0.000, 1),
(226, 'Uzbekistan', 'UZ', 'UZB', 215, 0.000, 1),
(227, 'Vanuatu', 'VU', 'VUT', 216, 0.000, 1),
(229, 'Venezuela', 'VE', 'VEN', 217, 0.000, 1),
(230, 'Viet Nam', 'VN', 'VNM', 218, 0.000, 1),
(231, 'Virgin Islands (British)', 'VG', 'VGB', 219, 0.000, 1),
(232, 'Virgin Islands (U.S.)', 'VI', 'VIR', 220, 0.000, 1),
(233, 'Wallis and Futuna Islands', 'WF', 'WLF', 221, 0.000, 1),
(234, 'Western Sahara', 'EH', 'ESH', 222, 0.000, 1),
(235, 'Yemen', 'YE', 'YEM', 223, 0.000, 1),
(236, 'Yugoslavia', 'YU', 'YUG', 224, 0.000, 1),
(237, 'Zaire', 'ZR', 'ZAR', 225, 0.000, 1),
(238, 'Zambia', 'ZM', 'ZMB', 226, 0.000, 1),
(239, 'Zimbabwe', 'ZW', 'ZWE', 227, 0.000, 1),
(240, 'Slovakia', 'SK', 'SVK', 182, 0.000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ec_customfield`
--

CREATE TABLE IF NOT EXISTS `ec_customfield` (
  `customfield_id` int(11) NOT NULL AUTO_INCREMENT,
  `table_name` varchar(30) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `field_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `field_label` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`customfield_id`),
  UNIQUE KEY `customfield_id` (`customfield_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_customfielddata`
--

CREATE TABLE IF NOT EXISTS `ec_customfielddata` (
  `customfielddata_id` int(11) NOT NULL AUTO_INCREMENT,
  `customfield_id` int(11) DEFAULT NULL,
  `table_id` int(11) NOT NULL,
  `data` blob NOT NULL,
  PRIMARY KEY (`customfielddata_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_download`
--

CREATE TABLE IF NOT EXISTS `ec_download` (
  `download_id` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `download_count` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `download_file_name` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `is_amazon_download` tinyint(1) NOT NULL DEFAULT '0',
  `amazon_key` varchar(1024) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`download_id`),
  KEY `download_order_id` (`order_id`),
  KEY `download_product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ec_giftcard`
--

CREATE TABLE IF NOT EXISTS `ec_giftcard` (
  `giftcard_id` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `amount` float(15,3) NOT NULL DEFAULT '0.000',
  `message` text COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`giftcard_id`),
  UNIQUE KEY `giftcard_id` (`giftcard_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ec_live_rate_cache`
--

CREATE TABLE IF NOT EXISTS `ec_live_rate_cache` (
  `live_rate_cache_id` int(11) NOT NULL AUTO_INCREMENT,
  `ec_cart_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `rate_data` text COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`live_rate_cache_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_manufacturer`
--

CREATE TABLE IF NOT EXISTS `ec_manufacturer` (
  `manufacturer_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_demo_item` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `clicks` int(11) NOT NULL DEFAULT '0',
  `post_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`manufacturer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ec_manufacturer`
--

INSERT INTO `ec_manufacturer` (`manufacturer_id`, `is_demo_item`, `name`, `clicks`, `post_id`) VALUES
(1, 0, 'Oz Production Event Services, LLC', 0, 4);

-- --------------------------------------------------------

--
-- Table structure for table `ec_menulevel1`
--

CREATE TABLE IF NOT EXISTS `ec_menulevel1` (
  `menulevel1_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_demo_item` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `clicks` int(11) NOT NULL DEFAULT '0',
  `seo_keywords` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `seo_description` blob,
  `banner_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`menulevel1_id`),
  UNIQUE KEY `menu1_menulevel1_id` (`menulevel1_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_menulevel2`
--

CREATE TABLE IF NOT EXISTS `ec_menulevel2` (
  `menulevel2_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_demo_item` tinyint(1) NOT NULL DEFAULT '0',
  `menulevel1_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `clicks` int(11) NOT NULL DEFAULT '0',
  `seo_keywords` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `seo_description` blob,
  `banner_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`menulevel2_id`),
  UNIQUE KEY `menu2_menulevel2_id` (`menulevel2_id`),
  KEY `menu2_menulevel1_id` (`menulevel1_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_menulevel3`
--

CREATE TABLE IF NOT EXISTS `ec_menulevel3` (
  `menulevel3_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_demo_item` tinyint(1) NOT NULL DEFAULT '0',
  `menulevel2_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `clicks` int(11) NOT NULL DEFAULT '0',
  `seo_keywords` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `seo_description` blob,
  `banner_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`menulevel3_id`),
  UNIQUE KEY `menu3_menulevel3_id` (`menulevel3_id`),
  KEY `menu3_menulevel2_id` (`menulevel2_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_option`
--

CREATE TABLE IF NOT EXISTS `ec_option` (
  `option_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_demo_item` tinyint(1) NOT NULL DEFAULT '0',
  `option_name` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_label` text COLLATE utf8mb4_unicode_520_ci,
  `option_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'combo',
  `option_required` tinyint(1) NOT NULL DEFAULT '1',
  `option_error_text` text COLLATE utf8mb4_unicode_520_ci,
  `option_meta` text COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_option_id` (`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_optionitem`
--

CREATE TABLE IF NOT EXISTS `ec_optionitem` (
  `optionitem_id` int(11) NOT NULL AUTO_INCREMENT,
  `option_id` int(11) NOT NULL DEFAULT '0',
  `optionitem_name` text COLLATE utf8mb4_unicode_520_ci,
  `optionitem_price` float(15,3) NOT NULL DEFAULT '0.000',
  `optionitem_price_onetime` float(15,3) NOT NULL DEFAULT '0.000',
  `optionitem_price_override` float(15,3) NOT NULL DEFAULT '-1.000',
  `optionitem_price_multiplier` float(15,3) NOT NULL DEFAULT '0.000',
  `optionitem_price_per_character` float(15,3) NOT NULL DEFAULT '0.000',
  `optionitem_weight` float(15,3) NOT NULL DEFAULT '0.000',
  `optionitem_weight_onetime` float(15,3) NOT NULL DEFAULT '0.000',
  `optionitem_weight_override` float(15,3) NOT NULL DEFAULT '-1.000',
  `optionitem_weight_multiplier` float(15,3) NOT NULL DEFAULT '0.000',
  `optionitem_order` int(11) NOT NULL DEFAULT '1',
  `optionitem_icon` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_initial_value` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_model_number` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_allow_download` tinyint(1) NOT NULL DEFAULT '1',
  `optionitem_disallow_shipping` tinyint(1) NOT NULL DEFAULT '0',
  `optionitem_initially_selected` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`optionitem_id`),
  KEY `option_id` (`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_optionitemimage`
--

CREATE TABLE IF NOT EXISTS `ec_optionitemimage` (
  `optionitemimage_id` int(11) NOT NULL AUTO_INCREMENT,
  `optionitem_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `image1` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `image2` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `image3` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `image4` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `image5` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`optionitemimage_id`),
  KEY `optionitem_id` (`optionitem_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_optionitemquantity`
--

CREATE TABLE IF NOT EXISTS `ec_optionitemquantity` (
  `optionitemquantity_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(17) NOT NULL DEFAULT '0',
  `optionitem_id_1` int(11) NOT NULL DEFAULT '0',
  `optionitem_id_2` int(11) NOT NULL DEFAULT '0',
  `optionitem_id_3` int(11) NOT NULL DEFAULT '0',
  `optionitem_id_4` int(11) NOT NULL DEFAULT '0',
  `optionitem_id_5` int(11) NOT NULL DEFAULT '0',
  `quantity` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`optionitemquantity_id`),
  UNIQUE KEY `optionitemquantity_id` (`optionitemquantity_id`),
  KEY `product_id` (`product_id`),
  KEY `optionitem_id_1` (`optionitem_id_1`),
  KEY `optionitem_id_2` (`optionitem_id_2`),
  KEY `optionitem_id_3` (`optionitem_id_3`),
  KEY `optionitem_id_4` (`optionitem_id_4`),
  KEY `optionitem_id_5` (`optionitem_id_5`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_option_to_product`
--

CREATE TABLE IF NOT EXISTS `ec_option_to_product` (
  `option_to_product_id` int(11) NOT NULL AUTO_INCREMENT,
  `option_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `role_label` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'all',
  `option_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`option_to_product_id`),
  UNIQUE KEY `option_to_product_id` (`option_to_product_id`),
  KEY `option_id` (`option_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_order`
--

CREATE TABLE IF NOT EXISTS `ec_order` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_demo_item` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `user_email` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_level` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'shopper',
  `last_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `order_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `orderstatus_id` int(11) NOT NULL DEFAULT '5',
  `order_weight` float(15,3) NOT NULL DEFAULT '0.000',
  `sub_total` float(15,3) NOT NULL DEFAULT '0.000',
  `tax_total` float(15,3) NOT NULL DEFAULT '0.000',
  `shipping_total` float(15,3) NOT NULL DEFAULT '0.000',
  `discount_total` float(15,3) NOT NULL DEFAULT '0.000',
  `vat_total` float(15,3) NOT NULL DEFAULT '0.000',
  `vat_rate` float(15,3) NOT NULL DEFAULT '0.000',
  `duty_total` float(15,3) NOT NULL DEFAULT '0.000',
  `gst_total` float(15,3) NOT NULL DEFAULT '0.000',
  `gst_rate` float(15,3) NOT NULL DEFAULT '0.000',
  `pst_total` float(15,3) NOT NULL DEFAULT '0.000',
  `pst_rate` float(15,3) NOT NULL DEFAULT '0.000',
  `hst_total` float(15,3) NOT NULL DEFAULT '0.000',
  `hst_rate` float(15,3) NOT NULL DEFAULT '0.000',
  `grand_total` float(15,3) NOT NULL DEFAULT '0.000',
  `refund_total` float(15,3) NOT NULL DEFAULT '0.000',
  `promo_code` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `giftcard_id` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `use_expedited_shipping` tinyint(1) NOT NULL DEFAULT '0',
  `shipping_method` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_carrier` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_service_code` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tracking_number` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_first_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_last_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_delivery_date_time` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_pickup_date_time` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_address_line_1` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_address_line_2` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_city` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_state` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_country` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_zip` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_phone` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_first_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_last_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_address_line_1` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_address_line_2` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_city` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_state` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_country` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_zip` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_phone` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `vat_registration_number` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `payment_method` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `paypal_email_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `paypal_transaction_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `paypal_payer_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `order_viewed` tinyint(1) NOT NULL DEFAULT '0',
  `order_notes` text COLLATE utf8mb4_unicode_520_ci,
  `order_customer_notes` blob,
  `txn_id` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `payment_txn_id` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `edit_sequence` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `quickbooks_status` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Not Queued',
  `credit_memo_txn_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `card_holder_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `creditcard_digits` varchar(4) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `cc_exp_month` varchar(2) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `cc_exp_year` varchar(4) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fraktjakt_order_id` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fraktjakt_shipment_id` varchar(20) COLLATE utf8mb4_unicode_520_ci DEFAULT '',
  `stripe_charge_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `nets_transaction_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `subscription_id` int(11) NOT NULL DEFAULT '0',
  `order_gateway` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `affirm_charge_id` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_company_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_company_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `guest_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `agreed_to_terms` tinyint(1) NOT NULL DEFAULT '0',
  `order_ip_address` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `gateway_transaction_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`order_id`),
  UNIQUE KEY `order_id` (`order_id`),
  KEY `user_id` (`user_id`),
  KEY `giftcard_id` (`giftcard_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `ec_order`
--

INSERT INTO `ec_order` (`order_id`, `is_demo_item`, `user_id`, `user_email`, `user_level`, `last_updated`, `order_date`, `orderstatus_id`, `order_weight`, `sub_total`, `tax_total`, `shipping_total`, `discount_total`, `vat_total`, `vat_rate`, `duty_total`, `gst_total`, `gst_rate`, `pst_total`, `pst_rate`, `hst_total`, `hst_rate`, `grand_total`, `refund_total`, `promo_code`, `giftcard_id`, `use_expedited_shipping`, `shipping_method`, `shipping_carrier`, `shipping_service_code`, `tracking_number`, `billing_first_name`, `billing_last_name`, `billing_delivery_date_time`, `billing_pickup_date_time`, `billing_address_line_1`, `billing_address_line_2`, `billing_city`, `billing_state`, `billing_country`, `billing_zip`, `billing_phone`, `shipping_first_name`, `shipping_last_name`, `shipping_address_line_1`, `shipping_address_line_2`, `shipping_city`, `shipping_state`, `shipping_country`, `shipping_zip`, `shipping_phone`, `vat_registration_number`, `payment_method`, `paypal_email_id`, `paypal_transaction_id`, `paypal_payer_id`, `order_viewed`, `order_notes`, `order_customer_notes`, `txn_id`, `payment_txn_id`, `edit_sequence`, `quickbooks_status`, `credit_memo_txn_id`, `card_holder_name`, `creditcard_digits`, `cc_exp_month`, `cc_exp_year`, `fraktjakt_order_id`, `fraktjakt_shipment_id`, `stripe_charge_id`, `nets_transaction_id`, `subscription_id`, `order_gateway`, `affirm_charge_id`, `billing_company_name`, `shipping_company_name`, `guest_key`, `agreed_to_terms`, `order_ip_address`, `gateway_transaction_id`) VALUES
(1, 0, 0, 'mamunbdaiub@gmail.com', '', '2018-05-02 12:51:38', '2018-05-02 12:51:38', 14, 0.000, 200.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 200.000, 0.000, '', '', 0, '', '', '', '', 'Abdullah', 'Mamun', '2018/05/02 06:51 PM', '2018/05/02 06:51 PM', 'NY, Newyork, USA', '', 'Kansas', 'DE', 'US', '10991', '43535353', 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'DE', 'US', '10991', '43535353', '', 'manual_bill', '', '', '', 1, NULL, '', '', '', '', 'Not Queued', '', '', '', '', '', '', '', '', '', 0, '0', '', '', '', 'PXXSAUUDPXCCWIDNTRPSSBSNTAEHHQ', 0, '0', ''),
(2, 0, 1, 'mdrajibul@gmail.com', 'shopper', '2018-05-02 15:06:00', '2018-05-02 15:06:00', 14, 0.000, 170.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 170.000, 0.000, '', '', 0, '', '', '', '', 'Rajibul', 'Islam', '2018/05/16 09:05 PM', '2018/05/24 09:05 PM', 'NY, Newyork, USA', '', 'Kansas', 'KS', 'US', '10991', '43535353', 'Rajibul', 'Islam', 'NY, Newyork, USA', '', 'Kansas', 'KS', 'US', '10991', '43535353', '', 'manual_bill', '', '', '', 0, NULL, '', '', '', '', 'Not Queued', '', '', '', '', '', '', '', '', '', 0, '0', '', 'Metabu Limited', 'Metabu Limited', '', 0, '0', ''),
(3, 0, 1, 'mdrajibul@gmail.com', 'shopper', '2018-05-03 12:13:48', '2018-05-03 12:13:48', 14, 0.000, 53.500, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 53.500, 0.000, '', '', 0, '', '', '', '', 'Rajibul', 'Islam', '2018/05/03 06:13 PM', '2018/05/03 06:13 PM', 'NY, Newyork, USA', '', 'Kansas', 'KS', 'US', '10991', '43535353', 'Rajibul', 'Islam', 'NY, Newyork, USA', '', 'Kansas', 'KS', 'US', '10991', '43535353', '', 'manual_bill', '', '', '', 0, NULL, '', '', '', '', 'Not Queued', '', '', '', '', '', '', '', '', '', 0, '0', '', 'Metabu Limited', 'Metabu Limited', '', 0, '0', '');

-- --------------------------------------------------------

--
-- Table structure for table `ec_orderdetail`
--

CREATE TABLE IF NOT EXISTS `ec_orderdetail` (
  `orderdetail_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `model_number` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `order_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `unit_price` float(15,3) NOT NULL DEFAULT '0.000',
  `total_price` float(15,3) NOT NULL DEFAULT '0.000',
  `quantity` int(11) NOT NULL DEFAULT '0',
  `days` int(11) DEFAULT '0',
  `image1` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `optionitem_id_1` int(11) NOT NULL DEFAULT '0',
  `optionitem_id_2` int(11) NOT NULL DEFAULT '0',
  `optionitem_id_3` int(11) NOT NULL DEFAULT '0',
  `optionitem_id_4` int(11) NOT NULL DEFAULT '0',
  `optionitem_id_5` int(11) NOT NULL DEFAULT '0',
  `optionitem_name_1` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_name_2` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_name_3` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_name_4` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_name_5` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_label_1` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_label_2` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_label_3` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_label_4` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_label_5` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_price_1` float(15,3) NOT NULL DEFAULT '0.000',
  `optionitem_price_2` float(15,3) NOT NULL DEFAULT '0.000',
  `optionitem_price_3` float(15,3) NOT NULL DEFAULT '0.000',
  `optionitem_price_4` float(15,3) NOT NULL DEFAULT '0.000',
  `optionitem_price_5` float(15,3) NOT NULL DEFAULT '0.000',
  `use_advanced_optionset` tinyint(1) NOT NULL DEFAULT '0',
  `giftcard_id` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipper_id` int(11) DEFAULT '0',
  `shipper_first_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipper_last_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `gift_card_message` text COLLATE utf8mb4_unicode_520_ci,
  `gift_card_from_name` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `gift_card_to_name` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `is_download` tinyint(1) NOT NULL DEFAULT '0',
  `is_giftcard` tinyint(1) NOT NULL DEFAULT '0',
  `is_taxable` tinyint(1) NOT NULL DEFAULT '1',
  `is_shippable` tinyint(1) NOT NULL DEFAULT '1',
  `download_file_name` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `download_key` text COLLATE utf8mb4_unicode_520_ci,
  `maximum_downloads_allowed` int(11) NOT NULL DEFAULT '0',
  `download_timelimit_seconds` int(11) DEFAULT '0',
  `is_amazon_download` tinyint(1) NOT NULL DEFAULT '0',
  `amazon_key` varchar(1024) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `is_deconetwork` tinyint(1) NOT NULL DEFAULT '0',
  `deconetwork_id` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_product_code` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_options` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_color_code` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_product_id` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_image_link` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `gift_card_email` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `include_code` tinyint(1) NOT NULL DEFAULT '0',
  `subscription_signup_fee` float(15,3) NOT NULL DEFAULT '0.000',
  `stock_adjusted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`orderdetail_id`),
  UNIQUE KEY `orderdetail_id` (`orderdetail_id`),
  KEY `orderdetail_order_id` (`order_id`),
  KEY `orderdetail_product_id` (`product_id`),
  KEY `orderdetail_giftcard_id` (`giftcard_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `ec_orderdetail`
--

INSERT INTO `ec_orderdetail` (`orderdetail_id`, `order_id`, `product_id`, `title`, `model_number`, `order_date`, `unit_price`, `total_price`, `quantity`, `days`, `image1`, `optionitem_id_1`, `optionitem_id_2`, `optionitem_id_3`, `optionitem_id_4`, `optionitem_id_5`, `optionitem_name_1`, `optionitem_name_2`, `optionitem_name_3`, `optionitem_name_4`, `optionitem_name_5`, `optionitem_label_1`, `optionitem_label_2`, `optionitem_label_3`, `optionitem_label_4`, `optionitem_label_5`, `optionitem_price_1`, `optionitem_price_2`, `optionitem_price_3`, `optionitem_price_4`, `optionitem_price_5`, `use_advanced_optionset`, `giftcard_id`, `shipper_id`, `shipper_first_name`, `shipper_last_name`, `gift_card_message`, `gift_card_from_name`, `gift_card_to_name`, `is_download`, `is_giftcard`, `is_taxable`, `is_shippable`, `download_file_name`, `download_key`, `maximum_downloads_allowed`, `download_timelimit_seconds`, `is_amazon_download`, `amazon_key`, `is_deconetwork`, `deconetwork_id`, `deconetwork_name`, `deconetwork_product_code`, `deconetwork_options`, `deconetwork_color_code`, `deconetwork_product_id`, `deconetwork_image_link`, `gift_card_email`, `include_code`, `subscription_signup_fee`, `stock_adjusted`) VALUES
(1, 1, 2, 'Table', 'table', '0000-00-00 00:00:00', 10.000, 200.000, 4, 5, '0', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', 0.000, 0.000, 0.000, 0.000, 0.000, 0, '0', 0, '', '', '', '0', '', 0, 0, 0, 1, '', '0', 0, 0, 0, '0', 0, '', '', '', '', '', '', '0', '0', 0, 0.000, 0),
(2, 2, 3, 'Pop Up Tent', 'Quest-Canopy', '0000-00-00 00:00:00', 40.000, 160.000, 2, 2, '0', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', 0.000, 0.000, 0.000, 0.000, 0.000, 0, '0', 0, '', '', '', '0', '', 0, 0, 1, 1, '', '0', 0, 0, 0, '0', 0, '', '', '', '', '', '', '0', '0', 0, 0.000, 0),
(3, 2, 2, 'Table', 'table', '0000-00-00 00:00:00', 10.000, 10.000, 1, 1, '0', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', 0.000, 0.000, 0.000, 0.000, 0.000, 0, '0', 0, '', '', '', '0', '', 0, 0, 0, 1, '', '0', 0, 0, 0, '0', 0, '', '', '', '', '', '', '0', '0', 0, 0.000, 0),
(4, 3, 1, 'Chair', 'Chair', '0000-00-00 00:00:00', 1.750, 3.500, 1, 2, '0', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', 0.000, 0.000, 0.000, 0.000, 0.000, 0, '0', 0, '', '', '', '0', '', 0, 0, 1, 1, '', '0', 0, 0, 0, '0', 0, '', '', '', '', '', '', '0', '0', 0, 0.000, 0),
(5, 3, 3, 'Pop Up Tent', 'Quest-Canopy', '0000-00-00 00:00:00', 40.000, 40.000, 1, 1, '0', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', 0.000, 0.000, 0.000, 0.000, 0.000, 0, '0', 0, '', '', '', '0', '', 0, 0, 1, 1, '', '0', 0, 0, 0, '0', 0, '', '', '', '', '', '', '0', '0', 0, 0.000, 0),
(6, 3, 2, 'Table', 'table', '0000-00-00 00:00:00', 10.000, 10.000, 1, 1, '0', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', 0.000, 0.000, 0.000, 0.000, 0.000, 0, '0', 0, '', '', '', '0', '', 0, 0, 0, 1, '', '0', 0, 0, 0, '0', 0, '', '', '', '', '', '', '0', '0', 0, 0.000, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ec_orderstatus`
--

CREATE TABLE IF NOT EXISTS `ec_orderstatus` (
  `status_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_status` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `is_approved` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`status_id`),
  UNIQUE KEY `orderstatus_status_id` (`status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=20 ;

--
-- Dumping data for table `ec_orderstatus`
--

INSERT INTO `ec_orderstatus` (`status_id`, `order_status`, `is_approved`) VALUES
(1, 'Status Not Found', 0),
(2, 'Order Shipped', 1),
(3, 'Order Confirmed', 1),
(4, 'Order on Hold', 0),
(5, 'Order Started', 0),
(6, 'Card Approved', 1),
(7, 'Card Denied', 0),
(8, 'Third Party Pending', 0),
(9, 'Third Party Error', 0),
(10, 'Third Party Approved', 1),
(11, 'Ready for Pickup', 1),
(12, 'Pending Approval', 0),
(14, 'Direct Deposit Pending', 0),
(15, 'Direct Deposit Received', 1),
(16, 'Refunded Order', 0),
(17, 'Partial Refund', 1),
(18, 'Order Picked Up', 1),
(19, 'Order Cancelled', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ec_order_option`
--

CREATE TABLE IF NOT EXISTS `ec_order_option` (
  `order_option_id` int(11) NOT NULL AUTO_INCREMENT,
  `orderdetail_id` int(11) NOT NULL DEFAULT '0',
  `option_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_name` text COLLATE utf8mb4_unicode_520_ci,
  `option_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'combo',
  `option_value` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `option_price_change` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_allow_download` tinyint(1) NOT NULL DEFAULT '1',
  `option_label` text COLLATE utf8mb4_unicode_520_ci,
  `option_to_product_id` int(11) NOT NULL DEFAULT '0',
  `option_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`order_option_id`),
  UNIQUE KEY `order_option_id` (`order_option_id`),
  KEY `orderdetail_id` (`orderdetail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_pageoption`
--

CREATE TABLE IF NOT EXISTS `ec_pageoption` (
  `pageoption_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL DEFAULT '0',
  `option_type` varchar(155) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`pageoption_id`),
  UNIQUE KEY `pageoption_id` (`pageoption_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_perpage`
--

CREATE TABLE IF NOT EXISTS `ec_perpage` (
  `perpage_id` int(11) NOT NULL AUTO_INCREMENT,
  `perpage` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`perpage_id`),
  UNIQUE KEY `perpageid` (`perpage_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `ec_perpage`
--

INSERT INTO `ec_perpage` (`perpage_id`, `perpage`) VALUES
(1, 50),
(2, 25),
(3, 10);

-- --------------------------------------------------------

--
-- Table structure for table `ec_pricepoint`
--

CREATE TABLE IF NOT EXISTS `ec_pricepoint` (
  `pricepoint_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_less_than` tinyint(1) NOT NULL DEFAULT '0',
  `is_greater_than` tinyint(1) NOT NULL DEFAULT '0',
  `low_point` float(15,3) NOT NULL DEFAULT '0.000',
  `high_point` float(15,3) DEFAULT '0.000',
  `pricepoint_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pricepoint_id`),
  UNIQUE KEY `pricepoint_pricepoint_id` (`pricepoint_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `ec_pricepoint`
--

INSERT INTO `ec_pricepoint` (`pricepoint_id`, `is_less_than`, `is_greater_than`, `low_point`, `high_point`, `pricepoint_order`) VALUES
(1, 1, 0, 0.000, 10.000, 0),
(2, 0, 0, 25.000, 49.990, 4),
(3, 0, 0, 50.000, 99.990, 5),
(4, 0, 0, 100.000, 299.990, 6),
(5, 0, 2, 299.990, 0.000, 7),
(6, 0, 0, 10.000, 14.990, 1),
(7, 0, 0, 15.000, 19.990, 2),
(8, 0, 0, 20.000, 24.990, 3);

-- --------------------------------------------------------

--
-- Table structure for table `ec_pricetier`
--

CREATE TABLE IF NOT EXISTS `ec_pricetier` (
  `pricetier_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `price` float(15,3) NOT NULL DEFAULT '0.000',
  `quantity` int(11) NOT NULL DEFAULT '10',
  PRIMARY KEY (`pricetier_id`),
  UNIQUE KEY `pricetier_id` (`pricetier_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_product`
--

CREATE TABLE IF NOT EXISTS `ec_product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_demo_item` tinyint(1) NOT NULL DEFAULT '0',
  `model_number` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_id` int(11) NOT NULL DEFAULT '0',
  `activate_in_store` tinyint(1) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8mb4_unicode_520_ci,
  `specifications` text COLLATE utf8mb4_unicode_520_ci,
  `order_completed_note` text COLLATE utf8mb4_unicode_520_ci,
  `order_completed_email_note` text COLLATE utf8mb4_unicode_520_ci,
  `order_completed_details_note` text COLLATE utf8mb4_unicode_520_ci,
  `price` float(15,3) NOT NULL DEFAULT '0.000',
  `list_price` float(15,3) NOT NULL DEFAULT '0.000',
  `product_cost` float(15,3) NOT NULL DEFAULT '0.000',
  `vat_rate` float(15,3) NOT NULL DEFAULT '0.000',
  `handling_price` float(15,3) NOT NULL DEFAULT '0.000',
  `handling_price_each` float(15,3) NOT NULL DEFAULT '0.000',
  `stock_quantity` int(7) NOT NULL DEFAULT '0',
  `min_purchase_quantity` int(11) NOT NULL DEFAULT '0',
  `max_purchase_quantity` int(11) NOT NULL DEFAULT '0',
  `weight` float(15,3) NOT NULL DEFAULT '0.000',
  `width` double(15,3) NOT NULL DEFAULT '1.000',
  `height` double(15,3) NOT NULL DEFAULT '1.000',
  `length` double(15,3) NOT NULL DEFAULT '1.000',
  `seo_description` text COLLATE utf8mb4_unicode_520_ci,
  `seo_keywords` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `use_specifications` tinyint(1) NOT NULL DEFAULT '0',
  `use_customer_reviews` tinyint(1) NOT NULL DEFAULT '0',
  `manufacturer_id` int(11) NOT NULL DEFAULT '0',
  `download_file_name` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `image1` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `image2` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `image3` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `image4` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `image5` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_id_1` int(11) NOT NULL DEFAULT '0',
  `option_id_2` int(11) NOT NULL DEFAULT '0',
  `option_id_3` int(11) NOT NULL DEFAULT '0',
  `option_id_4` int(11) NOT NULL DEFAULT '0',
  `option_id_5` int(11) NOT NULL DEFAULT '0',
  `use_advanced_optionset` tinyint(1) NOT NULL DEFAULT '0',
  `menulevel1_id_1` int(11) NOT NULL DEFAULT '0',
  `menulevel1_id_2` int(11) NOT NULL DEFAULT '0',
  `menulevel1_id_3` int(11) NOT NULL DEFAULT '0',
  `menulevel2_id_1` int(11) NOT NULL DEFAULT '0',
  `menulevel2_id_2` int(11) NOT NULL DEFAULT '0',
  `menulevel2_id_3` int(11) NOT NULL DEFAULT '0',
  `menulevel3_id_1` int(11) NOT NULL DEFAULT '0',
  `menulevel3_id_2` int(11) NOT NULL DEFAULT '0',
  `menulevel3_id_3` int(11) NOT NULL DEFAULT '0',
  `featured_product_id_1` int(11) NOT NULL DEFAULT '0',
  `featured_product_id_2` int(11) NOT NULL DEFAULT '0',
  `featured_product_id_3` int(11) NOT NULL DEFAULT '0',
  `featured_product_id_4` int(11) NOT NULL DEFAULT '0',
  `is_giftcard` tinyint(1) NOT NULL DEFAULT '0',
  `is_download` tinyint(1) NOT NULL DEFAULT '0',
  `is_donation` tinyint(1) NOT NULL DEFAULT '0',
  `is_special` tinyint(1) NOT NULL DEFAULT '0',
  `is_taxable` tinyint(1) NOT NULL DEFAULT '1',
  `is_shippable` tinyint(1) NOT NULL DEFAULT '1',
  `is_subscription_item` tinyint(1) NOT NULL DEFAULT '0',
  `is_preorder` tinyint(1) NOT NULL DEFAULT '0',
  `role_id` tinyint(1) NOT NULL DEFAULT '0',
  `added_to_db_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `show_on_startup` tinyint(1) NOT NULL DEFAULT '0',
  `use_optionitem_images` tinyint(1) NOT NULL DEFAULT '0',
  `use_optionitem_quantity_tracking` tinyint(1) NOT NULL DEFAULT '0',
  `views` int(11) NOT NULL DEFAULT '0',
  `last_viewed` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `show_stock_quantity` tinyint(1) NOT NULL DEFAULT '1',
  `maximum_downloads_allowed` int(11) NOT NULL DEFAULT '0',
  `download_timelimit_seconds` int(11) NOT NULL DEFAULT '0',
  `list_id` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `edit_sequence` varchar(55) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `quickbooks_status` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Not Queued',
  `income_account_ref` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Online Sales',
  `cogs_account_ref` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Cost of Goods Sold',
  `asset_account_ref` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Inventory Asset',
  `quickbooks_parent_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `quickbooks_parent_list_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `subscription_bill_length` int(11) NOT NULL DEFAULT '1',
  `subscription_bill_period` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'M',
  `subscription_bill_duration` int(11) NOT NULL DEFAULT '0',
  `trial_period_days` int(11) NOT NULL DEFAULT '0',
  `stripe_plan_added` tinyint(1) NOT NULL DEFAULT '0',
  `subscription_plan_id` int(11) NOT NULL DEFAULT '0',
  `allow_multiple_subscription_purchases` tinyint(1) NOT NULL DEFAULT '1',
  `membership_page` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `is_amazon_download` tinyint(1) NOT NULL DEFAULT '0',
  `amazon_key` varchar(1024) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `catalog_mode` tinyint(1) NOT NULL DEFAULT '0',
  `catalog_mode_phrase` varchar(1024) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `inquiry_mode` tinyint(1) NOT NULL DEFAULT '0',
  `inquiry_url` varchar(1024) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `is_deconetwork` tinyint(1) NOT NULL DEFAULT '0',
  `deconetwork_mode` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'designer',
  `deconetwork_product_id` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_size_id` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_color_id` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_design_id` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `short_description` text COLLATE utf8mb4_unicode_520_ci,
  `display_type` int(11) NOT NULL DEFAULT '1',
  `image_hover_type` int(11) NOT NULL DEFAULT '3',
  `tag_type` int(11) NOT NULL DEFAULT '0',
  `tag_bg_color` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tag_text_color` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tag_text` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `image_effect_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'none',
  `include_code` tinyint(1) NOT NULL DEFAULT '0',
  `TIC` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '00000',
  `subscription_signup_fee` float(15,3) NOT NULL DEFAULT '0.000',
  `subscription_unique_id` int(11) NOT NULL DEFAULT '0',
  `subscription_prorate` tinyint(1) NOT NULL DEFAULT '1',
  `allow_backorders` tinyint(1) NOT NULL DEFAULT '0',
  `backorder_fill_date` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_class_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_id`),
  UNIQUE KEY `product_product_id` (`product_id`),
  UNIQUE KEY `product_model_number` (`model_number`(191)),
  KEY `product_menulevel1_id_1` (`menulevel1_id_1`,`menulevel2_id_1`,`menulevel3_id_1`),
  KEY `product_menulevel1_id_2` (`menulevel1_id_2`,`menulevel2_id_2`,`menulevel3_id_2`),
  KEY `product_menulevel1_id_3` (`menulevel1_id_3`,`menulevel2_id_3`,`menulevel3_id_3`),
  KEY `product_manufacturer_id` (`manufacturer_id`),
  KEY `product_option_id_1` (`option_id_1`),
  KEY `product_option_id_2` (`option_id_2`),
  KEY `product_option_id_3` (`option_id_3`),
  KEY `product_option_id_4` (`option_id_4`),
  KEY `product_option_id_5` (`option_id_5`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `ec_product`
--

INSERT INTO `ec_product` (`product_id`, `is_demo_item`, `model_number`, `post_id`, `activate_in_store`, `title`, `description`, `specifications`, `order_completed_note`, `order_completed_email_note`, `order_completed_details_note`, `price`, `list_price`, `product_cost`, `vat_rate`, `handling_price`, `handling_price_each`, `stock_quantity`, `min_purchase_quantity`, `max_purchase_quantity`, `weight`, `width`, `height`, `length`, `seo_description`, `seo_keywords`, `use_specifications`, `use_customer_reviews`, `manufacturer_id`, `download_file_name`, `image1`, `image2`, `image3`, `image4`, `image5`, `option_id_1`, `option_id_2`, `option_id_3`, `option_id_4`, `option_id_5`, `use_advanced_optionset`, `menulevel1_id_1`, `menulevel1_id_2`, `menulevel1_id_3`, `menulevel2_id_1`, `menulevel2_id_2`, `menulevel2_id_3`, `menulevel3_id_1`, `menulevel3_id_2`, `menulevel3_id_3`, `featured_product_id_1`, `featured_product_id_2`, `featured_product_id_3`, `featured_product_id_4`, `is_giftcard`, `is_download`, `is_donation`, `is_special`, `is_taxable`, `is_shippable`, `is_subscription_item`, `is_preorder`, `role_id`, `added_to_db_date`, `show_on_startup`, `use_optionitem_images`, `use_optionitem_quantity_tracking`, `views`, `last_viewed`, `show_stock_quantity`, `maximum_downloads_allowed`, `download_timelimit_seconds`, `list_id`, `edit_sequence`, `quickbooks_status`, `income_account_ref`, `cogs_account_ref`, `asset_account_ref`, `quickbooks_parent_name`, `quickbooks_parent_list_id`, `subscription_bill_length`, `subscription_bill_period`, `subscription_bill_duration`, `trial_period_days`, `stripe_plan_added`, `subscription_plan_id`, `allow_multiple_subscription_purchases`, `membership_page`, `is_amazon_download`, `amazon_key`, `catalog_mode`, `catalog_mode_phrase`, `inquiry_mode`, `inquiry_url`, `is_deconetwork`, `deconetwork_mode`, `deconetwork_product_id`, `deconetwork_size_id`, `deconetwork_color_id`, `deconetwork_design_id`, `short_description`, `display_type`, `image_hover_type`, `tag_type`, `tag_bg_color`, `tag_text_color`, `tag_text`, `image_effect_type`, `include_code`, `TIC`, `subscription_signup_fee`, `subscription_unique_id`, `subscription_prorate`, `allow_backorders`, `backorder_fill_date`, `shipping_class_id`) VALUES
(1, 0, 'Chair', 9, 1, 'Chair', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis mattis fringilla lectus et blandit. Integer sit amet nulla sed mi porttitor tincidunt a non ligula. Quisque blandit dui odio, sed consectetur erat malesuada ac. Curabitur placerat orci magna, id malesuada massa iaculis id. Sed a felis id enim euismod tristique varius in libero. Sed tempus tristique felis ac vehicula. Vestibulum viverra, velit non aliquet rhoncus, odio dolor placerat dui, vel porttitor sem arcu ac massa. Mauris elementum, lacus sollicitudin dictum faucibus, magna enim aliquam libero, at pharetra justo quam eget arcu. Donec ut porttitor metus, luctus egestas dolor. Cras lacinia condimentum nunc fermentum vulputate. Donec feugiat auctor purus. Suspendisse lobortis diam eget pellentesque cursus. Proin non dictum lorem, porttitor tempor nisi.</p><p>Integer auctor lobortis nibh, a imperdiet dolor pulvinar a. Donec tempor aliquet mauris, vitae feugiat sapien dapibus sit amet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget libero sed arcu efficitur iaculis in ut quam. Fusce nisl tellus, ultrices vitae ex id, interdum porttitor diam. Aliquam et velit tellus. Vivamus varius nec lacus id interdum. Mauris sollicitudin orci eget lectus porta tincidunt. Phasellus suscipit, justo et ultricies tristique, eros libero maximus lacus, in pulvinar lectus arcu eu eros. Mauris nec convallis ligula, dictum dignissim lacus.</p>', NULL, NULL, NULL, NULL, 1.750, 0.000, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0.000, 1.000, 1.000, 1.000, NULL, '', 0, 0, 1, '', 'http://localhost/oz_production/public_html/wp-content/uploads/2018/04/chair.png', '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, '2018-04-30 14:23:26', 1, 0, 0, 11, '0000-00-00 00:00:00', 0, 0, 0, '', '', 'Not Queued', 'Online Sales', 'Cost of Goods Sold', 'Inventory Asset', '', '', 1, 'M', 0, 0, 0, 0, 1, '', 0, '', 0, NULL, 0, NULL, 0, 'designer', '', '', '', '', NULL, 1, 3, 0, '', '', '', 'none', 0, '00000', 0.000, 0, 1, 0, '', 0),
(2, 0, 'table', 28, 1, 'Table', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis mattis fringilla lectus et blandit. Integer sit amet nulla sed mi porttitor tincidunt a non ligula. Quisque blandit dui odio, sed consectetur erat malesuada ac. Curabitur placerat orci magna, id malesuada massa iaculis id. Sed a felis id enim euismod tristique varius in libero. Sed tempus tristique felis ac vehicula. Vestibulum viverra, velit non aliquet rhoncus, odio dolor placerat dui, vel porttitor sem arcu ac massa. Mauris elementum, lacus sollicitudin dictum faucibus, magna enim aliquam libero, at pharetra justo quam eget arcu. Donec ut porttitor metus, luctus egestas dolor. Cras lacinia condimentum nunc fermentum vulputate. Donec feugiat auctor purus. Suspendisse lobortis diam eget pellentesque cursus. Proin non dictum lorem, porttitor tempor nisi.</p><p>Integer auctor lobortis nibh, a imperdiet dolor pulvinar a. Donec tempor aliquet mauris, vitae feugiat sapien dapibus sit amet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget libero sed arcu efficitur iaculis in ut quam. Fusce nisl tellus, ultrices vitae ex id, interdum porttitor diam. Aliquam et velit tellus. Vivamus varius nec lacus id interdum. Mauris sollicitudin orci eget lectus porta tincidunt. Phasellus suscipit, justo et ultricies tristique, eros libero maximus lacus, in pulvinar lectus arcu eu eros. Mauris nec convallis ligula, dictum dignissim lacus.</p>', NULL, NULL, NULL, NULL, 10.000, 0.000, 0.000, 0.000, 0.000, 0.000, -5, 0, 0, 0.000, 1.000, 1.000, 1.000, NULL, '', 0, 0, 1, '', 'http://localhost/oz_production/public_html/wp-content/uploads/2018/04/table.png', '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, '2018-04-30 15:09:54', 1, 0, 0, 14, '0000-00-00 00:00:00', 0, 0, 0, '', '', 'Not Queued', 'Online Sales', 'Cost of Goods Sold', 'Inventory Asset', '', '', 1, 'M', 0, 0, 0, 0, 1, '', 0, '', 0, NULL, 0, NULL, 0, 'designer', '', '', '', '', NULL, 1, 3, 0, '', '', '', 'none', 0, '00000', 0.000, 0, 1, 0, '', 0),
(3, 0, 'Quest-Canopy', 53, 1, 'Pop Up Tent', 'The Quest® Q64 10 FT. x 10 FT. Slant Leg Instant Up Canopy offers cooling shade when you need it, where you need it. The angled steel frame creates a sturdy 10\\'' x 10\\'' base, while the 8\\'' x 8\\'' dome top offers 64 square feet of shade to lounge beneath or watch from the sidelines. Its instant up design means set-up is a snap, including 4 ground stakes for extra stability. This recreational canopy folds down to fit inside its convenient carry bag.', NULL, NULL, NULL, NULL, 40.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0.000, 1.000, 1.000, 1.000, NULL, '', 0, 0, 1, '', 'http://localhost/oz_production/public_html/wp-content/uploads/2018/05/pop-up-tent.jpg', '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, '2018-05-02 14:52:14', 1, 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, '', '', 'Not Queued', 'Online Sales', 'Cost of Goods Sold', 'Inventory Asset', '', '', 1, 'M', 0, 0, 0, 0, 1, '', 0, '', 0, NULL, 0, NULL, 0, 'designer', '', '', '', '', NULL, 1, 3, 0, '', '', '', 'none', 0, '00000', 0.000, 0, 1, 0, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ec_product_google_attributes`
--

CREATE TABLE IF NOT EXISTS `ec_product_google_attributes` (
  `product_google_attribute_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `attribute_value` text COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`product_google_attribute_id`),
  UNIQUE KEY `product_google_attribute_id` (`product_google_attribute_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_promocode`
--

CREATE TABLE IF NOT EXISTS `ec_promocode` (
  `promocode_id` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `is_dollar_based` tinyint(1) NOT NULL DEFAULT '0',
  `is_percentage_based` tinyint(1) NOT NULL DEFAULT '0',
  `is_shipping_based` tinyint(1) NOT NULL DEFAULT '0',
  `is_free_item_based` tinyint(1) NOT NULL DEFAULT '0',
  `is_for_me_based` tinyint(1) NOT NULL DEFAULT '0',
  `by_manufacturer_id` tinyint(1) NOT NULL DEFAULT '0',
  `by_category_id` tinyint(1) NOT NULL DEFAULT '0',
  `by_product_id` tinyint(1) NOT NULL DEFAULT '0',
  `by_all_products` int(11) NOT NULL DEFAULT '0',
  `promo_dollar` float(15,3) NOT NULL DEFAULT '0.000',
  `promo_percentage` float(15,3) NOT NULL DEFAULT '0.000',
  `promo_shipping` float(15,3) NOT NULL DEFAULT '0.000',
  `promo_free_item` float(15,3) NOT NULL DEFAULT '0.000',
  `promo_for_me` float(15,3) NOT NULL DEFAULT '0.000',
  `manufacturer_id` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `message` blob NOT NULL,
  `max_redemptions` int(11) NOT NULL DEFAULT '999',
  `times_redeemed` int(11) NOT NULL DEFAULT '0',
  `expiration_date` datetime DEFAULT NULL,
  `duration` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'forever',
  `duration_in_months` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`promocode_id`),
  KEY `promo_manufacturer_id` (`manufacturer_id`),
  KEY `promo_product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ec_promotion`
--

CREATE TABLE IF NOT EXISTS `ec_promotion` (
  `promotion_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `type` int(11) NOT NULL DEFAULT '0',
  `start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_date` datetime DEFAULT '0000-00-00 00:00:00',
  `product_id_1` int(11) NOT NULL DEFAULT '0',
  `product_id_2` int(11) NOT NULL DEFAULT '0',
  `product_id_3` int(11) NOT NULL DEFAULT '0',
  `manufacturer_id_1` int(11) NOT NULL DEFAULT '0',
  `manufacturer_id_2` int(11) NOT NULL DEFAULT '0',
  `manufacturer_id_3` int(11) NOT NULL DEFAULT '0',
  `category_id_1` int(11) NOT NULL DEFAULT '0',
  `category_id_2` int(11) NOT NULL DEFAULT '0',
  `category_id_3` int(11) NOT NULL DEFAULT '0',
  `price1` float(15,3) NOT NULL DEFAULT '0.000',
  `price2` float(15,3) NOT NULL DEFAULT '0.000',
  `price3` float(15,3) NOT NULL DEFAULT '0.000',
  `percentage1` float(15,3) NOT NULL DEFAULT '0.000',
  `percentage2` float(15,3) NOT NULL DEFAULT '0.000',
  `percentage3` float(15,3) NOT NULL DEFAULT '0.000',
  `number1` int(11) NOT NULL DEFAULT '0',
  `number2` int(11) NOT NULL DEFAULT '0',
  `number3` int(11) NOT NULL DEFAULT '0',
  `promo_limit` int(11) NOT NULL DEFAULT '3',
  PRIMARY KEY (`promotion_id`),
  UNIQUE KEY `promotion_promotion_id` (`promotion_id`),
  KEY `promotion_product_id_1` (`product_id_1`),
  KEY `promotion_product_id_2` (`product_id_2`),
  KEY `promotion_product_id_3` (`product_id_3`),
  KEY `promotion_manufacturer_id_1` (`manufacturer_id_1`),
  KEY `promotion_manufacturer_id_2` (`manufacturer_id_2`),
  KEY `promotion_manufacturer_id_3` (`manufacturer_id_3`),
  KEY `promotion_category_id_1` (`category_id_1`),
  KEY `promotion_category_id_2` (`category_id_2`),
  KEY `promotion_category_id_3` (`category_id_3`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_response`
--

CREATE TABLE IF NOT EXISTS `ec_response` (
  `response_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_error` tinyint(1) NOT NULL DEFAULT '0',
  `processor` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `response_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `response_text` text COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`response_id`),
  KEY `order_id` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_review`
--

CREATE TABLE IF NOT EXISTS `ec_review` (
  `review_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `rating` int(2) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` mediumblob NOT NULL,
  `date_submitted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`review_id`),
  UNIQUE KEY `review_id` (`review_id`),
  KEY `product_id` (`product_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_role`
--

CREATE TABLE IF NOT EXISTS `ec_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_label` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `admin_access` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`role_id`),
  UNIQUE KEY `role_id` (`role_id`),
  KEY `role_label` (`role_label`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `ec_role`
--

INSERT INTO `ec_role` (`role_id`, `role_label`, `admin_access`) VALUES
(1, 'admin', 1),
(2, 'shopper', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ec_roleaccess`
--

CREATE TABLE IF NOT EXISTS `ec_roleaccess` (
  `roleaccess_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_label` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `admin_panel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`roleaccess_id`),
  UNIQUE KEY `roleaccess_id` (`roleaccess_id`),
  KEY `role_label` (`role_label`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_roleprice`
--

CREATE TABLE IF NOT EXISTS `ec_roleprice` (
  `roleprice_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `role_label` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `role_price` float(15,3) NOT NULL DEFAULT '0.000',
  PRIMARY KEY (`roleprice_id`),
  UNIQUE KEY `roleprice_id` (`roleprice_id`),
  KEY `product_id` (`product_id`),
  KEY `role_label` (`role_label`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_setting`
--

CREATE TABLE IF NOT EXISTS `ec_setting` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `site_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `reg_code` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `storeversion` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `storetype` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'wordpress',
  `storepage` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'store',
  `cartpage` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'cart',
  `accountpage` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'account',
  `timezone` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Europe/London',
  `shipping_method` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'method',
  `shipping_expedite_rate` float(11,2) NOT NULL DEFAULT '0.00',
  `shipping_handling_rate` float(11,2) NOT NULL DEFAULT '0.00',
  `ups_access_license_number` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `ups_user_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `ups_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `ups_ship_from_zip` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `ups_shipper_number` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `ups_country_code` varchar(9) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'US',
  `ups_weight_type` varchar(19) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'LBS',
  `ups_conversion_rate` float(9,3) NOT NULL DEFAULT '1.000',
  `usps_user_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `usps_ship_from_zip` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fedex_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fedex_account_number` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fedex_meter_number` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fedex_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fedex_ship_from_zip` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fedex_weight_units` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'LB',
  `fedex_country_code` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'US',
  `fedex_conversion_rate` float(9,3) NOT NULL DEFAULT '1.000',
  `fedex_test_account` tinyint(1) NOT NULL DEFAULT '0',
  `auspost_api_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `auspost_ship_from_zip` varchar(55) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `dhl_site_id` varchar(155) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `dhl_password` varchar(155) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `dhl_ship_from_country` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'US',
  `dhl_ship_from_zip` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `dhl_weight_unit` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'LB',
  `dhl_test_mode` tinyint(1) NOT NULL DEFAULT '0',
  `fraktjakt_customer_id` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fraktjakt_login_key` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fraktjakt_conversion_rate` double(15,3) NOT NULL DEFAULT '1.000',
  `fraktjakt_test_mode` tinyint(1) NOT NULL DEFAULT '0',
  `fraktjakt_address` varchar(120) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fraktjakt_city` varchar(55) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fraktjakt_state` varchar(2) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fraktjakt_zip` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fraktjakt_country` varchar(2) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `ups_ship_from_state` varchar(2) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `ups_negotiated_rates` tinyint(1) NOT NULL DEFAULT '0',
  `canadapost_username` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `canadapost_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `canadapost_customer_number` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `canadapost_contract_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `canadapost_test_mode` tinyint(1) NOT NULL DEFAULT '0',
  `canadapost_ship_from_zip` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`setting_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ec_setting`
--

INSERT INTO `ec_setting` (`setting_id`, `site_url`, `reg_code`, `storeversion`, `storetype`, `storepage`, `cartpage`, `accountpage`, `timezone`, `shipping_method`, `shipping_expedite_rate`, `shipping_handling_rate`, `ups_access_license_number`, `ups_user_id`, `ups_password`, `ups_ship_from_zip`, `ups_shipper_number`, `ups_country_code`, `ups_weight_type`, `ups_conversion_rate`, `usps_user_name`, `usps_ship_from_zip`, `fedex_key`, `fedex_account_number`, `fedex_meter_number`, `fedex_password`, `fedex_ship_from_zip`, `fedex_weight_units`, `fedex_country_code`, `fedex_conversion_rate`, `fedex_test_account`, `auspost_api_key`, `auspost_ship_from_zip`, `dhl_site_id`, `dhl_password`, `dhl_ship_from_country`, `dhl_ship_from_zip`, `dhl_weight_unit`, `dhl_test_mode`, `fraktjakt_customer_id`, `fraktjakt_login_key`, `fraktjakt_conversion_rate`, `fraktjakt_test_mode`, `fraktjakt_address`, `fraktjakt_city`, `fraktjakt_state`, `fraktjakt_zip`, `fraktjakt_country`, `ups_ship_from_state`, `ups_negotiated_rates`, `canadapost_username`, `canadapost_password`, `canadapost_customer_number`, `canadapost_contract_id`, `canadapost_test_mode`, `canadapost_ship_from_zip`) VALUES
(1, 'localhost/oz_production/public_html', '', '1.0.0', 'wordpress', '6', '7', '8', 'America/Los_Angeles', 'method', 0.00, 0.00, '', '', '', '', '', '', '', 1.000, '', '', '', '', '', '', '', 'LB', 'US', 1.000, 0, '', '', '', '', '', '', '', 0, '', '', 1.000, 0, '', '', '', '', '', '', 0, '', '', '', '', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `ec_shippingrate`
--

CREATE TABLE IF NOT EXISTS `ec_shippingrate` (
  `shippingrate_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_demo_item` tinyint(1) NOT NULL DEFAULT '0',
  `zone_id` int(11) NOT NULL DEFAULT '0',
  `is_price_based` tinyint(1) NOT NULL DEFAULT '0',
  `is_weight_based` tinyint(1) NOT NULL DEFAULT '0',
  `is_method_based` tinyint(1) NOT NULL DEFAULT '0',
  `is_quantity_based` tinyint(1) NOT NULL DEFAULT '0',
  `is_percentage_based` tinyint(1) NOT NULL DEFAULT '0',
  `is_ups_based` tinyint(1) NOT NULL DEFAULT '0',
  `is_usps_based` tinyint(1) NOT NULL DEFAULT '0',
  `is_fedex_based` tinyint(1) NOT NULL DEFAULT '0',
  `is_auspost_based` tinyint(1) NOT NULL DEFAULT '0',
  `is_dhl_based` tinyint(1) NOT NULL DEFAULT '0',
  `is_canadapost_based` tinyint(1) NOT NULL DEFAULT '0',
  `trigger_rate` float(15,3) NOT NULL DEFAULT '0.000',
  `shipping_rate` float(15,3) NOT NULL DEFAULT '0.000',
  `shipping_label` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_order` int(11) NOT NULL DEFAULT '0',
  `shipping_code` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_override_rate` float(11,3) DEFAULT NULL,
  `free_shipping_at` float(15,3) NOT NULL DEFAULT '-1.000',
  PRIMARY KEY (`shippingrate_id`),
  UNIQUE KEY `shippingrate_id` (`shippingrate_id`),
  KEY `zone_id` (`zone_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=55 ;

--
-- Dumping data for table `ec_shippingrate`
--

INSERT INTO `ec_shippingrate` (`shippingrate_id`, `is_demo_item`, `zone_id`, `is_price_based`, `is_weight_based`, `is_method_based`, `is_quantity_based`, `is_percentage_based`, `is_ups_based`, `is_usps_based`, `is_fedex_based`, `is_auspost_based`, `is_dhl_based`, `is_canadapost_based`, `trigger_rate`, `shipping_rate`, `shipping_label`, `shipping_order`, `shipping_code`, `shipping_override_rate`, `free_shipping_at`) VALUES
(51, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 5.000, '', 0, '', 0.000, -1.000),
(52, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 0.000, 'Standard Shipping 7-10 Days', 1, '', NULL, -1.000),
(53, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 14.990, 'Priority 3 Day Shipping', 2, '', NULL, -1.000),
(54, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 19.990, 'Priority 2 Day Shipping', 3, '', NULL, -1.000);

-- --------------------------------------------------------

--
-- Table structure for table `ec_shipping_class`
--

CREATE TABLE IF NOT EXISTS `ec_shipping_class` (
  `shipping_class_id` int(11) NOT NULL AUTO_INCREMENT,
  `class_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`shipping_class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_shipping_class_to_rate`
--

CREATE TABLE IF NOT EXISTS `ec_shipping_class_to_rate` (
  `shipping_class_to_rate_id` int(11) NOT NULL AUTO_INCREMENT,
  `shipping_class_id` int(11) NOT NULL DEFAULT '0',
  `shipping_rate_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`shipping_class_to_rate_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_state`
--

CREATE TABLE IF NOT EXISTS `ec_state` (
  `id_sta` int(11) NOT NULL AUTO_INCREMENT,
  `idcnt_sta` int(11) NOT NULL DEFAULT '0',
  `code_sta` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `name_sta` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `group_sta` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `ship_to_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_sta`),
  KEY `idcnt_sta` (`idcnt_sta`),
  KEY `code_sta` (`code_sta`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=312 ;

--
-- Dumping data for table `ec_state`
--

INSERT INTO `ec_state` (`id_sta`, `idcnt_sta`, `code_sta`, `name_sta`, `sort_order`, `group_sta`, `ship_to_active`) VALUES
(1, 223, 'AL', 'Alabama', 9, '', 1),
(2, 223, 'AK', 'Alaska', 10, '', 1),
(4, 223, 'AZ', 'Arizona', 11, '', 1),
(5, 223, 'AR', 'Arkansas', 12, '', 1),
(12, 223, 'CA', 'California', 13, '', 1),
(13, 223, 'CO', 'Colorado', 14, '', 1),
(14, 223, 'CT', 'Connecticut', 15, '', 1),
(15, 223, 'DE', 'Delaware', 16, '', 1),
(16, 223, 'DC', 'District of Columbia', 17, '', 1),
(18, 223, 'FL', 'Florida', 18, '', 1),
(19, 223, 'GA', 'Georgia', 19, '', 1),
(21, 223, 'HI', 'Hawaii', 21, '', 1),
(22, 223, 'ID', 'Idaho', 22, '', 1),
(23, 223, 'IL', 'Illinois', 23, '', 1),
(24, 223, 'IN', 'Indiana', 24, '', 1),
(25, 223, 'IA', 'Iowa', 25, '', 1),
(26, 223, 'KS', 'Kansas', 26, '', 1),
(27, 223, 'KY', 'Kentucky', 27, '', 1),
(28, 223, 'LA', 'Louisiana', 28, '', 1),
(29, 223, 'ME', 'Maine', 29, '', 1),
(31, 223, 'MD', 'Maryland', 30, '', 1),
(32, 223, 'MA', 'Massachusetts', 31, '', 1),
(33, 223, 'MI', 'Michigan', 32, '', 1),
(34, 223, 'MN', 'Minnesota', 33, '', 1),
(35, 223, 'MS', 'Mississippi', 34, '', 1),
(36, 223, 'MO', 'Missouri', 35, '', 1),
(37, 223, 'MT', 'Montana', 36, '', 1),
(38, 223, 'NE', 'Nebraska', 37, '', 1),
(39, 223, 'NV', 'Nevada', 38, '', 1),
(40, 223, 'NH', 'New Hampshire', 39, '', 1),
(41, 223, 'NJ', 'New Jersey', 40, '', 1),
(42, 223, 'NM', 'New Mexico', 41, '', 1),
(43, 223, 'NY', 'New York', 42, '', 1),
(44, 223, 'NC', 'North Carolina', 43, '', 1),
(45, 223, 'ND', 'North Dakota', 44, '', 1),
(47, 223, 'OH', 'Ohio', 45, '', 1),
(48, 223, 'OK', 'Oklahoma', 46, '', 1),
(49, 223, 'OR', 'Oregon', 47, '', 1),
(51, 223, 'PA', 'Pennsylvania', 48, '', 1),
(52, 223, 'PR', 'Puerto Rico', 49, '', 1),
(53, 223, 'RI', 'Rhode Island', 50, '', 1),
(54, 223, 'SC', 'South Carolina', 51, '', 1),
(55, 223, 'SD', 'South Dakota', 52, '', 1),
(56, 223, 'TN', 'Tennessee', 53, '', 1),
(57, 223, 'TX', 'Texas', 54, '', 1),
(58, 223, 'UT', 'Utah', 55, '', 1),
(59, 223, 'VT', 'Vermont', 56, '', 1),
(60, 223, 'VI', 'Virgin Islands', 57, '', 1),
(61, 223, 'VA', 'Virginia', 58, '', 1),
(62, 223, 'WA', 'Washington', 59, '', 1),
(63, 223, 'WV', 'West Virginia', 60, '', 1),
(64, 223, 'WI', 'Wisconsin', 61, '', 1),
(65, 223, 'WY', 'Wyoming', 62, '', 1),
(66, 38, 'AB', 'Alberta', 100, '', 1),
(67, 38, 'BC', 'British Columbia', 101, '', 1),
(68, 38, 'MB', 'Manitoba', 102, '', 1),
(69, 38, 'NF', 'Newfoundland', 103, '', 1),
(70, 38, 'NB', 'New Brunswick', 104, '', 1),
(71, 38, 'NS', 'Nova Scotia', 105, '', 1),
(72, 38, 'NT', 'Northwest Territories', 106, '', 1),
(73, 38, 'NU', 'Nunavut', 107, '', 1),
(74, 38, 'ON', 'Ontario', 108, '', 1),
(75, 38, 'PE', 'Prince Edward Island', 109, '', 1),
(76, 38, 'QC', 'Quebec', 110, '', 1),
(77, 38, 'SK', 'Saskatchewan', 111, '', 1),
(78, 38, 'YT', 'Yukon Territory', 112, '', 1),
(79, 13, 'ACT', 'Australian Capital Territory', 113, '', 1),
(80, 13, 'CX', 'Christmas Island', 114, '', 1),
(81, 13, 'CC', 'Cocos Islands', 115, '', 1),
(82, 13, 'HM', 'Heard Island and McDonald Islands', 116, '', 1),
(83, 13, 'NSW', 'New South Wales', 117, '', 1),
(84, 13, 'NF', 'Norfolk Island', 118, '', 1),
(85, 13, 'NT', 'Northern Territory', 119, '', 1),
(86, 13, 'QLD', 'Queensland', 120, '', 1),
(87, 13, 'SA', 'South Australia', 121, '', 1),
(88, 13, 'TAS', 'Tasmania', 122, '', 1),
(89, 13, 'VIC', 'Victoria', 123, '', 1),
(90, 13, 'WA', 'Western Australia', 124, '', 1),
(91, 222, 'Avon', 'Avon', 125, 'England', 1),
(92, 222, 'Bedfordshire', 'Bedfordshire', 126, 'England', 1),
(93, 222, 'Berkshire', 'Berkshire', 127, 'England', 1),
(94, 222, 'Buckinghamshire', 'Buckinghamshire', 128, 'England', 1),
(95, 222, 'Cambridgeshire', 'Cambridgeshire', 129, 'England', 1),
(96, 222, 'Cheshire', 'Cheshire', 130, 'England', 1),
(97, 222, 'Cleveland', 'Cleveland', 131, 'England', 1),
(98, 222, 'Cornwall', 'Cornwall', 132, 'England', 1),
(99, 222, 'Cumbria', 'Cumbria', 133, 'England', 1),
(100, 222, 'Derbyshire', 'Derbyshire', 134, 'England', 1),
(101, 222, 'Devon', 'Devon', 135, 'England', 1),
(102, 222, 'Dorset', 'Dorset', 136, 'England', 1),
(103, 222, 'Durham', 'Durham', 137, 'England', 1),
(104, 222, 'East Sussex', 'East Sussex', 138, 'England', 1),
(105, 222, 'Essex', 'Essex', 139, 'England', 1),
(106, 222, 'Gloucestershire', 'Gloucestershire', 140, 'England', 1),
(107, 222, 'Hampshire', 'Hampshire', 141, 'England', 1),
(108, 222, 'Herefordshire', 'Herefordshire', 142, 'England', 1),
(109, 222, 'Hertfordshire', 'Hertfordshire', 143, 'England', 1),
(110, 222, 'Isle of Wight', 'Isle of Wight', 144, 'England', 1),
(111, 222, 'Kent', 'Kent', 145, 'England', 1),
(112, 222, 'Lancashire', 'Lancashire', 146, 'England', 1),
(113, 222, 'Leicestershire', 'Leicestershire', 147, 'England', 1),
(114, 222, 'Lincolnshire', 'Lincolnshire', 148, 'England', 1),
(115, 222, 'London', 'London', 149, 'England', 1),
(116, 222, 'Merseyside', 'Merseyside', 150, 'England', 1),
(117, 222, 'Middlesex', 'Middlesex', 151, 'England', 1),
(118, 222, 'Norfolk', 'Norfolk', 152, 'England', 1),
(119, 222, 'Northamptonshire', 'Northamptonshire', 153, 'England', 1),
(120, 222, 'Northumberland', 'Northumberland', 154, 'England', 1),
(121, 222, 'North Humberside', 'North Humberside', 155, 'England', 1),
(122, 222, 'North Yorkshire', 'North Yorkshire', 156, 'England', 1),
(123, 222, 'Nottinghamshire', 'Nottinghamshire', 157, 'England', 1),
(124, 222, 'Oxfordshire', 'Oxfordshire', 158, 'England', 1),
(125, 222, 'Rutland', 'Rutland', 159, 'England', 1),
(126, 222, 'Shropshire', 'Shropshire', 160, 'England', 1),
(127, 222, 'Somerset', 'Somerset', 161, 'England', 1),
(128, 222, 'South Humberside', 'South Humberside', 162, 'England', 1),
(129, 222, 'South Yorkshire', 'South Yorkshire', 163, 'England', 1),
(130, 222, 'Staffordshire', 'Staffordshire', 164, 'England', 1),
(131, 222, 'Suffolk', 'Suffolk', 165, 'England', 1),
(132, 222, 'Surrey', 'Surrey', 166, 'England', 1),
(133, 222, 'Tyne and Wear', 'Tyne and Wear', 167, 'England', 1),
(134, 222, 'Warwickshire', 'Warwickshire', 168, 'England', 1),
(135, 222, 'West Midlands', 'West Midlands', 169, 'England', 1),
(136, 222, 'West Sussex', 'West Sussex', 170, 'England', 1),
(137, 222, 'West Yorkshire', 'West Yorkshire', 171, 'England', 1),
(138, 222, 'Wiltshire', 'Wiltshire', 172, 'England', 1),
(139, 222, 'Worcestershire', 'Worcestershire', 173, 'England', 1),
(140, 222, 'Clwyd', 'Clwyd', 174, 'Wales', 1),
(141, 222, 'Dyfed', 'Dyfed', 175, 'Wales', 1),
(142, 222, 'Gwent', 'Gwent', 176, 'Wales', 1),
(143, 222, 'Gwynedd', 'Gwynedd', 177, 'Wales', 1),
(144, 222, 'Mid Glamorgan', 'Mid Glamorgan', 178, 'Wales', 1),
(145, 222, 'Powys', 'Powys', 179, 'Wales', 1),
(146, 222, 'South Glamorgan', 'South Glamorgan', 180, 'Wales', 1),
(147, 222, 'West Glamorgan', 'West Glamorgan', 181, 'Wales', 1),
(148, 222, 'Aberdeenshire', 'Aberdeenshire', 182, 'Scotland', 1),
(149, 222, 'Angus', 'Angus', 183, 'Scotland', 1),
(150, 222, 'Argyll', 'Argyll', 184, 'Scotland', 1),
(151, 222, 'Ayrshire', 'Ayrshire', 185, 'Scotland', 1),
(152, 222, 'Banffshire', 'Banffshire', 186, 'Scotland', 1),
(153, 222, 'Berwickshire', 'Berwickshire', 187, 'Scotland', 1),
(154, 222, 'Bute', 'Bute', 188, 'Scotland', 1),
(155, 222, 'Caithness', 'Caithness', 189, 'Scotland', 1),
(156, 222, 'Clackmannanshire', 'Clackmannanshire', 190, 'Scotland', 1),
(157, 222, 'Dumfriesshire', 'Dumfriesshire', 191, 'Scotland', 1),
(158, 222, 'Dunbartonshire', 'Dunbartonshire', 192, 'Scotland', 1),
(159, 222, 'East Lothian', 'East Lothian', 193, 'Scotland', 1),
(160, 222, 'Fife', 'Fife', 194, 'Scotland', 1),
(161, 222, 'Inverness-shire', 'Inverness-shire', 195, 'Scotland', 1),
(162, 222, 'Kincardineshire', 'Kincardineshire', 196, 'Scotland', 1),
(163, 222, 'Kinross-shire', 'Kinross-shire', 197, 'Scotland', 1),
(164, 222, 'Kirkcudbrightshire', 'Kirkcudbrightshire', 198, 'Scotland', 1),
(165, 222, 'Lanarkshire', 'Lanarkshire', 199, 'Scotland', 1),
(166, 222, 'Midlothian', 'Midlothian', 200, 'Scotland', 1),
(167, 222, 'Moray', 'Moray', 201, 'Scotland', 1),
(168, 222, 'Nairnshire', 'Nairnshire', 202, 'Scotland', 1),
(169, 222, 'Orkney', 'Orkney', 203, 'Scotland', 1),
(170, 222, 'Peeblesshire', 'Peeblesshire', 204, 'Scotland', 1),
(171, 222, 'Perthshire', 'Perthshire', 205, 'Scotland', 1),
(172, 222, 'Renfrewshire', 'Renfrewshire', 206, 'Scotland', 1),
(173, 222, 'Ross-shire', 'Ross-shire', 207, 'Scotland', 1),
(174, 222, 'Roxburghshire', 'Roxburghshire', 208, 'Scotland', 1),
(175, 222, 'Selkirkshire', 'Selkirkshire', 209, 'Scotland', 1),
(176, 222, 'Shetland', 'Shetland', 210, 'Scotland', 1),
(177, 222, 'Stirlingshire', 'Stirlingshire', 211, 'Scotland', 1),
(178, 222, 'Sutherland', 'Sutherland', 212, 'Scotland', 1),
(179, 222, 'West Lothian', 'West Lothian', 213, 'Scotland', 1),
(180, 222, 'Wigtownshire', 'Wigtownshire', 214, 'Scotland', 1),
(181, 222, 'Antrim', 'Antrim', 215, 'Northern Ireland', 1),
(182, 222, 'Down', 'Down', 217, 'Northern Ireland', 1),
(183, 222, 'Armagh', 'Armagh', 216, 'Northern Ireland', 1),
(184, 222, 'Fermanagh', 'Fermanagh', 218, 'Northern Ireland', 1),
(185, 222, 'Londonderry', 'Londonderry', 219, 'Northern Ireland', 1),
(186, 222, 'Tyrone', 'Tyrone', 220, 'Northern Ireland', 1),
(187, 30, 'AL', 'Alagoas', 221, '', 1),
(188, 30, 'AM', 'Amazonas', 222, '', 1),
(189, 30, 'BA', 'Bahia', 223, '', 1),
(190, 30, 'CE', 'Cearà', 224, '', 1),
(191, 30, 'DF', 'Distrito Federal', 225, '', 1),
(192, 30, 'ES', 'Espìrito Santo', 226, '', 1),
(193, 30, 'GO', 'Goias', 227, '', 1),
(194, 30, 'MA', 'Maranhao', 228, '', 1),
(195, 30, 'MT', 'Mato Grosso', 229, '', 1),
(196, 30, 'MS', 'Mato Grosso Do Sul', 230, '', 1),
(197, 30, 'MG', 'Minas Gerais', 231, '', 1),
(198, 30, 'PA', 'Parà', 232, '', 1),
(199, 30, 'PB', 'Paraìba', 233, '', 1),
(200, 30, 'PR', 'Paranà', 234, '', 1),
(201, 30, 'PE', 'Pernambuco', 235, '', 1),
(202, 30, 'PI', 'Piauì', 236, '', 1),
(203, 30, 'RJ', 'Rio de Janeiro', 237, '', 1),
(204, 30, 'RN', 'Rio Grande do Norte', 238, '', 1),
(205, 30, 'RS', 'Dio Grande do Sul', 239, '', 1),
(206, 30, 'RO', 'Rondônia', 240, '', 1),
(207, 30, 'SC', 'Santa Catarina', 241, '', 1),
(208, 30, 'SP', 'Sao Paulo', 242, '', 1),
(209, 30, 'SE', 'Sergipe', 243, '', 1),
(210, 44, 'ANH', 'Anhui', 244, '', 1),
(211, 44, 'BEI', 'Beijing', 245, '', 1),
(212, 44, 'CHO', 'Chongqing', 246, '', 1),
(213, 44, 'FUJ', 'Fujian', 247, '', 1),
(214, 44, 'GAN', 'Gansu', 248, '', 1),
(215, 44, 'GDG', 'Guangdong', 249, '', 1),
(216, 44, 'GXI', 'Guangxi', 250, '', 1),
(217, 44, 'GUI', 'Guizhou', 251, '', 1),
(218, 44, 'HAI', 'Hainan', 252, '', 1),
(219, 44, 'HEB', 'Hebei', 253, '', 1),
(220, 44, 'HEI', 'Heilongjiang', 254, '', 1),
(221, 44, 'HEN', 'Henan', 255, '', 1),
(222, 44, 'HUB', 'Hubei', 256, '', 1),
(223, 44, 'HUN', 'Hunan', 257, '', 1),
(224, 44, 'JSU', 'Jiangsu', 258, '', 1),
(225, 44, 'JXI', 'Jiangxi', 259, '', 1),
(226, 44, 'JIL', 'Jilin', 260, '', 1),
(227, 44, 'LIA', 'Liaoning', 261, '', 1),
(228, 44, 'MON', 'Nei Mongol', 262, '', 1),
(229, 44, 'NIN', 'Ningxia', 263, '', 1),
(230, 44, 'QIN', 'Qinghai', 264, '', 1),
(231, 44, 'SHA', 'Shaanxi', 265, '', 1),
(232, 44, 'SHD', 'Shandong', 266, '', 1),
(233, 44, 'SHH', 'Shanghai', 267, '', 1),
(234, 44, 'SHX', 'Shanxi', 268, '', 1),
(235, 44, 'SIC', 'Sichuan', 269, '', 1),
(236, 44, 'TIA', 'TIanjin', 270, '', 1),
(237, 44, 'XIN', 'Xinjiang', 271, '', 1),
(238, 44, 'XIZ', 'Xizang', 272, '', 1),
(239, 44, 'YUN', 'Yunnan', 273, '', 1),
(240, 44, 'ZHE', 'Zhejiang', 274, '', 1),
(241, 99, 'AND', 'Andhra Pradesh', 275, '', 1),
(242, 99, 'ASS', 'Assam', 276, '', 1),
(243, 99, 'BIH', 'Bihar', 277, '', 1),
(244, 99, 'CHH', 'Chhattisgarh', 278, '', 1),
(245, 99, 'DEL', 'Delhi', 279, '', 1),
(246, 99, 'GOA', 'Goa', 280, '', 1),
(247, 99, 'GUJ', 'Gujarat', 281, '', 1),
(248, 99, 'HAR', 'Haryana', 282, '', 1),
(249, 99, 'HIM', 'Himachal Pradesh', 283, '', 1),
(250, 99, 'JAM', 'Jammu & Kashmir', 284, '', 1),
(251, 99, 'JHA', 'Jharkhand', 285, '', 1),
(252, 99, 'KAR', 'Karnataka', 286, '', 1),
(253, 99, 'KER', 'Kerala', 287, '', 1),
(254, 99, 'MAD', 'Madhya Pradesh', 288, '', 1),
(255, 99, 'MAH', 'Maharashtra', 289, '', 1),
(256, 99, 'MEG', 'Meghalaya', 290, '', 1),
(257, 99, 'ORI', 'Orissa', 291, '', 1),
(258, 99, 'PON', 'Pondicherry', 292, '', 1),
(259, 99, 'PUN', 'Punjab', 293, '', 1),
(260, 99, 'RAJ', 'Rajasthan', 294, '', 1),
(261, 99, 'TAM', 'Tamil Nadu', 295, '', 1),
(262, 99, 'UTT', 'Uttar Pradesh', 296, '', 1),
(263, 99, 'UTR', 'Uttaranchal', 297, '', 1),
(264, 99, 'WES', 'West Bengal', 298, '', 1),
(265, 107, 'AIC', 'Aichi', 299, '', 1),
(266, 107, 'AKT', 'Akita', 300, '', 1),
(267, 107, 'AMR', 'Aomori', 301, '', 1),
(268, 107, 'CHB', 'Chiba', 302, '', 1),
(269, 107, 'EHM', 'Ehime', 303, '', 1),
(270, 107, 'FKI', 'Fukui', 304, '', 1),
(271, 107, 'FKO', 'Fukuoka', 305, '', 1),
(272, 107, 'FSM', 'Fukushima', 306, '', 1),
(273, 107, 'GFU', 'Gifu', 307, '', 1),
(274, 107, 'GUM', 'Gunma', 308, '', 1),
(275, 107, 'HRS', 'Hiroshima', 309, '', 1),
(276, 107, 'HKD', 'Hokkaido', 310, '', 1),
(277, 107, 'HYG', 'Hyogo', 311, '', 1),
(278, 107, 'IBR', 'Ibaraki', 312, '', 1),
(279, 107, 'IKW', 'Ishikawa', 313, '', 1),
(280, 107, 'IWT', 'Iwate', 314, '', 1),
(281, 107, 'KGW', 'Kagawa', 315, '', 1),
(282, 107, 'KGS', 'Kagoshima', 316, '', 1),
(283, 107, 'KNG', 'Kanagawa', 317, '', 1),
(284, 107, 'KCH', 'Kochi', 318, '', 1),
(285, 107, 'KMM', 'Kumamoto', 319, '', 1),
(286, 107, 'KYT', 'Kyoto', 320, '', 1),
(287, 107, 'MIE', 'Mie', 321, '', 1),
(288, 107, 'MYG', 'Miyagi', 322, '', 1),
(289, 107, 'MYZ', 'Miyazaki', 323, '', 1),
(290, 107, 'NGN', 'Nagano', 324, '', 1),
(291, 107, 'NGS', 'Nagasaki', 325, '', 1),
(292, 107, 'NRA', 'Nara', 326, '', 1),
(293, 107, 'NGT', 'Niigata', 327, '', 1),
(294, 107, 'OTA', 'Oita', 328, '', 1),
(295, 107, 'OKY', 'Okayama', 329, '', 1),
(296, 107, 'OKN', 'Okinawa', 330, '', 1),
(297, 107, 'OSK', 'Osaka', 331, '', 1),
(298, 107, 'SAG', 'Saga', 332, '', 1),
(299, 107, 'STM', 'Saitama', 333, '', 1),
(300, 107, 'SHG', 'Shiga', 334, '', 1),
(301, 107, 'SMN', 'Shimane', 335, '', 1),
(302, 107, 'SZK', 'Shizuoka', 336, '', 1),
(303, 107, 'TOC', 'Tochigi', 337, '', 1),
(304, 107, 'TKS', 'Tokushima', 338, '', 1),
(305, 107, 'TKY', 'Tokyo', 335, '', 1),
(306, 107, 'TTR', 'Tottori', 336, '', 1),
(307, 107, 'TYM', 'Toyama', 337, '', 1),
(308, 107, 'WKY', 'Wakayama', 338, '', 1),
(309, 107, 'YGT', 'Yamagata', 339, '', 1),
(310, 107, 'YGC', 'Yamaguchi', 340, '', 1),
(311, 107, 'YNS', 'Yamanashi', 341, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ec_subscriber`
--

CREATE TABLE IF NOT EXISTS `ec_subscriber` (
  `subscriber_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `last_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`subscriber_id`),
  UNIQUE KEY `subscriber_email` (`email`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_subscription`
--

CREATE TABLE IF NOT EXISTS `ec_subscription` (
  `subscription_id` int(11) NOT NULL AUTO_INCREMENT,
  `subscription_type` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'paypal',
  `subscription_status` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Active',
  `title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `email` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `last_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_country` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'US',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `model_number` varchar(510) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `price` double(21,3) NOT NULL DEFAULT '0.000',
  `payment_length` int(11) NOT NULL DEFAULT '1',
  `payment_period` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `payment_duration` int(11) NOT NULL DEFAULT '0',
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_payment_date` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `next_payment_date` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `number_payments_completed` int(11) NOT NULL DEFAULT '1',
  `paypal_txn_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `paypal_txn_type` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `paypal_subscr_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `paypal_username` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `paypal_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `stripe_subscription_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `quantity` int(11) NOT NULL DEFAULT '1',
  `num_failed_payment` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`subscription_id`),
  UNIQUE KEY `subscription_id` (`subscription_id`),
  KEY `user_id` (`user_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_subscription_plan`
--

CREATE TABLE IF NOT EXISTS `ec_subscription_plan` (
  `subscription_plan_id` int(11) NOT NULL AUTO_INCREMENT,
  `plan_title` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `can_downgrade` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`subscription_plan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_taxrate`
--

CREATE TABLE IF NOT EXISTS `ec_taxrate` (
  `taxrate_id` int(11) NOT NULL AUTO_INCREMENT,
  `tax_by_state` tinyint(1) NOT NULL DEFAULT '0',
  `tax_by_country` tinyint(1) NOT NULL DEFAULT '0',
  `tax_by_duty` tinyint(1) NOT NULL DEFAULT '0',
  `tax_by_vat` tinyint(1) NOT NULL DEFAULT '0',
  `tax_by_single_vat` tinyint(1) NOT NULL DEFAULT '0',
  `tax_by_all` tinyint(1) NOT NULL DEFAULT '0',
  `state_rate` float(15,3) NOT NULL DEFAULT '0.000',
  `country_rate` float(15,3) NOT NULL DEFAULT '0.000',
  `duty_rate` float(15,3) NOT NULL DEFAULT '0.000',
  `vat_rate` float(15,3) NOT NULL DEFAULT '0.000',
  `vat_added` tinyint(1) NOT NULL DEFAULT '0',
  `vat_included` tinyint(1) NOT NULL DEFAULT '0',
  `all_rate` float(15,3) NOT NULL DEFAULT '0.000',
  `state_code` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `country_code` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `vat_country_code` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `duty_exempt_country_code` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`taxrate_id`),
  UNIQUE KEY `taxrate_id` (`taxrate_id`),
  KEY `state_code` (`state_code`),
  KEY `country_code` (`country_code`),
  KEY `vat_country_code` (`vat_country_code`),
  KEY `duty_exempt_country_code` (`duty_exempt_country_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_tempcart`
--

CREATE TABLE IF NOT EXISTS `ec_tempcart` (
  `tempcart_id` int(11) NOT NULL AUTO_INCREMENT,
  `session_id` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `quantity` int(11) DEFAULT '0',
  `days` int(11) DEFAULT '0',
  `grid_quantity` int(11) DEFAULT '0',
  `grid_days` int(11) DEFAULT '0',
  `gift_card_message` blob,
  `gift_card_from_name` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `gift_card_to_name` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `optionitem_id_1` int(11) NOT NULL DEFAULT '0',
  `optionitem_id_2` int(11) NOT NULL DEFAULT '0',
  `optionitem_id_3` int(11) NOT NULL DEFAULT '0',
  `optionitem_id_4` int(11) NOT NULL DEFAULT '0',
  `optionitem_id_5` int(11) NOT NULL DEFAULT '0',
  `donation_price` float(15,3) NOT NULL DEFAULT '0.000',
  `last_changed_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_deconetwork` tinyint(1) NOT NULL DEFAULT '0',
  `deconetwork_id` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_product_code` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_options` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_edit_link` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_color_code` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_product_id` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_image_link` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_discount` float(15,3) NOT NULL DEFAULT '0.000',
  `deconetwork_tax` float(15,3) NOT NULL DEFAULT '0.000',
  `deconetwork_total` float(15,3) NOT NULL DEFAULT '0.000',
  `deconetwork_version` int(11) NOT NULL DEFAULT '1',
  `gift_card_email` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `abandoned_cart_email_sent` int(11) NOT NULL DEFAULT '0',
  `hide_from_admin` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tempcart_id`),
  UNIQUE KEY `tempcart_tempcart_id` (`tempcart_id`),
  KEY `tempcart_session_id` (`session_id`),
  KEY `tempcart_product_id` (`product_id`),
  KEY `tempcart_optionitem_id_1` (`optionitem_id_1`),
  KEY `tempcart_optionitem_id_2` (`optionitem_id_2`),
  KEY `tempcart_optionitem_id_3` (`optionitem_id_3`),
  KEY `tempcart_optionitem_id_4` (`optionitem_id_4`),
  KEY `tempcart_optionitem_id_5` (`optionitem_id_5`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=29 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_tempcart_data`
--

CREATE TABLE IF NOT EXISTS `ec_tempcart_data` (
  `tempcart_data_id` int(11) NOT NULL AUTO_INCREMENT,
  `tempcart_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `session_id` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `email` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `username` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `first_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `last_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `coupon_code` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `giftcard` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_first_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_last_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_delivery_date_time` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_pickup_date_time` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_company_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_address_line_1` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_address_line_2` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_city` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_state` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_zip` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_country` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_phone` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_selector` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_first_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_last_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_company_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_address_line_2` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_address_line_1` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_city` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_state` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_zip` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_country` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_phone` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `create_account` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `order_notes` text COLLATE utf8mb4_unicode_520_ci,
  `shipping_method` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `estimate_shipping_zip` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `expedited_shipping` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `estimate_shipping_country` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `is_guest` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `guest_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `subscription_option1` text COLLATE utf8mb4_unicode_520_ci,
  `subscription_option2` text COLLATE utf8mb4_unicode_520_ci,
  `subscription_option3` text COLLATE utf8mb4_unicode_520_ci,
  `subscription_option4` text COLLATE utf8mb4_unicode_520_ci,
  `subscription_option5` text COLLATE utf8mb4_unicode_520_ci,
  `subscription_advanced_option` text COLLATE utf8mb4_unicode_520_ci,
  `subscription_quantity` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `convert_to` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `translate_to` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `taxcloud_tax_amount` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `taxcloud_address_verified` tinyint(1) NOT NULL DEFAULT '0',
  `perpage` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `vat_registration_number` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`tempcart_data_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=94 ;

--
-- Dumping data for table `ec_tempcart_data`
--

INSERT INTO `ec_tempcart_data` (`tempcart_data_id`, `tempcart_time`, `session_id`, `user_id`, `email`, `username`, `first_name`, `last_name`, `coupon_code`, `giftcard`, `billing_first_name`, `billing_last_name`, `billing_delivery_date_time`, `billing_pickup_date_time`, `billing_company_name`, `billing_address_line_1`, `billing_address_line_2`, `billing_city`, `billing_state`, `billing_zip`, `billing_country`, `billing_phone`, `shipping_selector`, `shipping_first_name`, `shipping_last_name`, `shipping_company_name`, `shipping_address_line_2`, `shipping_address_line_1`, `shipping_city`, `shipping_state`, `shipping_zip`, `shipping_country`, `shipping_phone`, `create_account`, `order_notes`, `shipping_method`, `estimate_shipping_zip`, `expedited_shipping`, `estimate_shipping_country`, `is_guest`, `guest_key`, `subscription_option1`, `subscription_option2`, `subscription_option3`, `subscription_option4`, `subscription_option5`, `subscription_advanced_option`, `subscription_quantity`, `convert_to`, `translate_to`, `taxcloud_tax_amount`, `taxcloud_address_verified`, `perpage`, `vat_registration_number`) VALUES
(93, '2018-05-03 12:13:51', 'CMTFZITBJOZNOAKEFBMEONECIUYATQ', '1', 'mdrajibul@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `ec_tempcart_optionitem`
--

CREATE TABLE IF NOT EXISTS `ec_tempcart_optionitem` (
  `tempcart_optionitem_id` int(11) NOT NULL AUTO_INCREMENT,
  `tempcart_id` int(11) NOT NULL DEFAULT '0',
  `option_id` int(11) NOT NULL DEFAULT '0',
  `optionitem_id` int(11) NOT NULL DEFAULT '0',
  `optionitem_value` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `session_id` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_model_number` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`tempcart_optionitem_id`),
  UNIQUE KEY `tempcart_optionitem_id` (`tempcart_optionitem_id`),
  KEY `tempcart_id` (`tempcart_id`),
  KEY `option_id` (`option_id`),
  KEY `optionitem_id` (`optionitem_id`),
  KEY `session_id` (`session_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ec_tempcart_optionitem`
--

INSERT INTO `ec_tempcart_optionitem` (`tempcart_optionitem_id`, `tempcart_id`, `option_id`, `optionitem_id`, `optionitem_value`, `session_id`, `optionitem_model_number`) VALUES
(1, 999999999, 3, 3, 'test', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `ec_timezone`
--

CREATE TABLE IF NOT EXISTS `ec_timezone` (
  `timezone_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `identifier` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`timezone_id`),
  UNIQUE KEY `timezone_id` (`timezone_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=141 ;

--
-- Dumping data for table `ec_timezone`
--

INSERT INTO `ec_timezone` (`timezone_id`, `name`, `identifier`) VALUES
(1, '(GMT-12:00) International Date Line West', 'Pacific/Wake'),
(2, '(GMT-11:00) Midway Island', 'Pacific/Apia'),
(3, '(GMT-11:00) Samoa', 'Pacific/Apia'),
(4, '(GMT-10:00) Hawaii', 'Pacific/Honolulu'),
(5, '(GMT-09:00) Alaska', 'America/Anchorage'),
(6, '(GMT-08:00) Pacific Time (US & Canada) Tijuana', 'America/Los_Angeles'),
(7, '(GMT-07:00) Arizona', 'America/Phoenix'),
(8, '(GMT-07:00) Chihuahua', 'America/Chihuahua'),
(9, '(GMT-07:00) La Paz', 'America/Chihuahua'),
(10, '(GMT-07:00) Mazatlan', 'America/Chihuahua'),
(11, '(GMT-07:00) Mountain Time (US & Canada)', 'America/Denver'),
(12, '(GMT-06:00) Central America', 'America/Managua'),
(13, '(GMT-06:00) Central Time (US & Canada)', 'America/Chicago'),
(14, '(GMT-06:00) Guadalajara', 'America/Mexico_City'),
(15, '(GMT-06:00) Mexico City', 'America/Mexico_City'),
(16, '(GMT-06:00) Monterrey', 'America/Mexico_City'),
(17, '(GMT-06:00) Saskatchewan', 'America/Regina'),
(18, '(GMT-05:00) Bogota', 'America/Bogota'),
(19, '(GMT-05:00) Eastern Time (US & Canada)', 'America/New_York'),
(20, '(GMT-05:00) Indiana (East)', 'America/Indiana/Indianapolis'),
(21, '(GMT-05:00) Lima', 'America/Bogota'),
(22, '(GMT-05:00) Quito', 'America/Bogota'),
(23, '(GMT-04:00) Atlantic Time (Canada)', 'America/Halifax'),
(24, '(GMT-04:00) Caracas', 'America/Caracas'),
(25, '(GMT-04:00) La Paz', 'America/Caracas'),
(26, '(GMT-04:00) Santiago', 'America/Santiago'),
(27, '(GMT-03:30) Newfoundland', 'America/St_Johns'),
(28, '(GMT-03:00) Brasilia', 'America/Sao_Paulo'),
(29, '(GMT-03:00) Buenos Aires', 'America/Argentina/Buenos_Aires'),
(30, '(GMT-03:00) Georgetown', 'America/Argentina/Buenos_Aires'),
(31, '(GMT-03:00) Greenland', 'America/Godthab'),
(32, '(GMT-02:00) Mid-Atlantic', 'America/Noronha'),
(33, '(GMT-01:00) Azores', 'Atlantic/Azores'),
(34, '(GMT-01:00) Cape Verde Is.', 'Atlantic/Cape_Verde'),
(35, '(GMT) Casablanca', 'Africa/Casablanca'),
(36, '(GMT) Edinburgh', 'Europe/London'),
(37, '(GMT) Greenwich Mean Time : Dublin', 'Europe/London'),
(38, '(GMT) Lisbon', 'Europe/London'),
(39, '(GMT) London', 'Europe/London'),
(40, '(GMT) Monrovia', 'Africa/Casablanca'),
(41, '(GMT+01:00) Amsterdam', 'Europe/Berlin'),
(42, '(GMT+01:00) Belgrade', 'Europe/Belgrade'),
(43, '(GMT+01:00) Berlin', 'Europe/Berlin'),
(44, '(GMT+01:00) Bern', 'Europe/Berlin'),
(45, '(GMT+01:00) Bratislava', 'Europe/Belgrade'),
(46, '(GMT+01:00) Brussels', 'Europe/Paris'),
(47, '(GMT+01:00) Budapest', 'Europe/Belgrade'),
(48, '(GMT+01:00) Copenhagen', 'Europe/Paris'),
(49, '(GMT+01:00) Ljubljana', 'Europe/Belgrade'),
(50, '(GMT+01:00) Madrid', 'Europe/Paris'),
(51, '(GMT+01:00) Paris', 'Europe/Paris'),
(52, '(GMT+01:00) Prague', 'Europe/Belgrade'),
(53, '(GMT+01:00) Rome', 'Europe/Berlin'),
(54, '(GMT+01:00) Sarajevo', 'Europe/Sarajevo'),
(55, '(GMT+01:00) Skopje', 'Europe/Sarajevo'),
(56, '(GMT+01:00) Stockholm', 'Europe/Berlin'),
(57, '(GMT+01:00) Vienna', 'Europe/Berlin'),
(58, '(GMT+01:00) Warsaw', 'Europe/Sarajevo'),
(59, '(GMT+01:00) West Central Africa', 'Africa/Lagos'),
(60, '(GMT+01:00) Zagreb', 'Europe/Sarajevo'),
(61, '(GMT+02:00) Athens', 'Europe/Istanbul'),
(62, '(GMT+02:00) Bucharest', 'Europe/Bucharest'),
(63, '(GMT+02:00) Cairo', 'Africa/Cairo'),
(64, '(GMT+02:00) Harare', 'Africa/Johannesburg'),
(65, '(GMT+02:00) Helsinki', 'Europe/Helsinki'),
(66, '(GMT+02:00) Istanbul', 'Europe/Istanbul'),
(67, '(GMT+02:00) Jerusalem', 'Asia/Jerusalem'),
(68, '(GMT+02:00) Kyiv', 'Europe/Helsinki'),
(69, '(GMT+02:00) Minsk', 'Europe/Istanbul'),
(70, '(GMT+02:00) Pretoria', 'Africa/Johannesburg'),
(71, '(GMT+02:00) Riga', 'Europe/Helsinki'),
(72, '(GMT+02:00) Sofia', 'Europe/Helsinki'),
(73, '(GMT+02:00) Tallinn', 'Europe/Helsinki'),
(74, '(GMT+02:00) Vilnius', 'Europe/Helsinki'),
(75, '(GMT+03:00) Baghdad', 'Asia/Baghdad'),
(76, '(GMT+03:00) Kuwait', 'Asia/Riyadh'),
(77, '(GMT+03:00) Moscow', 'Europe/Moscow'),
(78, '(GMT+03:00) Nairobi', 'Africa/Nairobi'),
(79, '(GMT+03:00) Riyadh', 'Asia/Riyadh'),
(80, '(GMT+03:00) St. Petersburg', 'Europe/Moscow'),
(81, '(GMT+03:00) Volgograd', 'Europe/Moscow'),
(82, '(GMT+03:30) Tehran', 'Asia/Tehran'),
(83, '(GMT+04:00) Abu Dhabi', 'Asia/Muscat'),
(84, '(GMT+04:00) Baku', 'Asia/Tbilisi'),
(85, '(GMT+04:00) Muscat', 'Asia/Muscat'),
(86, '(GMT+04:00) Tbilisi', 'Asia/Tbilisi'),
(87, '(GMT+04:00) Yerevan', 'Asia/Tbilisi'),
(88, '(GMT+04:30) Kabul', 'Asia/Kabul'),
(89, '(GMT+05:00) Ekaterinburg', 'Asia/Yekaterinburg'),
(90, '(GMT+05:00) Islamabad', 'Asia/Karachi'),
(91, '(GMT+05:00) Karachi', 'Asia/Karachi'),
(92, '(GMT+05:00) Tashkent', 'Asia/Karachi'),
(93, '(GMT+05:30) Chennai', 'Asia/Calcutta'),
(94, '(GMT+05:30) Kolkata', 'Asia/Calcutta'),
(95, '(GMT+05:30) Mumbai', 'Asia/Calcutta'),
(96, '(GMT+05:30) New Delhi', 'Asia/Calcutta'),
(97, '(GMT+05:45) Kathmandu', 'Asia/Katmandu'),
(98, '(GMT+06:00) Almaty', 'Asia/Novosibirsk'),
(99, '(GMT+06:00) Astana', 'Asia/Dhaka'),
(100, '(GMT+06:00) Dhaka', 'Asia/Dhaka'),
(101, '(GMT+06:00) Novosibirsk', 'Asia/Novosibirsk'),
(102, '(GMT+06:00) Sri Jayawardenepura', 'Asia/Colombo'),
(103, '(GMT+06:30) Rangoon', 'Asia/Rangoon'),
(104, '(GMT+07:00) Bangkok', 'Asia/Bangkok'),
(105, '(GMT+07:00) Hanoi', 'Asia/Bangkok'),
(106, '(GMT+07:00) Jakarta', 'Asia/Bangkok'),
(107, '(GMT+07:00) Krasnoyarsk', 'Asia/Krasnoyarsk'),
(108, '(GMT+08:00) Beijing', 'Asia/Hong_Kong'),
(109, '(GMT+08:00) Chongqing', 'Asia/Hong_Kong'),
(110, '(GMT+08:00) Hong Kong', 'Asia/Hong_Kong'),
(111, '(GMT+08:00) Irkutsk', 'Asia/Irkutsk'),
(112, '(GMT+08:00) Kuala Lumpur', 'Asia/Singapore'),
(113, '(GMT+08:00) Perth', 'Australia/Perth'),
(114, '(GMT+08:00) Singapore', 'Asia/Singapore'),
(115, '(GMT+08:00) Taipei', 'Asia/Taipei'),
(116, '(GMT+08:00) Ulaan Bataar', 'Asia/Irkutsk'),
(117, '(GMT+08:00) Urumqi', 'Asia/Hong_Kong'),
(118, '(GMT+09:00) Osaka', 'Asia/Tokyo'),
(119, '(GMT+09:00) Sapporo', 'Asia/Tokyo'),
(120, '(GMT+09:00) Seoul', 'Asia/Seoul'),
(121, '(GMT+09:00) Tokyo', 'Asia/Tokyo'),
(122, '(GMT+09:00) Yakutsk', 'Asia/Yakutsk'),
(123, '(GMT+09:30) Adelaide', 'Australia/Adelaide'),
(124, '(GMT+09:30) Darwin', 'Australia/Darwin'),
(125, '(GMT+10:00) Brisbane', 'Australia/Brisbane'),
(126, '(GMT+10:00) Canberra', 'Australia/Sydney'),
(127, '(GMT+10:00) Guam', 'Pacific/Guam'),
(128, '(GMT+10:00) Hobart', 'Australia/Hobart'),
(129, '(GMT+10:00) Melbourne', 'Australia/Sydney'),
(130, '(GMT+10:00) Port Moresby', 'Pacific/Guam'),
(131, '(GMT+10:00) Sydney', 'Australia/Sydney'),
(132, '(GMT+10:00) Vladivostok', 'Asia/Vladivostok'),
(133, '(GMT+11:00) Magadan', 'Asia/Magadan'),
(134, '(GMT+11:00) New Caledonia', 'Asia/Magadan'),
(135, '(GMT+11:00) Solomon Is.', 'Asia/Magadan'),
(136, '(GMT+12:00) Auckland', 'Pacific/Auckland'),
(137, '(GMT+12:00) Fiji', 'Pacific/Fiji'),
(138, '(GMT+12:00) Kamchatka', 'Pacific/Fiji'),
(139, '(GMT+12:00) Marshall Is.', 'Pacific/Fiji'),
(140, '(GMT+12:00) Wellington', 'Pacific/Auckland');

-- --------------------------------------------------------

--
-- Table structure for table `ec_user`
--

CREATE TABLE IF NOT EXISTS `ec_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_demo_item` tinyint(1) NOT NULL DEFAULT '0',
  `email` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `list_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `edit_sequence` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `quickbooks_status` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Not Queued',
  `first_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `last_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `default_billing_address_id` int(11) NOT NULL DEFAULT '0',
  `default_shipping_address_id` int(11) NOT NULL DEFAULT '0',
  `user_level` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'shopper',
  `is_subscriber` tinyint(1) NOT NULL DEFAULT '0',
  `realauth_registered` tinyint(1) NOT NULL DEFAULT '0',
  `stripe_customer_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `default_card_type` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `default_card_last4` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `exclude_tax` tinyint(1) NOT NULL DEFAULT '0',
  `exclude_shipping` tinyint(1) NOT NULL DEFAULT '0',
  `user_notes` text COLLATE utf8mb4_unicode_520_ci,
  `vat_registration_number` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_user_id` (`user_id`),
  UNIQUE KEY `user_email` (`email`(191)),
  KEY `user_password` (`password`(191)),
  KEY `user_default_billing_address_id` (`default_billing_address_id`),
  KEY `user_default_shipping_address_id` (`default_shipping_address_id`),
  KEY `user_user_level` (`user_level`(191))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ec_user`
--

INSERT INTO `ec_user` (`user_id`, `is_demo_item`, `email`, `password`, `list_id`, `edit_sequence`, `quickbooks_status`, `first_name`, `last_name`, `default_billing_address_id`, `default_shipping_address_id`, `user_level`, `is_subscriber`, `realauth_registered`, `stripe_customer_id`, `default_card_type`, `default_card_last4`, `exclude_tax`, `exclude_shipping`, `user_notes`, `vat_registration_number`) VALUES
(1, 0, 'mdrajibul@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '', '', 'Not Queued', 'Rajibul', 'Islam', 61, 62, 'shopper', 0, 0, '', '', '', 0, 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `ec_webhook`
--

CREATE TABLE IF NOT EXISTS `ec_webhook` (
  `webhook_id` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `webhook_type` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `webhook_data` blob,
  PRIMARY KEY (`webhook_id`),
  UNIQUE KEY `webhook_id` (`webhook_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ec_zone`
--

CREATE TABLE IF NOT EXISTS `ec_zone` (
  `zone_id` int(11) NOT NULL AUTO_INCREMENT,
  `zone_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`zone_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `ec_zone`
--

INSERT INTO `ec_zone` (`zone_id`, `zone_name`) VALUES
(1, 'North America'),
(2, 'South America'),
(3, 'Europe'),
(4, 'Africa'),
(5, 'Asia'),
(6, 'Australia'),
(7, 'Oceania'),
(8, 'Lower 48 States'),
(9, 'Alaska and Hawaii');

-- --------------------------------------------------------

--
-- Table structure for table `ec_zone_to_location`
--

CREATE TABLE IF NOT EXISTS `ec_zone_to_location` (
  `zone_to_location_id` int(11) NOT NULL AUTO_INCREMENT,
  `zone_id` int(11) NOT NULL DEFAULT '0',
  `iso2_cnt` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `code_sta` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`zone_to_location_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=259 ;

--
-- Dumping data for table `ec_zone_to_location`
--

INSERT INTO `ec_zone_to_location` (`zone_to_location_id`, `zone_id`, `iso2_cnt`, `code_sta`) VALUES
(1, 1, 'AI', ''),
(2, 1, 'AQ', ''),
(3, 1, 'AW', ''),
(4, 1, 'BS', ''),
(5, 1, 'BB', ''),
(6, 1, 'BM', ''),
(7, 1, 'BZ', ''),
(8, 1, 'CA', ''),
(9, 1, 'KY', ''),
(10, 1, 'CR', ''),
(11, 1, 'CU', ''),
(12, 1, 'DM', ''),
(13, 1, 'DO', ''),
(14, 1, 'SV', ''),
(15, 1, 'GL', ''),
(16, 1, 'GD', ''),
(17, 1, 'GP', ''),
(18, 1, 'GT', ''),
(19, 1, 'HT', ''),
(20, 1, 'HN', ''),
(21, 1, 'JM', ''),
(22, 1, 'MQ', ''),
(23, 1, 'MX', ''),
(24, 1, 'MS', ''),
(25, 1, 'NI', ''),
(26, 1, 'PA', ''),
(27, 1, 'PR', ''),
(28, 1, 'KN', ''),
(29, 1, 'LC', ''),
(30, 1, 'TT', ''),
(31, 1, 'TC', ''),
(32, 1, 'US', ''),
(33, 1, 'VI', ''),
(34, 2, 'AR', ''),
(35, 2, 'BO', ''),
(36, 2, 'BR', ''),
(37, 2, 'CL', ''),
(38, 2, 'CO', ''),
(39, 2, 'EC', ''),
(40, 2, 'GF', ''),
(41, 2, 'GY', ''),
(42, 2, 'PY', ''),
(43, 2, 'PE', ''),
(44, 2, 'SR', ''),
(45, 2, 'UY', ''),
(46, 2, 'VE', ''),
(47, 6, 'AU', ''),
(48, 7, 'AS', ''),
(49, 7, 'AU', ''),
(50, 7, 'CK', ''),
(51, 7, 'FJ', ''),
(52, 7, 'PF', ''),
(53, 7, 'GU', ''),
(54, 7, 'KI', ''),
(55, 7, 'MH', ''),
(56, 7, 'NR', ''),
(57, 7, 'NC', ''),
(58, 7, 'NZ', ''),
(59, 7, 'NU', ''),
(60, 7, 'NF', ''),
(61, 7, 'PW', ''),
(62, 7, 'PG', ''),
(63, 7, 'PN', ''),
(64, 7, 'WS', ''),
(65, 7, 'SB', ''),
(66, 7, 'TK', ''),
(67, 7, 'TO', ''),
(68, 7, 'TV', ''),
(69, 7, 'VU', ''),
(70, 7, 'WF', ''),
(71, 3, 'AL', ''),
(72, 3, 'AD', ''),
(73, 3, 'AT', ''),
(74, 3, 'BY', ''),
(75, 3, 'BE', ''),
(76, 3, 'BG', ''),
(77, 3, 'HR', ''),
(78, 3, 'CZ', ''),
(79, 3, 'DK', ''),
(80, 3, 'EE', ''),
(81, 3, 'FO', ''),
(82, 3, 'FI', ''),
(83, 3, 'FR', ''),
(84, 3, 'DE', ''),
(85, 3, 'GI', ''),
(86, 3, 'GR', ''),
(87, 3, 'HU', ''),
(88, 3, 'IS', ''),
(89, 3, 'IE', ''),
(90, 3, 'IT', ''),
(91, 3, 'LV', ''),
(92, 3, 'LI', ''),
(93, 3, 'LT', ''),
(94, 3, 'LU', ''),
(95, 3, 'MT', ''),
(96, 3, 'MC', ''),
(97, 3, 'NL', ''),
(98, 3, 'NO', ''),
(99, 3, 'PL', ''),
(100, 3, 'PT', ''),
(101, 3, 'RO', ''),
(102, 3, 'RU', ''),
(103, 3, 'SM', ''),
(104, 3, 'SI', ''),
(105, 3, 'ES', ''),
(106, 3, 'SE', ''),
(107, 3, 'CH', ''),
(108, 3, 'UA', ''),
(109, 3, 'GB', ''),
(110, 4, 'DZ', ''),
(111, 4, 'AO', ''),
(112, 4, 'BJ', ''),
(113, 4, 'BW', ''),
(114, 4, 'BF', ''),
(115, 4, 'BI', ''),
(116, 4, 'CM', ''),
(117, 4, 'CV', ''),
(118, 4, 'TD', ''),
(119, 4, 'KM', ''),
(120, 4, 'CG', ''),
(121, 4, 'CI', ''),
(122, 4, 'DJ', ''),
(123, 4, 'EG', ''),
(124, 4, 'GQ', ''),
(125, 4, 'ER', ''),
(126, 4, 'ET', ''),
(127, 4, 'GA', ''),
(128, 4, 'GM', ''),
(129, 4, 'GH', ''),
(130, 4, 'GN', ''),
(131, 4, 'GW', ''),
(132, 4, 'KE', ''),
(133, 4, 'LS', ''),
(134, 4, 'LR', ''),
(135, 4, 'MG', ''),
(136, 4, 'MW', ''),
(137, 4, 'ML', ''),
(138, 4, 'MR', ''),
(139, 4, 'MU', ''),
(140, 4, 'YT', ''),
(141, 4, 'MA', ''),
(142, 4, 'MZ', ''),
(143, 4, 'NA', ''),
(144, 4, 'NE', ''),
(145, 4, 'NG', ''),
(146, 4, 'RE', ''),
(147, 4, 'RW', ''),
(148, 4, 'ST', ''),
(149, 4, 'SN', ''),
(150, 4, 'SC', ''),
(151, 4, 'SL', ''),
(152, 4, 'SO', ''),
(153, 4, 'ZA', ''),
(154, 4, 'SD', ''),
(155, 4, 'SZ', ''),
(156, 4, 'TG', ''),
(157, 4, 'TN', ''),
(158, 4, 'UG', ''),
(159, 4, 'ZM', ''),
(160, 4, 'ZW', ''),
(161, 5, 'AF', ''),
(162, 5, 'AM', ''),
(163, 5, 'AZ', ''),
(164, 5, 'BH', ''),
(165, 5, 'BD', ''),
(166, 5, 'BT', ''),
(167, 5, 'BN', ''),
(168, 5, 'KH', ''),
(169, 5, 'CN', ''),
(170, 5, 'CX', ''),
(171, 5, 'CY', ''),
(172, 5, 'TP', ''),
(173, 5, 'GE', ''),
(174, 5, 'HK', ''),
(175, 5, 'IN', ''),
(176, 5, 'ID', ''),
(177, 5, 'IQ', ''),
(178, 5, 'IL', ''),
(179, 5, 'JP', ''),
(180, 5, 'JO', ''),
(181, 5, 'KZ', ''),
(182, 5, 'KW', ''),
(183, 5, 'KG', ''),
(184, 5, 'LB', ''),
(185, 5, 'MO', ''),
(186, 5, 'MY', ''),
(187, 5, 'MV', ''),
(188, 5, 'MN', ''),
(189, 5, 'MM', ''),
(190, 5, 'NP', ''),
(191, 5, 'OM', ''),
(192, 5, 'PK', ''),
(193, 5, 'PH', ''),
(194, 5, 'QA', ''),
(195, 5, 'SA', ''),
(196, 5, 'SG', ''),
(197, 5, 'LK', ''),
(198, 5, 'TW', ''),
(199, 5, 'TJ', ''),
(200, 5, 'TH', ''),
(201, 5, 'TR', ''),
(202, 5, 'TM', ''),
(203, 5, 'AE', ''),
(204, 5, 'UZ', ''),
(205, 5, 'VN', ''),
(206, 5, 'YE', ''),
(207, 9, 'US', 'HI'),
(208, 9, 'US', 'AK'),
(209, 8, 'US', 'AL'),
(210, 8, 'US', 'AZ'),
(211, 8, 'US', 'AR'),
(212, 8, 'US', 'CA'),
(213, 8, 'US', 'CO'),
(214, 8, 'US', 'CT'),
(215, 8, 'US', 'DE'),
(216, 8, 'US', 'FL'),
(217, 8, 'US', 'GA'),
(218, 8, 'US', 'ID'),
(219, 8, 'US', 'IL'),
(220, 8, 'US', 'IN'),
(221, 8, 'US', 'IA'),
(222, 8, 'US', 'KS'),
(223, 8, 'US', 'KY'),
(224, 8, 'US', 'LA'),
(225, 8, 'US', 'ME'),
(226, 8, 'US', 'MD'),
(227, 8, 'US', 'MA'),
(228, 8, 'US', 'MI'),
(229, 8, 'US', 'MN'),
(230, 8, 'US', 'MS'),
(231, 8, 'US', 'MO'),
(232, 8, 'US', 'MT'),
(233, 8, 'US', 'NE'),
(234, 8, 'US', 'NV'),
(235, 8, 'US', 'NH'),
(236, 8, 'US', 'NJ'),
(237, 8, 'US', 'NM'),
(238, 8, 'US', 'NY'),
(239, 8, 'US', 'NC'),
(240, 8, 'US', 'ND'),
(241, 8, 'US', 'OH'),
(242, 8, 'US', 'OK'),
(243, 8, 'US', 'OR'),
(244, 8, 'US', 'PA'),
(245, 8, 'US', 'RI'),
(246, 8, 'US', 'SC'),
(247, 8, 'US', 'SD'),
(248, 8, 'US', 'TN'),
(249, 8, 'US', 'TX'),
(250, 8, 'US', 'UT'),
(251, 8, 'US', 'VT'),
(252, 8, 'US', 'VA'),
(253, 8, 'US', 'WA'),
(254, 8, 'US', 'WV'),
(255, 8, 'US', 'WI'),
(256, 8, 'US', 'WY'),
(257, 3, 'DC', ''),
(258, 8, 'US', 'DC');

-- --------------------------------------------------------

--
-- Table structure for table `ops_commentmeta`
--

CREATE TABLE IF NOT EXISTS `ops_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ops_comments`
--

CREATE TABLE IF NOT EXISTS `ops_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ops_comments`
--

INSERT INTO `ops_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2018-04-30 06:23:21', '2018-04-30 06:23:21', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href="https://gravatar.com">Gravatar</a>.', 0, 'post-trashed', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ops_db7_forms`
--

CREATE TABLE IF NOT EXISTS `ops_db7_forms` (
  `form_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `form_post_id` bigint(20) NOT NULL,
  `form_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `form_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`form_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ops_db7_forms`
--

INSERT INTO `ops_db7_forms` (`form_id`, `form_post_id`, `form_value`, `form_date`) VALUES
(1, 98, 'a:5:{s:12:"cfdb7_status";s:4:"read";s:10:"first-name";s:8:"Abdullah";s:5:"email";s:21:"mamunbdaiub@gmail.com";s:7:"subject";s:12:"Test Subject";s:7:"message";s:12:"Test Message";}', '2018-05-03 12:43:29');

-- --------------------------------------------------------

--
-- Table structure for table `ops_fed_menu`
--

CREATE TABLE IF NOT EXISTS `ops_fed_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `menu_slug` char(32) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `menu` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `menu_order` bigint(20) NOT NULL,
  `menu_image_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `show_user_profile` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `extra` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes',
  `user_role` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `extended` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menu_slug` (`menu_slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ops_fed_menu`
--

INSERT INTO `ops_fed_menu` (`id`, `menu_slug`, `menu`, `menu_order`, `menu_image_id`, `show_user_profile`, `extra`, `user_role`, `extended`) VALUES
(1, 'profile', 'Profile', 3, 'fa fa-user', 'Enable', 'no', 'a:2:{i:0;s:6:"author";i:1;s:11:"testimonial";}', '');

-- --------------------------------------------------------

--
-- Table structure for table `ops_fed_post`
--

CREATE TABLE IF NOT EXISTS `ops_fed_post` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `input_meta` char(32) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `label_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `input_order` bigint(20) NOT NULL,
  `is_required` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'false',
  `input_type` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `placeholder` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `class_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `id_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `input_step` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `input_min` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `input_max` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `input_row` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `is_tooltip` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'no',
  `tooltip_title` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `tooltip_body` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_type` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `input_value` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_role` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `input_location` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `extended` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `input_meta` (`input_meta`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ops_fed_user_profile`
--

CREATE TABLE IF NOT EXISTS `ops_fed_user_profile` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `input_meta` char(32) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `label_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `input_order` bigint(20) NOT NULL,
  `show_register` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Disable',
  `show_dashboard` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Disable',
  `show_user_profile` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Enable',
  `is_required` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'false',
  `input_type` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `placeholder` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `class_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `id_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `input_step` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `input_min` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `input_max` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `input_row` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `is_tooltip` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'no',
  `tooltip_title` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `tooltip_body` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `input_value` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_role` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `input_location` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `extra` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes',
  `menu` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'profile',
  `extended` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `input_meta` (`input_meta`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=12 ;

--
-- Dumping data for table `ops_fed_user_profile`
--

INSERT INTO `ops_fed_user_profile` (`id`, `input_meta`, `label_name`, `input_order`, `show_register`, `show_dashboard`, `show_user_profile`, `is_required`, `input_type`, `placeholder`, `class_name`, `id_name`, `input_step`, `input_min`, `input_max`, `input_row`, `is_tooltip`, `tooltip_title`, `tooltip_body`, `input_value`, `user_role`, `input_location`, `extra`, `menu`, `extended`) VALUES
(1, 'user_login', 'User Name', 5, 'Enable', 'Enable', 'Disable', 'true', 'single_line', 'User Name', '', '', '', '', '', '', 'no', '', '', '', 'a:6:{i:0;s:13:"administrator";i:1;s:6:"editor";i:2;s:6:"author";i:3;s:11:"contributor";i:4;s:10:"subscriber";i:5;s:11:"testimonial";}', '', 'no', 'profile', ''),
(2, 'user_pass', 'Password', 9, 'Enable', 'Enable', 'Disable', 'true', 'password', 'Password', '', '', '', '', '', '', 'no', '', '', '', 'a:6:{i:0;s:13:"administrator";i:1;s:6:"editor";i:2;s:6:"author";i:3;s:11:"contributor";i:4;s:10:"subscriber";i:5;s:11:"testimonial";}', '', 'no', 'profile', ''),
(3, 'confirmation_password', 'Confirmation Password', 11, 'Enable', 'Enable', 'Disable', 'true', 'password', 'Password', '', '', '', '', '', '', 'no', '', '', '', 'a:6:{i:0;s:13:"administrator";i:1;s:6:"editor";i:2;s:6:"author";i:3;s:11:"contributor";i:4;s:10:"subscriber";i:5;s:11:"testimonial";}', '', 'no', 'profile', ''),
(4, 'user_email', 'Email Address', 12, 'Enable', 'Enable', 'Disable', 'true', 'email', 'Email Address', '', '', '', '', '', '', 'no', '', '', '', 'a:6:{i:0;s:13:"administrator";i:1;s:6:"editor";i:2;s:6:"author";i:3;s:11:"contributor";i:4;s:10:"subscriber";i:5;s:11:"testimonial";}', '', 'no', 'profile', ''),
(5, 'user_nicename', 'Nicename', 14, 'Disable', 'Disable', 'Enable', 'false', 'single_line', 'Nicename', '', '', '', '', '', '', 'no', '', '', '', 'a:6:{i:0;s:13:"administrator";i:1;s:6:"editor";i:2;s:6:"author";i:3;s:11:"contributor";i:4;s:10:"subscriber";i:5;s:11:"testimonial";}', '', 'no', 'profile', ''),
(6, 'display_name', 'Display Name', 17, 'Disable', 'Disable', 'Enable', 'false', 'single_line', 'Display Name', '', '', '', '', '', '', 'no', '', '', '', 'a:6:{i:0;s:13:"administrator";i:1;s:6:"editor";i:2;s:6:"author";i:3;s:11:"contributor";i:4;s:10:"subscriber";i:5;s:11:"testimonial";}', '', 'no', 'profile', ''),
(7, 'first_name', 'First Name', 20, 'Disable', 'Disable', 'Enable', 'false', 'single_line', 'First Name', '', '', '', '', '', '', 'no', '', '', '', 'a:6:{i:0;s:13:"administrator";i:1;s:6:"editor";i:2;s:6:"author";i:3;s:11:"contributor";i:4;s:10:"subscriber";i:5;s:11:"testimonial";}', '', 'no', 'profile', ''),
(8, 'last_name', 'Last Name', 23, 'Disable', 'Disable', 'Enable', 'false', 'single_line', 'Last Name', '', '', '', '', '', '', 'no', '', '', '', 'a:6:{i:0;s:13:"administrator";i:1;s:6:"editor";i:2;s:6:"author";i:3;s:11:"contributor";i:4;s:10:"subscriber";i:5;s:11:"testimonial";}', '', 'no', 'profile', ''),
(9, 'nickname', 'Nickname', 17, 'Disable', 'Disable', 'Enable', 'false', 'single_line', 'Nickname', '', '', '', '', '', '', 'no', '', '', '', 'a:6:{i:0;s:13:"administrator";i:1;s:6:"editor";i:2;s:6:"author";i:3;s:11:"contributor";i:4;s:10:"subscriber";i:5;s:11:"testimonial";}', '', 'no', 'profile', ''),
(10, 'description', 'Description', 17, 'Disable', 'Disable', 'Enable', 'false', 'multi_line', 'Description', '', '', '', '', '', '', 'no', '', '', '', 'a:6:{i:0;s:13:"administrator";i:1;s:6:"editor";i:2;s:6:"author";i:3;s:11:"contributor";i:4;s:10:"subscriber";i:5;s:11:"testimonial";}', '', 'no', 'profile', ''),
(11, 'show_admin_bar_front', 'Show admin bar on front page', 17, 'Disable', 'Disable', 'Disable', 'false', 'checkbox', '', '', '', '', '', '', '', 'no', '', '', '', 'a:6:{i:0;s:13:"administrator";i:1;s:6:"editor";i:2;s:6:"author";i:3;s:11:"contributor";i:4;s:10:"subscriber";i:5;s:11:"testimonial";}', '', 'no', 'profile', '');

-- --------------------------------------------------------

--
-- Table structure for table `ops_links`
--

CREATE TABLE IF NOT EXISTS `ops_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ops_options`
--

CREATE TABLE IF NOT EXISTS `ops_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=832 ;

--
-- Dumping data for table `ops_options`
--

INSERT INTO `ops_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/oz_production/public_html', 'yes'),
(2, 'home', 'http://localhost/oz_production/public_html', 'yes'),
(3, 'blogname', 'Oz Production', 'yes'),
(4, 'blogdescription', 'Oz Production Event Services, LLC', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'info@ozproductionservices.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:163:{s:11:"^wp-json/?$";s:22:"index.php?rest_route=/";s:14:"^wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:21:"^index.php/wp-json/?$";s:22:"index.php?rest_route=/";s:24:"^index.php/wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:17:"^store/([^/]*)/?$";s:30:"index.php?ec_store=$matches[1]";s:10:"banners/?$";s:26:"index.php?post_type=banner";s:40:"banners/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?post_type=banner&feed=$matches[1]";s:35:"banners/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?post_type=banner&feed=$matches[1]";s:27:"banners/page/([0-9]{1,})/?$";s:44:"index.php?post_type=banner&paged=$matches[1]";s:15:"testimonials/?$";s:31:"index.php?post_type=testimonial";s:45:"testimonials/feed/(feed|rdf|rss|rss2|atom)/?$";s:48:"index.php?post_type=testimonial&feed=$matches[1]";s:40:"testimonials/(feed|rdf|rss|rss2|atom)/?$";s:48:"index.php?post_type=testimonial&feed=$matches[1]";s:32:"testimonials/page/([0-9]{1,})/?$";s:49:"index.php?post_type=testimonial&paged=$matches[1]";s:47:"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:42:"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:23:"category/(.+?)/embed/?$";s:46:"index.php?category_name=$matches[1]&embed=true";s:35:"category/(.+?)/page/?([0-9]{1,})/?$";s:53:"index.php?category_name=$matches[1]&paged=$matches[2]";s:17:"category/(.+?)/?$";s:35:"index.php?category_name=$matches[1]";s:44:"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:39:"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:20:"tag/([^/]+)/embed/?$";s:36:"index.php?tag=$matches[1]&embed=true";s:32:"tag/([^/]+)/page/?([0-9]{1,})/?$";s:43:"index.php?tag=$matches[1]&paged=$matches[2]";s:14:"tag/([^/]+)/?$";s:25:"index.php?tag=$matches[1]";s:45:"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:40:"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:21:"type/([^/]+)/embed/?$";s:44:"index.php?post_format=$matches[1]&embed=true";s:33:"type/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?post_format=$matches[1]&paged=$matches[2]";s:15:"type/([^/]+)/?$";s:33:"index.php?post_format=$matches[1]";s:50:"sc-banner/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:48:"index.php?sc-banner=$matches[1]&feed=$matches[2]";s:45:"sc-banner/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:48:"index.php?sc-banner=$matches[1]&feed=$matches[2]";s:26:"sc-banner/([^/]+)/embed/?$";s:42:"index.php?sc-banner=$matches[1]&embed=true";s:38:"sc-banner/([^/]+)/page/?([0-9]{1,})/?$";s:49:"index.php?sc-banner=$matches[1]&paged=$matches[2]";s:20:"sc-banner/([^/]+)/?$";s:31:"index.php?sc-banner=$matches[1]";s:55:"sc-testimonial/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:53:"index.php?sc-testimonial=$matches[1]&feed=$matches[2]";s:50:"sc-testimonial/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:53:"index.php?sc-testimonial=$matches[1]&feed=$matches[2]";s:31:"sc-testimonial/([^/]+)/embed/?$";s:47:"index.php?sc-testimonial=$matches[1]&embed=true";s:43:"sc-testimonial/([^/]+)/page/?([0-9]{1,})/?$";s:54:"index.php?sc-testimonial=$matches[1]&paged=$matches[2]";s:25:"sc-testimonial/([^/]+)/?$";s:36:"index.php?sc-testimonial=$matches[1]";s:33:"store/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:43:"store/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:63:"store/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:58:"store/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:58:"store/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:39:"store/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:22:"store/([^/]+)/embed/?$";s:41:"index.php?ec_store=$matches[1]&embed=true";s:26:"store/([^/]+)/trackback/?$";s:35:"index.php?ec_store=$matches[1]&tb=1";s:46:"store/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?ec_store=$matches[1]&feed=$matches[2]";s:41:"store/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?ec_store=$matches[1]&feed=$matches[2]";s:34:"store/([^/]+)/page/?([0-9]{1,})/?$";s:48:"index.php?ec_store=$matches[1]&paged=$matches[2]";s:41:"store/([^/]+)/comment-page-([0-9]{1,})/?$";s:48:"index.php?ec_store=$matches[1]&cpage=$matches[2]";s:30:"store/([^/]+)(?:/([0-9]+))?/?$";s:47:"index.php?ec_store=$matches[1]&page=$matches[2]";s:22:"store/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:32:"store/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:52:"store/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:47:"store/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:47:"store/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:28:"store/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:35:"banners/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:45:"banners/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:65:"banners/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:60:"banners/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:60:"banners/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:41:"banners/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:24:"banners/([^/]+)/embed/?$";s:39:"index.php?banner=$matches[1]&embed=true";s:28:"banners/([^/]+)/trackback/?$";s:33:"index.php?banner=$matches[1]&tb=1";s:48:"banners/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:45:"index.php?banner=$matches[1]&feed=$matches[2]";s:43:"banners/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:45:"index.php?banner=$matches[1]&feed=$matches[2]";s:36:"banners/([^/]+)/page/?([0-9]{1,})/?$";s:46:"index.php?banner=$matches[1]&paged=$matches[2]";s:43:"banners/([^/]+)/comment-page-([0-9]{1,})/?$";s:46:"index.php?banner=$matches[1]&cpage=$matches[2]";s:32:"banners/([^/]+)(?:/([0-9]+))?/?$";s:45:"index.php?banner=$matches[1]&page=$matches[2]";s:24:"banners/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:34:"banners/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:54:"banners/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:49:"banners/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:49:"banners/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:30:"banners/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:40:"testimonials/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:50:"testimonials/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:70:"testimonials/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:65:"testimonials/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:65:"testimonials/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:46:"testimonials/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:29:"testimonials/([^/]+)/embed/?$";s:44:"index.php?testimonial=$matches[1]&embed=true";s:33:"testimonials/([^/]+)/trackback/?$";s:38:"index.php?testimonial=$matches[1]&tb=1";s:53:"testimonials/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?testimonial=$matches[1]&feed=$matches[2]";s:48:"testimonials/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?testimonial=$matches[1]&feed=$matches[2]";s:41:"testimonials/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?testimonial=$matches[1]&paged=$matches[2]";s:48:"testimonials/([^/]+)/comment-page-([0-9]{1,})/?$";s:51:"index.php?testimonial=$matches[1]&cpage=$matches[2]";s:37:"testimonials/([^/]+)(?:/([0-9]+))?/?$";s:50:"index.php?testimonial=$matches[1]&page=$matches[2]";s:29:"testimonials/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:39:"testimonials/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:59:"testimonials/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:54:"testimonials/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:54:"testimonials/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:35:"testimonials/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:48:".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$";s:18:"index.php?feed=old";s:20:".*wp-app\\.php(/.*)?$";s:19:"index.php?error=403";s:18:".*wp-register.php$";s:23:"index.php?register=true";s:32:"feed/(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:27:"(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:8:"embed/?$";s:21:"index.php?&embed=true";s:20:"page/?([0-9]{1,})/?$";s:28:"index.php?&paged=$matches[1]";s:27:"comment-page-([0-9]{1,})/?$";s:39:"index.php?&page_id=11&cpage=$matches[1]";s:41:"comments/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:36:"comments/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:17:"comments/embed/?$";s:21:"index.php?&embed=true";s:44:"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:39:"search/(.+)/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:20:"search/(.+)/embed/?$";s:34:"index.php?s=$matches[1]&embed=true";s:32:"search/(.+)/page/?([0-9]{1,})/?$";s:41:"index.php?s=$matches[1]&paged=$matches[2]";s:14:"search/(.+)/?$";s:23:"index.php?s=$matches[1]";s:47:"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:42:"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:23:"author/([^/]+)/embed/?$";s:44:"index.php?author_name=$matches[1]&embed=true";s:35:"author/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?author_name=$matches[1]&paged=$matches[2]";s:17:"author/([^/]+)/?$";s:33:"index.php?author_name=$matches[1]";s:69:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:45:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$";s:74:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]";s:39:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$";s:63:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]";s:56:"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:51:"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:32:"([0-9]{4})/([0-9]{1,2})/embed/?$";s:58:"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true";s:44:"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]";s:26:"([0-9]{4})/([0-9]{1,2})/?$";s:47:"index.php?year=$matches[1]&monthnum=$matches[2]";s:43:"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:38:"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:19:"([0-9]{4})/embed/?$";s:37:"index.php?year=$matches[1]&embed=true";s:31:"([0-9]{4})/page/?([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&paged=$matches[2]";s:13:"([0-9]{4})/?$";s:26:"index.php?year=$matches[1]";s:27:".?.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:".?.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:".?.+?/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"(.?.+?)/embed/?$";s:41:"index.php?pagename=$matches[1]&embed=true";s:20:"(.?.+?)/trackback/?$";s:35:"index.php?pagename=$matches[1]&tb=1";s:40:"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:35:"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:28:"(.?.+?)/page/?([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&paged=$matches[2]";s:35:"(.?.+?)/comment-page-([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&cpage=$matches[2]";s:24:"(.?.+?)(?:/([0-9]+))?/?$";s:47:"index.php?pagename=$matches[1]&page=$matches[2]";s:27:"[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:"[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:"[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"([^/]+)/embed/?$";s:37:"index.php?name=$matches[1]&embed=true";s:20:"([^/]+)/trackback/?$";s:31:"index.php?name=$matches[1]&tb=1";s:40:"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:35:"([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:28:"([^/]+)/page/?([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&paged=$matches[2]";s:35:"([^/]+)/comment-page-([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&cpage=$matches[2]";s:24:"([^/]+)(?:/([0-9]+))?/?$";s:43:"index.php?name=$matches[1]&page=$matches[2]";s:16:"[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:26:"[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:46:"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:22:"[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:6:{i:0;s:30:"advanced-custom-fields/acf.php";i:1;s:43:"all-in-one-seo-pack/all_in_one_seo_pack.php";i:2;s:67:"contact-form-7-simple-recaptcha/contact-form-7-simple-recaptcha.php";i:3;s:36:"contact-form-7/wp-contact-form-7.php";i:4;s:42:"contact-form-cfdb7/contact-form-cfdb-7.php";i:5;s:26:"wp-easycart/wpeasycart.php";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'ozproduction', 'yes'),
(41, 'stylesheet', 'ozproduction', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'author', 'yes'),
(48, 'db_version', '38590', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:"title";s:0:"";s:5:"count";i:0;s:12:"hierarchical";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:1:{s:26:"wp-easycart/wpeasycart.php";s:12:"ec_uninstall";}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '11', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'initial_db_version', '38590', 'yes'),
(92, 'ops_user_roles', 'a:6:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:62:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;s:16:"aiosp_manage_seo";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:33:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:9:{s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:5:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:2:{s:4:"read";b:1;s:7:"level_0";b:1;}}s:11:"testimonial";a:2:{s:4:"name";s:11:"Testimonial";s:12:"capabilities";a:3:{s:4:"read";b:1;s:7:"level_0";b:1;s:12:"upload_files";b:1;}}}', 'yes'),
(93, 'fresh_site', '0', 'yes'),
(94, 'widget_search', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(95, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(96, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(97, 'widget_archives', 'a:2:{i:2;a:3:{s:5:"title";s:0:"";s:5:"count";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(98, 'widget_meta', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(99, 'sidebars_widgets', 'a:4:{s:19:"wp_inactive_widgets";a:0:{}s:19:"primary-widget-area";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:21:"secondary-widget-area";a:0:{}s:13:"array_version";i:3;}', 'yes'),
(100, 'widget_pages', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(101, 'widget_calendar', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(102, 'widget_media_audio', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(103, 'widget_media_image', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(104, 'widget_media_gallery', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(105, 'widget_media_video', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(106, 'widget_tag_cloud', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(107, 'widget_nav_menu', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(108, 'widget_custom_html', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(109, 'cron', 'a:4:{i:1525875902;a:1:{s:30:"wp_scheduled_auto_draft_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1525890203;a:3:{s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1525933429;a:2:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}s:25:"delete_expired_transients";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}s:7:"version";i:2;}', 'yes'),
(110, 'theme_mods_twentyseventeen', 'a:2:{s:18:"custom_css_post_id";i:-1;s:16:"sidebars_widgets";a:2:{s:4:"time";i:1525072201;s:4:"data";a:4:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:9:"sidebar-2";a:0:{}s:9:"sidebar-3";a:0:{}}}}', 'yes'),
(114, '_site_transient_update_core', 'O:8:"stdClass":4:{s:7:"updates";a:1:{i:0;O:8:"stdClass":10:{s:8:"response";s:6:"latest";s:8:"download";s:59:"https://downloads.wordpress.org/release/wordpress-4.9.5.zip";s:6:"locale";s:5:"en_US";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:59:"https://downloads.wordpress.org/release/wordpress-4.9.5.zip";s:10:"no_content";s:70:"https://downloads.wordpress.org/release/wordpress-4.9.5-no-content.zip";s:11:"new_bundled";s:71:"https://downloads.wordpress.org/release/wordpress-4.9.5-new-bundled.zip";s:7:"partial";b:0;s:8:"rollback";b:0;}s:7:"current";s:5:"4.9.5";s:7:"version";s:5:"4.9.5";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.7";s:15:"partial_version";s:0:"";}}s:12:"last_checked";i:1525849931;s:15:"version_checked";s:5:"4.9.5";s:12:"translations";a:0:{}}', 'no'),
(119, '_site_transient_update_themes', 'O:8:"stdClass":4:{s:12:"last_checked";i:1525849936;s:7:"checked";a:4:{s:12:"ozproduction";s:3:"1.0";s:13:"twentyfifteen";s:3:"1.9";s:15:"twentyseventeen";s:3:"1.5";s:13:"twentysixteen";s:3:"1.4";}s:8:"response";a:0:{}s:12:"translations";a:0:{}}', 'no'),
(124, 'can_compress_scripts', '1', 'no'),
(139, 'current_theme', 'ozproduction', 'yes'),
(140, 'theme_mods_ozproduction', 'a:5:{i:0;b:0;s:18:"nav_menu_locations";a:0:{}s:18:"custom_css_post_id";i:-1;s:16:"sidebars_widgets";a:2:{s:4:"time";i:1525332447;s:4:"data";a:3:{s:19:"wp_inactive_widgets";a:0:{}s:19:"primary-widget-area";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:21:"secondary-widget-area";a:0:{}}}s:11:"custom_logo";i:61;}', 'yes'),
(141, 'theme_switched', '', 'yes'),
(144, 'recently_activated', 'a:4:{s:34:"contact-form-7-recaptcha/index.php";i:1525353041;s:65:"frontend-dashboard-custom-post/frontend-dashboard-custom-post.php";i:1525352261;s:44:"advanced-custom-fields-row-field/acf-row.php";i:1525335568;s:41:"frontend-dashboard/frontend-dashboard.php";i:1525334752;}', 'yes'),
(151, 'ec_option_is_installed', '1', 'yes'),
(152, 'ec_option_storepage', '5', 'yes'),
(153, 'ec_option_cartpage', '6', 'yes'),
(154, 'ec_option_accountpage', '7', 'yes'),
(155, 'ec_option_default_manufacturer', '1', 'yes'),
(156, 'ec_option_store_locale', 'US', 'yes'),
(157, 'ec_option_setup_wizard_done', '1', 'yes'),
(158, 'ec_option_setup_wizard_step', '5', 'yes'),
(159, 'ec_option_review_complete', '1', 'yes'),
(160, 'ec_option_db_version', '1_30', 'yes'),
(161, 'ec_option_show_lite_message', '1', 'yes'),
(162, 'ec_option_new_linking_setup', '0', 'yes'),
(163, 'ec_option_show_install_message', '0', 'yes'),
(164, 'ec_option_added_custom_post_type', '2', 'yes'),
(165, 'ec_option_hide_admin_notice', '0', 'yes'),
(166, 'ec_option_hide_design_help_video', '0', 'yes'),
(167, 'ec_option_design_saved', '0', 'yes'),
(168, 'ec_option_amfphp_fix', '1', 'yes'),
(169, 'ec_option_track_user_clicks', '1', 'yes'),
(170, 'ec_option_cart_use_session_support', '0', 'yes'),
(171, 'ec_option_admin_display_sales_goal', '1', 'yes'),
(172, 'ec_option_admin_sales_goal', '1', 'yes'),
(173, 'ec_option_newsletter_done', '0', 'yes'),
(174, 'ec_option_allow_tracking', '-1', 'yes'),
(175, 'ec_option_weight', 'lbs', 'yes'),
(176, 'ec_option_base_currency', 'USD', 'yes'),
(177, 'ec_option_currency', '$', 'yes'),
(178, 'ec_option_currency_symbol_location', '1', 'yes'),
(179, 'ec_option_currency_negative_location', '1', 'yes'),
(180, 'ec_option_currency_decimal_symbol', '.', 'yes'),
(181, 'ec_option_currency_decimal_places', '2', 'yes'),
(182, 'ec_option_currency_thousands_seperator', ',', 'yes'),
(183, 'ec_option_show_currency_code', '0', 'yes'),
(184, 'ec_option_default_store_filter', '0', 'yes'),
(185, 'ec_option_default_payment_type', 'manual_bill', 'yes'),
(186, 'ec_option_shipping_type', 'price', 'yes'),
(187, 'ec_option_express_shipping_price', '9.99', 'yes'),
(188, 'ec_option_reg_code', '', 'yes'),
(189, 'ec_option_order_from_email', 'youremail@url.com', 'yes'),
(190, 'ec_option_password_from_email', 'youremail@url.com', 'yes'),
(191, 'ec_option_bcc_email_addresses', 'info@ozproductionservices.com', 'yes'),
(192, 'ec_option_use_state_dropdown', '1', 'yes'),
(193, 'ec_option_use_country_dropdown', '1', 'yes'),
(194, 'ec_option_estimate_shipping_zip', '1', 'yes'),
(195, 'ec_option_estimate_shipping_country', '1', 'yes'),
(196, 'ec_option_stylesheettype', '1', 'yes'),
(197, 'ec_option_googleanalyticsid', 'UA-XXXXXXX-X', 'yes'),
(198, 'ec_option_use_rtl', '0', 'yes'),
(199, 'ec_option_allow_guest', '1', 'yes'),
(200, 'ec_option_use_shipping', '0', 'yes'),
(201, 'ec_option_user_order_notes', '0', 'yes'),
(202, 'ec_option_terms_link', 'http://yoursite.com/termsandconditions', 'yes'),
(203, 'ec_option_privacy_link', 'http://yoursite.com/privacypolicy', 'yes'),
(204, 'ec_option_email_type', 'mail', 'yes'),
(205, 'ec_option_require_account_address', '0', 'yes'),
(206, 'ec_option_use_wp_mail', '0', 'yes'),
(207, 'ec_option_product_layout_type', 'grid_only', 'yes'),
(208, 'ec_option_show_featured_categories', '0', 'yes'),
(209, 'ec_option_product_filter_0', '1', 'yes'),
(210, 'ec_option_product_filter_1', '1', 'yes'),
(211, 'ec_option_product_filter_2', '1', 'yes'),
(212, 'ec_option_product_filter_3', '1', 'yes'),
(213, 'ec_option_product_filter_4', '1', 'yes'),
(214, 'ec_option_product_filter_5', '1', 'yes'),
(215, 'ec_option_product_filter_6', '1', 'yes'),
(216, 'ec_option_product_filter_7', '1', 'yes'),
(217, 'ec_option_show_giftcards', '0', 'yes'),
(218, 'ec_option_show_coupons', '0', 'yes'),
(219, 'ec_option_match_store_meta', '0', 'yes'),
(220, 'ec_option_use_old_linking_style', '1', 'yes'),
(221, 'ec_option_no_vat_on_shipping', '0', 'yes'),
(222, 'ec_option_display_as_catalog', '0', 'yes'),
(223, 'ec_option_addtocart_return_to_product', '0', 'yes'),
(224, 'ec_option_exchange_rates', 'EUR=.73,GBP=.6,JPY=101.9', 'yes'),
(225, 'ec_option_require_email_validation', '0', 'yes'),
(226, 'ec_option_display_country_top', '0', 'yes'),
(227, 'ec_option_use_address2', '1', 'yes'),
(228, 'ec_option_use_smart_states', '1', 'yes'),
(229, 'ec_option_show_account_subscriptions_link', '1', 'yes'),
(230, 'ec_option_gift_card_shipping_allowed', '0', 'yes'),
(231, 'ec_option_collect_shipping_for_subscriptions', '0', 'yes'),
(232, 'ec_option_skip_cart_login', '0', 'yes'),
(233, 'ec_option_use_contact_name', '1', 'yes'),
(234, 'ec_option_collect_user_phone', '1', 'yes'),
(235, 'ec_option_enable_company_name', '1', 'yes'),
(236, 'ec_option_skip_shipping_page', '0', 'yes'),
(237, 'ec_option_skip_reivew_screen', '0', 'yes'),
(238, 'ec_option_require_terms_agreement', '0', 'yes'),
(239, 'ec_option_show_menu_cart_icon', '0', 'yes'),
(240, 'ec_option_cart_menu_id', '0', 'yes'),
(241, 'ec_option_amazon_key', '', 'yes'),
(242, 'ec_option_amazon_secret', '', 'yes'),
(243, 'ec_option_amazon_bucket', '', 'yes'),
(244, 'ec_option_amazon_bucket_region', '', 'yes'),
(245, 'ec_option_deconetwork_url', '', 'yes'),
(246, 'ec_option_deconetwork_password', '', 'yes'),
(247, 'ec_option_tax_cloud_api_id', '', 'yes'),
(248, 'ec_option_tax_cloud_api_key', '', 'yes'),
(249, 'ec_option_tax_cloud_address', '', 'yes'),
(250, 'ec_option_tax_cloud_city', '', 'yes'),
(251, 'ec_option_tax_cloud_state', '', 'yes'),
(252, 'ec_option_tax_cloud_zip', '', 'yes'),
(253, 'ec_option_restrict_store', '0', 'yes'),
(254, 'ec_option_enable_user_notes', '0', 'yes'),
(255, 'ec_option_enable_newsletter_popup', '0', 'yes'),
(256, 'ec_option_use_estimate_shipping', '0', 'yes'),
(257, 'ec_option_use_custom_post_theme_template', '0', 'yes'),
(258, 'ec_option_show_email_on_receipt', '0', 'yes'),
(259, 'ec_option_show_breadcrumbs', '1', 'yes'),
(260, 'ec_option_show_model_number', '1', 'yes'),
(261, 'ec_option_show_categories', '1', 'yes'),
(262, 'ec_option_show_manufacturer', '1', 'yes'),
(263, 'ec_option_send_signup_email', '0', 'yes'),
(264, 'ec_option_show_magnification', '1', 'yes'),
(265, 'ec_option_show_large_popup', '1', 'yes'),
(266, 'ec_option_enable_product_paging', '1', 'yes'),
(267, 'ec_option_show_sort_box', '1', 'yes'),
(268, 'ec_option_customer_review_require_login', '0', 'yes'),
(269, 'ec_option_customer_review_show_user_name', '0', 'yes'),
(270, 'ec_option_hide_live_editor', '0', 'yes'),
(271, 'ec_option_hide_price_seasonal', '0', 'yes'),
(272, 'ec_option_hide_price_inquiry', '0', 'yes'),
(273, 'ec_option_enable_easy_canada_tax', '0', 'yes'),
(274, 'ec_option_collect_alberta_tax', '1', 'yes'),
(275, 'ec_option_collect_british_columbia_tax', '1', 'yes'),
(276, 'ec_option_collect_manitoba_tax', '1', 'yes'),
(277, 'ec_option_collect_new_brunswick_tax', '1', 'yes'),
(278, 'ec_option_collect_newfoundland_tax', '1', 'yes'),
(279, 'ec_option_collect_northwest_territories_tax', '1', 'yes'),
(280, 'ec_option_collect_nova_scotia_tax', '1', 'yes'),
(281, 'ec_option_collect_nunavut_tax', '1', 'yes'),
(282, 'ec_option_collect_ontario_tax', '1', 'yes'),
(283, 'ec_option_collect_prince_edward_island_tax', '1', 'yes'),
(284, 'ec_option_collect_quebec_tax', '1', 'yes'),
(285, 'ec_option_collect_saskatchewan_tax', '1', 'yes'),
(286, 'ec_option_collect_yukon_tax', '1', 'yes'),
(287, 'ec_option_collect_tax_on_shipping', '0', 'yes'),
(288, 'ec_option_show_multiple_vat_pricing', '0', 'yes'),
(289, 'ec_option_hide_cart_icon_on_empty', '0', 'yes'),
(290, 'ec_option_canada_tax_options', '0', 'yes'),
(291, 'ec_option_deconetwork_allow_blank_products', '0', 'yes'),
(292, 'ec_option_ship_items_seperately', '0', 'yes'),
(293, 'ec_option_default_dynamic_sizing', '1', 'yes'),
(294, 'ec_option_subscription_one_only', '1', 'yes'),
(295, 'ec_option_enable_gateway_log', '1', 'yes'),
(296, 'ec_option_enable_metric_unit_display', '0', 'yes'),
(297, 'ec_option_use_live_search', '1', 'yes'),
(298, 'ec_option_search_title', '1', 'yes'),
(299, 'ec_option_search_model_number', '1', 'yes'),
(300, 'ec_option_search_manufacturer', '1', 'yes'),
(301, 'ec_option_search_description', '1', 'yes'),
(302, 'ec_option_search_short_description', '1', 'yes'),
(303, 'ec_option_search_menu', '1', 'yes'),
(304, 'ec_option_show_image_on_receipt', '1', 'yes'),
(305, 'ec_option_search_by_or', '1', 'yes'),
(306, 'ec_option_custom_third_party', 'Gateway Name', 'yes'),
(307, 'ec_option_show_card_holder_name', '1', 'yes'),
(308, 'ec_option_show_stock_quantity', '1', 'yes'),
(309, 'ec_option_send_low_stock_emails', '0', 'yes'),
(310, 'ec_option_send_out_of_stock_emails', '0', 'yes'),
(311, 'ec_option_low_stock_trigger_total', '5', 'yes'),
(312, 'ec_option_show_delivery_days_live_shipping', '0', 'yes'),
(313, 'ec_option_model_number_extension', '-', 'yes'),
(314, 'ec_option_collect_vat_registration_number', '0', 'yes'),
(315, 'ec_option_validate_vat_registration_number', '0', 'yes'),
(316, 'ec_option_vatlayer_api_key', '', 'yes'),
(317, 'ec_option_vat_custom_rate', '0', 'yes'),
(318, 'ec_option_fedex_use_net_charge', '0', 'yes'),
(319, 'ec_option_static_ship_items_seperately', '0', 'yes'),
(320, 'ec_option_google_adwords_conversion_id', '', 'yes'),
(321, 'ec_option_google_adwords_language', 'en', 'yes'),
(322, 'ec_option_google_adwords_format', '3', 'yes'),
(323, 'ec_option_google_adwords_color', 'ffffff', 'yes'),
(324, 'ec_option_google_adwords_label', '', 'yes'),
(325, 'ec_option_google_adwords_currency', 'USD', 'yes'),
(326, 'ec_option_google_adwords_remarketing_only', 'false', 'yes'),
(327, 'ec_option_default_country', '0', 'yes'),
(328, 'ec_option_show_subscriber_feature', '0', 'yes'),
(329, 'ec_option_use_inquiry_form', '1', 'yes'),
(330, 'ec_option_order_use_smtp', '0', 'yes'),
(331, 'ec_option_order_from_smtp_host', '', 'yes'),
(332, 'ec_option_order_from_smtp_encryption_type', 'ssl', 'yes'),
(333, 'ec_option_order_from_smtp_port', '465', 'yes'),
(334, 'ec_option_order_from_smtp_username', '', 'yes'),
(335, 'ec_option_order_from_smtp_password', '', 'yes'),
(336, 'ec_option_password_use_smtp', '0', 'yes'),
(337, 'ec_option_password_from_smtp_host', '', 'yes'),
(338, 'ec_option_password_from_smtp_encryption_type', 'ssl', 'yes'),
(339, 'ec_option_password_from_smtp_port', '465', 'yes'),
(340, 'ec_option_password_from_smtp_username', '', 'yes'),
(341, 'ec_option_password_from_smtp_password', '', 'yes'),
(342, 'ec_option_minimum_order_total', '0.00', 'yes'),
(343, 'wpeasycart_abandoned_cart_automation', '0', 'yes'),
(344, 'ec_option_tiered_price_format', '1', 'yes'),
(345, 'ec_option_demo_data_installed', '0', 'yes'),
(346, 'ec_subscriptions_use_first_order_details', '0', 'yes'),
(347, 'ec_option_packing_slip_show_pricing', '1', 'yes'),
(348, 'ec_option_fb_pixel', '', 'yes'),
(349, 'ec_option_use_direct_deposit', '1', 'yes'),
(350, 'ec_option_direct_deposit_message', 'You have selected a manual payment method.', 'yes'),
(351, 'ec_option_use_affirm', '0', 'yes'),
(352, 'ec_option_affirm_public_key', '', 'yes'),
(353, 'ec_option_affirm_private_key', '', 'yes'),
(354, 'ec_option_affirm_financial_product', '', 'yes'),
(355, 'ec_option_affirm_sandbox_account', '0', 'yes'),
(356, 'ec_option_use_visa', '1', 'yes'),
(357, 'ec_option_use_delta', '0', 'yes'),
(358, 'ec_option_use_uke', '0', 'yes'),
(359, 'ec_option_use_discover', '1', 'yes'),
(360, 'ec_option_use_mastercard', '1', 'yes'),
(361, 'ec_option_use_mcdebit', '0', 'yes'),
(362, 'ec_option_use_amex', '1', 'yes'),
(363, 'ec_option_use_jcb', '0', 'yes'),
(364, 'ec_option_use_diners', '0', 'yes'),
(365, 'ec_option_use_laser', '0', 'yes'),
(366, 'ec_option_use_maestro', '0', 'yes'),
(367, 'ec_option_payment_process_method', '0', 'yes'),
(368, 'ec_option_payment_third_party', '0', 'yes'),
(369, 'ec_option_2checkout_thirdparty_sid', '', 'yes'),
(370, 'ec_option_2checkout_thirdparty_currency_code', 'USD', 'yes'),
(371, 'ec_option_2checkout_thirdparty_lang', 'en', 'yes'),
(372, 'ec_option_2checkout_thirdparty_purchase_step', 'payment-method', 'yes'),
(373, 'ec_option_2checkout_thirdparty_demo_mode', '0', 'yes'),
(374, 'ec_option_2checkout_thirdparty_sandbox_mode', '0', 'yes'),
(375, 'ec_option_2checkout_thirdparty_secret_word', '', 'yes'),
(376, 'ec_option_authorize_login_id', '', 'yes'),
(377, 'ec_option_authorize_trans_key', '', 'yes'),
(378, 'ec_option_authorize_test_mode', '0', 'yes'),
(379, 'ec_option_authorize_developer_account', '0', 'yes'),
(380, 'ec_option_authorize_use_legacy_url', '0', 'yes'),
(381, 'ec_option_authorize_currency_code', 'USD', 'yes'),
(382, 'ec_option_beanstream_merchant_id', '', 'yes'),
(383, 'ec_option_beanstream_api_passcode', '', 'yes'),
(384, 'ec_option_braintree_merchant_id', '', 'yes'),
(385, 'ec_option_braintree_merchant_account_id', '', 'yes'),
(386, 'ec_option_braintree_public_key', '', 'yes'),
(387, 'ec_option_braintree_private_key', '', 'yes'),
(388, 'ec_option_braintree_currency', 'USD', 'yes'),
(389, 'ec_option_braintree_environment', 'sandbox', 'yes'),
(390, 'ec_option_cardinal_processor_id', '', 'yes'),
(391, 'ec_option_cardinal_merchant_id', '', 'yes'),
(392, 'ec_option_cardinal_password', '', 'yes'),
(393, 'ec_option_cardinal_currency', '840,', 'yes'),
(394, 'ec_option_cardinal_test_mode', '0', 'yes'),
(395, 'ec_option_paypoint_merchant_id', '', 'yes'),
(396, 'ec_option_paypoint_vpn_password', '0', 'yes'),
(397, 'ec_option_paypoint_test_mode', '0', 'yes'),
(398, 'ec_option_chronopay_currency', '', 'yes'),
(399, 'ec_option_chronopay_product_id', '', 'yes'),
(400, 'ec_option_chronopay_shared_secret', '', 'yes'),
(401, 'ec_option_dwolla_thirdparty_key', '', 'yes'),
(402, 'ec_option_dwolla_thirdparty_secret', '', 'yes'),
(403, 'ec_option_dwolla_thirdparty_test_mode', '0', 'yes'),
(404, 'ec_option_dwolla_thirdparty_account_id', '812-xxx-xxxx', 'yes'),
(405, 'ec_option_versapay_id', '', 'yes'),
(406, 'ec_option_versapay_password', '', 'yes'),
(407, 'ec_option_versapay_language', 'en', 'yes'),
(408, 'ec_option_eway_use_rapid_pay', '0', 'yes'),
(409, 'ec_option_eway_customer_id', '', 'yes'),
(410, 'ec_option_eway_api_key', '', 'yes'),
(411, 'ec_option_eway_api_password', '', 'yes'),
(412, 'ec_option_eway_client_key', '', 'yes'),
(413, 'ec_option_eway_test_mode', '0', 'yes'),
(414, 'ec_option_eway_test_mode_success', '1', 'yes'),
(415, 'ec_option_firstdata_login_id', '', 'yes'),
(416, 'ec_option_firstdata_pem_file', '', 'yes'),
(417, 'ec_option_firstdata_host', 'secure.linkpt.net', 'yes'),
(418, 'ec_option_firstdata_port', '1129', 'yes'),
(419, 'ec_option_firstdata_test_mode', '0', 'yes'),
(420, 'ec_option_firstdata_use_ssl_cert', '0', 'yes'),
(421, 'ec_option_firstdatae4_exact_id', '', 'yes'),
(422, 'ec_option_firstdatae4_password', '', 'yes'),
(423, 'ec_option_firstdatae4_key_id', '', 'yes'),
(424, 'ec_option_firstdatae4_key', '', 'yes'),
(425, 'ec_option_firstdatae4_language', 'EN', 'yes'),
(426, 'ec_option_firstdatae4_currency', 'USD', 'yes'),
(427, 'ec_option_firstdatae4_test_mode', '0', 'yes'),
(428, 'ec_option_goemerchant_gateway_id', '', 'yes'),
(429, 'ec_option_goemerchant_processor_id', '', 'yes'),
(430, 'ec_option_goemerchant_trans_center_id', '', 'yes'),
(431, 'ec_option_intuit_oauth_version', '1', 'yes'),
(432, 'ec_option_intuit_app_token', '', 'yes'),
(433, 'ec_option_intuit_consumer_key', '', 'yes'),
(434, 'ec_option_intuit_consumer_secret', '', 'yes'),
(435, 'ec_option_intuit_currency', 'USD', 'yes'),
(436, 'ec_option_intuit_test_mode', '0', 'yes'),
(437, 'ec_option_intuit_realm_id', '', 'yes'),
(438, 'ec_option_intuit_access_token', '', 'yes'),
(439, 'ec_option_intuit_access_token_secret', '', 'yes'),
(440, 'ec_option_intuit_last_authorized', '0', 'yes'),
(441, 'ec_option_intuit_refresh_token', '', 'yes'),
(442, 'ec_option_nmi_3ds', '0', 'yes'),
(443, 'ec_option_nmi_api_key', '', 'yes'),
(444, 'ec_option_nmi_username', '', 'yes'),
(445, 'ec_option_nmi_password', '', 'yes'),
(446, 'ec_option_nmi_currency', 'USD', 'yes'),
(447, 'ec_option_nmi_processor_id', '', 'yes'),
(448, 'ec_option_nmi_ship_from_zip', '', 'yes'),
(449, 'ec_option_nmi_commodity_code', '', 'yes'),
(450, 'ec_option_nets_merchant_id', '', 'yes'),
(451, 'ec_option_nets_token', '', 'yes'),
(452, 'ec_option_nets_currency', 'USD', 'yes'),
(453, 'ec_option_nets_test_mode', '0', 'yes'),
(454, 'ec_option_migs_signature', '', 'yes'),
(455, 'ec_option_migs_access_code', '', 'yes'),
(456, 'ec_option_migs_merchant_id', '', 'yes'),
(457, 'ec_option_moneris_ca_store_id', '', 'yes'),
(458, 'ec_option_moneris_ca_api_token', '', 'yes'),
(459, 'ec_option_moneris_ca_test_mode', '0', 'yes'),
(460, 'ec_option_moneris_us_store_id', '', 'yes'),
(461, 'ec_option_moneris_us_api_token', '', 'yes'),
(462, 'ec_option_moneris_us_test_mode', '0', 'yes'),
(463, 'ec_option_payfast_merchant_id', '', 'yes'),
(464, 'ec_option_payfast_merchant_key', '', 'yes'),
(465, 'ec_option_payfast_passphrase', '', 'yes'),
(466, 'ec_option_payfast_sandbox', '0', 'yes'),
(467, 'ec_option_payfort_access_code', '', 'yes'),
(468, 'ec_option_payfort_merchant_id', '', 'yes'),
(469, 'ec_option_payfort_request_phrase', '', 'yes'),
(470, 'ec_option_payfort_response_phrase', '', 'yes'),
(471, 'ec_option_payfort_sha_type', 'sha256', 'yes'),
(472, 'ec_option_payfort_language', 'en', 'yes'),
(473, 'ec_option_payfort_currency_code', 'USD', 'yes'),
(474, 'ec_option_payfort_use_sadad', '0', 'yes'),
(475, 'ec_option_payfort_use_naps', '0', 'yes'),
(476, 'ec_option_payfort_sadad_olp', '', 'yes'),
(477, 'ec_option_payfort_use_currency_service', '0', 'yes'),
(478, 'ec_option_payfort_test_mode', '1', 'yes'),
(479, 'ec_option_payline_username', '', 'yes'),
(480, 'ec_option_payline_password', '', 'yes'),
(481, 'ec_option_payline_currency', 'USD', 'yes'),
(482, 'ec_option_payment_express_username', '', 'yes'),
(483, 'ec_option_payment_express_password', '', 'yes'),
(484, 'ec_option_payment_express_currency', 'NZD', 'yes'),
(485, 'ec_option_payment_express_developer_account', '0', 'yes'),
(486, 'ec_option_payment_express_thirdparty_username', '', 'yes'),
(487, 'ec_option_payment_express_thirdparty_key', '', 'yes'),
(488, 'ec_option_payment_express_thirdparty_currency', 'NZD', 'yes'),
(489, 'ec_option_paypal_use_sandbox', '0', 'yes'),
(490, 'ec_option_paypal_email', '', 'yes'),
(491, 'ec_option_paypal_enable_pay_now', '0', 'yes'),
(492, 'ec_option_paypal_enable_credit', '0', 'yes'),
(493, 'ec_option_paypal_sandbox_merchant_id', '', 'yes'),
(494, 'ec_option_paypal_sandbox_app_id', '', 'yes'),
(495, 'ec_option_paypal_sandbox_secret', '', 'yes'),
(496, 'ec_option_paypal_sandbox_access_token', '', 'yes'),
(497, 'ec_option_paypal_sandbox_access_token_expires', '0', 'yes'),
(498, 'ec_option_paypal_production_merchant_id', '', 'yes'),
(499, 'ec_option_paypal_production_app_id', '', 'yes'),
(500, 'ec_option_paypal_production_secret', '', 'yes'),
(501, 'ec_option_paypal_production_access_token', '', 'yes'),
(502, 'ec_option_paypal_production_access_token_expires', '0', 'yes'),
(503, 'ec_option_paypal_marketing_solution_cid_sandbox', '', 'yes'),
(504, 'ec_option_paypal_marketing_solution_cid_production', '', 'yes'),
(505, 'ec_option_paypal_currency_code', 'USD', 'yes'),
(506, 'ec_option_paypal_use_selected_currency', '0', 'yes'),
(507, 'ec_option_paypal_charset', 'UTF-8', 'yes'),
(508, 'ec_option_paypal_lc', 'US', 'yes'),
(509, 'ec_option_paypal_weight_unit', 'kgs', 'yes'),
(510, 'ec_option_paypal_collect_shipping', '0', 'yes'),
(511, 'ec_option_paypal_send_shipping_address', '0', 'yes'),
(512, 'ec_option_paypal_advanced_test_mode', '0', 'yes'),
(513, 'ec_option_paypal_advanced_vendor', '', 'yes'),
(514, 'ec_option_paypal_advanced_partner', '', 'yes'),
(515, 'ec_option_paypal_advanced_user', '', 'yes'),
(516, 'ec_option_paypal_advanced_password', '', 'yes'),
(517, 'ec_option_paypal_pro_test_mode', '0', 'yes'),
(518, 'ec_option_paypal_pro_vendor', '', 'yes'),
(519, 'ec_option_paypal_pro_partner', '', 'yes'),
(520, 'ec_option_paypal_pro_user', '', 'yes'),
(521, 'ec_option_paypal_pro_password', '', 'yes'),
(522, 'ec_option_paypal_pro_currency', 'USD', 'yes'),
(523, 'ec_option_paypal_payments_pro_test_mode', '0', 'yes'),
(524, 'ec_option_paypal_payments_pro_user', '', 'yes'),
(525, 'ec_option_paypal_payments_pro_password', '', 'yes'),
(526, 'ec_option_paypal_payments_pro_signature', '', 'yes'),
(527, 'ec_option_paypal_payments_pro_currency', 'USD', 'yes'),
(528, 'ec_option_skrill_merchant_id', '', 'yes'),
(529, 'ec_option_skrill_company_name', '', 'yes'),
(530, 'ec_option_skrill_email', '', 'yes'),
(531, 'ec_option_skrill_language', 'EN', 'yes'),
(532, 'ec_option_realex_thirdparty_type', 'legacy', 'yes'),
(533, 'ec_option_realex_thirdparty_merchant_id', '', 'yes'),
(534, 'ec_option_realex_thirdparty_secret', '', 'yes'),
(535, 'ec_option_realex_thirdparty_account', 'redirect', 'yes'),
(536, 'ec_option_realex_thirdparty_currency', 'GBP', 'yes'),
(537, 'ec_option_realex_merchant_id', '', 'yes'),
(538, 'ec_option_realex_secret', '', 'yes'),
(539, 'ec_option_realex_currency', 'GBP', 'yes'),
(540, 'ec_option_realex_3dsecure', '0', 'yes'),
(541, 'ec_option_realex_test_mode', '0', 'yes'),
(542, 'ec_option_redsys_merchant_code', '', 'yes'),
(543, 'ec_option_redsys_terminal', '', 'yes'),
(544, 'ec_option_redsys_currency', '978', 'yes'),
(545, 'ec_option_redsys_key', '', 'yes'),
(546, 'ec_option_redsys_test_mode', '0', 'yes'),
(547, 'ec_option_sagepay_vendor', '', 'yes'),
(548, 'ec_option_sagepay_currency', 'USD', 'yes'),
(549, 'ec_option_sagepay_simulator', '0', 'yes'),
(550, 'ec_option_sagepay_testmode', '0', 'yes'),
(551, 'ec_option_sagepayus_mid', '999999999997', 'yes'),
(552, 'ec_option_sagepayus_mkey', 'A1B2C3D4E5F6', 'yes'),
(553, 'ec_option_sagepayus_application_id', 'SAGETEST1', 'yes'),
(554, 'ec_option_sagepay_paynow_za_service_key', '', 'yes'),
(555, 'ec_option_securenet_id', '', 'yes'),
(556, 'ec_option_securenet_secure_key', '', 'yes'),
(557, 'ec_option_securenet_use_sandbox', '0', 'yes'),
(558, 'ec_option_securepay_merchant_id', '', 'yes'),
(559, 'ec_option_securepay_password', '', 'yes'),
(560, 'ec_option_securepay_currency', 'AUD', 'yes'),
(561, 'ec_option_securepay_test_mode', '0', 'yes'),
(562, 'ec_option_skrill_currency_code', 'USD', 'yes'),
(563, 'ec_option_square_access_token', '', 'yes'),
(564, 'ec_option_square_token_expires', '', 'yes'),
(565, 'ec_option_square_application_id', '', 'yes'),
(566, 'ec_option_square_location_id', '0', 'yes'),
(567, 'ec_option_square_currency', 'USD', 'yes'),
(568, 'ec_option_psigate_store_id', 'teststore', 'yes'),
(569, 'ec_option_psigate_passphrase', 'psigate1234', 'yes'),
(570, 'ec_option_psigate_test_mode', '1', 'yes'),
(571, 'ec_option_use_proxy', '0', 'yes'),
(572, 'ec_option_proxy_address', '0', 'yes'),
(573, 'ec_option_stripe_api_key', '', 'yes'),
(574, 'ec_option_stripe_public_api_key', '', 'yes'),
(575, 'ec_option_stripe_currency', 'USD', 'yes'),
(576, 'ec_option_stripe_order_create_customer', '0', 'yes'),
(577, 'ec_option_stripe_connect_use_sandbox', '0', 'yes'),
(578, 'ec_option_stripe_connect_sandbox_access_token', '', 'yes'),
(579, 'ec_option_stripe_connect_sandbox_refresh_token', '', 'yes');
INSERT INTO `ops_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(580, 'ec_option_stripe_connect_sandbox_publishable_key', '', 'yes'),
(581, 'ec_option_stripe_connect_sandbox_user_id', '', 'yes'),
(582, 'ec_option_stripe_connect_production_access_token', '', 'yes'),
(583, 'ec_option_stripe_connect_production_refresh_token', '', 'yes'),
(584, 'ec_option_stripe_connect_production_publishable_key', '', 'yes'),
(585, 'ec_option_stripe_connect_production_user_id', '', 'yes'),
(586, 'ec_option_virtualmerchant_ssl_merchant_id', '', 'yes'),
(587, 'ec_option_virtualmerchant_ssl_user_id', '', 'yes'),
(588, 'ec_option_virtualmerchant_ssl_pin', '', 'yes'),
(589, 'ec_option_virtualmerchant_currency', 'USD', 'yes'),
(590, 'ec_option_virtualmerchant_demo_account', '0', 'yes'),
(591, 'ec_option_language', 'en-us', 'yes');
INSERT INTO `ops_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(592, 'ec_option_language_data', '{"en-us":{"label":"US English","options":{"sort_bar":{"label":"Product Page Sort Menu","options":{"sort_by_price_low":{"title":"Product Page Sort Box Item 1","value":"Price Low-High"},"sort_by_price_high":{"title":"Product Page Sort Box Item 2","value":"Price High-Low"},"sort_by_title_a":{"title":"Product Page Sort Box Item 3","value":"Title A-Z"},"sort_by_title_z":{"title":"Product Page Sort Box Item 4","value":"Title Z-A"},"sort_by_newest":{"title":"Product Page Sort Box Item 5","value":"Newest"},"sort_by_rating":{"title":"Product Page Sort Box Item 6","value":"Best Rating"},"sort_by_most_viewed":{"title":"Product Page Sort Box Item 7","value":"Most Viewed"},"sort_default":{"title":"Product Page Sort Box Default","value":"Default Sorting"}}},"product_page":{"label":"Product Page","options":{"product_quick_view":{"title":"Product Quick View Label","value":"Quickview"},"product_design_now":{"title":"Product Design Now Label","value":"DESIGN NOW"},"product_no_results":{"title":"Product Page No Results","value":"No Results Found"},"product_items_per_page":{"title":"Product Page Items per Page","value":"Items Per Page:"},"product_paging_page":{"title":"Product Page Page in Page a of b","value":"Page"},"product_paging_of":{"title":"Product Page of in Page a of b","value":"of"},"product_showing":{"title":"Product Page Num Results Showing","value":"Showing"},"product_all":{"title":"Product Page Num Results all","value":"all"},"product_results":{"title":"Product Page Num Results","value":"Results"},"product_view_cart":{"title":"Product Page View Cart","value":"View Cart"},"product_product_added_note":{"title":"Product Page Added to Cart Note","value":"Product successfully added to your cart."},"product_product_view_per_page":{"title":"Product Page View in Per Page","value":"View"},"product_page_restricted_line_1":{"title":"Restricted Product Line 1","value":"OPEN ACCESS IS NOT ALLOWED FOR THE STORE"},"product_page_restricted_line_2":{"title":"Restricted Product Line 2 Prior to Link","value":"Please log in or create an account by"},"product_page_restricted_link_text":{"title":"Restricted Product Link Text","value":"clicking here"},"product_page_restricted_line_3":{"title":"Restricted Product Line 2 Post Link","value":". Your account may need to be verified before you will be able to access the store."},"product_page_start_trial_1":{"title":"Start Your Trial Text Part 1","value":"Start Your"},"product_page_start_trial_2":{"title":"Start Your Trial Text Part 2","value":"Day Free Trial Today!"},"product_inc_vat_text":{"title":"Including VAT Text","value":"inc VAT"},"product_excluding_vat_text":{"title":"Excluding VAT Text","value":"excluding VAT"}}},"quick_view":{"label":"Quick View Panel","options":{"quick_view_gift_card_message":{"title":"Gift Card Message Title","value":"SPECIAL MESSAGE"},"quick_view_gift_card_to_name":{"title":"Gift Card To Name Title","value":"TO"},"quick_view_gift_card_from_name":{"title":"Gift Card From Name Title","value":"FROM"},"quick_view_quantity":{"title":"Quantity Title","value":"QUANTITY"},"quick_view_remaining":{"title":"label in x items remaining","value":"remaining"},"quick_view_add_to_cart":{"title":"Add to Cart Button","value":"ADD TO CART"},"quick_view_select_options":{"title":"Product details missing options error text","value":"Please select all options for this product."},"quick_view_view_full_details":{"title":"Link Text View Full Product Details","value":"View Full Product Details"}}},"product_details":{"label":"Product Details Page","options":{"product_details_donation_label":{"title":"Your Donation Label","value":"Your Donation: "},"product_details_minimum_donation":{"title":"Your Donation Minimum Label","value":"minimum donation:"},"product_details_gift_card_message":{"title":"Store Gift Card Message","value":"Include a Message"},"product_details_gift_card_to_name":{"title":"Gift Card To Name Title","value":"TO"},"product_details_gift_card_from_name":{"title":"Gift Card From Name Title","value":"FROM"},"product_details_quantity":{"title":"Quantity Label","value":"Quantity"},"product_details_remaining":{"title":"x remaining","value":"remaining"},"product_details_x_of_y":{"title":"page text divider","value":"of"},"product_details_model_number":{"title":"model number text","value":"model number"},"product_details_reduced_price":{"title":"reduced price text","value":"REDUCED PRICE YOU SAVE"},"product_details_description":{"title":"Description Title","value":"Description"},"product_details_customer_reviews":{"title":"Customer Reviews Title","value":"Customer Reviews"},"product_details_specifications":{"title":"Specifications Title","value":"Specifications"},"product_details_customer_rating":{"title":"Customer Rating","value":"Overall Customer Rating"},"product_details_review_based_on":{"title":"Review Based ON","value":"based on "},"product_details_review":{"title":"Review","value":"review"},"product_details_review_no_reviews":{"title":"Product Details, Customer Reviews, No Reviews","value":"There are no reviews yet, submit yours in the box provided."},"product_details_add_to_cart":{"title":"Add to Cart button text","value":"ADD TO CART"},"product_details_added_to_cart":{"title":"Checkout Now button text","value":"CHECKOUT NOW"},"product_details_select_options":{"title":"Select Options Button","value":"Select Options"},"product_details_out_of_stock":{"title":"Out of Stock Text","value":"OUT OF STOCK"},"product_details_sign_up_now":{"title":"Sign Up Now Text","value":"SIGN UP NOW"},"product_details_inquiry_title":{"title":"Inquiry Title","value":"Submit Product Inquiry"},"product_details_inquiry_error":{"title":"Inquiry Error Text","value":"Please fill out all required fields"},"product_details_inquiry_name":{"title":"Inquiry Name","value":"*Your Name:"},"product_details_inquiry_email":{"title":"Inquiry Email","value":"*Your Email:"},"product_details_inquiry_message":{"title":"Inquire Button Text","value":"*Your Message:"},"product_details_inquire":{"title":"Inquire Button Text","value":"SUBMIT INQUIRY"},"product_details_inquiry_send_copy":{"title":"Inquiry - Send Copy","value":"Please send me a copy"},"product_details_inquiry_thank_you":{"title":"Inquiry Thank You Note","value":"Thank you for submitting a product inquiry and we will get back to you as soon as possible."},"product_details_minimum_quantity_text1":{"title":"Min Purchase Amount Text Part 1","value":"Minimum purchase amount of"},"product_details_minimum_quantity_text2":{"title":"Min Purchase Amount Text Part 2","value":"is required"},"product_details_maximum_quantity_text1":{"title":"Max Purchase Amount Text Part 1","value":"Maximum purchase amount of"},"product_details_maximum_quantity_text2":{"title":"Max Purchase Amount Text Part 2","value":"is allowed"},"product_details_handling_fee_notice1":{"title":"One Time Handling Fee Part 1","value":"*A one-time handling fee of"},"product_details_handling_fee_notice2":{"title":"One Time Handling Fee Part 2","value":"will be added to your shipping total at checkout."},"product_details_handling_fee_each_notice1":{"title":"Handling Fee Each Part 1","value":"*A handling fee of"},"product_details_handling_fee_each_notice2":{"title":"Handling Fee Each Part 2","value":"per item will be added to your shipping total at checkout."},"product_details_signup_fee_notice1":{"title":"One Time Sign-up Fee Part 1","value":"*A one-time sign-up fee of"},"product_details_signup_fee_notice2":{"title":"One Time Sign-up Fee Part 2","value":"will be added to your first payment total."},"product_details_home_link":{"title":"Home Breadcrumbs Link","value":"Home"},"product_details_store_link":{"title":"Store Breadcrumbs Link","value":"Shop"},"product_details_reviews_text":{"title":"Store Reviews Count","value":"Reviews"},"product_details_gift_card_recipient_name":{"title":"Store Gift Card To Name","value":"Recipient''s Name"},"product_details_gift_card_recipient_email":{"title":"Store Gift Card To Email","value":"Recipient''s Email"},"product_details_gift_card_sender_name":{"title":"Store Gift Card Sender''s Name","value":"Sender''s Name"},"product_details_donation_amount":{"title":"Store Donation Amount","value":"Donation Amount"},"product_details_loading_options":{"title":"Store Loading Options","value":"Loading Available Options"},"product_details_missing_option":{"title":"Store Missing Option","value":"Missing Option:"},"product_details_maximum_quantity":{"title":"Store Maximum Quantity Exceeded","value":"Maximum quantity exceeded"},"product_details_one_time_addition1":{"title":"Store One Time Addition 1","value":"A one-time price of"},"product_details_one_time_addition2":{"title":"Store One Time Addition 2","value":"will be added to your order."},"product_details_left_in_stock":{"title":"Store Left in Stock","value":"Left in Stock"},"product_details_categories":{"title":"Store Categories","value":"Categories:"},"product_details_manufacturer":{"title":"Store Manufacturer","value":"Manufacturer:"},"product_details_related_products":{"title":"Store Related Products","value":"Related Products"},"product_details_tier_buy":{"title":"Tiered Pricing buy x at y","value":"Buy"},"product_details_tier_buy_at":{"title":"Tiered Pricing buy x at y","value":"at"},"product_details_tier_or_more":{"title":"Tiered Pricing buy x or more","value":"or more"},"product_details_tier_each":{"title":"Tiered Pricing buy x at y each","value":"each"},"product_details_gift_card_error":{"title":"Gift Card Option Error Text","value":"Missing Gift Card Options"},"product_details_donation_error":{"title":"Donation Amount Error Text","value":"Invalid donation amount entered. Please enter a minimum value of"},"product_details_your_price":{"title":"Your Price:","value":"Your Price:"},"product_details_backorder_until":{"title":"Backorder UNTIL date, until","value":"until"},"product_details_backordered":{"title":"BACKORDERED until date","value":"Backordered"},"product_details_some_backordered":{"title":"Some BACKORDERED","value":"Some items are backordered, only"},"product_details_some_backordered_remaining":{"title":"Some BACKORDERED remaining","value":"remaining."},"product_details_backordered_message":{"title":"Backordered message in cart","value":"Some items in your cart are currently out of stock and on backorder."},"product_details_vat_included":{"title":"Including VAT","value":"Including"},"product_details_vat_excluded":{"title":"Excluding VAT","value":"Excluding"},"product_details_backorder_button":{"title":"Backorder Button","value":"BACKORDER NOW"},"product_details_reviews_for_text":{"title":"Reviews for","value":"Reviews for"},"product_details_as_low_as":{"title":"As low as","value":"As low as"}}},"customer_review":{"label":"Customer Review Panel","options":{"customer_review_title":{"title":"Product Details Rating Box Title","value":"Write a Review"},"customer_review_choose_rating":{"title":"Product Details Rating Box Choose Rating","value":"choose a rating"},"customer_review_enter_title":{"title":"Product Details Rating Box Choose Title","value":"enter a review title"},"customer_review_enter_description":{"title":"Product Details Rating Box Choose Description","value":"enter a review description"},"customer_review_select_option":{"title":"Product Details Default Select Option Text","value":"select an option"},"customer_review_close_button":{"title":"Product Details Rating Box Close Button","value":"Close"},"customer_review_submit_button":{"title":"Product Details Rating Box Submit Button","value":"Submit Review"},"product_details_write_a_review":{"title":"Product Details Write a Review Button","value":"Write a Review"},"product_details_add_a_review_for":{"title":"Store Add a Review","value":"Add a review for"},"product_details_your_review_title":{"title":"Store Review title","value":"Your Review Title"},"product_details_your_review_rating":{"title":"Store Review Rating","value":"Your Rating"},"product_details_your_review_message":{"title":"Store Review Message","value":"Your Review"},"product_details_your_review_submit":{"title":"Store Review Submit","value":"SUBMIT"},"product_details_review_submitted_button":{"title":"Product Details Review Submitted Button","value":"REVIEW SUBMITTED"},"product_details_submitting_review":{"title":"Store Submitting Review","value":"Submitting Your Review, Please Wait"},"product_details_review_submitted":{"title":"Store Review Submitted","value":"Your Review Has Been Submitted Successfully"},"product_details_review_log_in_first":{"title":"Log in to Review","value":"Please sign in or create an account to submit a review for this product."},"product_details_review_anonymous_reviewer":{"title":"Anonymous Reviewer Name","value":"Anonymous"},"review_error":{"title":"Store Review Error Missing Data","value":"You must include a title, rating, and message in your review."}}},"cart":{"label":"Cart","options":{"cart_title":{"title":"Checkout Cart Title","value":"SHOPPING CART"},"cart_checkout_details_title":{"title":"Checkout Details Title","value":"CHECKOUT DETAILS"},"cart_submit_payment_title":{"title":"Submit Payment Title","value":"SUBMIT PAYMENT"},"your_cart_title":{"title":"Your Cart Title","value":"YOUR CART"},"cart_empty_cart":{"title":"Checkout, Empty Cart Message","value":"There are no items in your cart."},"cart_checkout":{"title":"Checkout Page, Checkout Button","value":"Checkout"},"cart_continue_shopping":{"title":"Checkout Page, Continue Shopping Link","value":"Continue Shopping"},"cart_header_column1":{"title":"Cart Header, Column 1","value":"Product"},"cart_header_column2":{"title":"Cart Header, Column 2","value":"Details"},"cart_header_column3":{"title":"Cart Header, Column 3","value":"Price"},"cart_header_column4":{"title":"Cart Header, Column 4","value":"Quantity"},"cart_header_column5":{"title":"Cart Header, Column 5","value":"Total"},"cart_header_column6":{"title":"Cart Header, Column 6","value":"Actions"},"cart_item_vat_text":{"title":"Cart Item, Included Vat","value":"VAT @"},"cart_item_update_button":{"title":"Cart Item, Update Button","value":"UPDATE"},"cart_item_remove_button":{"title":"Cart Item, Remove Button","value":"REMOVE"},"cart_item_adjustment":{"title":"Cart Item, Adjustment per Item","value":"per item"},"cart_item_new_price_option":{"title":"Cart Item, Price Override Text","value":"Item Price of"},"cart_menu_icon_label":{"title":"Cart in Menu, Item","value":"Item"},"cart_menu_icon_label_plural":{"title":"Cart in Menu, Items","value":"Items"},"deconetwork_edit":{"title":"Deconetwork, Edit Design","value":"Edit Design"},"cart_order_adjustment":{"title":"Cart Item, Adjustment per Order","value":"per order"},"cart_return_to_store":{"title":"Empty Cart, Return to store","value":"RETURN TO STORE"},"cartitem_max_error":{"title":"Max Quantity Exceeded Error","value":"Maximum Quantity Exceeded"},"cartitem_min_error":{"title":"Min Quantity Required Error","value":"Minimum Quantity Required"},"cart_please_wait":{"title":"Cart Please Wait","value":"Please Wait"},"cart_item_adjustment_per_character":{"title":"Cart Per Character Adjustment","value":"Per Character"},"cart_minimum_purchase_amount1":{"title":"A Minimum Order Amount of","value":"A Minimum Order Amount of"},"cart_minimum_purchase_amount2":{"title":"Min Required Text 2","value":"is Required"}}},"cart_form_notices":{"label":"Cart - Form Error Notices","options":{"cart_notice_is_required":{"title":"Cart, Notice, Is Required Field","value":"is a required field"},"cart_notice_item_must_match":{"title":"Cart, Notice, Items Must Match","value":"must match"},"cart_notice_billing_first_name":{"title":"Cart, Notice, Billing, First Name","value":"Billing First Name"},"cart_notice_billing_last_name":{"title":"Cart, Notice, Billing, Last Name","value":"Billing Last Name"},"cart_notice_billing_address":{"title":"Cart, Notice, Billing, Address","value":"Billing Address"},"cart_notice_billing_city":{"title":"Cart, Notice, Billing, City","value":"Billing City"},"cart_notice_billing_state":{"title":"Cart, Notice, Billing, State","value":"Billing State"},"cart_notice_billing_zip_code":{"title":"Cart, Notice, Billing, Zip Code","value":"Billing Zip Code"},"cart_notice_billing_country":{"title":"Cart, Notice, Billing, Country","value":"Billing Country"},"cart_notice_billing_phone":{"title":"Cart, Notice, Billing, Phone","value":"Billing Phone"},"cart_notice_shipping_first_name":{"title":"Cart, Notice, Shipping, First Name","value":"Shipping First Name"},"cart_notice_shipping_last_name":{"title":"Cart, Notice, Shipping, Last Name","value":"Shipping Last Name"},"cart_notice_shipping_address":{"title":"Cart, Notice, Shipping, Address","value":"Shipping Address"},"cart_notice_shipping_city":{"title":"Cart, Notice, Shipping, City","value":"Shipping City"},"cart_notice_shipping_state":{"title":"Cart, Notice, Shipping, State","value":"Shipping State"},"cart_notice_shipping_zip_code":{"title":"Cart, Notice, Shipping, Zip Code","value":"Shipping Zip Code"},"cart_notice_shipping_country":{"title":"Cart, Notice, Shipping, Country","value":"Shipping Country"},"cart_notice_shipping_phone":{"title":"Cart, Notice, Shipping, Phone","value":"Shipping Phone"},"cart_notice_contact_first_name":{"title":"Cart, Notice, Contact First Name","value":"Contact First Name"},"cart_notice_contact_last_name":{"title":"Cart, Notice, Contact Last Name","value":"Contact Last Name"},"cart_notice_email":{"title":"Cart, Notice, Email","value":"Email"},"cart_notice_retype_email":{"title":"Cart, Notice, Retype Email","value":"Retype Email"},"cart_notice_emails_match":{"title":"Cart, Notice, Emails Match","value":"Emails"},"cart_notice_password":{"title":"Cart, Notice, Password","value":"Password"},"cart_notice_retype_password":{"title":"Cart, Notice, Retype Password","value":"Retype Password"},"cart_notice_passwords_match":{"title":"Cart, Notice, Passwords Match","value":"Passwords"},"cart_notice_length_error":{"title":"Cart, Notice, Password Format","value":"Please enter a password of at least 6 characters"},"cart_notice_payment_card_type":{"title":"Cart, Notice, Card Type","value":"Card Type"},"cart_notice_payment_card_holder_name":{"title":"Cart, Notice, Card Holder Required","value":"Card Holder Name"},"cart_notice_payment_card_number":{"title":"Cart, Notice, Card Number Required","value":"Card Number"},"cart_notice_payment_card_number_error":{"title":"Cart, Notice, Card Number Required","value":"The credit card number entered is invalid"},"cart_notice_payment_card_exp_month":{"title":"Cart, Notice, EXP Month Required","value":"Expiration Month"},"cart_notice_payment_card_exp_year":{"title":"Cart, Notice, EXP Year Required","value":"Expiration Year"},"cart_notice_payment_card_code":{"title":"Cart, Notice, Security Code Required","value":"Security Code"},"cart_notice_please_enter_your":{"title":"Cart, Notice, Please Enter Your","value":"Please enter your"},"cart_notice_please_select_your":{"title":"Cart, Notice, Please Select Your","value":"Please select your"},"cart_notice_please_enter_valid":{"title":"Cart, Notice, Please Enter Valid","value":"Please enter a valid"},"cart_notice_emails_do_not_match":{"title":"Cart, Notice, Email Do Not Match","value":"Your email addresses do not match"},"cart_notice_passwords_do_not_match":{"title":"Cart, Notice, Passwords Do Not Match","value":"Your passwords do not match"},"cart_notice_checkout_details_errors":{"title":"Cart, Notice, Checkout Details Errors","value":"Please correct the errors in your checkout details"},"cart_notice_payment_accept_terms":{"title":"Cart, Notice, Please Accept Terms","value":"Please agree to the terms and conditions"},"cart_notice_payment_correct_errors":{"title":"Cart, Notice, Payment Correct Errors","value":"Please correct the errors to your checkout information above"}}},"cart_process":{"label":"Cart - Progress Bar","options":{"cart_process_cart_link":{"title":"Cart, Progress, Cart","value":"Cart"},"cart_process_shipping_link":{"title":"Cart, Progress, Address","value":"Address"},"cart_process_review_link":{"title":"Cart, Progress, Cart","value":"Review\\/Pay"},"cart_process_complete_link":{"title":"Cart, Progress, Complete","value":"Complete"}}},"cart_coupons":{"label":"Cart - Enter Coupons and Gift Cards","options":{"cart_coupon_title":{"title":"Cart, Coupon Title","value":"Coupon"},"cart_gift_card_title":{"title":"Cart, Gift Card Title","value":"Gift Card"},"cart_coupon_sub_title":{"title":"Cart, Coupons Sub-Title","value":"Redeem coupon codes or gift cards by entering them below."},"cart_redeem_gift_card":{"title":"Cart, Redeem Gift Card Button","value":"Redeem Gift Card"},"cart_apply_coupon":{"title":"Cart, Apply Coupon Button","value":"Apply Coupon"},"cart_invalid_coupon":{"title":"Cart, Invalid Coupon","value":"Not a valid coupon code"},"cart_not_applicable_coupon":{"title":"Cart, Not Applicable Coupon","value":"Coupon code does not apply to this product"},"cart_max_exceeded_coupon":{"title":"Cart, Max Coupon Use Exceeded","value":"Max uses exceeded"},"cart_invalid_giftcard":{"title":"Cart, Invalid Gift Card","value":"Not a valid gift card number"},"cart_enter_coupon":{"title":"Cart, Enter Coupon Hint","value":"Enter Coupon Code"},"cart_enter_gift_code":{"title":"Cart, Enter Gift Card Hint","value":"Enter Gift Card"},"cart_coupon_expired":{"title":"Cart, Coupon Code Expired","value":"Coupon Has Expired"}}},"cart_estimate_shipping":{"label":"Cart - Estimate Shipping","options":{"cart_estimate_shipping_title":{"title":"Cart, Estimate Shipping Title","value":"Estimate Shipping Costs"},"cart_estimate_shipping_sub_title":{"title":"Cart, Estimate Shipping Sub Title","value":"Please enter your zip code to estimate shipping costs."},"cart_estimate_shipping_input_country_label":{"title":"Cart, Estimate Shipping, Input Label Country","value":"Country:"},"cart_estimate_shipping_select_one":{"title":"Cart, Estimate Shipping, Select one for country box","value":"Select One"},"cart_estimate_shipping_input_label":{"title":"Cart, Estimate Shipping, Input Label Zip Code","value":"Zip Code:"},"cart_estimate_shipping_button":{"title":"Cart, Estimate Shipping Button","value":"Estimate Shipping"},"cart_estimate_shipping_standard":{"title":"Cart, Standard Shipping Label","value":"Standard Shipping"},"cart_estimate_shipping_express":{"title":"Cart, Express Shipping Label","value":"Ship Express"},"cart_estimate_shipping_error":{"title":"Cart, Mismatch Postal Code Error","value":"Your postal code does not match our store''s country. Shipping rates will be determined after you have entered your shipping information"},"cart_estimate_shipping_hint":{"title":"Cart, Estimate Shipping Hint","value":"Enter Zip Code"},"delivery_in":{"title":"Cart, Live Shipping Delivery In (Days)","value":"delivery in"},"delivery_days":{"title":"Cart, Live Shipping (Delivery In) Days","value":"days"}}},"cart_totals":{"label":"Cart - Cart Totals","options":{"cart_totals_title":{"title":"Cart, Cart Totals Title","value":"Cart Totals"},"cart_totals_subtotal":{"title":"Cart, Cart Totals, Subtotal","value":"Cart Subtotal"},"cart_totals_shipping":{"title":"Cart, Cart Totals, Shipping","value":"Shipping"},"cart_totals_tax":{"title":"Cart, Cart Totals, Tax","value":"Tax"},"cart_totals_discounts":{"title":"Cart, Cart Totals, Discounts","value":"Discounts"},"cart_totals_vat":{"title":"Cart, Cart Totals, VAT","value":"VAT"},"cart_totals_duty":{"title":"Cart, Cart Totals, Duty","value":"Duty"},"cart_totals_grand_total":{"title":"Cart, Cart Totals, Grand Total","value":"Grand Total"},"cart_totals_label":{"title":"Cart Totals Label","value":"Cart Totals"}}},"cart_login":{"label":"Cart - Cart Login","options":{"cart_login_title":{"title":"Cart, Login Title","value":"Returning Customer"},"cart_login_sub_title":{"title":"Cart, Login Sub Title","value":"To checkout with an existing account, please sign in below."},"cart_login_email_label":{"title":"Cart, Login, Email Label","value":"Email Address"},"cart_login_password_label":{"title":"Cart, Login, Password Label","value":"Password"},"cart_login_button":{"title":"Cart, Login Button","value":"SIGN IN"},"cart_login_forgot_password_link":{"title":"Cart, Login, Forgot Password Link","value":"Forgot Your Password?"},"cart_login_account_information_title":{"title":"Cart, Login Complete Title","value":"Account Information"},"cart_login_account_information_text":{"title":"Cart, Login Complete Text","value":"You are currently checking out as "},"cart_login_account_a_guest_text":{"title":"Cart, Login Complete, a guest","value":"a guest"},"cart_login_account_information_text2":{"title":"Cart, Login Complete Text Part 2","value":" to checkout with a different account."},"cart_login_account_information_logout_link":{"title":"Cart, Login Complete, Logout Link","value":"click here"}}},"cart_guest_checkout":{"label":"Cart - Guest Checkout","options":{"cart_guest_title":{"title":"Cart, Login, Guest Checkout Title","value":"Checkout Without an Account"},"cart_guest_sub_title":{"title":"Cart, Login, Guest Checkout Sub Title","value":"Not registered? Checkout without an existing account."},"cart_guest_message":{"title":"Cart, Login, Guest Checkout Message","value":"No account? Continue as a guest.<br\\/>You will have an opportunity to create an account later."},"cart_guest_button":{"title":"Cart, Login, Guest Checkout Button","value":"CONTINUE TO CHECKOUT"}}},"cart_subscription_guest_checkout":{"label":"Cart - Subscription No Account Checkout","options":{"cart_subscription_guest_title":{"title":"Cart, Login, Subscription No Account Title","value":"Create Account on Checkout"},"cart_subscription_guest_sub_title":{"title":"Cart, Login, Subscription No Account Sub Title","value":"Not registered? Checkout without an existing account."},"cart_subscription_guest_message":{"title":"Cart, Login, Subscription No Account Message","value":"No account? No problem.<br\\/>You will have an opportunity to create an account later."},"cart_subscription_guest_button":{"title":"Cart, Login, Guest Checkout Button","value":"CONTINUE TO CHECKOUT"}}},"cart_billing_information":{"label":"Cart - Billing Information","options":{"cart_billing_information_title":{"title":"Cart, Billing Information Title","value":"Shipping Information"},"cart_billing_information_first_name":{"title":"Cart, Billing, First Name Label","value":"First Name"},"cart_billing_information_last_name":{"title":"Cart, Billing, Last Name Label","value":"Last Name"},"cart_billing_information_delivery_date_time":{"title":"Cart, Billing, Delivery Date Time Label","value":"Delivery Date Time"},"cart_billing_information_pickup_date_time":{"title":"Cart, Billing, Pickup Date Time Label","value":"Pickup Date Time"},"cart_billing_information_address":{"title":"Cart, Billing, Address Label","value":"Address"},"cart_billing_information_address2":{"title":"Cart, Billing, Address 2 Label","value":"Address 2"},"cart_billing_information_city":{"title":"Cart, Billing, City Label","value":"City"},"cart_billing_information_state":{"title":"Cart, Billing, State Label","value":"State"},"cart_billing_information_select_state":{"title":"Cart, Billing, Default No State Selected","value":"Select a State"},"cart_billing_information_select_province":{"title":"Cart, Billing, Default No Province Selected","value":"Select a Province"},"cart_billing_information_select_county":{"title":"Cart, Billing, Default No County Selected","value":"Select a County"},"cart_billing_information_select_other":{"title":"Cart, Billing, Default None Selected","value":"Select One"},"cart_billing_information_country":{"title":"Cart, Billing, Country Label","value":"Country"},"cart_billing_information_select_country":{"title":"Cart, Billing, Country Menu, Default None Selected","value":"Select a Country"},"cart_billing_information_zip":{"title":"Cart, Billing, Zip Label","value":"Zip Code"},"cart_billing_information_phone":{"title":"Cart, Billing, Phone Label","value":"Phone"},"cart_billing_information_ship_to_this":{"title":"Cart, Billing, Use Billing Radio","value":"Ship to this address"},"cart_billing_information_ship_to_different":{"title":"Cart, Billing, Use Shipping Radio","value":"SHIP TO DIFFERENT ADDRESS"},"cart_billing_information_company_name":{"title":"Cart, Billing, Company Name Label","value":"Company Name"},"cart_billing_information_vat_registration_number":{"title":"Cart, Billing, VAT Registration Number Label","value":"VAT Registration Number"}}},"cart_shipping_information":{"label":"Cart - Shipping Information","options":{"cart_shipping_information_title":{"title":"Cart, Shipping Information Title","value":"Shipping Information"},"cart_shipping_information_first_name":{"title":"Cart, Shipping, First Name Label","value":"First Name"},"cart_shipping_information_last_name":{"title":"Cart, Shipping, Last Name Label","value":"Last Name"},"cart_shipping_information_address":{"title":"Cart, Shipping, Address Label","value":"Address"},"cart_shipping_information_address2":{"title":"Cart, Shipping, Address 2 Label","value":"Address 2"},"cart_shipping_information_city":{"title":"Cart, Shipping, City Label","value":"City"},"cart_shipping_information_state":{"title":"Cart, Shipping, State Label","value":"State"},"cart_shipping_information_select_state":{"title":"Cart, Shipping, Default No State Selected","value":"Select a State"},"cart_shipping_information_select_province":{"title":"Cart, Shipping, Default No Province Selected","value":"Select a Province"},"cart_shipping_information_select_county":{"title":"Cart, Shipping, Default No County Selected","value":"Select a County"},"cart_shipping_information_select_other":{"title":"Cart, Shipping, Default None Selected","value":"Select One"},"cart_shipping_information_country":{"title":"Cart, Shipping, Country Label","value":"Country"},"cart_shipping_information_select_country":{"title":"Cart, Shipping, Country Menu, Default None Selected","value":"Select a Country"},"cart_shipping_information_zip":{"title":"Cart, Shipping, Zip Label","value":"Zip Code"},"cart_shipping_information_phone":{"title":"Cart, Shipping, Phone Label","value":"Phone"},"cart_shipping_information_company_name":{"title":"Cart, Shipping, Company Name Label","value":"Company Name"}}},"cart_contact_information":{"label":"Cart - Contact Information","options":{"cart_contact_information_title":{"title":"Cart, Contact Information Title","value":"Contact Information"},"cart_contact_information_first_name":{"title":"Cart, Contact Information, First Name Label","value":"First Name"},"cart_contact_information_last_name":{"title":"Cart, Contact Information, Last Name Label","value":"Last Name"},"cart_contact_information_email":{"title":"Cart, Contact Information, Email Label","value":"Email"},"cart_contact_information_retype_email":{"title":"Cart, Contact Information, Retype Email Label","value":"Retype Email"},"cart_contact_information_create_account":{"title":"Cart, Contact Information, Create Account Label","value":"Create Account"},"cart_contact_information_password":{"title":"Cart, Contact Information, Password Label","value":"Password"},"cart_contact_information_retype_password":{"title":"Cart, Contact Information, Retype Password Label","value":"Retype Password"},"cart_contact_information_subscribe":{"title":"Cart, Contact Information, Subscribe Label","value":"Would you like to receive emails and updates from us?"},"cart_contact_information_continue_button":{"title":"Cart, Contact Information, Continue Button","value":"CONTINUE"},"cart_contact_information_continue_payment":{"title":"Cart, Contact Information, Continue to Payment","value":"CONTINUE TO PAYMENT"},"cart_contact_information_continue_shipping":{"title":"Cart, Contact Information, Continue to Shipping","value":"CONTINUE TO SHIPPING"}}},"cart_shipping_method":{"label":"Cart - Shipping Method","options":{"cart_shipping_method_title":{"title":"Cart, Shipping Method, Title","value":"Shipping Method"},"cart_shipping_method_continue_button":{"title":"Cart, Shipping Method Continue Button","value":"CONTINUE"},"cart_shipping_method_please_select_one":{"title":"Cart, Shipping please select one","value":"Please select a shipping method"},"cart_shipping_update_shipping":{"title":"Cart, Update Shipping","value":"Update Shipping"},"cart_shipping_no_rates_available":{"title":"Cart, No Shipping Rates Available","value":"There are no available shipping methods in your area. Contact us to complete your order."}}},"cart_address_review":{"label":"Cart - Address Review","options":{"cart_address_review_title":{"title":"Cart, Address Information Title","value":"Address Information"},"cart_address_review_billing_title":{"title":"Cart, Address Review, Billing Title","value":"Billing Address"},"cart_address_review_shipping_title":{"title":"Cart, Address Review, Shipping Title","value":"Shipping Address"},"cart_address_review_edit_link":{"title":"Cart, Address Review, Edit Link","value":"Edit Address or Shipping Options"},"cart_address_review_edit_link2":{"title":"Cart, Address Review, Edit Link","value":"Edit Address Information"}}},"cart_payment_information":{"label":"Cart - Payment Information","options":{"cart_payment_information_title":{"title":"Cart, Payment Information Title","value":"Payment Information"},"cart_payment_information_manual_payment":{"title":"Cart, Payment Information, Manual Payment Label","value":"Pay by Direct Deposit"},"cart_payment_information_affirm":{"title":"Cart, Payment Information, Affirm Payment Label","value":"Pay with Easy Monthly Payments"},"cart_payment_information_third_party":{"title":"Cart, Payment Information, Third Party Payment Label","value":"Pay Using"},"cart_payment_information_third_party_first":{"title":"Cart, Payment Information, Third Party Text, Part 1","value":"You will be redirected to"},"cart_payment_information_third_party_second":{"title":"Cart, Payment Information, Third Party Text, Part 2","value":"when you click submit order to complete your payment."},"cart_payment_information_credit_card":{"title":"Cart, Payment Information, Credit Card Payment Label","value":"Pay by Credit Card"},"cart_payment_information_payment_method":{"title":"Cart, Payment Information, Payment Method Label","value":"Payment Method"},"cart_payment_information_card_holder_name":{"title":"Cart, Payment Information, Card Holder Label","value":"Card Holder Name"},"cart_payment_information_card_number":{"title":"Cart, Payment Information, Card Number Label","value":"Card Number"},"cart_payment_information_expiration_date":{"title":"Cart, Payment Information, Expiration Date Label","value":"Expiration Date"},"cart_payment_information_security_code":{"title":"Cart, Payment Information, Security Code Label","value":"Security Code"},"cart_payment_information_checkout_text":{"title":"Cart, Payment Information, Checkout Text","value":"By submitting your order, you are agreeing to the [terms]terms and conditions[\\/terms] and [privacy]privacy policy[\\/privacy] of our website.  Once you click submit order, the checkout process is complete and no changes can be made to your order. Thank you for shopping with us!"},"cart_payment_information_review_checkout_text":{"title":"Cart, Payment Information, Review Checkout Text","value":"Before submitting your order and making it final, you will be given a chance to review and confirm the final totals. Please click the review order button when you are ready to review, confirm, and submit your order."},"cart_payment_information_review_order_button":{"title":"Cart, Payment Information, Review Order Button","value":"REVIEW ORDER"},"cart_payment_information_submit_order_button":{"title":"Cart, Payment Information, Submit Order Button","value":"SUBMIT ORDER"},"cart_payment_information_cancel_order_button":{"title":"Cart, Payment Information, Edit Order Button","value":"EDIT ORDER"},"cart_payment_information_order_notes_title":{"title":"Cart, Payment Information, Customer Notes Title","value":"ORDER NOTES"},"cart_payment_information_order_notes_message":{"title":"Cart, Payment Information, Customer Notes Message","value":"If you have any special requests for your order please enter them here."},"cart_payment_information_review_title":{"title":"Cart, Review Cart Title","value":"REVIEW YOUR CART"},"cart_payment_information_edit_cart_link":{"title":"Cart, edit cart items link","value":"edit cart items"},"cart_payment_information_edit_billing_link":{"title":"Cart, edit billing link","value":"edit billing address"},"cart_payment_information_edit_shipping_link":{"title":"Cart, edit shipping link","value":"edit shipping address"},"cart_payment_information_edit_shipping_method_link":{"title":"Cart, edit shipping link","value":"edit shipping method"},"cart_payment_review_totals_title":{"title":"Cart, Review Total Title","value":"REVIEW CART TOTALS"},"cart_payment_review_agree":{"title":"Cart, Review Agree Text","value":"I agree to the terms and conditions of this site."},"cart_change_payment_method":{"title":"Cart, Change Payment Method","value":"change payment method"}}},"cart_success":{"label":"Order Receipt","options":{"cart_payment_receipt_order_details_link":{"title":"Email Receipt, Link to Account","value":"View Order Details"},"cart_payment_receipt_subscription_details_link":{"title":"Email Receipt, Link to Subscription","value":"View Subscription Details"},"cart_payment_receipt_title":{"title":"Email Receipt Title","value":"Order Confirmation - Order Number"},"cart_payment_complete_line_1":{"title":"Cart, Payment Complete, Line 1","value":"Dear:"},"cart_payment_complete_line_2":{"title":"Cart, Payment Complete, Line 2","value":"Thank you for your order! Your Reference Number is:"},"cart_payment_complete_line_3":{"title":"Cart, Payment Complete, Line 3","value":"Below is a summary of order. You can check the status of your order and all the details by visiting our website and logging into your account."},"cart_payment_complete_line_4":{"title":"Cart, Payment Complete, Line 4","value":"Please keep this as a record of your order!"},"cart_payment_complete_line_5":{"title":"Cart, Payment Complete, Line 5","value":"Click Here to View Your Membership Content"},"cart_payment_complete_click_here":{"title":"Cart, Payment Complete, Click Here","value":"Click Here"},"cart_payment_complete_to_view_order":{"title":"Cart, Payment Complete, To View Order","value":"to View Your Order"},"cart_payment_complete_billing_label":{"title":"Cart, Payment Complete, Billing Label","value":"Billing Address"},"cart_payment_complete_shipping_label":{"title":"Cart, Payment Complete, Shipping Label","value":"Shipping Address"},"cart_payment_complete_details_header_1":{"title":"Cart, Payment Complete, Header 1","value":"Product"},"cart_payment_complete_details_header_2":{"title":"Cart, Payment Complete, Header 2","value":"Qty"},"cart_payment_complete_details_header_3":{"title":"Cart, Payment Complete, Header 3","value":"Price"},"cart_payment_complete_details_header_4":{"title":"Cart, Payment Complete, Header 4","value":"Ext Price"},"cart_payment_complete_order_totals_subtotal":{"title":"Cart, Payment Complete, Subtotal","value":"Subtotal"},"cart_payment_complete_order_totals_shipping":{"title":"Cart, Payment Complete, Shipping","value":"Shipping"},"cart_payment_complete_order_totals_tax":{"title":"Cart, Payment Complete, Tax","value":"Tax"},"cart_payment_complete_order_totals_discount":{"title":"Cart, Payment Complete, Discount","value":"Discount"},"cart_payment_complete_order_totals_vat":{"title":"Cart, Payment Complete, VAT","value":"VAT @"},"cart_payment_complete_order_totals_duty":{"title":"Cart, Payment Complete, Duty","value":"Duty"},"cart_payment_complete_order_totals_grand_total":{"title":"Cart, Payment Complete, Order Total","value":"Order Total"},"cart_payment_complete_bottom_line_1":{"title":"Cart, Payment Complete, Bottom Line 1","value":"Please double check your order when you receive it and let us know immediately if there are any concerns or issues. We always value your business and hope you enjoy your product."},"cart_payment_complete_bottom_line_2":{"title":"Cart, Payment Complete, Bottom Line 2","value":"Thank you very much!"},"cart_success_thank_you_title":{"title":"Cart, Payment Complete, Thank You Title","value":"Thank you for your order"},"cart_success_order_number_is":{"title":"Cart, Payment Complete, Your Order Number","value":"Order number is:"},"cart_success_will_receive_email":{"title":"Cart, Payment Complete, Will receive email","value":"You will receive an email confirmation shortly at"},"cart_success_print_receipt_text":{"title":"Cart, Payment Complete, Print Receipt Link Text","value":"Print Receipt"},"cart_success_save_order_text":{"title":"Cart, Payment Complete, Save Your Info","value":"Save your information for next time"},"cart_success_create_password":{"title":"Cart, Payment Complete, Create Password","value":"Create Password:"},"cart_success_verify_password":{"title":"Cart, Payment Complete, Verify Password","value":"Verify Password:"},"cart_success_password_hint":{"title":"Cart, Payment Complete, Password Hint","value":"(6-12 Characters)"},"cart_success_create_account":{"title":"Cart, Payment Complete, Create Account","value":"Create Account"},"cart_giftcard_receipt_title":{"title":"Cart, Gift Card Email Subject","value":"Gift Card Delivery"},"cart_giftcard_receipt_header":{"title":"Cart, Gift Card Header","value":"Your Gift Card"},"cart_giftcard_receipt_to":{"title":"Cart, Gift Card To","value":"To"},"cart_giftcard_receipt_from":{"title":"Cart, Gift Card From","value":"From"},"cart_giftcard_receipt_id":{"title":"Cart, Gift Card ID Label","value":"Your Gift Card ID"},"cart_giftcard_receipt_amount":{"title":"Cart, Gift Card Amount","value":"Gift Card Total"},"cart_giftcard_receipt_message":{"title":"Cart, Gift Card Message","value":"You can redeem your gift card during checkout at"},"cart_downloads_available":{"title":"Cart Success, Downloads Available","value":"Your order includes downloads and are available in your account."},"cart_downloads_unavailable":{"title":"Cart Success, Downloads Unavailable","value":"Your order includes downloads, but your payment has not yet been approved. Once payment has been approved your downloads will be available in your account."},"cart_downloads_click_to_go":{"title":"Cart Success, Downloads Click to go Text","value":"Click to go there now."},"cart_success_view_downloads":{"title":"Cart Success, View Downloads","value":"View Downloads"}}},"subscription_trial":{"label":"Subscription Trial Email","options":{"trial_message_1":{"title":"Subscription Trial Part 1","value":"Thank you for signing up! Your"},"trial_message_2":{"title":"Subscription Trial Part 2","value":"day free trial of"},"trial_message_3":{"title":"Subscription Trial Part 3","value":"has now started! If you have any questions or concerns during your trial, please contact us, we are happy to help."},"trial_message_4":{"title":"Subscription Trial Part 4","value":"We hope you stick with us, but if you decide this isn''t for you, you may cancel at any time prior to the end of your trial to prevent being charged. To cancel, log into your account, click manage subscriptions, view your subscription, and cancel on the details page."},"trial_message_link":{"title":"View Subscription Information","value":"View Subscription Information"},"subscription_trial_email_title":{"title":"Subscription Trial Email Title","value":"Your Trial Has Started"},"trial_ending_message_1":{"title":"Subscription Trial Ending Part 1","value":"Your"},"trial_ending_message_2":{"title":"Subscription Trial Part 2","value":"day free trial of"},"trial_ending_message_3":{"title":"Subscription Trial Part 3","value":"is ending in only 3 days! If you have any questions or concerns, please contact us! We are happy to help."},"trial_ending_message_4":{"title":"Subscription Trial Part 4","value":"We hope you stick with us, but if you decide this isn''t for you, you may cancel at any time prior to the end of your trial to prevent being charged. To cancel, log into your account, click manage subscriptions, view your subscription, and cancel on the details page."},"trial_ending_message_link":{"title":"View Subscription Information","value":"View Subscription Information"},"subscription_trial_ending_email_title":{"title":"Subscription Trial Email Title","value":"Your Trial is Ending in 3 Days"}}},"subscription_ended":{"label":"Subscription Ended Email","options":{"ended_message_1":{"title":"Subscription Ended Part 1","value":"Your subscription with us has either ended, been cancelled, or expired due to too many failed payments."},"ended_message_2":{"title":"Subscription Ended Part 2","value":"We are sorry to see you go and hope you return. If this subscription was cancelled by accident, you may click the link below to purchase a new subscription."},"ended_details":{"title":"Subscription Ended Details","value":"The following subscription has ended:"},"emded_message_link":{"title":"Start a New Subscription","value":"Start a New Subscription"},"subscription_ended_email_title":{"title":"Subscription Ended Email Title","value":"Your Subscription Has Ended"}}},"account_login":{"label":"Account - Login","options":{"account_login_title":{"title":"Account, Login Title","value":"Returning Customer"},"account_login_sub_title":{"title":"Account, Login Sub Title","value":"Sign in below to access your existing account."},"account_login_email_label":{"title":"Account, Login, Email Label","value":"Email Address"},"account_login_password_label":{"title":"Account, Login, Password Label","value":"Password"},"account_login_button":{"title":"Account, Login Button","value":"SIGN IN"},"account_login_forgot_password_link":{"title":"Account, Login, Forgot Password Link","value":"Forgot Your Password?"},"account_login_account_information_title":{"title":"Account, Login Complete Title","value":"Account Information"},"account_login_account_information_text":{"title":"Account, Login Complete Text","value":"You are currently checking out as Demo User. To checkout with a different account,"},"account_login_account_information_logout_link":{"title":"Account, Login Complete, Logout Link","value":"click here"},"account_new_user_title":{"title":"Account, Login, New User Title","value":"New User"},"account_new_user_sub_title":{"title":"Account, Login, New User Sub Title","value":"Not registered? Click the button below"},"account_new_user_message":{"title":"Account, Login, New User Message","value":"No account? Create an account to take full advantage of this website."},"account_new_user_button":{"title":"Account, Login, New User Button","value":"CREATE ACCOUNT"}}},"account_forgot_password":{"label":"Account - Forgot Password","options":{"account_forgot_password_title":{"title":"Account, Retrieve Password Title","value":"Retrieve Your Password"},"account_forgot_password_email_label":{"title":"Account, Retrieve Password Email Label","value":"Email"},"account_forgot_password_button":{"title":"Account, Retrieve Password Button","value":"RETRIEVE PASSWORD"}}},"account_validation_email":{"label":"Account - Validation Email","options":{"account_validation_email_message":{"title":"Account, Short Validate Email Message","value":"To activate your account, please click the link below."},"account_validation_email_title":{"title":"Account, Activate Your Account Title","value":"Please Activate Your Account"},"account_validation_email_link":{"title":"Account, Activate Your Account Link","value":"Click Here to Activate"}}},"account_register":{"label":"Account - Register","options":{"account_register_title":{"title":"Account, Register Title","value":"Create an Account"},"account_register_first_name":{"title":"Account, Register, First Name Label","value":"First Name"},"account_register_last_name":{"title":"Account, Register, Last Name Label","value":"Last Name"},"account_register_email":{"title":"Account, Register, Email Label","value":"Email Address"},"account_register_password":{"title":"Account, Register, Password Label","value":"Password"},"account_register_retype_password":{"title":"Account, Register, Re-type Password Label","value":"Re-type Password"},"account_register_subscribe":{"title":"Account, Register, Subscribe Text","value":"Would you like to subscribe to our email list?"},"account_register_button":{"title":"Account, Register Button","value":"REGISTER"},"account_billing_information_user_notes":{"title":"Account, Billing, User Notes","value":"Additional Information about your Registration"},"account_register_email_title":{"title":"Account, Register Email Title","value":"New EasyCart Registration"},"account_register_email_message":{"title":"Account, Register Email Message","value":"New Registration with Email Address:"}}},"account_navigation":{"label":"Account - Navigation","options":{"account_navigation_title":{"title":"Account, Navigation Title","value":"Account Navigation"},"account_navigation_basic_inforamtion":{"title":"Account, Navigation, Basic Information Link","value":"basic information"},"account_navigation_orders":{"title":"Account, Navigation, View Orders Link","value":"view orders"},"account_navigation_password":{"title":"Account, Navigation, Change Password Link","value":"change password"},"account_navigation_subscriptions":{"title":"Account, Navigation, Subscriptions Link","value":"manage subscriptions"},"account_navigation_payment_methods":{"title":"Account, Navigation, Payments Link","value":"manage payment methods"},"account_navigation_sign_out":{"title":"Account, Navigation, Sign Out Link","value":"sign out"},"account_navigation_dashboard":{"title":"Account, Navigation, Dashboard Link","value":"account dashboard"},"account_navigation_billing_information":{"title":"Account, Navigation, Billing Link","value":"billing information"},"account_navigation_shipping_information":{"title":"Account, Navigation, Shipping Link","value":"shipping information"}}},"account_dashboard":{"label":"Account - Dashboard","options":{"account_dashboard_recent_orders_title":{"title":"Account, Dashboard, Recent Orders Title","value":"Recent Orders"},"account_dashboard_recent_orders_none":{"title":"Account, Dashboard, Recent Order, No Orders","value":"No Orders Have Been Placed"},"account_dashboard_all_orders_linke":{"title":"Account, Dashboard, All Orders Link","value":"VIEW ALL ORDERS"},"account_dashboard_order_placed":{"title":"Account, Dashboard, Order Placed Label","value":"ORDER PLACED"},"account_dashboard_order_total":{"title":"Account, Dashboard, Total Label","value":"TOTAL"},"account_dashboard_order_ship_to":{"title":"Account, Dashboard, Ship To Label","value":"SHIP TO"},"account_dashboard_order_order_label":{"title":"Account, Dashboard, Order Label","value":"ORDER"},"account_dashboard_order_view_details":{"title":"Account, Dashboard, View Details Link","value":"View Details"},"account_dashboard_order_buy_item_again":{"title":"Account, Dashboard, Buy Item Again","value":"Buy Item Again"},"account_dashboard_preference_title":{"title":"Account, Dashboard, Preference Title","value":"Account Preferences"},"account_dashboard_email_title":{"title":"Account, Dashboard, Email Title","value":"Your Primary Email"},"account_dashboard_email_edit_link":{"title":"Account, Dashboard, Email Edit Link","value":"edit"},"account_dashboard_email_note":{"title":"Account, Dashboard, Email Note","value":"(This is used to log in)"},"account_dashboard_billing_title":{"title":"Account, Dashboard, Billing Title","value":"Billing Address"},"account_dashboard_billing_link":{"title":"Account, Dashboard, Billing Edit Link","value":"edit billing address"},"account_dashboard_shipping_title":{"title":"Account, Dashboard, Shipping Title","value":"Shipping Address"},"account_dashboard_shipping_link":{"title":"Account, Dashboard, Shipping Edit Link","value":"edit shipping address"}}},"account_personal_information":{"label":"Account - Edit Personal Information","options":{"account_personal_information_title":{"title":"Account, Personal Information Title","value":"Personal Information"},"account_personal_information_sub_title":{"title":"Account, Personal Information Sub Title","value":"Please note: Changes made to your account information, including shipping addresses, will only affect new orders. All previously placed orders will be sent to the address listed on the date of purchase."},"account_personal_information_first_name":{"title":"Account, Personal Information, First Name Label","value":"First Name"},"account_personal_information_last_name":{"title":"Account, Personal Information, Last Name Label","value":"Last Name"},"account_personal_information_email":{"title":"Account, Personal Information, Email Label","value":"Email Address"},"account_personal_information_subscribe":{"title":"Account, Personal Information, Subscribe Label","value":"Would you like to subscribe to our newsletter?"},"account_personal_information_update_button":{"title":"Account, Personal Information, Update Button","value":"UPDATE"},"account_personal_information_cancel_link":{"title":"Account, Personal Information, Cancel Link","value":"CANCEL"}}},"account_password":{"label":"Account - Edit Password","options":{"account_password_title":{"title":"Account, Password Title","value":"Edit Your Password"},"account_password_sub_title":{"title":"Account, Password Sub Title","value":"Please note: Once you change your password you will be required to use the new password each time you log into our site."},"account_password_current_password":{"title":"Account, Password, Current Password Label","value":"Current Password"},"account_password_new_password":{"title":"Account, Password, New Password Label","value":"New Password"},"account_password_retype_new_password":{"title":"Account, Password, Retype New Password Label","value":"Retype New Password"},"account_password_update_button":{"title":"Account, Password, Update Button","value":"UPDATE"},"account_password_cancel_button":{"title":"Account, Password, Cancel Link","value":"CANCEL"}}},"account_billing_information":{"label":"Account - Edit Billing Information","options":{"account_billing_information_title":{"title":"Account, Billing Information Title","value":"Billing Information"},"account_billing_information_first_name":{"title":"Account, Billing Information, First Name Label","value":"First Name"},"account_billing_information_last_name":{"title":"Account, Billing Information, Last Name Label","value":"Last Name"},"account_billing_information_address":{"title":"Account, Billing Information, Address Label","value":"Address"},"account_billing_information_address2":{"title":"Account, Billing Information, Address 2 Label","value":"Address 2"},"account_billing_information_city":{"title":"Account, Billing Information, City Label","value":"City"},"account_billing_information_state":{"title":"Account, Billing Information, State Label","value":"State"},"account_billing_information_default_no_state":{"title":"Account, Billing Information, No State Label","value":"Select a State"},"account_billing_information_zip":{"title":"Account, Billing Information, Zip Label","value":"Zip Code"},"account_billing_information_country":{"title":"Account, Billing Information, Country Label","value":"Country"},"account_billing_information_default_no_country":{"title":"Account, Billing Information, No Country Label","value":"Select a Country"},"account_billing_information_phone":{"title":"Account, Billing Information, Phone Label","value":"Phone"},"account_billing_information_update_button":{"title":"Account, Billing Information, Update Button","value":"UPDATE"},"account_billing_information_cancel":{"title":"Account, Billing Information, Cancel Link","value":"CANCEL"}}},"account_shipping_information":{"label":"Account - Edit Shipping Information","options":{"account_shipping_information_title":{"title":"Account, Shipping Information Title","value":"Shipping Information"},"account_shipping_information_first_name":{"title":"Account, Shipping Information, First Name Label","value":"First Name"},"account_shipping_information_last_name":{"title":"Account, Shipping Information, Last Name Label","value":"Last Name"},"account_shipping_information_address":{"title":"Account, Shipping Information, Address Label","value":"Address"},"account_shipping_information_address2":{"title":"Account, Shipping Information, Address 2 Label","value":"Address 2"},"account_shipping_information_city":{"title":"Account, Shipping Information, City Label","value":"City"},"account_shipping_information_state":{"title":"Account, Shipping Information, State Label","value":"State"},"account_shipping_information_default_no_state":{"title":"Account, Shipping Information, No State Label","value":"Select a State"},"account_shipping_information_zip":{"title":"Account, Shipping Information, Zip Label","value":"Zip Code"},"account_shipping_information_country":{"title":"Account, Shipping Information, Country Label","value":"Country"},"account_shipping_information_default_no_country":{"title":"Account, Shipping Information, No Country Label","value":"Select a Country"},"account_shipping_information_phone":{"title":"Account, Shipping Information, Phone Label","value":"Phone"},"account_shipping_information_update_button":{"title":"Account, Shipping Information, Update Button","value":"UPDATE"},"account_shipping_information_cancel":{"title":"Account, Shipping Information, Cancel Link","value":"CANCEL"}}},"account_orders":{"label":"Account - Orders","options":{"account_orders_title":{"title":"Account, Orders Title","value":"Your Order History"},"account_orders_header_1":{"title":"Account, Orders, Header 1","value":"ID"},"account_orders_header_2":{"title":"Account, Orders, Header 2","value":"Date"},"account_orders_header_3":{"title":"Account, Orders, Header 3","value":"Total"},"account_orders_header_4":{"title":"Account, Orders, Header 4","value":"Status"},"account_orders_view_order_button":{"title":"Account, Orders, View Order Button","value":"VIEW ORDER"}}},"account_forgot_password_email":{"label":"Account - Forgot Password Recovery Email","options":{"account_forgot_password_email_title":{"title":"Account, Password Recovery Email, Email Title","value":"Your New Password"},"account_forgot_password_email_dear":{"title":"Account, Password Recovery Email, Dear User","value":"Dear"},"account_forgot_password_email_your_new_password":{"title":"Account, Password Recovery Email, Your New Password","value":"Your new password is:"},"account_forgot_password_email_change_password":{"title":"Account, Password Recovery Email, Change Password Text","value":"Be sure to log into your account and change your password to something you can remember."},"account_forgot_password_email_thank_you":{"title":"Account, Password Recovery Email, Thank You Text","value":"Thank You Very much!"}}},"account_order_details":{"label":"Account - Order Details","options":{"account_orders_details_order_info_title":{"title":"Account, Orders Details, Order Information Title","value":"Order Information"},"account_orders_details_your_order_title":{"title":"Account, Orders Details, Your Order Title","value":"Your Order"},"account_orders_details_order_number":{"title":"Account, Orders Details, Order Number Label","value":"Order Number:"},"account_orders_details_order_date":{"title":"Account, Orders Details, Order Date Label","value":"Order Date:"},"account_orders_details_order_status":{"title":"Account, Orders Details, Order Status Label","value":"Order Status:"},"account_orders_details_order_tracking":{"title":"Account, Orders Details, Order Tracking Label","value":"Tracking Number:"},"account_orders_details_shipping_method":{"title":"Account, Orders Details, Shipping Method Label","value":"Shipping Method:"},"account_orders_details_coupon_code":{"title":"Account, Orders Details, Coupon Code Label","value":"Coupon Code:"},"account_orders_details_gift_card":{"title":"Account, Orders Details, Gift Card Label","value":"Gift Card:"},"account_orders_details_view_subscription":{"title":"Account, Orders Details, View Subscription","value":"View Subscription Details"},"account_orders_details_billing_label":{"title":"Account, Orders Details, Billing Label","value":"Billing Address"},"account_orders_details_shipping_label":{"title":"Account, Orders Details, Shipping Label","value":"Shipping Address"},"account_orders_details_payment_method":{"title":"Account, Orders Details, Payment Method","value":"Payment Method:"},"account_order_details_payment_method_manual":{"title":"Account, Order Details, Payment Method, Manual Bill Label","value":"Direct Deposit"},"account_orders_details_subtotal":{"title":"Account, Orders Details, Subtotal","value":"Sub Total:"},"account_orders_details_tax_total":{"title":"Account, Orders Details, Tax Total","value":"Tax Total:"},"account_orders_details_shipping_total":{"title":"Account, Orders Details, Shipping Total","value":"Shipping Total:"},"account_orders_details_discount_total":{"title":"Account, Orders Details, Discount Total","value":"Discount Total:"},"account_orders_details_duty_total":{"title":"Account, Orders Details, Duty Total","value":"Duty Total:"},"account_orders_details_vat_total":{"title":"Account, Orders Details, VAT Total","value":"VAT Total:"},"account_orders_details_refund_total":{"title":"Account, Orders Details, Refund Total","value":"Refunded Amount:"},"account_orders_details_grand_total":{"title":"Account, Orders Details, Grand Total","value":"Order Total:"},"account_orders_details_header_1":{"title":"Account, Orders Details, Header 1","value":"Product"},"account_orders_details_header_2":{"title":"Account, Orders Details, Header 2","value":"Options"},"account_orders_details_header_3":{"title":"Account, Orders Details, Header 3","value":"Price"},"account_orders_details_header_4":{"title":"Account, Orders Details, Header 4","value":"Quantity"},"account_orders_details_header_5":{"title":"Account, Orders Details, Header 5","value":"Total"},"account_orders_details_gift_message":{"title":"Account, Orders Details, Gift Card Message","value":"message: "},"account_orders_details_gift_from":{"title":"Account, Orders Details, Gift Card From","value":"from: "},"account_orders_details_gift_to":{"title":"Account, Orders Details, Gift Card To","value":"to: "},"account_orders_details_gift_card_id":{"title":"Account, Orders Details, Gift Card ID","value":"Gift Card Code: "},"account_orders_details_print_online":{"title":"Account, Orders Details, Print Online Link","value":"Print Online"},"account_orders_details_download":{"title":"Account, Orders Details, Download","value":"Download"},"account_orders_details_downloads_used":{"title":"Account, Orders Details, Downloads Used","value":"Downloads Used"},"account_orders_details_downloads_expire_time":{"title":"Account, Orders Details, Download Expiration","value":"Download Expires On:"},"account_orders_details_your_codes":{"title":"Account, Orders Details, Your Codes","value":"Your Code(s):"},"complete_payment":{"title":"Account, Orders Details, Complete Payment","value":"Complete Order Payment"},"order_status_status_not_found":{"title":"Account, Orders Status, Status Not Found","value":"Status Not Found"},"order_status_order_shipped":{"title":"Account, Orders Status, Order Shipped","value":"Order Shipped"},"order_status_order_confirmed":{"title":"Account, Orders Status, Order Confirmed","value":"Order Confirmed"},"order_status_order_on_hold":{"title":"Account, Orders Status, Order on Hold","value":"Order on Hold"},"order_status_order_started":{"title":"Account, Orders Status, Order Started","value":"Order Started"},"order_status_card_approved":{"title":"Account, Orders Status, Card Approved","value":"Card Approved"},"order_status_card_denied":{"title":"Account, Orders Status, Card Denied","value":"Card Denied"},"order_status_third_party_pending":{"title":"Account, Orders Status, Third Party Pending","value":"Third Party Pending"},"order_status_third_party_error":{"title":"Account, Orders Status, Third Party Error","value":"Third Party Error"},"order_status_third_party_approved":{"title":"Account, Orders Status, Third Party Approved","value":"Third Party Approved"},"order_status_ready_for_pickup":{"title":"Account, Orders Status, Ready for Pickup","value":"Ready for Pickup"},"order_status_pending_approval":{"title":"Account, Orders Status, Pending Approval","value":"Pending Approval"},"order_status_direct_deposit_pending":{"title":"Account, Orders Status, Direct Deposit Pending","value":"Direct Deposit Pending"},"order_status_direct_deposit_received":{"title":"Account, Orders Status, Direct Deposit Received","value":"Direct Deposit Received"},"order_status_refunded_order":{"title":"Account, Orders Status, Refunded Order","value":"Refunded Order"},"order_status_partial_refund":{"title":"Account, Orders Status, Partial Refund","value":"Partial Refund"},"order_status_order_picked_up":{"title":"Account, Orders Status, Order Picked UP","value":"Order Picked Up"},"no_order_found":{"title":"Account, No Order Found","value":"Order was not found or is not associated with the account you are currently logged into"},"return_to_dashboard":{"title":"Account, Return to Dashboard","value":"Return to Account Dashboard"}}},"account_subscriptions":{"label":"Account - Subscriptions","options":{"account_subscriptions_title":{"title":"Account, Subscriptions Title","value":"Your Active Subscriptions"},"account_canceled_subscriptions_title":{"title":"Account, Canceled Subscriptions Title","value":"Your Canceled Subscriptions"},"account_subscriptions_header_1":{"title":"Account, Subscriptions, Header 1","value":"Subscription Name"},"account_subscriptions_header_2":{"title":"Account, Subscriptions, Header 2","value":"Next Bill Date"},"account_subscriptions_header_3":{"title":"Account, Subscriptions, Header 3","value":"Last Charged"},"account_subscriptions_header_4":{"title":"Account, Subscriptions, Header 4","value":"Price\\/Cycle"},"account_subscriptions_view_subscription_button":{"title":"Account, Subscriptions, View Details Button","value":"VIEW DETAILS"},"account_subscriptions_none_found":{"title":"Account, Subscriptions, None Found","value":"There are no subscriptions associated with your account."},"update_subscription_subtitle":{"title":"Account, Subscription Details, Credit Card Note","value":"You are not required to update your credit card information."},"save_changes_button":{"title":"Account, Subscription Details, Save Changes Button","value":"SAVE CHANGES"},"cancel_subscription_button":{"title":"Account, Subscription Details, Cancel Subscription Button","value":"CANCEL SUBSCRIPTION"},"cancel_subscription_confirm_text":{"title":"Account, Subscription Details, Cancel Subscription Button","value":"Are you sure you would like to cancel this subscription?"},"subscription_details_next_billing":{"title":"Account, Subscription Details, Next Billing Date","value":"Next Billing Date"},"subscription_details_last_payment":{"title":"Account, Subscription Details, Last Payment Date","value":"Last Payment Date"},"subscription_details_current_billing":{"title":"Account, Subscription Details, Current Billing Title","value":"Current Billing Method"},"subscription_details_notice":{"title":"Account, Subscription Details, User Notice","value":"*NOTICE: any changes to the billing method or subscription plan will take effect in the next billing cycle. Pricing changes will be prorated beginning immediately and will be reflected on your next bill."},"subscription_details_past_payments":{"title":"Account, Subscription Details, Past Payments Title","value":"Past Payments"}}},"ec_errors":{"label":"Store Error Text","options":{"login_failed":{"title":"Account, User Login Failed","value":"The username or password you entered is incorrect."},"register_email_error":{"title":"Account Registration Email In Use Error","value":"The email you have entered already has an account."},"no_reset_email_found":{"title":"No Email Found Error","value":"The email address you entered was not found."},"personal_information_update_error":{"title":"Account, updating personal info error","value":"An error occurred while updating your personal information."},"password_wrong_current":{"title":"Wrong password entered Error","value":"The current password you entered did not match your account password."},"password_no_match":{"title":"Passwords do not match Error","value":"The new password and the retype new password values did not match."},"password_update_error":{"title":"password update error","value":"An error occurred while updating your password."},"billing_information_error":{"title":"Account, billing information update error","value":"An error occurred while updating your billing information."},"shipping_information_error":{"title":"Account, shipping information update error","value":"An error occurred while updating your shipping information."},"email_exists_error":{"title":"Cart, create account on checkout, email exists","value":"This email already has an account. Please login or use a different email address to continue."},"3dsecure_failed":{"title":"Cart, Optional, Error message when 3D Secure Fails, Gateway Dependent","value":"Your payment could not completed because the 3D Secure method failed. Please try your payment again. If your problem persists, please contact us to have the issue resolved."},"manualbill_failed":{"title":"Cart, Optional, Manual billing failed, Gateway Dependent","value":"Your payment could not completed, manual billing failed. Please try again."},"thirdparty_failed":{"title":"Cart, Optional, Third Party Failed, Gateway Dependent","value":"Your payment could not completed, third party failed. Please try again."},"payment_failed":{"title":"Cart, Optional, Credit Card Failed, Gateway Dependent","value":"Your payment could not completed because your credit card could not be verified. Please check your credit card information and try again. If your problem persists, please contact us to complete payment."},"already_subscribed":{"title":"Cart, Optional, Already Subscribed, Gateway Dependent","value":"You have already subscribed to this product. Please visit your account to manage your active subscriptions."},"subscription_not_found":{"title":"Account, Subscription Not Found Error","value":"An unknown error occurred in which the subscription you are trying to purchase was not found in our system. Please try again and contact us if you have continued difficulties."},"invalid_address":{"title":"Cart, Invalid Live Shipping Address","value":"Something is wrong with the address you have entered. Please check your city, state, zip, and country to make sure the values are correct."},"activation_error":{"title":"Account, Activation Error","value":"An error has occurred while attempting to activate your account. Please contact us to have the issue resolved."},"not_activated":{"title":"Account, Not Activated","value":"The account you are attempting to access has not been activated. Please activate your account through the activation email sent when you signed up for an account or contact us if you are having problems."},"subscription_update_failed":{"title":"Account, Subscription Update Error","value":"There was a problem while updating your subscription, please try again or contact us if you continue to have problems."},"subscription_cancel_failed":{"title":"Account, Subscription Cancel Error","value":"There was a problem while cancelling your subscription, please try again or contact us if you continue to have problems."},"user_insert_error":{"title":"Account, Subscription User Insert Error","value":"There was a problem creating your user account in our subscription system. Please contact us to complete your transaction."},"subscription_added_failed":{"title":"Account, Subscription Plan Added Error","value":"There was an internal problem while creating this product in our subscription service. Please contact us to complete your transaction."},"subscription_failed":{"title":"Account, Subscription Creation Error","value":"There was a problem while creating your subscription in our system. Please contact us to complete your transaction."},"nets_processing":{"title":"Account, Nets Error Level Authorize","value":"While completing your order at Nets Netaxept, an error occurred. You may complete your order at any time by clicking the button below."},"nets_processing_payment":{"title":"Account, Nets Error Level Capture","value":"Your payment has been authorized at Nets Netaxept, but there seems to have been an issue in our system. Please contact us to complete your order."},"subscription_payment_failed_title":{"title":"Cart, Subscription Payment Failed","value":"Payment Failed"},"subscription_payment_failed_text":{"title":"Cart, Subscription Payment Failed Text","value":"The payment has failed for one of your subscriptions. Please click the link below to update your credit card information."},"subscription_payment_failed_link":{"title":"Cart, Subscription Payment Failed Link","value":"Click to Update Billing"},"minquantity":{"title":"Store, Minimum Quantity Error","value":"This product requires a minimum purchase quantity."},"missing_gift_card_options":{"title":"Store, Missing Gift Card Options","value":"Missing Gift Card Options"},"missing_inquiry_options":{"title":"Store, Missing Inquiry Options","value":"Missing Inquiry Options"},"session_expired":{"title":"Store, Session Expired","value":"Your session has expired, please enter your checkout information again to complete your order."},"invalid_vat_number":{"title":"Cart, Invalid VAT Number Entered","value":"Your VAT registration number is invalid. Please leave this field empty or correct the error to continue."}}},"ec_success":{"label":"Store Success Text","options":{"personal_information_updated":{"title":"Account Personal Information Update Success","value":"Your personal information was updated successfully."},"password_updated":{"title":"Account Personal Information Update Success","value":"Your password was updated successfully."},"billing_information_updated":{"title":"Account Billing Update Success","value":"Your billing information was updated successfully."},"shipping_information_updated":{"title":"Account Shipping Update Success","value":"Your shipping information was updated successfully."},"reset_email_sent":{"title":"Account Email Sent Success","value":"Your new password has been sent to your email address."},"cart_account_created":{"title":"Cart, Create Account, Account Created Success Text","value":"Your account has been created, all orders will now be associated with your new account."},"cart_account_free_order":{"title":"Cart, Order Type, Gift Card, Free Order","value":"Free Order"},"store_added_to_cart":{"title":"Store, Added Item to Cart","value":"You have successfully added [prod_title] to your cart."},"activation_success":{"title":"Account, Activation Success","value":"You have successfully activated your account"},"validation_required":{"title":"Account, Validation Required","value":"You have successfully created an account, but your email needs to be validated. An email has been sent to your account with instructions on how to complete the registration process. Contact us if you have any questions."},"subscription_updated":{"title":"Account, Subscription Updated","value":"Your subscription has been updated successfully."},"subscription_canceled":{"title":"Account, Subscription Canceled","value":"Your subscription has been canceled."},"inquiry_sent":{"title":"Store, Inquiry Sent","value":"Your product inquiry has been sent successfully."},"add_to_cart_success":{"title":"Store, Added to Cart","value":"Successfully Added to your Shopping Cart"},"adding_to_cart":{"title":"Store, Adding to Cart","value":"Adding to Cart..."}}},"ec_shipping_email":{"label":"Shipping Emailer","options":{"shipping_email_title":{"title":"Shipping Email Title","value":"Shipping Confirmation - Order Number"},"shipping_dear":{"title":"Shipping Dear Customer","value":"Dear"},"shipping_subtitle1":{"title":"Order Shipped First Half","value":"Your recent order  with the number"},"shipping_subtitle2":{"title":"Order Shipped Second Half","value":"has been shipped! You should be receiving it within a short time period."},"shipping_description":{"title":"Order Shipped Description","value":"You may check the status of your order by visiting your carrier''s website and using the following tracking number."},"shipping_carrier":{"title":"Order Shipped Carrier","value":"Package Carrier:"},"shipping_tracking":{"title":"Order Shipped Tracking Number","value":"Package Tracking Number:"},"shipping_billing_label":{"title":"Billing Label","value":"Billing Address"},"shipping_shipping_label":{"title":"Shipping Label","value":"Shipping Address"},"shipping_product":{"title":"Product Label","value":"Product"},"shipping_quantity":{"title":"Quantity Label","value":"Qty"},"shipping_unit_price":{"title":"Unit Price Label","value":"Unit Price"},"shipping_total_price":{"title":"Total Price Label","value":"Ext Price"},"shipping_final_note1":{"title":"Shipping Emailer Final Note Line 1","value":"Please double check your order when you receive it and let us know immediately if there are any concerns or issues. We always value your business and hope you enjoy your product."},"shipping_final_note2":{"title":"Shipping Emailer Final Note Line 2","value":"Thank you very much!"}}},"ec_login_widget":{"label":"Login Widget","options":{"hello_text":{"title":"Hello","value":"Hello"},"dashboard_text":{"title":"Dashboard","value":"Dashboard"},"order_history_text":{"title":"Order History","value":"Order History"},"billing_info_text":{"title":"Billing Information","value":"Billing Information"},"shipping_info_text":{"title":"Shipping Information","value":"Shipping Information"},"change_password_text":{"title":"Change Password","value":"Change Password"},"sign_out_text":{"title":"Sign Out","value":"Sign Out"}}},"ec_minicart_widget":{"label":"Minicart Widget","options":{"subtotal_text":{"title":"Minicart Widget, Subtotal Text","value":"SUBTOTAL"},"checkout_button_text":{"title":"Minicart Widget, Checkout Button Text","value":"CHECKOUT"}}},"ec_pricepoint_widget":{"label":"Price Point Widget","options":{"less_than":{"title":"Less Than Text","value":"Less Than"},"greater_than":{"title":"Greater Than Text","value":"Greater Than"}}},"ec_newsletter_popup":{"label":"Newsletter Popup","options":{"signup_form_title":{"title":"Signup Form Title","value":"Newsletter Signup"},"signup_form_subtitle":{"title":"Signup Form Subtitle","value":"Sign up now and never miss a thing!"},"signup_form_email_placeholder":{"title":"Signup Form Email Placeholder","value":"email address"},"signup_form_name_placeholder":{"title":"Signup Form Name Placeholder","value":"your name"},"signup_form_button_text":{"title":"Signup Form Button","value":"SUBMIT"},"signup_form_success_title":{"title":"Signup Success Title","value":"You''re Signed Up!"},"signup_form_success_subtitle":{"title":"Signup Success Subtitle","value":"Now that you are signed up, we will send you exclusive offers periodically."}}},"ec_abandoned_cart_email":{"label":"Abandoned Cart Email","options":{"email_title":{"title":"Email Title","value":"You Left Items in Your Cart"},"something_in_cart":{"title":"Main Title, Something in Cart","value":"THERE''S SOMETHING IN YOUR CART."},"complete_question":{"title":"Informational Text","value":"Would you like to complete your purchase?"},"complete_checkout":{"title":"Complete Checkout Button","value":"Complete Checkout"}}},"language_code":{"label":"Language Code","options":{"code":{"title":"Do Not Change","value":"EN"}}}}}}', 'yes');
INSERT INTO `ops_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(593, 'ec_option_use_seperate_language_forms', '1', 'yes'),
(594, 'ec_option_base_theme', '0', 'yes'),
(595, 'ec_option_base_layout', '0', 'yes'),
(596, 'ec_option_latest_theme', 'base-responsive-v3', 'yes'),
(597, 'ec_option_latest_layout', 'base-responsive-v3', 'yes'),
(598, 'ec_option_caching_on', '1', 'yes'),
(599, 'ec_option_cache_update_period', '2', 'yes'),
(600, 'ec_option_cached_date', '0', 'yes'),
(601, 'ec_option_custom_css', '', 'yes'),
(602, 'ec_option_css_replacements', 'main_color=#242424,second_color=#6b6b6b,third_color=#adadad,title_color=#0f0f0f,text_color=#141414,link_color=#242424,link_hover_color=#121212,sale_color=#900,backdrop_color=#333,content_bg=#FFF,error_text=#900,error_color=#F1D9D9,error_color2=#FF0606,success_text=#333,success_color=#E6FFE6,success_color2=#6FFF47', 'yes'),
(603, 'ec_option_font_replacements', 'title_font=Arial, Helvetica, sans-serif:::subtitle_font=Arial, Helvetica, sans-serif:::content_font=Arial, Helvetica, sans-serif', 'yes'),
(604, 'ec_option_responsive_sizes', 'size_level1_high=479:::size_level2_low=480:::size_level2_high=767:::size_level3_low=768:::size_level3_high=960:::size_level4_low=961:::size_level4_high=1300:::size_level5_low=1301', 'yes'),
(605, 'ec_option_details_main_color', '#222222', 'yes'),
(606, 'ec_option_details_second_color', '#666666', 'yes'),
(607, 'ec_option_use_dark_bg', '0', 'yes'),
(608, 'ec_option_default_product_type', '1', 'yes'),
(609, 'ec_option_default_product_image_hover_type', '3', 'yes'),
(610, 'ec_option_default_product_image_effect_type', 'none', 'yes'),
(611, 'ec_option_default_quick_view', '0', 'yes'),
(612, 'ec_option_default_desktop_columns', '3', 'yes'),
(613, 'ec_option_default_desktop_image_height', '310px', 'yes'),
(614, 'ec_option_default_laptop_columns', '3', 'yes'),
(615, 'ec_option_default_laptop_image_height', '310px', 'yes'),
(616, 'ec_option_default_tablet_wide_columns', '2', 'yes'),
(617, 'ec_option_default_tablet_wide_image_height', '310px', 'yes'),
(618, 'ec_option_default_tablet_columns', '2', 'yes'),
(619, 'ec_option_default_tablet_image_height', '380px', 'yes'),
(620, 'ec_option_default_smartphone_columns', '1', 'yes'),
(621, 'ec_option_default_smartphone_image_height', '270px', 'yes'),
(622, 'ec_option_details_columns_desktop', '2', 'yes'),
(623, 'ec_option_details_columns_laptop', '2', 'yes'),
(624, 'ec_option_details_columns_tablet_wide', '1', 'yes'),
(625, 'ec_option_details_columns_tablet', '1', 'yes'),
(626, 'ec_option_details_columns_smartphone', '1', 'yes'),
(627, 'ec_option_cart_columns_desktop', '2', 'yes'),
(628, 'ec_option_cart_columns_laptop', '2', 'yes'),
(629, 'ec_option_cart_columns_tablet_wide', '1', 'yes'),
(630, 'ec_option_cart_columns_tablet', '1', 'yes'),
(631, 'ec_option_cart_columns_smartphone', '1', 'yes'),
(632, 'ec_option_email_logo', '', 'yes'),
(633, 'ec_option_use_facebook_icon', '1', 'yes'),
(634, 'ec_option_use_twitter_icon', '1', 'yes'),
(635, 'ec_option_use_delicious_icon', '1', 'yes'),
(636, 'ec_option_use_myspace_icon', '1', 'yes'),
(637, 'ec_option_use_linkedin_icon', '1', 'yes'),
(638, 'ec_option_use_email_icon', '1', 'yes'),
(639, 'ec_option_use_digg_icon', '1', 'yes'),
(640, 'ec_option_use_googleplus_icon', '1', 'yes'),
(641, 'ec_option_use_pinterest_icon', '1', 'yes'),
(642, 'ec_option_checklist_state', '', 'yes'),
(643, 'ec_option_checklist_currency', '', 'yes'),
(644, 'ec_option_checklist_default_payment', '', 'yes'),
(645, 'ec_option_checklist_guest', '', 'yes'),
(646, 'ec_option_checklist_shipping_enabled', '', 'yes'),
(647, 'ec_option_checklist_checkout_notes', '', 'yes'),
(648, 'ec_option_checklist_billing_registration', '', 'yes'),
(649, 'ec_option_checklist_google_analytics', '', 'yes'),
(650, 'ec_option_checklist_manual_billing', '', 'yes'),
(651, 'ec_option_checklist_third_party_complete', '', 'yes'),
(652, 'ec_option_checklist_third_party', '', 'yes'),
(653, 'ec_option_checklist_has_paypal', '', 'yes'),
(654, 'ec_option_checklist_has_skrill', '', 'yes'),
(655, 'ec_option_checklist_has_paymentexpress_thirdparty', '', 'yes'),
(656, 'ec_option_checklist_has_realex_thirdparty', '', 'yes'),
(657, 'ec_option_checklist_credit_cart_complete', '', 'yes'),
(658, 'ec_option_checklist_credit_card', '', 'yes'),
(659, 'ec_option_checklist_credit_card_location', '', 'yes'),
(660, 'ec_option_checklist_tax_complete', '', 'yes'),
(661, 'ec_option_checklist_tax_choice', '', 'yes'),
(662, 'ec_option_checklist_shipping_complete', '', 'yes'),
(663, 'ec_option_checklist_shipping_choice', '', 'yes'),
(664, 'ec_checklist_shipping_use_ups', '', 'yes'),
(665, 'ec_checklist_shipping_use_usps', '', 'yes'),
(666, 'ec_checklist_shipping_use_fedex', '', 'yes'),
(667, 'ec_checklist_shipping_use_auspost', '', 'yes'),
(668, 'ec_checklist_shipping_use_dhl', '', 'yes'),
(669, 'ec_option_checklist_language_complete', '', 'yes'),
(670, 'ec_option_checklist_theme_complete', '', 'yes'),
(671, 'ec_option_checklist_colorization_complete', '', 'yes'),
(672, 'ec_option_checklist_logo_added_complete', '', 'yes'),
(673, 'ec_option_checklist_admin_embedded_complete', '', 'yes'),
(674, 'ec_option_checklist_admin_consoles_complete', '', 'yes'),
(675, 'ec_option_checklist_page', '', 'yes'),
(676, 'ec_option_quickbooks_user', '', 'yes'),
(677, 'ec_option_quickbooks_password', '', 'yes'),
(678, 'ec_option_wpoptions_version', '4_0_37', 'yes'),
(679, 'ec_option_db_insert_v4', '1', 'yes'),
(680, 'ec_option_db_new_version', '57', 'yes'),
(681, 'ec_option_data_folders_installed', '4_0_37', 'yes'),
(682, 'widget_ec_categorywidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(683, 'widget_ec_cartwidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(684, 'widget_ec_colorwidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(685, 'widget_ec_currencywidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(686, 'widget_ec_donationwidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(687, 'widget_ec_groupwidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(688, 'widget_ec_languagewidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(689, 'widget_ec_loginwidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(690, 'widget_ec_manufacturerwidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(691, 'widget_ec_menuwidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(692, 'widget_ec_newsletterwidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(693, 'widget_ec_pricepointwidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(694, 'widget_ec_productwidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(695, 'widget_ec_searchwidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(696, 'widget_ec_specialswidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(697, 'ec_option_v3_fix', '1', 'yes'),
(698, 'ec_option_published_check', '4_0_37', 'yes'),
(700, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:"auto_add";a:0:{}}', 'yes'),
(705, 'WPLANG', '', 'yes'),
(706, 'new_admin_email', 'info@ozproductionservices.com', 'yes'),
(748, 'fed_plugin_version', '1.2.11', 'yes'),
(749, 'fed_admin_login', 'a:3:{s:8:"register";a:3:{s:4:"role";a:1:{s:6:"author";s:6:"Enable";}s:4:"name";s:9:"User Role";s:8:"position";s:3:"999";}s:8:"settings";a:7:{s:13:"fed_login_url";i:34;s:16:"fed_register_url";i:36;s:23:"fed_forgot_password_url";i:0;s:22:"fed_redirect_login_url";i:38;s:25:"fed_redirect_register_url";i:0;s:23:"fed_redirect_logout_url";i:34;s:17:"fed_dashboard_url";i:38;}s:11:"restrict_wp";a:1:{s:4:"role";a:2:{s:6:"author";s:6:"Enable";s:11:"testimonial";s:6:"Enable";}}}', 'yes'),
(750, 'fed_admin_settings_post', 'a:3:{s:11:"permissions";a:2:{s:15:"post_permission";a:6:{s:13:"administrator";s:6:"Enable";s:6:"editor";s:6:"Enable";s:6:"author";s:6:"Enable";s:11:"contributor";s:6:"Enable";s:10:"subscriber";s:6:"Enable";s:11:"testimonial";s:6:"Enable";}s:21:"fed_upload_permission";a:6:{s:13:"administrator";s:6:"Enable";s:6:"editor";s:6:"Enable";s:6:"author";s:6:"Enable";s:11:"contributor";s:6:"Enable";s:10:"subscriber";s:6:"Enable";s:11:"testimonial";s:6:"Enable";}}s:8:"settings";a:1:{s:15:"fed_post_status";s:7:"publish";}s:4:"menu";a:3:{s:11:"rename_post";s:4:"Post";s:13:"post_position";i:2;s:14:"post_menu_icon";s:15:"fa fa-file-text";}}', 'yes'),
(751, 'fed_admin_settings_upl', 'a:1:{s:8:"settings";a:3:{s:26:"fed_upl_change_profile_pic";s:0:"";s:20:"fed_upl_disable_desc";s:2:"no";s:22:"fed_upl_no_recent_post";s:1:"5";}}', 'yes'),
(752, 'fed_cp_admin_settings', 'a:2:{s:4:"post";a:5:{s:8:"settings";a:4:{s:15:"fed_post_status";s:7:"publish";s:17:"disable_post_edit";s:2:"no";s:19:"disable_post_delete";s:2:"no";s:17:"disable_post_view";s:2:"no";}s:11:"permissions";a:1:{s:15:"post_permission";a:3:{s:13:"administrator";s:6:"Enable";s:6:"author";s:6:"Enable";s:11:"testimonial";s:6:"Enable";}}s:4:"menu";a:3:{s:11:"rename_post";s:4:"Post";s:13:"post_position";s:1:"2";s:14:"post_menu_icon";s:15:"fa fa-file-text";}s:9:"dashboard";a:0:{}s:10:"taxonomies";a:0:{}}s:11:"testimonial";a:5:{s:8:"settings";a:4:{s:15:"fed_post_status";s:7:"pending";s:17:"disable_post_edit";s:2:"no";s:19:"disable_post_delete";s:2:"no";s:17:"disable_post_view";s:2:"no";}s:11:"permissions";a:1:{s:15:"post_permission";a:2:{s:6:"author";s:6:"Enable";s:11:"testimonial";s:6:"Enable";}}s:4:"menu";a:3:{s:11:"rename_post";s:11:"Testimonial";s:13:"post_position";s:1:"2";s:14:"post_menu_icon";s:19:"fa fa-align-justify";}s:9:"dashboard";a:1:{s:14:"allow_comments";s:6:"Enable";}s:10:"taxonomies";a:0:{}}}', 'yes'),
(755, 'fed_cp_custom_posts', 'a:1:{s:11:"testimonial";a:49:{s:4:"slug";s:11:"testimonial";s:5:"label";s:12:"testimonials";s:13:"singular_name";s:11:"Testimonial";s:9:"menu_icon";s:18:"dashicons-book-alt";s:4:"name";s:12:"Testimonials";s:9:"menu_name";s:12:"Testimonials";s:14:"name_admin_bar";s:0:"";s:8:"archives";s:0:"";s:10:"attributes";s:0:"";s:17:"parent_item_colon";s:0:"";s:9:"all_items";s:0:"";s:12:"add_new_item";s:0:"";s:7:"add_new";s:0:"";s:8:"new_item";s:0:"";s:9:"edit_item";s:0:"";s:9:"view_item";s:0:"";s:10:"view_items";s:0:"";s:12:"search_items";s:0:"";s:9:"not_found";s:0:"";s:18:"not_found_in_trash";s:0:"";s:14:"featured_image";s:0:"";s:18:"set_featured_image";s:0:"";s:21:"remove_featured_image";s:0:"";s:18:"use_featured_image";s:0:"";s:16:"insert_into_item";s:0:"";s:21:"uploaded_to_this_item";s:0:"";s:10:"items_list";s:0:"";s:21:"items_list_navigation";s:0:"";s:17:"filter_items_list";s:0:"";s:11:"description";s:0:"";s:13:"menu_position";s:0:"";s:15:"capability_type";s:4:"post";s:12:"hierarchical";s:5:"false";s:6:"public";s:4:"true";s:7:"show_ui";s:4:"true";s:12:"show_in_menu";s:4:"true";s:17:"show_in_admin_bar";s:4:"true";s:17:"show_in_nav_menus";s:4:"true";s:10:"can_export";s:4:"true";s:11:"has_archive";s:4:"true";s:19:"exclude_from_search";s:5:"false";s:18:"publicly_queryable";s:4:"true";s:9:"query_var";s:4:"true";s:16:"delete_with_user";s:5:"false";s:12:"show_in_rest";s:4:"true";s:9:"rest_base";s:0:"";s:7:"rewrite";s:4:"true";s:12:"rewrite_slug";s:0:"";s:8:"supports";a:6:{s:5:"title";s:7:"Enabled";s:6:"editor";s:6:"Enable";s:6:"author";s:6:"Enable";s:13:"custom-fields";s:6:"Enable";s:15:"page-attributes";s:6:"Enable";s:12:"post-formats";s:6:"Enable";}}}', 'yes'),
(756, 'fed_admin_message_notification', 'remove', 'yes'),
(774, '_site_transient_timeout_browser_bd6a9fe252598f30ffd1edb8511fd2b1', '1525935065', 'no'),
(775, '_site_transient_browser_bd6a9fe252598f30ffd1edb8511fd2b1', 'a:10:{s:4:"name";s:6:"Chrome";s:7:"version";s:13:"66.0.3359.139";s:8:"platform";s:7:"Windows";s:10:"update_url";s:29:"https://www.google.com/chrome";s:7:"img_src";s:43:"http://s.w.org/images/browsers/chrome.png?1";s:11:"img_src_ssl";s:44:"https://s.w.org/images/browsers/chrome.png?1";s:15:"current_version";s:2:"18";s:7:"upgrade";b:0;s:8:"insecure";b:0;s:6:"mobile";b:0;}', 'no'),
(790, 'fed_admin_settings_user', 'a:1:{s:4:"user";a:1:{s:17:"upload_permission";a:1:{s:11:"testimonial";s:6:"Enable";}}}', 'yes'),
(791, '_site_transient_timeout_browser_5745ee6112ac292f8b72fc01594d872e', '1525936296', 'no'),
(792, '_site_transient_browser_5745ee6112ac292f8b72fc01594d872e', 'a:10:{s:4:"name";s:7:"Firefox";s:7:"version";s:4:"59.0";s:8:"platform";s:7:"Windows";s:10:"update_url";s:24:"https://www.firefox.com/";s:7:"img_src";s:44:"http://s.w.org/images/browsers/firefox.png?1";s:11:"img_src_ssl";s:45:"https://s.w.org/images/browsers/firefox.png?1";s:15:"current_version";s:2:"56";s:7:"upgrade";b:0;s:8:"insecure";b:0;s:6:"mobile";b:0;}', 'no'),
(802, 'theme_mods_twentyfifteen', 'a:3:{i:0;b:0;s:18:"nav_menu_locations";a:0:{}s:16:"sidebars_widgets";a:2:{s:4:"time";i:1525332464;s:4:"data";a:2:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}}}}', 'yes'),
(806, 'acf_version', '4.4.12', 'yes'),
(808, 'wpcf7', 'a:2:{s:7:"version";s:5:"4.9.2";s:13:"bulk_validate";a:4:{s:9:"timestamp";i:1525350179;s:7:"version";s:5:"4.9.2";s:11:"count_valid";i:1;s:13:"count_invalid";i:0;}}', 'yes'),
(809, 'cfdb7_view_install_date', '2018-05-03 12:23:04', 'yes'),
(812, 'aioseop_options', 'a:87:{s:16:"aiosp_home_title";s:0:"";s:22:"aiosp_home_description";s:0:"";s:20:"aiosp_togglekeywords";s:1:"1";s:19:"aiosp_home_keywords";s:0:"";s:26:"aiosp_use_static_home_info";s:1:"0";s:9:"aiosp_can";s:2:"on";s:30:"aiosp_no_paged_canonical_links";s:0:"";s:31:"aiosp_customize_canonical_links";s:0:"";s:20:"aiosp_rewrite_titles";s:1:"1";s:20:"aiosp_force_rewrites";s:1:"1";s:24:"aiosp_use_original_title";s:1:"0";s:28:"aiosp_home_page_title_format";s:12:"%page_title%";s:23:"aiosp_page_title_format";s:27:"%page_title% | %blog_title%";s:23:"aiosp_post_title_format";s:27:"%post_title% | %blog_title%";s:27:"aiosp_category_title_format";s:31:"%category_title% | %blog_title%";s:26:"aiosp_archive_title_format";s:30:"%archive_title% | %blog_title%";s:23:"aiosp_date_title_format";s:21:"%date% | %blog_title%";s:25:"aiosp_author_title_format";s:23:"%author% | %blog_title%";s:22:"aiosp_tag_title_format";s:20:"%tag% | %blog_title%";s:25:"aiosp_search_title_format";s:23:"%search% | %blog_title%";s:24:"aiosp_description_format";s:13:"%description%";s:22:"aiosp_404_title_format";s:33:"Nothing found for %request_words%";s:18:"aiosp_paged_format";s:14:" - Part %page%";s:17:"aiosp_enablecpost";s:2:"on";s:17:"aiosp_cpostactive";a:2:{i:0;s:4:"post";i:1;s:4:"page";}s:19:"aiosp_cpostadvanced";s:1:"0";s:18:"aiosp_cpostnoindex";s:0:"";s:19:"aiosp_cpostnofollow";s:0:"";s:17:"aiosp_cposttitles";s:0:"";s:21:"aiosp_posttypecolumns";a:2:{i:0;s:4:"post";i:1;s:4:"page";}s:19:"aiosp_google_verify";s:0:"";s:17:"aiosp_bing_verify";s:0:"";s:22:"aiosp_pinterest_verify";s:0:"";s:22:"aiosp_google_publisher";s:0:"";s:28:"aiosp_google_disable_profile";s:0:"";s:29:"aiosp_google_sitelinks_search";s:0:"";s:26:"aiosp_google_set_site_name";s:0:"";s:30:"aiosp_google_specify_site_name";s:0:"";s:28:"aiosp_google_author_advanced";s:1:"0";s:28:"aiosp_google_author_location";a:1:{i:0;s:3:"all";}s:29:"aiosp_google_enable_publisher";s:2:"on";s:30:"aiosp_google_specify_publisher";s:0:"";s:25:"aiosp_google_analytics_id";s:0:"";s:25:"aiosp_ga_advanced_options";s:2:"on";s:15:"aiosp_ga_domain";s:0:"";s:21:"aiosp_ga_multi_domain";s:0:"";s:21:"aiosp_ga_addl_domains";s:0:"";s:21:"aiosp_ga_anonymize_ip";s:0:"";s:28:"aiosp_ga_display_advertising";s:0:"";s:22:"aiosp_ga_exclude_users";s:0:"";s:29:"aiosp_ga_track_outbound_links";s:0:"";s:25:"aiosp_ga_link_attribution";s:0:"";s:27:"aiosp_ga_enhanced_ecommerce";s:0:"";s:20:"aiosp_use_categories";s:0:"";s:26:"aiosp_use_tags_as_keywords";s:2:"on";s:32:"aiosp_dynamic_postspage_keywords";s:2:"on";s:22:"aiosp_category_noindex";s:2:"on";s:26:"aiosp_archive_date_noindex";s:2:"on";s:28:"aiosp_archive_author_noindex";s:2:"on";s:18:"aiosp_tags_noindex";s:0:"";s:20:"aiosp_search_noindex";s:0:"";s:17:"aiosp_404_noindex";s:2:"on";s:17:"aiosp_tax_noindex";s:0:"";s:23:"aiosp_paginated_noindex";s:0:"";s:24:"aiosp_paginated_nofollow";s:0:"";s:27:"aiosp_generate_descriptions";s:0:"";s:18:"aiosp_skip_excerpt";s:0:"";s:20:"aiosp_run_shortcodes";s:0:"";s:33:"aiosp_hide_paginated_descriptions";s:0:"";s:32:"aiosp_dont_truncate_descriptions";s:0:"";s:19:"aiosp_schema_markup";s:2:"on";s:20:"aiosp_unprotect_meta";s:0:"";s:33:"aiosp_redirect_attachement_parent";s:0:"";s:14:"aiosp_ex_pages";s:0:"";s:20:"aiosp_post_meta_tags";s:0:"";s:20:"aiosp_page_meta_tags";s:0:"";s:21:"aiosp_front_meta_tags";s:0:"";s:20:"aiosp_home_meta_tags";s:0:"";s:12:"aiosp_do_log";s:0:"";s:19:"last_active_version";s:3:"2.5";s:29:"aiosp_attachment_title_format";s:27:"%post_title% | %blog_title%";s:31:"aiosp_oembed_cache_title_format";s:27:"%post_title% | %blog_title%";s:22:"aiosp_acf_title_format";s:27:"%post_title% | %blog_title%";s:37:"aiosp_wpcf7_contact_form_title_format";s:27:"%post_title% | %blog_title%";s:27:"aiosp_ec_store_title_format";s:27:"%post_title% | %blog_title%";s:25:"aiosp_banner_title_format";s:27:"%post_title% | %blog_title%";s:30:"aiosp_testimonial_title_format";s:27:"%post_title% | %blog_title%";}', 'yes'),
(817, 'cf7sr_key', '6Leq6sMSAAAAAPDE_IvoCJzJrP9OZ9NaOQyXfEgF', 'yes'),
(818, 'cf7sr_secret', '6Leq6sMSAAAAAJi54Pdu7AkF4yVMs68eCkioj4bF', 'yes'),
(819, 'cf7sr_message', 'Invalid Captcha', 'yes'),
(822, '_site_transient_timeout_theme_roots', '1525851734', 'no'),
(823, '_site_transient_theme_roots', 'a:4:{s:12:"ozproduction";s:7:"/themes";s:13:"twentyfifteen";s:7:"/themes";s:15:"twentyseventeen";s:7:"/themes";s:13:"twentysixteen";s:7:"/themes";}', 'no'),
(824, '_site_transient_update_plugins', 'O:8:"stdClass":5:{s:12:"last_checked";i:1525849937;s:7:"checked";a:7:{s:30:"advanced-custom-fields/acf.php";s:6:"4.4.12";s:19:"akismet/akismet.php";s:5:"4.0.3";s:43:"all-in-one-seo-pack/all_in_one_seo_pack.php";s:3:"2.5";s:36:"contact-form-7/wp-contact-form-7.php";s:5:"4.9.2";s:67:"contact-form-7-simple-recaptcha/contact-form-7-simple-recaptcha.php";s:5:"0.0.2";s:42:"contact-form-cfdb7/contact-form-cfdb-7.php";s:5:"1.2.1";s:26:"wp-easycart/wpeasycart.php";s:6:"4.0.37";}s:8:"response";a:2:{s:36:"contact-form-7/wp-contact-form-7.php";O:8:"stdClass":12:{s:2:"id";s:28:"w.org/plugins/contact-form-7";s:4:"slug";s:14:"contact-form-7";s:6:"plugin";s:36:"contact-form-7/wp-contact-form-7.php";s:11:"new_version";s:5:"5.0.1";s:3:"url";s:45:"https://wordpress.org/plugins/contact-form-7/";s:7:"package";s:63:"https://downloads.wordpress.org/plugin/contact-form-7.5.0.1.zip";s:5:"icons";a:2:{s:2:"2x";s:66:"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=984007";s:2:"1x";s:66:"https://ps.w.org/contact-form-7/assets/icon-128x128.png?rev=984007";}s:7:"banners";a:2:{s:2:"2x";s:69:"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901";s:2:"1x";s:68:"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427";}s:11:"banners_rtl";a:0:{}s:6:"tested";s:5:"4.9.5";s:12:"requires_php";b:0;s:13:"compatibility";O:8:"stdClass":0:{}}s:26:"wp-easycart/wpeasycart.php";O:8:"stdClass":12:{s:2:"id";s:25:"w.org/plugins/wp-easycart";s:4:"slug";s:11:"wp-easycart";s:6:"plugin";s:26:"wp-easycart/wpeasycart.php";s:11:"new_version";s:6:"4.0.38";s:3:"url";s:42:"https://wordpress.org/plugins/wp-easycart/";s:7:"package";s:61:"https://downloads.wordpress.org/plugin/wp-easycart.4.0.38.zip";s:5:"icons";a:2:{s:2:"2x";s:64:"https://ps.w.org/wp-easycart/assets/icon-256x256.png?rev=1015460";s:2:"1x";s:64:"https://ps.w.org/wp-easycart/assets/icon-128x128.png?rev=1015460";}s:7:"banners";a:2:{s:2:"2x";s:67:"https://ps.w.org/wp-easycart/assets/banner-1544x500.png?rev=1738545";s:2:"1x";s:66:"https://ps.w.org/wp-easycart/assets/banner-772x250.png?rev=1738545";}s:11:"banners_rtl";a:0:{}s:6:"tested";s:5:"4.9.5";s:12:"requires_php";b:0;s:13:"compatibility";O:8:"stdClass":0:{}}}s:12:"translations";a:0:{}s:9:"no_update";a:5:{s:30:"advanced-custom-fields/acf.php";O:8:"stdClass":9:{s:2:"id";s:36:"w.org/plugins/advanced-custom-fields";s:4:"slug";s:22:"advanced-custom-fields";s:6:"plugin";s:30:"advanced-custom-fields/acf.php";s:11:"new_version";s:6:"4.4.12";s:3:"url";s:53:"https://wordpress.org/plugins/advanced-custom-fields/";s:7:"package";s:72:"https://downloads.wordpress.org/plugin/advanced-custom-fields.4.4.12.zip";s:5:"icons";a:2:{s:2:"2x";s:75:"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png?rev=1082746";s:2:"1x";s:75:"https://ps.w.org/advanced-custom-fields/assets/icon-128x128.png?rev=1082746";}s:7:"banners";a:2:{s:2:"2x";s:78:"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099";s:2:"1x";s:77:"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102";}s:11:"banners_rtl";a:0:{}}s:19:"akismet/akismet.php";O:8:"stdClass":9:{s:2:"id";s:21:"w.org/plugins/akismet";s:4:"slug";s:7:"akismet";s:6:"plugin";s:19:"akismet/akismet.php";s:11:"new_version";s:5:"4.0.3";s:3:"url";s:38:"https://wordpress.org/plugins/akismet/";s:7:"package";s:56:"https://downloads.wordpress.org/plugin/akismet.4.0.3.zip";s:5:"icons";a:2:{s:2:"2x";s:59:"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272";s:2:"1x";s:59:"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272";}s:7:"banners";a:1:{s:2:"1x";s:61:"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904";}s:11:"banners_rtl";a:0:{}}s:43:"all-in-one-seo-pack/all_in_one_seo_pack.php";O:8:"stdClass":9:{s:2:"id";s:33:"w.org/plugins/all-in-one-seo-pack";s:4:"slug";s:19:"all-in-one-seo-pack";s:6:"plugin";s:43:"all-in-one-seo-pack/all_in_one_seo_pack.php";s:11:"new_version";s:3:"2.5";s:3:"url";s:50:"https://wordpress.org/plugins/all-in-one-seo-pack/";s:7:"package";s:66:"https://downloads.wordpress.org/plugin/all-in-one-seo-pack.2.5.zip";s:5:"icons";a:2:{s:2:"2x";s:71:"https://ps.w.org/all-in-one-seo-pack/assets/icon-256x256.png?rev=979908";s:2:"1x";s:71:"https://ps.w.org/all-in-one-seo-pack/assets/icon-128x128.png?rev=979908";}s:7:"banners";a:2:{s:2:"2x";s:75:"https://ps.w.org/all-in-one-seo-pack/assets/banner-1544x500.png?rev=1354894";s:2:"1x";s:74:"https://ps.w.org/all-in-one-seo-pack/assets/banner-772x250.png?rev=1354894";}s:11:"banners_rtl";a:0:{}}s:67:"contact-form-7-simple-recaptcha/contact-form-7-simple-recaptcha.php";O:8:"stdClass":9:{s:2:"id";s:45:"w.org/plugins/contact-form-7-simple-recaptcha";s:4:"slug";s:31:"contact-form-7-simple-recaptcha";s:6:"plugin";s:67:"contact-form-7-simple-recaptcha/contact-form-7-simple-recaptcha.php";s:11:"new_version";s:5:"0.0.2";s:3:"url";s:62:"https://wordpress.org/plugins/contact-form-7-simple-recaptcha/";s:7:"package";s:80:"https://downloads.wordpress.org/plugin/contact-form-7-simple-recaptcha.0.0.2.zip";s:5:"icons";a:1:{s:7:"default";s:75:"https://s.w.org/plugins/geopattern-icon/contact-form-7-simple-recaptcha.svg";}s:7:"banners";a:0:{}s:11:"banners_rtl";a:0:{}}s:42:"contact-form-cfdb7/contact-form-cfdb-7.php";O:8:"stdClass":9:{s:2:"id";s:32:"w.org/plugins/contact-form-cfdb7";s:4:"slug";s:18:"contact-form-cfdb7";s:6:"plugin";s:42:"contact-form-cfdb7/contact-form-cfdb-7.php";s:11:"new_version";s:5:"1.2.1";s:3:"url";s:49:"https://wordpress.org/plugins/contact-form-cfdb7/";s:7:"package";s:61:"https://downloads.wordpress.org/plugin/contact-form-cfdb7.zip";s:5:"icons";a:2:{s:2:"2x";s:71:"https://ps.w.org/contact-form-cfdb7/assets/icon-256x256.png?rev=1619878";s:2:"1x";s:71:"https://ps.w.org/contact-form-cfdb7/assets/icon-128x128.png?rev=1619878";}s:7:"banners";a:1:{s:2:"1x";s:73:"https://ps.w.org/contact-form-cfdb7/assets/banner-772x250.png?rev=1619902";}s:11:"banners_rtl";a:0:{}}}}', 'no'),
(825, '_site_transient_timeout_aioseop_update_check_time', '1525871552', 'no'),
(826, '_site_transient_aioseop_update_check_time', '1525849952', 'no'),
(827, '_transient_timeout_feed_54af7cb0f50a8f4f8cdb30f2d9b4f50c', '1525893157', 'no');
INSERT INTO `ops_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(828, '_transient_feed_54af7cb0f50a8f4f8cdb30f2d9b4f50c', 'a:4:{s:5:"child";a:1:{s:0:"";a:1:{s:3:"rss";a:1:{i:0;a:6:{s:4:"data";s:3:"\n\n\n";s:7:"attribs";a:1:{s:0:"";a:1:{s:7:"version";s:3:"2.0";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:1:{s:7:"channel";a:1:{i:0;a:6:{s:4:"data";s:52:"\n	\n	\n	\n	\n	\n	\n	\n	\n	\n\n \n	\n		\n		\n		\n		\n		\n		\n		\n		\n		\n	";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:8:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:14:"Semper Plugins";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:25:"https://semperplugins.com";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:25:"Premium WordPress Plugins";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:13:"lastBuildDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 17 Apr 2018 21:24:18 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"language";a:1:{i:0;a:5:{s:4:"data";s:5:"en-US";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:9:"generator";a:1:{i:0;a:5:{s:4:"data";s:30:"https://wordpress.org/?v=4.9.5";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:5:"image";a:1:{i:0;a:6:{s:4:"data";s:11:"\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:5:{s:3:"url";a:1:{i:0;a:5:{s:4:"data";s:88:"https://semperplugins.com/wp-content/uploads/2016/06/cropped-AIOSEOP-Gear-Blue-32x32.png";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:14:"Semper Plugins";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:25:"https://semperplugins.com";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:5:"width";a:1:{i:0;a:5:{s:4:"data";s:2:"32";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"height";a:1:{i:0;a:5:{s:4:"data";s:2:"32";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}}s:4:"item";a:10:{i:0;a:6:{s:4:"data";s:45:"\n		\n		\n		\n		\n		\n				\n		\n\n		\n		\n				\n			\n		\n		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:5:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:44:"5 Reasons Why Your Website Is Not Converting";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:71:"https://semperplugins.com/5-reasons-why-your-website-is-not-converting/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:80:"https://semperplugins.com/5-reasons-why-your-website-is-not-converting/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 07 Nov 2017 13:58:39 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:2:{i:0;a:5:{s:4:"data";s:4:"Blog";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:15:"site conversion";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"https://semperplugins.com/?p=3552/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:421:"How do you measure the success of a site? Conversion. You could have a lot of traffic going to your site, but those visits don’t really matter if you aren’t converting them into customers. The only visitors that matter are<span class="ellipsis">&#8230;</span><div class="read-more"><a href="https://semperplugins.com/5-reasons-why-your-website-is-not-converting/">Read more &#8250;</a></div><!-- end of .read-more -->";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:12:"Arnaud Broes";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:8338:"<p><img class="aligncenter wp-image-3698 size-full" src="https://semperplugins.com/wp-content/uploads/2017/07/5-reasons-why-your-site-is-not-converting-featured.jpg" alt="Faced with a website that isn''t converting? Here are five reasons why your conversion rate is underwhelming." width="850" height="250" srcset="https://semperplugins.com/wp-content/uploads/2017/07/5-reasons-why-your-site-is-not-converting-featured.jpg 850w, https://semperplugins.com/wp-content/uploads/2017/07/5-reasons-why-your-site-is-not-converting-featured-300x88.jpg 300w, https://semperplugins.com/wp-content/uploads/2017/07/5-reasons-why-your-site-is-not-converting-featured-768x226.jpg 768w, https://semperplugins.com/wp-content/uploads/2017/07/5-reasons-why-your-site-is-not-converting-featured-100x29.jpg 100w, https://semperplugins.com/wp-content/uploads/2017/07/5-reasons-why-your-site-is-not-converting-featured-150x44.jpg 150w, https://semperplugins.com/wp-content/uploads/2017/07/5-reasons-why-your-site-is-not-converting-featured-200x59.jpg 200w, https://semperplugins.com/wp-content/uploads/2017/07/5-reasons-why-your-site-is-not-converting-featured-450x132.jpg 450w, https://semperplugins.com/wp-content/uploads/2017/07/5-reasons-why-your-site-is-not-converting-featured-600x176.jpg 600w" sizes="(max-width: 850px) 100vw, 850px" /></p>\n<p>How do you measure the success of a site? Conversion.</p>\n<p>You could have a lot of traffic going to your site, but those visits don’t really matter if you aren’t converting them into customers. The only visitors that matter are those that are adding to your bottom-line. All other traffic is essentially wasted.</p>\n<p>Are you finding yourself with a lot of traffic, selling the right products at competitive prices, yet not with many paying customers? Then read on to find out why your site isn’t converting and what you can do about it.</p>\n<h2>Check Your Current Conversion Rate</h2>\n<p>First things first, you should know your current conversion rate. Whether you measure your conversions by product purchases, brochure downloads or email newsletter signups: you need to be measuring and tracking those conversions regularly. You can track your conversion rate using Goals in <a href="https://analytics.google.com/analytics/web/" target="_blank" rel="noopener">Google Analytics,</a> or through any other analytics platform of your choosing.</p>\n<p>So what&#8217;s considered a good conversion rate? The ideal rate is between 2 to 3 percent. This rate could be higher or lower based on the value of the conversion (e.g. you’d have more difficulty converting customers if you’re selling products or services worth $10,000+). But if you’re having less than 2 percent conversion and your conversion value isn’t very high, then you might have a problem on your hands.</p>\n<p>Here are five reasons why your site may be underperforming.</p>\n<h2>#1. You’re Providing a Bad Mobile Experience</h2>\n<p>If your website isn’t mobile-friendly, there is no way it will survive in these times. It isn&#8217;t enough for your site to be responsive, it has to be designed specifically for mobile in terms of content and structure. Failure to do so would mean marginalizing a substantial portion of your customer base and in turn decreasing your conversion rate.</p>\n<p>But what does designing for mobile entail?</p>\n<ul>\n<li>Using large and easily legible text</li>\n<li>Using short paragraphs</li>\n<li>Making sure that every key feature is just a tap away</li>\n<li>Having just a single call-to-action</li>\n</ul>\n<p>The key is to keep testing your mobile site. Perhaps you should even ask customers and family members for feedback on your site’s mobile experience. Afterwards, you should review what is working and what is not &#8211; and address those issues.</p>\n<p>As long as you do not ignore the importance of having a good mobile experience for your site, you can easily hone in on the reason(s) your site is not converting.</p>\n<h2>#2. No Call to Action</h2>\n<p>Your website could be user-friendly with engaging and quality content, but without a clear and concise call-to-action, you simply won’t convert. Users may want to convert but just don&#8217;t have the opportunity or means to do so. Because you haven’t provided it.</p>\n<p>Make your call-to-action clear, concise, prominent, specific and compelling. Provide all the information users need so they know exactly what you want them to do next. Create calls-to-action that are relevant and specific; and place them in a prominent place on every page of your site.</p>\n<p>Whatever you do, make sure it is very easy for the user to convert when they are ready to.</p>\n<h2>#3. Your Users Are Annoyed with Your Website</h2>\n<p>You may be missing out on conversion opportunities if there is something off-putting about your site. Look at your bounce rate: if it is high, then you know there is something that is not appealing to users.</p>\n<p>In such a case, you need to find out what the problem is &#8211; directly from the user. You may use heat maps and look at your user journeys via Google Analytics, but perhaps it will make your job easier to ask users directly (e.g. via a quick survey).</p>\n<p>A few common annoyances on websites:</p>\n<ul>\n<li>You do not offer any useful information</li>\n<li>Navigation is too difficult</li>\n<li>You have too many ads or popups</li>\n<li>Your site doesn’t look good</li>\n</ul>\n<p>These are all problems with a rather easy fix. Take your time investigating them and fix the errors as soon as you can.</p>\n<h2>#4. You’re Off-Target</h2>\n<p>Your website should be anything but general. A lot of website owners aim to please everyone – but they can’t – and end up isolating their entire audience.</p>\n<p>Perhaps you’re writing for the wrong audience, perhaps you’re writing for a larger demographic than you should be; whatever the case, inaccurate targeting could negatively impact your conversion rate. Your copy, branding, marketing and site design should speak directly to a niche demographic.</p>\n<p>Conduct market research to properly define and know your audience.</p>\n<h2>#5. You Have a Slow Site</h2>\n<p>We previously covered the importance of website load speed and provided <a href="https://semperplugins.com/10-quick-easy-ways-to-speed-up-your-site/">tips on to speed up your site</a>. When your website doesn’t load fast, your visitors leave and are unlikely to visit again in the near future.</p>\n<p>There are many factors that can slow down your site, such as:</p>\n<ul>\n<li>A site that isn’t optimized for mobile</li>\n<li>Broken links</li>\n<li>Not using caching</li>\n<li>Messy code</li>\n<li>Images that aren’t optimized</li>\n<li>Flash and Java</li>\n<li>Average web hosting</li>\n</ul>\n<p>You can <a href="https://developers.google.com/speed/pagespeed/insights/" target="_blank" rel="noopener">check your current site speed via Page Speed Insights</a>. If your site is taking longer than 3 seconds to load, you need to work on it. Studies show that most users quickly exit sites that take longer than 3 seconds to load.</p>\n<h2>Measure, Measure and Measure</h2>\n<p>This is actually a sixth reason why your site may not be converting. Measure, measure and keep measuring. If you’re not measuring, you’re guessing, and in turn, not really making any strides.</p>\n<p>The problem most website owners encounter is that they are either not tracking their website traffic or that they are tracking but not reviewing their metrics. Some do know they should be tracking, but just aren’t sure what to look for or what to make of the results.</p>\n<p>Well, you should always start with your goals in mind. Then you can identify which key metrics will help you to measure your progress towards those goals.</p>\n<p>Once you start measuring, you can improve on those metrics; figure out what is working, what isn’t, and then fine-tune and optimize.</p>\n<h2>Wrapping Up</h2>\n<p>These five reasons are just a starting point to help boost your conversion rates. There are many other reasons why your site may not be converting. However, these are fairly common in under-converting sites.</p>\n<p>If you find that any of these reasons apply to your business, simply make the changes and you’ll see a big difference or improvement in your conversion rates and bottom line.</p>\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:76:"https://semperplugins.com/5-reasons-why-your-website-is-not-converting/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:2:"50";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:1;a:6:{s:4:"data";s:51:"\n		\n		\n		\n		\n		\n				\n		\n		\n		\n\n		\n		\n				\n			\n		\n		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:5:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:38:"Choosing the Right Domain Name for SEO";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:65:"https://semperplugins.com/choosing-the-right-domain-name-for-seo/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:74:"https://semperplugins.com/choosing-the-right-domain-name-for-seo/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 12 Sep 2017 14:51:02 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:4:{i:0;a:5:{s:4:"data";s:4:"Blog";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:8:"SEO Tips";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:2;a:5:{s:4:"data";s:12:"domain names";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:3;a:5:{s:4:"data";s:8:"SEO tips";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"https://semperplugins.com/?p=3443/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:398:"Back in the day, way way back, your website domain could have had a major impact on your site’s rankings. That changed a few years ago (2012 to be exact) when Google wanted to weed out websites that may have<span class="ellipsis">&#8230;</span><div class="read-more"><a href="https://semperplugins.com/choosing-the-right-domain-name-for-seo/">Read more &#8250;</a></div><!-- end of .read-more -->";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:12:"Arnaud Broes";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:7445:"<p><img class="size-full wp-image-3702 aligncenter" src="https://semperplugins.com/wp-content/uploads/2017/06/choosing-a-domain-name-for-seo-featured.jpg" alt="" width="850" height="250" srcset="https://semperplugins.com/wp-content/uploads/2017/06/choosing-a-domain-name-for-seo-featured.jpg 850w, https://semperplugins.com/wp-content/uploads/2017/06/choosing-a-domain-name-for-seo-featured-300x88.jpg 300w, https://semperplugins.com/wp-content/uploads/2017/06/choosing-a-domain-name-for-seo-featured-768x226.jpg 768w, https://semperplugins.com/wp-content/uploads/2017/06/choosing-a-domain-name-for-seo-featured-100x29.jpg 100w, https://semperplugins.com/wp-content/uploads/2017/06/choosing-a-domain-name-for-seo-featured-150x44.jpg 150w, https://semperplugins.com/wp-content/uploads/2017/06/choosing-a-domain-name-for-seo-featured-200x59.jpg 200w, https://semperplugins.com/wp-content/uploads/2017/06/choosing-a-domain-name-for-seo-featured-450x132.jpg 450w, https://semperplugins.com/wp-content/uploads/2017/06/choosing-a-domain-name-for-seo-featured-600x176.jpg 600w" sizes="(max-width: 850px) 100vw, 850px" /></p>\n<p>Back in the day, way way back, your website domain could have had a major impact on your site’s rankings. That changed a few years ago (2012 to be exact) when Google wanted to weed out websites that may have good domain names but very little “substance”.</p>\n<p>Exact Match Domains (EMD) is where the problem started. Businesses would essentially purchase keyword domains to rank for a particular keyword. And the EMD was enough for them to rank: they barely focused on quality content and relevance (two things that matter greatly for SEO nowadays). That strategy no longer works.</p>\n<p>That’s not to say that domain names no longer affect SEO. They still do (and studies have proven that), but how?</p>\n<p>Let’s find out.</p>\n<h2>How Domain Names Impact SEO</h2>\n<p>There have been a lot of changes to SEO, particularly from the largest search engine: Google. But one thing that hasn’t changed, but has been emphasized more, is original quality content, relevance and always aligning with search engine best practices. A component of this is your domain name &#8211; it’s unique, only you can have it.</p>\n<p>Even though it may not directly impact your rankings anymore, a good domain name will get you more brand recognition, trust and higher click-through-rates (CTR).</p>\n<p>However, the definition of a good domain name has changed, at least according to Google. We see this in how the search engine treats certain types of domains.</p>\n<h2>Exact Match Domains (EMD)</h2>\n<p>EMDs are domain names that include keyword phrases. For example, let’s say you are selling hair products and want to rank for “quality hair products.” The EMD you would most likely use is qualityhairproducts.com. Just purchasing that domain would have gotten you the first page ranking a while ago.</p>\n<p>Yup! You wouldn’t have needed original content or backlinks. You would simply rank for it with your domain name. That all changed after Google released the Exact Match Domain Update.</p>\n<p>The update essentially made EMDs obsolete in search engine rankings. In fact, a study by High Position showed that the average EMD ranking went from position 13.4 to 26.6 (yikes!) after Google’s update. The average top 10 EMD also dropped in rankings: from 3.2 down to 11.9. So now we know that choosing an exact match domain in 2017 is quite pointless, a bad idea even.</p>\n<p>So how should you decide on a domain name to maximize SEO? You need only do two things: choose a memorable brand name (to make your domain name) and pick a .com extension.</p>\n<p>Let’s go into more detail.</p>\n<h2>Your Brand Name Should Be Your Domain Name</h2>\n<p>We can all agree that your brand is important. Your brand name is how your customers recognize and find you in search engines and social media platforms. So think of your domain name as the foothold of your online brand.</p>\n<p>In fact, the more customers are using your brand name to search for you online, the more your SEO and rankings will improve. This is referred to as brand signalling (any reference of your business online). Matt Cutts, former Head of Web Spam at Google, claimed that Google “actually came up with a classifier to say, okay, IRS or Wikipedia or New York Times is over on this side, and the low-quality sites are over on this side.”</p>\n<p>That’s right, Google now cares more about brands for SEO than it does about keywords and links. For that reason, using your brand name is more important than keywords. You’re probably scratching your head at this point, wondering how Google would associate your brand with certain keywords&#8230;</p>\n<p>Well, Google will associate keywords with your brand as your brand becomes more popular; and as you produce more relevant and high quality content. Let’s take a look at Bitly as an example. It’s a URL shortener and link management platform yet their brand name does not match those keywords. It however ranks at the top for those keywords in Google.</p>\n<p>That’s because of its popularity. As long as people recognize it and are searching for it, Google will measure that brand signal and rank it accordingly. For this reason, it is important to have a memorable brand name and consequently, a memorable domain name.</p>\n<p>Don’t worry if your brand name contains a keyword, or in other words, is a PMD (partial match domain). That’s because Google is only searching for spam sites with EMDs and PMDs &#8211; the actual problem isn’t the keywords, but rather the content and quality of the site. A PMD or EMD with bad user experience and low quality content would experience a steep downgrade in rankings. Whereas, a PMD or EMD with great user experience and content would not be greatly affected.</p>\n<p><em>However</em>, if you are just starting out, you should go with a memorable brand name domain and avoid using keywords.</p>\n<h2>Opt For a .com Extension</h2>\n<p>You probably already know that .com is the most popular domain extension. That is because most other domain extensions like .biz and .us are viewed as spam. Although choosing it may not directly impact your rankings, you may be viewed as a low-ranking site which could affect your SEO.</p>\n<p>.com is simply the most convenient and safest choice to go with. When in doubt, go with it.</p>\n<h2>Tips for Choosing the Perfect Domain Name</h2>\n<p>Now let’s recap and go over the tips on picking the best domain name:</p>\n<ul>\n<li>Use your brand name</li>\n<li>Don’t use exact match domains. Partial match domains are OK but a brand name is always more effective</li>\n<li>Choose a .com extension</li>\n<li>Make it memorable so users can easily remember it</li>\n<li>Keep it short, 15 characters at most</li>\n<li>Avoid numbers, hyphens and special characters</li>\n<li>Avoid misspelling words on purpose. It’s a no-no for branding</li>\n<li>Make it easy to spell</li>\n</ul>\n<h2>Wrapping Up</h2>\n<p>We hope you now have a good understanding of how domain names affect SEO in 2017. If you’re starting a new business or changing to a better domain name, the tips above should help you pick out the best one for your business.</p>\n<p>Remember: As long as you have great content and a good SEO strategy, a strong and unique domain name may rank you higher. Choose one wisely, use it correctly and start reaping the value.</p>\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:70:"https://semperplugins.com/choosing-the-right-domain-name-for-seo/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:2:"41";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:2;a:6:{s:4:"data";s:48:"\n		\n		\n		\n		\n		\n				\n		\n		\n\n		\n		\n				\n			\n		\n		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:5:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:39:"How to Come up with Great Content Ideas";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:66:"https://semperplugins.com/how-to-come-up-with-great-content-ideas/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:75:"https://semperplugins.com/how-to-come-up-with-great-content-ideas/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 29 Aug 2017 13:00:30 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:3:{i:0;a:5:{s:4:"data";s:4:"Blog";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:8:"SEO Tips";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:2;a:5:{s:4:"data";s:17:"content marketing";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:32:"http://semperplugins.com/?p=2375";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:423:"Do you spend hours staring at a blank computer screen or piece of paper, desperately racking your brain for just one great content idea? Well, the sad reality is that you cannot afford to wait for inspiration to strike: your readers<span class="ellipsis">&#8230;</span><div class="read-more"><a href="https://semperplugins.com/how-to-come-up-with-great-content-ideas/">Read more &#8250;</a></div><!-- end of .read-more -->";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:12:"Arnaud Broes";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:12701:"<p><img class="size-full wp-image-3674 aligncenter" src="https://semperplugins.com/wp-content/uploads/2017/09/great-content-ideas-featured.jpg" alt="Looking for some great ideas to produce new content? Here are a few tips to get you started." width="850" height="250" srcset="https://semperplugins.com/wp-content/uploads/2017/09/great-content-ideas-featured.jpg 850w, https://semperplugins.com/wp-content/uploads/2017/09/great-content-ideas-featured-300x88.jpg 300w, https://semperplugins.com/wp-content/uploads/2017/09/great-content-ideas-featured-768x226.jpg 768w, https://semperplugins.com/wp-content/uploads/2017/09/great-content-ideas-featured-100x29.jpg 100w, https://semperplugins.com/wp-content/uploads/2017/09/great-content-ideas-featured-150x44.jpg 150w, https://semperplugins.com/wp-content/uploads/2017/09/great-content-ideas-featured-200x59.jpg 200w, https://semperplugins.com/wp-content/uploads/2017/09/great-content-ideas-featured-450x132.jpg 450w, https://semperplugins.com/wp-content/uploads/2017/09/great-content-ideas-featured-600x176.jpg 600w" sizes="(max-width: 850px) 100vw, 850px" /></p>\n<p>Do you spend hours staring at a blank computer screen or piece of paper, desperately racking your brain for just one great content idea? Well, the sad reality is that you cannot afford to wait for inspiration to strike: your readers are waiting for yet another one of your great posts to enjoy and share.</p>\n<p>This article is your reference for tips and tricks to find inspiration and create great content to grow your WordPress site.</p>\n<p>We’ll cover the following topics:</p>\n<ul>\n<li>How to source inspiration directly from your readers</li>\n<li>How to get great topic ideas from your social media channels</li>\n<li>Useful tools you may already be using, but not as effectively</li>\n<li>How to overcome writer&#8217;s block</li>\n</ul>\n<h2>Know Your Readers and Their Concerns</h2>\n<p>This is arguably the starting point to creating great content: knowing your reader and their concerns.</p>\n<p>To gain and maintain a steady stream of followers, you must produce consistently good content that your blog visitors are interested in. Since your content is ideally centered around the needs and interests of your readers, it is therefore very important to know who they are.  But how would you know what your readers are interested in?</p>\n<p>First, put yourself in your reader&#8217;s shoes. Then ask and answer the following questions:</p>\n<ol>\n<li>What issues do your readers struggle with most?</li>\n<li>What would better enable them to understand and solve those issues?</li>\n<li>What would a reader enter in search engines to find content in your niche?</li>\n</ol>\n<p>Creating valuable content boils down to problem solving. You can start writing winning content more easily once you know what your readers are looking for and what their pain points are.</p>\n<p>A key thing to remember, however, is that some of your readers may be beginners who are trying to come to grips with your subject area. So create content that is clear &amp; appealing to beginners and which does not exclusively focus on advanced readers.</p>\n<h3>Look at Your Analytics</h3>\n<p>Your website analytics will provide you with a lot of insight regarding where your readers are coming from and how they&#8217;re interacting with your site. Check <a href="https://analytics.google.com/analytics/web/" target="_blank" rel="noopener">Google Analytics</a> for keywords that users are searching for to get to your site, and then create engaging content about those topics.</p>\n<p>You can also use those insights to update and keep existing posts relevant; or to create new posts with detailed information on the problems your target audience is trying to solve.</p>\n<h3>Go Through Your Blog Comments</h3>\n<p>Your blog comments can be extremely helpful to you in a number of ways.</p>\n<p>First of all, they provide you with great insight into what readers think of the content you have written. This may include positive or negative feedback on the style and clarity of your writing, questions for more information on areas you can expand on, wide-spread problems being discussed and/or solutions to those problems.</p>\n<p>Note them down and come up with post ideas to help your readers.</p>\n<p>If your readers take the time to report a problem they are having, or to give you feedback, you need to listen. It usually means that it&#8217;s a significant challenge they, and other users, are facing.</p>\n<h3>Sift Through Your FAQs</h3>\n<p>You can actively engage with your readers by answering their most frequently asked questions in blog posts. This will show readers that you have carefully considered their responses to your content, and also helps other readers who may also have similar questions.</p>\n<p>And as a nice bonus, you may attract some more visitors that wouldn&#8217;t have found your WordPress site otherwise.</p>\n<h2>Use Social Media</h2>\n<p>Social media channels are arguably the best place to source inspiration. Take <a href="https://www.buzzfeed.com/" target="_blank" rel="noopener">BuzzFeed</a> for example. They constantly browse the web for fun ideas &#8211; typically small trends or trending topics in niche areas &#8211; and make them more mainstream.</p>\n<div id="attachment_3026" style="width: 510px" class="wp-caption aligncenter"><img class="wp-image-3026" src="https://semperplugins.com/wp-content/uploads/2016/12/twitter-trends.jpg" alt="At the time of writing, Twitter show us, for example, that growth hacking and content marketing tips are trending among bloggers." width="500" height="380" srcset="https://semperplugins.com/wp-content/uploads/2016/12/twitter-trends.jpg 754w, https://semperplugins.com/wp-content/uploads/2016/12/twitter-trends-300x228.jpg 300w, https://semperplugins.com/wp-content/uploads/2016/12/twitter-trends-328x250.jpg 328w, https://semperplugins.com/wp-content/uploads/2016/12/twitter-trends-100x76.jpg 100w, https://semperplugins.com/wp-content/uploads/2016/12/twitter-trends-150x114.jpg 150w, https://semperplugins.com/wp-content/uploads/2016/12/twitter-trends-200x152.jpg 200w, https://semperplugins.com/wp-content/uploads/2016/12/twitter-trends-450x343.jpg 450w, https://semperplugins.com/wp-content/uploads/2016/12/twitter-trends-600x457.jpg 600w" sizes="(max-width: 500px) 100vw, 500px" /><p class="wp-caption-text">At the time of writing, Twitter show us that growth hacking and content marketing tips are now trending among bloggers.</p></div>\n<p>You can also use social media to stay on top of industry trends; and to monitor the new keywords created by these trends. Use tools like <a href="https://www.google.com/trends/" target="_blank" rel="noopener">Google Trends</a>, Twitter Trends, <a href="https://www.reddit.com/" target="_blank" rel="noopener">Reddit</a>, etc. to keep up with the conversation and take it to your blog.</p>\n<h2><b>Questionnaires </b>and Polls</h2>\n<p>Another useful way to engage with readers is to put out short surveys or polls on your blog, social media or your mailing list. Ask them any questions they may have or for their opinion on certain topics that you covered. This is also a fantastic way to keep them engaged with your content.</p>\n<p>Make it clear to your mailing list that you welcome questions. One way to do this is to offer some sort of incentive: perhaps a product discount, a coupon, or free mentoring to anyone who is willing to give a short interview. Interviewing in particular is a great way to find out a host of questions or issues your audience may have with your topic or industry that may never have even crossed your mind. This strategy may seem daunting, but is an effective way of coming up with content that is higher quality and more relevant to your audience.</p>\n<h2>Recycle and Transform</h2>\n<p>The fastest way for you to generate content ideas is by reading through existing content and expanding and improving on it. This means looking though what your competitors are writing about, and using that as a beacon to light your own blog posts. It is about taking already existing material, not copying it, but using it as a starting point for your own inspiration.</p>\n<p>An important thing to consistently keep in mind when expanding and improving existing material is to not limit yourself to the original piece; to consider what else readers would benefit from knowing. Perhaps you could make the piece more personal and relatable by referencing your personal experiences.</p>\n<div id="attachment_3639" style="width: 515px" class="wp-caption aligncenter"><img class="wp-image-3639" src="https://semperplugins.com/wp-content/uploads/2016/12/feedly-screenshot.png" alt="Using Feedly, you can quickly find out what your competitors are writing about and use their content as a source of inspiration for your own." width="505" height="369" srcset="https://semperplugins.com/wp-content/uploads/2016/12/feedly-screenshot.png 1280w, https://semperplugins.com/wp-content/uploads/2016/12/feedly-screenshot-300x219.png 300w, https://semperplugins.com/wp-content/uploads/2016/12/feedly-screenshot-768x561.png 768w, https://semperplugins.com/wp-content/uploads/2016/12/feedly-screenshot-1024x748.png 1024w, https://semperplugins.com/wp-content/uploads/2016/12/feedly-screenshot-342x250.png 342w, https://semperplugins.com/wp-content/uploads/2016/12/feedly-screenshot-100x73.png 100w, https://semperplugins.com/wp-content/uploads/2016/12/feedly-screenshot-150x110.png 150w, https://semperplugins.com/wp-content/uploads/2016/12/feedly-screenshot-200x146.png 200w, https://semperplugins.com/wp-content/uploads/2016/12/feedly-screenshot-450x329.png 450w, https://semperplugins.com/wp-content/uploads/2016/12/feedly-screenshot-600x438.png 600w, https://semperplugins.com/wp-content/uploads/2016/12/feedly-screenshot-900x657.png 900w" sizes="(max-width: 505px) 100vw, 505px" /><p class="wp-caption-text">Using Feedly, you can quickly find out what your competitors are writing about and use their content as a source of inspiration for your own.</p></div>\n<p>So how can you discover other content in your niche? Use tools such as <a href="https://feedly.com/" target="_blank" rel="noopener">Feedly</a> which shows you a list of publications that have been produced about a certain topic. Once you create an account, you can simply enter in specific keywords, URLs, or browse through existing categories.</p>\n<h2>Read Every Day</h2>\n<p>Reading every day actually means reading something relevant to your niche every single day. By doing so, you will find that over time, not only will your knowledge of the subject matter improve substantially, but that new sources of inspiration will spring up as well.</p>\n<p>Reading content written by others in your field and the comments left behind by their users is also a good way to find inspiration. When reading through other peoples content, you may notice that similar problems keep cropping up but have remained unanswered. You can subsequently try to answer those questions in depth and address those issues in your own blog.</p>\n<p>Visit popular blogs and websites, and observe the different ways in which they operate. Pay close attention to their writing style, and the topics that they are covering. You will slowly begin to pick up fresh ideas, and gain exposure to different viewpoints.</p>\n<p>Reading stimulates the brain, thus making it that much easier for you to generate great content ideas.</p>\n<h2>How to Tackle Writer’s Block</h2>\n<p>So you&#8217;ve found inspiration and are now struggling with writer&#8217;s block. One of the best techniques to overcome it is simply to write! Write something down every single day, no matter how small, or jumbled. The point is to get all your ideas down on paper, which you can later revise and expand on.</p>\n<p>Keep your work well organized, and easy to refer back to. You can use online note applications such as <a href="http://evernote.com/" target="_blank" rel="noopener">Evernote</a> or Google Docs to keep your work backed up and easily accessible.</p>\n<p>Having something to start with makes that blank page much less intimidating.</p>\n<h2>In a Nutshell</h2>\n<p>Once you get started using these tips and tricks, you will find that generating good content ideas is not as difficult as it may have initially seemed. So we hope this post has helped you to find inspiration for your next engaging blog post. The truth is, you really never know when your next source of inspiration may strike, so best of luck to you!</p>\n<p>Do you have any special tricks to come up with great content ideas? Share them with your fellow readers below in the comment section!</p>\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:71:"https://semperplugins.com/how-to-come-up-with-great-content-ideas/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:2:"22";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:3;a:6:{s:4:"data";s:51:"\n		\n		\n		\n		\n		\n				\n		\n		\n		\n\n		\n		\n				\n			\n		\n		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:5:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:40:"How to Build a Landing Page in WordPress";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:64:"https://semperplugins.com/how-to-build-a-wordpress-landing-page/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:73:"https://semperplugins.com/how-to-build-a-wordpress-landing-page/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 07 Jul 2017 13:00:33 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:4:{i:0;a:5:{s:4:"data";s:4:"Blog";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:13:"landing pages";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:2;a:5:{s:4:"data";s:17:"WordPress plugins";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:3;a:5:{s:4:"data";s:16:"WordPress themes";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"https://semperplugins.com/?p=3441/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:419:"What a lot of people don&#8217;t know is that you can build great landing pages for your WordPress site without having to break the bank by hiring services from lead generation and marketing companies. You can even create a landing<span class="ellipsis">&#8230;</span><div class="read-more"><a href="https://semperplugins.com/how-to-build-a-wordpress-landing-page/">Read more &#8250;</a></div><!-- end of .read-more -->";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:12:"Arnaud Broes";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:14070:"<p><img class="size-full wp-image-3668 aligncenter" src="https://semperplugins.com/wp-content/uploads/2017/09/how-to-build-a-landing-page-featured.jpg" alt="You can either build a WordPress landing page yourself using a customized page template or by installing a specialized landing page builder plugin." width="850" height="250" srcset="https://semperplugins.com/wp-content/uploads/2017/09/how-to-build-a-landing-page-featured.jpg 850w, https://semperplugins.com/wp-content/uploads/2017/09/how-to-build-a-landing-page-featured-300x88.jpg 300w, https://semperplugins.com/wp-content/uploads/2017/09/how-to-build-a-landing-page-featured-768x226.jpg 768w, https://semperplugins.com/wp-content/uploads/2017/09/how-to-build-a-landing-page-featured-100x29.jpg 100w, https://semperplugins.com/wp-content/uploads/2017/09/how-to-build-a-landing-page-featured-150x44.jpg 150w, https://semperplugins.com/wp-content/uploads/2017/09/how-to-build-a-landing-page-featured-200x59.jpg 200w, https://semperplugins.com/wp-content/uploads/2017/09/how-to-build-a-landing-page-featured-450x132.jpg 450w, https://semperplugins.com/wp-content/uploads/2017/09/how-to-build-a-landing-page-featured-600x176.jpg 600w" sizes="(max-width: 850px) 100vw, 850px" /></p>\n<p>What a lot of people don&#8217;t know is that you can build great landing pages for your WordPress site without having to break the bank by hiring services from lead generation and marketing companies.</p>\n<p>You can even <strong>create a landing page yourself in less than 10 minutes</strong>. We’ll show you how.</p>\n<p>First things first, let&#8217;s cover the basics of landing pages.</p>\n<h2>Why Should I Build a Landing Page?</h2>\n<p>Because they convert. A landing page is essentially the entry point to your site. A great landing page will <strong>grab a visitor’s attention and persuade them to take a desired action</strong>: perhaps you want them to sign up for your email list, download a brochure, buy a product, etc. Whatever the goal, a good landing page is always focused towards that goal.</p>\n<p>Just to make sure that you understand, let’s look at the difference between a landing page and the homepage of a site: <strong>a landing page is focused on a specific goal or objective</strong> while your main homepage has a broad objective. The aim of a landing page is to take your visitors through a specific marketing journey while the main homepage allows visitors to decide on their own journey through your site.</p>\n<p>Anyone can tell you that you don&#8217;t really need a landing page to get a visitor to take a specific action. You can actually make them take the desired action on your homepage.</p>\n<p>BUT, <strong>with a good landing page you will achieve a higher conversion rate</strong> because it is very specific in its goal. A homepage has a lot of information and elements that may distract a user from taking the desired action.</p>\n<p>So, are you ready to build one? Here are several ways to build landing pages in WordPress.</p>\n<h2>Building a Custom Page</h2>\n<p>Building a custom page within WordPress is the old-fashioned approach. You would simply just build a custom page template on any theme you have; and do whatever you need to in the template.</p>\n<p>The easiest way to build a custom page template in WordPress is to copy the default page PHP template and edit CSS and HTML components to get the desired design or structure.</p>\n<h2>Using Plugins to Build Landing Pages</h2>\n<p>Plugins are the best way to go if you do not want to worry about dealing with code.</p>\n<p>Unfortunately, there aren’t many free landing page builder plugins out there. And the few free ones often have in-plugin purchases or limited features. There is, however, one free plugin that has all the features you need to build a functional landing page in WordPress. It’s among the top landing page plugins (see below).</p>\n<h3>WordPress Landing Pages</h3>\n<p><a href="https://wordpress.org/plugins/landing-pages/" target="_blank" rel="noopener">WordPress Landing Pages</a> is an easy-to-use plugin that has <strong>all the basic features you need to build a functional landing page</strong>. It has a fair selection of templates, allows you to split test (a/b testing) your landing pages, as well as to track and measure conversions.</p>\n<div id="attachment_3482" style="width: 723px" class="wp-caption aligncenter"><img class="wp-image-3482" src="https://semperplugins.com/wp-content/uploads/2017/06/wordpress-landing-pages.png" alt="The WordPress Landing Pages plugin is fully integrated in the Theme Builder and allows you to quickly customize any elements you like." width="713" height="373" srcset="https://semperplugins.com/wp-content/uploads/2017/06/wordpress-landing-pages.png 1920w, https://semperplugins.com/wp-content/uploads/2017/06/wordpress-landing-pages-300x157.png 300w, https://semperplugins.com/wp-content/uploads/2017/06/wordpress-landing-pages-768x402.png 768w, https://semperplugins.com/wp-content/uploads/2017/06/wordpress-landing-pages-1024x537.png 1024w, https://semperplugins.com/wp-content/uploads/2017/06/wordpress-landing-pages-477x250.png 477w, https://semperplugins.com/wp-content/uploads/2017/06/wordpress-landing-pages-100x52.png 100w, https://semperplugins.com/wp-content/uploads/2017/06/wordpress-landing-pages-150x79.png 150w, https://semperplugins.com/wp-content/uploads/2017/06/wordpress-landing-pages-200x105.png 200w, https://semperplugins.com/wp-content/uploads/2017/06/wordpress-landing-pages-450x236.png 450w, https://semperplugins.com/wp-content/uploads/2017/06/wordpress-landing-pages-600x314.png 600w, https://semperplugins.com/wp-content/uploads/2017/06/wordpress-landing-pages-900x472.png 900w, https://semperplugins.com/wp-content/uploads/2017/06/wordpress-landing-pages-1200x630.png 1200w" sizes="(max-width: 713px) 100vw, 713px" /><p class="wp-caption-text">The WordPress Landing Pages plugin is fully integrated in the Theme Builder and allows you to quickly customize any elements you like.</p></div>\n<p>All in all, it is a good option if you are on a budget and don’t want to invest in a more functional landing page plugin at the moment.</p>\n<h3>Thrive Landing Pages</h3>\n<p><a href="https://thrivethemes.com/landingpages/" target="_blank" rel="noopener">Thrive Landing Pages</a> has a drag &amp; drop editor that makes it extremely easy to create and edit landing pages. It<strong> comes with over 140 templates</strong> (some themed) for you to choose from. The great thing about the plugin is that the landing pages are combined in sets to provide a consistent user experience. That means that the opt-in page, &#8220;thank you&#8221; page and confirmation pages in a particular set have the same feel and don’t provide random experiences.</p>\n<h3>Leadpages</h3>\n<p>Leadpages is actually an online landing page builder &#8211; we’ll be addressing it more thoroughly down below &#8211; with a plugin for WordPress. It has a centralized editor and your <strong>landing pages are hosted on Leadpages servers</strong>. The best thing about Leadpages is that you can easily and quickly create or publish a landing page. All you need to do is choose a template, customize it (through the drag &amp; drop page builder) and add it to your site.</p>\n<h3>Beaver Builder</h3>\n<p><a href="https://www.wpbeaverbuilder.com/" target="_blank" rel="noopener">Beaver Builder</a> is simply a drag &amp; drop page theme builder (another option covered below) that also comes as a plugin. You can easily and quickly create custom layouts and designs &#8211; you have around 30 landing pages to choose from.</p>\n<div id="attachment_3483" style="width: 734px" class="wp-caption aligncenter"><img class="wp-image-3483" src="https://semperplugins.com/wp-content/uploads/2017/06/beaver-builder.jpg" alt="You can try out the Beaver Builder plugin for yourself at http://demo.wpbeaverbuilder.com/." width="724" height="361" srcset="https://semperplugins.com/wp-content/uploads/2017/06/beaver-builder.jpg 1917w, https://semperplugins.com/wp-content/uploads/2017/06/beaver-builder-300x150.jpg 300w, https://semperplugins.com/wp-content/uploads/2017/06/beaver-builder-768x383.jpg 768w, https://semperplugins.com/wp-content/uploads/2017/06/beaver-builder-1024x511.jpg 1024w, https://semperplugins.com/wp-content/uploads/2017/06/beaver-builder-501x250.jpg 501w, https://semperplugins.com/wp-content/uploads/2017/06/beaver-builder-100x50.jpg 100w, https://semperplugins.com/wp-content/uploads/2017/06/beaver-builder-150x75.jpg 150w, https://semperplugins.com/wp-content/uploads/2017/06/beaver-builder-200x100.jpg 200w, https://semperplugins.com/wp-content/uploads/2017/06/beaver-builder-450x225.jpg 450w, https://semperplugins.com/wp-content/uploads/2017/06/beaver-builder-600x300.jpg 600w, https://semperplugins.com/wp-content/uploads/2017/06/beaver-builder-900x449.jpg 900w" sizes="(max-width: 724px) 100vw, 724px" /><p class="wp-caption-text">You can try out the Beaver Builder plugin for yourself at <a href="http://demo.wpbeaverbuilder.com/" target="_blank" rel="noopener">demo.wpbeaverbuilder.com</a>.</p></div>\n<p>The great thing about Beaver Builder is that <strong>you can use your WordPress widgets to add more functionality</strong> and bring elements in from your site to your landing pages.</p>\n<h3>OptimizePress</h3>\n<p>You’ve probably heard of <a href="https://www.optimizepress.com/" target="_blank" rel="noopener">OptimizePress</a> before &#8211; it has been <strong>one of the most popular plugins</strong> to build landing pages for quite some time. That’s because it offers a wide range of templates and a powerful drag &amp; drop editor to quickly create the landing page you need. Note: the size of the plugin is quite large (29MB).</p>\n<h2>Theme Builders</h2>\n<p>As we mentioned earlier, <strong>most WordPress themes are quite restrictive when it comes to design and structure</strong>. For example, you can customize the primary content block of a page in most themes but you cannot amend the design of anything outside the block. So, if you aim to create a landing page from a theme page template, you would just end up with a new page with the same design as your website &#8211; the opposite of a landing page. Unless you build a custom page template (mentioned earlier) or use advanced theme builders.</p>\n<p>What advanced theme builders offer are drag and drop builders. They essentially enable you to customize every and any design aspect of any page. The interface is also very easy to use.<br />\nThere are plenty of great drag &amp; drop theme builders available. A few off the top of my head: <a href="https://www.elegantthemes.com/gallery/divi/" target="_blank" rel="noopener">Divi</a>, <a href="https://wpexplorer-themes.com/total/" target="_blank" rel="noopener">Total</a>, Beaver Builder (its plugin version is recommended above) and <a href="http://demo.qodeinteractive.com/strata/" target="_blank" rel="noopener">Strata</a>.</p>\n<p>If you need a variety of landing page templates, it is highly recommended you use this method instead of creating custom theme templates. It will save you time and make your work much easier.</p>\n<h2>Leadpages</h2>\n<p><a href="https://www.leadpages.net/" target="_blank" rel="noopener">Leadpages is known for creating stunning and functional landing pages</a>. It offers dozens of integrations to help make your WordPress landing page user friendly and attractive. The downside is that it isn’t free: plans start at $36.99 per month.</p>\n<div id="attachment_3484" style="width: 743px" class="wp-caption aligncenter"><img class="wp-image-3484" src="https://semperplugins.com/wp-content/uploads/2017/06/leadpages.jpg" alt="Leadpages has more than a hundred of templates to choose from that can be added to your site within minutes." width="733" height="362" srcset="https://semperplugins.com/wp-content/uploads/2017/06/leadpages.jpg 1877w, https://semperplugins.com/wp-content/uploads/2017/06/leadpages-300x148.jpg 300w, https://semperplugins.com/wp-content/uploads/2017/06/leadpages-768x379.jpg 768w, https://semperplugins.com/wp-content/uploads/2017/06/leadpages-1024x506.jpg 1024w, https://semperplugins.com/wp-content/uploads/2017/06/leadpages-506x250.jpg 506w, https://semperplugins.com/wp-content/uploads/2017/06/leadpages-100x49.jpg 100w, https://semperplugins.com/wp-content/uploads/2017/06/leadpages-150x74.jpg 150w, https://semperplugins.com/wp-content/uploads/2017/06/leadpages-200x99.jpg 200w, https://semperplugins.com/wp-content/uploads/2017/06/leadpages-450x222.jpg 450w, https://semperplugins.com/wp-content/uploads/2017/06/leadpages-600x296.jpg 600w, https://semperplugins.com/wp-content/uploads/2017/06/leadpages-900x444.jpg 900w" sizes="(max-width: 733px) 100vw, 733px" /><p class="wp-caption-text">Leadpages has more than a hundred of templates to choose from that can be added to your site within minutes.</p></div>\n<p>What you get is <strong>144+ well-designed templates</strong>, a simple editor that enables you to create and <strong>publish a stunning landing page in 10 minutes</strong> or less and the ability to split test your landing pages.</p>\n<p>It’s not a stretch to say that Leadpages is the most popular landing page option. It is actually hard to find a popular blog online that doesn’t use it. Two of the many reasons for it’s popularity is that you do not need to be a designer to use it and all their templates have been tested as effective and high converting templates.</p>\n<p>The price may be worth it to you if you want to save time, as the platform is incredibly easy to use.</p>\n<h2>Wrapping Up</h2>\n<p>Although WordPress may not be designed to handle landing pages, you can easily create one using either the old-fashioned method of building a custom page template or by using a plugin. Above we mentioned some of the best plugins you can use to build highly functional and attractive landing pages, developer or not.</p>\n<p>Choose the method that works best for your business and start collecting, cultivating and converting leads!</p>\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:69:"https://semperplugins.com/how-to-build-a-wordpress-landing-page/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:2:"35";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:4;a:6:{s:4:"data";s:51:"\n		\n		\n		\n		\n		\n				\n		\n		\n		\n\n		\n		\n				\n			\n		\n		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:5:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:52:"WordPress Will Require Users to Have HTTPS This Year";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:64:"https://semperplugins.com/wordpress-requires-sites-to-use-https/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:73:"https://semperplugins.com/wordpress-requires-sites-to-use-https/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 06 Jun 2017 18:32:54 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:4:{i:0;a:5:{s:4:"data";s:8:"Security";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:14:"WordPress News";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:2;a:5:{s:4:"data";s:5:"HTTPS";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:3;a:5:{s:4:"data";s:18:"WordPress security";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"https://semperplugins.com/?p=3129/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:471:"WordPress founder, Matt Mullenweg, recently announced that the software will require all hosts to have HTTPS for certain WordPress features to function. Don’t panic just yet. If you already have HTTPS, this shouldn’t affect you. But if you’re still using HTTP, you’ll need<span class="ellipsis">&#8230;</span><div class="read-more"><a href="https://semperplugins.com/wordpress-requires-sites-to-use-https/">Read more &#8250;</a></div><!-- end of .read-more -->";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:12:"Arnaud Broes";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:12957:"<p><img class="size-full wp-image-3666 aligncenter" src="https://semperplugins.com/wp-content/uploads/2017/09/wordpress-requires-https-featured.jpg" alt="HTTPS will not only be an important ranking factor in the future for search machines, but also required for both existing and new WordPress installations." width="850" height="250" srcset="https://semperplugins.com/wp-content/uploads/2017/09/wordpress-requires-https-featured.jpg 850w, https://semperplugins.com/wp-content/uploads/2017/09/wordpress-requires-https-featured-300x88.jpg 300w, https://semperplugins.com/wp-content/uploads/2017/09/wordpress-requires-https-featured-768x226.jpg 768w, https://semperplugins.com/wp-content/uploads/2017/09/wordpress-requires-https-featured-100x29.jpg 100w, https://semperplugins.com/wp-content/uploads/2017/09/wordpress-requires-https-featured-150x44.jpg 150w, https://semperplugins.com/wp-content/uploads/2017/09/wordpress-requires-https-featured-200x59.jpg 200w, https://semperplugins.com/wp-content/uploads/2017/09/wordpress-requires-https-featured-450x132.jpg 450w, https://semperplugins.com/wp-content/uploads/2017/09/wordpress-requires-https-featured-600x176.jpg 600w" sizes="(max-width: 850px) 100vw, 850px" /></p>\n<p>WordPress founder, Matt Mullenweg, <a href="https://wordpress.org/news/2016/12/moving-toward-ssl/">recently announced that the software will require all hosts to have HTTPS for certain WordPress features</a> to function.</p>\n<p>Don’t panic just yet. If you already have HTTPS, this shouldn’t affect you. <strong>But if you’re still using HTTP, you’ll need to upgrade soon.</strong> The good news is that the transition is not as difficult as you think it is and the benefits outweigh the assumed disadvantages.</p>\n<p>This article will go over what the WordPress HTTPS mandate means for you as a site owner; the advantages; as well as how to upgrade to HTTPS if you haven’t already done so.</p>\n<h2>What is HTTPS?</h2>\n<p>HTTPS adds a security layer to HTTP (Hypertext Transfer Protocol). <strong>HTTPS essentially encrypts data</strong> (using SSL or TSL) that is communicated between servers and clients until it reaches the intended recipient.</p>\n<p>This<strong> prevents cybercriminals from accessing sensitive user information</strong> and also reduces the risk of tapping and modification of sensitive data. Although HTTPS is not completely foolproof, it undoubtedly has major security advantages.</p>\n<p>HTTPS sites can be easily identified, as they have a locked padlock icon located on the link bar in most common browsers.</p>\n<h2>Why is WordPress Pushing HTTPS?</h2>\n<p>There&#8217;s mainly two reasons for this, so let&#8217;s quickly dive into them.</p>\n<h3>Google Prefers It</h3>\n<p>It is no secret that greater encryption and cyber security has made the Internet a safer place for users. As usual, a Google update signaled the necessity of HTTPS for user experience, SEO and internet security.</p>\n<p>In 2014, Google suggested that <strong>enabling HTTPS on your site could result in higher search rankings</strong>. Although it still isn’t the only important factor in raising your site rankings, you shouldn’t underestimate its value. For example, if two sites are equal in all ways, but one site has HTTPS, that site would get a boost in rankings.</p>\n<div id="attachment_3342" style="width: 511px" class="wp-caption aligncenter"><img class="wp-image-3342" src="https://semperplugins.com/wp-content/uploads/2017/03/https-green-padlock.png" alt="Chrome will display a green padlock in the link bar when a site is using HTTPS, assuring users it''s using the latest security protocol." width="501" height="217" srcset="https://semperplugins.com/wp-content/uploads/2017/03/https-green-padlock.png 1138w, https://semperplugins.com/wp-content/uploads/2017/03/https-green-padlock-300x130.png 300w, https://semperplugins.com/wp-content/uploads/2017/03/https-green-padlock-768x333.png 768w, https://semperplugins.com/wp-content/uploads/2017/03/https-green-padlock-1024x444.png 1024w, https://semperplugins.com/wp-content/uploads/2017/03/https-green-padlock-577x250.png 577w, https://semperplugins.com/wp-content/uploads/2017/03/https-green-padlock-100x43.png 100w, https://semperplugins.com/wp-content/uploads/2017/03/https-green-padlock-150x65.png 150w, https://semperplugins.com/wp-content/uploads/2017/03/https-green-padlock-200x87.png 200w, https://semperplugins.com/wp-content/uploads/2017/03/https-green-padlock-450x195.png 450w, https://semperplugins.com/wp-content/uploads/2017/03/https-green-padlock-600x260.png 600w, https://semperplugins.com/wp-content/uploads/2017/03/https-green-padlock-900x390.png 900w" sizes="(max-width: 501px) 100vw, 501px" /><p class="wp-caption-text">Chrome will display a green padlock in the link bar when a site is using HTTPS, assuring users it&#8217;s using the latest security protocol.</p></div>\n<p>In January of this year, Google released version 56 of Google Chrome. This new release brought about some changes, notably with how Google Chrome treats HTTPS vs. HTTP sites. The browser now clearly identifies sites that are not operating HTTPS on their systems. For example, <strong>a “Not Secure” message now appears on pages without HTTPS that try to collect passwords or sensitive information</strong>. You can expect that, eventually, all pages not using HTTPS will clearly be labeled as having insecure connections.</p>\n<p>We can reasonably assume that Google’s preference for HTTPS has been a contributing factor for the changes implemented by WordPress.</p>\n<h3>Users Prefer HTTPS Too</h3>\n<p>A secure connection can make all the difference from a user&#8217;s perspective. Users see HTTPS as a positive signal that you are taking your site security seriously, for their benefit. So, having <strong>HTTPS could mean more traffic and longer usage times</strong> on your site.</p>\n<p>HTTPS is particularly important if you are operating an e-commerce site. Simply seeing the padlock icon could make users more comfortable in entering their payment details and other personal information. Particularly with the new Chrome update (mentioned earlier) which shows a “Not Secure” label on e-commerce sites or sites that require a user login or credit card information, but don’t have HTTPS.</p>\n<p>Both Google and user preference should be enough reason for you to upgrade your site to HTTPS. It is simply necessary to ensure watertight security for your users and to protect your online business reputation.</p>\n<div id="attachment_3343" style="width: 510px" class="wp-caption aligncenter"><img class=" wp-image-3343" src="https://semperplugins.com/wp-content/uploads/2017/03/https-not-secure.png" alt="Sites that require users to login or enter credit card information are now displayed as &quot;Not secure&quot; in Chrome when they haven''t switched to HTTPS yet." width="500" height="196" srcset="https://semperplugins.com/wp-content/uploads/2017/03/https-not-secure.png 1600w, https://semperplugins.com/wp-content/uploads/2017/03/https-not-secure-300x118.png 300w, https://semperplugins.com/wp-content/uploads/2017/03/https-not-secure-768x301.png 768w, https://semperplugins.com/wp-content/uploads/2017/03/https-not-secure-1024x401.png 1024w, https://semperplugins.com/wp-content/uploads/2017/03/https-not-secure-638x250.png 638w, https://semperplugins.com/wp-content/uploads/2017/03/https-not-secure-100x39.png 100w, https://semperplugins.com/wp-content/uploads/2017/03/https-not-secure-150x59.png 150w, https://semperplugins.com/wp-content/uploads/2017/03/https-not-secure-200x78.png 200w, https://semperplugins.com/wp-content/uploads/2017/03/https-not-secure-450x176.png 450w, https://semperplugins.com/wp-content/uploads/2017/03/https-not-secure-600x235.png 600w, https://semperplugins.com/wp-content/uploads/2017/03/https-not-secure-900x353.png 900w" sizes="(max-width: 500px) 100vw, 500px" /><p class="wp-caption-text">Sites that require users to login or enter credit card information are now displayed as &#8220;Not secure&#8221; in Chrome when they haven&#8217;t switched to HTTPS yet.</p></div>\n<p>Remember when JavaScript was first introduced and quickly embraced by users and webmasters? Looking back, we can see now that JavaScript was essential for smoother and better user experience. HTTPS similarly, presents a number of unique advantages for user experience and security that we should all quickly embrace.</p>\n<p>We know that you may be overwhelmed switching from HTTP to HTTPS. After all, change does takes time to get used to, but in this instance, you may need to quickly get on board. At this point, <strong>the advantages of HTTPS have greatly outnumbered the disadvantages</strong>. Plus, upgrading to HTTPS is no longer the costly, time consuming, and difficult process that it once was. In fact, getting an SSL certificate in 2017 is fast, sometimes free, and quite easy to implement.</p>\n<h2>How to Get HTTPS</h2>\n<p><a href="https://wordpress.org/hosting/" target="_blank" rel="noopener noreferrer">WordPress hosting partners</a> should now provide an SSL certificate for all accounts. (It is required that they all do so as early as the first quarter of this year.)</p>\n<p>Your hosting provider may already provide a free SSL certificate, so check with them first before you make any third-party purchase. If they do not offer a free one, you could ask them if they sell third party SSL certificates. Once purchased, you can ask your provider to install the certificate for you on your server.</p>\n<div id="attachment_3344" style="width: 510px" class="wp-caption aligncenter"><img class=" wp-image-3344" src="https://semperplugins.com/wp-content/uploads/2017/03/lets-encrypt-sponsors.png" alt="Dozens of major companies are already backing Lets Encrypt, including Automattic (known for WooCommerce, Jetpack, Akismet and WordPress.com)" width="500" height="395" srcset="https://semperplugins.com/wp-content/uploads/2017/03/lets-encrypt-sponsors.png 546w, https://semperplugins.com/wp-content/uploads/2017/03/lets-encrypt-sponsors-300x237.png 300w, https://semperplugins.com/wp-content/uploads/2017/03/lets-encrypt-sponsors-317x250.png 317w, https://semperplugins.com/wp-content/uploads/2017/03/lets-encrypt-sponsors-100x79.png 100w, https://semperplugins.com/wp-content/uploads/2017/03/lets-encrypt-sponsors-150x118.png 150w, https://semperplugins.com/wp-content/uploads/2017/03/lets-encrypt-sponsors-200x158.png 200w, https://semperplugins.com/wp-content/uploads/2017/03/lets-encrypt-sponsors-450x355.png 450w" sizes="(max-width: 500px) 100vw, 500px" /><p class="wp-caption-text">Dozens of major companies are already backing Lets Encrypt, including Automattic (known for WooCommerce, Akismet and WordPress.com)</p></div>\n<p>Another option is to explore the free alternatives, independent of your hosting provider. There are projects such as “Let’s Encrypt” which have now made it easy and quick to <a href="https://letsencrypt.org/" target="_blank" rel="noopener noreferrer">secure a free HTTPS certificate for your website</a>.</p>\n<p>Let’s Encrypt is an authorized open Certificate Authority with millions of active certificates in place. There are other comparable projects out there that can help by guiding you step-by-step through the installation process or who have been authorized to deliver certificates.</p>\n<p>Remember that SSL certificates upgrade the website, but not the content itself. That means that the content on your page will also need to be updated so as to avoid 404 errors. Google may interpret the error as a mismatch in the security level of your site. The only way to avoid this is by encrypting the content of your website to match your SSL certificate.</p>\n<p>To track and resolve any 404 errors on your site, you may want to use a specialised plugin such as Redirection to do so.</p>\n<h2>What if You Just Don’t Want to Upgrade to HTTPS?</h2>\n<p>You could see a number of things happening to your site over time if you do not upgrade to HTTPS. The first may be facing the consequences set out by Google, i.e: <strong>lower rankings </strong>and having your users staring at a “Not Secure” warning when they try to access your site via Google Chrome.</p>\n<p>The second is that you could <strong>struggle with WordPress updates and lose some or all functionality</strong> on specific WordPress plugins.</p>\n<p>Third, your site may be <strong>an easier target for hacking</strong>.</p>\n<p>Those are three consequences that require you to seriously reconsider if you really want to take the risk of not upgrading to HTTPS.</p>\n<h2>Wrapping Up</h2>\n<p>Let’s put it this way: you will simply have nothing to lose by adopting HTTPS. Yet, if you do not use HTTPS, you could risk leaving your site in the “dark ages” of the Internet.</p>\n<p>But then again, if you’re a WordPress site owner, you have no choice. Take the plunge and let us know how it worked out for you!</p>\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:69:"https://semperplugins.com/wordpress-requires-sites-to-use-https/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:2:"53";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:5;a:6:{s:4:"data";s:51:"\n		\n		\n		\n		\n		\n				\n		\n		\n		\n\n		\n		\n				\n			\n		\n		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:5:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:44:"All in One SEO Pack at WordCamp Raleigh 2017";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:71:"https://semperplugins.com/all-in-one-seo-pack-at-wordcamp-raleigh-2017/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:80:"https://semperplugins.com/all-in-one-seo-pack-at-wordcamp-raleigh-2017/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 30 May 2017 15:42:28 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:4:{i:0;a:5:{s:4:"data";s:19:"All in One SEO Pack";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:4:"Blog";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:2;a:5:{s:4:"data";s:16:"WordCamp Raleigh";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:3;a:5:{s:4:"data";s:13:"WordPress SEO";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"https://semperplugins.com/?p=3426/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:417:"Last month, the team from All in One SEO Pack were in attendance at WordCamp Raleigh 2017.  We were not only there are organizers of the event but also as sponsors and speakers. If you didn&#8217;t get a chance to attend<span class="ellipsis">&#8230;</span><div class="read-more"><a href="https://semperplugins.com/all-in-one-seo-pack-at-wordcamp-raleigh-2017/">Read more &#8250;</a></div><!-- end of .read-more -->";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:14:"Steve Mortiboy";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:3680:"<p>Last month, the team from All in One SEO Pack were in attendance at <a href="https://2017.raleigh.wordcamp.org/" target="_blank" rel="noopener noreferrer">WordCamp Raleigh 2017</a>.  We were not only there are organizers of the event but also as sponsors and speakers.</p>\n<p>If you didn&#8217;t get a chance to attend WordCamp Raleigh then here&#8217;s a recap of the event.</p>\n<p>WordCamp Raleigh is a conference all about WordPress.  The event has been held each year since 2010.  This year was the best attended yet, with over 300 attendees.  This year we had four tracks &#8211; Beginner, Business, Power User and Developer. We also added workshops in addition to the tracks.  There were three workshops &#8211; Beginners Guide to WordPress, WordPress REST API and Beginners Guide to WordPress SEO.  All three were extremely popular.</p>\n<h2>Local SEO presentation by Steve Mortiboy</h2>\n<p>I spoke on the topic of Local SEO &#8211; How to get your business listed on Google&#8217;s local search results.  Here are my slides from this presentation:</p>\n<p><iframe src="https://www.slideshare.net/slideshow/embed_code/key/4Ssk3Lm2vq2l18" width="427" height="356" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> </p>\n<div style="margin-bottom:5px"> <strong> <a href="https://www.slideshare.net/dismort/local-seo-getting-your-local-business-on-google" title="Local SEO - Getting your local business on google" target="_blank">Local SEO &#8211; Getting your local business on google</a> </strong> from <strong><a href="https://www.slideshare.net/dismort" target="_blank">Steve Mortiboy</a></strong> </div>\n<p>Here is the video from my presentation on WordPress.tv:</p>\n<p><iframe width=''605'' height=''340'' src=''https://videopress.com/embed/u6Rx29m9?hd=0'' frameborder=''0'' allowfullscreen></iframe><script src=''https://v0.wordpress.com/js/next/videopress-iframe.js?m=1435166243''></script></p>\n<h2>Social Meta Optimization by Tony Zeoli</h2>\n<p>In addition to my session, Tony Zeoli also gave a presentation on Social Meta Optimization where he used All in One SEO Pack to demonstrate how you can improve you social media presence.  You can find more about Tony&#8217;s presentation on his <a href="https://tonyzeoli.com/2017/05/28/social-meta-optimization-presentation-at-wordcamp-raleigh-2017/2062/" target="_blank" rel="noopener noreferrer">website</a>.  The video from his presentation is also on WordPress.tv here:</p>\n<p><iframe width=''605'' height=''340'' src=''https://videopress.com/embed/rG2jN5os?hd=0'' frameborder=''0'' allowfullscreen></iframe><script src=''https://v0.wordpress.com/js/next/videopress-iframe.js?m=1435166243''></script></p>\n<h2>Beginners Guide to SEO by Tony Zeoli</h2>\n<p>Finally, Tony Zeoli also presented the Beginners Guide to WordPress SEO workshop which was held on the Sunday at WordCamp Raleigh.  This workshop was not recorded on video but you can find a write by Tony and links to his slides on his <a href="https://tonyzeoli.com/2017/05/20/wordcamp-raleigh-2017-wordpress-seo-for-beginners-workshop/1998/" target="_blank" rel="noopener noreferrer">website</a>.</p>\n<p>If you didn&#8217;t get a chance to attend WordCamp Raleigh this year, keep a watch on the <a href="https://2017.raleigh.wordcamp.org/" target="_blank" rel="noopener noreferrer">website</a> for announcements regarding WordCamp Raleigh 2018.  Or you can find a WordCamp in your city or country by checking out the central <a href="https://central.wordcamp.org/schedule/" target="_blank" rel="noopener noreferrer">WordCamp schedule</a>.</p>\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:76:"https://semperplugins.com/all-in-one-seo-pack-at-wordcamp-raleigh-2017/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"4";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:6;a:6:{s:4:"data";s:45:"\n		\n		\n		\n		\n		\n				\n		\n\n		\n		\n				\n			\n		\n		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:5:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:48:"Important Sections of Google Analytics Explained";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:75:"https://semperplugins.com/important-sections-of-google-analytics-explained/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:84:"https://semperplugins.com/important-sections-of-google-analytics-explained/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 24 May 2017 15:00:49 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:2:{i:0;a:5:{s:4:"data";s:4:"Blog";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:16:"google analytics";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:32:"http://semperplugins.com/?p=2298";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:443:"Google Analytics (GA) is one of the most powerful, effective and comprehensive platforms to track and analyze your site traffic and marketing efforts. But a lot of businesses find it intimidating and challenging to use. If you’re one of those<span class="ellipsis">&#8230;</span><div class="read-more"><a href="https://semperplugins.com/important-sections-of-google-analytics-explained/">Read more &#8250;</a></div><!-- end of .read-more -->";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:12:"Arnaud Broes";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:13749:"<p><img class="size-full wp-image-3381 aligncenter" src="https://semperplugins.com/wp-content/uploads/2016/08/important-sections-of-google-analytics-featured.jpg" alt="" width="851" height="251" srcset="https://semperplugins.com/wp-content/uploads/2016/08/important-sections-of-google-analytics-featured.jpg 851w, https://semperplugins.com/wp-content/uploads/2016/08/important-sections-of-google-analytics-featured-300x88.jpg 300w, https://semperplugins.com/wp-content/uploads/2016/08/important-sections-of-google-analytics-featured-768x227.jpg 768w, https://semperplugins.com/wp-content/uploads/2016/08/important-sections-of-google-analytics-featured-848x250.jpg 848w, https://semperplugins.com/wp-content/uploads/2016/08/important-sections-of-google-analytics-featured-100x29.jpg 100w, https://semperplugins.com/wp-content/uploads/2016/08/important-sections-of-google-analytics-featured-150x44.jpg 150w, https://semperplugins.com/wp-content/uploads/2016/08/important-sections-of-google-analytics-featured-200x59.jpg 200w, https://semperplugins.com/wp-content/uploads/2016/08/important-sections-of-google-analytics-featured-450x133.jpg 450w, https://semperplugins.com/wp-content/uploads/2016/08/important-sections-of-google-analytics-featured-600x177.jpg 600w" sizes="(max-width: 851px) 100vw, 851px" /></p>\n<p>Google Analytics (GA) is <strong>one of the most powerful, effective and comprehensive platforms</strong> to track and analyze your site traffic and marketing efforts. But a lot of businesses find it intimidating and challenging to use. If you’re one of those businesses, this article is for you.</p>\n<p>Although GA can be quite overwhelming, getting useful data to work with is not as difficult as you think it is. Understanding how to get some basic information from the platform can give you pretty good insight into how your website is working for your business.</p>\n<p>This article will give you a basic understanding of the four key sections that contain actionable data:</p>\n<p style="padding-left: 30px;">1. Audience<br />\n2. Behavior<br />\n3. Acquisition<br />\n4. Conversions</p>\n<p>Let’s get started.</p>\n<h2><strong>An Introduction to Google Analytics</strong></h2>\n<p>You probably have a Google Analytics account and most likely have it connected to your website. If you don’t, and you’re thinking of signing up, <a href="https://support.google.com/analytics/answer/1008015?hl=en" target="_blank" rel="noopener noreferrer">use this guide on how to get started</a>. Once you’re up and running, you’ll have to wait a few days to get data to analyze.</p>\n<p>The first thing you’ll notice when you open the platform is GA’s (somewhat confusing) layout and menu system. It uses a top navigation for account level features and information; and a side navigation for combing through your analytics data.</p>\n<p>The Reporting section’s side navigation (which is where you can view your web analytics) is broken down into the following sections:</p>\n<ul>\n<li><strong>Real-Time:</strong> Shows what is happening on your site right now. So you’ll see the current visitor count on your website, what pages they’re browsing and for how long</li>\n<li><strong>Audience:</strong> Gives detailed information about your website visitors</li>\n<li><strong>Acquisition:</strong> Shows how your visitors are arriving to your site. It gives you a breakdown (by channel) of how visitors are getting to your site</li>\n<li><strong>Behavior:</strong> Shows you how visitors are interacting with your site</li>\n<li><strong>Conversions:</strong> Provides conversion data for your various business goals and activities</li>\n</ul>\n<p>As you can see, Audience, Acquisition, Behavior and Conversions tabs contain the <strong>actionable data you can use to optimize your site and grow your business.</strong></p>\n<p>So let’s help you get a basic understanding of those tabs.</p>\n<h2><strong>Audience</strong></h2>\n<p>To view your audience insights, click on Audience &gt; Overview on the left hand menu of the analytics dashboard.</p>\n<p>As you will see from the Audience Overview data, Google Analytics gives you detailed insights into your audience: from location, to page views to device type to language and more.</p>\n<p>This information is invaluable in allowing you to tailor your site<strong> to better suit the needs of your visitors and effectively grant them an easier viewing experience</strong>.</p>\n<p><a href="https://semperplugins.com/wp-content/uploads/2016/08/google-analytics-audience-section.jpg"><img class="aligncenter wp-image-3405" src="https://semperplugins.com/wp-content/uploads/2016/08/google-analytics-audience-section.jpg" alt="" width="600" height="255" srcset="https://semperplugins.com/wp-content/uploads/2016/08/google-analytics-audience-section.jpg 1621w, https://semperplugins.com/wp-content/uploads/2016/08/google-analytics-audience-section-300x128.jpg 300w, https://semperplugins.com/wp-content/uploads/2016/08/google-analytics-audience-section-768x327.jpg 768w, https://semperplugins.com/wp-content/uploads/2016/08/google-analytics-audience-section-1024x436.jpg 1024w, https://semperplugins.com/wp-content/uploads/2016/08/google-analytics-audience-section-587x250.jpg 587w, https://semperplugins.com/wp-content/uploads/2016/08/google-analytics-audience-section-100x43.jpg 100w, https://semperplugins.com/wp-content/uploads/2016/08/google-analytics-audience-section-150x64.jpg 150w, https://semperplugins.com/wp-content/uploads/2016/08/google-analytics-audience-section-200x85.jpg 200w, https://semperplugins.com/wp-content/uploads/2016/08/google-analytics-audience-section-450x192.jpg 450w, https://semperplugins.com/wp-content/uploads/2016/08/google-analytics-audience-section-600x255.jpg 600w, https://semperplugins.com/wp-content/uploads/2016/08/google-analytics-audience-section-900x383.jpg 900w" sizes="(max-width: 600px) 100vw, 600px" /></a></p>\n<p>There is however some terminology you first need to understand in order to accurately analyze your Audience data:</p>\n<ul>\n<li>A <strong>Session</strong> refers to a single fixed period of time that a visitor takes action(s) on your site. So if a visitor views 3 pages on your site over a 3-minute period and then exits your site, it’ll be recorded as a single session and three pageviews</li>\n<li>As you may have gleaned from above, a <strong>Pageview</strong> is recorded whenever a visitor views a page on your site</li>\n<li>A <strong>New Visitor</strong> is any user that is visiting your site for the first time. They will be classified as a <strong>Returning Visitor</strong> on their second and subsequent visits</li>\n<li>A <strong>User</strong> is any computer or device that Google classifies as a unique visitor in accessing your site. So let’s say a user views your site on both their computer and tablet. Google will record that as two users. Even if a user accesses your site from two different browsers on the same computer, Google will record it as two users</li>\n<li><strong>Pages/Session</strong> is the average number of pageviews per session</li>\n</ul>\n<p>It is recommended that you spend some time understanding these definitions before you begin analyzing the comprehensive audience data.</p>\n<h2><strong>Behavior</strong></h2>\n<p><a href="https://semperplugins.com/wp-content/uploads/2016/08/google-analytics-behaviour-section.jpg"><img class="aligncenter wp-image-3407" src="https://semperplugins.com/wp-content/uploads/2016/08/google-analytics-behaviour-section.jpg" alt="" width="600" height="286" srcset="https://semperplugins.com/wp-content/uploads/2016/08/google-analytics-behaviour-section.jpg 1621w, https://semperplugins.com/wp-content/uploads/2016/08/google-analytics-behaviour-section-300x143.jpg 300w, https://semperplugins.com/wp-content/uploads/2016/08/google-analytics-behaviour-section-768x367.jpg 768w, https://semperplugins.com/wp-content/uploads/2016/08/google-analytics-behaviour-section-1024x489.jpg 1024w, https://semperplugins.com/wp-content/uploads/2016/08/google-analytics-behaviour-section-524x250.jpg 524w, https://semperplugins.com/wp-content/uploads/2016/08/google-analytics-behaviour-section-100x48.jpg 100w, https://semperplugins.com/wp-content/uploads/2016/08/google-analytics-behaviour-section-150x72.jpg 150w, https://semperplugins.com/wp-content/uploads/2016/08/google-analytics-behaviour-section-200x95.jpg 200w, https://semperplugins.com/wp-content/uploads/2016/08/google-analytics-behaviour-section-450x215.jpg 450w, https://semperplugins.com/wp-content/uploads/2016/08/google-analytics-behaviour-section-600x286.jpg 600w, https://semperplugins.com/wp-content/uploads/2016/08/google-analytics-behaviour-section-900x430.jpg 900w" sizes="(max-width: 600px) 100vw, 600px" /></a></p>\n<p>The Behavior section gives you <strong>invaluable data on how your users are interacting with your site.</strong> You can track how long an average user stays on a specific page, the hours and days you receive the most traffic, bounce rates (the percentage of visitors who exit your site after viewing only one page), your site speed, and much more detailed and useful metrics.</p>\n<p>Spend some time exploring and familiarizing yourself with this section. It contains everything you need to know to optimize your site and user experience, and also to manage your blog or content pages.</p>\n<h2><strong>Acquisition</strong></h2>\n<p><a href="https://semperplugins.com/wp-content/uploads/2016/08/google-analytics-acquisition-section.jpg"><img class="aligncenter wp-image-3409" src="https://semperplugins.com/wp-content/uploads/2016/08/google-analytics-acquisition-section.jpg" alt="" width="600" height="297" srcset="https://semperplugins.com/wp-content/uploads/2016/08/google-analytics-acquisition-section.jpg 1609w, https://semperplugins.com/wp-content/uploads/2016/08/google-analytics-acquisition-section-300x149.jpg 300w, https://semperplugins.com/wp-content/uploads/2016/08/google-analytics-acquisition-section-768x380.jpg 768w, https://semperplugins.com/wp-content/uploads/2016/08/google-analytics-acquisition-section-1024x507.jpg 1024w, https://semperplugins.com/wp-content/uploads/2016/08/google-analytics-acquisition-section-505x250.jpg 505w, https://semperplugins.com/wp-content/uploads/2016/08/google-analytics-acquisition-section-100x50.jpg 100w, https://semperplugins.com/wp-content/uploads/2016/08/google-analytics-acquisition-section-150x74.jpg 150w, https://semperplugins.com/wp-content/uploads/2016/08/google-analytics-acquisition-section-200x99.jpg 200w, https://semperplugins.com/wp-content/uploads/2016/08/google-analytics-acquisition-section-450x223.jpg 450w, https://semperplugins.com/wp-content/uploads/2016/08/google-analytics-acquisition-section-600x297.jpg 600w, https://semperplugins.com/wp-content/uploads/2016/08/google-analytics-acquisition-section-900x446.jpg 900w" sizes="(max-width: 600px) 100vw, 600px" /></a></p>\n<p>The Acquisition section is pretty straightforward: it contains <strong>data on how your site acquires traffic</strong>. It records the following traffic channels: organic (search and direct), paid, referral, email and social. It also gives you important insights into the performance of your marketing campaigns.</p>\n<p>What you should really be looking at within the Acquisition section is Goals. Goals are what you hope visitors accomplish when or after using your site. They are summarized in the Overview of this section but aren’t presented or broken down in detail. That’s where the Conversions section comes in.</p>\n<h2><strong>Conversions</strong></h2>\n<p>Conversion tracking is arguably the most important functionality in Google Analytics. After all, the ultimate goal of any web analytics software/tool is <strong>to help you determine and increase the success of your website and business</strong>.</p>\n<p>To make the most out of Google Analytics, you need to understand and identify your key performance indicators (KPIs) and set them up as Goals. Goals could range from video views, newsletter signups, purchases and more.</p>\n<p>Carefully think about how you measure the success of your business before setting up your Goals in GA. An accurate setup will enable you to more effectively analyze your website traffic data.</p>\n<h2>Using All in One SEO Pack with Google Analytics</h2>\n<p>By now you should have already discovered that Google Analytics is a great tool to track your visitors behaviour. If you are using WordPress for your site and have decided to venture into the statistics, <a href="https://semperplugins.com/all-in-one-seo-pack-pro-version/">we recommend you use our All in One SEO Pack plugin</a>.</p>\n<p>It offers a simple way to integrate Google Analytics on your site without having to risk breaking your site or messing with code. The whole process is very simple and is described <a href="https://semperplugins.com/documentation/setting-up-google-analytics/">here</a>.</p>\n<p>But that&#8217;s not all it has to offer: there are also various other advanced settings such as tracking multiple domains, adding additional domains, enhanced e-commerce, tracking outbound links, excluding WordPress user roles from tracking and many more.</p>\n<h2><strong>Wrapping Up</strong></h2>\n<p>A basic overview of those four actionable sections should ease the challenge of collecting relevant data for your business. However, getting invaluable data from Google Analytics requires work, time and patience. Stay tuned for an article that delves deeper into extracting and accurately analyzing data in Google Analytics.</p>\n<p>For now, you can take a look at some of the tutorials in the <a href="https://analyticsacademy.withgoogle.com/" target="_blank" rel="noopener noreferrer">Google Analytics Academy</a>.</p>\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:80:"https://semperplugins.com/important-sections-of-google-analytics-explained/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:2:"16";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:7;a:6:{s:4:"data";s:51:"\n		\n		\n		\n		\n		\n				\n		\n		\n		\n\n		\n		\n				\n			\n		\n		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:5:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:33:"How Social Media Impacts Your SEO";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:55:"https://semperplugins.com/how-social-media-impacts-seo/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:64:"https://semperplugins.com/how-social-media-impacts-seo/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 15 May 2017 16:00:29 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:4:{i:0;a:5:{s:4:"data";s:19:"All in One SEO Pack";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:8:"SEO Tips";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:2;a:5:{s:4:"data";s:10:"social SEO";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:3;a:5:{s:4:"data";s:13:"WordPress SEO";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:32:"http://semperplugins.com/?p=2893";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:404:"Social media has evolved quite rapidly in the years it has been around. From BBS (Bulletin Board) to Friendster, Diaspora, Myspace and onto Facebook. But when we think of social media today, we arguably only have 3 or 4 large<span class="ellipsis">&#8230;</span><div class="read-more"><a href="https://semperplugins.com/how-social-media-impacts-seo/">Read more &#8250;</a></div><!-- end of .read-more -->";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:12:"Arnaud Broes";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:11760:"<p><img class="size-full wp-image-3275 aligncenter" src="https://semperplugins.com/wp-content/uploads/2016/11/how-social-media-impacts-seo-featured.jpg" alt="" width="851" height="251" srcset="https://semperplugins.com/wp-content/uploads/2016/11/how-social-media-impacts-seo-featured.jpg 851w, https://semperplugins.com/wp-content/uploads/2016/11/how-social-media-impacts-seo-featured-300x88.jpg 300w, https://semperplugins.com/wp-content/uploads/2016/11/how-social-media-impacts-seo-featured-768x227.jpg 768w, https://semperplugins.com/wp-content/uploads/2016/11/how-social-media-impacts-seo-featured-848x250.jpg 848w, https://semperplugins.com/wp-content/uploads/2016/11/how-social-media-impacts-seo-featured-100x29.jpg 100w, https://semperplugins.com/wp-content/uploads/2016/11/how-social-media-impacts-seo-featured-150x44.jpg 150w, https://semperplugins.com/wp-content/uploads/2016/11/how-social-media-impacts-seo-featured-200x59.jpg 200w, https://semperplugins.com/wp-content/uploads/2016/11/how-social-media-impacts-seo-featured-450x133.jpg 450w, https://semperplugins.com/wp-content/uploads/2016/11/how-social-media-impacts-seo-featured-600x177.jpg 600w" sizes="(max-width: 851px) 100vw, 851px" /></p>\n<p>Social media has evolved quite rapidly in the years it has been around. From BBS (Bulletin Board) to Friendster, Diaspora, Myspace and onto Facebook. But when we think of social media today, we arguably only have 3 or 4 large networks in mind: Facebook, Twitter, Instagram and LinkedIn.</p>\n<p>Nowadays, those four platforms have become essential to every marketer’s strategy and are used to effectively get your brand in front of your target audience; but do they <em>directly</em> impact your SEO and search ranking? No, they currently do not.</p>\n<p>However, that doesn&#8217;t mean that social media marketing and SEO do not affect each other in a variety of different ways. They are both inbound strategies that aim to attract your audience or customers to your brand. They are also both organic ways to attract your audience with unique quality content.</p>\n<p>Perhaps the best way to describe the relationship between SEO and social media is to say that they are co-dependent. Effective social media campaigns will help improve your site ranking, and good SEO will inevitably boost your social clout. This is what we refer to as social SEO: <strong>the use of social media to boost your search engine rankings</strong>.</p>\n<p>This post will demystify the relationship between social media and SEO; and show you how you can make social SEO work for your WordPress site.</p>\n<h2>How Important is Social SEO?</h2>\n<p>You can certainly rank your website well without focusing much on social SEO. You can’t, however, rank your website by focusing solely on social SEO. But that shouldn’t make you dismiss the whole idea too soon.</p>\n<p>An active and strong social media presence makes it easier and faster to rank your site as search engines attribute <strong>authority</strong> based on tweets, shares, likes and Google +1s. Your social media presence also puts your content in front of a larger audience, some of whom will link your content on their own sites, creating valuable organic <strong>backlinks</strong> which contribute to your PageRank.</p>\n<div id="attachment_3276" style="width: 630px" class="wp-caption aligncenter"><img class="wp-image-3276" src="https://semperplugins.com/wp-content/uploads/2016/11/social-media-marketing-graph.png" alt="According to a recent study, Social Media Marketing even surpassed the impact of regular SEO in 2016, considering that market is already rather saturated. Time to hop on the bandwagon!" width="620" height="359" srcset="https://semperplugins.com/wp-content/uploads/2016/11/social-media-marketing-graph.png 981w, https://semperplugins.com/wp-content/uploads/2016/11/social-media-marketing-graph-300x174.png 300w, https://semperplugins.com/wp-content/uploads/2016/11/social-media-marketing-graph-768x445.png 768w, https://semperplugins.com/wp-content/uploads/2016/11/social-media-marketing-graph-432x250.png 432w, https://semperplugins.com/wp-content/uploads/2016/11/social-media-marketing-graph-100x58.png 100w, https://semperplugins.com/wp-content/uploads/2016/11/social-media-marketing-graph-150x87.png 150w, https://semperplugins.com/wp-content/uploads/2016/11/social-media-marketing-graph-200x116.png 200w, https://semperplugins.com/wp-content/uploads/2016/11/social-media-marketing-graph-450x261.png 450w, https://semperplugins.com/wp-content/uploads/2016/11/social-media-marketing-graph-600x347.png 600w, https://semperplugins.com/wp-content/uploads/2016/11/social-media-marketing-graph-900x521.png 900w" sizes="(max-width: 620px) 100vw, 620px" /><p class="wp-caption-text">According to a recent study, Social Media Marketing even surpassed the impact of regular SEO in 2016, considering that market is already rather saturated. Time to hop on the bandwagon! Source: smartinsights.com/managing-digital-marketing/marketing-innovation/marketing-trends-2016/</p></div>\n<p>That is not to say that social SEO will eventually replace regular SEO, but we cannot ignore the fact that social signals are slowly becoming an integral part of search engine algorithms. Even though they have nothing on backlinks – and probably won’t, at least not in the near future – <strong>social links</strong> are still quite important. But <em>how much</em> effect or weight do they have on your rankings, particularly your ranking for target keywords?</p>\n<p>Well, search engines have not yet made that information public. We just have to work with the definitive information that social signals do (indirectly) impact your site rankings and will continue to do so.</p>\n<h2>When Should I Focus on Social SEO?</h2>\n<p>If you are in an industry where your competitors are heavily focusing on both SEO and social SEO, and you want to rank well for your industry keywords, you have no other option but to do the same. Backlinks coupled with social signals will boost your rankings much more than backlinks alone, so you will lag behind your competition if you are only focused on building the latter. And if they aren’t focused on social SEO, <strong>you should jump on the opportunity to get one step ahead of your competitors</strong>.</p>\n<p>Although social media does not directly impact your site rankings on Google, social networks are dominating the internet and will arguably dictate the future of the internet. You should invest properly in social media as it could soon <em>directly</em> impact your site’s rankings on search engines.</p>\n<h2>How to Effectively Implement Social SEO</h2>\n<p>How can you increase your site rankings with good social SEO? Here are a few summarized tips:</p>\n<ul>\n<li>Create a business page on the “main” social networks: Facebook, Twitter, LinkedIn, Google+, Pinterest, Instagram, etc.</li>\n<li>Be active on those channels and engage your followers and subscribers</li>\n<li>Create shareable, engaging and unique content</li>\n<li>Make it as easy as possible for people to share your content</li>\n<li>Share your content, and that of other influencers in your field on your social channels</li>\n</ul>\n<p><strong>The more active you are on social media, the more awareness you are bringing to your brand</strong>, the more your social following grows, and the higher the chance that your content is being shared across a broader network. It’s a snowball effect: the more followers you have, the more engagement you’ll get and the easier it will become to grow your community.</p>\n<p>A bigger community means that more people are viewing your content and the higher the chance that the content will be shared. This increases the number of backlinks to your site which will impact your search rankings for the better.</p>\n<p>Also, the more engagement you have on your social channels, the higher the chance that people will look for your brand on Google or other search engines via branded searches – which help your site rank for non-branded keywords that are relevant to your brand.</p>\n<p>You do have to consider that <strong>the above scenarios are based on the assumption that you are producing quality and unique content</strong>. If you are creating bad content, you won’t get anywhere. Users will not interact with your brand if you’re not offering them value or good entertainment. If you want your content shared and widely distributed, you need to put in the time to create quality and valuable content your audience will enjoy.</p>\n<p>In summary, the following social signals will increase your search engine rankings for the keywords you’re targeting:</p>\n<ul>\n<li>Your social network followers</li>\n<li>The likes, shares, retweets, Google +1s your website gets</li>\n<li>Mentions by other authority or influencers in your field</li>\n<li>Positive reviews on your business’ Google+ page</li>\n</ul>\n<h2>Using All in One SEO Pack for Social Meta</h2>\n<p><a href="https://semperplugins.com/all-in-one-seo-pack-pro-version/?loc=sp_navmenu">Our All in One SEO Pack plugin comes with a Social Meta module</a> to help you get going with social marketing. This feature allows you to add Open Graph meta tags to enrich your posts by creating a rich snippet. Let’s briefly go over how Open Graph meta tags work and how they specifically help you with social SEO.</p>\n<p>Basically, every web page on any site can be shared on social media channels. However, the way that shared page is displayed on social media may be very different. Websites that aren’t optimized with Open Graph meta tags won’t be as easily interpreted by Facebook, Twitter and the like and will, most of the time, show up as a blank link with a small description underneath.</p>\n<div id="attachment_3278" style="width: 516px" class="wp-caption aligncenter"><img class="size-full wp-image-3278" src="https://semperplugins.com/wp-content/uploads/2016/11/rich-social-snippet.png" alt="Would you be more inclined to click on a blank link or on a rich snippet such as this?" width="506" height="495" /><p class="wp-caption-text">Would you be more inclined to click on a blank link or on a rich snippet such as this?</p></div>\n<p>However, adding Open Graph meta tags to a page on your site enriches the snippet and enables Facebook and other social media networks to display your content in the way you want to present it when it is shared. This is where All in One SEO Pack comes into action. For a full overview of all Open Graph and social meta features, please visit our <a href="https://semperplugins.com/documentation/social-meta-module/">documentation section</a>.</p>\n<p>There is no concrete evidence that Open Graph meta tags are important for search engines, but they do enable you to create presentable good looking posts on social media (instead of a bland-looking post). This significantly increases your organic traffic from social media to your WordPress site. Studies have shown that <strong>traffic increases 100%-200% if a post looks great</strong> vs. a blank link that doesn’t really mean much to social media users.</p>\n<h2>Wrapping It Up</h2>\n<p>Overall, a solid social media strategy, in conjunction with good SEO practices, will increase organic traffic to your site. Solely focusing on SEO may not cut it any longer if you want to be a step ahead of your competition. Revising your strategy to incorporate social SEO will inevitably boost your organic traffic and could very well increase your search engine rankings in the near future.</p>\n<p>Do you currently have a social SEO strategy that’s working for you? Have you been using the social SEO module of our All in One SEO Pack plugin? Let us know in the comments below!</p>\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:60:"https://semperplugins.com/how-social-media-impacts-seo/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"9";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:8;a:6:{s:4:"data";s:48:"\n		\n		\n		\n		\n		\n				\n		\n		\n\n		\n		\n				\n			\n		\n		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:5:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:39:"How to Build an Email List in WordPress";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:66:"https://semperplugins.com/how-to-build-an-email-list-in-wordpress/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:75:"https://semperplugins.com/how-to-build-an-email-list-in-wordpress/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 01 May 2017 14:00:18 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:3:{i:0;a:5:{s:4:"data";s:4:"Blog";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:15:"email marketing";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:2;a:5:{s:4:"data";s:14:"subscribe2html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:32:"http://semperplugins.com/?p=2904";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:423:"How many of you have prioritized pageviews over email marketing? We all have at some point. After all, pageviews equals more ad revenue…right? Well, having a good email list is more valuable and can bring in more pageviews than you<span class="ellipsis">&#8230;</span><div class="read-more"><a href="https://semperplugins.com/how-to-build-an-email-list-in-wordpress/">Read more &#8250;</a></div><!-- end of .read-more -->";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:12:"Arnaud Broes";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:9578:"<p><img class="size-full wp-image-3258 aligncenter" src="https://semperplugins.com/wp-content/uploads/2016/11/how-to-build-an-email-list-featured.jpg" alt="" width="851" height="251" srcset="https://semperplugins.com/wp-content/uploads/2016/11/how-to-build-an-email-list-featured.jpg 851w, https://semperplugins.com/wp-content/uploads/2016/11/how-to-build-an-email-list-featured-300x88.jpg 300w, https://semperplugins.com/wp-content/uploads/2016/11/how-to-build-an-email-list-featured-768x227.jpg 768w, https://semperplugins.com/wp-content/uploads/2016/11/how-to-build-an-email-list-featured-848x250.jpg 848w, https://semperplugins.com/wp-content/uploads/2016/11/how-to-build-an-email-list-featured-100x29.jpg 100w, https://semperplugins.com/wp-content/uploads/2016/11/how-to-build-an-email-list-featured-150x44.jpg 150w, https://semperplugins.com/wp-content/uploads/2016/11/how-to-build-an-email-list-featured-200x59.jpg 200w, https://semperplugins.com/wp-content/uploads/2016/11/how-to-build-an-email-list-featured-450x133.jpg 450w, https://semperplugins.com/wp-content/uploads/2016/11/how-to-build-an-email-list-featured-600x177.jpg 600w" sizes="(max-width: 851px) 100vw, 851px" /></p>\n<p>How many of you have prioritized pageviews over email marketing? We all have at some point. After all, pageviews equals more ad revenue…right? Well, having a good email list is more valuable and can bring in more pageviews than you get using traditional methods.</p>\n<p>So how do you build an email list in WordPress?</p>\n<p>You always hear the same advice: create great content and people will subscribe, or have an opt-in form on your blog. Those methods are tried and tested but there are other more creative and effective ways to implement them and speed up the growth of your email list.</p>\n<p>We’re talking content upgrades, high-value opt-in forms and social proof.</p>\n<p>Let’s first start off with the basics before we dive into those three methods and how to effectively employ them.</p>\n<h2>The Basics</h2>\n<h3>What is an Email List?</h3>\n<p>An email list is a list of email addresses of your existing and prospective customers who have signed up or subscribed via your WordPress site to get updates, newsletters, special offers or announcements.</p>\n<h3>Why You Should Build an Email List</h3>\n<p>Email consistently outperforms social media across the board. It gets a higher view rate than Facebook or Twitter; 50-100 times the click rates of Facebook or Twitter; and is more cost-effective than any other marketing and social channel. Why? Because email is more intimate and private.</p>\n<p>If those stats haven’t convinced you yet, here are three other really good reasons:</p>\n<p><strong>#1.</strong> Your emails are guaranteed to reach the inboxes of each and every one of the active contacts on your email list. The email stays in their inbox until they take an action. Compare that to Facebook or Twitter, where your statuses and posts may not be seen by your followers because of algorithms and the inherent nature of those social networks. Bottom line: people check their emails more thoroughly than they do their social timelines</p>\n<p><strong>#2.</strong> Email is more secure and more flexible. You own your email list. You however, have very limited control and no ownership of your Twitter and Facebook account data. Furthermore, your social accounts could get hacked or suspended for whatever reason; or the platform could cease to exist or become less popular (think Myspace) etc. Whereas you own your email list and can communicate with your customers and prospects on your own terms.</p>\n<p><strong>#3.</strong> Email has more specific targeting. You can send information or announcements on product specific or geo-specific services to specific contacts that match some particular criteria.</p>\n<p>That’s not to say that you should neglect your social networks. Social is great for user engagement so don’t give up on social just yet. Email will just bring you more visitors and conversions.</p>\n<p>So how can you get started today?</p>\n<p>Well, there are quite a few WordPress email marketing plugins at your disposal. Most of them require you to use them in conjunction with email marketing service providers and only help you to create signup forms on your site. They don’t go much further than that. A few however do. <a href="https://semperplugins.com/subscribe2-html/" target="_blank" rel="noopener noreferrer">One of those is Subscribe2 HTML</a>.</p>\n<h2>Subscribe2 HTML</h2>\n<p>Subscribe2HTML plugin is arguably one of the most comprehensive and reliable email marketing plugins for WordPress. It gives you the option of sending emails on a per-post basis or periodically in a newsletter format.</p>\n<p>It manages your email lists in a straightforward way. For example, you don’t need to log in to compose an email every time before sending one. This comes in handy when you need to send out timely post updates – a great feature for high traffic blogs.</p>\n<p>There are two subscription options for Subscribe2 HTML: Registered Subscribers and Public Subscribers. Registered Subscribers are users who have subscribed to your blog and are consequently required to log-in to your site to manage their subscriptions. They can opt to receive either plain text or HTML emails. You can send them post notifications by category; and they can choose whether they want just the excerpt or the full post. (If they don’t make a choice, they will get the full post when they select the plain text option).</p>\n<div id="attachment_3261" style="width: 391px" class="wp-caption aligncenter"><img class=" wp-image-3261" src="https://semperplugins.com/wp-content/uploads/2016/11/subscribe2.png" alt="Suscribe2HTML is full of features and can be configured in any way you like." width="381" height="462" srcset="https://semperplugins.com/wp-content/uploads/2016/11/subscribe2.png 610w, https://semperplugins.com/wp-content/uploads/2016/11/subscribe2-248x300.png 248w, https://semperplugins.com/wp-content/uploads/2016/11/subscribe2-206x250.png 206w, https://semperplugins.com/wp-content/uploads/2016/11/subscribe2-100x121.png 100w, https://semperplugins.com/wp-content/uploads/2016/11/subscribe2-150x182.png 150w, https://semperplugins.com/wp-content/uploads/2016/11/subscribe2-200x242.png 200w, https://semperplugins.com/wp-content/uploads/2016/11/subscribe2-300x363.png 300w, https://semperplugins.com/wp-content/uploads/2016/11/subscribe2-450x545.png 450w, https://semperplugins.com/wp-content/uploads/2016/11/subscribe2-600x727.png 600w" sizes="(max-width: 381px) 100vw, 381px" /><p class="wp-caption-text">Suscribe2HTML is full of features and can be configured in any way you like.</p></div>\n<p>Public Subscribers can sign up through an online form. However, their subscription only starts when they confirm the subscription request via email. They do not get to choose between an excerpt or plain text version of an email.</p>\n<p>As an admin, you can subscribe or unsubscribe users and send emails to all verified subscribers. You also have a selection of email templates to choose from.</p>\n<p>There are additional options and settings available, like choosing the frequency of your emails and the time of day to send them. The plugin is all about automation to save you valuable time.</p>\n<h2>3 Effective strategies to Grow Your Email List</h2>\n<h3>Content</h3>\n<p>Consistently creating great content is the advice you always hear and read about when looking for ways to increase traffic to your blog and to grow your email list. That’s all good and true, but have you heard of a content upgrade? It’s a simple and highly effective strategy to grow your email list.</p>\n<p>How it works:</p>\n<ol>\n<li>Write an engaging blog post.</li>\n<li>Create a more in-depth version of the post.</li>\n<li>Give readers the option to read the upgraded content in return for their email address.</li>\n</ol>\n<p>This strategy works on the basis that your readers are already interested in the blog post since they clicked on it. So, a content upgrade plays on that interest by offering more. It’s simple, straightforward and highly effective.</p>\n<h3>Opt-In Forms</h3>\n<p>Like you’ve heard over and over again (and seen on many high-traffic sites), opt-in forms are the way to go when growing your email list. It’s worth mentioning again in this post. Whether you’re offering an eBook, white paper or free software, opt-in forms will increase your email subscriptions.</p>\n<p>The key is not to offer as many incentives as you can. The most efficient strategy is to pick a high-value incentive and place it in appropriate areas of your site (the sidebar, pop-ups, blog post, footer, etc.). This strategy is especially great for small businesses that don’t have the time or the manpower to produce upgraded content for every blog post.</p>\n<h3>Social Proof</h3>\n<p>A majority of online users rely on ratings, popularity and reviews in making purchasing decisions. Social proof is very important and shouldn’t be underestimated.</p>\n<p>That being said, you should focus on your numbers. Add up all your followers and subscribers on all channels and put the total out there. The bigger the number, the more impressive.</p>\n<p>In fact, various research studies have shown that social proof has more impact on behavior than financial, environmental and personal incentives.</p>\n<p>Would you also like to be included in our email list? Go to <a href="https://semperplugins.com/">our homepage</a> and enter your email address to subscribe!</p>\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:71:"https://semperplugins.com/how-to-build-an-email-list-in-wordpress/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:2:"14";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:9;a:6:{s:4:"data";s:45:"\n		\n		\n		\n		\n		\n				\n		\n\n		\n		\n				\n			\n		\n		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:5:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:40:"9 Steps to Clean Your WordPress Database";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:63:"https://semperplugins.com/how-to-clean-your-wordpress-database/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:72:"https://semperplugins.com/how-to-clean-your-wordpress-database/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 06 Apr 2017 13:00:25 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:2:{i:0;a:5:{s:4:"data";s:4:"Blog";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:18:"WordPress database";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"https://semperplugins.com/?p=3225/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:422:"Is your WordPress database a mess? Have you been complaining about it since time immemorial? Well, then your WordPress site might be as bloated as you were after that marvelous Thanksgiving dinner at your mom’s. If your site has been<span class="ellipsis">&#8230;</span><div class="read-more"><a href="https://semperplugins.com/how-to-clean-your-wordpress-database/">Read more &#8250;</a></div><!-- end of .read-more -->";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Derek Iwasiuk";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:9209:"<p><img class="size-full wp-image-3232 aligncenter" src="https://semperplugins.com/wp-content/uploads/2017/03/clean-up-your-wordpress-database.jpg" alt="" width="850" height="250" srcset="https://semperplugins.com/wp-content/uploads/2017/03/clean-up-your-wordpress-database.jpg 850w, https://semperplugins.com/wp-content/uploads/2017/03/clean-up-your-wordpress-database-300x88.jpg 300w, https://semperplugins.com/wp-content/uploads/2017/03/clean-up-your-wordpress-database-768x226.jpg 768w, https://semperplugins.com/wp-content/uploads/2017/03/clean-up-your-wordpress-database-100x29.jpg 100w, https://semperplugins.com/wp-content/uploads/2017/03/clean-up-your-wordpress-database-150x44.jpg 150w, https://semperplugins.com/wp-content/uploads/2017/03/clean-up-your-wordpress-database-200x59.jpg 200w, https://semperplugins.com/wp-content/uploads/2017/03/clean-up-your-wordpress-database-450x132.jpg 450w, https://semperplugins.com/wp-content/uploads/2017/03/clean-up-your-wordpress-database-600x176.jpg 600w" sizes="(max-width: 850px) 100vw, 850px" /></p>\n<p>Is your WordPress database a mess? Have you been complaining about it since time immemorial? Well, then your WordPress site might be as bloated as you were after that marvelous Thanksgiving dinner at your mom’s. If your site has been live for a while now, <strong>chances are that its database has acquired quite a bit of garbage and is demanding a spring clean</strong>.</p>\n<p>The trick to maintaining a database is to clean it regularly to prevent the accumulation of unnecessary data. However, we are guessing that you have been skimping on the cleaning bit, which is why you are here today. Therefore, without wasting time, we will tell you how to clean up that database in a few easy steps.</p>\n<h2><strong>#1. Basic Ground Rules</strong></h2>\n<p>Before you take on the massive task of spring-cleaning, you must ensure that your valuables are safe and sound. Losing a single system file can mess up your website entirely and can cause you to lose thousands of dollars in revenue.</p>\n<p>So start by creating copies of important files and create a backup of your entire website. You should definitely<a href="https://ithemes.com/purchase/backupbuddy/" target="_blank" rel="noopener noreferrer"> check out Backupbuddy from our friends at iThemes</a>. It&#8217;s a great plugin that allows you to schedule automatic full or database-only backups, and we highly recommend it.</p>\n<h2><strong>#2. Screen Through the Clutter</strong></h2>\n<p>Do you remember how yard sales used to work before Craigslist and eBay came along? Just like separating the valuables from trash, you need to pick out the plugins you use and the ones you don’t.</p>\n<p>You can easily get started by going through your complete list of plugins in your WordPress dashboard. Go to your admin panel and click on Plugins. Next, go to Installed Plugins and click on Inactive at the top of the screen.</p>\n<p>Eliminating unused plugins should easily free up a couple of bytes and is the first step towards cleaning up your WordPress database.</p>\n<h2><strong>#3. Time For a Purge </strong></h2>\n<p>This might sound very ominous (esp. to the ones familiar with the popular movie franchise), but it is the best way to free up space. After deleting all unused plugins you should start cleaning out post data.</p>\n<p>If you have been using WordPress from a while, you might not be as surprised to know that all your post data is being stored in your website database. You can find it all in the wp_postmeta table, and you can run this query to get rid of all superfluous data:</p>\n<blockquote><p>DELETE FROM wp_postmeta WHERE meta_key = &#8216;your-meta-key</p></blockquote>\n<p>Always remember to replace the last part with the value that has to be cleared out.</p>\n<h2>#4. <strong>Take Care of Spam</strong></h2>\n<p>Spam is no longer restricted to your inbox. If you look through your database carefully, you will find tons of spam comments as well. These include promotional features and unscrupulous backlinks that other sites use to eat their way into your SEO. Monitoring comments is an essential practice that is currently underrated.</p>\n<p>The easiest way to manage spam comments on your site is to execute this query:</p>\n<blockquote><p>DELETE FROM wp_comments WHERE comment_approved = &#8216;spam&#8217;;</p></blockquote>\n<p>If you are currently <a href="https://semperplugins.com/wordpress-multisite/" target="_blank" rel="noopener noreferrer">using a multi-site installation of WordPress</a>, you may like to try the following query:</p>\n<blockquote><p>DELETE FROM wp_#_comments WHERE comment_approved = &#8216;spam&#8217;;</p></blockquote>\n<h2>#5. <strong>What About Comments Waiting on Moderation?</strong></h2>\n<p>As a WordPress website/blog admin, you have the power to delete all comments that are awaiting moderation. This eliminates the need of unnecessarily going through each and every spam comment that you haven’t yet moderated.</p>\n<p>Just a word of advice: go through the list once to approve all genuine comments before running this query:</p>\n<blockquote><p>DELETE FROM wp_comments WHERE comment_approved = &#8216;0&#8217;;</p></blockquote>\n<h2><strong>#6. Taking Care of Unused Tags</strong></h2>\n<p>This has happened to almost all of us. When we were new at blogging, we created a few hundred tags thinking we would use them from time to time. And now that we post regularly, we hardly ever change tags. We use the most common ones and stick to those.</p>\n<p>Don’t be surprised if you have completely forgotten about a score of tags that you exist on your website database (thanks to your wild imaginative powers). Fortunately, we have a query that will take care of all unused tags, but be sure to check the ones that you do use before running this query:</p>\n<blockquote><p>DELETE FROM wp_terms wt<br />\nINNER JOIN wp_term taxonomy wtt ON wt.term_id = wtt.term_id WHERE wtt.taxonomy = &#8216;post_tag&#8217; and wtt.count = 0;</p></blockquote>\n<p>Check out some of our earlier posts if you are unsure about <a href="https://semperplugins.com/categories-and-tags/" target="_blank" rel="noopener noreferrer">the differences between categories &amp; tags and how to use them</a>.</p>\n<h2>#7. <strong>Bid Farewell to Pingbacks</strong></h2>\n<p>If you are currently using a setting that has turned off the option of accepting pingbacks, then you may want to use the following code. This will remove every pingback ever made to your website from your database.</p>\n<blockquote><p>DELETE FROM wp_comments WHERE comment_type = &#8216;pingback&#8217;;</p></blockquote>\n<p>Again, if you are currently <a href="https://semperplugins.com/wordpress-multisite/" target="_blank" rel="noopener noreferrer">using a multisite installation</a>, you may try using the next one. just remember to replace # with your current site id.</p>\n<blockquote><p>DELETE FROM wp_#_comments WHERE comment_type = &#8216;pingback&#8217;;</p></blockquote>\n<h2><strong>#8. No more Post Revisions</strong></h2>\n<p>Post revisions are indeed necessary for blogs, but once you see the space they take up on your database, you won’t think twice about bidding them farewell. They grow fast and exponentially. You can clear them out at one go using the next query:</p>\n<blockquote><p>DELETE a, b, c FROM wp_posts a LEFT JOIN wp_term_relationships b ON (a.ID = b.object_id)<br />\nLEFT JOIN wp_postmeta c ON (a.ID = c.post_id) WHERE a.post_type = &#8216;revision&#8217;</p></blockquote>\n<p>You can even chose to disable all post revisions for your website using the following SQL code:</p>\n<blockquote><p>define(&#8216;WP_POST_REVISIONS&#8217;), false);</p></blockquote>\n<h2>#9. Getting Rid of Trackbacks</h2>\n<p>Your final step should be getting rid of all old trackbacks. To make sure that nothing goes wrong, double check so that all your pingbacks and trackbacks have been disabled before trying the next SQL code:</p>\n<blockquote><p>DELETE FROM wp_comments WHERE comment_type = &#8216;trackback&#8217;;</p></blockquote>\n<p>For multisite users, the next query should be more profitable. You can use this one to clean out all your trackbacks at one go. Like previous ones, replace # with your current site ID to remove all of your site&#8217;s trackbacks.</p>\n<blockquote><p>DELETE FROM wp_#_comments WHERE comment_type = &#8216;trackback&#8217;;</p></blockquote>\n<p>Besides these, you can also scrap your old posts once your audience has fallen out of love with them. This might be difficult at first, but many old posts are nothing but unwarranted burden for WordPress databases.</p>\n<p>If you are new and quite lost with the new set of codes, queries and rules we were talking about right here, leave your maintenance <a href="https://wordpress.org/plugins/wp-optimize/" target="_blank" rel="noopener noreferrer">to seasoned plugins like Optimize</a>. They work great with multisite installations as well, and include extensive options for managing and removing unused tables.</p>\n<p>This article has been written by Derek Iwasiuk from <a href="https://engagethecrowd.com/minneapolis-seo-services/" target="_blank" rel="noopener noreferrer">www.engagethecrowd.com</a>.</p>\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:68:"https://semperplugins.com/how-to-clean-your-wordpress-database/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:2:"18";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}}}s:27:"http://www.w3.org/2005/Atom";a:1:{s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:3:{s:4:"href";s:31:"https://semperplugins.com/feed/";s:3:"rel";s:4:"self";s:4:"type";s:19:"application/rss+xml";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:44:"http://purl.org/rss/1.0/modules/syndication/";a:2:{s:12:"updatePeriod";a:1:{i:0;a:5:{s:4:"data";s:6:"hourly";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:15:"updateFrequency";a:1:{i:0;a:5:{s:4:"data";s:1:"1";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}}}}}}}}s:4:"type";i:128;s:7:"headers";O:42:"Requests_Utility_CaseInsensitiveDictionary":1:{s:7:"\0*\0data";a:15:{s:6:"server";s:5:"nginx";s:4:"date";s:29:"Wed, 09 May 2018 07:12:40 GMT";s:12:"content-type";s:34:"application/rss+xml; charset=UTF-8";s:14:"content-length";s:5:"31626";s:7:"expires";s:29:"Thu, 19 Nov 1981 08:52:00 GMT";s:13:"cache-control";s:35:"no-store, no-cache, must-revalidate";s:6:"pragma";s:8:"no-cache";s:3:"p3p";s:69:"CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"";s:12:"x-robots-tag";s:15:"noindex, follow";s:4:"link";s:62:"<https://semperplugins.com/wp-json/>; rel="https://api.w.org/"";s:10:"set-cookie";a:2:{i:0;s:44:"PHPSESSID=necd46to7s2v9ltqutcs22fg05; path=/";i:1;s:70:"Cart66DBSID=8RR7MVWL1LVQQH1S4YZ1U92ENE9Q131ERKAWPQZL; path=/; HttpOnly";}s:13:"last-modified";s:29:"Tue, 17 Apr 2018 21:24:18 GMT";s:4:"etag";s:39:""8fd12d3c58f8fc974cea218f20116385-gzip"";s:4:"vary";s:15:"Accept-Encoding";s:16:"content-encoding";s:4:"gzip";}}s:5:"build";s:14:"20130910220210";}', 'no');
INSERT INTO `ops_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(829, '_transient_timeout_feed_mod_54af7cb0f50a8f4f8cdb30f2d9b4f50c', '1525893158', 'no'),
(830, '_transient_feed_mod_54af7cb0f50a8f4f8cdb30f2d9b4f50c', '1525849958', 'no'),
(831, '_transient_timeout_aioseop_feed', '1525893158', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `ops_postmeta`
--

CREATE TABLE IF NOT EXISTS `ops_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=365 ;

--
-- Dumping data for table `ops_postmeta`
--

INSERT INTO `ops_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 8, '_wp_attached_file', '2018/04/chair.png'),
(3, 8, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1000;s:6:"height";i:1000;s:4:"file";s:17:"2018/04/chair.png";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"chair-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:17:"chair-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:12:"medium_large";a:4:{s:4:"file";s:17:"chair-768x768.png";s:5:"width";i:768;s:6:"height";i:768;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(4, 2, '_wp_trash_meta_status', 'publish'),
(5, 2, '_wp_trash_meta_time', '1525098299'),
(6, 2, '_wp_desired_post_slug', 'sample-page'),
(7, 11, '_edit_last', '1'),
(8, 11, 'wpeasycart_restrict_redirect_url', ''),
(9, 11, '_edit_lock', '1525336052:1'),
(10, 13, '_edit_last', '1'),
(11, 13, '_edit_lock', '1525334664:1'),
(12, 13, 'wpeasycart_restrict_redirect_url', ''),
(13, 15, '_edit_last', '1'),
(14, 15, '_edit_lock', '1525275803:1'),
(15, 15, 'wpeasycart_restrict_redirect_url', ''),
(16, 17, '_edit_last', '1'),
(17, 17, 'wpeasycart_restrict_redirect_url', ''),
(18, 17, '_edit_lock', '1525098522:1'),
(19, 19, '_menu_item_type', 'custom'),
(20, 19, '_menu_item_menu_item_parent', '0'),
(21, 19, '_menu_item_object_id', '19'),
(22, 19, '_menu_item_object', 'custom'),
(23, 19, '_menu_item_target', ''),
(24, 19, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(25, 19, '_menu_item_xfn', ''),
(26, 19, '_menu_item_url', 'http://localhost/oz_production/public_html/'),
(27, 19, '_menu_item_orphaned', '1525098707'),
(28, 20, '_menu_item_type', 'post_type'),
(29, 20, '_menu_item_menu_item_parent', '0'),
(30, 20, '_menu_item_object_id', '13'),
(31, 20, '_menu_item_object', 'page'),
(32, 20, '_menu_item_target', ''),
(33, 20, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(34, 20, '_menu_item_xfn', ''),
(35, 20, '_menu_item_url', ''),
(37, 21, '_menu_item_type', 'post_type'),
(38, 21, '_menu_item_menu_item_parent', '0'),
(39, 21, '_menu_item_object_id', '7'),
(40, 21, '_menu_item_object', 'page'),
(41, 21, '_menu_item_target', ''),
(42, 21, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(43, 21, '_menu_item_xfn', ''),
(44, 21, '_menu_item_url', ''),
(46, 22, '_menu_item_type', 'post_type'),
(47, 22, '_menu_item_menu_item_parent', '0'),
(48, 22, '_menu_item_object_id', '6'),
(49, 22, '_menu_item_object', 'page'),
(50, 22, '_menu_item_target', ''),
(51, 22, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(52, 22, '_menu_item_xfn', ''),
(53, 22, '_menu_item_url', ''),
(55, 23, '_menu_item_type', 'post_type'),
(56, 23, '_menu_item_menu_item_parent', '0'),
(57, 23, '_menu_item_object_id', '17'),
(58, 23, '_menu_item_object', 'page'),
(59, 23, '_menu_item_target', ''),
(60, 23, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(61, 23, '_menu_item_xfn', ''),
(62, 23, '_menu_item_url', ''),
(64, 24, '_menu_item_type', 'post_type'),
(65, 24, '_menu_item_menu_item_parent', '0'),
(66, 24, '_menu_item_object_id', '11'),
(67, 24, '_menu_item_object', 'page'),
(68, 24, '_menu_item_target', ''),
(69, 24, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(70, 24, '_menu_item_xfn', ''),
(71, 24, '_menu_item_url', ''),
(73, 25, '_menu_item_type', 'post_type'),
(74, 25, '_menu_item_menu_item_parent', '0'),
(75, 25, '_menu_item_object_id', '15'),
(76, 25, '_menu_item_object', 'page'),
(77, 25, '_menu_item_target', ''),
(78, 25, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(79, 25, '_menu_item_xfn', ''),
(80, 25, '_menu_item_url', ''),
(81, 25, '_menu_item_orphaned', '1525098709'),
(91, 11, '_wp_page_template', 'home.php'),
(92, 27, '_wp_attached_file', '2018/04/table.png'),
(93, 27, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:500;s:6:"height";i:282;s:4:"file";s:17:"2018/04/table.png";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"table-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:17:"table-300x169.png";s:5:"width";i:300;s:6:"height";i:169;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(94, 13, '_wp_page_template', 'default'),
(95, 13, 'about_project_completed', '39'),
(96, 13, 'about_team_member', '14'),
(97, 13, 'about_on_going_project', '29'),
(98, 13, 'about_weekly_event', '5'),
(99, 30, '_menu_item_type', 'post_type'),
(100, 30, '_menu_item_menu_item_parent', '0'),
(101, 30, '_menu_item_object_id', '15'),
(102, 30, '_menu_item_object', 'page'),
(103, 30, '_menu_item_target', ''),
(104, 30, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(105, 30, '_menu_item_xfn', ''),
(106, 30, '_menu_item_url', ''),
(108, 31, '_edit_last', '1'),
(109, 31, '_edit_lock', '1525266073:1'),
(110, 31, 'about_project_completed', NULL),
(111, 31, 'about_team_member', NULL),
(112, 31, 'about_on_going_project', NULL),
(113, 31, 'about_weekly_event', NULL),
(114, 32, '_edit_last', '1'),
(115, 32, '_edit_lock', '1525266854:1'),
(116, 32, 'about_project_completed', NULL),
(117, 32, 'about_team_member', NULL),
(118, 32, 'about_on_going_project', NULL),
(119, 32, 'about_weekly_event', NULL),
(120, 32, '_wp_old_slug', 'testimonials-1-2'),
(121, 33, '_edit_last', '1'),
(122, 33, '_edit_lock', '1525266991:1'),
(123, 33, 'about_project_completed', NULL),
(124, 33, 'about_team_member', NULL),
(125, 33, 'about_on_going_project', NULL),
(126, 33, 'about_weekly_event', NULL),
(127, 34, '_edit_last', '1'),
(128, 34, '_edit_lock', '1525267070:1'),
(129, 34, '_wp_page_template', 'default'),
(130, 34, 'about_project_completed', NULL),
(131, 34, 'about_team_member', NULL),
(132, 34, 'about_on_going_project', NULL),
(133, 34, 'about_weekly_event', NULL),
(134, 34, 'wpeasycart_restrict_redirect_url', ''),
(135, 36, '_edit_last', '1'),
(136, 36, '_wp_page_template', 'default'),
(137, 36, 'about_project_completed', NULL),
(138, 36, 'about_team_member', NULL),
(139, 36, 'about_on_going_project', NULL),
(140, 36, 'about_weekly_event', NULL),
(141, 36, 'wpeasycart_restrict_redirect_url', ''),
(142, 36, '_edit_lock', '1525266967:1'),
(143, 38, '_edit_last', '1'),
(144, 38, '_edit_lock', '1525331272:1'),
(145, 38, '_wp_page_template', 'default'),
(146, 38, 'about_project_completed', NULL),
(147, 38, 'about_team_member', NULL),
(148, 38, 'about_on_going_project', NULL),
(149, 38, 'about_weekly_event', NULL),
(150, 38, 'wpeasycart_restrict_redirect_url', ''),
(151, 40, '_edit_last', '1'),
(152, 40, '_wp_page_template', 'default'),
(153, 40, 'about_project_completed', NULL),
(154, 40, 'about_team_member', NULL),
(155, 40, 'about_on_going_project', NULL),
(156, 40, 'about_weekly_event', NULL),
(157, 40, 'wpeasycart_restrict_redirect_url', ''),
(158, 40, '_edit_lock', '1525348773:1'),
(159, 1, '_wp_trash_meta_status', 'publish'),
(160, 1, '_wp_trash_meta_time', '1525270320'),
(161, 1, '_wp_desired_post_slug', 'hello-world'),
(162, 1, 'about_project_completed', NULL),
(163, 1, 'about_team_member', NULL),
(164, 1, 'about_on_going_project', NULL),
(165, 1, 'about_weekly_event', NULL),
(166, 1, '_wp_trash_meta_comments_status', 'a:1:{i:1;s:1:"1";}'),
(167, 44, '_edit_last', '1'),
(168, 44, '_edit_lock', '1525345924:1'),
(171, 44, 'about_project_completed', NULL),
(172, 44, 'about_team_member', NULL),
(173, 44, 'about_on_going_project', NULL),
(174, 44, 'about_weekly_event', NULL),
(175, 44, 'wpeasycart_restrict_redirect_url', ''),
(176, 46, '_edit_last', '1'),
(179, 46, 'about_project_completed', NULL),
(180, 46, 'about_team_member', NULL),
(181, 46, 'about_on_going_project', NULL),
(182, 46, 'about_weekly_event', NULL),
(183, 46, 'wpeasycart_restrict_redirect_url', ''),
(184, 46, '_edit_lock', '1525350030:1'),
(203, 15, '_wp_page_template', 'default'),
(204, 15, 'about_project_completed', NULL),
(205, 15, 'about_team_member', NULL),
(206, 15, 'about_on_going_project', NULL),
(207, 15, 'about_weekly_event', NULL),
(208, 15, 'service_slogan_1', 'THE DETAILS ARE NOT THE DETAILS'),
(209, 15, 'service_slogan_2', 'THEY MAKE THE DESIGN'),
(210, 51, '_wp_attached_file', '2018/05/quote.jpg'),
(211, 51, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1920;s:6:"height";i:814;s:4:"file";s:17:"2018/05/quote.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"quote-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:17:"quote-300x127.jpg";s:5:"width";i:300;s:6:"height";i:127;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:17:"quote-768x326.jpg";s:5:"width";i:768;s:6:"height";i:326;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:18:"quote-1024x434.jpg";s:5:"width";i:1024;s:6:"height";i:434;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(212, 15, '_thumbnail_id', '51'),
(213, 52, '_wp_attached_file', '2018/05/tents_1.png'),
(214, 52, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:392;s:6:"height";i:260;s:4:"file";s:19:"2018/05/tents_1.png";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"tents_1-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:19:"tents_1-300x199.png";s:5:"width";i:300;s:6:"height";i:199;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(215, 54, '_wp_attached_file', '2018/05/pop-up-tent.jpg'),
(216, 54, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:488;s:6:"height";i:488;s:4:"file";s:23:"2018/05/pop-up-tent.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:23:"pop-up-tent-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:23:"pop-up-tent-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(217, 44, 'service_slogan_1', NULL),
(218, 44, 'service_slogan_2', NULL),
(219, 60, '_edit_lock', '1525332212:1'),
(220, 60, '_edit_last', '1'),
(221, 60, 'about_project_completed', NULL),
(222, 60, 'about_team_member', NULL),
(223, 60, 'about_on_going_project', NULL),
(224, 60, 'about_weekly_event', NULL),
(225, 60, 'service_slogan_1', NULL),
(226, 60, 'service_slogan_2', NULL),
(227, 61, '_wp_attached_file', '2018/05/red.png'),
(228, 61, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:110;s:6:"height";i:23;s:4:"file";s:15:"2018/05/red.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(229, 62, '_wp_trash_meta_status', 'publish'),
(230, 62, '_wp_trash_meta_time', '1525332669'),
(231, 63, '_wp_trash_meta_status', 'publish'),
(232, 63, '_wp_trash_meta_time', '1525332756'),
(233, 65, '_edit_lock', '1525335906:1'),
(234, 65, '_edit_last', '1'),
(235, 65, 'field_5aeac17f8c4c9', 'a:14:{s:3:"key";s:19:"field_5aeac17f8c4c9";s:5:"label";s:15:"Header Banner 1";s:4:"name";s:15:"header_banner_1";s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";s:1:"0";s:13:"default_value";s:0:"";s:11:"placeholder";s:5:"Title";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:10:"formatting";s:4:"html";s:9:"maxlength";s:0:"";s:17:"conditional_logic";a:3:{s:6:"status";s:1:"0";s:5:"rules";a:1:{i:0;a:3:{s:5:"field";s:4:"null";s:8:"operator";s:2:"==";s:5:"value";s:0:"";}}s:8:"allorany";s:3:"all";}s:8:"order_no";i:0;}'),
(237, 65, 'position', 'normal'),
(238, 65, 'layout', 'no_box'),
(239, 65, 'hide_on_screen', ''),
(240, 65, 'about_project_completed', NULL),
(241, 65, 'about_team_member', NULL),
(242, 65, 'about_on_going_project', NULL),
(243, 65, 'about_weekly_event', NULL),
(244, 65, 'service_slogan_1', NULL),
(245, 65, 'service_slogan_2', NULL),
(250, 65, 'field_5aeac4167544e', 'a:14:{s:3:"key";s:19:"field_5aeac4167544e";s:5:"label";s:15:"Header Banner 2";s:4:"name";s:15:"header_banner_2";s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";s:1:"0";s:13:"default_value";s:0:"";s:11:"placeholder";s:5:"Title";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:10:"formatting";s:4:"html";s:9:"maxlength";s:0:"";s:17:"conditional_logic";a:3:{s:6:"status";s:1:"0";s:5:"rules";a:1:{i:0;a:2:{s:5:"field";s:4:"null";s:8:"operator";s:2:"==";}}s:8:"allorany";s:3:"all";}s:8:"order_no";i:1;}'),
(251, 65, 'field_5aeac4317544f', 'a:14:{s:3:"key";s:19:"field_5aeac4317544f";s:5:"label";s:15:"Header Banner 3";s:4:"name";s:15:"header_banner_3";s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";s:1:"0";s:13:"default_value";s:0:"";s:11:"placeholder";s:5:"Title";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:10:"formatting";s:4:"html";s:9:"maxlength";s:0:"";s:17:"conditional_logic";a:3:{s:6:"status";s:1:"0";s:5:"rules";a:1:{i:0;a:2:{s:5:"field";s:4:"null";s:8:"operator";s:2:"==";}}s:8:"allorany";s:3:"all";}s:8:"order_no";i:2;}'),
(252, 65, 'rule', 'a:5:{s:5:"param";s:4:"page";s:8:"operator";s:2:"==";s:5:"value";s:2:"11";s:8:"order_no";i:0;s:8:"group_no";i:0;}'),
(253, 65, '_wp_trash_meta_status', 'publish'),
(254, 65, '_wp_trash_meta_time', '1525335553'),
(255, 65, '_wp_desired_post_slug', 'acf_home'),
(256, 68, '_edit_lock', '1525345333:1'),
(257, 68, '_edit_last', '1'),
(258, 68, 'about_project_completed', NULL),
(259, 68, 'about_team_member', NULL),
(260, 68, 'about_on_going_project', NULL),
(261, 68, 'about_weekly_event', NULL),
(262, 68, 'service_slogan_1', NULL),
(263, 68, 'service_slogan_2', NULL),
(264, 70, '_wp_attached_file', '2018/05/event_1.png'),
(265, 70, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1920;s:6:"height";i:774;s:4:"file";s:19:"2018/05/event_1.png";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"event_1-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:19:"event_1-300x121.png";s:5:"width";i:300;s:6:"height";i:121;s:9:"mime-type";s:9:"image/png";}s:12:"medium_large";a:4:{s:4:"file";s:19:"event_1-768x310.png";s:5:"width";i:768;s:6:"height";i:310;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:20:"event_1-1024x413.png";s:5:"width";i:1024;s:6:"height";i:413;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(266, 71, '_wp_attached_file', '2018/05/event_2.png'),
(267, 71, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1920;s:6:"height";i:774;s:4:"file";s:19:"2018/05/event_2.png";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"event_2-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:19:"event_2-300x121.png";s:5:"width";i:300;s:6:"height";i:121;s:9:"mime-type";s:9:"image/png";}s:12:"medium_large";a:4:{s:4:"file";s:19:"event_2-768x310.png";s:5:"width";i:768;s:6:"height";i:310;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:20:"event_2-1024x413.png";s:5:"width";i:1024;s:6:"height";i:413;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(268, 68, '_thumbnail_id', '71'),
(269, 72, '_edit_lock', '1525345125:1'),
(270, 72, '_edit_last', '1'),
(271, 72, 'about_project_completed', NULL),
(272, 72, 'about_team_member', NULL),
(273, 72, 'about_on_going_project', NULL),
(274, 72, 'about_weekly_event', NULL),
(275, 72, 'service_slogan_1', NULL),
(276, 72, 'service_slogan_2', NULL),
(277, 72, '_thumbnail_id', '70'),
(278, 76, '_edit_lock', '1525345330:1'),
(279, 76, '_edit_last', '1'),
(282, 76, 'position', 'normal'),
(283, 76, 'layout', 'no_box'),
(284, 76, 'hide_on_screen', ''),
(285, 76, 'about_project_completed', NULL),
(286, 76, 'about_team_member', NULL),
(287, 76, 'about_on_going_project', NULL),
(288, 76, 'about_weekly_event', NULL),
(289, 76, 'service_slogan_1', NULL),
(290, 76, 'service_slogan_2', NULL),
(291, 76, 'field_5aeac8f449152', 'a:11:{s:3:"key";s:19:"field_5aeac8f449152";s:5:"label";s:7:"Image 1";s:4:"name";s:7:"image_1";s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";s:1:"0";s:11:"save_format";s:3:"url";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:17:"conditional_logic";a:3:{s:6:"status";s:1:"0";s:5:"rules";a:1:{i:0;a:3:{s:5:"field";s:4:"null";s:8:"operator";s:2:"==";s:5:"value";s:0:"";}}s:8:"allorany";s:3:"all";}s:8:"order_no";i:0;}'),
(293, 76, 'field_5aeac916e1dad', 'a:11:{s:3:"key";s:19:"field_5aeac916e1dad";s:5:"label";s:7:"Image 2";s:4:"name";s:7:"image_2";s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";s:1:"0";s:11:"save_format";s:3:"url";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:17:"conditional_logic";a:3:{s:6:"status";s:1:"0";s:5:"rules";a:1:{i:0;a:3:{s:5:"field";s:4:"null";s:8:"operator";s:2:"==";s:5:"value";s:0:"";}}s:8:"allorany";s:3:"all";}s:8:"order_no";i:1;}'),
(297, 78, '_wp_attached_file', '2018/05/tents_1-1.png'),
(298, 78, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:392;s:6:"height";i:260;s:4:"file";s:21:"2018/05/tents_1-1.png";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:21:"tents_1-1-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:21:"tents_1-1-300x199.png";s:5:"width";i:300;s:6:"height";i:199;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(299, 79, '_wp_attached_file', '2018/05/chair_1.png'),
(300, 79, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:400;s:6:"height";i:300;s:4:"file";s:19:"2018/05/chair_1.png";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"chair_1-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:19:"chair_1-300x225.png";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(301, 68, '_', 'field_5aeac8ec84862'),
(302, 68, 'image_1', '78'),
(303, 68, '_image_1', 'field_5aeac8f449152'),
(304, 68, 'image_2', '79'),
(305, 68, '_image_2', 'field_5aeac916e1dad'),
(307, 72, 'image_1', '79'),
(308, 72, '_image_1', 'field_5aeac8f449152'),
(309, 72, 'image_2', '78'),
(310, 72, '_image_2', 'field_5aeac916e1dad'),
(311, 76, 'field_5aead13e4e117', 'a:13:{s:3:"key";s:19:"field_5aead13e4e117";s:5:"label";s:17:"Double Image Show";s:4:"name";s:17:"double_image_show";s:4:"type";s:5:"radio";s:12:"instructions";s:0:"";s:8:"required";s:1:"0";s:7:"choices";a:2:{s:3:"Yes";s:3:"Yes";s:2:"No";s:2:"No";}s:12:"other_choice";s:1:"0";s:17:"save_other_choice";s:1:"0";s:13:"default_value";s:2:"No";s:6:"layout";s:8:"vertical";s:17:"conditional_logic";a:3:{s:6:"status";s:1:"0";s:5:"rules";a:1:{i:0;a:2:{s:5:"field";s:4:"null";s:8:"operator";s:2:"==";}}s:8:"allorany";s:3:"all";}s:8:"order_no";i:2;}'),
(314, 72, 'double_image_show', 'No'),
(315, 72, '_double_image_show', 'field_5aead13e4e117'),
(316, 76, 'rule', 'a:5:{s:5:"param";s:9:"post_type";s:8:"operator";s:2:"==";s:5:"value";s:6:"banner";s:8:"order_no";i:0;s:8:"group_no";i:0;}'),
(317, 68, 'double_image_show', 'Yes'),
(318, 68, '_double_image_show', 'field_5aead13e4e117'),
(319, 92, '_edit_lock', '1525345538:1'),
(320, 92, '_edit_last', '1'),
(321, 92, 'image_1', ''),
(322, 92, '_image_1', 'field_5aeac8f449152'),
(323, 92, 'image_2', ''),
(324, 92, '_image_2', 'field_5aeac916e1dad'),
(325, 92, 'double_image_show', 'No'),
(326, 92, '_double_image_show', 'field_5aead13e4e117'),
(327, 92, 'about_project_completed', NULL),
(328, 92, 'about_team_member', NULL),
(329, 92, 'about_on_going_project', NULL),
(330, 92, 'about_weekly_event', NULL),
(331, 92, 'service_slogan_1', NULL),
(332, 92, 'service_slogan_2', NULL),
(333, 55, '_wp_trash_meta_status', 'pending'),
(334, 55, '_wp_trash_meta_time', '1525345902'),
(335, 55, '_wp_desired_post_slug', ''),
(336, 55, 'about_project_completed', NULL),
(337, 55, 'about_team_member', NULL),
(338, 55, 'about_on_going_project', NULL),
(339, 55, 'about_weekly_event', NULL),
(340, 55, 'service_slogan_1', NULL),
(341, 55, 'service_slogan_2', NULL),
(342, 60, '_wp_trash_meta_status', 'publish'),
(343, 60, '_wp_trash_meta_time', '1525345906'),
(344, 60, '_wp_desired_post_slug', 'testimonials-1-2'),
(347, 46, 'service_slogan_1', NULL),
(348, 46, 'service_slogan_2', NULL),
(349, 98, '_form', '<div class="row">\n                                <div class="col-xs-12 col-sm-6">\n                                    <div class="form-group">\n                                        <label for="form-name" class="sr-only">Name</label>\n                                        [text* first-name id:form-name class:form-control placeholder "Name"]\n                                    </div>\n                                    <div class="space-10"></div>\n                                </div>\n                                <div class="col-xs-12 col-sm-6">\n                                    <div class="form-group">\n                                        <label for="form-email" class="sr-only">Email</label>\n                                        [email* email id:form-email class:form-control placeholder "Email"]\n                                    </div>\n                                    <div class="space-10"></div>\n                                </div>\n                                <div class="col-xs-12">\n                                    <div class="form-group">\n                                        <label for="form-subject" class="sr-only">Subject</label>\n                                        [text* subject id:form-subject class:form-control placeholder "Subject"]\n                                    </div>\n                                    <div class="space-10"></div>\n                                </div>\n                                <div class="col-xs-12">\n                                    <div class="form-group">\n                                        <label for="form-message" class="sr-only">comment</label>\n                                        [textarea message cols:20 rows:6 id:form-message class:form-control placeholder "Message"]\n                                    </div>\n<div>[cf7sr-simple-recaptcha]</div>\n                                    <div class="space-10"></div>\n                                    [submit class:btn class:btn-link class:no-round class:text-uppercase "Send message"]\n                                </div>\n                            </div>'),
(350, 98, '_mail', 'a:9:{s:6:"active";b:1;s:7:"subject";s:25:"Oz Production "[subject]"";s:6:"sender";s:44:"[first-name] <info@ozproductionservices.com>";s:9:"recipient";s:29:"info@ozproductionservices.com";s:4:"body";s:200:"<table>\n<tr>\n<td>Name:</td>\n<td>[first-name]</td>\n</tr>\n<tr>\n<td>Email:</td>\n<td>[email]</td>\n</tr>\n<tr>\n<td>Subject</td>\n<td>[subject]</td>\n</tr>\n<tr>\n<td>Comment</td>\n<td>message</td>\n</tr>\n</table>";s:18:"additional_headers";s:17:"Reply-To: [email]";s:11:"attachments";s:0:"";s:8:"use_html";b:0;s:13:"exclude_blank";b:0;}'),
(351, 98, '_mail_2', 'a:9:{s:6:"active";b:0;s:7:"subject";s:30:"Oz Production "[your-subject]"";s:6:"sender";s:45:"Oz Production <info@ozproductionservices.com>";s:9:"recipient";s:12:"[your-email]";s:4:"body";s:136:"Message Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Oz Production (http://localhost/oz_production/public_html)";s:18:"additional_headers";s:39:"Reply-To: info@ozproductionservices.com";s:11:"attachments";s:0:"";s:8:"use_html";b:0;s:13:"exclude_blank";b:0;}'),
(352, 98, '_messages', 'a:23:{s:12:"mail_sent_ok";s:45:"Thank you for your message. It has been sent.";s:12:"mail_sent_ng";s:71:"There was an error trying to send your message. Please try again later.";s:16:"validation_error";s:61:"One or more fields have an error. Please check and try again.";s:4:"spam";s:71:"There was an error trying to send your message. Please try again later.";s:12:"accept_terms";s:69:"You must accept the terms and conditions before sending your message.";s:16:"invalid_required";s:22:"The field is required.";s:16:"invalid_too_long";s:22:"The field is too long.";s:17:"invalid_too_short";s:23:"The field is too short.";s:12:"invalid_date";s:29:"The date format is incorrect.";s:14:"date_too_early";s:44:"The date is before the earliest one allowed.";s:13:"date_too_late";s:41:"The date is after the latest one allowed.";s:13:"upload_failed";s:46:"There was an unknown error uploading the file.";s:24:"upload_file_type_invalid";s:49:"You are not allowed to upload files of this type.";s:21:"upload_file_too_large";s:20:"The file is too big.";s:23:"upload_failed_php_error";s:38:"There was an error uploading the file.";s:14:"invalid_number";s:29:"The number format is invalid.";s:16:"number_too_small";s:47:"The number is smaller than the minimum allowed.";s:16:"number_too_large";s:46:"The number is larger than the maximum allowed.";s:23:"quiz_answer_not_correct";s:36:"The answer to the quiz is incorrect.";s:17:"captcha_not_match";s:31:"Your entered code is incorrect.";s:13:"invalid_email";s:38:"The e-mail address entered is invalid.";s:11:"invalid_url";s:19:"The URL is invalid.";s:11:"invalid_tel";s:32:"The telephone number is invalid.";}'),
(353, 98, '_additional_settings', ''),
(354, 98, '_locale', 'en_US'),
(356, 38, '_wp_trash_meta_status', 'publish'),
(357, 38, '_wp_trash_meta_time', '1525849967'),
(358, 38, '_wp_desired_post_slug', 'user-dashboard'),
(359, 34, '_wp_trash_meta_status', 'publish'),
(360, 34, '_wp_trash_meta_time', '1525849967'),
(361, 34, '_wp_desired_post_slug', 'user-login'),
(362, 36, '_wp_trash_meta_status', 'publish'),
(363, 36, '_wp_trash_meta_time', '1525849967'),
(364, 36, '_wp_desired_post_slug', 'user-registration');

-- --------------------------------------------------------

--
-- Table structure for table `ops_posts`
--

CREATE TABLE IF NOT EXISTS `ops_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=100 ;

--
-- Dumping data for table `ops_posts`
--

INSERT INTO `ops_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2018-04-30 06:23:21', '2018-04-30 06:23:21', 'Welcome to WordPress. This is your first post. Edit or delete it, then start writing!', 'Hello world!', '', 'trash', 'open', 'open', '', 'hello-world__trashed', '', '', '2018-05-02 14:12:00', '2018-05-02 14:12:00', '', 0, 'http://localhost/oz_production/public_html/?p=1', 0, 'post', '', 1),
(2, 1, '2018-04-30 06:23:21', '2018-04-30 06:23:21', 'This is an example page. It''s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\n\n<blockquote>Hi there! I''m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin'' caught in the rain.)</blockquote>\n\n...or something like this:\n\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\n\nAs a new WordPress user, you should go to <a href="http://localhost/oz_production/public_html/wp-admin/">your dashboard</a> to delete this page and create new pages for your content. Have fun!', 'Sample Page', '', 'trash', 'closed', 'open', '', 'sample-page__trashed', '', '', '2018-04-30 14:24:59', '2018-04-30 14:24:59', '', 0, 'http://localhost/oz_production/public_html/?page_id=2', 0, 'page', '', 0),
(4, 1, '2018-04-30 13:37:27', '2018-04-30 13:37:27', '[ec_store manufacturerid="1"]', 'Oz Production Event Services, LLC', '', 'publish', 'closed', 'closed', '', 'oz-production-event-services-llc', '', '', '2018-04-30 13:37:27', '2018-04-30 13:37:27', '', 0, 'http://localhost/oz_production/public_html/2018/04/30/oz-production-event-services-llc/', 0, 'ec_store', '', 0),
(5, 1, '2018-04-30 13:37:27', '2018-04-30 13:37:27', '[ec_store]', 'Store', '', 'publish', 'closed', 'closed', '', 'store', '', '', '2018-04-30 13:37:27', '2018-04-30 13:37:27', '', 0, 'http://localhost/oz_production/public_html/store/', 0, 'page', '', 0),
(6, 1, '2018-04-30 13:37:28', '2018-04-30 13:37:28', '[ec_cart]', 'Cart', '', 'publish', 'closed', 'closed', '', 'cart', '', '', '2018-04-30 13:37:28', '2018-04-30 13:37:28', '', 0, 'http://localhost/oz_production/public_html/cart/', 0, 'page', '', 0),
(7, 1, '2018-04-30 13:37:28', '2018-04-30 13:37:28', '[ec_account]', 'Account', '', 'publish', 'closed', 'closed', '', 'account', '', '', '2018-04-30 13:37:28', '2018-04-30 13:37:28', '', 0, 'http://localhost/oz_production/public_html/account/', 0, 'page', '', 0),
(8, 1, '2018-04-30 14:22:55', '2018-04-30 14:22:55', '', 'chair', '', 'inherit', 'open', 'closed', '', 'chair', '', '', '2018-04-30 14:22:55', '2018-04-30 14:22:55', '', 0, 'http://localhost/oz_production/public_html/wp-content/uploads/2018/04/chair.png', 0, 'attachment', 'image/png', 0),
(9, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[ec_store modelnumber="Chair"]', 'Chair', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis mattis fringilla lectus et blandit. Integer sit amet nulla sed mi porttitor tincidunt a non ligula. Quisque blandit dui odio, sed consectetur erat malesuada ac. Curabitur placerat orci magna, id malesuada massa iaculis id. Sed a felis id enim euismod tristique varius in libero. Sed tempus tristique felis ac vehicula. Vestibulum viverra, velit non aliquet rhoncus, odio dolor placerat dui, vel porttitor sem arcu ac massa. Mauris elementum, lacus sollicitudin dictum faucibus, magna enim aliquam libero, at pharetra justo quam eget arcu. Donec ut porttitor metus, luctus egestas dolor. Cras lacinia condimentum nunc fermentum vulputate. Donec feugiat auctor purus. Suspendisse lobortis diam eget pellentesque cursus. Proin non dictum lorem, porttitor tempor nisi.</p><p>Integer auctor lobortis nibh, a imperdiet dolor pulvinar a. Donec tempor aliquet mauris, vitae feugiat sapien dapibus sit amet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget libero sed arcu efficitur iaculis in ut quam. Fusce nisl tellus, ultrices vitae ex id, interdum porttitor diam. Aliquam et velit tellus. Vivamus varius nec lacus id interdum. Mauris sollicitudin orci eget lectus porta tincidunt. Phasellus suscipit, justo et ultricies tristique, eros libero maximus lacus, in pulvinar lectus arcu eu eros. Mauris nec convallis ligula, dictum dignissim lacus.</p>', 'publish', 'open', 'open', '', 'chair', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 0, 'http://localhost/oz_production/public_html/store/chair/', 0, 'ec_store', '', 0),
(10, 1, '2018-04-30 14:24:59', '2018-04-30 14:24:59', 'This is an example page. It''s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\n\n<blockquote>Hi there! I''m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin'' caught in the rain.)</blockquote>\n\n...or something like this:\n\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\n\nAs a new WordPress user, you should go to <a href="http://localhost/oz_production/public_html/wp-admin/">your dashboard</a> to delete this page and create new pages for your content. Have fun!', 'Sample Page', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2018-04-30 14:24:59', '2018-04-30 14:24:59', '', 2, 'http://localhost/oz_production/public_html/2018/04/30/2-revision-v1/', 0, 'revision', '', 0),
(11, 1, '2018-04-30 14:25:07', '2018-04-30 14:25:07', '', 'Home', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2018-04-30 14:38:56', '2018-04-30 14:38:56', '', 0, 'http://localhost/oz_production/public_html/?page_id=11', 0, 'page', '', 0),
(12, 1, '2018-04-30 14:25:07', '2018-04-30 14:25:07', '', 'Home', '', 'inherit', 'closed', 'closed', '', '11-revision-v1', '', '', '2018-04-30 14:25:07', '2018-04-30 14:25:07', '', 11, 'http://localhost/oz_production/public_html/2018/04/30/11-revision-v1/', 0, 'revision', '', 0),
(13, 1, '2018-04-30 14:25:26', '2018-04-30 14:25:26', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis mattis fringilla lectus et blandit. Integer sit amet nulla sed mi porttitor tincidunt a non ligula. Quisque blandit dui odio, sed consectetur erat malesuada ac. Curabitur placerat orci magna, id malesuada massa iaculis id. Sed a felis id enim euismod tristique varius in libero. Sed tempus tristique felis ac vehicula. Vestibulum viverra, velit non aliquet rhoncus, odio dolor placerat dui, vel porttitor sem arcu ac massa. Mauris elementum, lacus sollicitudin dictum faucibus, magna enim aliquam libero, at pharetra justo quam eget arcu. Donec ut porttitor metus, luctus egestas dolor.', 'About Us', '', 'publish', 'closed', 'closed', '', 'about-us', '', '', '2018-05-01 06:49:59', '2018-05-01 06:49:59', '', 0, 'http://localhost/oz_production/public_html/?page_id=13', 0, 'page', '', 0),
(14, 1, '2018-04-30 14:25:26', '2018-04-30 14:25:26', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis mattis fringilla lectus et blandit. Integer sit amet nulla sed mi porttitor tincidunt a non ligula. Quisque blandit dui odio, sed consectetur erat malesuada ac. Curabitur placerat orci magna, id malesuada massa iaculis id. Sed a felis id enim euismod tristique varius in libero. Sed tempus tristique felis ac vehicula. Vestibulum viverra, velit non aliquet rhoncus, odio dolor placerat dui, vel porttitor sem arcu ac massa. Mauris elementum, lacus sollicitudin dictum faucibus, magna enim aliquam libero, at pharetra justo quam eget arcu. Donec ut porttitor metus, luctus egestas dolor. Cras lacinia condimentum nunc fermentum vulputate. Donec feugiat auctor purus. Suspendisse lobortis diam eget pellentesque cursus. Proin non dictum lorem, porttitor tempor nisi.\r\n\r\nInteger auctor lobortis nibh, a imperdiet dolor pulvinar a. Donec tempor aliquet mauris, vitae feugiat sapien dapibus sit amet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget libero sed arcu efficitur iaculis in ut quam. Fusce nisl tellus, ultrices vitae ex id, interdum porttitor diam. Aliquam et velit tellus. Vivamus varius nec lacus id interdum. Mauris sollicitudin orci eget lectus porta tincidunt. Phasellus suscipit, justo et ultricies tristique, eros libero maximus lacus, in pulvinar lectus arcu eu eros. Mauris nec convallis ligula, dictum dignissim lacus.', 'About Us', '', 'inherit', 'closed', 'closed', '', '13-revision-v1', '', '', '2018-04-30 14:25:26', '2018-04-30 14:25:26', '', 13, 'http://localhost/oz_production/public_html/2018/04/30/13-revision-v1/', 0, 'revision', '', 0),
(15, 1, '2018-04-30 14:25:45', '2018-04-30 14:25:45', '', 'Services', '', 'publish', 'closed', 'closed', '', 'services', '', '', '2018-05-02 14:45:44', '2018-05-02 14:45:44', '', 0, 'http://localhost/oz_production/public_html/?page_id=15', 0, 'page', '', 0),
(16, 1, '2018-04-30 14:25:45', '2018-04-30 14:25:45', '', 'Services', '', 'inherit', 'closed', 'closed', '', '15-revision-v1', '', '', '2018-04-30 14:25:45', '2018-04-30 14:25:45', '', 15, 'http://localhost/oz_production/public_html/2018/04/30/15-revision-v1/', 0, 'revision', '', 0),
(17, 1, '2018-04-30 14:25:55', '2018-04-30 14:25:55', '', 'Contact Us', '', 'publish', 'closed', 'closed', '', 'contact-us', '', '', '2018-04-30 14:31:01', '2018-04-30 14:31:01', '', 0, 'http://localhost/oz_production/public_html/?page_id=17', 0, 'page', '', 0),
(18, 1, '2018-04-30 14:25:55', '2018-04-30 14:25:55', '', 'Contact Us', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2018-04-30 14:25:55', '2018-04-30 14:25:55', '', 17, 'http://localhost/oz_production/public_html/2018/04/30/17-revision-v1/', 0, 'revision', '', 0),
(19, 1, '2018-04-30 14:31:47', '0000-00-00 00:00:00', '', 'Home', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-04-30 14:31:47', '0000-00-00 00:00:00', '', 0, 'http://localhost/oz_production/public_html/?p=19', 1, 'nav_menu_item', '', 0),
(20, 1, '2018-04-30 14:32:48', '2018-04-30 14:32:48', ' ', '', '', 'publish', 'closed', 'closed', '', '20', '', '', '2018-05-01 06:59:17', '2018-05-01 06:59:17', '', 0, 'http://localhost/oz_production/public_html/?p=20', 2, 'nav_menu_item', '', 0),
(21, 1, '2018-04-30 14:32:48', '2018-04-30 14:32:48', ' ', '', '', 'publish', 'closed', 'closed', '', '21', '', '', '2018-05-01 06:59:17', '2018-05-01 06:59:17', '', 0, 'http://localhost/oz_production/public_html/?p=21', 4, 'nav_menu_item', '', 0),
(22, 1, '2018-04-30 14:32:48', '2018-04-30 14:32:48', ' ', '', '', 'publish', 'closed', 'closed', '', '22', '', '', '2018-05-01 06:59:17', '2018-05-01 06:59:17', '', 0, 'http://localhost/oz_production/public_html/?p=22', 5, 'nav_menu_item', '', 0),
(23, 1, '2018-04-30 14:32:48', '2018-04-30 14:32:48', ' ', '', '', 'publish', 'closed', 'closed', '', '23', '', '', '2018-05-01 06:59:17', '2018-05-01 06:59:17', '', 0, 'http://localhost/oz_production/public_html/?p=23', 6, 'nav_menu_item', '', 0),
(24, 1, '2018-04-30 14:32:47', '2018-04-30 14:32:47', ' ', '', '', 'publish', 'closed', 'closed', '', '24', '', '', '2018-05-01 06:59:17', '2018-05-01 06:59:17', '', 0, 'http://localhost/oz_production/public_html/?p=24', 1, 'nav_menu_item', '', 0),
(25, 1, '2018-04-30 14:31:49', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-04-30 14:31:49', '0000-00-00 00:00:00', '', 0, 'http://localhost/oz_production/public_html/?p=25', 1, 'nav_menu_item', '', 0),
(27, 1, '2018-04-30 15:09:51', '2018-04-30 15:09:51', '', 'table', '', 'inherit', 'open', 'closed', '', 'table', '', '', '2018-04-30 15:09:51', '2018-04-30 15:09:51', '', 0, 'http://localhost/oz_production/public_html/wp-content/uploads/2018/04/table.png', 0, 'attachment', 'image/png', 0),
(28, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[ec_store modelnumber="table"]', 'Table', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis mattis fringilla lectus et blandit. Integer sit amet nulla sed mi porttitor tincidunt a non ligula. Quisque blandit dui odio, sed consectetur erat malesuada ac. Curabitur placerat orci magna, id malesuada massa iaculis id. Sed a felis id enim euismod tristique varius in libero. Sed tempus tristique felis ac vehicula. Vestibulum viverra, velit non aliquet rhoncus, odio dolor placerat dui, vel porttitor sem arcu ac massa. Mauris elementum, lacus sollicitudin dictum faucibus, magna enim aliquam libero, at pharetra justo quam eget arcu. Donec ut porttitor metus, luctus egestas dolor. Cras lacinia condimentum nunc fermentum vulputate. Donec feugiat auctor purus. Suspendisse lobortis diam eget pellentesque cursus. Proin non dictum lorem, porttitor tempor nisi.</p><p>Integer auctor lobortis nibh, a imperdiet dolor pulvinar a. Donec tempor aliquet mauris, vitae feugiat sapien dapibus sit amet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget libero sed arcu efficitur iaculis in ut quam. Fusce nisl tellus, ultrices vitae ex id, interdum porttitor diam. Aliquam et velit tellus. Vivamus varius nec lacus id interdum. Mauris sollicitudin orci eget lectus porta tincidunt. Phasellus suscipit, justo et ultricies tristique, eros libero maximus lacus, in pulvinar lectus arcu eu eros. Mauris nec convallis ligula, dictum dignissim lacus.</p>', 'publish', 'open', 'open', '', 'table', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 0, 'http://localhost/oz_production/public_html/store/table/', 0, 'ec_store', '', 0),
(29, 1, '2018-05-01 06:29:11', '2018-05-01 06:29:11', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis mattis fringilla lectus et blandit. Integer sit amet nulla sed mi porttitor tincidunt a non ligula. Quisque blandit dui odio, sed consectetur erat malesuada ac. Curabitur placerat orci magna, id malesuada massa iaculis id. Sed a felis id enim euismod tristique varius in libero. Sed tempus tristique felis ac vehicula. Vestibulum viverra, velit non aliquet rhoncus, odio dolor placerat dui, vel porttitor sem arcu ac massa. Mauris elementum, lacus sollicitudin dictum faucibus, magna enim aliquam libero, at pharetra justo quam eget arcu. Donec ut porttitor metus, luctus egestas dolor.', 'About Us', '', 'inherit', 'closed', 'closed', '', '13-revision-v1', '', '', '2018-05-01 06:29:11', '2018-05-01 06:29:11', '', 13, 'http://localhost/oz_production/public_html/13-revision-v1/', 0, 'revision', '', 0),
(30, 1, '2018-05-01 06:59:17', '2018-05-01 06:59:17', ' ', '', '', 'publish', 'closed', 'closed', '', '30', '', '', '2018-05-01 06:59:17', '2018-05-01 06:59:17', '', 0, 'http://localhost/oz_production/public_html/?p=30', 3, 'nav_menu_item', '', 0),
(31, 1, '2018-05-02 13:03:21', '2018-05-02 13:03:21', 'This is the most awesome, full featured, easy, costomizeble theme. It’s extremely responsive and very helpful to all suggestions.', 'Testimonials 1', '', 'publish', 'closed', 'closed', '', 'testimonials-1', '', '', '2018-05-02 13:03:21', '2018-05-02 13:03:21', '', 0, 'http://localhost/oz_production/public_html/?post_type=testimonial&#038;p=31', 0, 'testimonial', '', 0),
(32, 1, '2018-05-02 13:03:59', '2018-05-02 13:03:59', 'Williamsburg carles vegan helvetica. Cosby sweater eu banh mi, qui irure terry richardson ex squid Aliquip placeat salvia cillum iphone.', 'Testimonials 2', '', 'publish', 'closed', 'closed', '', 'testimonials-2', '', '', '2018-05-02 13:04:17', '2018-05-02 13:04:17', '', 0, 'http://localhost/oz_production/public_html/?post_type=testimonial&#038;p=32', 0, 'testimonial', '', 0),
(33, 1, '2018-05-02 13:04:26', '2018-05-02 13:04:26', 'Williamsburg carles vegan helvetica. Cosby sweater eu banh mi, qui irure terry richardson ex squid Aliquip placeat salvia cillum iphone.', 'Testimonials 3', '', 'publish', 'closed', 'closed', '', 'testimonials-3', '', '', '2018-05-02 13:04:26', '2018-05-02 13:04:26', '', 0, 'http://localhost/oz_production/public_html/?post_type=testimonial&#038;p=33', 0, 'testimonial', '', 0),
(34, 1, '2018-05-02 13:17:50', '2018-05-02 13:17:50', '[fed_login]', 'User Login', '', 'trash', 'closed', 'closed', '', 'user-login__trashed', '', '', '2018-05-09 07:12:47', '2018-05-09 07:12:47', '', 0, 'http://localhost/oz_production/public_html/?page_id=34', 0, 'page', '', 0),
(35, 1, '2018-05-02 13:17:50', '2018-05-02 13:17:50', '[fed_login]', 'User Login', '', 'inherit', 'closed', 'closed', '', '34-revision-v1', '', '', '2018-05-02 13:17:50', '2018-05-02 13:17:50', '', 34, 'http://localhost/oz_production/public_html/34-revision-v1/', 0, 'revision', '', 0),
(36, 1, '2018-05-02 13:18:19', '2018-05-02 13:18:19', '', 'User Registration', '', 'trash', 'closed', 'closed', '', 'user-registration__trashed', '', '', '2018-05-09 07:12:47', '2018-05-09 07:12:47', '', 0, 'http://localhost/oz_production/public_html/?page_id=36', 0, 'page', '', 0),
(37, 1, '2018-05-02 13:18:19', '2018-05-02 13:18:19', '', 'User Registration', '', 'inherit', 'closed', 'closed', '', '36-revision-v1', '', '', '2018-05-02 13:18:19', '2018-05-02 13:18:19', '', 36, 'http://localhost/oz_production/public_html/36-revision-v1/', 0, 'revision', '', 0),
(38, 1, '2018-05-02 13:18:41', '2018-05-02 13:18:41', '[fed_dashboard]', 'User Dashboard', '', 'trash', 'closed', 'closed', '', 'user-dashboard__trashed', '', '', '2018-05-09 07:12:47', '2018-05-09 07:12:47', '', 0, 'http://localhost/oz_production/public_html/?page_id=38', 0, 'page', '', 0),
(39, 1, '2018-05-02 13:18:41', '2018-05-02 13:18:41', '[fed_dashboard]', 'User Dashboard', '', 'inherit', 'closed', 'closed', '', '38-revision-v1', '', '', '2018-05-02 13:18:41', '2018-05-02 13:18:41', '', 38, 'http://localhost/oz_production/public_html/38-revision-v1/', 0, 'revision', '', 0),
(40, 1, '2018-05-02 13:49:30', '2018-05-02 13:49:30', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit', 'Testimonials', '', 'publish', 'closed', 'closed', '', 'testimonials', '', '', '2018-05-02 13:49:38', '2018-05-02 13:49:38', '', 0, 'http://localhost/oz_production/public_html/?page_id=40', 0, 'page', '', 0),
(41, 1, '2018-05-02 13:49:30', '2018-05-02 13:49:30', '', 'Testimonials', '', 'inherit', 'closed', 'closed', '', '40-revision-v1', '', '', '2018-05-02 13:49:30', '2018-05-02 13:49:30', '', 40, 'http://localhost/oz_production/public_html/40-revision-v1/', 0, 'revision', '', 0),
(42, 1, '2018-05-02 13:49:38', '2018-05-02 13:49:38', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit', 'Testimonials', '', 'inherit', 'closed', 'closed', '', '40-revision-v1', '', '', '2018-05-02 13:49:38', '2018-05-02 13:49:38', '', 40, 'http://localhost/oz_production/public_html/40-revision-v1/', 0, 'revision', '', 0),
(43, 1, '2018-05-02 14:12:00', '2018-05-02 14:12:00', 'Welcome to WordPress. This is your first post. Edit or delete it, then start writing!', 'Hello world!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2018-05-02 14:12:00', '2018-05-02 14:12:00', '', 1, 'http://localhost/oz_production/public_html/1-revision-v1/', 0, 'revision', '', 0),
(44, 1, '2018-05-02 14:12:24', '2018-05-02 14:12:24', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam sit nonummy nibh euismod tincidunt ut laoreet dolore magna aliquarm erat sit volutpat. Nostrud exerci tation ullamcorper suscipit lobortis nisl aliquip commodo consequat.\r\n\r\nDuis autem vel eum iriure dolor vulputate velit esse molestie at dolore.', 'Footer About Us', '', 'publish', 'open', 'open', '', 'footer-about-us', '', '', '2018-05-02 14:29:53', '2018-05-02 14:29:53', '', 0, 'http://localhost/oz_production/public_html/?p=44', 0, 'post', '', 0),
(45, 1, '2018-05-02 14:12:24', '2018-05-02 14:12:24', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam sit nonummy nibh euismod tincidunt ut laoreet dolore magna aliquarm erat sit volutpat. Nostrud exerci tation ullamcorper suscipit lobortis nisl aliquip commodo consequat.\r\n\r\nDuis autem vel eum iriure dolor vulputate velit esse molestie at dolore.', 'Footer About Us', '', 'inherit', 'closed', 'closed', '', '44-revision-v1', '', '', '2018-05-02 14:12:24', '2018-05-02 14:12:24', '', 44, 'http://localhost/oz_production/public_html/44-revision-v1/', 0, 'revision', '', 0),
(46, 1, '2018-05-02 14:13:12', '2018-05-02 14:13:12', '35, Lorem Lis Ipsum, NY\r\n<br>\r\nNewyork, US\r\n<br>\r\nPhone: 111 111 1111\r\n<br>\r\nFax: *** *** ****\r\n<br>\r\nEmail: info@ozproductionservices.com', 'Contact', '', 'publish', 'open', 'open', '', 'contact', '', '', '2018-05-03 11:14:38', '2018-05-03 11:14:38', '', 0, 'http://localhost/oz_production/public_html/?p=46', 0, 'post', '', 0),
(47, 1, '2018-05-02 14:13:12', '2018-05-02 14:13:12', '', 'Contact', '', 'inherit', 'closed', 'closed', '', '46-revision-v1', '', '', '2018-05-02 14:13:12', '2018-05-02 14:13:12', '', 46, 'http://localhost/oz_production/public_html/46-revision-v1/', 0, 'revision', '', 0),
(49, 1, '2018-05-02 14:15:37', '2018-05-02 14:15:37', '35, Lorem Lis Ipsum, NY\r\nNewyork, US\r\nPhone: 111 111 1111\r\nFax: *** *** ****\r\nEmail: info@ozproductionservices.com', 'Contact', '', 'inherit', 'closed', 'closed', '', '46-revision-v1', '', '', '2018-05-02 14:15:37', '2018-05-02 14:15:37', '', 46, 'http://localhost/oz_production/public_html/46-revision-v1/', 0, 'revision', '', 0),
(50, 1, '2018-05-02 14:31:53', '2018-05-02 14:31:53', '35, Lorem Lis Ipsum, NY\r\n<br>\r\nNewyork, US\r\n\r\nPhone: 111 111 1111\r\n<br>\r\nFax: *** *** ****\r\n<br>\r\nEmail: info@ozproductionservices.com', 'Contact', '', 'inherit', 'closed', 'closed', '', '46-revision-v1', '', '', '2018-05-02 14:31:53', '2018-05-02 14:31:53', '', 46, 'http://localhost/oz_production/public_html/46-revision-v1/', 0, 'revision', '', 0),
(51, 1, '2018-05-02 14:45:37', '2018-05-02 14:45:37', '', 'quote', '', 'inherit', 'open', 'closed', '', 'quote', '', '', '2018-05-02 14:45:37', '2018-05-02 14:45:37', '', 15, 'http://localhost/oz_production/public_html/wp-content/uploads/2018/05/quote.jpg', 0, 'attachment', 'image/jpeg', 0),
(52, 1, '2018-05-02 14:51:58', '2018-05-02 14:51:58', '', 'tents_1', '', 'inherit', 'open', 'closed', '', 'tents_1', '', '', '2018-05-02 14:51:58', '2018-05-02 14:51:58', '', 0, 'http://localhost/oz_production/public_html/wp-content/uploads/2018/05/tents_1.png', 0, 'attachment', 'image/png', 0),
(53, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[ec_store modelnumber="Quest-Canopy"]', 'Pop Up Tent', 'The Quest® Q64 10 FT. x 10 FT. Slant Leg Instant Up Canopy offers cooling shade when you need it, where you need it. The angled steel frame creates a sturdy 10\\'' x 10\\'' base, while the 8\\'' x 8\\'' dome top offers 64 square feet of shade to lounge beneath or watch from the sidelines. Its instant up design means set-up is a snap, including 4 ground stakes for extra stability. This recreational canopy folds down to fit inside its convenient carry bag.', 'publish', 'open', 'open', '', 'pop-up-tent', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 0, 'http://localhost/oz_production/public_html/store/pop-up-tent/', 0, 'ec_store', '', 0),
(54, 1, '2018-05-02 14:54:14', '2018-05-02 14:54:14', '', 'pop-up-tent', '', 'inherit', 'open', 'closed', '', 'pop-up-tent-2', '', '', '2018-05-02 14:54:14', '2018-05-02 14:54:14', '', 0, 'http://localhost/oz_production/public_html/wp-content/uploads/2018/05/pop-up-tent.jpg', 0, 'attachment', 'image/jpeg', 0),
(55, 2, '2018-05-03 11:11:42', '2018-05-03 11:11:42', '', 'Testimonials', '', 'trash', 'open', 'closed', '', '__trashed', '', '', '2018-05-03 11:11:42', '2018-05-03 11:11:42', '', 0, 'http://localhost/oz_production/public_html/?post_type=testimonial&#038;p=55', 0, 'testimonial', '', 0),
(56, 2, '2018-05-03 07:11:36', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2018-05-03 07:11:36', '0000-00-00 00:00:00', '', 0, 'http://localhost/oz_production/public_html/?p=56', 0, 'post', '', 0),
(57, 3, '2018-05-03 07:15:46', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2018-05-03 07:15:46', '0000-00-00 00:00:00', '', 0, 'http://localhost/oz_production/public_html/?p=57', 0, 'post', '', 0),
(58, 3, '2018-05-03 07:15:50', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2018-05-03 07:15:50', '0000-00-00 00:00:00', '', 0, 'http://localhost/oz_production/public_html/?p=58', 0, 'post', '', 0),
(59, 3, '2018-05-03 07:15:55', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-05-03 07:15:55', '0000-00-00 00:00:00', '', 0, 'http://localhost/oz_production/public_html/?post_type=testimonial&p=59', 0, 'testimonial', '', 0),
(60, 3, '2018-05-03 07:19:39', '2018-05-03 07:19:39', 'Test Content', 'Testimonials 1', '', 'trash', 'open', 'closed', '', 'testimonials-1-2__trashed', '', '', '2018-05-03 11:11:46', '2018-05-03 11:11:46', '', 0, 'http://localhost/oz_production/public_html/?post_type=testimonial&#038;p=60', 0, 'testimonial', '', 0),
(61, 1, '2018-05-03 07:30:23', '2018-05-03 07:30:23', '', 'red', '', 'inherit', 'open', 'closed', '', 'red', '', '', '2018-05-03 07:30:23', '2018-05-03 07:30:23', '', 0, 'http://localhost/oz_production/public_html/wp-content/uploads/2018/05/red.png', 0, 'attachment', 'image/png', 0),
(62, 1, '2018-05-03 07:31:09', '2018-05-03 07:31:09', '{\n    "site_icon": {\n        "value": 61,\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-05-03 07:31:09"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '38323dbc-299a-4cc0-b45d-e7d2788b027c', '', '', '2018-05-03 07:31:09', '2018-05-03 07:31:09', '', 0, 'http://localhost/oz_production/public_html/38323dbc-299a-4cc0-b45d-e7d2788b027c/', 0, 'customize_changeset', '', 0),
(63, 1, '2018-05-03 07:32:36', '2018-05-03 07:32:36', '{\n    "site_icon": {\n        "value": "",\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-05-03 07:32:36"\n    },\n    "ozproduction::custom_logo": {\n        "value": 61,\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2018-05-03 07:32:36"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '9e1968ce-71c1-443e-b455-350784e9de7a', '', '', '2018-05-03 07:32:36', '2018-05-03 07:32:36', '', 0, 'http://localhost/oz_production/public_html/9e1968ce-71c1-443e-b455-350784e9de7a/', 0, 'customize_changeset', '', 0),
(65, 1, '2018-05-03 08:01:37', '2018-05-03 08:01:37', '', 'Home', '', 'trash', 'closed', 'closed', '', 'acf_home__trashed', '', '', '2018-05-03 08:19:13', '2018-05-03 08:19:13', '', 0, 'http://localhost/oz_production/public_html/?post_type=acf&#038;p=65', 0, 'acf', '', 0),
(68, 1, '2018-05-03 08:26:48', '2018-05-03 08:26:48', 'Lorem ipsum dolor sit amet, consectetuer adipiscing<br> elit amet sed diam nonummy nibh dolore.', 'Parallax One Page Has Arrived', '', 'publish', 'open', 'closed', '', 'parallax-one-page-has-arrived', '', '', '2018-05-03 09:44:51', '2018-05-03 09:44:51', '', 0, 'http://localhost/oz_production/public_html/?post_type=banner&#038;p=68', 0, 'banner', '', 0),
(70, 1, '2018-05-03 08:27:52', '2018-05-03 08:27:52', '', 'event_1', '', 'inherit', 'open', 'closed', '', 'event_1', '', '', '2018-05-03 08:27:52', '2018-05-03 08:27:52', '', 68, 'http://localhost/oz_production/public_html/wp-content/uploads/2018/05/event_1.png', 0, 'attachment', 'image/png', 0),
(71, 1, '2018-05-03 08:28:01', '2018-05-03 08:28:01', '', 'event_2', '', 'inherit', 'open', 'closed', '', 'event_2', '', '', '2018-05-03 08:28:01', '2018-05-03 08:28:01', '', 68, 'http://localhost/oz_production/public_html/wp-content/uploads/2018/05/event_2.png', 0, 'attachment', 'image/png', 0),
(72, 1, '2018-05-03 08:29:38', '2018-05-03 08:29:38', 'Lorem ipsum dolor sit amet, consectetuer adipiscing<br> elit amet sed diam nonummy nibh dolore.', 'Extremely Responsive  Design', '', 'publish', 'open', 'closed', '', 'extremely-responsive-design', '', '', '2018-05-03 09:08:51', '2018-05-03 09:08:51', '', 0, 'http://localhost/oz_production/public_html/?post_type=banner&#038;p=72', 0, 'banner', '', 0),
(74, 1, '2018-05-03 08:29:43', '2018-05-03 08:29:43', '', 'Home', '', 'inherit', 'closed', 'closed', '', '11-autosave-v1', '', '', '2018-05-03 08:29:43', '2018-05-03 08:29:43', '', 11, 'http://localhost/oz_production/public_html/11-autosave-v1/', 0, 'revision', '', 0),
(76, 1, '2018-05-03 08:31:45', '2018-05-03 08:31:45', '', 'Home Banner', '', 'publish', 'closed', 'closed', '', 'acf_home-banner', '', '', '2018-05-03 09:09:27', '2018-05-03 09:09:27', '', 0, 'http://localhost/oz_production/public_html/?post_type=acf&#038;p=76', 0, 'acf', '', 0),
(78, 1, '2018-05-03 08:33:57', '2018-05-03 08:33:57', '', 'tents_1', '', 'inherit', 'open', 'closed', '', 'tents_1-2', '', '', '2018-05-03 08:33:57', '2018-05-03 08:33:57', '', 68, 'http://localhost/oz_production/public_html/wp-content/uploads/2018/05/tents_1-1.png', 0, 'attachment', 'image/png', 0),
(79, 1, '2018-05-03 08:34:09', '2018-05-03 08:34:09', '', 'chair_1', '', 'inherit', 'open', 'closed', '', 'chair_1', '', '', '2018-05-03 08:34:09', '2018-05-03 08:34:09', '', 68, 'http://localhost/oz_production/public_html/wp-content/uploads/2018/05/chair_1.png', 0, 'attachment', 'image/png', 0),
(91, 1, '2018-05-03 09:35:10', '2018-05-03 09:35:10', 'Lorem ipsum dolor sit amet, consectetuer adipiscing<br> elit amet sed diam nonummy nibh dolore.', 'Extremely Responsive  Design', '', 'inherit', 'closed', 'closed', '', '72-autosave-v1', '', '', '2018-05-03 09:35:10', '2018-05-03 09:35:10', '', 72, 'http://localhost/oz_production/public_html/72-autosave-v1/', 0, 'revision', '', 0),
(92, 1, '2018-05-03 11:02:18', '2018-05-03 11:02:18', 'Lorem ipsum dolor sit amet, consectetuer adipiscing<br> elit amet sed diam nonummy nibh dolore.', 'Parallax One Page Has Arrived', '', 'publish', 'open', 'closed', '', 'parallax-one-page-has-arrived-2', '', '', '2018-05-03 11:02:18', '2018-05-03 11:02:18', '', 0, 'http://localhost/oz_production/public_html/?post_type=banner&#038;p=92', 0, 'banner', '', 0),
(93, 1, '2018-05-03 11:02:20', '2018-05-03 11:02:20', 'Lorem ipsum dolor sit amet, consectetuer adipiscing<br> elit amet sed diam nonummy nibh dolore.', 'Parallax One Page Has Arrived', '', 'inherit', 'closed', 'closed', '', '92-autosave-v1', '', '', '2018-05-03 11:02:20', '2018-05-03 11:02:20', '', 92, 'http://localhost/oz_production/public_html/92-autosave-v1/', 0, 'revision', '', 0),
(94, 1, '2018-05-03 11:04:42', '2018-05-03 11:04:42', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit', 'Testimonials', '', 'inherit', 'closed', 'closed', '', '40-autosave-v1', '', '', '2018-05-03 11:04:42', '2018-05-03 11:04:42', '', 40, 'http://localhost/oz_production/public_html/40-autosave-v1/', 0, 'revision', '', 0),
(95, 1, '2018-05-03 11:14:01', '2018-05-03 11:14:01', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam sit nonummy nibh euismod tincidunt ut laoreet dolore magna aliquarm erat sit volutpat. Nostrud exerci tation ullamcorper suscipit lobortis nisl aliquip commodo consequat.\n\nDuis autem vel eum iriure dolor vulputate velit esse molestie at dolore.', 'Footer About Us', '', 'inherit', 'closed', 'closed', '', '44-autosave-v1', '', '', '2018-05-03 11:14:01', '2018-05-03 11:14:01', '', 44, 'http://localhost/oz_production/public_html/44-autosave-v1/', 0, 'revision', '', 0),
(96, 1, '2018-05-03 11:14:38', '2018-05-03 11:14:38', '35, Lorem Lis Ipsum, NY\r\n<br>\r\nNewyork, US\r\n<br>\r\nPhone: 111 111 1111\r\n<br>\r\nFax: *** *** ****\r\n<br>\r\nEmail: info@ozproductionservices.com', 'Contact', '', 'inherit', 'closed', 'closed', '', '46-revision-v1', '', '', '2018-05-03 11:14:38', '2018-05-03 11:14:38', '', 46, 'http://localhost/oz_production/public_html/46-revision-v1/', 0, 'revision', '', 0),
(97, 1, '2018-05-03 11:14:42', '2018-05-03 11:14:42', '35, Lorem Lis Ipsum, NY\n<br>\nNewyork, US\n<br>\nPhone: 111 111 1111\n<br>\nFax: *** *** ****\n<br>\nEmail: info@ozproductionservices.com', 'Contact', '', 'inherit', 'closed', 'closed', '', '46-autosave-v1', '', '', '2018-05-03 11:14:42', '2018-05-03 11:14:42', '', 46, 'http://localhost/oz_production/public_html/46-autosave-v1/', 0, 'revision', '', 0),
(98, 1, '2018-05-03 12:22:59', '2018-05-03 12:22:59', '<div class="row">\r\n                                <div class="col-xs-12 col-sm-6">\r\n                                    <div class="form-group">\r\n                                        <label for="form-name" class="sr-only">Name</label>\r\n                                        [text* first-name id:form-name class:form-control placeholder "Name"]\r\n                                    </div>\r\n                                    <div class="space-10"></div>\r\n                                </div>\r\n                                <div class="col-xs-12 col-sm-6">\r\n                                    <div class="form-group">\r\n                                        <label for="form-email" class="sr-only">Email</label>\r\n                                        [email* email id:form-email class:form-control placeholder "Email"]\r\n                                    </div>\r\n                                    <div class="space-10"></div>\r\n                                </div>\r\n                                <div class="col-xs-12">\r\n                                    <div class="form-group">\r\n                                        <label for="form-subject" class="sr-only">Subject</label>\r\n                                        [text* subject id:form-subject class:form-control placeholder "Subject"]\r\n                                    </div>\r\n                                    <div class="space-10"></div>\r\n                                </div>\r\n                                <div class="col-xs-12">\r\n                                    <div class="form-group">\r\n                                        <label for="form-message" class="sr-only">comment</label>\r\n                                        [textarea message cols:20 rows:6 id:form-message class:form-control placeholder "Message"]\r\n                                    </div>\r\n<div>[cf7sr-simple-recaptcha]</div>\r\n                                    <div class="space-10"></div>\r\n                                    [submit class:btn class:btn-link class:no-round class:text-uppercase "Send message"]\r\n                                </div>\r\n                            </div>\n1\nOz Production "[subject]"\n[first-name] <info@ozproductionservices.com>\ninfo@ozproductionservices.com\n<table>\r\n<tr>\r\n<td>Name:</td>\r\n<td>[first-name]</td>\r\n</tr>\r\n<tr>\r\n<td>Email:</td>\r\n<td>[email]</td>\r\n</tr>\r\n<tr>\r\n<td>Subject</td>\r\n<td>[subject]</td>\r\n</tr>\r\n<tr>\r\n<td>Comment</td>\r\n<td>message</td>\r\n</tr>\r\n</table>\nReply-To: [email]\n\n\n\n\nOz Production "[your-subject]"\nOz Production <info@ozproductionservices.com>\n[your-email]\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on Oz Production (http://localhost/oz_production/public_html)\nReply-To: info@ozproductionservices.com\n\n\n\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.\nThe date format is incorrect.\nThe date is before the earliest one allowed.\nThe date is after the latest one allowed.\nThere was an unknown error uploading the file.\nYou are not allowed to upload files of this type.\nThe file is too big.\nThere was an error uploading the file.\nThe number format is invalid.\nThe number is smaller than the minimum allowed.\nThe number is larger than the maximum allowed.\nThe answer to the quiz is incorrect.\nYour entered code is incorrect.\nThe e-mail address entered is invalid.\nThe URL is invalid.\nThe telephone number is invalid.', 'Contact form', '', 'publish', 'closed', 'closed', '', 'contact-form-1', '', '', '2018-05-03 13:12:32', '2018-05-03 13:12:32', '', 0, 'http://localhost/oz_production/public_html/?post_type=wpcf7_contact_form&#038;p=98', 0, 'wpcf7_contact_form', '', 0),
(99, 1, '2018-05-09 07:12:38', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2018-05-09 07:12:38', '0000-00-00 00:00:00', '', 0, 'http://localhost/oz_production/public_html/?p=99', 0, 'post', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ops_termmeta`
--

CREATE TABLE IF NOT EXISTS `ops_termmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ops_terms`
--

CREATE TABLE IF NOT EXISTS `ops_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `ops_terms`
--

INSERT INTO `ops_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'Main_Menu', 'main_menu', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ops_term_relationships`
--

CREATE TABLE IF NOT EXISTS `ops_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `ops_term_relationships`
--

INSERT INTO `ops_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(20, 2, 0),
(21, 2, 0),
(22, 2, 0),
(23, 2, 0),
(24, 2, 0),
(30, 2, 0),
(44, 1, 0),
(46, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ops_term_taxonomy`
--

CREATE TABLE IF NOT EXISTS `ops_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `ops_term_taxonomy`
--

INSERT INTO `ops_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 2),
(2, 2, 'nav_menu', '', 0, 6);

-- --------------------------------------------------------

--
-- Table structure for table `ops_usermeta`
--

CREATE TABLE IF NOT EXISTS `ops_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=74 ;

--
-- Dumping data for table `ops_usermeta`
--

INSERT INTO `ops_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'false'),
(11, 1, 'locale', ''),
(12, 1, 'ops_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(13, 1, 'ops_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', ''),
(15, 1, 'show_welcome_panel', '1'),
(17, 1, 'ops_dashboard_quick_press_last_post_id', '99'),
(18, 1, 'community-events-location', 'a:1:{s:2:"ip";s:2:"::";}'),
(19, 1, 'ops_user-settings', 'libraryContent=browse&editor=html'),
(20, 1, 'ops_user-settings-time', '1525330260'),
(21, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:"link-target";i:1;s:11:"css-classes";i:2;s:3:"xfn";i:3;s:11:"description";i:4;s:15:"title-attribute";}'),
(22, 1, 'metaboxhidden_nav-menus', 'a:2:{i:0;s:22:"add-post-type-ec_store";i:1;s:12:"add-post_tag";}'),
(23, 1, 'nav_menu_recently_edited', '2'),
(24, 1, 'closedpostboxes_testimonial', 'a:0:{}'),
(25, 1, 'metaboxhidden_testimonial', 'a:2:{i:0;s:10:"postcustom";i:1;s:7:"slugdiv";}'),
(27, 2, 'nickname', 'mamun'),
(28, 2, 'first_name', ''),
(29, 2, 'last_name', ''),
(30, 2, 'description', ''),
(31, 2, 'rich_editing', 'true'),
(32, 2, 'syntax_highlighting', 'true'),
(33, 2, 'comment_shortcuts', 'false'),
(34, 2, 'admin_color', 'fresh'),
(35, 2, 'use_ssl', '0'),
(36, 2, 'show_admin_bar_front', 'true'),
(37, 2, 'locale', ''),
(38, 2, 'user_login', 'mamun'),
(39, 2, 'user_pass', 'mamun@1234#'),
(40, 2, 'confirmation_password', 'mamun@1234#'),
(41, 2, 'user_email', 'mamunbdaiub@gmail.com'),
(42, 2, 'ops_capabilities', 'a:1:{s:6:"author";b:1;}'),
(43, 2, 'ops_user_level', '2'),
(44, 2, 'dismissed_wp_pointers', ''),
(46, 2, 'session_tokens', 'a:1:{s:64:"fabae56e25c4e9eaeae4bb5e80d34ce5d104af03b73320597085a8fb5db874ea";a:4:{s:10:"expiration";i:1525503050;s:2:"ip";s:3:"::1";s:2:"ua";s:78:"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0";s:5:"login";i:1525330250;}}'),
(47, 2, 'ops_dashboard_quick_press_last_post_id', '56'),
(48, 3, 'nickname', 'rajib'),
(49, 3, 'first_name', ''),
(50, 3, 'last_name', ''),
(51, 3, 'description', ''),
(52, 3, 'rich_editing', 'true'),
(53, 3, 'syntax_highlighting', 'true'),
(54, 3, 'comment_shortcuts', 'false'),
(55, 3, 'admin_color', 'fresh'),
(56, 3, 'use_ssl', '0'),
(57, 3, 'show_admin_bar_front', 'true'),
(58, 3, 'locale', ''),
(59, 3, 'user_login', 'rajib'),
(60, 3, 'user_pass', '123456'),
(61, 3, 'confirmation_password', '123456'),
(62, 3, 'user_email', 'mamun@gmail.com'),
(63, 3, 'ops_capabilities', 'a:1:{s:6:"author";b:1;}'),
(64, 3, 'ops_user_level', '2'),
(65, 3, 'dismissed_wp_pointers', ''),
(66, 3, 'session_tokens', 'a:1:{s:64:"6a1d2cd949275b45162c88bdb624d79a74e2c846e03c2650b107dc07128cd59a";a:4:{s:10:"expiration";i:1525504535;s:2:"ip";s:3:"::1";s:2:"ua";s:78:"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0";s:5:"login";i:1525331735;}}'),
(67, 1, 'closedpostboxes_page', 'a:0:{}'),
(68, 1, 'metaboxhidden_page', 'a:10:{i:0;s:24:"wp-easycart-product-lock";i:1;s:30:"wpeasycart_restrict_product_id";i:2;s:27:"wpeasycart_restrict_user_id";i:3;s:27:"wpeasycart_restrict_role_id";i:4;s:32:"wpeasycart_restrict_redirect_url";i:5;s:10:"postcustom";i:6;s:16:"commentstatusdiv";i:7;s:11:"commentsdiv";i:8;s:7:"slugdiv";i:9;s:9:"authordiv";}'),
(69, 1, 'closedpostboxes_banner', 'a:0:{}'),
(70, 1, 'metaboxhidden_banner', 'a:3:{i:0;s:16:"commentstatusdiv";i:1;s:11:"commentsdiv";i:2;s:7:"slugdiv";}'),
(71, 1, 'closedpostboxes_dashboard', 'a:1:{i:0;s:24:"ec_free_dashboard_widget";}'),
(72, 1, 'metaboxhidden_dashboard', 'a:0:{}'),
(73, 1, 'aioseop_seen_about_page', '2.5');

-- --------------------------------------------------------

--
-- Table structure for table `ops_users`
--

CREATE TABLE IF NOT EXISTS `ops_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `ops_users`
--

INSERT INTO `ops_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$P$BDjAoHz/99hUpHGagR.A6K7tIl8Co9.', 'admin', 'info@ozproductionservices.com', '', '2018-04-30 06:23:21', '', 1, 'admin'),
(2, 'mamun', '$P$B7etliaWIr9/sIricqtiGgzT4XZx1W.', 'mamun', 'mamunbdaiub@gmail.com', '', '2018-05-03 06:49:14', '1525330155:$P$B/rkV3Pj7kJbfC.k3dk/LEIA0U.1pW.', 0, 'mamun'),
(3, 'rajib', '$P$BwzCNhLPcPhjbhD/58P/tR/mgdGuuy1', 'rajib', 'mamun@gmail.com', '', '2018-05-03 07:14:58', '1525331699:$P$Bp/qQNHxVrj2zLzeoDA2H0M6PwAQn..', 0, 'rajib');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
