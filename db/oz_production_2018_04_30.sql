-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 30, 2018 at 06:15 PM
-- Server version: 5.6.17
-- PHP Version: 5.6.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `oz_production`
--

-- --------------------------------------------------------

--
-- Table structure for table `ec_address`
--

CREATE TABLE IF NOT EXISTS `ec_address` (
  `address_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `first_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `last_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `address_line_1` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `address_line_2` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT '',
  `city` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `state` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `zip` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `country` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `phone` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `company_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`address_id`),
  UNIQUE KEY `address_id` (`address_id`),
  KEY `ec_address_idx1` (`address_id`),
  KEY `ec_address_idx2` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_affiliate_rule`
--

CREATE TABLE IF NOT EXISTS `ec_affiliate_rule` (
  `affiliate_rule_id` int(11) NOT NULL AUTO_INCREMENT,
  `rule_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `rule_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `rule_amount` float(15,3) NOT NULL DEFAULT '0.000',
  `rule_limit` int(11) NOT NULL DEFAULT '0',
  `rule_active` tinyint(1) NOT NULL DEFAULT '1',
  `rule_recurring` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`affiliate_rule_id`),
  UNIQUE KEY `affiliate_rule_id` (`affiliate_rule_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_affiliate_rule_to_affiliate`
--

CREATE TABLE IF NOT EXISTS `ec_affiliate_rule_to_affiliate` (
  `rule_to_account_id` int(11) NOT NULL AUTO_INCREMENT,
  `affiliate_rule_id` int(11) NOT NULL DEFAULT '0',
  `affiliate_id` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`rule_to_account_id`),
  UNIQUE KEY `rule_to_account_id` (`rule_to_account_id`),
  KEY `affiliate_rule_id` (`affiliate_rule_id`),
  KEY `affiliate_id` (`affiliate_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_affiliate_rule_to_product`
--

CREATE TABLE IF NOT EXISTS `ec_affiliate_rule_to_product` (
  `rule_to_product_id` int(11) NOT NULL AUTO_INCREMENT,
  `affiliate_rule_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`rule_to_product_id`),
  UNIQUE KEY `rule_to_product_id` (`rule_to_product_id`),
  KEY `affiliate_rule_id` (`affiliate_rule_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_bundle`
--

CREATE TABLE IF NOT EXISTS `ec_bundle` (
  `bundle_id` int(11) NOT NULL AUTO_INCREMENT,
  `key_product_id` int(11) NOT NULL DEFAULT '0',
  `bundled_product_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`bundle_id`),
  UNIQUE KEY `bundle_id` (`bundle_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_category`
--

CREATE TABLE IF NOT EXISTS `ec_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_demo_item` tinyint(1) NOT NULL DEFAULT '0',
  `category_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_id` int(11) NOT NULL DEFAULT '0',
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `short_description` text COLLATE utf8mb4_unicode_520_ci,
  `image` text COLLATE utf8mb4_unicode_520_ci,
  `featured_category` tinyint(1) NOT NULL DEFAULT '0',
  `priority` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `category_id` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_categoryitem`
--

CREATE TABLE IF NOT EXISTS `ec_categoryitem` (
  `categoryitem_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`categoryitem_id`),
  UNIQUE KEY `categoryitem_id` (`categoryitem_id`),
  KEY `product_id` (`product_id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_code`
--

CREATE TABLE IF NOT EXISTS `ec_code` (
  `code_id` int(11) NOT NULL AUTO_INCREMENT,
  `code_val` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT '',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `orderdetail_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`code_id`),
  UNIQUE KEY `code_id` (`code_id`),
  KEY `product_id` (`product_id`),
  KEY `orderdetail_id` (`orderdetail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_country`
--

CREATE TABLE IF NOT EXISTS `ec_country` (
  `id_cnt` int(11) NOT NULL AUTO_INCREMENT,
  `name_cnt` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `iso2_cnt` varchar(10) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `iso3_cnt` varchar(10) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `sort_order` int(11) NOT NULL,
  `vat_rate_cnt` float(9,3) NOT NULL DEFAULT '0.000',
  `ship_to_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_cnt`),
  KEY `iso2_cnt` (`iso2_cnt`),
  KEY `iso3_cnt` (`iso3_cnt`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=241 ;

--
-- Dumping data for table `ec_country`
--

INSERT INTO `ec_country` (`id_cnt`, `name_cnt`, `iso2_cnt`, `iso3_cnt`, `sort_order`, `vat_rate_cnt`, `ship_to_active`) VALUES
(1, 'Afghanistan', 'AF', 'AFG', 10, 0.000, 1),
(2, 'Albania', 'AL', 'ALB', 11, 0.000, 1),
(3, 'Algeria', 'DZ', 'DZA', 12, 0.000, 1),
(4, 'American Samoa', 'AS', 'ASM', 13, 0.000, 1),
(5, 'Andorra', 'AD', 'AND', 14, 0.000, 1),
(6, 'Angola', 'AO', 'AGO', 15, 0.000, 1),
(7, 'Anguilla', 'AI', 'AIA', 16, 0.000, 1),
(8, 'Antarctica', 'AQ', 'ATA', 17, 0.000, 1),
(9, 'Antigua and Barbuda', 'AG', 'ATG', 18, 0.000, 1),
(10, 'Argentina', 'AR', 'ARG', 19, 0.000, 1),
(11, 'Armenia', 'AM', 'ARM', 20, 0.000, 1),
(12, 'Aruba', 'AW', 'ABW', 21, 0.000, 1),
(13, 'Australia', 'AU', 'AUS', 3, 0.000, 1),
(14, 'Austria', 'AT', 'AUT', 23, 0.000, 1),
(15, 'Azerbaijan', 'AZ', 'AZE', 24, 0.000, 1),
(16, 'Bahamas', 'BS', 'BHS', 25, 0.000, 1),
(17, 'Bahrain', 'BH', 'BHR', 26, 0.000, 1),
(18, 'Bangladesh', 'BD', 'BGD', 27, 0.000, 1),
(19, 'Barbados', 'BB', 'BRB', 28, 0.000, 1),
(20, 'Belarus', 'BY', 'BLR', 29, 0.000, 1),
(21, 'Belgium', 'BE', 'BEL', 30, 0.000, 1),
(22, 'Belize', 'BZ', 'BLZ', 31, 0.000, 1),
(23, 'Benin', 'BJ', 'BEN', 32, 0.000, 1),
(24, 'Bermuda', 'BM', 'BMU', 33, 0.000, 1),
(25, 'Bhutan', 'BT', 'BTN', 34, 0.000, 1),
(26, 'Bolivia', 'BO', 'BOL', 35, 0.000, 1),
(28, 'Botswana', 'BW', 'BWA', 36, 0.000, 1),
(29, 'Bouvet Island', 'BV', 'BVT', 37, 0.000, 1),
(30, 'Brazil', 'BR', 'BRA', 38, 0.000, 1),
(32, 'Brunei Darussalam', 'BN', 'BRN', 39, 0.000, 1),
(33, 'Bulgaria', 'BG', 'BGR', 40, 0.000, 1),
(34, 'Burkina Faso', 'BF', 'BFA', 41, 0.000, 1),
(35, 'Burundi', 'BI', 'BDI', 42, 0.000, 1),
(36, 'Cambodia', 'KH', 'KHM', 43, 0.000, 1),
(37, 'Cameroon', 'CM', 'CMR', 44, 0.000, 1),
(38, 'Canada', 'CA', 'CAN', 2, 0.000, 1),
(39, 'Cape Verde', 'CV', 'CPV', 46, 0.000, 1),
(40, 'Cayman Islands', 'KY', 'CYM', 47, 0.000, 1),
(42, 'Chad', 'TD', 'TCD', 48, 0.000, 1),
(43, 'Chile', 'CL', 'CHL', 49, 0.000, 1),
(44, 'China', 'CN', 'CHN', 50, 0.000, 1),
(45, 'Christmas Island', 'CX', 'CXR', 51, 0.000, 1),
(47, 'Colombia', 'CO', 'COL', 52, 0.000, 1),
(48, 'Comoros', 'KM', 'COM', 53, 0.000, 1),
(49, 'Congo', 'CG', 'COG', 54, 0.000, 1),
(50, 'Cook Islands', 'CK', 'COK', 55, 0.000, 1),
(51, 'Costa Rica', 'CR', 'CRI', 56, 0.000, 1),
(52, 'Cote D''''Ivoire', 'CI', 'CIV', 57, 0.000, 1),
(53, 'Croatia', 'HR', 'HRV', 58, 0.000, 1),
(54, 'Cuba', 'CU', 'CUB', 59, 0.000, 1),
(55, 'Cyprus', 'CY', 'CYP', 60, 0.000, 1),
(56, 'Czech Republic', 'CZ', 'CZE', 61, 0.000, 1),
(57, 'Denmark', 'DK', 'DNK', 62, 0.000, 1),
(58, 'Djibouti', 'DJ', 'DJI', 63, 0.000, 1),
(59, 'Dominica', 'DM', 'DMA', 64, 0.000, 1),
(60, 'Dominican Republic', 'DO', 'DOM', 65, 0.000, 1),
(61, 'East Timor', 'TP', 'TMP', 66, 0.000, 1),
(62, 'Ecuador', 'EC', 'ECU', 67, 0.000, 1),
(63, 'Egypt', 'EG', 'EGY', 68, 0.000, 1),
(64, 'El Salvador', 'SV', 'SLV', 69, 0.000, 1),
(65, 'Equatorial Guinea', 'GQ', 'GNQ', 70, 0.000, 1),
(66, 'Eritrea', 'ER', 'ERI', 71, 0.000, 1),
(67, 'Estonia', 'EE', 'EST', 72, 0.000, 1),
(68, 'Ethiopia', 'ET', 'ETH', 73, 0.000, 1),
(70, 'Faroe Islands', 'FO', 'FRO', 74, 0.000, 1),
(71, 'Fiji', 'FJ', 'FJI', 75, 0.000, 1),
(72, 'Finland', 'FI', 'FIN', 76, 0.000, 1),
(73, 'France', 'FR', 'FRA', 77, 0.000, 1),
(74, 'France, Metropolitan', 'FX', 'FXX', 78, 0.000, 1),
(75, 'French Guiana', 'GF', 'GUF', 79, 0.000, 1),
(76, 'French Polynesia', 'PF', 'PYF', 80, 0.000, 1),
(78, 'Gabon', 'GA', 'GAB', 81, 0.000, 1),
(79, 'Gambia', 'GM', 'GMB', 82, 0.000, 1),
(80, 'Georgia', 'GE', 'GEO', 83, 0.000, 1),
(81, 'Germany', 'DE', 'DEU', 84, 0.000, 1),
(82, 'Ghana', 'GH', 'GHA', 85, 0.000, 1),
(83, 'Gibraltar', 'GI', 'GIB', 86, 0.000, 1),
(84, 'Greece', 'GR', 'GRC', 87, 0.000, 1),
(85, 'Greenland', 'GL', 'GRL', 88, 0.000, 1),
(86, 'Grenada', 'GD', 'GRD', 89, 0.000, 1),
(87, 'Guadeloupe', 'GP', 'GLP', 90, 0.000, 1),
(88, 'Guam', 'GU', 'GUM', 91, 0.000, 1),
(89, 'Guatemala', 'GT', 'GTM', 92, 0.000, 1),
(90, 'Guinea', 'GN', 'GIN', 93, 0.000, 1),
(91, 'Guinea-bissau', 'GW', 'GNB', 94, 0.000, 1),
(92, 'Guyana', 'GY', 'GUY', 95, 0.000, 1),
(93, 'Haiti', 'HT', 'HTI', 96, 0.000, 1),
(95, 'Honduras', 'HN', 'HND', 97, 0.000, 1),
(96, 'Hong Kong', 'HK', 'HKG', 98, 0.000, 1),
(97, 'Hungary', 'HU', 'HUN', 99, 0.000, 1),
(98, 'Iceland', 'IS', 'ISL', 100, 0.000, 1),
(99, 'India', 'IN', 'IND', 101, 0.000, 1),
(100, 'Indonesia', 'ID', 'IDN', 102, 0.000, 1),
(102, 'Iraq', 'IQ', 'IRQ', 103, 0.000, 1),
(103, 'Ireland', 'IE', 'IRL', 104, 0.000, 1),
(104, 'Israel', 'IL', 'ISR', 105, 0.000, 1),
(105, 'Italy', 'IT', 'ITA', 106, 0.000, 1),
(106, 'Jamaica', 'JM', 'JAM', 107, 0.000, 1),
(107, 'Japan', 'JP', 'JPN', 108, 0.000, 1),
(108, 'Jordan', 'JO', 'JOR', 109, 0.000, 1),
(109, 'Kazakhstan', 'KZ', 'KAZ', 110, 0.000, 1),
(110, 'Kenya', 'KE', 'KEN', 111, 0.000, 1),
(111, 'Kiribati', 'KI', 'KIR', 112, 0.000, 1),
(113, 'Korea, Republic of', 'KR', 'KOR', 113, 0.000, 1),
(114, 'Kuwait', 'KW', 'KWT', 114, 0.000, 1),
(115, 'Kyrgyzstan', 'KG', 'KGZ', 115, 0.000, 1),
(117, 'Latvia', 'LV', 'LVA', 116, 0.000, 1),
(118, 'Lebanon', 'LB', 'LBN', 117, 0.000, 1),
(119, 'Lesotho', 'LS', 'LSO', 118, 0.000, 1),
(120, 'Liberia', 'LR', 'LBR', 119, 0.000, 1),
(122, 'Liechtenstein', 'LI', 'LIE', 120, 0.000, 1),
(123, 'Lithuania', 'LT', 'LTU', 121, 0.000, 1),
(124, 'Luxembourg', 'LU', 'LUX', 122, 0.000, 1),
(125, 'Macau', 'MO', 'MAC', 123, 0.000, 1),
(127, 'Madagascar', 'MG', 'MDG', 124, 0.000, 1),
(128, 'Malawi', 'MW', 'MWI', 125, 0.000, 1),
(129, 'Malaysia', 'MY', 'MYS', 126, 0.000, 1),
(130, 'Maldives', 'MV', 'MDV', 127, 0.000, 1),
(131, 'Mali', 'ML', 'MLI', 128, 0.000, 1),
(132, 'Malta', 'MT', 'MLT', 129, 0.000, 1),
(133, 'Marshall Islands', 'MH', 'MHL', 130, 0.000, 1),
(134, 'Martinique', 'MQ', 'MTQ', 131, 0.000, 1),
(135, 'Mauritania', 'MR', 'MRT', 132, 0.000, 1),
(136, 'Mauritius', 'MU', 'MUS', 133, 0.000, 1),
(137, 'Mayotte', 'YT', 'MYT', 134, 0.000, 1),
(138, 'Mexico', 'MX', 'MEX', 135, 0.000, 1),
(141, 'Monaco', 'MC', 'MCO', 136, 0.000, 1),
(142, 'Mongolia', 'MN', 'MNG', 137, 0.000, 1),
(143, 'Montserrat', 'MS', 'MSR', 138, 0.000, 1),
(144, 'Morocco', 'MA', 'MAR', 139, 0.000, 1),
(145, 'Mozambique', 'MZ', 'MOZ', 140, 0.000, 1),
(146, 'Myanmar', 'MM', 'MMR', 141, 0.000, 1),
(147, 'Namibia', 'NA', 'NAM', 142, 0.000, 1),
(148, 'Nauru', 'NR', 'NRU', 143, 0.000, 1),
(149, 'Nepal', 'NP', 'NPL', 144, 0.000, 1),
(150, 'Netherlands', 'NL', 'NLD', 145, 0.000, 1),
(151, 'Netherlands Antilles', 'AN', 'ANT', 146, 0.000, 1),
(152, 'New Caledonia', 'NC', 'NCL', 147, 0.000, 1),
(153, 'New Zealand', 'NZ', 'NZL', 148, 0.000, 1),
(154, 'Nicaragua', 'NI', 'NIC', 149, 0.000, 1),
(155, 'Niger', 'NE', 'NER', 150, 0.000, 1),
(156, 'Nigeria', 'NG', 'NGA', 151, 0.000, 1),
(157, 'Niue', 'NU', 'NIU', 152, 0.000, 1),
(158, 'Norfolk Island', 'NF', 'NFK', 153, 0.000, 1),
(160, 'Norway', 'NO', 'NOR', 154, 0.000, 1),
(161, 'Oman', 'OM', 'OMN', 155, 0.000, 1),
(162, 'Pakistan', 'PK', 'PAK', 156, 0.000, 1),
(163, 'Palau', 'PW', 'PLW', 157, 0.000, 1),
(164, 'Panama', 'PA', 'PAN', 158, 0.000, 1),
(165, 'Papua New Guinea', 'PG', 'PNG', 159, 0.000, 1),
(166, 'Paraguay', 'PY', 'PRY', 160, 0.000, 1),
(167, 'Peru', 'PE', 'PER', 161, 0.000, 1),
(168, 'Philippines', 'PH', 'PHL', 162, 0.000, 1),
(169, 'Pitcairn', 'PN', 'PCN', 163, 0.000, 1),
(170, 'Poland', 'PL', 'POL', 164, 0.000, 1),
(171, 'Portugal', 'PT', 'PRT', 165, 0.000, 1),
(172, 'Puerto Rico', 'PR', 'PRI', 166, 0.000, 1),
(173, 'Qatar', 'QA', 'QAT', 167, 0.000, 1),
(174, 'Reunion', 'RE', 'REU', 168, 0.000, 1),
(175, 'Romania', 'RO', 'ROM', 169, 0.000, 1),
(176, 'Russian Federation', 'RU', 'RUS', 170, 0.000, 1),
(177, 'Rwanda', 'RW', 'RWA', 171, 0.000, 1),
(178, 'Saint Kitts and Nevis', 'KN', 'KNA', 172, 0.000, 1),
(179, 'Saint Lucia', 'LC', 'LCA', 173, 0.000, 1),
(181, 'Samoa', 'WS', 'WSM', 174, 0.000, 1),
(182, 'San Marino', 'SM', 'SMR', 175, 0.000, 1),
(183, 'Sao Tome and Principe', 'ST', 'STP', 176, 0.000, 1),
(184, 'Saudi Arabia', 'SA', 'SAU', 177, 0.000, 1),
(185, 'Senegal', 'SN', 'SEN', 178, 0.000, 1),
(186, 'Seychelles', 'SC', 'SYC', 179, 0.000, 1),
(187, 'Sierra Leone', 'SL', 'SLE', 180, 0.000, 1),
(188, 'Singapore', 'SG', 'SGP', 181, 0.000, 1),
(190, 'Slovenia', 'SI', 'SVN', 182, 0.000, 1),
(191, 'Solomon Islands', 'SB', 'SLB', 183, 0.000, 1),
(192, 'Somalia', 'SO', 'SOM', 184, 0.000, 1),
(193, 'South Africa', 'ZA', 'ZAF', 185, 0.000, 1),
(195, 'Spain', 'ES', 'ESP', 186, 0.000, 1),
(196, 'Sri Lanka', 'LK', 'LKA', 187, 0.000, 1),
(197, 'St. Helena', 'SH', 'SHN', 188, 0.000, 1),
(198, 'St. Pierre and Miquelon', 'PM', 'SPM', 189, 0.000, 1),
(199, 'Sudan', 'SD', 'SDN', 190, 0.000, 1),
(200, 'Suriname', 'SR', 'SUR', 191, 0.000, 1),
(202, 'Swaziland', 'SZ', 'SWZ', 192, 0.000, 1),
(203, 'Sweden', 'SE', 'SWE', 193, 0.000, 1),
(204, 'Switzerland', 'CH', 'CHE', 194, 0.000, 1),
(205, 'Syrian Arab Republic', 'SY', 'SYR', 195, 0.000, 1),
(206, 'Taiwan', 'TW', 'TWN', 196, 0.000, 1),
(207, 'Tajikistan', 'TJ', 'TJK', 197, 0.000, 1),
(209, 'Thailand', 'TH', 'THA', 198, 0.000, 1),
(210, 'Togo', 'TG', 'TGO', 199, 0.000, 1),
(211, 'Tokelau', 'TK', 'TKL', 200, 0.000, 1),
(212, 'Tonga', 'TO', 'TON', 201, 0.000, 1),
(213, 'Trinidad and Tobago', 'TT', 'TTO', 202, 0.000, 1),
(214, 'Tunisia', 'TN', 'TUN', 203, 0.000, 1),
(215, 'Turkey', 'TR', 'TUR', 204, 0.000, 1),
(216, 'Turkmenistan', 'TM', 'TKM', 205, 0.000, 1),
(217, 'Turks and Caicos Islands', 'TC', 'TCA', 206, 0.000, 1),
(218, 'Tuvalu', 'TV', 'TUV', 207, 0.000, 1),
(219, 'Uganda', 'UG', 'UGA', 208, 0.000, 1),
(220, 'Ukraine', 'UA', 'UKR', 209, 0.000, 1),
(221, 'United Arab Emirates', 'AE', 'ARE', 210, 0.000, 1),
(222, 'United Kingdom', 'GB', 'GBR', 211, 0.000, 1),
(223, 'United States', 'US', 'USA', 1, 0.000, 1),
(224, 'US Minor Outlying Islands', 'UM', 'UMI', 213, 0.000, 1),
(225, 'Uruguay', 'UY', 'URY', 214, 0.000, 1),
(226, 'Uzbekistan', 'UZ', 'UZB', 215, 0.000, 1),
(227, 'Vanuatu', 'VU', 'VUT', 216, 0.000, 1),
(229, 'Venezuela', 'VE', 'VEN', 217, 0.000, 1),
(230, 'Viet Nam', 'VN', 'VNM', 218, 0.000, 1),
(231, 'Virgin Islands (British)', 'VG', 'VGB', 219, 0.000, 1),
(232, 'Virgin Islands (U.S.)', 'VI', 'VIR', 220, 0.000, 1),
(233, 'Wallis and Futuna Islands', 'WF', 'WLF', 221, 0.000, 1),
(234, 'Western Sahara', 'EH', 'ESH', 222, 0.000, 1),
(235, 'Yemen', 'YE', 'YEM', 223, 0.000, 1),
(236, 'Yugoslavia', 'YU', 'YUG', 224, 0.000, 1),
(237, 'Zaire', 'ZR', 'ZAR', 225, 0.000, 1),
(238, 'Zambia', 'ZM', 'ZMB', 226, 0.000, 1),
(239, 'Zimbabwe', 'ZW', 'ZWE', 227, 0.000, 1),
(240, 'Slovakia', 'SK', 'SVK', 182, 0.000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ec_customfield`
--

CREATE TABLE IF NOT EXISTS `ec_customfield` (
  `customfield_id` int(11) NOT NULL AUTO_INCREMENT,
  `table_name` varchar(30) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `field_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `field_label` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`customfield_id`),
  UNIQUE KEY `customfield_id` (`customfield_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_customfielddata`
--

CREATE TABLE IF NOT EXISTS `ec_customfielddata` (
  `customfielddata_id` int(11) NOT NULL AUTO_INCREMENT,
  `customfield_id` int(11) DEFAULT NULL,
  `table_id` int(11) NOT NULL,
  `data` blob NOT NULL,
  PRIMARY KEY (`customfielddata_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_download`
--

CREATE TABLE IF NOT EXISTS `ec_download` (
  `download_id` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `download_count` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `download_file_name` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `is_amazon_download` tinyint(1) NOT NULL DEFAULT '0',
  `amazon_key` varchar(1024) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`download_id`),
  KEY `download_order_id` (`order_id`),
  KEY `download_product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ec_giftcard`
--

CREATE TABLE IF NOT EXISTS `ec_giftcard` (
  `giftcard_id` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `amount` float(15,3) NOT NULL DEFAULT '0.000',
  `message` text COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`giftcard_id`),
  UNIQUE KEY `giftcard_id` (`giftcard_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ec_live_rate_cache`
--

CREATE TABLE IF NOT EXISTS `ec_live_rate_cache` (
  `live_rate_cache_id` int(11) NOT NULL AUTO_INCREMENT,
  `ec_cart_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `rate_data` text COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`live_rate_cache_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_manufacturer`
--

CREATE TABLE IF NOT EXISTS `ec_manufacturer` (
  `manufacturer_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_demo_item` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `clicks` int(11) NOT NULL DEFAULT '0',
  `post_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`manufacturer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ec_manufacturer`
--

INSERT INTO `ec_manufacturer` (`manufacturer_id`, `is_demo_item`, `name`, `clicks`, `post_id`) VALUES
(1, 0, 'Oz Production Event Services, LLC', 0, 4);

-- --------------------------------------------------------

--
-- Table structure for table `ec_menulevel1`
--

CREATE TABLE IF NOT EXISTS `ec_menulevel1` (
  `menulevel1_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_demo_item` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `clicks` int(11) NOT NULL DEFAULT '0',
  `seo_keywords` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `seo_description` blob,
  `banner_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`menulevel1_id`),
  UNIQUE KEY `menu1_menulevel1_id` (`menulevel1_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_menulevel2`
--

CREATE TABLE IF NOT EXISTS `ec_menulevel2` (
  `menulevel2_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_demo_item` tinyint(1) NOT NULL DEFAULT '0',
  `menulevel1_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `clicks` int(11) NOT NULL DEFAULT '0',
  `seo_keywords` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `seo_description` blob,
  `banner_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`menulevel2_id`),
  UNIQUE KEY `menu2_menulevel2_id` (`menulevel2_id`),
  KEY `menu2_menulevel1_id` (`menulevel1_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_menulevel3`
--

CREATE TABLE IF NOT EXISTS `ec_menulevel3` (
  `menulevel3_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_demo_item` tinyint(1) NOT NULL DEFAULT '0',
  `menulevel2_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `clicks` int(11) NOT NULL DEFAULT '0',
  `seo_keywords` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `seo_description` blob,
  `banner_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`menulevel3_id`),
  UNIQUE KEY `menu3_menulevel3_id` (`menulevel3_id`),
  KEY `menu3_menulevel2_id` (`menulevel2_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_option`
--

CREATE TABLE IF NOT EXISTS `ec_option` (
  `option_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_demo_item` tinyint(1) NOT NULL DEFAULT '0',
  `option_name` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_label` text COLLATE utf8mb4_unicode_520_ci,
  `option_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'combo',
  `option_required` tinyint(1) NOT NULL DEFAULT '1',
  `option_error_text` text COLLATE utf8mb4_unicode_520_ci,
  `option_meta` text COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_option_id` (`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_optionitem`
--

CREATE TABLE IF NOT EXISTS `ec_optionitem` (
  `optionitem_id` int(11) NOT NULL AUTO_INCREMENT,
  `option_id` int(11) NOT NULL DEFAULT '0',
  `optionitem_name` text COLLATE utf8mb4_unicode_520_ci,
  `optionitem_price` float(15,3) NOT NULL DEFAULT '0.000',
  `optionitem_price_onetime` float(15,3) NOT NULL DEFAULT '0.000',
  `optionitem_price_override` float(15,3) NOT NULL DEFAULT '-1.000',
  `optionitem_price_multiplier` float(15,3) NOT NULL DEFAULT '0.000',
  `optionitem_price_per_character` float(15,3) NOT NULL DEFAULT '0.000',
  `optionitem_weight` float(15,3) NOT NULL DEFAULT '0.000',
  `optionitem_weight_onetime` float(15,3) NOT NULL DEFAULT '0.000',
  `optionitem_weight_override` float(15,3) NOT NULL DEFAULT '-1.000',
  `optionitem_weight_multiplier` float(15,3) NOT NULL DEFAULT '0.000',
  `optionitem_order` int(11) NOT NULL DEFAULT '1',
  `optionitem_icon` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_initial_value` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_model_number` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_allow_download` tinyint(1) NOT NULL DEFAULT '1',
  `optionitem_disallow_shipping` tinyint(1) NOT NULL DEFAULT '0',
  `optionitem_initially_selected` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`optionitem_id`),
  KEY `option_id` (`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_optionitemimage`
--

CREATE TABLE IF NOT EXISTS `ec_optionitemimage` (
  `optionitemimage_id` int(11) NOT NULL AUTO_INCREMENT,
  `optionitem_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `image1` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `image2` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `image3` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `image4` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `image5` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`optionitemimage_id`),
  KEY `optionitem_id` (`optionitem_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_optionitemquantity`
--

CREATE TABLE IF NOT EXISTS `ec_optionitemquantity` (
  `optionitemquantity_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(17) NOT NULL DEFAULT '0',
  `optionitem_id_1` int(11) NOT NULL DEFAULT '0',
  `optionitem_id_2` int(11) NOT NULL DEFAULT '0',
  `optionitem_id_3` int(11) NOT NULL DEFAULT '0',
  `optionitem_id_4` int(11) NOT NULL DEFAULT '0',
  `optionitem_id_5` int(11) NOT NULL DEFAULT '0',
  `quantity` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`optionitemquantity_id`),
  UNIQUE KEY `optionitemquantity_id` (`optionitemquantity_id`),
  KEY `product_id` (`product_id`),
  KEY `optionitem_id_1` (`optionitem_id_1`),
  KEY `optionitem_id_2` (`optionitem_id_2`),
  KEY `optionitem_id_3` (`optionitem_id_3`),
  KEY `optionitem_id_4` (`optionitem_id_4`),
  KEY `optionitem_id_5` (`optionitem_id_5`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_option_to_product`
--

CREATE TABLE IF NOT EXISTS `ec_option_to_product` (
  `option_to_product_id` int(11) NOT NULL AUTO_INCREMENT,
  `option_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `role_label` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'all',
  `option_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`option_to_product_id`),
  UNIQUE KEY `option_to_product_id` (`option_to_product_id`),
  KEY `option_id` (`option_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_order`
--

CREATE TABLE IF NOT EXISTS `ec_order` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_demo_item` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `user_email` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_level` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'shopper',
  `last_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `order_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `orderstatus_id` int(11) NOT NULL DEFAULT '5',
  `order_weight` float(15,3) NOT NULL DEFAULT '0.000',
  `sub_total` float(15,3) NOT NULL DEFAULT '0.000',
  `tax_total` float(15,3) NOT NULL DEFAULT '0.000',
  `shipping_total` float(15,3) NOT NULL DEFAULT '0.000',
  `discount_total` float(15,3) NOT NULL DEFAULT '0.000',
  `vat_total` float(15,3) NOT NULL DEFAULT '0.000',
  `vat_rate` float(15,3) NOT NULL DEFAULT '0.000',
  `duty_total` float(15,3) NOT NULL DEFAULT '0.000',
  `gst_total` float(15,3) NOT NULL DEFAULT '0.000',
  `gst_rate` float(15,3) NOT NULL DEFAULT '0.000',
  `pst_total` float(15,3) NOT NULL DEFAULT '0.000',
  `pst_rate` float(15,3) NOT NULL DEFAULT '0.000',
  `hst_total` float(15,3) NOT NULL DEFAULT '0.000',
  `hst_rate` float(15,3) NOT NULL DEFAULT '0.000',
  `grand_total` float(15,3) NOT NULL DEFAULT '0.000',
  `refund_total` float(15,3) NOT NULL DEFAULT '0.000',
  `promo_code` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `giftcard_id` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `use_expedited_shipping` tinyint(1) NOT NULL DEFAULT '0',
  `shipping_method` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_carrier` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_service_code` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tracking_number` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_first_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_last_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_address_line_1` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_address_line_2` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_city` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_state` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_country` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_zip` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_phone` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_first_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_last_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_address_line_1` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_address_line_2` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_city` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_state` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_country` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_zip` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_phone` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `vat_registration_number` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `payment_method` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `paypal_email_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `paypal_transaction_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `paypal_payer_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `order_viewed` tinyint(1) NOT NULL DEFAULT '0',
  `order_notes` text COLLATE utf8mb4_unicode_520_ci,
  `order_customer_notes` blob,
  `txn_id` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `payment_txn_id` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `edit_sequence` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `quickbooks_status` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Not Queued',
  `credit_memo_txn_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `card_holder_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `creditcard_digits` varchar(4) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `cc_exp_month` varchar(2) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `cc_exp_year` varchar(4) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fraktjakt_order_id` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fraktjakt_shipment_id` varchar(20) COLLATE utf8mb4_unicode_520_ci DEFAULT '',
  `stripe_charge_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `nets_transaction_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `subscription_id` int(11) NOT NULL DEFAULT '0',
  `order_gateway` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `affirm_charge_id` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_company_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_company_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `guest_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `agreed_to_terms` tinyint(1) NOT NULL DEFAULT '0',
  `order_ip_address` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `gateway_transaction_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`order_id`),
  UNIQUE KEY `order_id` (`order_id`),
  KEY `user_id` (`user_id`),
  KEY `giftcard_id` (`giftcard_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_orderdetail`
--

CREATE TABLE IF NOT EXISTS `ec_orderdetail` (
  `orderdetail_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `model_number` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `order_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `unit_price` float(15,3) NOT NULL DEFAULT '0.000',
  `total_price` float(15,3) NOT NULL DEFAULT '0.000',
  `quantity` int(11) NOT NULL DEFAULT '0',
  `image1` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `optionitem_id_1` int(11) NOT NULL DEFAULT '0',
  `optionitem_id_2` int(11) NOT NULL DEFAULT '0',
  `optionitem_id_3` int(11) NOT NULL DEFAULT '0',
  `optionitem_id_4` int(11) NOT NULL DEFAULT '0',
  `optionitem_id_5` int(11) NOT NULL DEFAULT '0',
  `optionitem_name_1` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_name_2` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_name_3` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_name_4` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_name_5` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_label_1` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_label_2` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_label_3` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_label_4` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_label_5` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_price_1` float(15,3) NOT NULL DEFAULT '0.000',
  `optionitem_price_2` float(15,3) NOT NULL DEFAULT '0.000',
  `optionitem_price_3` float(15,3) NOT NULL DEFAULT '0.000',
  `optionitem_price_4` float(15,3) NOT NULL DEFAULT '0.000',
  `optionitem_price_5` float(15,3) NOT NULL DEFAULT '0.000',
  `use_advanced_optionset` tinyint(1) NOT NULL DEFAULT '0',
  `giftcard_id` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipper_id` int(11) DEFAULT '0',
  `shipper_first_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipper_last_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `gift_card_message` text COLLATE utf8mb4_unicode_520_ci,
  `gift_card_from_name` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `gift_card_to_name` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `is_download` tinyint(1) NOT NULL DEFAULT '0',
  `is_giftcard` tinyint(1) NOT NULL DEFAULT '0',
  `is_taxable` tinyint(1) NOT NULL DEFAULT '1',
  `is_shippable` tinyint(1) NOT NULL DEFAULT '1',
  `download_file_name` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `download_key` text COLLATE utf8mb4_unicode_520_ci,
  `maximum_downloads_allowed` int(11) NOT NULL DEFAULT '0',
  `download_timelimit_seconds` int(11) DEFAULT '0',
  `is_amazon_download` tinyint(1) NOT NULL DEFAULT '0',
  `amazon_key` varchar(1024) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `is_deconetwork` tinyint(1) NOT NULL DEFAULT '0',
  `deconetwork_id` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_product_code` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_options` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_color_code` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_product_id` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_image_link` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `gift_card_email` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `include_code` tinyint(1) NOT NULL DEFAULT '0',
  `subscription_signup_fee` float(15,3) NOT NULL DEFAULT '0.000',
  `stock_adjusted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`orderdetail_id`),
  UNIQUE KEY `orderdetail_id` (`orderdetail_id`),
  KEY `orderdetail_order_id` (`order_id`),
  KEY `orderdetail_product_id` (`product_id`),
  KEY `orderdetail_giftcard_id` (`giftcard_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_orderstatus`
--

CREATE TABLE IF NOT EXISTS `ec_orderstatus` (
  `status_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_status` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `is_approved` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`status_id`),
  UNIQUE KEY `orderstatus_status_id` (`status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=20 ;

--
-- Dumping data for table `ec_orderstatus`
--

INSERT INTO `ec_orderstatus` (`status_id`, `order_status`, `is_approved`) VALUES
(1, 'Status Not Found', 0),
(2, 'Order Shipped', 1),
(3, 'Order Confirmed', 1),
(4, 'Order on Hold', 0),
(5, 'Order Started', 0),
(6, 'Card Approved', 1),
(7, 'Card Denied', 0),
(8, 'Third Party Pending', 0),
(9, 'Third Party Error', 0),
(10, 'Third Party Approved', 1),
(11, 'Ready for Pickup', 1),
(12, 'Pending Approval', 0),
(14, 'Direct Deposit Pending', 0),
(15, 'Direct Deposit Received', 1),
(16, 'Refunded Order', 0),
(17, 'Partial Refund', 1),
(18, 'Order Picked Up', 1),
(19, 'Order Cancelled', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ec_order_option`
--

CREATE TABLE IF NOT EXISTS `ec_order_option` (
  `order_option_id` int(11) NOT NULL AUTO_INCREMENT,
  `orderdetail_id` int(11) NOT NULL DEFAULT '0',
  `option_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_name` text COLLATE utf8mb4_unicode_520_ci,
  `option_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'combo',
  `option_value` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `option_price_change` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_allow_download` tinyint(1) NOT NULL DEFAULT '1',
  `option_label` text COLLATE utf8mb4_unicode_520_ci,
  `option_to_product_id` int(11) NOT NULL DEFAULT '0',
  `option_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`order_option_id`),
  UNIQUE KEY `order_option_id` (`order_option_id`),
  KEY `orderdetail_id` (`orderdetail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_pageoption`
--

CREATE TABLE IF NOT EXISTS `ec_pageoption` (
  `pageoption_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL DEFAULT '0',
  `option_type` varchar(155) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`pageoption_id`),
  UNIQUE KEY `pageoption_id` (`pageoption_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_perpage`
--

CREATE TABLE IF NOT EXISTS `ec_perpage` (
  `perpage_id` int(11) NOT NULL AUTO_INCREMENT,
  `perpage` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`perpage_id`),
  UNIQUE KEY `perpageid` (`perpage_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `ec_perpage`
--

INSERT INTO `ec_perpage` (`perpage_id`, `perpage`) VALUES
(1, 50),
(2, 25),
(3, 10);

-- --------------------------------------------------------

--
-- Table structure for table `ec_pricepoint`
--

CREATE TABLE IF NOT EXISTS `ec_pricepoint` (
  `pricepoint_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_less_than` tinyint(1) NOT NULL DEFAULT '0',
  `is_greater_than` tinyint(1) NOT NULL DEFAULT '0',
  `low_point` float(15,3) NOT NULL DEFAULT '0.000',
  `high_point` float(15,3) DEFAULT '0.000',
  `pricepoint_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pricepoint_id`),
  UNIQUE KEY `pricepoint_pricepoint_id` (`pricepoint_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `ec_pricepoint`
--

INSERT INTO `ec_pricepoint` (`pricepoint_id`, `is_less_than`, `is_greater_than`, `low_point`, `high_point`, `pricepoint_order`) VALUES
(1, 1, 0, 0.000, 10.000, 0),
(2, 0, 0, 25.000, 49.990, 4),
(3, 0, 0, 50.000, 99.990, 5),
(4, 0, 0, 100.000, 299.990, 6),
(5, 0, 2, 299.990, 0.000, 7),
(6, 0, 0, 10.000, 14.990, 1),
(7, 0, 0, 15.000, 19.990, 2),
(8, 0, 0, 20.000, 24.990, 3);

-- --------------------------------------------------------

--
-- Table structure for table `ec_pricetier`
--

CREATE TABLE IF NOT EXISTS `ec_pricetier` (
  `pricetier_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `price` float(15,3) NOT NULL DEFAULT '0.000',
  `quantity` int(11) NOT NULL DEFAULT '10',
  PRIMARY KEY (`pricetier_id`),
  UNIQUE KEY `pricetier_id` (`pricetier_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_product`
--

CREATE TABLE IF NOT EXISTS `ec_product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_demo_item` tinyint(1) NOT NULL DEFAULT '0',
  `model_number` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_id` int(11) NOT NULL DEFAULT '0',
  `activate_in_store` tinyint(1) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8mb4_unicode_520_ci,
  `specifications` text COLLATE utf8mb4_unicode_520_ci,
  `order_completed_note` text COLLATE utf8mb4_unicode_520_ci,
  `order_completed_email_note` text COLLATE utf8mb4_unicode_520_ci,
  `order_completed_details_note` text COLLATE utf8mb4_unicode_520_ci,
  `price` float(15,3) NOT NULL DEFAULT '0.000',
  `list_price` float(15,3) NOT NULL DEFAULT '0.000',
  `product_cost` float(15,3) NOT NULL DEFAULT '0.000',
  `vat_rate` float(15,3) NOT NULL DEFAULT '0.000',
  `handling_price` float(15,3) NOT NULL DEFAULT '0.000',
  `handling_price_each` float(15,3) NOT NULL DEFAULT '0.000',
  `stock_quantity` int(7) NOT NULL DEFAULT '0',
  `min_purchase_quantity` int(11) NOT NULL DEFAULT '0',
  `max_purchase_quantity` int(11) NOT NULL DEFAULT '0',
  `weight` float(15,3) NOT NULL DEFAULT '0.000',
  `width` double(15,3) NOT NULL DEFAULT '1.000',
  `height` double(15,3) NOT NULL DEFAULT '1.000',
  `length` double(15,3) NOT NULL DEFAULT '1.000',
  `seo_description` text COLLATE utf8mb4_unicode_520_ci,
  `seo_keywords` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `use_specifications` tinyint(1) NOT NULL DEFAULT '0',
  `use_customer_reviews` tinyint(1) NOT NULL DEFAULT '0',
  `manufacturer_id` int(11) NOT NULL DEFAULT '0',
  `download_file_name` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `image1` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `image2` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `image3` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `image4` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `image5` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_id_1` int(11) NOT NULL DEFAULT '0',
  `option_id_2` int(11) NOT NULL DEFAULT '0',
  `option_id_3` int(11) NOT NULL DEFAULT '0',
  `option_id_4` int(11) NOT NULL DEFAULT '0',
  `option_id_5` int(11) NOT NULL DEFAULT '0',
  `use_advanced_optionset` tinyint(1) NOT NULL DEFAULT '0',
  `menulevel1_id_1` int(11) NOT NULL DEFAULT '0',
  `menulevel1_id_2` int(11) NOT NULL DEFAULT '0',
  `menulevel1_id_3` int(11) NOT NULL DEFAULT '0',
  `menulevel2_id_1` int(11) NOT NULL DEFAULT '0',
  `menulevel2_id_2` int(11) NOT NULL DEFAULT '0',
  `menulevel2_id_3` int(11) NOT NULL DEFAULT '0',
  `menulevel3_id_1` int(11) NOT NULL DEFAULT '0',
  `menulevel3_id_2` int(11) NOT NULL DEFAULT '0',
  `menulevel3_id_3` int(11) NOT NULL DEFAULT '0',
  `featured_product_id_1` int(11) NOT NULL DEFAULT '0',
  `featured_product_id_2` int(11) NOT NULL DEFAULT '0',
  `featured_product_id_3` int(11) NOT NULL DEFAULT '0',
  `featured_product_id_4` int(11) NOT NULL DEFAULT '0',
  `is_giftcard` tinyint(1) NOT NULL DEFAULT '0',
  `is_download` tinyint(1) NOT NULL DEFAULT '0',
  `is_donation` tinyint(1) NOT NULL DEFAULT '0',
  `is_special` tinyint(1) NOT NULL DEFAULT '0',
  `is_taxable` tinyint(1) NOT NULL DEFAULT '1',
  `is_shippable` tinyint(1) NOT NULL DEFAULT '1',
  `is_subscription_item` tinyint(1) NOT NULL DEFAULT '0',
  `is_preorder` tinyint(1) NOT NULL DEFAULT '0',
  `role_id` tinyint(1) NOT NULL DEFAULT '0',
  `added_to_db_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `show_on_startup` tinyint(1) NOT NULL DEFAULT '0',
  `use_optionitem_images` tinyint(1) NOT NULL DEFAULT '0',
  `use_optionitem_quantity_tracking` tinyint(1) NOT NULL DEFAULT '0',
  `views` int(11) NOT NULL DEFAULT '0',
  `last_viewed` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `show_stock_quantity` tinyint(1) NOT NULL DEFAULT '1',
  `maximum_downloads_allowed` int(11) NOT NULL DEFAULT '0',
  `download_timelimit_seconds` int(11) NOT NULL DEFAULT '0',
  `list_id` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `edit_sequence` varchar(55) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `quickbooks_status` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Not Queued',
  `income_account_ref` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Online Sales',
  `cogs_account_ref` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Cost of Goods Sold',
  `asset_account_ref` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Inventory Asset',
  `quickbooks_parent_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `quickbooks_parent_list_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `subscription_bill_length` int(11) NOT NULL DEFAULT '1',
  `subscription_bill_period` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'M',
  `subscription_bill_duration` int(11) NOT NULL DEFAULT '0',
  `trial_period_days` int(11) NOT NULL DEFAULT '0',
  `stripe_plan_added` tinyint(1) NOT NULL DEFAULT '0',
  `subscription_plan_id` int(11) NOT NULL DEFAULT '0',
  `allow_multiple_subscription_purchases` tinyint(1) NOT NULL DEFAULT '1',
  `membership_page` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `is_amazon_download` tinyint(1) NOT NULL DEFAULT '0',
  `amazon_key` varchar(1024) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `catalog_mode` tinyint(1) NOT NULL DEFAULT '0',
  `catalog_mode_phrase` varchar(1024) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `inquiry_mode` tinyint(1) NOT NULL DEFAULT '0',
  `inquiry_url` varchar(1024) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `is_deconetwork` tinyint(1) NOT NULL DEFAULT '0',
  `deconetwork_mode` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'designer',
  `deconetwork_product_id` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_size_id` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_color_id` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_design_id` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `short_description` text COLLATE utf8mb4_unicode_520_ci,
  `display_type` int(11) NOT NULL DEFAULT '1',
  `image_hover_type` int(11) NOT NULL DEFAULT '3',
  `tag_type` int(11) NOT NULL DEFAULT '0',
  `tag_bg_color` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tag_text_color` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tag_text` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `image_effect_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'none',
  `include_code` tinyint(1) NOT NULL DEFAULT '0',
  `TIC` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '00000',
  `subscription_signup_fee` float(15,3) NOT NULL DEFAULT '0.000',
  `subscription_unique_id` int(11) NOT NULL DEFAULT '0',
  `subscription_prorate` tinyint(1) NOT NULL DEFAULT '1',
  `allow_backorders` tinyint(1) NOT NULL DEFAULT '0',
  `backorder_fill_date` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_class_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_id`),
  UNIQUE KEY `product_product_id` (`product_id`),
  UNIQUE KEY `product_model_number` (`model_number`(191)),
  KEY `product_menulevel1_id_1` (`menulevel1_id_1`,`menulevel2_id_1`,`menulevel3_id_1`),
  KEY `product_menulevel1_id_2` (`menulevel1_id_2`,`menulevel2_id_2`,`menulevel3_id_2`),
  KEY `product_menulevel1_id_3` (`menulevel1_id_3`,`menulevel2_id_3`,`menulevel3_id_3`),
  KEY `product_manufacturer_id` (`manufacturer_id`),
  KEY `product_option_id_1` (`option_id_1`),
  KEY `product_option_id_2` (`option_id_2`),
  KEY `product_option_id_3` (`option_id_3`),
  KEY `product_option_id_4` (`option_id_4`),
  KEY `product_option_id_5` (`option_id_5`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `ec_product`
--

INSERT INTO `ec_product` (`product_id`, `is_demo_item`, `model_number`, `post_id`, `activate_in_store`, `title`, `description`, `specifications`, `order_completed_note`, `order_completed_email_note`, `order_completed_details_note`, `price`, `list_price`, `product_cost`, `vat_rate`, `handling_price`, `handling_price_each`, `stock_quantity`, `min_purchase_quantity`, `max_purchase_quantity`, `weight`, `width`, `height`, `length`, `seo_description`, `seo_keywords`, `use_specifications`, `use_customer_reviews`, `manufacturer_id`, `download_file_name`, `image1`, `image2`, `image3`, `image4`, `image5`, `option_id_1`, `option_id_2`, `option_id_3`, `option_id_4`, `option_id_5`, `use_advanced_optionset`, `menulevel1_id_1`, `menulevel1_id_2`, `menulevel1_id_3`, `menulevel2_id_1`, `menulevel2_id_2`, `menulevel2_id_3`, `menulevel3_id_1`, `menulevel3_id_2`, `menulevel3_id_3`, `featured_product_id_1`, `featured_product_id_2`, `featured_product_id_3`, `featured_product_id_4`, `is_giftcard`, `is_download`, `is_donation`, `is_special`, `is_taxable`, `is_shippable`, `is_subscription_item`, `is_preorder`, `role_id`, `added_to_db_date`, `show_on_startup`, `use_optionitem_images`, `use_optionitem_quantity_tracking`, `views`, `last_viewed`, `show_stock_quantity`, `maximum_downloads_allowed`, `download_timelimit_seconds`, `list_id`, `edit_sequence`, `quickbooks_status`, `income_account_ref`, `cogs_account_ref`, `asset_account_ref`, `quickbooks_parent_name`, `quickbooks_parent_list_id`, `subscription_bill_length`, `subscription_bill_period`, `subscription_bill_duration`, `trial_period_days`, `stripe_plan_added`, `subscription_plan_id`, `allow_multiple_subscription_purchases`, `membership_page`, `is_amazon_download`, `amazon_key`, `catalog_mode`, `catalog_mode_phrase`, `inquiry_mode`, `inquiry_url`, `is_deconetwork`, `deconetwork_mode`, `deconetwork_product_id`, `deconetwork_size_id`, `deconetwork_color_id`, `deconetwork_design_id`, `short_description`, `display_type`, `image_hover_type`, `tag_type`, `tag_bg_color`, `tag_text_color`, `tag_text`, `image_effect_type`, `include_code`, `TIC`, `subscription_signup_fee`, `subscription_unique_id`, `subscription_prorate`, `allow_backorders`, `backorder_fill_date`, `shipping_class_id`) VALUES
(1, 0, 'Chair', 9, 1, 'Chair', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis mattis fringilla lectus et blandit. Integer sit amet nulla sed mi porttitor tincidunt a non ligula. Quisque blandit dui odio, sed consectetur erat malesuada ac. Curabitur placerat orci magna, id malesuada massa iaculis id. Sed a felis id enim euismod tristique varius in libero. Sed tempus tristique felis ac vehicula. Vestibulum viverra, velit non aliquet rhoncus, odio dolor placerat dui, vel porttitor sem arcu ac massa. Mauris elementum, lacus sollicitudin dictum faucibus, magna enim aliquam libero, at pharetra justo quam eget arcu. Donec ut porttitor metus, luctus egestas dolor. Cras lacinia condimentum nunc fermentum vulputate. Donec feugiat auctor purus. Suspendisse lobortis diam eget pellentesque cursus. Proin non dictum lorem, porttitor tempor nisi.</p><p>Integer auctor lobortis nibh, a imperdiet dolor pulvinar a. Donec tempor aliquet mauris, vitae feugiat sapien dapibus sit amet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget libero sed arcu efficitur iaculis in ut quam. Fusce nisl tellus, ultrices vitae ex id, interdum porttitor diam. Aliquam et velit tellus. Vivamus varius nec lacus id interdum. Mauris sollicitudin orci eget lectus porta tincidunt. Phasellus suscipit, justo et ultricies tristique, eros libero maximus lacus, in pulvinar lectus arcu eu eros. Mauris nec convallis ligula, dictum dignissim lacus.</p>', NULL, NULL, NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0.000, 1.000, 1.000, 1.000, NULL, '', 0, 0, 1, '', 'http://localhost/oz_production/public_html/wp-content/uploads/2018/04/chair.png', '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, '2018-04-30 14:23:26', 1, 0, 0, 10, '0000-00-00 00:00:00', 0, 0, 0, '', '', 'Not Queued', 'Online Sales', 'Cost of Goods Sold', 'Inventory Asset', '', '', 1, 'M', 0, 0, 0, 0, 1, '', 0, '', 0, NULL, 0, NULL, 0, 'designer', '', '', '', '', NULL, 1, 3, 0, '', '', '', 'none', 0, '00000', 0.000, 0, 1, 0, '', 0),
(2, 0, 'table', 28, 1, 'Table', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis mattis fringilla lectus et blandit. Integer sit amet nulla sed mi porttitor tincidunt a non ligula. Quisque blandit dui odio, sed consectetur erat malesuada ac. Curabitur placerat orci magna, id malesuada massa iaculis id. Sed a felis id enim euismod tristique varius in libero. Sed tempus tristique felis ac vehicula. Vestibulum viverra, velit non aliquet rhoncus, odio dolor placerat dui, vel porttitor sem arcu ac massa. Mauris elementum, lacus sollicitudin dictum faucibus, magna enim aliquam libero, at pharetra justo quam eget arcu. Donec ut porttitor metus, luctus egestas dolor. Cras lacinia condimentum nunc fermentum vulputate. Donec feugiat auctor purus. Suspendisse lobortis diam eget pellentesque cursus. Proin non dictum lorem, porttitor tempor nisi.</p><p>Integer auctor lobortis nibh, a imperdiet dolor pulvinar a. Donec tempor aliquet mauris, vitae feugiat sapien dapibus sit amet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget libero sed arcu efficitur iaculis in ut quam. Fusce nisl tellus, ultrices vitae ex id, interdum porttitor diam. Aliquam et velit tellus. Vivamus varius nec lacus id interdum. Mauris sollicitudin orci eget lectus porta tincidunt. Phasellus suscipit, justo et ultricies tristique, eros libero maximus lacus, in pulvinar lectus arcu eu eros. Mauris nec convallis ligula, dictum dignissim lacus.</p>', NULL, NULL, NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0.000, 1.000, 1.000, 1.000, NULL, '', 0, 0, 1, '', 'http://localhost/oz_production/public_html/wp-content/uploads/2018/04/table.png', '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, '2018-04-30 15:09:54', 1, 0, 0, 9, '0000-00-00 00:00:00', 0, 0, 0, '', '', 'Not Queued', 'Online Sales', 'Cost of Goods Sold', 'Inventory Asset', '', '', 1, 'M', 0, 0, 0, 0, 1, '', 0, '', 0, NULL, 0, NULL, 0, 'designer', '', '', '', '', NULL, 1, 3, 0, '', '', '', 'none', 0, '00000', 0.000, 0, 1, 0, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ec_product_google_attributes`
--

CREATE TABLE IF NOT EXISTS `ec_product_google_attributes` (
  `product_google_attribute_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `attribute_value` text COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`product_google_attribute_id`),
  UNIQUE KEY `product_google_attribute_id` (`product_google_attribute_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_promocode`
--

CREATE TABLE IF NOT EXISTS `ec_promocode` (
  `promocode_id` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `is_dollar_based` tinyint(1) NOT NULL DEFAULT '0',
  `is_percentage_based` tinyint(1) NOT NULL DEFAULT '0',
  `is_shipping_based` tinyint(1) NOT NULL DEFAULT '0',
  `is_free_item_based` tinyint(1) NOT NULL DEFAULT '0',
  `is_for_me_based` tinyint(1) NOT NULL DEFAULT '0',
  `by_manufacturer_id` tinyint(1) NOT NULL DEFAULT '0',
  `by_category_id` tinyint(1) NOT NULL DEFAULT '0',
  `by_product_id` tinyint(1) NOT NULL DEFAULT '0',
  `by_all_products` int(11) NOT NULL DEFAULT '0',
  `promo_dollar` float(15,3) NOT NULL DEFAULT '0.000',
  `promo_percentage` float(15,3) NOT NULL DEFAULT '0.000',
  `promo_shipping` float(15,3) NOT NULL DEFAULT '0.000',
  `promo_free_item` float(15,3) NOT NULL DEFAULT '0.000',
  `promo_for_me` float(15,3) NOT NULL DEFAULT '0.000',
  `manufacturer_id` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `message` blob NOT NULL,
  `max_redemptions` int(11) NOT NULL DEFAULT '999',
  `times_redeemed` int(11) NOT NULL DEFAULT '0',
  `expiration_date` datetime DEFAULT NULL,
  `duration` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'forever',
  `duration_in_months` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`promocode_id`),
  KEY `promo_manufacturer_id` (`manufacturer_id`),
  KEY `promo_product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ec_promotion`
--

CREATE TABLE IF NOT EXISTS `ec_promotion` (
  `promotion_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `type` int(11) NOT NULL DEFAULT '0',
  `start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_date` datetime DEFAULT '0000-00-00 00:00:00',
  `product_id_1` int(11) NOT NULL DEFAULT '0',
  `product_id_2` int(11) NOT NULL DEFAULT '0',
  `product_id_3` int(11) NOT NULL DEFAULT '0',
  `manufacturer_id_1` int(11) NOT NULL DEFAULT '0',
  `manufacturer_id_2` int(11) NOT NULL DEFAULT '0',
  `manufacturer_id_3` int(11) NOT NULL DEFAULT '0',
  `category_id_1` int(11) NOT NULL DEFAULT '0',
  `category_id_2` int(11) NOT NULL DEFAULT '0',
  `category_id_3` int(11) NOT NULL DEFAULT '0',
  `price1` float(15,3) NOT NULL DEFAULT '0.000',
  `price2` float(15,3) NOT NULL DEFAULT '0.000',
  `price3` float(15,3) NOT NULL DEFAULT '0.000',
  `percentage1` float(15,3) NOT NULL DEFAULT '0.000',
  `percentage2` float(15,3) NOT NULL DEFAULT '0.000',
  `percentage3` float(15,3) NOT NULL DEFAULT '0.000',
  `number1` int(11) NOT NULL DEFAULT '0',
  `number2` int(11) NOT NULL DEFAULT '0',
  `number3` int(11) NOT NULL DEFAULT '0',
  `promo_limit` int(11) NOT NULL DEFAULT '3',
  PRIMARY KEY (`promotion_id`),
  UNIQUE KEY `promotion_promotion_id` (`promotion_id`),
  KEY `promotion_product_id_1` (`product_id_1`),
  KEY `promotion_product_id_2` (`product_id_2`),
  KEY `promotion_product_id_3` (`product_id_3`),
  KEY `promotion_manufacturer_id_1` (`manufacturer_id_1`),
  KEY `promotion_manufacturer_id_2` (`manufacturer_id_2`),
  KEY `promotion_manufacturer_id_3` (`manufacturer_id_3`),
  KEY `promotion_category_id_1` (`category_id_1`),
  KEY `promotion_category_id_2` (`category_id_2`),
  KEY `promotion_category_id_3` (`category_id_3`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_response`
--

CREATE TABLE IF NOT EXISTS `ec_response` (
  `response_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_error` tinyint(1) NOT NULL DEFAULT '0',
  `processor` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `response_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `response_text` text COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`response_id`),
  KEY `order_id` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_review`
--

CREATE TABLE IF NOT EXISTS `ec_review` (
  `review_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `rating` int(2) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` mediumblob NOT NULL,
  `date_submitted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`review_id`),
  UNIQUE KEY `review_id` (`review_id`),
  KEY `product_id` (`product_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_role`
--

CREATE TABLE IF NOT EXISTS `ec_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_label` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `admin_access` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`role_id`),
  UNIQUE KEY `role_id` (`role_id`),
  KEY `role_label` (`role_label`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `ec_role`
--

INSERT INTO `ec_role` (`role_id`, `role_label`, `admin_access`) VALUES
(1, 'admin', 1),
(2, 'shopper', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ec_roleaccess`
--

CREATE TABLE IF NOT EXISTS `ec_roleaccess` (
  `roleaccess_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_label` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `admin_panel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`roleaccess_id`),
  UNIQUE KEY `roleaccess_id` (`roleaccess_id`),
  KEY `role_label` (`role_label`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_roleprice`
--

CREATE TABLE IF NOT EXISTS `ec_roleprice` (
  `roleprice_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `role_label` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `role_price` float(15,3) NOT NULL DEFAULT '0.000',
  PRIMARY KEY (`roleprice_id`),
  UNIQUE KEY `roleprice_id` (`roleprice_id`),
  KEY `product_id` (`product_id`),
  KEY `role_label` (`role_label`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_setting`
--

CREATE TABLE IF NOT EXISTS `ec_setting` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `site_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `reg_code` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `storeversion` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `storetype` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'wordpress',
  `storepage` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'store',
  `cartpage` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'cart',
  `accountpage` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'account',
  `timezone` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Europe/London',
  `shipping_method` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'method',
  `shipping_expedite_rate` float(11,2) NOT NULL DEFAULT '0.00',
  `shipping_handling_rate` float(11,2) NOT NULL DEFAULT '0.00',
  `ups_access_license_number` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `ups_user_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `ups_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `ups_ship_from_zip` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `ups_shipper_number` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `ups_country_code` varchar(9) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'US',
  `ups_weight_type` varchar(19) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'LBS',
  `ups_conversion_rate` float(9,3) NOT NULL DEFAULT '1.000',
  `usps_user_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `usps_ship_from_zip` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fedex_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fedex_account_number` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fedex_meter_number` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fedex_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fedex_ship_from_zip` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fedex_weight_units` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'LB',
  `fedex_country_code` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'US',
  `fedex_conversion_rate` float(9,3) NOT NULL DEFAULT '1.000',
  `fedex_test_account` tinyint(1) NOT NULL DEFAULT '0',
  `auspost_api_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `auspost_ship_from_zip` varchar(55) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `dhl_site_id` varchar(155) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `dhl_password` varchar(155) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `dhl_ship_from_country` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'US',
  `dhl_ship_from_zip` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `dhl_weight_unit` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'LB',
  `dhl_test_mode` tinyint(1) NOT NULL DEFAULT '0',
  `fraktjakt_customer_id` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fraktjakt_login_key` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fraktjakt_conversion_rate` double(15,3) NOT NULL DEFAULT '1.000',
  `fraktjakt_test_mode` tinyint(1) NOT NULL DEFAULT '0',
  `fraktjakt_address` varchar(120) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fraktjakt_city` varchar(55) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fraktjakt_state` varchar(2) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fraktjakt_zip` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fraktjakt_country` varchar(2) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `ups_ship_from_state` varchar(2) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `ups_negotiated_rates` tinyint(1) NOT NULL DEFAULT '0',
  `canadapost_username` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `canadapost_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `canadapost_customer_number` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `canadapost_contract_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `canadapost_test_mode` tinyint(1) NOT NULL DEFAULT '0',
  `canadapost_ship_from_zip` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`setting_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ec_setting`
--

INSERT INTO `ec_setting` (`setting_id`, `site_url`, `reg_code`, `storeversion`, `storetype`, `storepage`, `cartpage`, `accountpage`, `timezone`, `shipping_method`, `shipping_expedite_rate`, `shipping_handling_rate`, `ups_access_license_number`, `ups_user_id`, `ups_password`, `ups_ship_from_zip`, `ups_shipper_number`, `ups_country_code`, `ups_weight_type`, `ups_conversion_rate`, `usps_user_name`, `usps_ship_from_zip`, `fedex_key`, `fedex_account_number`, `fedex_meter_number`, `fedex_password`, `fedex_ship_from_zip`, `fedex_weight_units`, `fedex_country_code`, `fedex_conversion_rate`, `fedex_test_account`, `auspost_api_key`, `auspost_ship_from_zip`, `dhl_site_id`, `dhl_password`, `dhl_ship_from_country`, `dhl_ship_from_zip`, `dhl_weight_unit`, `dhl_test_mode`, `fraktjakt_customer_id`, `fraktjakt_login_key`, `fraktjakt_conversion_rate`, `fraktjakt_test_mode`, `fraktjakt_address`, `fraktjakt_city`, `fraktjakt_state`, `fraktjakt_zip`, `fraktjakt_country`, `ups_ship_from_state`, `ups_negotiated_rates`, `canadapost_username`, `canadapost_password`, `canadapost_customer_number`, `canadapost_contract_id`, `canadapost_test_mode`, `canadapost_ship_from_zip`) VALUES
(1, 'localhost/oz_production/public_html', '', '1.0.0', 'wordpress', '6', '7', '8', 'America/Los_Angeles', 'method', 0.00, 0.00, '', '', '', '', '', '', '', 1.000, '', '', '', '', '', '', '', 'LB', 'US', 1.000, 0, '', '', '', '', '', '', '', 0, '', '', 1.000, 0, '', '', '', '', '', '', 0, '', '', '', '', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `ec_shippingrate`
--

CREATE TABLE IF NOT EXISTS `ec_shippingrate` (
  `shippingrate_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_demo_item` tinyint(1) NOT NULL DEFAULT '0',
  `zone_id` int(11) NOT NULL DEFAULT '0',
  `is_price_based` tinyint(1) NOT NULL DEFAULT '0',
  `is_weight_based` tinyint(1) NOT NULL DEFAULT '0',
  `is_method_based` tinyint(1) NOT NULL DEFAULT '0',
  `is_quantity_based` tinyint(1) NOT NULL DEFAULT '0',
  `is_percentage_based` tinyint(1) NOT NULL DEFAULT '0',
  `is_ups_based` tinyint(1) NOT NULL DEFAULT '0',
  `is_usps_based` tinyint(1) NOT NULL DEFAULT '0',
  `is_fedex_based` tinyint(1) NOT NULL DEFAULT '0',
  `is_auspost_based` tinyint(1) NOT NULL DEFAULT '0',
  `is_dhl_based` tinyint(1) NOT NULL DEFAULT '0',
  `is_canadapost_based` tinyint(1) NOT NULL DEFAULT '0',
  `trigger_rate` float(15,3) NOT NULL DEFAULT '0.000',
  `shipping_rate` float(15,3) NOT NULL DEFAULT '0.000',
  `shipping_label` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_order` int(11) NOT NULL DEFAULT '0',
  `shipping_code` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_override_rate` float(11,3) DEFAULT NULL,
  `free_shipping_at` float(15,3) NOT NULL DEFAULT '-1.000',
  PRIMARY KEY (`shippingrate_id`),
  UNIQUE KEY `shippingrate_id` (`shippingrate_id`),
  KEY `zone_id` (`zone_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=55 ;

--
-- Dumping data for table `ec_shippingrate`
--

INSERT INTO `ec_shippingrate` (`shippingrate_id`, `is_demo_item`, `zone_id`, `is_price_based`, `is_weight_based`, `is_method_based`, `is_quantity_based`, `is_percentage_based`, `is_ups_based`, `is_usps_based`, `is_fedex_based`, `is_auspost_based`, `is_dhl_based`, `is_canadapost_based`, `trigger_rate`, `shipping_rate`, `shipping_label`, `shipping_order`, `shipping_code`, `shipping_override_rate`, `free_shipping_at`) VALUES
(51, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 5.000, '', 0, '', 0.000, -1.000),
(52, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 7.990, 'Standard Shipping 7-10 Days', 1, '', NULL, -1.000),
(53, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 14.990, 'Priority 3 Day Shipping', 2, '', NULL, -1.000),
(54, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 19.990, 'Priority 2 Day Shipping', 3, '', NULL, -1.000);

-- --------------------------------------------------------

--
-- Table structure for table `ec_shipping_class`
--

CREATE TABLE IF NOT EXISTS `ec_shipping_class` (
  `shipping_class_id` int(11) NOT NULL AUTO_INCREMENT,
  `class_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`shipping_class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_shipping_class_to_rate`
--

CREATE TABLE IF NOT EXISTS `ec_shipping_class_to_rate` (
  `shipping_class_to_rate_id` int(11) NOT NULL AUTO_INCREMENT,
  `shipping_class_id` int(11) NOT NULL DEFAULT '0',
  `shipping_rate_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`shipping_class_to_rate_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_state`
--

CREATE TABLE IF NOT EXISTS `ec_state` (
  `id_sta` int(11) NOT NULL AUTO_INCREMENT,
  `idcnt_sta` int(11) NOT NULL DEFAULT '0',
  `code_sta` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `name_sta` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `group_sta` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `ship_to_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_sta`),
  KEY `idcnt_sta` (`idcnt_sta`),
  KEY `code_sta` (`code_sta`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=312 ;

--
-- Dumping data for table `ec_state`
--

INSERT INTO `ec_state` (`id_sta`, `idcnt_sta`, `code_sta`, `name_sta`, `sort_order`, `group_sta`, `ship_to_active`) VALUES
(1, 223, 'AL', 'Alabama', 9, '', 1),
(2, 223, 'AK', 'Alaska', 10, '', 1),
(4, 223, 'AZ', 'Arizona', 11, '', 1),
(5, 223, 'AR', 'Arkansas', 12, '', 1),
(12, 223, 'CA', 'California', 13, '', 1),
(13, 223, 'CO', 'Colorado', 14, '', 1),
(14, 223, 'CT', 'Connecticut', 15, '', 1),
(15, 223, 'DE', 'Delaware', 16, '', 1),
(16, 223, 'DC', 'District of Columbia', 17, '', 1),
(18, 223, 'FL', 'Florida', 18, '', 1),
(19, 223, 'GA', 'Georgia', 19, '', 1),
(21, 223, 'HI', 'Hawaii', 21, '', 1),
(22, 223, 'ID', 'Idaho', 22, '', 1),
(23, 223, 'IL', 'Illinois', 23, '', 1),
(24, 223, 'IN', 'Indiana', 24, '', 1),
(25, 223, 'IA', 'Iowa', 25, '', 1),
(26, 223, 'KS', 'Kansas', 26, '', 1),
(27, 223, 'KY', 'Kentucky', 27, '', 1),
(28, 223, 'LA', 'Louisiana', 28, '', 1),
(29, 223, 'ME', 'Maine', 29, '', 1),
(31, 223, 'MD', 'Maryland', 30, '', 1),
(32, 223, 'MA', 'Massachusetts', 31, '', 1),
(33, 223, 'MI', 'Michigan', 32, '', 1),
(34, 223, 'MN', 'Minnesota', 33, '', 1),
(35, 223, 'MS', 'Mississippi', 34, '', 1),
(36, 223, 'MO', 'Missouri', 35, '', 1),
(37, 223, 'MT', 'Montana', 36, '', 1),
(38, 223, 'NE', 'Nebraska', 37, '', 1),
(39, 223, 'NV', 'Nevada', 38, '', 1),
(40, 223, 'NH', 'New Hampshire', 39, '', 1),
(41, 223, 'NJ', 'New Jersey', 40, '', 1),
(42, 223, 'NM', 'New Mexico', 41, '', 1),
(43, 223, 'NY', 'New York', 42, '', 1),
(44, 223, 'NC', 'North Carolina', 43, '', 1),
(45, 223, 'ND', 'North Dakota', 44, '', 1),
(47, 223, 'OH', 'Ohio', 45, '', 1),
(48, 223, 'OK', 'Oklahoma', 46, '', 1),
(49, 223, 'OR', 'Oregon', 47, '', 1),
(51, 223, 'PA', 'Pennsylvania', 48, '', 1),
(52, 223, 'PR', 'Puerto Rico', 49, '', 1),
(53, 223, 'RI', 'Rhode Island', 50, '', 1),
(54, 223, 'SC', 'South Carolina', 51, '', 1),
(55, 223, 'SD', 'South Dakota', 52, '', 1),
(56, 223, 'TN', 'Tennessee', 53, '', 1),
(57, 223, 'TX', 'Texas', 54, '', 1),
(58, 223, 'UT', 'Utah', 55, '', 1),
(59, 223, 'VT', 'Vermont', 56, '', 1),
(60, 223, 'VI', 'Virgin Islands', 57, '', 1),
(61, 223, 'VA', 'Virginia', 58, '', 1),
(62, 223, 'WA', 'Washington', 59, '', 1),
(63, 223, 'WV', 'West Virginia', 60, '', 1),
(64, 223, 'WI', 'Wisconsin', 61, '', 1),
(65, 223, 'WY', 'Wyoming', 62, '', 1),
(66, 38, 'AB', 'Alberta', 100, '', 1),
(67, 38, 'BC', 'British Columbia', 101, '', 1),
(68, 38, 'MB', 'Manitoba', 102, '', 1),
(69, 38, 'NF', 'Newfoundland', 103, '', 1),
(70, 38, 'NB', 'New Brunswick', 104, '', 1),
(71, 38, 'NS', 'Nova Scotia', 105, '', 1),
(72, 38, 'NT', 'Northwest Territories', 106, '', 1),
(73, 38, 'NU', 'Nunavut', 107, '', 1),
(74, 38, 'ON', 'Ontario', 108, '', 1),
(75, 38, 'PE', 'Prince Edward Island', 109, '', 1),
(76, 38, 'QC', 'Quebec', 110, '', 1),
(77, 38, 'SK', 'Saskatchewan', 111, '', 1),
(78, 38, 'YT', 'Yukon Territory', 112, '', 1),
(79, 13, 'ACT', 'Australian Capital Territory', 113, '', 1),
(80, 13, 'CX', 'Christmas Island', 114, '', 1),
(81, 13, 'CC', 'Cocos Islands', 115, '', 1),
(82, 13, 'HM', 'Heard Island and McDonald Islands', 116, '', 1),
(83, 13, 'NSW', 'New South Wales', 117, '', 1),
(84, 13, 'NF', 'Norfolk Island', 118, '', 1),
(85, 13, 'NT', 'Northern Territory', 119, '', 1),
(86, 13, 'QLD', 'Queensland', 120, '', 1),
(87, 13, 'SA', 'South Australia', 121, '', 1),
(88, 13, 'TAS', 'Tasmania', 122, '', 1),
(89, 13, 'VIC', 'Victoria', 123, '', 1),
(90, 13, 'WA', 'Western Australia', 124, '', 1),
(91, 222, 'Avon', 'Avon', 125, 'England', 1),
(92, 222, 'Bedfordshire', 'Bedfordshire', 126, 'England', 1),
(93, 222, 'Berkshire', 'Berkshire', 127, 'England', 1),
(94, 222, 'Buckinghamshire', 'Buckinghamshire', 128, 'England', 1),
(95, 222, 'Cambridgeshire', 'Cambridgeshire', 129, 'England', 1),
(96, 222, 'Cheshire', 'Cheshire', 130, 'England', 1),
(97, 222, 'Cleveland', 'Cleveland', 131, 'England', 1),
(98, 222, 'Cornwall', 'Cornwall', 132, 'England', 1),
(99, 222, 'Cumbria', 'Cumbria', 133, 'England', 1),
(100, 222, 'Derbyshire', 'Derbyshire', 134, 'England', 1),
(101, 222, 'Devon', 'Devon', 135, 'England', 1),
(102, 222, 'Dorset', 'Dorset', 136, 'England', 1),
(103, 222, 'Durham', 'Durham', 137, 'England', 1),
(104, 222, 'East Sussex', 'East Sussex', 138, 'England', 1),
(105, 222, 'Essex', 'Essex', 139, 'England', 1),
(106, 222, 'Gloucestershire', 'Gloucestershire', 140, 'England', 1),
(107, 222, 'Hampshire', 'Hampshire', 141, 'England', 1),
(108, 222, 'Herefordshire', 'Herefordshire', 142, 'England', 1),
(109, 222, 'Hertfordshire', 'Hertfordshire', 143, 'England', 1),
(110, 222, 'Isle of Wight', 'Isle of Wight', 144, 'England', 1),
(111, 222, 'Kent', 'Kent', 145, 'England', 1),
(112, 222, 'Lancashire', 'Lancashire', 146, 'England', 1),
(113, 222, 'Leicestershire', 'Leicestershire', 147, 'England', 1),
(114, 222, 'Lincolnshire', 'Lincolnshire', 148, 'England', 1),
(115, 222, 'London', 'London', 149, 'England', 1),
(116, 222, 'Merseyside', 'Merseyside', 150, 'England', 1),
(117, 222, 'Middlesex', 'Middlesex', 151, 'England', 1),
(118, 222, 'Norfolk', 'Norfolk', 152, 'England', 1),
(119, 222, 'Northamptonshire', 'Northamptonshire', 153, 'England', 1),
(120, 222, 'Northumberland', 'Northumberland', 154, 'England', 1),
(121, 222, 'North Humberside', 'North Humberside', 155, 'England', 1),
(122, 222, 'North Yorkshire', 'North Yorkshire', 156, 'England', 1),
(123, 222, 'Nottinghamshire', 'Nottinghamshire', 157, 'England', 1),
(124, 222, 'Oxfordshire', 'Oxfordshire', 158, 'England', 1),
(125, 222, 'Rutland', 'Rutland', 159, 'England', 1),
(126, 222, 'Shropshire', 'Shropshire', 160, 'England', 1),
(127, 222, 'Somerset', 'Somerset', 161, 'England', 1),
(128, 222, 'South Humberside', 'South Humberside', 162, 'England', 1),
(129, 222, 'South Yorkshire', 'South Yorkshire', 163, 'England', 1),
(130, 222, 'Staffordshire', 'Staffordshire', 164, 'England', 1),
(131, 222, 'Suffolk', 'Suffolk', 165, 'England', 1),
(132, 222, 'Surrey', 'Surrey', 166, 'England', 1),
(133, 222, 'Tyne and Wear', 'Tyne and Wear', 167, 'England', 1),
(134, 222, 'Warwickshire', 'Warwickshire', 168, 'England', 1),
(135, 222, 'West Midlands', 'West Midlands', 169, 'England', 1),
(136, 222, 'West Sussex', 'West Sussex', 170, 'England', 1),
(137, 222, 'West Yorkshire', 'West Yorkshire', 171, 'England', 1),
(138, 222, 'Wiltshire', 'Wiltshire', 172, 'England', 1),
(139, 222, 'Worcestershire', 'Worcestershire', 173, 'England', 1),
(140, 222, 'Clwyd', 'Clwyd', 174, 'Wales', 1),
(141, 222, 'Dyfed', 'Dyfed', 175, 'Wales', 1),
(142, 222, 'Gwent', 'Gwent', 176, 'Wales', 1),
(143, 222, 'Gwynedd', 'Gwynedd', 177, 'Wales', 1),
(144, 222, 'Mid Glamorgan', 'Mid Glamorgan', 178, 'Wales', 1),
(145, 222, 'Powys', 'Powys', 179, 'Wales', 1),
(146, 222, 'South Glamorgan', 'South Glamorgan', 180, 'Wales', 1),
(147, 222, 'West Glamorgan', 'West Glamorgan', 181, 'Wales', 1),
(148, 222, 'Aberdeenshire', 'Aberdeenshire', 182, 'Scotland', 1),
(149, 222, 'Angus', 'Angus', 183, 'Scotland', 1),
(150, 222, 'Argyll', 'Argyll', 184, 'Scotland', 1),
(151, 222, 'Ayrshire', 'Ayrshire', 185, 'Scotland', 1),
(152, 222, 'Banffshire', 'Banffshire', 186, 'Scotland', 1),
(153, 222, 'Berwickshire', 'Berwickshire', 187, 'Scotland', 1),
(154, 222, 'Bute', 'Bute', 188, 'Scotland', 1),
(155, 222, 'Caithness', 'Caithness', 189, 'Scotland', 1),
(156, 222, 'Clackmannanshire', 'Clackmannanshire', 190, 'Scotland', 1),
(157, 222, 'Dumfriesshire', 'Dumfriesshire', 191, 'Scotland', 1),
(158, 222, 'Dunbartonshire', 'Dunbartonshire', 192, 'Scotland', 1),
(159, 222, 'East Lothian', 'East Lothian', 193, 'Scotland', 1),
(160, 222, 'Fife', 'Fife', 194, 'Scotland', 1),
(161, 222, 'Inverness-shire', 'Inverness-shire', 195, 'Scotland', 1),
(162, 222, 'Kincardineshire', 'Kincardineshire', 196, 'Scotland', 1),
(163, 222, 'Kinross-shire', 'Kinross-shire', 197, 'Scotland', 1),
(164, 222, 'Kirkcudbrightshire', 'Kirkcudbrightshire', 198, 'Scotland', 1),
(165, 222, 'Lanarkshire', 'Lanarkshire', 199, 'Scotland', 1),
(166, 222, 'Midlothian', 'Midlothian', 200, 'Scotland', 1),
(167, 222, 'Moray', 'Moray', 201, 'Scotland', 1),
(168, 222, 'Nairnshire', 'Nairnshire', 202, 'Scotland', 1),
(169, 222, 'Orkney', 'Orkney', 203, 'Scotland', 1),
(170, 222, 'Peeblesshire', 'Peeblesshire', 204, 'Scotland', 1),
(171, 222, 'Perthshire', 'Perthshire', 205, 'Scotland', 1),
(172, 222, 'Renfrewshire', 'Renfrewshire', 206, 'Scotland', 1),
(173, 222, 'Ross-shire', 'Ross-shire', 207, 'Scotland', 1),
(174, 222, 'Roxburghshire', 'Roxburghshire', 208, 'Scotland', 1),
(175, 222, 'Selkirkshire', 'Selkirkshire', 209, 'Scotland', 1),
(176, 222, 'Shetland', 'Shetland', 210, 'Scotland', 1),
(177, 222, 'Stirlingshire', 'Stirlingshire', 211, 'Scotland', 1),
(178, 222, 'Sutherland', 'Sutherland', 212, 'Scotland', 1),
(179, 222, 'West Lothian', 'West Lothian', 213, 'Scotland', 1),
(180, 222, 'Wigtownshire', 'Wigtownshire', 214, 'Scotland', 1),
(181, 222, 'Antrim', 'Antrim', 215, 'Northern Ireland', 1),
(182, 222, 'Down', 'Down', 217, 'Northern Ireland', 1),
(183, 222, 'Armagh', 'Armagh', 216, 'Northern Ireland', 1),
(184, 222, 'Fermanagh', 'Fermanagh', 218, 'Northern Ireland', 1),
(185, 222, 'Londonderry', 'Londonderry', 219, 'Northern Ireland', 1),
(186, 222, 'Tyrone', 'Tyrone', 220, 'Northern Ireland', 1),
(187, 30, 'AL', 'Alagoas', 221, '', 1),
(188, 30, 'AM', 'Amazonas', 222, '', 1),
(189, 30, 'BA', 'Bahia', 223, '', 1),
(190, 30, 'CE', 'Cearà', 224, '', 1),
(191, 30, 'DF', 'Distrito Federal', 225, '', 1),
(192, 30, 'ES', 'Espìrito Santo', 226, '', 1),
(193, 30, 'GO', 'Goias', 227, '', 1),
(194, 30, 'MA', 'Maranhao', 228, '', 1),
(195, 30, 'MT', 'Mato Grosso', 229, '', 1),
(196, 30, 'MS', 'Mato Grosso Do Sul', 230, '', 1),
(197, 30, 'MG', 'Minas Gerais', 231, '', 1),
(198, 30, 'PA', 'Parà', 232, '', 1),
(199, 30, 'PB', 'Paraìba', 233, '', 1),
(200, 30, 'PR', 'Paranà', 234, '', 1),
(201, 30, 'PE', 'Pernambuco', 235, '', 1),
(202, 30, 'PI', 'Piauì', 236, '', 1),
(203, 30, 'RJ', 'Rio de Janeiro', 237, '', 1),
(204, 30, 'RN', 'Rio Grande do Norte', 238, '', 1),
(205, 30, 'RS', 'Dio Grande do Sul', 239, '', 1),
(206, 30, 'RO', 'Rondônia', 240, '', 1),
(207, 30, 'SC', 'Santa Catarina', 241, '', 1),
(208, 30, 'SP', 'Sao Paulo', 242, '', 1),
(209, 30, 'SE', 'Sergipe', 243, '', 1),
(210, 44, 'ANH', 'Anhui', 244, '', 1),
(211, 44, 'BEI', 'Beijing', 245, '', 1),
(212, 44, 'CHO', 'Chongqing', 246, '', 1),
(213, 44, 'FUJ', 'Fujian', 247, '', 1),
(214, 44, 'GAN', 'Gansu', 248, '', 1),
(215, 44, 'GDG', 'Guangdong', 249, '', 1),
(216, 44, 'GXI', 'Guangxi', 250, '', 1),
(217, 44, 'GUI', 'Guizhou', 251, '', 1),
(218, 44, 'HAI', 'Hainan', 252, '', 1),
(219, 44, 'HEB', 'Hebei', 253, '', 1),
(220, 44, 'HEI', 'Heilongjiang', 254, '', 1),
(221, 44, 'HEN', 'Henan', 255, '', 1),
(222, 44, 'HUB', 'Hubei', 256, '', 1),
(223, 44, 'HUN', 'Hunan', 257, '', 1),
(224, 44, 'JSU', 'Jiangsu', 258, '', 1),
(225, 44, 'JXI', 'Jiangxi', 259, '', 1),
(226, 44, 'JIL', 'Jilin', 260, '', 1),
(227, 44, 'LIA', 'Liaoning', 261, '', 1),
(228, 44, 'MON', 'Nei Mongol', 262, '', 1),
(229, 44, 'NIN', 'Ningxia', 263, '', 1),
(230, 44, 'QIN', 'Qinghai', 264, '', 1),
(231, 44, 'SHA', 'Shaanxi', 265, '', 1),
(232, 44, 'SHD', 'Shandong', 266, '', 1),
(233, 44, 'SHH', 'Shanghai', 267, '', 1),
(234, 44, 'SHX', 'Shanxi', 268, '', 1),
(235, 44, 'SIC', 'Sichuan', 269, '', 1),
(236, 44, 'TIA', 'TIanjin', 270, '', 1),
(237, 44, 'XIN', 'Xinjiang', 271, '', 1),
(238, 44, 'XIZ', 'Xizang', 272, '', 1),
(239, 44, 'YUN', 'Yunnan', 273, '', 1),
(240, 44, 'ZHE', 'Zhejiang', 274, '', 1),
(241, 99, 'AND', 'Andhra Pradesh', 275, '', 1),
(242, 99, 'ASS', 'Assam', 276, '', 1),
(243, 99, 'BIH', 'Bihar', 277, '', 1),
(244, 99, 'CHH', 'Chhattisgarh', 278, '', 1),
(245, 99, 'DEL', 'Delhi', 279, '', 1),
(246, 99, 'GOA', 'Goa', 280, '', 1),
(247, 99, 'GUJ', 'Gujarat', 281, '', 1),
(248, 99, 'HAR', 'Haryana', 282, '', 1),
(249, 99, 'HIM', 'Himachal Pradesh', 283, '', 1),
(250, 99, 'JAM', 'Jammu & Kashmir', 284, '', 1),
(251, 99, 'JHA', 'Jharkhand', 285, '', 1),
(252, 99, 'KAR', 'Karnataka', 286, '', 1),
(253, 99, 'KER', 'Kerala', 287, '', 1),
(254, 99, 'MAD', 'Madhya Pradesh', 288, '', 1),
(255, 99, 'MAH', 'Maharashtra', 289, '', 1),
(256, 99, 'MEG', 'Meghalaya', 290, '', 1),
(257, 99, 'ORI', 'Orissa', 291, '', 1),
(258, 99, 'PON', 'Pondicherry', 292, '', 1),
(259, 99, 'PUN', 'Punjab', 293, '', 1),
(260, 99, 'RAJ', 'Rajasthan', 294, '', 1),
(261, 99, 'TAM', 'Tamil Nadu', 295, '', 1),
(262, 99, 'UTT', 'Uttar Pradesh', 296, '', 1),
(263, 99, 'UTR', 'Uttaranchal', 297, '', 1),
(264, 99, 'WES', 'West Bengal', 298, '', 1),
(265, 107, 'AIC', 'Aichi', 299, '', 1),
(266, 107, 'AKT', 'Akita', 300, '', 1),
(267, 107, 'AMR', 'Aomori', 301, '', 1),
(268, 107, 'CHB', 'Chiba', 302, '', 1),
(269, 107, 'EHM', 'Ehime', 303, '', 1),
(270, 107, 'FKI', 'Fukui', 304, '', 1),
(271, 107, 'FKO', 'Fukuoka', 305, '', 1),
(272, 107, 'FSM', 'Fukushima', 306, '', 1),
(273, 107, 'GFU', 'Gifu', 307, '', 1),
(274, 107, 'GUM', 'Gunma', 308, '', 1),
(275, 107, 'HRS', 'Hiroshima', 309, '', 1),
(276, 107, 'HKD', 'Hokkaido', 310, '', 1),
(277, 107, 'HYG', 'Hyogo', 311, '', 1),
(278, 107, 'IBR', 'Ibaraki', 312, '', 1),
(279, 107, 'IKW', 'Ishikawa', 313, '', 1),
(280, 107, 'IWT', 'Iwate', 314, '', 1),
(281, 107, 'KGW', 'Kagawa', 315, '', 1),
(282, 107, 'KGS', 'Kagoshima', 316, '', 1),
(283, 107, 'KNG', 'Kanagawa', 317, '', 1),
(284, 107, 'KCH', 'Kochi', 318, '', 1),
(285, 107, 'KMM', 'Kumamoto', 319, '', 1),
(286, 107, 'KYT', 'Kyoto', 320, '', 1),
(287, 107, 'MIE', 'Mie', 321, '', 1),
(288, 107, 'MYG', 'Miyagi', 322, '', 1),
(289, 107, 'MYZ', 'Miyazaki', 323, '', 1),
(290, 107, 'NGN', 'Nagano', 324, '', 1),
(291, 107, 'NGS', 'Nagasaki', 325, '', 1),
(292, 107, 'NRA', 'Nara', 326, '', 1),
(293, 107, 'NGT', 'Niigata', 327, '', 1),
(294, 107, 'OTA', 'Oita', 328, '', 1),
(295, 107, 'OKY', 'Okayama', 329, '', 1),
(296, 107, 'OKN', 'Okinawa', 330, '', 1),
(297, 107, 'OSK', 'Osaka', 331, '', 1),
(298, 107, 'SAG', 'Saga', 332, '', 1),
(299, 107, 'STM', 'Saitama', 333, '', 1),
(300, 107, 'SHG', 'Shiga', 334, '', 1),
(301, 107, 'SMN', 'Shimane', 335, '', 1),
(302, 107, 'SZK', 'Shizuoka', 336, '', 1),
(303, 107, 'TOC', 'Tochigi', 337, '', 1),
(304, 107, 'TKS', 'Tokushima', 338, '', 1),
(305, 107, 'TKY', 'Tokyo', 335, '', 1),
(306, 107, 'TTR', 'Tottori', 336, '', 1),
(307, 107, 'TYM', 'Toyama', 337, '', 1),
(308, 107, 'WKY', 'Wakayama', 338, '', 1),
(309, 107, 'YGT', 'Yamagata', 339, '', 1),
(310, 107, 'YGC', 'Yamaguchi', 340, '', 1),
(311, 107, 'YNS', 'Yamanashi', 341, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ec_subscriber`
--

CREATE TABLE IF NOT EXISTS `ec_subscriber` (
  `subscriber_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `last_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`subscriber_id`),
  UNIQUE KEY `subscriber_email` (`email`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_subscription`
--

CREATE TABLE IF NOT EXISTS `ec_subscription` (
  `subscription_id` int(11) NOT NULL AUTO_INCREMENT,
  `subscription_type` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'paypal',
  `subscription_status` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Active',
  `title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `email` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `last_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_country` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'US',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `model_number` varchar(510) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `price` double(21,3) NOT NULL DEFAULT '0.000',
  `payment_length` int(11) NOT NULL DEFAULT '1',
  `payment_period` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `payment_duration` int(11) NOT NULL DEFAULT '0',
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_payment_date` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `next_payment_date` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `number_payments_completed` int(11) NOT NULL DEFAULT '1',
  `paypal_txn_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `paypal_txn_type` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `paypal_subscr_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `paypal_username` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `paypal_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `stripe_subscription_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `quantity` int(11) NOT NULL DEFAULT '1',
  `num_failed_payment` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`subscription_id`),
  UNIQUE KEY `subscription_id` (`subscription_id`),
  KEY `user_id` (`user_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_subscription_plan`
--

CREATE TABLE IF NOT EXISTS `ec_subscription_plan` (
  `subscription_plan_id` int(11) NOT NULL AUTO_INCREMENT,
  `plan_title` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `can_downgrade` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`subscription_plan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_taxrate`
--

CREATE TABLE IF NOT EXISTS `ec_taxrate` (
  `taxrate_id` int(11) NOT NULL AUTO_INCREMENT,
  `tax_by_state` tinyint(1) NOT NULL DEFAULT '0',
  `tax_by_country` tinyint(1) NOT NULL DEFAULT '0',
  `tax_by_duty` tinyint(1) NOT NULL DEFAULT '0',
  `tax_by_vat` tinyint(1) NOT NULL DEFAULT '0',
  `tax_by_single_vat` tinyint(1) NOT NULL DEFAULT '0',
  `tax_by_all` tinyint(1) NOT NULL DEFAULT '0',
  `state_rate` float(15,3) NOT NULL DEFAULT '0.000',
  `country_rate` float(15,3) NOT NULL DEFAULT '0.000',
  `duty_rate` float(15,3) NOT NULL DEFAULT '0.000',
  `vat_rate` float(15,3) NOT NULL DEFAULT '0.000',
  `vat_added` tinyint(1) NOT NULL DEFAULT '0',
  `vat_included` tinyint(1) NOT NULL DEFAULT '0',
  `all_rate` float(15,3) NOT NULL DEFAULT '0.000',
  `state_code` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `country_code` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `vat_country_code` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `duty_exempt_country_code` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`taxrate_id`),
  UNIQUE KEY `taxrate_id` (`taxrate_id`),
  KEY `state_code` (`state_code`),
  KEY `country_code` (`country_code`),
  KEY `vat_country_code` (`vat_country_code`),
  KEY `duty_exempt_country_code` (`duty_exempt_country_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_tempcart`
--

CREATE TABLE IF NOT EXISTS `ec_tempcart` (
  `tempcart_id` int(11) NOT NULL AUTO_INCREMENT,
  `session_id` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `quantity` int(11) DEFAULT '0',
  `grid_quantity` int(11) DEFAULT '0',
  `gift_card_message` blob,
  `gift_card_from_name` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `gift_card_to_name` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `optionitem_id_1` int(11) NOT NULL DEFAULT '0',
  `optionitem_id_2` int(11) NOT NULL DEFAULT '0',
  `optionitem_id_3` int(11) NOT NULL DEFAULT '0',
  `optionitem_id_4` int(11) NOT NULL DEFAULT '0',
  `optionitem_id_5` int(11) NOT NULL DEFAULT '0',
  `donation_price` float(15,3) NOT NULL DEFAULT '0.000',
  `last_changed_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_deconetwork` tinyint(1) NOT NULL DEFAULT '0',
  `deconetwork_id` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_product_code` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_options` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_edit_link` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_color_code` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_product_id` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_image_link` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_discount` float(15,3) NOT NULL DEFAULT '0.000',
  `deconetwork_tax` float(15,3) NOT NULL DEFAULT '0.000',
  `deconetwork_total` float(15,3) NOT NULL DEFAULT '0.000',
  `deconetwork_version` int(11) NOT NULL DEFAULT '1',
  `gift_card_email` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `abandoned_cart_email_sent` int(11) NOT NULL DEFAULT '0',
  `hide_from_admin` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tempcart_id`),
  UNIQUE KEY `tempcart_tempcart_id` (`tempcart_id`),
  KEY `tempcart_session_id` (`session_id`),
  KEY `tempcart_product_id` (`product_id`),
  KEY `tempcart_optionitem_id_1` (`optionitem_id_1`),
  KEY `tempcart_optionitem_id_2` (`optionitem_id_2`),
  KEY `tempcart_optionitem_id_3` (`optionitem_id_3`),
  KEY `tempcart_optionitem_id_4` (`optionitem_id_4`),
  KEY `tempcart_optionitem_id_5` (`optionitem_id_5`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ec_tempcart`
--

INSERT INTO `ec_tempcart` (`tempcart_id`, `session_id`, `product_id`, `quantity`, `grid_quantity`, `gift_card_message`, `gift_card_from_name`, `gift_card_to_name`, `optionitem_id_1`, `optionitem_id_2`, `optionitem_id_3`, `optionitem_id_4`, `optionitem_id_5`, `donation_price`, `last_changed_date`, `is_deconetwork`, `deconetwork_id`, `deconetwork_name`, `deconetwork_product_code`, `deconetwork_options`, `deconetwork_edit_link`, `deconetwork_color_code`, `deconetwork_product_id`, `deconetwork_image_link`, `deconetwork_discount`, `deconetwork_tax`, `deconetwork_total`, `deconetwork_version`, `gift_card_email`, `abandoned_cart_email_sent`, `hide_from_admin`) VALUES
(1, 'QVUEIYCUSMIKOMAUGJHBQDSBAGKXGR', 2, 2, 0, '', '', '', 0, 0, 0, 0, 0, 0.000, '2018-04-30 15:33:52', 0, '', '', '', '', '', '', '', '', 0.000, 0.000, 0.000, 1, '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ec_tempcart_data`
--

CREATE TABLE IF NOT EXISTS `ec_tempcart_data` (
  `tempcart_data_id` int(11) NOT NULL AUTO_INCREMENT,
  `tempcart_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `session_id` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `email` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `username` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `first_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `last_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `coupon_code` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `giftcard` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_first_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_last_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_company_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_address_line_1` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_address_line_2` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_city` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_state` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_zip` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_country` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_phone` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_selector` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_first_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_last_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_company_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_address_line_2` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_address_line_1` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_city` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_state` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_zip` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_country` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_phone` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `create_account` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `order_notes` text COLLATE utf8mb4_unicode_520_ci,
  `shipping_method` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `estimate_shipping_zip` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `expedited_shipping` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `estimate_shipping_country` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `is_guest` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `guest_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `subscription_option1` text COLLATE utf8mb4_unicode_520_ci,
  `subscription_option2` text COLLATE utf8mb4_unicode_520_ci,
  `subscription_option3` text COLLATE utf8mb4_unicode_520_ci,
  `subscription_option4` text COLLATE utf8mb4_unicode_520_ci,
  `subscription_option5` text COLLATE utf8mb4_unicode_520_ci,
  `subscription_advanced_option` text COLLATE utf8mb4_unicode_520_ci,
  `subscription_quantity` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `convert_to` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `translate_to` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `taxcloud_tax_amount` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `taxcloud_address_verified` tinyint(1) NOT NULL DEFAULT '0',
  `perpage` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `vat_registration_number` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`tempcart_data_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ec_tempcart_data`
--

INSERT INTO `ec_tempcart_data` (`tempcart_data_id`, `tempcart_time`, `session_id`, `user_id`, `email`, `username`, `first_name`, `last_name`, `coupon_code`, `giftcard`, `billing_first_name`, `billing_last_name`, `billing_company_name`, `billing_address_line_1`, `billing_address_line_2`, `billing_city`, `billing_state`, `billing_zip`, `billing_country`, `billing_phone`, `shipping_selector`, `shipping_first_name`, `shipping_last_name`, `shipping_company_name`, `shipping_address_line_2`, `shipping_address_line_1`, `shipping_city`, `shipping_state`, `shipping_zip`, `shipping_country`, `shipping_phone`, `create_account`, `order_notes`, `shipping_method`, `estimate_shipping_zip`, `expedited_shipping`, `estimate_shipping_country`, `is_guest`, `guest_key`, `subscription_option1`, `subscription_option2`, `subscription_option3`, `subscription_option4`, `subscription_option5`, `subscription_advanced_option`, `subscription_quantity`, `convert_to`, `translate_to`, `taxcloud_tax_amount`, `taxcloud_address_verified`, `perpage`, `vat_registration_number`) VALUES
(1, '2018-04-30 15:09:15', 'QVUEIYCUSMIKOMAUGJHBQDSBAGKXGR', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `ec_tempcart_optionitem`
--

CREATE TABLE IF NOT EXISTS `ec_tempcart_optionitem` (
  `tempcart_optionitem_id` int(11) NOT NULL AUTO_INCREMENT,
  `tempcart_id` int(11) NOT NULL DEFAULT '0',
  `option_id` int(11) NOT NULL DEFAULT '0',
  `optionitem_id` int(11) NOT NULL DEFAULT '0',
  `optionitem_value` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `session_id` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_model_number` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`tempcart_optionitem_id`),
  UNIQUE KEY `tempcart_optionitem_id` (`tempcart_optionitem_id`),
  KEY `tempcart_id` (`tempcart_id`),
  KEY `option_id` (`option_id`),
  KEY `optionitem_id` (`optionitem_id`),
  KEY `session_id` (`session_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ec_tempcart_optionitem`
--

INSERT INTO `ec_tempcart_optionitem` (`tempcart_optionitem_id`, `tempcart_id`, `option_id`, `optionitem_id`, `optionitem_value`, `session_id`, `optionitem_model_number`) VALUES
(1, 999999999, 3, 3, 'test', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `ec_timezone`
--

CREATE TABLE IF NOT EXISTS `ec_timezone` (
  `timezone_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `identifier` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`timezone_id`),
  UNIQUE KEY `timezone_id` (`timezone_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=141 ;

--
-- Dumping data for table `ec_timezone`
--

INSERT INTO `ec_timezone` (`timezone_id`, `name`, `identifier`) VALUES
(1, '(GMT-12:00) International Date Line West', 'Pacific/Wake'),
(2, '(GMT-11:00) Midway Island', 'Pacific/Apia'),
(3, '(GMT-11:00) Samoa', 'Pacific/Apia'),
(4, '(GMT-10:00) Hawaii', 'Pacific/Honolulu'),
(5, '(GMT-09:00) Alaska', 'America/Anchorage'),
(6, '(GMT-08:00) Pacific Time (US & Canada) Tijuana', 'America/Los_Angeles'),
(7, '(GMT-07:00) Arizona', 'America/Phoenix'),
(8, '(GMT-07:00) Chihuahua', 'America/Chihuahua'),
(9, '(GMT-07:00) La Paz', 'America/Chihuahua'),
(10, '(GMT-07:00) Mazatlan', 'America/Chihuahua'),
(11, '(GMT-07:00) Mountain Time (US & Canada)', 'America/Denver'),
(12, '(GMT-06:00) Central America', 'America/Managua'),
(13, '(GMT-06:00) Central Time (US & Canada)', 'America/Chicago'),
(14, '(GMT-06:00) Guadalajara', 'America/Mexico_City'),
(15, '(GMT-06:00) Mexico City', 'America/Mexico_City'),
(16, '(GMT-06:00) Monterrey', 'America/Mexico_City'),
(17, '(GMT-06:00) Saskatchewan', 'America/Regina'),
(18, '(GMT-05:00) Bogota', 'America/Bogota'),
(19, '(GMT-05:00) Eastern Time (US & Canada)', 'America/New_York'),
(20, '(GMT-05:00) Indiana (East)', 'America/Indiana/Indianapolis'),
(21, '(GMT-05:00) Lima', 'America/Bogota'),
(22, '(GMT-05:00) Quito', 'America/Bogota'),
(23, '(GMT-04:00) Atlantic Time (Canada)', 'America/Halifax'),
(24, '(GMT-04:00) Caracas', 'America/Caracas'),
(25, '(GMT-04:00) La Paz', 'America/Caracas'),
(26, '(GMT-04:00) Santiago', 'America/Santiago'),
(27, '(GMT-03:30) Newfoundland', 'America/St_Johns'),
(28, '(GMT-03:00) Brasilia', 'America/Sao_Paulo'),
(29, '(GMT-03:00) Buenos Aires', 'America/Argentina/Buenos_Aires'),
(30, '(GMT-03:00) Georgetown', 'America/Argentina/Buenos_Aires'),
(31, '(GMT-03:00) Greenland', 'America/Godthab'),
(32, '(GMT-02:00) Mid-Atlantic', 'America/Noronha'),
(33, '(GMT-01:00) Azores', 'Atlantic/Azores'),
(34, '(GMT-01:00) Cape Verde Is.', 'Atlantic/Cape_Verde'),
(35, '(GMT) Casablanca', 'Africa/Casablanca'),
(36, '(GMT) Edinburgh', 'Europe/London'),
(37, '(GMT) Greenwich Mean Time : Dublin', 'Europe/London'),
(38, '(GMT) Lisbon', 'Europe/London'),
(39, '(GMT) London', 'Europe/London'),
(40, '(GMT) Monrovia', 'Africa/Casablanca'),
(41, '(GMT+01:00) Amsterdam', 'Europe/Berlin'),
(42, '(GMT+01:00) Belgrade', 'Europe/Belgrade'),
(43, '(GMT+01:00) Berlin', 'Europe/Berlin'),
(44, '(GMT+01:00) Bern', 'Europe/Berlin'),
(45, '(GMT+01:00) Bratislava', 'Europe/Belgrade'),
(46, '(GMT+01:00) Brussels', 'Europe/Paris'),
(47, '(GMT+01:00) Budapest', 'Europe/Belgrade'),
(48, '(GMT+01:00) Copenhagen', 'Europe/Paris'),
(49, '(GMT+01:00) Ljubljana', 'Europe/Belgrade'),
(50, '(GMT+01:00) Madrid', 'Europe/Paris'),
(51, '(GMT+01:00) Paris', 'Europe/Paris'),
(52, '(GMT+01:00) Prague', 'Europe/Belgrade'),
(53, '(GMT+01:00) Rome', 'Europe/Berlin'),
(54, '(GMT+01:00) Sarajevo', 'Europe/Sarajevo'),
(55, '(GMT+01:00) Skopje', 'Europe/Sarajevo'),
(56, '(GMT+01:00) Stockholm', 'Europe/Berlin'),
(57, '(GMT+01:00) Vienna', 'Europe/Berlin'),
(58, '(GMT+01:00) Warsaw', 'Europe/Sarajevo'),
(59, '(GMT+01:00) West Central Africa', 'Africa/Lagos'),
(60, '(GMT+01:00) Zagreb', 'Europe/Sarajevo'),
(61, '(GMT+02:00) Athens', 'Europe/Istanbul'),
(62, '(GMT+02:00) Bucharest', 'Europe/Bucharest'),
(63, '(GMT+02:00) Cairo', 'Africa/Cairo'),
(64, '(GMT+02:00) Harare', 'Africa/Johannesburg'),
(65, '(GMT+02:00) Helsinki', 'Europe/Helsinki'),
(66, '(GMT+02:00) Istanbul', 'Europe/Istanbul'),
(67, '(GMT+02:00) Jerusalem', 'Asia/Jerusalem'),
(68, '(GMT+02:00) Kyiv', 'Europe/Helsinki'),
(69, '(GMT+02:00) Minsk', 'Europe/Istanbul'),
(70, '(GMT+02:00) Pretoria', 'Africa/Johannesburg'),
(71, '(GMT+02:00) Riga', 'Europe/Helsinki'),
(72, '(GMT+02:00) Sofia', 'Europe/Helsinki'),
(73, '(GMT+02:00) Tallinn', 'Europe/Helsinki'),
(74, '(GMT+02:00) Vilnius', 'Europe/Helsinki'),
(75, '(GMT+03:00) Baghdad', 'Asia/Baghdad'),
(76, '(GMT+03:00) Kuwait', 'Asia/Riyadh'),
(77, '(GMT+03:00) Moscow', 'Europe/Moscow'),
(78, '(GMT+03:00) Nairobi', 'Africa/Nairobi'),
(79, '(GMT+03:00) Riyadh', 'Asia/Riyadh'),
(80, '(GMT+03:00) St. Petersburg', 'Europe/Moscow'),
(81, '(GMT+03:00) Volgograd', 'Europe/Moscow'),
(82, '(GMT+03:30) Tehran', 'Asia/Tehran'),
(83, '(GMT+04:00) Abu Dhabi', 'Asia/Muscat'),
(84, '(GMT+04:00) Baku', 'Asia/Tbilisi'),
(85, '(GMT+04:00) Muscat', 'Asia/Muscat'),
(86, '(GMT+04:00) Tbilisi', 'Asia/Tbilisi'),
(87, '(GMT+04:00) Yerevan', 'Asia/Tbilisi'),
(88, '(GMT+04:30) Kabul', 'Asia/Kabul'),
(89, '(GMT+05:00) Ekaterinburg', 'Asia/Yekaterinburg'),
(90, '(GMT+05:00) Islamabad', 'Asia/Karachi'),
(91, '(GMT+05:00) Karachi', 'Asia/Karachi'),
(92, '(GMT+05:00) Tashkent', 'Asia/Karachi'),
(93, '(GMT+05:30) Chennai', 'Asia/Calcutta'),
(94, '(GMT+05:30) Kolkata', 'Asia/Calcutta'),
(95, '(GMT+05:30) Mumbai', 'Asia/Calcutta'),
(96, '(GMT+05:30) New Delhi', 'Asia/Calcutta'),
(97, '(GMT+05:45) Kathmandu', 'Asia/Katmandu'),
(98, '(GMT+06:00) Almaty', 'Asia/Novosibirsk'),
(99, '(GMT+06:00) Astana', 'Asia/Dhaka'),
(100, '(GMT+06:00) Dhaka', 'Asia/Dhaka'),
(101, '(GMT+06:00) Novosibirsk', 'Asia/Novosibirsk'),
(102, '(GMT+06:00) Sri Jayawardenepura', 'Asia/Colombo'),
(103, '(GMT+06:30) Rangoon', 'Asia/Rangoon'),
(104, '(GMT+07:00) Bangkok', 'Asia/Bangkok'),
(105, '(GMT+07:00) Hanoi', 'Asia/Bangkok'),
(106, '(GMT+07:00) Jakarta', 'Asia/Bangkok'),
(107, '(GMT+07:00) Krasnoyarsk', 'Asia/Krasnoyarsk'),
(108, '(GMT+08:00) Beijing', 'Asia/Hong_Kong'),
(109, '(GMT+08:00) Chongqing', 'Asia/Hong_Kong'),
(110, '(GMT+08:00) Hong Kong', 'Asia/Hong_Kong'),
(111, '(GMT+08:00) Irkutsk', 'Asia/Irkutsk'),
(112, '(GMT+08:00) Kuala Lumpur', 'Asia/Singapore'),
(113, '(GMT+08:00) Perth', 'Australia/Perth'),
(114, '(GMT+08:00) Singapore', 'Asia/Singapore'),
(115, '(GMT+08:00) Taipei', 'Asia/Taipei'),
(116, '(GMT+08:00) Ulaan Bataar', 'Asia/Irkutsk'),
(117, '(GMT+08:00) Urumqi', 'Asia/Hong_Kong'),
(118, '(GMT+09:00) Osaka', 'Asia/Tokyo'),
(119, '(GMT+09:00) Sapporo', 'Asia/Tokyo'),
(120, '(GMT+09:00) Seoul', 'Asia/Seoul'),
(121, '(GMT+09:00) Tokyo', 'Asia/Tokyo'),
(122, '(GMT+09:00) Yakutsk', 'Asia/Yakutsk'),
(123, '(GMT+09:30) Adelaide', 'Australia/Adelaide'),
(124, '(GMT+09:30) Darwin', 'Australia/Darwin'),
(125, '(GMT+10:00) Brisbane', 'Australia/Brisbane'),
(126, '(GMT+10:00) Canberra', 'Australia/Sydney'),
(127, '(GMT+10:00) Guam', 'Pacific/Guam'),
(128, '(GMT+10:00) Hobart', 'Australia/Hobart'),
(129, '(GMT+10:00) Melbourne', 'Australia/Sydney'),
(130, '(GMT+10:00) Port Moresby', 'Pacific/Guam'),
(131, '(GMT+10:00) Sydney', 'Australia/Sydney'),
(132, '(GMT+10:00) Vladivostok', 'Asia/Vladivostok'),
(133, '(GMT+11:00) Magadan', 'Asia/Magadan'),
(134, '(GMT+11:00) New Caledonia', 'Asia/Magadan'),
(135, '(GMT+11:00) Solomon Is.', 'Asia/Magadan'),
(136, '(GMT+12:00) Auckland', 'Pacific/Auckland'),
(137, '(GMT+12:00) Fiji', 'Pacific/Fiji'),
(138, '(GMT+12:00) Kamchatka', 'Pacific/Fiji'),
(139, '(GMT+12:00) Marshall Is.', 'Pacific/Fiji'),
(140, '(GMT+12:00) Wellington', 'Pacific/Auckland');

-- --------------------------------------------------------

--
-- Table structure for table `ec_user`
--

CREATE TABLE IF NOT EXISTS `ec_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_demo_item` tinyint(1) NOT NULL DEFAULT '0',
  `email` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `list_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `edit_sequence` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `quickbooks_status` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Not Queued',
  `first_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `last_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `default_billing_address_id` int(11) NOT NULL DEFAULT '0',
  `default_shipping_address_id` int(11) NOT NULL DEFAULT '0',
  `user_level` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'shopper',
  `is_subscriber` tinyint(1) NOT NULL DEFAULT '0',
  `realauth_registered` tinyint(1) NOT NULL DEFAULT '0',
  `stripe_customer_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `default_card_type` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `default_card_last4` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `exclude_tax` tinyint(1) NOT NULL DEFAULT '0',
  `exclude_shipping` tinyint(1) NOT NULL DEFAULT '0',
  `user_notes` text COLLATE utf8mb4_unicode_520_ci,
  `vat_registration_number` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_user_id` (`user_id`),
  UNIQUE KEY `user_email` (`email`(191)),
  KEY `user_password` (`password`(191)),
  KEY `user_default_billing_address_id` (`default_billing_address_id`),
  KEY `user_default_shipping_address_id` (`default_shipping_address_id`),
  KEY `user_user_level` (`user_level`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_webhook`
--

CREATE TABLE IF NOT EXISTS `ec_webhook` (
  `webhook_id` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `webhook_type` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `webhook_data` blob,
  PRIMARY KEY (`webhook_id`),
  UNIQUE KEY `webhook_id` (`webhook_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ec_zone`
--

CREATE TABLE IF NOT EXISTS `ec_zone` (
  `zone_id` int(11) NOT NULL AUTO_INCREMENT,
  `zone_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`zone_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `ec_zone`
--

INSERT INTO `ec_zone` (`zone_id`, `zone_name`) VALUES
(1, 'North America'),
(2, 'South America'),
(3, 'Europe'),
(4, 'Africa'),
(5, 'Asia'),
(6, 'Australia'),
(7, 'Oceania'),
(8, 'Lower 48 States'),
(9, 'Alaska and Hawaii');

-- --------------------------------------------------------

--
-- Table structure for table `ec_zone_to_location`
--

CREATE TABLE IF NOT EXISTS `ec_zone_to_location` (
  `zone_to_location_id` int(11) NOT NULL AUTO_INCREMENT,
  `zone_id` int(11) NOT NULL DEFAULT '0',
  `iso2_cnt` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `code_sta` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`zone_to_location_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=259 ;

--
-- Dumping data for table `ec_zone_to_location`
--

INSERT INTO `ec_zone_to_location` (`zone_to_location_id`, `zone_id`, `iso2_cnt`, `code_sta`) VALUES
(1, 1, 'AI', ''),
(2, 1, 'AQ', ''),
(3, 1, 'AW', ''),
(4, 1, 'BS', ''),
(5, 1, 'BB', ''),
(6, 1, 'BM', ''),
(7, 1, 'BZ', ''),
(8, 1, 'CA', ''),
(9, 1, 'KY', ''),
(10, 1, 'CR', ''),
(11, 1, 'CU', ''),
(12, 1, 'DM', ''),
(13, 1, 'DO', ''),
(14, 1, 'SV', ''),
(15, 1, 'GL', ''),
(16, 1, 'GD', ''),
(17, 1, 'GP', ''),
(18, 1, 'GT', ''),
(19, 1, 'HT', ''),
(20, 1, 'HN', ''),
(21, 1, 'JM', ''),
(22, 1, 'MQ', ''),
(23, 1, 'MX', ''),
(24, 1, 'MS', ''),
(25, 1, 'NI', ''),
(26, 1, 'PA', ''),
(27, 1, 'PR', ''),
(28, 1, 'KN', ''),
(29, 1, 'LC', ''),
(30, 1, 'TT', ''),
(31, 1, 'TC', ''),
(32, 1, 'US', ''),
(33, 1, 'VI', ''),
(34, 2, 'AR', ''),
(35, 2, 'BO', ''),
(36, 2, 'BR', ''),
(37, 2, 'CL', ''),
(38, 2, 'CO', ''),
(39, 2, 'EC', ''),
(40, 2, 'GF', ''),
(41, 2, 'GY', ''),
(42, 2, 'PY', ''),
(43, 2, 'PE', ''),
(44, 2, 'SR', ''),
(45, 2, 'UY', ''),
(46, 2, 'VE', ''),
(47, 6, 'AU', ''),
(48, 7, 'AS', ''),
(49, 7, 'AU', ''),
(50, 7, 'CK', ''),
(51, 7, 'FJ', ''),
(52, 7, 'PF', ''),
(53, 7, 'GU', ''),
(54, 7, 'KI', ''),
(55, 7, 'MH', ''),
(56, 7, 'NR', ''),
(57, 7, 'NC', ''),
(58, 7, 'NZ', ''),
(59, 7, 'NU', ''),
(60, 7, 'NF', ''),
(61, 7, 'PW', ''),
(62, 7, 'PG', ''),
(63, 7, 'PN', ''),
(64, 7, 'WS', ''),
(65, 7, 'SB', ''),
(66, 7, 'TK', ''),
(67, 7, 'TO', ''),
(68, 7, 'TV', ''),
(69, 7, 'VU', ''),
(70, 7, 'WF', ''),
(71, 3, 'AL', ''),
(72, 3, 'AD', ''),
(73, 3, 'AT', ''),
(74, 3, 'BY', ''),
(75, 3, 'BE', ''),
(76, 3, 'BG', ''),
(77, 3, 'HR', ''),
(78, 3, 'CZ', ''),
(79, 3, 'DK', ''),
(80, 3, 'EE', ''),
(81, 3, 'FO', ''),
(82, 3, 'FI', ''),
(83, 3, 'FR', ''),
(84, 3, 'DE', ''),
(85, 3, 'GI', ''),
(86, 3, 'GR', ''),
(87, 3, 'HU', ''),
(88, 3, 'IS', ''),
(89, 3, 'IE', ''),
(90, 3, 'IT', ''),
(91, 3, 'LV', ''),
(92, 3, 'LI', ''),
(93, 3, 'LT', ''),
(94, 3, 'LU', ''),
(95, 3, 'MT', ''),
(96, 3, 'MC', ''),
(97, 3, 'NL', ''),
(98, 3, 'NO', ''),
(99, 3, 'PL', ''),
(100, 3, 'PT', ''),
(101, 3, 'RO', ''),
(102, 3, 'RU', ''),
(103, 3, 'SM', ''),
(104, 3, 'SI', ''),
(105, 3, 'ES', ''),
(106, 3, 'SE', ''),
(107, 3, 'CH', ''),
(108, 3, 'UA', ''),
(109, 3, 'GB', ''),
(110, 4, 'DZ', ''),
(111, 4, 'AO', ''),
(112, 4, 'BJ', ''),
(113, 4, 'BW', ''),
(114, 4, 'BF', ''),
(115, 4, 'BI', ''),
(116, 4, 'CM', ''),
(117, 4, 'CV', ''),
(118, 4, 'TD', ''),
(119, 4, 'KM', ''),
(120, 4, 'CG', ''),
(121, 4, 'CI', ''),
(122, 4, 'DJ', ''),
(123, 4, 'EG', ''),
(124, 4, 'GQ', ''),
(125, 4, 'ER', ''),
(126, 4, 'ET', ''),
(127, 4, 'GA', ''),
(128, 4, 'GM', ''),
(129, 4, 'GH', ''),
(130, 4, 'GN', ''),
(131, 4, 'GW', ''),
(132, 4, 'KE', ''),
(133, 4, 'LS', ''),
(134, 4, 'LR', ''),
(135, 4, 'MG', ''),
(136, 4, 'MW', ''),
(137, 4, 'ML', ''),
(138, 4, 'MR', ''),
(139, 4, 'MU', ''),
(140, 4, 'YT', ''),
(141, 4, 'MA', ''),
(142, 4, 'MZ', ''),
(143, 4, 'NA', ''),
(144, 4, 'NE', ''),
(145, 4, 'NG', ''),
(146, 4, 'RE', ''),
(147, 4, 'RW', ''),
(148, 4, 'ST', ''),
(149, 4, 'SN', ''),
(150, 4, 'SC', ''),
(151, 4, 'SL', ''),
(152, 4, 'SO', ''),
(153, 4, 'ZA', ''),
(154, 4, 'SD', ''),
(155, 4, 'SZ', ''),
(156, 4, 'TG', ''),
(157, 4, 'TN', ''),
(158, 4, 'UG', ''),
(159, 4, 'ZM', ''),
(160, 4, 'ZW', ''),
(161, 5, 'AF', ''),
(162, 5, 'AM', ''),
(163, 5, 'AZ', ''),
(164, 5, 'BH', ''),
(165, 5, 'BD', ''),
(166, 5, 'BT', ''),
(167, 5, 'BN', ''),
(168, 5, 'KH', ''),
(169, 5, 'CN', ''),
(170, 5, 'CX', ''),
(171, 5, 'CY', ''),
(172, 5, 'TP', ''),
(173, 5, 'GE', ''),
(174, 5, 'HK', ''),
(175, 5, 'IN', ''),
(176, 5, 'ID', ''),
(177, 5, 'IQ', ''),
(178, 5, 'IL', ''),
(179, 5, 'JP', ''),
(180, 5, 'JO', ''),
(181, 5, 'KZ', ''),
(182, 5, 'KW', ''),
(183, 5, 'KG', ''),
(184, 5, 'LB', ''),
(185, 5, 'MO', ''),
(186, 5, 'MY', ''),
(187, 5, 'MV', ''),
(188, 5, 'MN', ''),
(189, 5, 'MM', ''),
(190, 5, 'NP', ''),
(191, 5, 'OM', ''),
(192, 5, 'PK', ''),
(193, 5, 'PH', ''),
(194, 5, 'QA', ''),
(195, 5, 'SA', ''),
(196, 5, 'SG', ''),
(197, 5, 'LK', ''),
(198, 5, 'TW', ''),
(199, 5, 'TJ', ''),
(200, 5, 'TH', ''),
(201, 5, 'TR', ''),
(202, 5, 'TM', ''),
(203, 5, 'AE', ''),
(204, 5, 'UZ', ''),
(205, 5, 'VN', ''),
(206, 5, 'YE', ''),
(207, 9, 'US', 'HI'),
(208, 9, 'US', 'AK'),
(209, 8, 'US', 'AL'),
(210, 8, 'US', 'AZ'),
(211, 8, 'US', 'AR'),
(212, 8, 'US', 'CA'),
(213, 8, 'US', 'CO'),
(214, 8, 'US', 'CT'),
(215, 8, 'US', 'DE'),
(216, 8, 'US', 'FL'),
(217, 8, 'US', 'GA'),
(218, 8, 'US', 'ID'),
(219, 8, 'US', 'IL'),
(220, 8, 'US', 'IN'),
(221, 8, 'US', 'IA'),
(222, 8, 'US', 'KS'),
(223, 8, 'US', 'KY'),
(224, 8, 'US', 'LA'),
(225, 8, 'US', 'ME'),
(226, 8, 'US', 'MD'),
(227, 8, 'US', 'MA'),
(228, 8, 'US', 'MI'),
(229, 8, 'US', 'MN'),
(230, 8, 'US', 'MS'),
(231, 8, 'US', 'MO'),
(232, 8, 'US', 'MT'),
(233, 8, 'US', 'NE'),
(234, 8, 'US', 'NV'),
(235, 8, 'US', 'NH'),
(236, 8, 'US', 'NJ'),
(237, 8, 'US', 'NM'),
(238, 8, 'US', 'NY'),
(239, 8, 'US', 'NC'),
(240, 8, 'US', 'ND'),
(241, 8, 'US', 'OH'),
(242, 8, 'US', 'OK'),
(243, 8, 'US', 'OR'),
(244, 8, 'US', 'PA'),
(245, 8, 'US', 'RI'),
(246, 8, 'US', 'SC'),
(247, 8, 'US', 'SD'),
(248, 8, 'US', 'TN'),
(249, 8, 'US', 'TX'),
(250, 8, 'US', 'UT'),
(251, 8, 'US', 'VT'),
(252, 8, 'US', 'VA'),
(253, 8, 'US', 'WA'),
(254, 8, 'US', 'WV'),
(255, 8, 'US', 'WI'),
(256, 8, 'US', 'WY'),
(257, 3, 'DC', ''),
(258, 8, 'US', 'DC');

-- --------------------------------------------------------

--
-- Table structure for table `ops_commentmeta`
--

CREATE TABLE IF NOT EXISTS `ops_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ops_comments`
--

CREATE TABLE IF NOT EXISTS `ops_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ops_comments`
--

INSERT INTO `ops_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2018-04-30 06:23:21', '2018-04-30 06:23:21', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href="https://gravatar.com">Gravatar</a>.', 0, '1', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ops_links`
--

CREATE TABLE IF NOT EXISTS `ops_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ops_options`
--

CREATE TABLE IF NOT EXISTS `ops_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=711 ;

--
-- Dumping data for table `ops_options`
--

INSERT INTO `ops_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/oz_production/public_html', 'yes'),
(2, 'home', 'http://localhost/oz_production/public_html', 'yes'),
(3, 'blogname', 'Oz Production', 'yes'),
(4, 'blogdescription', 'Oz Production Event Services, LLC', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'info@ozproductionservices.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:106:{s:11:"^wp-json/?$";s:22:"index.php?rest_route=/";s:14:"^wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:21:"^index.php/wp-json/?$";s:22:"index.php?rest_route=/";s:24:"^index.php/wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:17:"^store/([^/]*)/?$";s:30:"index.php?ec_store=$matches[1]";s:47:"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:42:"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:23:"category/(.+?)/embed/?$";s:46:"index.php?category_name=$matches[1]&embed=true";s:35:"category/(.+?)/page/?([0-9]{1,})/?$";s:53:"index.php?category_name=$matches[1]&paged=$matches[2]";s:17:"category/(.+?)/?$";s:35:"index.php?category_name=$matches[1]";s:44:"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:39:"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:20:"tag/([^/]+)/embed/?$";s:36:"index.php?tag=$matches[1]&embed=true";s:32:"tag/([^/]+)/page/?([0-9]{1,})/?$";s:43:"index.php?tag=$matches[1]&paged=$matches[2]";s:14:"tag/([^/]+)/?$";s:25:"index.php?tag=$matches[1]";s:45:"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:40:"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:21:"type/([^/]+)/embed/?$";s:44:"index.php?post_format=$matches[1]&embed=true";s:33:"type/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?post_format=$matches[1]&paged=$matches[2]";s:15:"type/([^/]+)/?$";s:33:"index.php?post_format=$matches[1]";s:33:"store/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:43:"store/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:63:"store/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:58:"store/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:58:"store/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:39:"store/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:22:"store/([^/]+)/embed/?$";s:41:"index.php?ec_store=$matches[1]&embed=true";s:26:"store/([^/]+)/trackback/?$";s:35:"index.php?ec_store=$matches[1]&tb=1";s:46:"store/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?ec_store=$matches[1]&feed=$matches[2]";s:41:"store/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?ec_store=$matches[1]&feed=$matches[2]";s:34:"store/([^/]+)/page/?([0-9]{1,})/?$";s:48:"index.php?ec_store=$matches[1]&paged=$matches[2]";s:41:"store/([^/]+)/comment-page-([0-9]{1,})/?$";s:48:"index.php?ec_store=$matches[1]&cpage=$matches[2]";s:30:"store/([^/]+)(?:/([0-9]+))?/?$";s:47:"index.php?ec_store=$matches[1]&page=$matches[2]";s:22:"store/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:32:"store/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:52:"store/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:47:"store/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:47:"store/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:28:"store/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:48:".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$";s:18:"index.php?feed=old";s:20:".*wp-app\\.php(/.*)?$";s:19:"index.php?error=403";s:18:".*wp-register.php$";s:23:"index.php?register=true";s:32:"feed/(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:27:"(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:8:"embed/?$";s:21:"index.php?&embed=true";s:20:"page/?([0-9]{1,})/?$";s:28:"index.php?&paged=$matches[1]";s:41:"comments/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:36:"comments/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:17:"comments/embed/?$";s:21:"index.php?&embed=true";s:44:"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:39:"search/(.+)/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:20:"search/(.+)/embed/?$";s:34:"index.php?s=$matches[1]&embed=true";s:32:"search/(.+)/page/?([0-9]{1,})/?$";s:41:"index.php?s=$matches[1]&paged=$matches[2]";s:14:"search/(.+)/?$";s:23:"index.php?s=$matches[1]";s:47:"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:42:"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:23:"author/([^/]+)/embed/?$";s:44:"index.php?author_name=$matches[1]&embed=true";s:35:"author/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?author_name=$matches[1]&paged=$matches[2]";s:17:"author/([^/]+)/?$";s:33:"index.php?author_name=$matches[1]";s:69:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:45:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$";s:74:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]";s:39:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$";s:63:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]";s:56:"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:51:"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:32:"([0-9]{4})/([0-9]{1,2})/embed/?$";s:58:"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true";s:44:"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]";s:26:"([0-9]{4})/([0-9]{1,2})/?$";s:47:"index.php?year=$matches[1]&monthnum=$matches[2]";s:43:"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:38:"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:19:"([0-9]{4})/embed/?$";s:37:"index.php?year=$matches[1]&embed=true";s:31:"([0-9]{4})/page/?([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&paged=$matches[2]";s:13:"([0-9]{4})/?$";s:26:"index.php?year=$matches[1]";s:27:".?.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:".?.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:".?.+?/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"(.?.+?)/embed/?$";s:41:"index.php?pagename=$matches[1]&embed=true";s:20:"(.?.+?)/trackback/?$";s:35:"index.php?pagename=$matches[1]&tb=1";s:40:"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:35:"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:28:"(.?.+?)/page/?([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&paged=$matches[2]";s:35:"(.?.+?)/comment-page-([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&cpage=$matches[2]";s:24:"(.?.+?)(?:/([0-9]+))?/?$";s:47:"index.php?pagename=$matches[1]&page=$matches[2]";s:27:"[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:"[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:"[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"([^/]+)/embed/?$";s:37:"index.php?name=$matches[1]&embed=true";s:20:"([^/]+)/trackback/?$";s:31:"index.php?name=$matches[1]&tb=1";s:40:"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:35:"([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:28:"([^/]+)/page/?([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&paged=$matches[2]";s:35:"([^/]+)/comment-page-([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&cpage=$matches[2]";s:24:"([^/]+)(?:/([0-9]+))?/?$";s:43:"index.php?name=$matches[1]&page=$matches[2]";s:16:"[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:26:"[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:46:"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:22:"[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:1:{i:0;s:26:"wp-easycart/wpeasycart.php";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'ozproduction', 'yes'),
(41, 'stylesheet', 'ozproduction', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '38590', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'posts', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:"title";s:0:"";s:5:"count";i:0;s:12:"hierarchical";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(79, 'widget_text', 'a:0:{}', 'yes'),
(80, 'widget_rss', 'a:0:{}', 'yes'),
(81, 'uninstall_plugins', 'a:1:{s:26:"wp-easycart/wpeasycart.php";s:12:"ec_uninstall";}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '0', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'initial_db_version', '38590', 'yes'),
(92, 'ops_user_roles', 'a:5:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:61:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:34:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:10:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:5:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:2:{s:4:"read";b:1;s:7:"level_0";b:1;}}}', 'yes'),
(93, 'fresh_site', '0', 'yes'),
(94, 'widget_search', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(95, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(96, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(97, 'widget_archives', 'a:2:{i:2;a:3:{s:5:"title";s:0:"";s:5:"count";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(98, 'widget_meta', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(99, 'sidebars_widgets', 'a:4:{s:19:"wp_inactive_widgets";a:0:{}s:19:"primary-widget-area";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:21:"secondary-widget-area";a:0:{}s:13:"array_version";i:3;}', 'yes'),
(100, 'widget_pages', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(101, 'widget_calendar', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(102, 'widget_media_audio', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(103, 'widget_media_image', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(104, 'widget_media_gallery', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(105, 'widget_media_video', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(106, 'widget_tag_cloud', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(107, 'widget_nav_menu', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(108, 'widget_custom_html', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(109, 'cron', 'a:4:{i:1525112603;a:3:{s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1525155829;a:2:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}s:25:"delete_expired_transients";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1525184702;a:1:{s:30:"wp_scheduled_auto_draft_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}s:7:"version";i:2;}', 'yes'),
(110, 'theme_mods_twentyseventeen', 'a:2:{s:18:"custom_css_post_id";i:-1;s:16:"sidebars_widgets";a:2:{s:4:"time";i:1525072201;s:4:"data";a:4:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:9:"sidebar-2";a:0:{}s:9:"sidebar-3";a:0:{}}}}', 'yes'),
(114, '_site_transient_update_core', 'O:8:"stdClass":4:{s:7:"updates";a:1:{i:0;O:8:"stdClass":10:{s:8:"response";s:6:"latest";s:8:"download";s:59:"https://downloads.wordpress.org/release/wordpress-4.9.5.zip";s:6:"locale";s:5:"en_US";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:59:"https://downloads.wordpress.org/release/wordpress-4.9.5.zip";s:10:"no_content";s:70:"https://downloads.wordpress.org/release/wordpress-4.9.5-no-content.zip";s:11:"new_bundled";s:71:"https://downloads.wordpress.org/release/wordpress-4.9.5-new-bundled.zip";s:7:"partial";b:0;s:8:"rollback";b:0;}s:7:"current";s:5:"4.9.5";s:7:"version";s:5:"4.9.5";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.7";s:15:"partial_version";s:0:"";}}s:12:"last_checked";i:1525095357;s:15:"version_checked";s:5:"4.9.5";s:12:"translations";a:0:{}}', 'no'),
(119, '_site_transient_update_themes', 'O:8:"stdClass":4:{s:12:"last_checked";i:1525099622;s:7:"checked";a:4:{s:12:"ozproduction";s:3:"1.0";s:13:"twentyfifteen";s:3:"1.9";s:15:"twentyseventeen";s:3:"1.5";s:13:"twentysixteen";s:3:"1.4";}s:8:"response";a:0:{}s:12:"translations";a:0:{}}', 'no'),
(120, '_site_transient_timeout_browser_efc56fe28520bcd166ef136f44025003', '1525674231', 'no'),
(121, '_site_transient_browser_efc56fe28520bcd166ef136f44025003', 'a:10:{s:4:"name";s:6:"Chrome";s:7:"version";s:13:"65.0.3325.181";s:8:"platform";s:7:"Windows";s:10:"update_url";s:29:"https://www.google.com/chrome";s:7:"img_src";s:43:"http://s.w.org/images/browsers/chrome.png?1";s:11:"img_src_ssl";s:44:"https://s.w.org/images/browsers/chrome.png?1";s:15:"current_version";s:2:"18";s:7:"upgrade";b:0;s:8:"insecure";b:0;s:6:"mobile";b:0;}', 'no'),
(123, '_site_transient_timeout_community-events-4501c091b0366d76ea3218b6cfdd8097', '1525112633', 'no'),
(124, 'can_compress_scripts', '1', 'no'),
(125, '_site_transient_community-events-4501c091b0366d76ea3218b6cfdd8097', 'a:2:{s:8:"location";a:1:{s:2:"ip";s:2:"::";}s:6:"events";a:1:{i:0;a:7:{s:4:"type";s:6:"meetup";s:5:"title";s:38:"WordPress 15th Anniversary Celebration";s:3:"url";s:63:"https://www.meetup.com/Dhaka-WordPress-Meetup/events/249500665/";s:6:"meetup";s:22:"Dhaka WordPress Meetup";s:10:"meetup_url";s:46:"https://www.meetup.com/Dhaka-WordPress-Meetup/";s:4:"date";s:19:"2018-05-27 16:00:00";s:8:"location";a:4:{s:8:"location";s:17:"Dhaka, Bangladesh";s:7:"country";s:2:"bd";s:8:"latitude";d:23.740286000000001;s:9:"longitude";d:90.374899999999997;}}}}', 'no'),
(126, '_transient_timeout_feed_ac0b00fe65abe10e0c5b588f3ed8c7ca', '1525112634', 'no');
INSERT INTO `ops_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(127, '_transient_feed_ac0b00fe65abe10e0c5b588f3ed8c7ca', 'a:4:{s:5:"child";a:1:{s:0:"";a:1:{s:3:"rss";a:1:{i:0;a:6:{s:4:"data";s:3:"\n\n\n";s:7:"attribs";a:1:{s:0:"";a:1:{s:7:"version";s:3:"2.0";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:1:{s:7:"channel";a:1:{i:0;a:6:{s:4:"data";s:49:"\n	\n	\n	\n	\n	\n	\n	\n	\n	\n	\n		\n		\n		\n		\n		\n		\n		\n		\n		\n	";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:4:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:14:"WordPress News";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:26:"https://wordpress.org/news";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:14:"WordPress News";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:13:"lastBuildDate";a:1:{i:0;a:5:{s:4:"data";s:34:"\n	Fri, 20 Apr 2018 21:07:41 +0000	";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"language";a:1:{i:0;a:5:{s:4:"data";s:5:"en-US";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:9:"generator";a:1:{i:0;a:5:{s:4:"data";s:40:"https://wordpress.org/?v=5.0-alpha-43018";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"item";a:10:{i:0;a:6:{s:4:"data";s:39:"\n		\n		\n		\n		\n				\n		\n		\n\n		\n		\n				\n			";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:4:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:50:"Celebrate the WordPress 15th Anniversary on May 27";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:86:"https://wordpress.org/news/2018/04/celebrate-the-wordpress-15th-anniversary-on-may-27/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 20 Apr 2018 21:07:29 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:3:{i:0;a:5:{s:4:"data";s:6:"Events";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:5:"Store";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:2;a:5:{s:4:"data";s:4:"wp15";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"https://wordpress.org/news/?p=5753";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:345:"May 27, 2018 is the 15th anniversary of the first WordPress release ﻿— and we can&#8217;t wait to celebrate! Party time! Join WordPress fans all over the world in celebrating the 15th Anniversary of WordPress by throwing your own party! Here&#8217;s how you can join in the fun: Check the WordPress 15th Anniversary website to see [&#8230;]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:16:"Andrea Middleton";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:3948:"<p>May 27, 2018 is the <strong>15th anniversary</strong> of the <a href="https://wordpress.org/news/2003/05/wordpress-now-available/">first WordPress release</a> <a href="https://wordpress.org/news/2003/05/wordpress-now-available/">﻿</a>— and we can&#8217;t wait to celebrate!</p>\n\n<figure class="wp-block-image aligncenter"><img src="https://i1.wp.com/wordpress.org/news/files/2018/04/wp15_mark-1.png?resize=342%2C268&#038;ssl=1" alt="" class="wp-image-5841" width="342" height="268" srcset="https://i1.wp.com/wordpress.org/news/files/2018/04/wp15_mark-1.png?w=2188&amp;ssl=1 2188w, https://i1.wp.com/wordpress.org/news/files/2018/04/wp15_mark-1.png?resize=300%2C236&amp;ssl=1 300w, https://i1.wp.com/wordpress.org/news/files/2018/04/wp15_mark-1.png?resize=768%2C605&amp;ssl=1 768w, https://i1.wp.com/wordpress.org/news/files/2018/04/wp15_mark-1.png?resize=1024%2C806&amp;ssl=1 1024w, https://i1.wp.com/wordpress.org/news/files/2018/04/wp15_mark-1.png?w=1264&amp;ssl=1 1264w, https://i1.wp.com/wordpress.org/news/files/2018/04/wp15_mark-1.png?w=1896&amp;ssl=1 1896w" sizes="(max-width: 342px) 100vw, 342px" data-recalc-dims="1" /></figure>\n\n<h2>Party time!</h2>\n\n<p>Join WordPress fans all over the world in celebrating the 15th Anniversary of WordPress by throwing your own party! Here&#8217;s how you can join in the fun:</p>\n\n<ol>\n    <li>Check the <a href="https://wp15.wordpress.net/about/">WordPress 15th Anniversary website</a> to see if there&#8217;s a party already planned for your town. If there is, RSVP for the party and invite your friends!<br /></li>\n    <li>If there isn&#8217;t, then pick a place to go where a bunch of people can be merry — a park, a pub, a backyard; any family-friendly venue will do!</li>\n    <li>List your party with <a href="https://www.meetup.com/pro/wordpress/">your local WordPress meetup group</a> (Don&#8217;t have a group? <a href="https://make.wordpress.org/community/handbook/meetup-organizer/welcome/#starting-a-new-meetup-com-group">Start one!</a>)  and then spread the word to other local meetups, tech groups, press, etc and get people to say they’ll come to your party.</li>\n    <li><a href="https://make.wordpress.org/community/handbook/meetup-organizer/event-formats/wordpress-15th-anniversary-celebrations/#request-wordpress-15th-anniversary-swag">Request</a> some special 15th anniversary WordPress swag (no later than April 27, please, so we have time to ship it to you).<br /></li>\n    <li>Have party attendees post photos, videos, and the like with the #WP15 hashtag, and <a href="https://wp15.wordpress.net/live/">check out the social media stream</a> to see how the rest of the world is sharing and celebrating.</li>\n</ol>\n\n<p>Don&#8217;t miss this chance to participate in a global celebration of WordPress!<br /></p>\n\n<h2>Special Swag</h2>\n\n<p>In honor of the 15th anniversary, we’ve added some <a href="https://mercantile.wordpress.org/product-category/wordpress-15/">special 15th anniversary items</a> in the swag store — you can use the offer code <strong>CELEBRATEWP15</strong> to take 15% off this (and any other WordPress swag you buy), all the way through the end of 2018!</p>\n\n<p>Keep checking the swag store, because we&#8217;ll be adding more swag over the next few weeks!</p>\n\n<h2>Share the fun</h2>\n\n<p>However you celebrate the WordPress 15th anniversary — with <a href="https://wp15.wordpress.net/about/">a party</a>, with <a href="https://wp15.wordpress.net/swag/">commemorative swag</a>, by <a href="https://wp15.wordpress.net/live/">telling the world</a> what WordPress means to you — remember to use the #WP15 hashtag to share it! And don&#8217;t forget to <a href="https://wp15.wordpress.net/live/">check the stream of WordPress 15th anniversary posts</a>.</p>\n\n<p>When <a href="https://venturebeat.com/2018/03/05/wordpress-now-powers-30-of-websites/">30% of the internet</a> has a reason to celebrate, you know it&#8217;s going to be great! </p>\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:30:"com-wordpress:feed-additions:1";a:1:{s:7:"post-id";a:1:{i:0;a:5:{s:4:"data";s:4:"5753";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:1;a:6:{s:4:"data";s:33:"\n		\n		\n		\n		\n				\n\n		\n		\n				\n			";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:4:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:34:"GDPR Compliance Tools in WordPress";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:70:"https://wordpress.org/news/2018/04/gdpr-compliance-tools-in-wordpress/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 12 Apr 2018 20:11:50 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:8:"Features";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"https://wordpress.org/news/?p=5728";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:188:"GDPR compliance is an important consideration for all WordPress websites. The GDPR Compliance team is looking for help to test the privacy tools that are currently being developed in core.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:10:"Andrew Ozz";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:3914:"<p>GDPR compliance is an important consideration for all WordPress websites. The GDPR Compliance team is looking for help to test the privacy tools that are currently being developed in core. </p>\n\n<h2>What is GDPR?</h2>\n\n<p>GDPR stands for <a href="https://en.wikipedia.org/wiki/General_Data_Protection_Regulation">General Data Protection Regulation</a> and is intended to strengthen and unify data protection for all individuals within the European Union. Its primary aim is to give control back to the EU residents over their personal data. <br /></p>\n\n<p>Why the urgency? Although the GDPR was introduced two years ago, it becomes  enforceable starting May 25, 2018.</p>\n\n<h2>Make WordPress GDPR Compliance Team</h2>\n\n<p>Currently, the GDPR Compliance Team understands that helping WordPress-based sites become compliant is a large and ongoing task. The team is focusing on creating a comprehensive core policy, plugin guidelines, privacy tools and documentation. All of this requires your help.<br /></p>\n\n<p>The GDPR Compliance Team is focusing on four main areas:</p>\n\n<ul>\n    <li>Add functionality to assist site owners in creating comprehensive privacy policies for their websites.</li>\n    <li>Create guidelines for plugins to become GDPR ready.</li>\n    <li>Add administration tools to facilitate compliance and encourage user privacy in general.</li>\n    <li>Add documentation to educate site owners on privacy, the main GDPR compliance requirements, and on how to use the new privacy tools.</li>\n</ul>\n\n<h2>Don’t we already have a privacy policy?</h2>\n\n<p>Yes and no. That said, The GDPR puts tighter guidelines and restrictions. Though we have many plugins that create privacy pages, we need means to generate a unified, comprehensive privacy policy. We will need tools for users to easily come into compliance.<br /></p>\n\n<p>Site owners will be able to create GDPR compliant privacy policy in three steps:</p>\n\n<ol>\n    <li>Adding a dedicated page for the policy.<br /></li>\n    <li>Adding privacy information from plugins.</li>\n    <li>Reviewing and publishing the policy.</li>\n</ol>\n\n<p>A new &#8220;postbox&#8221; will be added to the Edit Page screen when editing the policy. All plugins that collect or store user data will be able to add privacy information there. In addition it will alert the site owners when any privacy information changes after a plugin is activated, deactivated, or updated.<br /></p>\n\n<p>There is a new functionality to confirm user requests by email address. It is intended for site owners to be able to verify requests from users for displaying, downloading, or anonymizing of personal data.<br /></p>\n\n<p>A new &#8220;Privacy&#8221; page is added under the &#8220;Tools&#8221; menu. It will display new, confirmed requests from users, as well as already fulfilled requests. It will also contain the tools for exporting and anonymizing of personal data and for requesting email confirmation to avoid abuse attempts.<br /></p>\n\n<p>New section on privacy will be added to the <a href="https://developer.wordpress.org/plugins/">Plugin Handbook</a>. It will contain some general information on user privacy, what a plugin should do to be compliant, and also tips and examples on how to use the new privacy related functionality in WordPress.<br /></p>\n\n<p>The new privacy tools are scheduled for release at the end of April or beginning of May 2018.</p>\n\n<h2>How can you get involved?</h2>\n\n<p>We would love to have your help. The first step is awareness and education. For more information about the upcoming privacy tools see ﻿<a href="https://make.wordpress.org/core/2018/03/28/roadmap-tools-for-gdpr-compliance/">the roadmap</a>.</p>\n\n<p>If you would like to get involved in building WordPress Core and testing the new privacy tools, please join the #gdpr-compliance channel in the <a href="https://make.wordpress.org/chat/">Make WordPress</a> Slack group.</p>\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:30:"com-wordpress:feed-additions:1";a:1:{s:7:"post-id";a:1:{i:0;a:5:{s:4:"data";s:4:"5728";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:2;a:6:{s:4:"data";s:36:"\n		\n		\n		\n		\n				\n		\n\n		\n		\n				\n			";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:4:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:48:"WordPress 4.9.5 Security and Maintenance Release";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:84:"https://wordpress.org/news/2018/04/wordpress-4-9-5-security-and-maintenance-release/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 03 Apr 2018 19:56:54 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:2:{i:0;a:5:{s:4:"data";s:8:"Releases";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:3:"4.9";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"https://wordpress.org/news/?p=5645";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:376:"WordPress 4.9.5 is now available. This is a security and maintenance release for all versions since WordPress 3.7. We strongly encourage you to update your sites immediately. WordPress versions 4.9.4 and earlier are affected by three security issues. As part of the core team&#x27;s ongoing commitment to security hardening, the following fixes have been implemented [&#8230;]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:17:"Aaron D. Campbell";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:6347:"<p>WordPress 4.9.5 is now available. This is a <strong>security and maintenance release</strong> for all versions since WordPress 3.7. We strongly encourage you to update your sites immediately.</p>\n\n<p>WordPress versions 4.9.4 and earlier are affected by three security issues. As part of the core team&#x27;s ongoing commitment to security hardening, the following fixes have been implemented in 4.9.5:</p>\n\n<ol>\n    <li>Don&#x27;t treat <code>localhost</code> as same host by default.</li>\n    <li>Use safe redirects when redirecting the login page if SSL is forced.</li>\n    <li>Make sure the version string is correctly escaped for use in generator tags.</li>\n</ol>\n\n<p>Thank you to the reporters of these issues for practicing <a href="https://make.wordpress.org/core/handbook/testing/reporting-security-vulnerabilities/">﻿coordinated security disclosure</a>: <a href="https://profiles.wordpress.org/xknown">xknown</a> of the WordPress Security Team, <a href="https://hackerone.com/nitstorm">Nitin Venkatesh (nitstorm)</a>, and <a href="https://twitter.com/voldemortensen">Garth Mortensen</a> of the WordPress Security Team.</p>\n\n<p>Twenty-five other bugs were fixed in WordPress 4.9.5. Particularly of note were:</p>\n\n<ul>\n    <li>The previous styles on caption shortcodes have been restored.</li>\n    <li>Cropping on touch screen devices is now supported.</li>\n    <li>A variety of strings such as error messages have been updated for better clarity.</li>\n    <li>The position of an attachment placeholder during uploads has been fixed.</li>\n    <li>Custom nonce functionality in the REST API JavaScript client has been made consistent throughout the code base.</li>\n    <li>Improved compatibility with PHP 7.2.</li>\n</ul>\n\n<p><a href="https://make.wordpress.org/core/2018/04/03/wordpress-4-9-5/">This post has more information about all of the issues fixed in 4.9.5 if you&#x27;d like to learn more</a>.</p>\n\n<p><a href="https://wordpress.org/download/">Download WordPress 4.9.5</a> or venture over to Dashboard → Updates and click &quot;Update Now.&quot; Sites that support automatic background updates are already beginning to update automatically.</p>\n\n<p>Thank you to everyone who contributed to WordPress 4.9.5:</p>\n\n<p><a href="https://profiles.wordpress.org/1265578519-1/">1265578519</a>, <a href="https://profiles.wordpress.org/jorbin/">Aaron Jorbin</a>, <a href="https://profiles.wordpress.org/adamsilverstein/">Adam Silverstein</a>, <a href="https://profiles.wordpress.org/schlessera/">Alain Schlesser</a>, <a href="https://profiles.wordpress.org/alexgso/">alexgso</a>, <a href="https://profiles.wordpress.org/afercia/">Andrea Fercia</a>, <a href="https://profiles.wordpress.org/andrei0x309/">andrei0x309</a>, <a href="https://profiles.wordpress.org/antipole/">antipole</a>, <a href="https://profiles.wordpress.org/aranwer104/">Anwer AR</a>, <a href="https://profiles.wordpress.org/birgire/">Birgir Erlendsson (birgire)</a>, <a href="https://profiles.wordpress.org/blair-jersyer/">Blair jersyer</a>, <a href="https://profiles.wordpress.org/bandonrandon/">Brooke.</a>, <a href="https://profiles.wordpress.org/chetan200891/">Chetan Prajapati</a>, <a href="https://profiles.wordpress.org/codegrau/">codegrau</a>, <a href="https://profiles.wordpress.org/conner_bw/">conner_bw</a>, <a href="https://profiles.wordpress.org/davidakennedy/">David A. Kennedy</a>, <a href="https://profiles.wordpress.org/designsimply/">designsimply</a>, <a href="https://profiles.wordpress.org/dd32/">Dion Hulse</a>, <a href="https://profiles.wordpress.org/ocean90/">Dominik Schilling (ocean90)</a>, <a href="https://profiles.wordpress.org/electricfeet/">ElectricFeet</a>, <a href="https://profiles.wordpress.org/ericmeyer/">ericmeyer</a>, <a href="https://profiles.wordpress.org/fpcsjames/">FPCSJames</a>, <a href="https://profiles.wordpress.org/garrett-eclipse/">Garrett Hyder</a>, <a href="https://profiles.wordpress.org/pento/">Gary Pendergast</a>, <a href="https://profiles.wordpress.org/soulseekah/">Gennady Kovshenin</a>, <a href="https://profiles.wordpress.org/henrywright/">Henry Wright</a>, <a href="https://profiles.wordpress.org/audrasjb/">Jb Audras</a>, <a href="https://profiles.wordpress.org/jbpaul17/">Jeffrey Paul</a>, <a href="https://profiles.wordpress.org/jipmoors/">Jip Moors</a>, <a href="https://profiles.wordpress.org/joemcgill/">Joe McGill</a>, <a href="https://profiles.wordpress.org/joen/">Joen Asmussen</a>, <a href="https://profiles.wordpress.org/johnbillion/">John Blackbourn</a>, <a href="https://profiles.wordpress.org/johnpgreen/">johnpgreen</a>, <a href="https://profiles.wordpress.org/junaidkbr/">Junaid Ahmed</a>, <a href="https://profiles.wordpress.org/kristastevens/">kristastevens</a>, <a href="https://profiles.wordpress.org/obenland/">Konstantin Obenland</a>, <a href="https://profiles.wordpress.org/lakenh/">Laken Hafner</a>, <a href="https://profiles.wordpress.org/lancewillett/">Lance Willett</a>, <a href="https://profiles.wordpress.org/leemon/">leemon</a>, <a href="https://profiles.wordpress.org/melchoyce/">Mel Choyce</a>, <a href="https://profiles.wordpress.org/mikeschroder/">Mike Schroder</a>, <a href="https://profiles.wordpress.org/mrmadhat/">mrmadhat</a>, <a href="https://profiles.wordpress.org/nandorsky/">nandorsky</a>, <a href="https://profiles.wordpress.org/jainnidhi/">Nidhi Jain</a>, <a href="https://profiles.wordpress.org/swissspidy/">Pascal Birchler</a>, <a href="https://profiles.wordpress.org/qcmiao/">qcmiao</a>, <a href="https://profiles.wordpress.org/rachelbaker/">Rachel Baker</a>, <a href="https://profiles.wordpress.org/larrach/">Rachel Peter</a>, <a href="https://profiles.wordpress.org/ravanh/">RavanH</a>, <a href="https://profiles.wordpress.org/otto42/">Samuel Wood (Otto)</a>, <a href="https://profiles.wordpress.org/sebastienthivinfocom/">Sebastien SERRE</a>, <a href="https://profiles.wordpress.org/sergeybiryukov/">Sergey Biryukov</a>, <a href="https://profiles.wordpress.org/shital-patel/">Shital Marakana</a>, <a href="https://profiles.wordpress.org/netweb/">Stephen Edgar</a>, <a href="https://profiles.wordpress.org/karmatosed/">Tammie Lister</a>, <a href="https://profiles.wordpress.org/thomas-vitale/">Thomas Vitale</a>, <a href="https://profiles.wordpress.org/kwonye/">Will Kwon</a>, and <a href="https://profiles.wordpress.org/yahil/">Yahil Madakiya</a>.</p>\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:30:"com-wordpress:feed-additions:1";a:1:{s:7:"post-id";a:1:{i:0;a:5:{s:4:"data";s:4:"5645";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:3;a:6:{s:4:"data";s:33:"\n		\n		\n		\n		\n				\n\n		\n		\n				\n			";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:4:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:34:"The Month in WordPress: March 2018";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:69:"https://wordpress.org/news/2018/04/the-month-in-wordpress-march-2018/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 02 Apr 2018 08:00:22 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:18:"Month in WordPress";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"https://wordpress.org/news/?p=5632";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:317:"With a significant new milestone and some great improvements to WordPress as a platform, this month has been an important one for the project. Read on to find out more about what happened during the month of March. WordPress Now Powers 30% of the Internet Over the last 15 years, the popularity and usage of [&#8230;]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:15:"Hugh Lashbrooke";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:4328:"<p>With a significant new milestone and some great improvements to WordPress as a platform, this month has been an important one for the project. Read on to find out more about what happened during the month of March.\n\n</p>\n\n<hr class="wp-block-separator" />\n\n<h2>WordPress Now Powers 30% of the Internet</h2>\n\n<p>Over the last 15 years, the popularity and usage of WordPress has been steadily growing. That growth hit a significant milestone this month when <a href="https://w3techs.com/technologies/details/cm-wordpress/all/all">W3Techs reported that WordPress now powers over 30% of sites on the web.</a></p>\n\n<p>The percentage is determined based on W3Techs’ review of the top 10 million sites on the web, and it’s a strong indicator of the popularity and flexibility of WordPress as a platform.</p>\n\n<p>If you would like to have hand in helping to grow WordPress even further, <a href="https://make.wordpress.org/">you can get involved today</a>.</p>\n\n<h2>WordPress Jargon Glossary Goes Live</h2>\n\n<p>The WordPress Marketing Team has been hard at work lately putting together <a href="https://make.wordpress.org/marketing/2018/02/28/wordpress-jargon-glossary/">a comprehensive glossary of WordPress jargon</a> to help newcomers to the project become more easily acquainted with things.</p>\n\n<p>The glossary <a href="https://make.wordpress.org/marketing/2018/02/28/wordpress-jargon-glossary/">is available here</a> along with a downloadable PDF to make it simpler to reference offline.</p>\n\n<p>Publishing this resource is part of an overall effort to make WordPress more easily accessible for people who are not so familiar with the project. If you would like to assist the Marketing Team with this, you can follow <a href="https://make.wordpress.org/marketing/">the team blog</a> and join the #marketing channel in the<a href="https://make.wordpress.org/chat/"> Making WordPress Slack group</a>.</p>\n\n<h2>Focusing on Privacy in WordPress</h2>\n\n<p>Online privacy has been in the news this month for all the wrong reasons. It has reinforced the commitment of the GDPR Compliance Team to continue working on enhancements to WordPress core that allow site owners to improve privacy standards.</p>\n\n<p>The team&#x27;s work, and the wider privacy project, spans four areas: Adding tools which will allow site administrators to collect the information they need about their sites, examining the plugin guidelines with privacy in mind, enhancing privacy standards in WordPress core, and creating documentation focused on best practices in online privacy.</p>\n\n<p>To get involved with the project, you can <a href="https://make.wordpress.org/core/2018/03/28/roadmap-tools-for-gdpr-compliance/">view the roadmap</a>, <a href="https://make.wordpress.org/core/tag/gdpr-compliance/">follow the updates</a>, <a href="https://core.trac.wordpress.org/query?status=!closed&amp;keywords=~gdpr">submit patches</a>, and join the #gdpr-compliance channel in the <a href="https://make.wordpress.org/chat">Making WordPress Slack group</a>. Office hours are 15:00 UTC on Wednesdays.</p>\n\n<hr class="wp-block-separator" />\n\n<h2>Further Reading:</h2>\n\n<ul>\n    <li>The WordPress Foundation has published <a href="https://wordpressfoundation.org/2017-annual-report/">their annual report for 2017</a> showing just how much the community has grown over the last year.</li>\n    <li>The dates for WordCamp US <a href="https://2018.us.wordcamp.org/2018/03/13/announcing-wordcamp-us-2018/">have been announced</a> — this flagship WordCamp event will be held on 7-9 December this year in Nashville, Tennessee.</li>\n    <li>WordPress 4.9.5 is due for release on April 3 — <a href="https://make.wordpress.org/core/2018/03/21/wordpress-4-9-5-beta/">find out more here</a>.</li>\n    <li>Version 2.5 of Gutenberg, the new editor for WordPress core, <a href="https://make.wordpress.org/core/2018/03/29/whats-new-in-gutenberg-29th-march/">was released this month</a> with a host of great improvements.</li>\n    <li>WordSesh, a virtual WordPress conference, <a href="http://wordsesh.com/">is returning in July this year</a>.</li>\n</ul>\n\n<p><em>If you have a story we should consider including in the next “Month in WordPress” post, please <a href="https://make.wordpress.org/community/month-in-wordpress-submissions/">submit it here</a>.</em><br /></p>\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:30:"com-wordpress:feed-additions:1";a:1:{s:7:"post-id";a:1:{i:0;a:5:{s:4:"data";s:4:"5632";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:4;a:6:{s:4:"data";s:33:"\n		\n		\n		\n		\n				\n\n		\n		\n				\n			";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:4:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:37:"The Month in WordPress: February 2018";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:72:"https://wordpress.org/news/2018/03/the-month-in-wordpress-february-2018/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 01 Mar 2018 08:41:04 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:18:"Month in WordPress";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"https://wordpress.org/news/?p=5613";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:324:"Judging by the flurry of activity across the WordPress project throughout February, it looks like everyone is really getting into the swing of things for 2018. There have been a lot of interesting new developments, so read on to see what the community has been up to for the past month. WordPress 4.9.3 &#38; 4.9.4 [&#8230;]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:15:"Hugh Lashbrooke";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:5936:"<p>Judging by the flurry of activity across the WordPress project throughout February, it looks like everyone is really getting into the swing of things for 2018. There have been a lot of interesting new developments, so read on to see what the community has been up to for the past month.</p>\n\n<hr class="wp-block-separator" />\n\n<h2>WordPress 4.9.3 &amp; 4.9.4</h2>\n\n<p>Early in the month, <a href="https://wordpress.org/news/2018/02/wordpress-4-9-3-maintenance-release/">version 4.9.3 of WordPress was released</a>, including a number of important bug fixes. Unfortunately it introduced a bug that prevented many sites from automatically updating to future releases. To remedy this issue, <a href="https://wordpress.org/news/2018/02/wordpress-4-9-4-maintenance-release/">version 4.9.4 was released</a> the following day requiring many people to manually update their sites.</p>\n\n<p>While this kind of issue is always regrettable, the good thing is that it was fixed quickly, and that not all sites had updated to 4.9.3 yet, which meant they bypassed the bug in that version.</p>\n\n<p>You can find out more technical information about this issue <a href="https://make.wordpress.org/core/2018/02/06/wordpress-4-9-4-release-the-technical-details/">on the Core development blog</a>.</p>\n\n<h2>The WordCamp Incubator is Back</h2>\n\n<p>In 2016, the Global Community Team ran an experimental program to help spread WordPress to underserved areas by providing more significant organizing support for their first WordCamp event. This program was dubbed the WordCamp Incubator, and it was so successful in the three cities where it ran that <a href="https://wordpress.org/news/2018/02/wordcamp-incubator-2-0/">the program is back for 2018</a>.</p>\n\n<p>Right now, the Community Team is looking for cities to be a part of this year’s incubator by <a href="https://wordcampcentral.polldaddy.com/s/wordcamp-incubator-program-2018-city-application">taking applications</a>. Additionally, each incubator community will need an experienced WordCamp organizer to assist them as a co-lead organizer for their event — if that sounds interesting to you, then you can <a href="https://wordcampcentral.polldaddy.com/s/wordcamp-incubator-program-2018-co-lead-application">fill in the application form for co-leads</a>.</p>\n\n<p>You can find out further information about the WordCamp Incubator <a href="https://make.wordpress.org/community/2018/02/19/wordcamp-incubator-program-2018-announcement/">on the Community Team blog</a>.</p>\n\n<h2>WordPress Meetup Roundtables scheduled for March</h2>\n\n<p>In order to assist local WordPress meetup organizers with running their meetup groups, some members of the Community Team have organized <a href="https://make.wordpress.org/community/2018/02/23/wordpress-meetup-roundtables-scheduled-for-march/">weekly meetup roundtable discussions through the month of March</a>.</p>\n\n<p>These will be run as video chats at 16:00 UTC every Wednesday this month and will be a great place for meetup organizers to come together and help each other out with practical ideas and advice.</p>\n\n<p>If you are not already in the WordPress meetup program and would like to join, you can find out more information in <a href="https://make.wordpress.org/community/handbook/meetup-organizer/welcome/">the WordPress Meetup Organizer Handbook</a>.</p>\n\n<h2>GDPR Compliance in WordPress Core</h2>\n\n<p>The General Data Protection Regulation (GDPR) is an upcoming regulation that will affect all online services across Europe. In order to prepare for this, a working group has been formed to make sure that WordPress is compliant with the GDPR regulations.</p>\n\n<p>Aside from the fact that this will be a requirement for the project going forward, it will also have an important and significant impact on the privacy and security of WordPress as a whole. The working group has posted <a href="https://make.wordpress.org/core/2018/02/19/proposed-roadmap-tools-for-gdpr-compliance/">their proposed roadmap</a> for this project and it looks very promising.</p>\n\n<p>To get involved in building WordPress Core, jump into the #gdpr-compliance channel in the <a href="https://make.wordpress.org/chat/">Making WordPress Slack group</a>, and follow <a href="https://make.wordpress.org/core/">the Core team blog</a>.</p>\n\n<hr class="wp-block-separator" />\n\n<h2>Further Reading:</h2>\n\n<ul>\n    <li>WPShout published <a href="https://wpshout.com/complete-guide-wordpress-security/">a thorough guide to WordPress security</a>.</li>\n    <li>The Community Team has published interesting statistics from the WordCamp program in <a href="https://make.wordpress.org/community/2018/02/27/wordcamps-in-2016/">2016</a> and <a href="https://make.wordpress.org/community/2018/02/28/wordcamps-in-2017/">2017</a>.</li>\n    <li><a href="https://make.wordpress.org/community/2018/02/15/potential-addition-of-a-new-onboarding-team/">An intriguing proposal has been made</a> for a new ‘Onboarding’ team to be started in the WordPress project.</li>\n    <li>The new editing experience for WordPress, named Gutenberg, continues to be actively developed with <a href="https://make.wordpress.org/core/2018/02/16/whats-new-in-gutenberg-16th-february/">a feature-packed release</a> this past month.</li>\n    <li>The Advanced WordPress Facebook group <a href="https://www.youtube.com/watch?v=4vS_jR5-nIo">held an interview with WordPress co-founder, Matt Mullenweg</a> about the Gutenberg project.</li>\n    <li><a href="https://make.wordpress.org/meta/2018/02/27/two-factor-authentication-on-wp-org/">Two factor authentication is on its way to the WordPress.org network</a> — this will be a great improvement to the overall security of the project.</li>\n</ul>\n\n<p><em>If you have a story we should consider including in the next “Month in WordPress” post, please <a href="https://make.wordpress.org/community/month-in-wordpress-submissions/">submit it here</a>.</em></p>\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:30:"com-wordpress:feed-additions:1";a:1:{s:7:"post-id";a:1:{i:0;a:5:{s:4:"data";s:4:"5613";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:5;a:6:{s:4:"data";s:39:"\n		\n		\n		\n		\n				\n		\n		\n\n		\n		\n				\n			";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:4:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:22:"WordCamp Incubator 2.0";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:58:"https://wordpress.org/news/2018/02/wordcamp-incubator-2-0/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 21 Feb 2018 22:53:20 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:3:{i:0;a:5:{s:4:"data";s:9:"Community";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:6:"Events";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:2;a:5:{s:4:"data";s:8:"WordCamp";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"https://wordpress.org/news/?p=5577";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:343:"WordCamps are informal, community-organized events that are put together by a team of local WordPress users who have a passion for growing their communities. They are born out of active WordPress meetup groups that meet regularly and are able to host an annual WordCamp event. This has worked very well in many communities, with over [&#8230;]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:15:"Hugh Lashbrooke";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:2450:"<p><a href="https://central.wordcamp.org/">WordCamps</a> are informal, community-organized events that are put together by a team of local WordPress users who have a passion for growing their communities. They are born out of active WordPress meetup groups that meet regularly and are able to host an annual WordCamp event. This has worked very well in many communities, with over 120 WordCamps being hosted around the world in 2017.<br /></p>\n\n<p>Sometimes though, passionate and enthusiastic community members can’t pull together enough people in their community to make a WordCamp happen. To address this, we introduced <a href="https://wordpress.org/news/2016/02/experiment-wordcamp-incubator/">the WordCamp Incubator program</a> in 2016.<br /></p>\n\n<p>The goal of the incubator program is <strong>to help spread WordPress to underserved areas by providing more significant organizing support for their first WordCamp event.</strong> In 2016, members of <a href="https://make.wordpress.org/community/">the global community team</a> worked with volunteers in three cities — Denpasar, Harare and Medellín — giving direct, hands-on assistance in making local WordCamps possible. All three of these WordCamp incubators <a href="https://make.wordpress.org/community/2017/06/30/wordcamp-incubator-report/">were a great success</a>, so we&#x27;re bringing the incubator program back for 2018.<br /></p>\n\n<p>Where should the next WordCamp incubators be? If you have always wanted a WordCamp in your city but haven’t been able to get a community started, this is a great opportunity. We will be taking applications for the next few weeks, then will get in touch with everyone who applied to discuss the possibilities. We will announce the chosen cities by the end of March.<br /></p>\n\n<p><strong>To apply, </strong><a href="https://wordcampcentral.polldaddy.com/s/wordcamp-incubator-program-2018-city-application"><strong>fill in the application</strong></a><strong> by March 15, 2018.</strong> You don’t need to have any specific information handy, it’s just a form to let us know you’re interested. You can apply to nominate your city even if you don’t want to be the main organizer, but for this to work well we will need local liaisons and volunteers, so please only nominate cities where you live or work so that we have at least one local connection to begin.<br /></p>\n\n<p>We&#x27;re looking forward to hearing from you!<br /></p>\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:30:"com-wordpress:feed-additions:1";a:1:{s:7:"post-id";a:1:{i:0;a:5:{s:4:"data";s:4:"5577";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:6;a:6:{s:4:"data";s:36:"\n		\n		\n		\n		\n				\n		\n\n		\n		\n				\n			";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:4:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:35:"WordPress 4.9.4 Maintenance Release";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:71:"https://wordpress.org/news/2018/02/wordpress-4-9-4-maintenance-release/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 06 Feb 2018 16:17:55 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:2:{i:0;a:5:{s:4:"data";s:8:"Releases";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:3:"4.9";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"https://wordpress.org/news/?p=5559";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:350:"WordPress 4.9.4 is now available. This maintenance release fixes a severe bug in 4.9.3, which will cause sites that support automatic background updates to fail to update automatically, and will require action from you (or your host) for it to be updated to 4.9.4. Four years ago with WordPress 3.7 &#8220;Basie&#8221;, we added the ability [&#8230;]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:10:"Dion Hulse";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:1823:"<p>WordPress 4.9.4 is now available.</p>\n<p>This maintenance release fixes a severe bug in 4.9.3, which will cause sites that support automatic background updates to fail to update automatically, and will require action from you (or your host) for it to be updated to 4.9.4.</p>\n<p>Four years ago with <a href="https://wordpress.org/news/2013/10/basie/">WordPress 3.7 &#8220;Basie&#8221;</a>, we added the ability for WordPress to self-update, keeping your website secure and bug-free, even when you weren&#8217;t available to do it yourself. For four years it&#8217;s helped keep millions of installs updated with very few issues over that time. Unfortunately <a href="https://wordpress.org/news/2018/02/wordpress-4-9-3-maintenance-release/">yesterdays 4.9.3 release</a> contained a severe bug which was only discovered after release. The bug will cause WordPress to encounter an error when it attempts to update itself to WordPress 4.9.4, and will require an update to be performed through the WordPress dashboard or hosts update tools.</p>\n<p>WordPress managed hosting companies who install updates automatically for their customers can install the update as normal, and we&#8217;ll be working with other hosts to ensure that as many customers of theirs who can be automatically updated to WordPress 4.9.4 can be.</p>\n<p>For more technical details of the issue, we&#8217;ve <a href="https://make.wordpress.org/core/2018/02/06/wordpress-4-9-4-release-the-technical-details/">posted on our Core Development blog</a>. For a full list of changes, consult the <a href="https://core.trac.wordpress.org/query?status=closed&amp;milestone=4.9.4&amp;group=component">list of tickets</a>.</p>\n<p><a href="https://wordpress.org/download/">Download WordPress 4.9.4</a> or visit Dashboard → Updates and click “Update Now.”</p>\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:30:"com-wordpress:feed-additions:1";a:1:{s:7:"post-id";a:1:{i:0;a:5:{s:4:"data";s:4:"5559";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:7;a:6:{s:4:"data";s:33:"\n		\n		\n		\n		\n				\n\n		\n		\n				\n			";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:4:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:35:"WordPress 4.9.3 Maintenance Release";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:71:"https://wordpress.org/news/2018/02/wordpress-4-9-3-maintenance-release/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 05 Feb 2018 19:47:45 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:8:"Releases";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"https://wordpress.org/news/?p=5545";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:372:"WordPress 4.9.3 is now available. This maintenance release fixes 34 bugs in 4.9, including fixes for Customizer changesets, widgets, visual editor, and PHP 7.2 compatibility. For a full list of changes, consult the list of tickets and the changelog. Download WordPress 4.9.3 or visit Dashboard → Updates and click “Update Now.” Sites that support automatic [&#8230;]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:15:"Sergey Biryukov";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:3421:"<p>WordPress 4.9.3 is now available.</p>\n<p>This maintenance release fixes 34 bugs in 4.9, including fixes for Customizer changesets, widgets, visual editor, and PHP 7.2 compatibility. For a full list of changes, consult the <a href="https://core.trac.wordpress.org/query?status=closed&amp;milestone=4.9.3&amp;group=component">list of tickets</a> and the <a href="https://core.trac.wordpress.org/log/branches/4.9?rev=42630&amp;stop_rev=42521">changelog</a>.</p>\n<p><a href="https://wordpress.org/download/">Download WordPress 4.9.3</a> or visit Dashboard → Updates and click “Update Now.” Sites that support automatic background updates are already beginning to update automatically.</p>\n<p>Thank you to everyone who contributed to WordPress 4.9.3:</p>\n<p><a href="https://profiles.wordpress.org/jorbin/">Aaron Jorbin</a>, <a href="https://profiles.wordpress.org/abdullahramzan/">abdullahramzan</a>, <a href="https://profiles.wordpress.org/adamsilverstein/">Adam Silverstein</a>, <a href="https://profiles.wordpress.org/afercia/">Andrea Fercia</a>, <a href="https://profiles.wordpress.org/andreiglingeanu/">andreiglingeanu</a>, <a href="https://profiles.wordpress.org/azaozz/">Andrew Ozz</a>, <a href="https://profiles.wordpress.org/bpayton/">Brandon Payton</a>, <a href="https://profiles.wordpress.org/chetan200891/">Chetan Prajapati</a>, <a href="https://profiles.wordpress.org/coleh/">coleh</a>, <a href="https://profiles.wordpress.org/darko-a7/">Darko A7</a>, <a href="https://profiles.wordpress.org/desertsnowman/">David Cramer</a>, <a href="https://profiles.wordpress.org/dlh/">David Herrera</a>, <a href="https://profiles.wordpress.org/dd32/">Dion Hulse</a>, <a href="https://profiles.wordpress.org/flixos90/">Felix Arntz</a>, <a href="https://profiles.wordpress.org/frank-klein/">Frank Klein</a>, <a href="https://profiles.wordpress.org/pento/">Gary Pendergast</a>, <a href="https://profiles.wordpress.org/audrasjb/">Jb Audras</a>, <a href="https://profiles.wordpress.org/jbpaul17/">Jeffrey Paul</a>, <a href="https://profiles.wordpress.org/lizkarkoski/">lizkarkoski</a>, <a href="https://profiles.wordpress.org/clorith/">Marius L. J.</a>, <a href="https://profiles.wordpress.org/mattyrob/">mattyrob</a>, <a href="https://profiles.wordpress.org/monikarao/">Monika Rao</a>, <a href="https://profiles.wordpress.org/munyagu/">munyagu</a>, <a href="https://profiles.wordpress.org/ndavison/">ndavison</a>, <a href="https://profiles.wordpress.org/nickmomrik/">Nick Momrik</a>, <a href="https://profiles.wordpress.org/peterwilsoncc/">Peter Wilson</a>, <a href="https://profiles.wordpress.org/rachelbaker/">Rachel Baker</a>, <a href="https://profiles.wordpress.org/rishishah/">rishishah</a>, <a href="https://profiles.wordpress.org/othellobloke/">Ryan Paul</a>, <a href="https://profiles.wordpress.org/sasiddiqui/">Sami Ahmed Siddiqui</a>, <a href="https://profiles.wordpress.org/sayedwp/">Sayed Taqui</a>, <a href="https://profiles.wordpress.org/seanchayes/">Sean Hayes</a>, <a href="https://profiles.wordpress.org/sergeybiryukov/">Sergey Biryukov</a>, <a href="https://profiles.wordpress.org/shooper/">Shawn Hooper</a>, <a href="https://profiles.wordpress.org/netweb/">Stephen Edgar</a>, <a href="https://profiles.wordpress.org/manikmist09/">Sultan Nasir Uddin</a>, <a href="https://profiles.wordpress.org/tigertech/">tigertech</a>, and <a href="https://profiles.wordpress.org/westonruter/">Weston Ruter</a>.</p>\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:30:"com-wordpress:feed-additions:1";a:1:{s:7:"post-id";a:1:{i:0;a:5:{s:4:"data";s:4:"5545";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:8;a:6:{s:4:"data";s:33:"\n		\n		\n		\n		\n				\n\n		\n		\n				\n			";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:4:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:36:"The Month in WordPress: January 2018";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:71:"https://wordpress.org/news/2018/02/the-month-in-wordpress-january-2018/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 02 Feb 2018 08:10:07 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:18:"Month in WordPress";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"https://wordpress.org/news/?p=5541";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:339:"Things got off to a gradual start in 2018 with momentum starting to pick up over the course of the month. There were some notable developments in January, including a new point release and work being done on other important areas of the WordPress project. WordPress 4.9.2 Security and Maintenance Release On January 16, WordPress [&#8230;]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:15:"Hugh Lashbrooke";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:3840:"<p>Things got off to a gradual start in 2018 with momentum starting to pick up over the course of the month. There were some notable developments in January, including a new point release and work being done on other important areas of the WordPress project.</p>\n\n<hr class="wp-block-separator" />\n\n<h2>WordPress 4.9.2 Security and Maintenance Release</h2>\n\n<p>On January 16, <a href="https://wordpress.org/news/2018/01/wordpress-4-9-2-security-and-maintenance-release/">WordPress 4.9.2 was released</a> to fix an important security issue with the media player, as well as a number of other smaller bugs. This release goes a long way to smoothing out the 4.9 release cycle with the next point release, v4.9.3, <a href="https://make.wordpress.org/core/2018/01/31/wordpress-4-9-3-release-pushed-to-february-5th/">due in early February</a>.</p>\n\n<p>To get involved in building WordPress Core, jump into the #core channel in the<a href="https://make.wordpress.org/chat/"> Making WordPress Slack group</a>, and follow<a href="https://make.wordpress.org/core/"> the Core team blog</a>.</p>\n\n<h2>Updated Plugin Directory Guidelines</h2>\n\n<p>At the end of 2017, <a href="https://developer.wordpress.org/plugins/wordpress-org/detailed-plugin-guidelines/">the guidelines for the Plugin Directory</a> received a significant update to make them clearer and expanded to address certain situations. This does not necessarily make these guidelines complete, but rather more user-friendly and practical; they govern how developers build plugins for the Plugin Directory, so they need to evolve with the global community that the Directory serves.</p>\n\n<p>If you would like to contribute to these guidelines, you can make a pull request to <a href="https://github.com/WordPress/wporg-plugin-guidelines">the GitHub repository</a> or email <a href="mailto:plugins@wordpress.org">plugins@wordpress.org</a>. You can also jump into the #pluginreview channel in the<a href="https://make.wordpress.org/chat/"> Making WordPress Slack group</a>.</p>\n\n<hr class="wp-block-separator" />\n\n<h2>Further Reading:</h2>\n\n<ul>\n    <li>Near the end of last year a lot of work was put into improving the standards in the WordPress core codebase and now <a href="https://make.wordpress.org/core/2017/11/30/wordpress-php-now-mostly-conforms-to-wordpress-coding-standards/">the entire platform is at nearly 100% compliance with the WordPress coding standards</a>.</li>\n    <li>Gutenberg, the new editor coming to WordPress core in the next major release, <a href="https://make.wordpress.org/core/2018/01/25/whats-new-in-gutenberg-25th-january/">was updated to v2.1 this month</a> with some great usability and technical improvements.</li>\n    <li>The Global Community Team is <a href="https://make.wordpress.org/community/2018/01/16/2018-goals-for-the-global-community-team-suggestions-time/">taking suggestions for the goals of the Community program in 2018</a>.</li>\n    <li><a href="https://online.wpcampus.org/">WPCampus Online</a>, a digital conference focused on WordPress in higher education, took place on January 30. The videos of the event sessions will be online soon.</li>\n    <li>A WordPress community member <a href="https://wptavern.com/new-toolkit-simplifies-the-process-of-creating-gutenberg-blocks">has released a toolkit</a> to help developers build blocks for Gutenberg.</li>\n    <li>The community team that works to improve the WordPress hosting experience is relatively young, but <a href="https://make.wordpress.org/hosting/2018/01/25/hosting-meeting-notes-january-10-2018/">they have been making some great progress recently</a>.</li>\n</ul>\n\n<p><em>If you have a story we should consider including in the next “Month in WordPress” post, please <a href="https://make.wordpress.org/community/month-in-wordpress-submissions/">submit it here</a>.</em></p>\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:30:"com-wordpress:feed-additions:1";a:1:{s:7:"post-id";a:1:{i:0;a:5:{s:4:"data";s:4:"5541";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:9;a:6:{s:4:"data";s:39:"\n		\n		\n		\n		\n				\n		\n		\n\n		\n		\n				\n			";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:4:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:48:"WordPress 4.9.2 Security and Maintenance Release";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:84:"https://wordpress.org/news/2018/01/wordpress-4-9-2-security-and-maintenance-release/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 16 Jan 2018 23:00:14 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:3:{i:0;a:5:{s:4:"data";s:8:"Releases";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:8:"Security";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:2;a:5:{s:4:"data";s:3:"4.9";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"https://wordpress.org/news/?p=5376";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:360:"WordPress 4.9.2 is now available. This is a security and maintenance release for all versions since WordPress 3.7﻿. We strongly encourage you to update your sites immediately. An XSS vulnerability was discovered in the Flash fallback files in MediaElement, a library that is included with WordPress. Because the Flash files are no longer needed for [&#8230;]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:8:"Ian Dunn";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:3946:"<p>WordPress 4.9.2 is now available. This is a <strong>security and maintenance release</strong> for all versions since WordPress 3.7﻿. We strongly encourage you to update your sites immediately.</p>\n\n<p>An XSS vulnerability was discovered in the Flash fallback files in MediaElement, a library that is included with WordPress. Because the Flash files are no longer needed for most use cases, they have been removed from WordPress.</p>\n\n<p>MediaElement has released a new version that contains a fix for the bug, and <a href="https://wordpress.org/plugins/mediaelement-flash-fallbacks/">a WordPress plugin containing the fixed files</a> is available in the plugin repository.</p>\n\n<p>Thank you to the reporters of this issue for practicing <a href="https://make.wordpress.org/core/handbook/testing/reporting-security-vulnerabilities/">responsible security disclosure</a>: <a href="https://opnsec.com">Enguerran Gillier</a> and <a href="https://widiz.com/">Widiz﻿</a>.</p>\n\n<p>21 other bugs were fixed in WordPress 4.9.2. Particularly of note were:</p>\n\n<ul>\n    <li>JavaScript errors that prevented saving posts in Firefox have been fixed.</li>\n    <li>The previous taxonomy-agnostic behavior of <code>get_category_link()</code> and <code>category_description()</code> was restored.</li>\n    <li>Switching themes will now attempt to restore previous widget assignments, even when there are no sidebars to map.<br /></li>\n</ul>\n\n<p>The Codex has <a href="https://codex.wordpress.org/Version_4.9.2">more information about all of the issues fixed in 4.9.2</a>, if you&#x27;d like to learn more.</p>\n\n<p>﻿<a href="https://wordpress.org/download/"></a><a href="https://wordpress.org/download/">Download WordPress 4.9.2</a> or venture over to Dashboard → Updates and click &quot;Update Now.&quot; Sites that support automatic background updates are already beginning to update automatically.</p>\n\n<p>Thank you to everyone who contributed to WordPress 4.9.2:</p>\n\n<p><a href="https://profiles.wordpress.org/0x6f0/">0x6f0</a>, <a href="https://profiles.wordpress.org/jorbin/">Aaron Jorbin</a>, <a href="https://profiles.wordpress.org/afercia/">Andrea Fercia</a>, <a href="https://profiles.wordpress.org/aduth/">Andrew Duthie</a>, <a href="https://profiles.wordpress.org/azaozz/">Andrew Ozz</a>, <a href="https://profiles.wordpress.org/blobfolio/">Blobfolio</a>, <a href="https://profiles.wordpress.org/boonebgorges/">Boone Gorges</a>, <a href="https://profiles.wordpress.org/icaleb/">Caleb Burks</a>, <a href="https://profiles.wordpress.org/poena/">Carolina Nymark</a>, <a href="https://profiles.wordpress.org/chasewg/">chasewg</a>, <a href="https://profiles.wordpress.org/chetan200891/">Chetan Prajapati</a>, <a href="https://profiles.wordpress.org/dd32/">Dion Hulse</a>, <a href="https://profiles.wordpress.org/hardik-amipara/">Hardik Amipara</a>, <a href="https://profiles.wordpress.org/ionvv/">ionvv</a>, <a href="https://profiles.wordpress.org/jaswrks/">Jason Caldwell</a>, <a href="https://profiles.wordpress.org/jbpaul17/">Jeffrey Paul</a>, <a href="https://profiles.wordpress.org/jeremyfelt/">Jeremy Felt</a>, <a href="https://profiles.wordpress.org/joemcgill/">Joe McGill</a>, <a href="https://profiles.wordpress.org/johnschulz/">johnschulz</a>, <a href="https://profiles.wordpress.org/juiiee8487/">Juhi Patel</a>, <a href="https://profiles.wordpress.org/obenland/">Konstantin Obenland</a>, <a href="https://profiles.wordpress.org/markjaquith/">Mark Jaquith</a>, <a href="https://profiles.wordpress.org/rabmalin/">Nilambar Sharma</a>, <a href="https://profiles.wordpress.org/peterwilsoncc/">Peter Wilson</a>, <a href="https://profiles.wordpress.org/rachelbaker/">Rachel Baker</a>, <a href="https://profiles.wordpress.org/rinkuyadav999/">Rinku Y</a>, <a href="https://profiles.wordpress.org/sergeybiryukov/">Sergey Biryukov</a>, and <a href="https://profiles.wordpress.org/westonruter/">Weston Ruter</a>.﻿<strong></strong><br /></p>\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:30:"com-wordpress:feed-additions:1";a:1:{s:7:"post-id";a:1:{i:0;a:5:{s:4:"data";s:4:"5376";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}}}s:27:"http://www.w3.org/2005/Atom";a:1:{s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:3:{s:4:"href";s:32:"https://wordpress.org/news/feed/";s:3:"rel";s:4:"self";s:4:"type";s:19:"application/rss+xml";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:44:"http://purl.org/rss/1.0/modules/syndication/";a:2:{s:12:"updatePeriod";a:1:{i:0;a:5:{s:4:"data";s:9:"\n	hourly	";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:15:"updateFrequency";a:1:{i:0;a:5:{s:4:"data";s:4:"\n	1	";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:30:"com-wordpress:feed-additions:1";a:1:{s:4:"site";a:1:{i:0;a:5:{s:4:"data";s:8:"14607090";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}}}}}}}}s:4:"type";i:128;s:7:"headers";O:42:"Requests_Utility_CaseInsensitiveDictionary":1:{s:7:"\0*\0data";a:9:{s:6:"server";s:5:"nginx";s:4:"date";s:29:"Mon, 30 Apr 2018 06:23:56 GMT";s:12:"content-type";s:34:"application/rss+xml; charset=UTF-8";s:25:"strict-transport-security";s:11:"max-age=360";s:6:"x-olaf";s:3:"⛄";s:13:"last-modified";s:29:"Fri, 20 Apr 2018 21:07:41 GMT";s:4:"link";s:63:"<https://wordpress.org/news/wp-json/>; rel="https://api.w.org/"";s:15:"x-frame-options";s:10:"SAMEORIGIN";s:4:"x-nc";s:9:"HIT ord 2";}}s:5:"build";s:14:"20130910220210";}', 'no');
INSERT INTO `ops_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(128, '_transient_timeout_feed_mod_ac0b00fe65abe10e0c5b588f3ed8c7ca', '1525112634', 'no'),
(129, '_transient_feed_mod_ac0b00fe65abe10e0c5b588f3ed8c7ca', '1525069434', 'no'),
(130, '_transient_timeout_feed_d117b5738fbd35bd8c0391cda1f2b5d9', '1525112636', 'no');
INSERT INTO `ops_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(131, '_transient_feed_d117b5738fbd35bd8c0391cda1f2b5d9', 'a:4:{s:5:"child";a:1:{s:0:"";a:1:{s:3:"rss";a:1:{i:0;a:6:{s:4:"data";s:3:"\n\n\n";s:7:"attribs";a:1:{s:0:"";a:1:{s:7:"version";s:3:"2.0";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:1:{s:7:"channel";a:1:{i:0;a:6:{s:4:"data";s:61:"\n	\n	\n	\n	\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:16:"WordPress Planet";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:28:"http://planet.wordpress.org/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"language";a:1:{i:0;a:5:{s:4:"data";s:2:"en";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:47:"WordPress Planet - http://planet.wordpress.org/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"item";a:50:{i:0;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:28:"Matt: Rent-A-Family in Japan";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:22:"https://ma.tt/?p=48060";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:45:"https://ma.tt/2018/04/rent-a-family-in-japan/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:909:"<p><a href="http://elifbatuman.com/">Elif Batuman</a>, who was recently a Pulitzer finalist for her novel <a href="https://www.amazon.com/dp/B01HNJIJ3U/"><em>The Idiot</em></a>, has a stunning story in the <em>New Yorker</em> on <a href="https://www.newyorker.com/magazine/2018/04/30/japans-rent-a-family-industry">Japan’s Rent-a-Family Industry</a>, &#8220;People who are short on relatives can hire a husband, a mother, a grandson. The resulting relationships can be more real than you’d expect.&#8221;</p>\n\n<p>You think from the title it&#8217;s going to be one of those gee-whiz stories or vaguely condescending toward Japanese, but what follows is actually an incredibly poignant and powerful view of society through a lens I had never imagined before. It&#8217;s a <a href="https://longreads.com/">#longread</a> but I hope you take the time to sit with it this weekend. You may need a swordsman.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Sun, 29 Apr 2018 01:05:13 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:4:"Matt";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:1;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:47:"Post Status: The meta episode — Draft podcast";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:31:"https://poststatus.com/?p=45443";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:54:"https://poststatus.com/the-meta-episode-draft-podcast/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:2135:"<p>Welcome to the Post Status <a href="https://poststatus.com/category/draft">Draft podcast</a>, which you can find <a href="https://itunes.apple.com/us/podcast/post-status-draft-wordpress/id976403008">on iTunes</a>, <a href="https://play.google.com/music/m/Ih5egfxskgcec4qadr3f4zfpzzm?t=Post_Status__Draft_WordPress_Podcast">Google Play</a>, <a href="http://www.stitcher.com/podcast/krogsgard/post-status-draft-wordpress-podcast">Stitcher</a>, and <a href="http://simplecast.fm/podcasts/1061/rss">via RSS</a> for your favorite podcatcher. Post Status Draft is hosted by Brian Krogsgard and co-host Brian Richards.</p>\n<p>In this episode, Brian and Brian discuss meta data in WordPress, including the challenge of implementing data into new tools, such as the REST API and the Gutenberg editor.</p>\n<!--[if lt IE 9]><script>document.createElement(''audio'');</script><![endif]-->\n<a href="https://audio.simplecast.com/3a6227a2.mp3">https://audio.simplecast.com/3a6227a2.mp3</a>\n<p><a href="https://audio.simplecast.com/3a6227a2.mp3">Direct Download</a></p>\n<h3>Links</h3>\n<ul>\n<li><a href="https://make.wordpress.org/core/2018/04/26/completing-the-implementation-of-metadata-registration-with-the-rest-api/">Completing the implementation of meta data registration with the REST API</a></li>\n<li><a href="https://make.wordpress.org/core/2018/04/23/gutenberg-rest-api-and-you/">Gutenberg, REST API, and You</a></li>\n<li><a href="https://github.com/alleyinteractive/wordpress-fieldmanager">Fields Manager</a></li>\n<li><a href="https://www.advancedcustomfields.com/">Advanced Custom Fields</a></li>\n<li><a href="https://cmb2.io/">CMB2</a></li>\n</ul>\n<h3>Sponsor: Pippin&#8217;s Plugins</h3>\n<p>This episode is sponsored by Pippin’s Plugins. <a href="http://pippinsplugins.com/">Pippin’s Plugins</a> creates a suite of plugins that work great alone, or together. Whether you need to restrict content, sell downloads, or start an affiliate program, they’ve got you covered. For more information, check out their <a href="http://pippinsplugins.com/">website</a> and thank you to Pippin’s Plugins for being a Post Status partner.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 27 Apr 2018 20:10:29 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:14:"Katie Richards";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:2;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:52:"WPTavern: A Gutenberg Migration Guide for Developers";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=80527";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:63:"https://wptavern.com/a-gutenberg-migration-guide-for-developers";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1423:"<p>In order to help developers learn how to migrate from the classic editor to Gutenberg, Daniel Bachhuber has launched a <a href="https://github.com/danielbachhuber/gutenberg-migration-guide">Gutenberg Migration Guide</a>. Bachhuber is <a href="https://make.wordpress.org/core/2018/04/26/your-help-wanted-gutenberg-migration-guide/">seeking the community&#8217;s help</a> in identifying and filling a database to document all of the ways the classic editor can be customized.<br /></p>\n\n<blockquote class="wp-block-quote">\n    <p> Take a look through the <a href="https://github.com/danielbachhuber/gutenberg-migration-guide">Gutenberg Migration Guide</a>. For each action, filter, and so on, we’d like to document real-world examples of how they’ve been used. Then, for each of those real-world examples, identify how the feature might be replicated in Gutenberg.</p><cite>Daniel Bachhuber</cite></blockquote>\n\n<p>He uses the media_buttons action as an example. This action is commonly used to add a button to the top of the editor. Developers can accomplish the same task in Gutenberg <a href="https://github.com/danielbachhuber/gutenberg-migration-guide/blob/master/action-media-buttons.md">using the block inserter</a>. </p>\n\n<p>If you have any questions or suggestions, you&#8217;re encouraged to <a href="https://github.com/danielbachhuber/gutenberg-migration-guide/issues">create a new issue</a> on GitHub. </p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 27 Apr 2018 20:06:23 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Jeff Chandler";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:3;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:85:"WPTavern: WPWeekly Episode 313 – BuddyPress, Gutenberg, and An Upcoming Anniversary";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:58:"https://wptavern.com?p=80519&preview=true&preview_id=80519";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:90:"https://wptavern.com/wpweekly-episode-313-buddypress-gutenberg-and-an-upcoming-anniversary";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:2714:"<p>In this episode, <a href="http://jjj.me">John James Jacoby</a> and I start the show with a shout out to <a href="https://webdevstudios.com/2018/04/24/10-years-webdevstudios/">WebDevStudios</a>, a web development agency that&#8217;s celebrating its 10th year in business. We then cover what&#8217;s new in BuddyPress 3.0, why plugins hosted on WordPress.org can no longer claim legal compliance, and what to expect from 0.7 of the AMP for WordPress plugin. Last but not least, we share what&#8217;s new in Gutenberg 2.7 and explain why you shouldn&#8217;t edit content written in Gutenberg with the WordPress for iOS app just yet.</p>\n<h2>Stories Discussed:</h2>\n<p><a href="https://wordpress.org/news/2018/04/celebrate-the-wordpress-15th-anniversary-on-may-27/">Celebrate the WordPress 15th Anniversary on May 27</a><br />\n<a href="https://wptavern.com/plugins-hosted-on-wordpress-org-can-no-longer-guarantee-legal-compliance">Plugins Hosted on WordPress.org Can No Longer Guarantee Legal Compliance</a><br />\n<a href="https://wptavern.com/buddypress-3-0-beta-2-released">BuddyPress 3.0 Beta 2 Released</a><br />\n<a href="https://wptavern.com/wordpress-accessibility-team-is-seeking-contributors-for-its-handbook-project">WordPress Accessibility Team Is Seeking Contributors for Its Handbook Project</a><br />\n<a href="https://wptavern.com/amp-for-wordpress-0-7-rc-1-released">AMP for WordPress 0.7 RC 1 Released</a><br />\n<a href="https://wptavern.com/gutenberg-2-7-released-adds-ability-to-edit-permalinks">Gutenberg 2.7 Released, Adds Ability to Edit Permalinks</a><br />\n<a href="https://wptavern.com/wordpress-for-ios-and-gutenberg-dont-get-along">WordPress for iOS and Gutenberg Don’t Get Along</a><br />\n<a href="https://wptavern.com/talking-gutenberg-on-episode-eight-of-the-drunken-ux-podcast">Talking Gutenberg on Episode Eight of the Drunken UX Podcast</a></p>\n<h2>Picks of the Week:</h2>\n<p><a href="https://deliciousbrains.com/building-wordpress-plugins/">Delicious Brains explains how they create and release WordPress plugins.</a></p>\n<h2>WPWeekly Meta:</h2>\n<p><strong>Next Episode:</strong> Wednesday, May 2nd 3:00 P.M. Eastern</p>\n<p>Subscribe to <a href="https://itunes.apple.com/us/podcast/wordpress-weekly/id694849738">WordPress Weekly via Itunes</a></p>\n<p>Subscribe to <a href="https://www.wptavern.com/feed/podcast">WordPress Weekly via RSS</a></p>\n<p>Subscribe to <a href="http://www.stitcher.com/podcast/wordpress-weekly-podcast?refid=stpr">WordPress Weekly via Stitcher Radio</a></p>\n<p>Subscribe to <a href="https://play.google.com/music/listen?u=0#/ps/Ir3keivkvwwh24xy7qiymurwpbe">WordPress Weekly via Google Play</a></p>\n<p><strong>Listen To Episode #313:</strong><br />\n</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 27 Apr 2018 02:08:45 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Jeff Chandler";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:4;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:45:"WPTavern: AMP for WordPress 0.7 RC 1 Released";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=80438";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:56:"https://wptavern.com/amp-for-wordpress-0-7-rc-1-released";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1966:"<p>XWP, Automattic, and Google&#8217;s AMP team, <a href="https://make.xwp.co/2018/04/23/wordpress-amp-plugin-0-7-rc1/">has released</a> 0.7 Release Candidate 1 of the <a href="https://wordpress.org/plugins/amp/">AMP for WordPress plugin﻿</a>. Hinted at during <a href="https://www.youtube.com/watch?v=GGS-tKTXw4Y">AMP Conf 2018</a> earlier this year, 0.7 is a major release that contains significant new features.</p>\n\n<p>This release adds Native AMP support for all of the default widgets, embeds, and commenting. Notifications will be triggered for posts that contain content with validation errors or if you use a theme or plugin that adds invalid AMP markup.</p>\n\n<p>While earlier versions of AMP displayed content in a way that was different from a site&#8217;s theme, 0.7 creates a native experience. For example, if you visit the <a href="https://ampdemo.xwp.io/">AMP Conf WordPress Theme Demo site</a> on an iPhone 7, the site looks exactly the same. As you can see in the image below, you can&#8217;t tell it&#8217;s running AMP. <br /></p>\n\n<img />\n    AMP Conf Demo Theme\n\n\n<p>Before 0.7 is officially released, the development team is asking for users to put 0.7 RC 1 through its paces and <a href="https://github.com/Automattic/amp-wp/issues">report issues</a> on the project&#8217;s GitHub page. You can download the <a href="https://github.com/Automattic/amp-wp/releases/download/0.7-RC1/amp.zip">pre-release version here</a>.﻿<br />.﻿<br /></p>\n\n<p>For more information on the AMP project, <a href="https://wptavern.com/wpweekly-episode-309-all-amped-up">listen to episode 309 of WordPress Weekly</a> where I interviewed <a href="https://medinathoughts.com/">Alberto Medina</a>, Developer Advocate working with the Web Content Ecosystems Team at Google, and <a href="https://weston.ruter.net/">Weston Ruter</a>, CTO of XWP. In this interview, we covered why the project was created, its future, and its potential impacts on the Open Web. </p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 25 Apr 2018 17:30:14 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Jeff Chandler";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:5;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:69:"WPTavern: How Delicious Brains Creates and Releases WordPress Plugins";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=80410";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:80:"https://wptavern.com/how-delicious-brains-creates-and-releases-wordpress-plugins";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:420:"<p>Delicious Brains have <a href="https://deliciousbrains.com/building-wordpress-plugins/">published the process</a> they use for creating and releasing WordPress plugins. The post covers development, brainstorming, reviewing, testing, and wire frames. The team also describes the products and services they use and the roles they play within their projects. How is their process different or similar to yours?<br /></p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 25 Apr 2018 16:32:05 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Jeff Chandler";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:6;a:6:{s:4:"data";s:11:"\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:25:"HeroPress: Second Careers";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://heropress.com/?p=2514";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:95:"https://heropress.com/second-careers/#utm_source=rss&utm_medium=rss&utm_campaign=second-careers";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:3381:"<img width="960" height="480" src="https://s20094.pcdn.co/wp-content/uploads/2016/05/042418-1024x512.jpg" class="attachment-large size-large wp-post-image" alt="Pull Quote: I''d rather solve the right problem with code that has room for improvement, than solve the wrong problem with perfect code." /><p>I&#8217;m always fascinated by people who find second careers when they&#8217;re at the top of their game in the first one. To be really excellent at something and then switch to something else altogether is fascinating to see. Very often those people bubble to the top of whatever career they venture into.</p>\n<p>This week&#8217;s HeroPress replay is titled &#8220;<a href="https://heropress.com/essays/moving-on-from-moving-on-stage/">Moving On From Moving On Stage</a>&#8221; by Karin Taliga. Karin is a dancer, and has had a rich and full career in that field. But the web called to her. Like the siren&#8217;s song, always keening &#8220;Come play here, it&#8217;s wonderful!&#8221;</p>\n<p>When someone recommended I contact Karin 2 years ago she was winding up her dancing career and moving into a full time career as a web developer. The whole concept of a second career sounds exciting. I&#8217;ve always thought of &#8220;career&#8221; as something you do your whole life. But Karin has more adult life ahead of her than behind her, and I&#8217;m super excited to see where she goes with it.</p>\n<p>Check out Karin&#8217;s original post from May of 2016.</p>\n<blockquote class="wp-embedded-content"><p><a href="https://heropress.com/essays/moving-on-from-moving-on-stage/">Moving On From Moving On Stage</a></p></blockquote>\n<p></p>\n<div class="rtsocial-container rtsocial-container-align-right rtsocial-horizontal"><div class="rtsocial-twitter-horizontal"><div class="rtsocial-twitter-horizontal-button"><a title="Tweet: Second Careers" class="rtsocial-twitter-button" href="https://twitter.com/share?text=Second%20Careers&via=heropress&url=https%3A%2F%2Fheropress.com%2Fsecond-careers%2F" rel="nofollow" target="_blank"></a></div></div><div class="rtsocial-fb-horizontal fb-light"><div class="rtsocial-fb-horizontal-button"><a title="Like: Second Careers" class="rtsocial-fb-button rtsocial-fb-like-light" href="https://www.facebook.com/sharer.php?u=https%3A%2F%2Fheropress.com%2Fsecond-careers%2F" rel="nofollow" target="_blank"></a></div></div><div class="rtsocial-linkedin-horizontal"><div class="rtsocial-linkedin-horizontal-button"><a class="rtsocial-linkedin-button" href="https://www.linkedin.com/shareArticle?mini=true&url=https%3A%2F%2Fheropress.com%2Fsecond-careers%2F&title=Second+Careers" rel="nofollow" target="_blank" title="Share: Second Careers"></a></div></div><div class="rtsocial-pinterest-horizontal"><div class="rtsocial-pinterest-horizontal-button"><a class="rtsocial-pinterest-button" href="https://pinterest.com/pin/create/button/?url=https://heropress.com/second-careers/&media=https://heropress.com/wp-content/uploads/2016/05/042418-150x150.jpg&description=Second Careers" rel="nofollow" target="_blank" title="Pin: Second Careers"></a></div></div><a rel="nofollow" class="perma-link" href="https://heropress.com/second-careers/" title="Second Careers"></a></div><p>The post <a rel="nofollow" href="https://heropress.com/second-careers/">Second Careers</a> appeared first on <a rel="nofollow" href="https://heropress.com">HeroPress</a>.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 25 Apr 2018 11:30:56 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:7;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:59:"WPTavern: WordPress for iOS and Gutenberg Don’t Get Along";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=80236";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:67:"https://wptavern.com/wordpress-for-ios-and-gutenberg-dont-get-along";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:3064:"<p>When it comes to editing and crafting content on the go, the <a href="https://apps.wordpress.com/mobile/">WordPress Mobile apps</a> are a good choice. The question is, how does the editor in the iOS app interact with content written in Gutenberg? Let&#8217;s find out.</p>\n\n<h2>Quick Edits Turn Into Lengthy, Frustrating Fixes<br /></h2>\n\n<p>For testing purposes, I used a simple scenario that many users may run into. I&#8217;ve written and published a post in Gutenberg using paragraph, unordered lists, and image blocks. I then used the WordPress for iOS mobile app to access the post, correct a typo, and save it. The goal is to see if content is affected by saving it in a different editor.<br /></p>\n\n<p>Here is what the content looks like written and published in Gutenberg.</p>\n\n<img />\n    Content Written and Published in Gutenberg\n\n\n<p>Here is what the post looks like in the iOS app. It displays what appears to be Comment shortcodes at the beginning of each paragraph.<br /></p>\n\n<img />\n    Gutenberg Content in WordPress for Ios\n\n\n<p>After correcting a typo and saving the changes, this is what happened to the post. As you can see, what was supposed to be a quick fix has turned into a lengthy process of fixing the entire article in Gutenberg.</p>\n\n<img />\n    Content Written in Gutenberg After Editing in the WordPress for iOS App\n\n\n<p>All of the content runs together as one giant block. To say that this is frustrating is an understatement, especially if you&#8217;re on the road and don&#8217;t have access to a desktop or tablet that can load the WordPress backend.  <br /></p>\n\n<p>Here is what the content looks like in Gutenberg after saving the edits in the iOS app. There are large gaps and a few of the blocks have warnings stating that they appear to have been modified externally.</p>\n\n<img />\n    Content in Gutenberg After It Was Edited in the WordPress for iOS App\n\n\n<p>Clicking the convert to block buttons turns the messages into blocks but it doesn&#8217;t return the formatting and in some cases, content goes missing. Before editing in the iOS app, this block contained a quote with a citation. Now it&#8217;s empty. <br /></p>\n\n<img />\n    Quote Block Is Missing Content\n\n\n<p>WordPress has post revisions so I was able to quickly restore the breaking changes introduced by the iOS app. But this user experience between Gutenberg and the WordPress for iOS app is a great example of how something so simple can easily turn into a perceived disaster by users and ultimately, tarnish the new editor&#8217;s reputation.</p>\n\n<p>Searching the Gutenberg repository on Github for iOS <a href="https://github.com/WordPress/gutenberg/issues?utf8=%E2%9C%93&q=is%3Aissue+is%3Aopen+ios">produces some results</a>, but none refer to the compatibility issues I experienced. </p>\n\n<p>I found out the hard way and will not be making any more changes to posts written in Gutenberg in the iOS app until compatibility between both editors exists. I recommend you don&#8217;t as well unless you want to fix a lot more than a typo. <br /></p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 24 Apr 2018 23:33:56 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Jeff Chandler";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:8;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:60:"Dev Blog: Celebrate the WordPress 15th Anniversary on May 27";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"https://wordpress.org/news/?p=5753";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:86:"https://wordpress.org/news/2018/04/celebrate-the-wordpress-15th-anniversary-on-may-27/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:3246:"<p>May 27, 2018 is the <strong>15th anniversary</strong> of the <a href="https://wordpress.org/news/2003/05/wordpress-now-available/">first WordPress release</a> <a href="https://wordpress.org/news/2003/05/wordpress-now-available/">﻿</a>— and we can&#8217;t wait to celebrate!</p>\n\n<img src="https://i1.wp.com/wordpress.org/news/files/2018/04/wp15_mark-1.png?resize=342%2C268&ssl=1" alt="" class="wp-image-5841" width="342" height="268" />\n\n<h2>Party time!</h2>\n\n<p>Join WordPress fans all over the world in celebrating the 15th Anniversary of WordPress by throwing your own party! Here&#8217;s how you can join in the fun:</p>\n\n<ol>\n    <li>Check the <a href="https://wp15.wordpress.net/about/">WordPress 15th Anniversary website</a> to see if there&#8217;s a party already planned for your town. If there is, RSVP for the party and invite your friends!<br /></li>\n    <li>If there isn&#8217;t, then pick a place to go where a bunch of people can be merry — a park, a pub, a backyard; any family-friendly venue will do!</li>\n    <li>List your party with <a href="https://www.meetup.com/pro/wordpress/">your local WordPress meetup group</a> (Don&#8217;t have a group? <a href="https://make.wordpress.org/community/handbook/meetup-organizer/welcome/#starting-a-new-meetup-com-group">Start one!</a>)  and then spread the word to other local meetups, tech groups, press, etc and get people to say they’ll come to your party.</li>\n    <li><a href="https://make.wordpress.org/community/handbook/meetup-organizer/event-formats/wordpress-15th-anniversary-celebrations/#request-wordpress-15th-anniversary-swag">Request</a> some special 15th anniversary WordPress swag (no later than April 27, please, so we have time to ship it to you).<br /></li>\n    <li>Have party attendees post photos, videos, and the like with the #WP15 hashtag, and <a href="https://wp15.wordpress.net/live/">check out the social media stream</a> to see how the rest of the world is sharing and celebrating.</li>\n</ol>\n\n<p>Don&#8217;t miss this chance to participate in a global celebration of WordPress!<br /></p>\n\n<h2>Special Swag</h2>\n\n<p>In honor of the 15th anniversary, we’ve added some <a href="https://mercantile.wordpress.org/product-category/wordpress-15/">special 15th anniversary items</a> in the swag store — you can use the offer code <strong>CELEBRATEWP15</strong> to take 15% off this (and any other WordPress swag you buy), all the way through the end of 2018!</p>\n\n<p>Keep checking the swag store, because we&#8217;ll be adding more swag over the next few weeks!</p>\n\n<h2>Share the fun</h2>\n\n<p>However you celebrate the WordPress 15th anniversary — with <a href="https://wp15.wordpress.net/about/">a party</a>, with <a href="https://wp15.wordpress.net/swag/">commemorative swag</a>, by <a href="https://wp15.wordpress.net/live/">telling the world</a> what WordPress means to you — remember to use the #WP15 hashtag to share it! And don&#8217;t forget to <a href="https://wp15.wordpress.net/live/">check the stream of WordPress 15th anniversary posts</a>.</p>\n\n<p>When <a href="https://venturebeat.com/2018/03/05/wordpress-now-powers-30-of-websites/">30% of the internet</a> has a reason to celebrate, you know it&#8217;s going to be great! </p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 20 Apr 2018 21:07:29 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:16:"Andrea Middleton";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:9;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:65:"WPTavern: Gutenberg 2.7 Released, Adds Ability to Edit Permalinks";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=80121";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:75:"https://wptavern.com/gutenberg-2-7-released-adds-ability-to-edit-permalinks";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1977:"<p>Gutenberg 2.7 <a href="https://wordpress.org/plugins/gutenberg/">is available</a> for testing and not only does it refine the visuals around block controls, it adds the highly requested ability to <a href="https://github.com/WordPress/gutenberg/pull/5756">edit permalinks</a>.</p>\n\n<img />\n    Editing Permalinks in Gutenberg 2.7\n\n\n<p>A new pagination block is available that adds a page break, allowing users to break posts into multiple pages. The block is located in the <strong>Blocks &#8211; Layout Elements</strong> section.</p>\n\n<p>There are a number of changes to the link insertion interface. Gutenberg 2.7 brings back the option to have links open in the same window.<br /></p>\n\n<img />\n    Toggle Determines Whether Links Open in a New Window\n\n\n<p>When editing linked text, the Unlink icon now stays in the toolbar instead of displaying within the link options modal. When adding links, there&#8217;s a URL suggestion tool similar to what&#8217;s available in WordPress&#8217; current editor.</p>\n\n<p>What will be welcomed news to plugin developers, the <a href="https://github.com/WordPress/gutenberg/pull/6031">PluginSidebar API</a> is ﻿exposed and considered final. According to the pull request, this change does the following.</p>\n\n<blockquote class="wp-block-quote">\n    <p>Refactors all the existing Sidebar components to share the same set components and removes duplicated custom CSS styles applied to <code>&lt;PluginSidebar /></code>. There are no changes to the public API of <code>&lt;PublicSidebar /></code> component, other than it is going to be available under <code>wp.editPost.PluginSidebar</code>.</p><cite>Grzegorz Ziółkowski<br /></cite></blockquote>\n\n<p>This release, like the others before it, has a changelog that&#8217;s a mile long. Please check out the <a href="https://make.wordpress.org/core/2018/04/18/whats-new-in-gutenberg-18th-april/">release post</a> for a detailed list of changes and links to issues on GitHub. <br /></p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 20 Apr 2018 03:05:51 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Jeff Chandler";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:10;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:87:"WPTavern: WordPress Accessibility Team Is Seeking Contributors for Its Handbook Project";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=80068";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:98:"https://wptavern.com/wordpress-accessibility-team-is-seeking-contributors-for-its-handbook-project";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1290:"<p>The <a href="https://make.wordpress.org/accessibility/">WordPress Accessibility team</a> is seeking contributors for its <a href="https://make.wordpress.org/accessibility/handbook/">handbook project</a>. It&#8217;s a collection of tips, resources, <a href="https://make.wordpress.org/accessibility/handbook/which-tools-can-i-use/">tools</a>, and <a href="https://make.wordpress.org/accessibility/handbook/best-practices/">best practices</a>. The goal is to educate users through summaries, articles, and reference materials.<br /></p>\n\n<p>The handbook was created after the accessibility team repeatedly noticed the same accessibility issues cropping up and not having a central place to send people to learn about them.</p>\n\n<p>The team is looking for people to review articles, discover resources to add to the handbook, and suggest topics to cover. If you&#8217;re interested in contributing, please join the #<a href="https://wordpress.slack.com/archives/C6PK2QCTY">accessibility-docs</a> channel on <a href="https://make.wordpress.org/chat/">Slack</a> where you can ask questions and learn more about the project.</p>\n\n<p>Also, consider following <a href="https://twitter.com/WPAccessibility">WPAccessibility</a> on Twitter to keep tabs on team projects and links to resources. </p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 20 Apr 2018 01:57:11 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Jeff Chandler";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:11;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:40:"WPTavern: BuddyPress 3.0 Beta 2 Released";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=79984";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:51:"https://wptavern.com/buddypress-3-0-beta-2-released";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1844:"<p>The BuddyPress development team <a href="https://buddypress.org/2018/04/buddypress-3-0-beta-1/">has released</a> Beta 2 of BuddyPress 3.0. BuddyPress 3.0 is a major release that contains some significant changes. A new template pack called Nouveau will replace the bp-legacy template packs introduced in BuddyPress 1.7.</p>\n\n<p>The new template pack has been refactored to be semantic, accessible, and use a new set of markup files. Loops, members, and activity areas now run under Backbone to provide a smoother experience. JavaScript has been rewritten to be more modular and have better structure. <br /></p>\n\n<img />\n    BuddyPress 3.0 Customizer Options\n\n\n<p>BuddyPress 3.0 utilizes the Customizer by providing options to manipulate the Nouveau template pack or the site itself. For example, you can modify a user&#8217;s navigation options from the frontend. There&#8217;s also an option to adjust the number of columns for the Members loop. </p>\n\n<p>There are <a href="https://buddypress.trac.wordpress.org/query?status=closed&milestone=3.0&page=2&col=id&col=summary&col=status&col=milestone&col=owner&col=type&col=priority&order=priority">138 tickets closed</a> in this release. In addition to what&#8217;s noted above, 3.0 will <a href="https://buddypress.trac.wordpress.org/ticket/7722">remove support for WordPress 4.3 and below</a> and BuddyPress functions for bbPress 1.x forums <a href="https://buddypress.trac.wordpress.org/ticket/6851">will be deprecated</a>.</p>\n\n<p>Considering the scope and breadth of changes in 3.0, users are highly encouraged to test <a href="https://buddypress.org/2018/04/buddypress-3-0-beta-1/">BuddyPress 3.0 Beta 2</a>. If you encounter any issues, please report them in the <a href="https://buddypress.org/support/forum/how-to/">Troubleshooting and How-to</a> section of the support forums. </p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 18 Apr 2018 22:55:30 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Jeff Chandler";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:12;a:6:{s:4:"data";s:11:"\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:41:"HeroPress: Where WordPress REALLY Matters";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://heropress.com/?p=2509";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:127:"https://heropress.com/where-wordpress-really-matters/#utm_source=rss&utm_medium=rss&utm_campaign=where-wordpress-really-matters";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:3591:"<img width="960" height="480" src="https://s20094.pcdn.co/wp-content/uploads/2016/03/041818-1024x512.jpg" class="attachment-large size-large wp-post-image" alt="Pull Quote: The silence is there. But it no longer scares me." /><p>A couple years ago I was given possibly the biggest gift the WordPress community has ever given me. The organizers of WordCamp Pune called me to speak, and the community sent me. It was an amazing experience that changed my life.</p>\n<p>While I was there I met Mahangu Weerasinghe, a wonderful man from Sri Lanka. He spoke about things that really really resonated with me. His talk was about linguistic accessibility to the Internet in Southeast Asia. Many people told me that English is enough to communicate to all of India, but Mahangu pointed out that MILLIONS of people in Southeast Asia cannot read or understand a single language on the web, let alone English.</p>\n<p>WordPress can change that, and that&#8217;s where WordPress really matters.  It&#8217;s wonderful that people around the world can make a living with it, and it&#8217;s wonderful that it gives creative outlet to so many, but <strong>important</strong> that WordPress can give global voice to those who have none.</p>\n<p>Mahangu felt for a long time that he had no voice. WordPress changed that for him, and now he&#8217;s using WordPress to change that for everyone.  He&#8217;s been hugely inspirational to me, and I hope he is for you as well.</p>\n<blockquote class="wp-embedded-content"><p><a href="https://heropress.com/essays/breaking-the-silence/">Breaking the Silence</a></p></blockquote>\n<p></p>\n<div class="rtsocial-container rtsocial-container-align-right rtsocial-horizontal"><div class="rtsocial-twitter-horizontal"><div class="rtsocial-twitter-horizontal-button"><a title="Tweet: Where WordPress REALLY Matters" class="rtsocial-twitter-button" href="https://twitter.com/share?text=Where%20WordPress%20REALLY%20Matters&via=heropress&url=https%3A%2F%2Fheropress.com%2Fwhere-wordpress-really-matters%2F" rel="nofollow" target="_blank"></a></div></div><div class="rtsocial-fb-horizontal fb-light"><div class="rtsocial-fb-horizontal-button"><a title="Like: Where WordPress REALLY Matters" class="rtsocial-fb-button rtsocial-fb-like-light" href="https://www.facebook.com/sharer.php?u=https%3A%2F%2Fheropress.com%2Fwhere-wordpress-really-matters%2F" rel="nofollow" target="_blank"></a></div></div><div class="rtsocial-linkedin-horizontal"><div class="rtsocial-linkedin-horizontal-button"><a class="rtsocial-linkedin-button" href="https://www.linkedin.com/shareArticle?mini=true&url=https%3A%2F%2Fheropress.com%2Fwhere-wordpress-really-matters%2F&title=Where+WordPress+REALLY+Matters" rel="nofollow" target="_blank" title="Share: Where WordPress REALLY Matters"></a></div></div><div class="rtsocial-pinterest-horizontal"><div class="rtsocial-pinterest-horizontal-button"><a class="rtsocial-pinterest-button" href="https://pinterest.com/pin/create/button/?url=https://heropress.com/where-wordpress-really-matters/&media=https://heropress.com/wp-content/uploads/2016/03/041818-150x150.jpg&description=Where WordPress REALLY Matters" rel="nofollow" target="_blank" title="Pin: Where WordPress REALLY Matters"></a></div></div><a rel="nofollow" class="perma-link" href="https://heropress.com/where-wordpress-really-matters/" title="Where WordPress REALLY Matters"></a></div><p>The post <a rel="nofollow" href="https://heropress.com/where-wordpress-really-matters/">Where WordPress REALLY Matters</a> appeared first on <a rel="nofollow" href="https://heropress.com">HeroPress</a>.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 18 Apr 2018 12:00:21 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:13;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:23:"Matt: Abstract Aluminum";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:22:"https://ma.tt/?p=48051";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:40:"https://ma.tt/2018/04/abstract-aluminum/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:346:"<p>You probably haven&#8217;t thought much about beer cans, Abstract Aluminum Space, the Midwest Premium, and how it all ties into Goldman Sachs, so you should read <a href="https://www.bloomberg.com/view/articles/2014-09-03/the-goldman-sachs-aluminum-conspiracy-lawsuit-is-over">how the Goldman Sachs aluminum conspiracy lawsuit is over</a>.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 17 Apr 2018 21:57:21 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:4:"Matt";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:14;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:33:"BuddyPress: BuddyPress 3.0 Beta 2";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:32:"https://buddypress.org/?p=272059";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:53:"https://buddypress.org/2018/04/buddypress-3-0-beta-1/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:3509:"<p>It&#8217;s with a huge amount of pleasure and excitement that we&#8217;re announcing the Beta 2 release of BP 3.0 today ready for testing and feedback.</p>\n<p>BuddyPress 3.0 will be a major milestone release for us and one we&#8217;re all really excited about, it&#8217;s been a long time coming but finally we are close to releasing the first template pack for BP, this is a completely new &#8216;theme&#8217; or set of template files and functionality designed to replace bp-legacy which has served us so well since it&#8217;s inception way back in the  major release of 1.7 where we introduced &#8216;Theme Compatibility&#8217;, and we&#8217;re all really eager for any feedback during these beta phases you may grab a copy of our beta1 release <a href="https://downloads.wordpress.org/plugin/buddypress.3.0.0-beta1.zip">here</a> to test with.</p>\n<p>Nouveau &#8211; as our new template pack has been named &#8211; provides an all new clean set of markup files, refactored from the ground up to be semantic and accessible. Styles are re-written and provided as Sass partials for developers if wanting to build out new packs. A lot of core functionality for components has been re-written and re-located to be sourced from include files by component in the template directory which allows even easier access to modify functions by overloading to a new theme or child theme. Our major loops, members, activity etc have been re-factored to run under Backbone for a smooth Ajax experience and indeed all the Javascript functionality is re-written to be far more modular than it was before and has a far better modern feel to it&#8217;s structuring.</p>\n<p>For the first time we have brought in the Customizer to provide user option choices and a range of layout configurations may be selected. In our initial offering we have provided various layout options for the main BP navigation elements  allowing for vertical navs or horizontal, tab effect where suitable. for the component loops such as members, Groups we provide an option to display in a grid layout &amp; at row quantity options or simply as a flat classic list layout.</p>\n<p>While we are really excited about Nouveau 3.0 also has many other improvements to offer and you can <a href="https://buddypress.trac.wordpress.org/query?status=closed&milestone=3.0&col=id&col=summary&col=status&col=milestone&col=owner&col=type&col=priority&order=priority">view a list of all closed tickets for 3.0</a></p>\n<p>As always your feedback and testing is an invaluable part of our releases, helping us to catch any last minute bugs.<br />\nYou can download the beta release for testing at <a href="https://downloads.wordpress.org/plugin/buddypress.3.0.0-beta1.zip">downloads.wordpress.org</a> and install on a local copy of WordPress ( please remember this is a beta release and should not be run on an active production site). Any issues found can be reported on our Trac by creating a <a href="https://buddypress.trac.wordpress.org/newticket">new ticket</a></p>\n<p>If you&#8217;re a developer comfortable with SVN you might like to checkout a development copy which you can do <a href="https://svn.buddypress.org/trunk">from this link</a> patches can be submitted to existing tickets or issues found reported on a new ticket.</p>\n<p>Further guidance on contributing to BuddyPress is covered on our <a href="https://codex.buddypress.org/participate-and-contribute/">Contributor guidelines page</a> in our <a href="https://codex.buddypress.org/">Codex</a></p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 17 Apr 2018 20:30:27 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:12:"Hugo Ashmore";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:15;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:70:"WPTavern: Talking Gutenberg on Episode Eight of the Drunken UX Podcast";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=79942";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:81:"https://wptavern.com/talking-gutenberg-on-episode-eight-of-the-drunken-ux-podcast";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:738:"<p>Last week, I had the pleasure of joining Michael Fienen and Aaron Hill, hosts of the Drunken UX podcast, to <a href="https://drunkenux.com/podcast/8-sweet-home-automattic-where-we-use-gutenberg/">discuss Gutenberg</a>. We covered a lot of topics, including, why Gutenberg was created, our experiences, its timeline, pros, cons, resources, our biggest concerns, and what developers and freelancers need to know.</p>\n\n<p>The show is one hour and thirty minutes in length. By the way, please don&#8217;t criticize my drink of choice.</p>\n\n\n    <blockquote class="wp-embedded-content"><a href="https://drunkenux.com/podcast/8-sweet-home-automattic-where-we-use-gutenberg/">#8: Sweet Home Automattic, Where We Use Gutenberg</a></blockquote>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 17 Apr 2018 00:14:21 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Jeff Chandler";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:16;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:82:"WPTavern: Plugins Hosted on WordPress.org Can No Longer Guarantee Legal Compliance";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=79884";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:93:"https://wptavern.com/plugins-hosted-on-wordpress-org-can-no-longer-guarantee-legal-compliance";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:2105:"<p>The plugin review team has amended <a href="https://developer.wordpress.org/plugins/wordpress-org/detailed-plugin-guidelines/#9-developers-and-their-plugins-must-not-do-anything-illegal-dishonest-or-morally-offensive">guideline number nine</a> which states, <em>developers and their plugins must not do anything illegal, dishonest, or morally offensive</em>, to include the following statement:</p>\n\n<ul>\n    <li>Implying that a plugin can create, provide, automate, or guarantee legal compliance</li>\n</ul>\n\n<p>Mika Epstein, a member of the WordPress.org plugin review team, <a href="https://make.wordpress.org/plugins/2018/04/12/legal-compliance-added-to-guidelines/">says</a> the change was made because plugins by themselves can not provide legal compliance. <br /></p>\n\n<blockquote class="wp-block-quote">\n    <p>Sadly, no plugin in and of itself can provide legal compliance. While a plugin can certainly <em>assist</em> in automating the steps on a compliance journey, or allow you to develop a workflow to solve the situation, they cannot protect a site administrator from mistakes or lack of compliance, nor can they protect site users from incorrect or incomplete legal compliance on the part of the web site.</p><cite>Mika Epstein</cite></blockquote>\n\n<p>Since sites can have any combination of WordPress plugins and themes activated, it&#8217;s nearly impossible for a single plugin to make sure they&#8217;re 100% legally compliant.</p>\n\n<p>Plugin developers affected by this change will be contacted by the review team and be asked to change their titles, descriptions, plugin header images, and or the text within the readme.</p>\n\n<p>Instead of claiming compliance, the team has published a <a href="https://developer.wordpress.org/plugins/wordpress-org/compliance-disclaimers/">frequently asked questions</a> document that recommends plugin authors explain how the plugin will assist in compliance. If you have any questions, please leave a comment on the <a href="https://make.wordpress.org/plugins/2018/04/12/legal-compliance-added-to-guidelines/">announcement post</a>. </p>\n\n<p></p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 16 Apr 2018 23:35:37 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Jeff Chandler";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:17;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:55:"Post Status: All about you(r privacy) — Draft podcast";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:31:"https://poststatus.com/?p=45249";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:60:"https://poststatus.com/all-about-your-privacy-draft-podcast/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:2485:"<p>Welcome to the Post Status <a href="https://poststatus.com/category/draft">Draft podcast</a>, which you can find <a href="https://itunes.apple.com/us/podcast/post-status-draft-wordpress/id976403008">on iTunes</a>, <a href="https://play.google.com/music/m/Ih5egfxskgcec4qadr3f4zfpzzm?t=Post_Status__Draft_WordPress_Podcast">Google Play</a>, <a href="http://www.stitcher.com/podcast/krogsgard/post-status-draft-wordpress-podcast">Stitcher</a>, and <a href="http://simplecast.fm/podcasts/1061/rss">via RSS</a> for your favorite podcatcher. Post Status Draft is hosted by Brian Krogsgard and co-host Brian Richards.</p>\n<p><span>In this episode, the two Brians discuss the current conversations and controversy surrounding data collection and visitor privacy on the web. The duo dig in to the General Data Protection Regulation (GDPR) and what it means for you both as site visitors and site owners and, in particular, how WordPress core and plugin authors are (or should be) responding to the new regulation. It’s a pretty deep topic with many implications and ramifications. Be sure to follow the episode links, too, so that you can be best informed and prepared for when GDPR goes into effect on May 25, 2018.</span></p>\n<p></p>\n<p>Links</p>\n<ul>\n<li><a href="https://www.cjr.org/tow_center_reports/understanding-general-data-protection-regulation.php">CJR report on understanding the General Data Protection Regulation</a></li>\n<li><a href="https://make.wordpress.org/core/2018/03/28/roadmap-tools-for-gdpr-compliance/">Core&#8217;s roadmap for GDPR compliance</a></li>\n<li><a href="https://core.trac.wordpress.org/query?status=!closed&keywords=~gdpr">Trac issues related to GDPR</a></li>\n<li><a href="https://pagely.com/blog/gdpr-wordpress-2018-resources/?mc_cid=a002d1fc74&mc_eid=58d2ea272a">Pagely&#8217;s GDPR guide</a></li>\n<li><a href="https://www.smashingmagazine.com/2018/02/gdpr-for-web-developers/?mc_cid=a002d1fc74&mc_eid=58d2ea272a">Heather Burns&#8217; detailed GDPR analysis in Smashing Magazine</a></li>\n</ul>\n<h3>Sponsor: Valet</h3>\n<p>This episode is sponsored by <a href="https://www.valet.io/">Valet</a>. Valet helps keep your clients happy &amp; coming back. They offer expert services and keep the websites they manage functioning flawlessly. They offer preventative care that provides peace of mind around the clock. For more information, check out <a href="https://www.valet.io/">their website</a> and thank you to Valet for being a Post Status partner.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 16 Apr 2018 12:56:30 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:14:"Katie Richards";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:18;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:27:"Matt: Russell’s Treadmill";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:22:"https://ma.tt/?p=48043";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:41:"https://ma.tt/2018/04/russells-treadmill/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:704:"<p>From <a href="https://www.amazon.com/dp/B015D3X0YG">Bertrand Russell&#8217;s A Conquest of Happiness</a>.</p>\n\n<blockquote class="wp-block-quote">\n    <p>It is very singular how little men seem to realize that they are not caught in the grip of a mechanism from which there is no escape, but that the treadmill is one upon which they remain merely because they have not noticed that it fails to take them up to a higher level.</p>\n</blockquote>\n\n<p>He also says later, &#8220;﻿There are two motives for reading a book: one, that you enjoy it; the other, that you can boast about it.&#8221; <img src="https://s.w.org/images/core/emoji/2.4/72x72/1f602.png" alt="😂" class="wp-smiley" /></p>\n\n<p></p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 13 Apr 2018 20:22:46 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:4:"Matt";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:19;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:44:"Dev Blog: GDPR Compliance Tools in WordPress";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"https://wordpress.org/news/?p=5728";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:70:"https://wordpress.org/news/2018/04/gdpr-compliance-tools-in-wordpress/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:3913:"<p>GDPR compliance is an important consideration for all WordPress websites. The GDPR Compliance team is looking for help to test the privacy tools that are currently being developed in core. </p>\n\n<h2>What is GDPR?</h2>\n\n<p>GDPR stands for <a href="https://en.wikipedia.org/wiki/General_Data_Protection_Regulation">General Data Protection Regulation</a> and is intended to strengthen and unify data protection for all individuals within the European Union. Its primary aim is to give control back to the EU residents over their personal data. <br /></p>\n\n<p>Why the urgency? Although the GDPR was introduced two years ago, it becomes  enforceable starting May 25, 2018.</p>\n\n<h2>Make WordPress GDPR Compliance Team</h2>\n\n<p>Currently, the GDPR Compliance Team understands that helping WordPress-based sites become compliant is a large and ongoing task. The team is focusing on creating a comprehensive core policy, plugin guidelines, privacy tools and documentation. All of this requires your help.<br /></p>\n\n<p>The GDPR Compliance Team is focusing on four main areas:</p>\n\n<ul>\n    <li>Add functionality to assist site owners in creating comprehensive privacy policies for their websites.</li>\n    <li>Create guidelines for plugins to become GDPR ready.</li>\n    <li>Add administration tools to facilitate compliance and encourage user privacy in general.</li>\n    <li>Add documentation to educate site owners on privacy, the main GDPR compliance requirements, and on how to use the new privacy tools.</li>\n</ul>\n\n<h2>Don’t we already have a privacy policy?</h2>\n\n<p>Yes and no. That said, The GDPR puts tighter guidelines and restrictions. Though we have many plugins that create privacy pages, we need means to generate a unified, comprehensive privacy policy. We will need tools for users to easily come into compliance.<br /></p>\n\n<p>Site owners will be able to create GDPR compliant privacy policy in three steps:</p>\n\n<ol>\n    <li>Adding a dedicated page for the policy.<br /></li>\n    <li>Adding privacy information from plugins.</li>\n    <li>Reviewing and publishing the policy.</li>\n</ol>\n\n<p>A new &#8220;postbox&#8221; will be added to the Edit Page screen when editing the policy. All plugins that collect or store user data will be able to add privacy information there. In addition it will alert the site owners when any privacy information changes after a plugin is activated, deactivated, or updated.<br /></p>\n\n<p>There is a new functionality to confirm user requests by email address. It is intended for site owners to be able to verify requests from users for displaying, downloading, or anonymizing of personal data.<br /></p>\n\n<p>A new &#8220;Privacy&#8221; page is added under the &#8220;Tools&#8221; menu. It will display new, confirmed requests from users, as well as already fulfilled requests. It will also contain the tools for exporting and anonymizing of personal data and for requesting email confirmation to avoid abuse attempts.<br /></p>\n\n<p>New section on privacy will be added to the <a href="https://developer.wordpress.org/plugins/">Plugin Handbook</a>. It will contain some general information on user privacy, what a plugin should do to be compliant, and also tips and examples on how to use the new privacy related functionality in WordPress.<br /></p>\n\n<p>The new privacy tools are scheduled for release at the end of April or beginning of May 2018.</p>\n\n<h2>How can you get involved?</h2>\n\n<p>We would love to have your help. The first step is awareness and education. For more information about the upcoming privacy tools see ﻿<a href="https://make.wordpress.org/core/2018/03/28/roadmap-tools-for-gdpr-compliance/">the roadmap</a>.</p>\n\n<p>If you would like to get involved in building WordPress Core and testing the new privacy tools, please join the #gdpr-compliance channel in the <a href="https://make.wordpress.org/chat/">Make WordPress</a> Slack group.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 12 Apr 2018 20:11:50 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:10:"Andrew Ozz";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:20;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:103:"WPTavern: WPWeekly Episode 312 – Dragon Drop, WordPress Accessibility Statement, and WooCommerce GDPR";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:58:"https://wptavern.com?p=79862&preview=true&preview_id=79862";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:108:"https://wptavern.com/wpweekly-episode-312-dragon-drop-wordpress-accessibility-statement-and-woocommerce-gdpr";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1888:"<p>In this episode, <a href="http://jjj.me">John James Jacoby</a> and I start the show by sharing our thoughts on Mark Zuckberberg&#8217;s congressional hearing. We then discuss what&#8217;s new in Gutenberg 2.6 and describe our user experience. We let you know what&#8217;s in WooCommerce 3.3.5 and discuss what the development team is doing to prepare for GDPR compliance.</p>\n<h2>Stories Discussed:</h2>\n<p><a href="https://wptavern.com/gutenberg-2-6-introduces-drag-and-drop-block-sorting">Gutenberg 2.6 Introduces Drag and Drop Block Sorting</a><br />\n<a href="https://wptavern.com/theme-review-changes-place-more-onus-onto-theme-authors">Theme Review Changes Place More Onus Onto Theme Authors</a><br />\n<a href="https://wordpress.org/about/accessibility/">WordPress Accessibility Statement</a><br />\n<a href="https://woocommerce.wordpress.com/2018/04/10/woocommerce-3-3-5-fix-release-notes/">WooCommerce 3.3.5 Released</a><br />\n<a href="https://woocommerce.wordpress.com/2018/04/10/how-were-tackling-gdpr-in-woocommerce-core/">How WooCommerce is tackling GDPR</a></p>\n<h2>Picks of the Week:</h2>\n<p><a href="https://atomicblocks.com/introducing-the-atomic-blocks-plugin-and-theme/">AtomBlocks by Mike McAlister</a></p>\n<h2>WPWeekly Meta:</h2>\n<p><strong>Next Episode:</strong> Wednesday, April 18th 3:00 P.M. Eastern</p>\n<p>Subscribe to <a href="https://itunes.apple.com/us/podcast/wordpress-weekly/id694849738">WordPress Weekly via Itunes</a></p>\n<p>Subscribe to <a href="https://www.wptavern.com/feed/podcast">WordPress Weekly via RSS</a></p>\n<p>Subscribe to <a href="http://www.stitcher.com/podcast/wordpress-weekly-podcast?refid=stpr">WordPress Weekly via Stitcher Radio</a></p>\n<p>Subscribe to <a href="https://play.google.com/music/listen?u=0#/ps/Ir3keivkvwwh24xy7qiymurwpbe">WordPress Weekly via Google Play</a></p>\n<p><strong>Listen To Episode #312:</strong><br />\n</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 12 Apr 2018 01:05:46 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Jeff Chandler";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:21;a:6:{s:4:"data";s:11:"\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:27:"HeroPress: Growing Up Rural";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://heropress.com/?p=2503";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:99:"https://heropress.com/growing-up-rural/#utm_source=rss&utm_medium=rss&utm_campaign=growing-up-rural";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:2947:"<img width="960" height="480" src="https://s20094.pcdn.co/wp-content/uploads/2015/10/ImpactForOthers-HeroPress-1024x512.jpg" class="attachment-large size-large wp-post-image" alt="Pull Quote: I hope and pray that in some small way I''ll be able to take what I''ve learned and make an impact for others." /><p>This week&#8217;s throwback essay is titled &#8220;<a href="https://heropress.com/essays/i-dont-know-anything-and-thats-ok/">I don&#8217;t know anything, and that&#8217;s ok</a>&#8220;. It was written back in 2015 by my friend Kyle. He and I grew up in similar circumstances; far from a hub of civilization, in a relatively economically depressed area, without the best education opportunities.</p>\n<p>Yet he and I both managed to find the web, dive in, and find home. We support our families and find joy in the work we produce. I&#8217;m not sure there&#8217;s a better way to live than that.</p>\n<p>Check out Kyle&#8217;s essay and let him know what you think.</p>\n<blockquote class="wp-embedded-content"><p><a href="https://heropress.com/essays/i-dont-know-anything-and-thats-ok/">I Don’t Know Anything and That’s OK</a></p></blockquote>\n<p></p>\n<div class="rtsocial-container rtsocial-container-align-right rtsocial-horizontal"><div class="rtsocial-twitter-horizontal"><div class="rtsocial-twitter-horizontal-button"><a title="Tweet: Growing Up Rural" class="rtsocial-twitter-button" href="https://twitter.com/share?text=Growing%20Up%20Rural&via=heropress&url=https%3A%2F%2Fheropress.com%2Fgrowing-up-rural%2F" rel="nofollow" target="_blank"></a></div></div><div class="rtsocial-fb-horizontal fb-light"><div class="rtsocial-fb-horizontal-button"><a title="Like: Growing Up Rural" class="rtsocial-fb-button rtsocial-fb-like-light" href="https://www.facebook.com/sharer.php?u=https%3A%2F%2Fheropress.com%2Fgrowing-up-rural%2F" rel="nofollow" target="_blank"></a></div></div><div class="rtsocial-linkedin-horizontal"><div class="rtsocial-linkedin-horizontal-button"><a class="rtsocial-linkedin-button" href="https://www.linkedin.com/shareArticle?mini=true&url=https%3A%2F%2Fheropress.com%2Fgrowing-up-rural%2F&title=Growing+Up+Rural" rel="nofollow" target="_blank" title="Share: Growing Up Rural"></a></div></div><div class="rtsocial-pinterest-horizontal"><div class="rtsocial-pinterest-horizontal-button"><a class="rtsocial-pinterest-button" href="https://pinterest.com/pin/create/button/?url=https://heropress.com/growing-up-rural/&media=https://heropress.com/wp-content/uploads/2015/10/ImpactForOthers-HeroPress-150x150.jpg&description=Growing Up Rural" rel="nofollow" target="_blank" title="Pin: Growing Up Rural"></a></div></div><a rel="nofollow" class="perma-link" href="https://heropress.com/growing-up-rural/" title="Growing Up Rural"></a></div><p>The post <a rel="nofollow" href="https://heropress.com/growing-up-rural/">Growing Up Rural</a> appeared first on <a rel="nofollow" href="https://heropress.com">HeroPress</a>.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 11 Apr 2018 12:15:27 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:22;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:65:"WPTavern: Theme Review Changes Place More Onus Onto Theme Authors";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=79771";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:76:"https://wptavern.com/theme-review-changes-place-more-onus-onto-theme-authors";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:2093:"<p>The WordPress Theme Review team has <a href="https://make.wordpress.org/themes/2018/04/09/changes-in-theme-review-process/">implemented changes</a> that simplify the process and places more responsibility onto theme authors. Theme reviewers now only need to check the following items to pass a theme.</p>\n\n<ul>\n    <li>Licensing</li>\n    <li>Malicious or egregious stuff</li>\n    <li>Content Creation</li>\n    <li>Security</li>\n</ul>\n\n<p>Although the bar to pass a theme is significantly lower, theme authors are still expected to follow the <a href="https://make.wordpress.org/themes/handbook/review/required/">required</a> and <a href="https://make.wordpress.org/themes/handbook/review/recommended/">recommended</a> requirements listed in the theme handbook.</p>\n\n<p>Moderators will check themes after they&#8217;ve gone live to make sure the author is following guidelines. If a moderator discovers any issues, a request will be made to the theme author to correct them. Failure to do so could lead to a temporary or permanent suspension.</p>\n\n<p>Justin Tadlock <a href="https://make.wordpress.org/themes/2018/04/09/changes-in-theme-review-process/#comment-43128">clarified</a> in the comments examples of egregious issues.</p>\n\n<ul>\n    <li>Illegal</li>\n    <li>Dishonest</li>\n    <li>Morally offensive</li>\n    <li>PHP Errors</li>\n</ul>\n\n<p>In the past two years, The Theme Review Team has battled the theme review queue with moderate success. <a href="https://wptavern.com/wordpress-theme-review-team-making-progress-on-clearing-out-1000-review-backlog">In early 2017</a>, the number of themes in the queue dropped below 200. Although there has been some work on automating the process, it&#8217;s largely reliant on humans.</p>\n\n<p>Even though it hasn&#8217;t been updated in more than a year, theme authors are highly encouraged to use the <a href="https://wordpress.org/plugins/theme-check/">Theme Check plugin</a> before submitting themes for review.</p>\n\n<p>With a simplified process to get a theme live, reviewers are hoping it will free them up to focus on larger projects.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 10 Apr 2018 23:45:23 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Jeff Chandler";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:23;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:62:"WPTavern: Gutenberg 2.6 Introduces Drag and Drop Block Sorting";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=79658";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:73:"https://wptavern.com/gutenberg-2-6-introduces-drag-and-drop-block-sorting";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1729:"<p><a href="https://wordpress.org/plugins/Gutenberg/">Gutenberg 2.6</a> is available for download and with it, comes a new way of sorting blocks. In addition to using up and down arrows, this version introduces the ability to sort blocks by <a href="https://github.com/WordPress/gutenberg/issues/6041">dragging and dropping</a>.</p>\n\n<p>If you hover the cursor over the up and down arrows on the left side of a block, you&#8217;ll see a hand icon. Simply click, hold, and drag the block up or down below or above the blue indicator.</p>\n\n\n    \n\n\n<p>In my limited testing, I found drag and drop to be hit or miss. Sometimes, when I try to drag and drop a block, the blue line doesn&#8217;t show up.</p>\n\n<p>The hand icon for the cursor is different for the top and bottom of the block. You can drag a block by hovering over the bottom of it but you can&#8217;t do it from the top.</p>\n\n<img />\n    Hovering the cursor at the top of the block\n\n\n<img />\n    Hovering the cursor at the bottom of the block\n\n\n<p>I also found it difficult to add a new block manually. For example, when I add a paragraph block, I don&#8217;t see the Plus icon to create a new block underneath it anymore.</p>\n\n<p>Pressing enter at the end of a paragraph creates a new Paragraph block automatically. But I don&#8217;t know how to transform it into an image block. I&#8217;ll need to do more testing to figure out what&#8217;s going on. <br /></p>\n\n<p>There&#8217;s a host of <a href="https://make.wordpress.org/core/2018/04/05/whats-new-in-gutenberg-5th-april/">other improvements and bug fixes </a>in this release, some of which I covered <a href="https://wptavern.com/an-update-to-my-gutenberg-experience">in this post</a>. ﻿<br />. ﻿<br /></p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 10 Apr 2018 00:09:58 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Jeff Chandler";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:24;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:49:"Post Status: Designing the news — Draft podcast";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:31:"https://poststatus.com/?p=45154";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:56:"https://poststatus.com/designing-the-news-draft-podcast/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1904:"<p>Welcome to the Post Status <a href="https://poststatus.com/category/draft">Draft podcast</a>, which you can find <a href="https://itunes.apple.com/us/podcast/post-status-draft-wordpress/id976403008">on iTunes</a>, <a href="https://play.google.com/music/m/Ih5egfxskgcec4qadr3f4zfpzzm?t=Post_Status__Draft_WordPress_Podcast">Google Play</a>, <a href="http://www.stitcher.com/podcast/krogsgard/post-status-draft-wordpress-podcast">Stitcher</a>, and <a href="http://simplecast.fm/podcasts/1061/rss">via RSS</a> for your favorite podcatcher. Post Status Draft is hosted by Brian Krogsgard and co-host Brian Richards.</p>\n<p><span>In this episode, Brian and Brian discuss a variety of news topics spanning design, development, and business. Tune in to learn about the history of WordPress and the web, the newest TechCrunch redesign, a WordCamp for WordCamp organizers, and more.</span></p>\n<p></p>\n<h3>Links</h3>\n<ul>\n<li><a href="https://zeen101.com/for-developers/leakypaywall/">Leaky Paywall</a></li>\n<li><a href="https://designintech.report/">2018 Design in Tech report</a></li>\n<li><a href="https://gutenberg.courses/development/">Gutenberg Development Course</a></li>\n<li><a href="https://techcrunch.com/2018/03/13/welcome-to-the-new-techcrunch/">TechCrunch redesign</a></li>\n<li><a href="https://thehistoryoftheweb.com/the-story-of-wordpress/">WordPress turns 15</a>, via History of the Web</li>\n<li><a href="https://make.wordpress.org/community/2018/04/03/want-to-help-organize-a-wordcamp-for-organizers/">Proposal for a WordCamp for WordCamp organizers </a></li>\n</ul>\n<h3>Sponsor: Gravity Forms</h3>\n<p><a href="http://www.gravityforms.com/?utm_source=post_status&utm_medium=banner&utm_campaign=ps_ads">Gravity Forms</a> makes the best web forms on the planet. Over a million WordPress sites are already using Gravity Forms. Is yours? Thanks to Gravity Forms for being a Post Status partner.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 09 Apr 2018 18:36:43 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:14:"Katie Richards";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:25;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:46:"WPTavern: An Update to My Gutenberg Experience";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=79564";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:57:"https://wptavern.com/an-update-to-my-gutenberg-experience";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:2508:"<p>Not long after I published <a href="https://wptavern.com/my-gutenberg-experience-thus-far">my experience with Gutenberg</a>, developers reached out to me to work on some of the issues I mentioned. Riad Benguella <a href="https://github.com/WordPress/gutenberg/pull/5902">figured out</a> why meta boxes were not collapsing or expanding.</p>\n\n<p>It turns out that some meta boxes depend on the post script which has a side effect of calling the window.postboxes.add_postbox_toggles( postType ) twice, causing meta boxes to break.</p>\n\n<p>Gutenberg 2.6 <a href="https://make.wordpress.org/core/2018/04/05/whats-new-in-gutenberg-5th-april/">released earlier this week</a>, fixes the issue and all meta boxes function properly again. This also fixes the issue I had with the Telegram for WordPress plugin. <br /></p>\n\n<p><a href="https://wordpress.org/plugins/public-post-preview/">Public Post Preview</a> still doesn&#8217;t work in Gutenberg but the plugin&#8217;s developer, Dominik Schilling, shared some experiments he has conducted with adding support for Gutenberg on Twitter.</p>\n\n\n    <blockquote class="twitter-tweet"><p lang="en" dir="ltr">Can''t wait for seeing Gutenberg in WordPress core. So many new possibilities. Even for Public Post Preview (<a href="https://t.co/Xsw9hugxKT">https://t.co/Xsw9hugxKT</a>). With just a few lines I was able to create this: <a href="https://t.co/fxyuBIMPOl">pic.twitter.com/fxyuBIMPOl</a></p>&mdash; Dominik Schilling <img src="https://s.w.org/images/core/emoji/2.4/72x72/1f30a.png" alt="🌊" class="wp-smiley" /> (@ocean90) <a href="https://twitter.com/ocean90/status/980420608822562816?ref_src=twsrc%5Etfw">April 1, 2018</a></blockquote>\n\n\n<p>In the preview video, you can see Public Post Preview&#8217;s options added to the sidebar and in addition to generating a link, you can choose when that URL expires which is better than what&#8217;s currently available in the plugin.</p>\n\n<p>I mentioned how Tags would sometimes disappear and there overall behavior was inconsistent. This <a href="https://github.com/WordPress/gutenberg/pull/5913">pull request </a>that made it into Gutenberg 2.6, fixes the issue by only including the term in the Tag selector if it&#8217;s known.</p>\n\n<p>Although I&#8217;m still bummed that certain plugins are not yet compatible with Gutenberg, I&#8217;m pretty happy that two of the major pain points I experienced have been fixed. Thanks to Riad, Tammie Lister, and others for helping to solve these problems so quickly. </p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 06 Apr 2018 21:29:35 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Jeff Chandler";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:26;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:61:"WPTavern: A WordCamp for Organizers Is in the Planning Stages";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=79513";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:72:"https://wptavern.com/a-wordcamp-for-organizers-is-in-the-planning-stages";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:2684:"<p>Last year, Drew Jaynes and Carol Stambaugh <a href="https://make.wordpress.org/community/2017/08/16/proposed-event-wordcamp-for-wordcamp-organizers/">proposed</a> a new WordCamp event geared towards organizers to the WordPress Community Team. After fleshing out the details and discussing the idea over the course of seven months, the duo <a href="https://make.wordpress.org/community/2018/04/03/want-to-help-organize-a-wordcamp-for-organizers/">has announced</a> that a WordCamp for Organizers is officially on the table.</p>\n\n<p>The goal of the event is to provide an opportunity for meetup and WordPress event organizers to share their experience with others in the community. The plan is to host a one-day event a day or two before <a href="https://2018.us.wordcamp.org/">WordCamp US</a> in Nashville, TN, later this year.</p>\n\n<p>&#8220;The idea for WordCamp for Organizers – what some of us affectionately refer to as &#8216;dotOrganize&#8217; – was really borne out of many conversations I’ve had over the years with others in the WordPress community lamenting the lack of a ready knowledge sharing opportunity between event organizers,&#8221; Jaynes said.</p>\n\n<p>&#8220;One common thread seemed to be the idea of common lessons learned, and how awesome it would be to just have an event for organizers to get together and swap tips and tricks. </p>\n\n<p>&#8220;We’re all here organizing the same community, maybe we should get together and trade notes! And so we now have a new topic-based WordCamp just for organizers. </p>\n\n<p>&#8220;It’s kind of meta – organizing a WordCamp for Organizers, but I really feel like this could be a boon for anybody currently organizing or looking to start organizing in WordPress. Exciting!&#8221;</p>\n\n<p>Some of the session topics that could be presented on include:</p>\n\n<ul>\n    <li>Spreading the word about your community events</li>\n    <li>Tips for wrangling speakers, volunteers, and sponsors</li>\n    <li>Conflict resolution among organizing teams</li>\n    <li>How to respond to a code of conduct issue</li>\n    <li>Finding and using official organizing tools and resources<br /></li>\n</ul>\n\n<p>The team is seeking volunteers who can spend 2-4 hours per week to help organize the event. Although it&#8217;s focused on organizers, those who have attended many conferences, including WordCamps, with little event organizing experience, are encouraged to join the team.</p>\n\n<p>If you&#8217;re interested in volunteering, please leave a comment on the <a href="https://make.wordpress.org/community/2018/04/03/want-to-help-organize-a-wordcamp-for-organizers/">official announcement post</a>. </p>\n\n<p><br /></p><br /></p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 06 Apr 2018 00:22:47 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Jeff Chandler";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:27;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:57:"WPTavern: Jetpack 6.0 Takes Steps Towards GDPR Compliance";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=79459";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:68:"https://wptavern.com/jetpack-6-0-takes-steps-towards-gdpr-compliance";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1148:"<p>Jetpack 6.0 <a href="https://jetpack.com/2018/04/03/jetpack-6-0/">is available</a> for upgrade. It comes with improvements to the social media icons widget, enhanced brute force protection, and better compatibility between WooCommerce and Jetpack.</p>\n\n<p>Its headlining features though are privacy related as the <a href="https://www.eugdpr.org/">General Data Protection Regulation</a> (GDPR) is set to go into effect May 25th. In 6.0, Jetpack has a dedicated privacy settings page that links to privacy documents and includes a way to opt-out of activity tracking.</p>\n\n<p>These settings can be accessed by clicking the Privacy link at the bottom of the Jetpack Dashboard page. </p>\n\n<img />\n    Jetpack 6.0 Privacy Settings\n\n\n<p>The &#x27;<a href="https://jetpack.com/support/what-data-does-jetpack-sync/">What Data Does Jetpack Sync</a>&#x27; page outlines what data is used, how it&#x27;s used, the relationship it has with the WordPress mobile apps, and provides an inside look at how Jetpack works.</p>\n\n<p>These are the first steps towards GDPR compliance with more updates planned before the regulation goes into effect next month. </p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 05 Apr 2018 23:20:53 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Jeff Chandler";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:28;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:94:"WPTavern: WPWeekly Episode 311 – Jetpack 6.0, WordPress 4.9.5, and A WordCamp for Organizers";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:58:"https://wptavern.com?p=79447&preview=true&preview_id=79447";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:95:"https://wptavern.com/wpweekly-episode-311-jetpack-wordpress-4-9-5-and-a-wordcamp-for-organizers";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1628:"<p>In this episode, <a href="http://jjj.me">John James Jacoby</a> and I discuss a <a href="https://glueckpress.com/9336/amp-and-wordpress/">great article</a> published by Caspar Hübinger on AMP and WordPress. We cover what&#8217;s new in WordPress 4.9.5, Jetpack 6.0, and a WordCamp geared toward organizers.</p>\n<h2>Stories Discussed:</h2>\n<p><a href="https://wptavern.com/wordpress-4-9-5-squashes-25-bugs">WordPress 4.9.5 Squashes 25 Bugs</a><br />\n<a href="https://wptavern.com/try-gutenberg-prompt-pushed-back-to-a-later-release">‘Try Gutenberg’ Prompt Pushed Back to A Later Release</a><br />\n<a href="https://jetpack.com/2018/04/03/jetpack-6-0/">Jetpack 6.0 Released</a><br />\n<a href="https://make.wordpress.org/community/2018/04/03/code-of-conduct-survey/">Code of Conduct Survey</a><br />\n<a href="https://make.wordpress.org/community/2018/04/03/want-to-help-organize-a-wordcamp-for-organizers/">Want to Help Organize a WordCamp for Organizers?</a></p>\n<h2>WPWeekly Meta:</h2>\n<p><strong>Next Episode:</strong> Wednesday, April 11th 3:00 P.M. Eastern</p>\n<p>Subscribe to <a href="https://itunes.apple.com/us/podcast/wordpress-weekly/id694849738">WordPress Weekly via Itunes</a></p>\n<p>Subscribe to <a href="https://www.wptavern.com/feed/podcast">WordPress Weekly via RSS</a></p>\n<p>Subscribe to <a href="http://www.stitcher.com/podcast/wordpress-weekly-podcast?refid=stpr">WordPress Weekly via Stitcher Radio</a></p>\n<p>Subscribe to <a href="https://play.google.com/music/listen?u=0#/ps/Ir3keivkvwwh24xy7qiymurwpbe">WordPress Weekly via Google Play</a></p>\n<p><strong>Listen To Episode #311:</strong><br />\n</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 05 Apr 2018 01:15:24 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Jeff Chandler";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:29;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:76:"HeroPress: The Year I Got Cancer Was The Year My WordPress Business Took Off";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:56:"https://heropress.com/?post_type=heropress-essays&p=2497";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:204:"https://heropress.com/essays/the-year-i-got-cancer-was-the-year-my-wordpress-business-took-off/#utm_source=rss&utm_medium=rss&utm_campaign=the-year-i-got-cancer-was-the-year-my-wordpress-business-took-off";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:8809:"<img width="960" height="480" src="https://s20094.pcdn.co/wp-content/uploads/2018/04/040418-1024x512.jpg" class="attachment-large size-large wp-post-image" alt="Pull Quote: Take care of yourselves and of each other, it''s very important." /><p>In 2010 I was beginning to take on client work creating WordPress web sites when I was diagnosed with ovarian cancer. Ironically, at 43 years old I was more fit and healthy than I had ever been in my entire life. I had been running road marathons and trail ultra marathons for a few years leading up to my diagnosis. Good thing too, because surgery and treatment were obviously very tough.</p>\n<p>I was transitioning my design studio from analog to digital. Previously, I was working as a professional artist and illustrator creating commission portraiture and college mascot illustrations with some moderate successes. My background is in Fine Art and Design. I studied Drawing, Sculpture, &amp; Time Arts. (2D, 3D &amp; 4D &#8211; 4D can be described as: Length, width, height, and time/ motion) Naturally, as a Time Arts artist/illustrator I love the web! I love designing for the web and mobile too. Web work is immediately gratifying. I do design for both the web and print but I have always felt drawn to the web as it is more accessible, more fluid, dynamic, interconnected, animated, media rich, and well … if you are reading this then you already get the picture. <img src="https://s.w.org/images/core/emoji/2.4/72x72/1f642.png" alt="🙂" class="wp-smiley" /></p>\n<blockquote><p>To get through treatment it was important to me that I still work every day.</p></blockquote>\n<p>Some days it was just a couple hours in the afternoon but it really helped me to cope and endure unplanned surgery and many painful chemotherapy treatments. I had recently completed a couple of websites with the help of my mentor Jerry Turk. Those sites had good reach. I mean they were the kind of sites that groups of people used and managed so I got some attention for the work and word of mouth spread locally.</p>\n<p>It was while getting through that period of treatment and the shock of a devastating diagnosis that my digital design agency really got legs. It could not have happened at a better time. So, my studio, C&amp;D Studios &#8211; <a href="https://CandDStudios.com">https://CandDStudios.com</a> continued it’s move towards being 100% digital. No more analog photography and not much more drawing at the drawing table either. Now my work was nearly 100% focused on screens and it would also begin to pay the bills. That was eight years ago. I was very fortunate to learn using reliable frameworks, themes, plug-ins, and hosts that would also stand the test of time. Thank you Genesis Framework, StudioPress and Gravity Forms to name a few…</p>\n<p>Fast forward to 2016 after having been cancer free all those years and cranking out a lot of agency work, I had a cancer reoccurrence. I never wanted cancer to be part of my story and I tried to deny or overcome it in all ways but it had resurfaced in October of 2016. Professionally, I was involved in collaborating on very large scale enterprise sites with teams. I was spending the year testing the waters at a new level of production. It was not good timing to require another surgery and 18 more rounds of chemotherapy. Fortunately, I found support in the community from other designers and developers whom helped me to the finish lines with 3 large projects in areas of e-commerce, college membership and enterprise site work -one with a large volume of SVG animations. After surgery, in February of 2017 I completed 18 rounds of chemotherapy treatments.</p>\n<blockquote><p>I have survived and am cancer free once again!</p></blockquote>\n<p>While going through treatment the second time I was not really focused on what good thing will I be able to grow towards professionally. Honestly, when you go through these things in life &#8211; you just spend your energy getting through as best as you can. I think it is worth noting here that while a positive attitude is great and it can improve a patient’s overall experience. Please don’t tell people they will survive cancer because of their positive attitude. Cancer is horrible, it does not discriminate and when people can’t beat a disease with their mental attitude they end up blaming themselves unnecessarily. That is not good. That is not what people intend when then try to offer support in that way but that is something I wanted to share.</p>\n<p>Now I have landed on the other side of treatment again and as I reflect, it’s been a really epic year! Our WordPress community has been open and supportive, welcoming me at conferences, online, and in slack groups, whether I had hair, energy, or resources &#8211; always welcoming and always encouraging. Over the years I have made some wonderful friends through WordPress! Some of us have been at this for a long time and we now have many shared experiences and skills.</p>\n<h3>Having Survived Again I’m Launching a WordPress Product</h3>\n<p>Having survived again, and having been inspired at PressNomics in Tempe last spring, I’m busy launching a mobile product/service with my team Dr. Kendra Remington and Rita Best called <a href="https://docswithapps.com">Docs With Apps</a>.</p>\n<p>I still accept some client work, and some retainer work but I’m pretty selective about the projects we work on “in house.”</p>\n<p>I love SVG animation work so in 2018 I’m overjoyed to be doing more contracted SVG animation work with my collaborator Jackie D’Elia. Hit us up!!</p>\n<p>These are some very wonderful times in technology and within the Internet of things. I feel very grateful to have been able to ride the digital wave into the present and future. At WCUS in Nashville I began the process of contributing to make WordPress after having spent many years empowering others with it.</p>\n<p>When I was young I needed a way to get my portfolio on line and that is how I got started. Thank you for the opportunity to share my story, to publicly thank my husband Dominic, my family, and friends too. The future is going to be awesome!</p>\n<p>Take care of yourself and of each other, it&#8217;s very important.</p>\n<div class="rtsocial-container rtsocial-container-align-right rtsocial-horizontal"><div class="rtsocial-twitter-horizontal"><div class="rtsocial-twitter-horizontal-button"><a title="Tweet: The Year I Got Cancer Was The Year My WordPress Business Took Off" class="rtsocial-twitter-button" href="https://twitter.com/share?text=The%20Year%20I%20Got%20Cancer%20Was%20The%20Year%20My%20WordPress%20Business%20Took%20Off&via=heropress&url=https%3A%2F%2Fheropress.com%2Fessays%2Fthe-year-i-got-cancer-was-the-year-my-wordpress-business-took-off%2F" rel="nofollow" target="_blank"></a></div></div><div class="rtsocial-fb-horizontal fb-light"><div class="rtsocial-fb-horizontal-button"><a title="Like: The Year I Got Cancer Was The Year My WordPress Business Took Off" class="rtsocial-fb-button rtsocial-fb-like-light" href="https://www.facebook.com/sharer.php?u=https%3A%2F%2Fheropress.com%2Fessays%2Fthe-year-i-got-cancer-was-the-year-my-wordpress-business-took-off%2F" rel="nofollow" target="_blank"></a></div></div><div class="rtsocial-linkedin-horizontal"><div class="rtsocial-linkedin-horizontal-button"><a class="rtsocial-linkedin-button" href="https://www.linkedin.com/shareArticle?mini=true&url=https%3A%2F%2Fheropress.com%2Fessays%2Fthe-year-i-got-cancer-was-the-year-my-wordpress-business-took-off%2F&title=The+Year+I+Got+Cancer+Was+The+Year+My+WordPress+Business+Took+Off" rel="nofollow" target="_blank" title="Share: The Year I Got Cancer Was The Year My WordPress Business Took Off"></a></div></div><div class="rtsocial-pinterest-horizontal"><div class="rtsocial-pinterest-horizontal-button"><a class="rtsocial-pinterest-button" href="https://pinterest.com/pin/create/button/?url=https://heropress.com/essays/the-year-i-got-cancer-was-the-year-my-wordpress-business-took-off/&media=https://heropress.com/wp-content/uploads/2018/04/040418-150x150.jpg&description=The Year I Got Cancer Was The Year My WordPress Business Took Off" rel="nofollow" target="_blank" title="Pin: The Year I Got Cancer Was The Year My WordPress Business Took Off"></a></div></div><a rel="nofollow" class="perma-link" href="https://heropress.com/essays/the-year-i-got-cancer-was-the-year-my-wordpress-business-took-off/" title="The Year I Got Cancer Was The Year My WordPress Business Took Off"></a></div><p>The post <a rel="nofollow" href="https://heropress.com/essays/the-year-i-got-cancer-was-the-year-my-wordpress-business-took-off/">The Year I Got Cancer Was The Year My WordPress Business Took Off</a> appeared first on <a rel="nofollow" href="https://heropress.com">HeroPress</a>.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 04 Apr 2018 12:00:48 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:11:"Cathi Bosco";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:30;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:42:"WPTavern: WordPress 4.9.5 Squashes 25 Bugs";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=79399";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:53:"https://wptavern.com/wordpress-4-9-5-squashes-25-bugs";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:940:"<p>WordPress 4.9.5 <a href="https://wordpress.org/news/2018/04/wordpress-4-9-5-security-and-maintenance-release/">is available</a> for download and is a maintenance and security release. WordPress 4.9.4 and earlier versions are affected by three security issues. The following security hardening changes are in 4.9.5.</p>\n\n<ul>\n    <li>Localhost is no longer treated as the same host by default.<br /></li>\n    <li>Safe redirects are used when redirecting the login page if SSL is forced.</li>\n    <li>Versions strings are correctly escaped for use in generator tags.</li>\n</ul>\n\n<p>Twenty-five bugs are fixed in this release including, improve compatibility with PHP 7.2, previous styles on caption shortcodes are restored, and clearer error messages. To see a full list of changes along with their associated trac tickets, check out <a href="https://make.wordpress.org/core/2018/04/02/wordpress-4-9-5/">the detailed release post</a>. </p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 03 Apr 2018 23:02:27 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Jeff Chandler";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:31;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:58:"Dev Blog: WordPress 4.9.5 Security and Maintenance Release";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"https://wordpress.org/news/?p=5645";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:84:"https://wordpress.org/news/2018/04/wordpress-4-9-5-security-and-maintenance-release/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:6336:"<p>WordPress 4.9.5 is now available. This is a <strong>security and maintenance release</strong> for all versions since WordPress 3.7. We strongly encourage you to update your sites immediately.</p>\n\n<p>WordPress versions 4.9.4 and earlier are affected by three security issues. As part of the core team&#x27;s ongoing commitment to security hardening, the following fixes have been implemented in 4.9.5:</p>\n\n<ol>\n    <li>Don&#x27;t treat <code>localhost</code> as same host by default.</li>\n    <li>Use safe redirects when redirecting the login page if SSL is forced.</li>\n    <li>Make sure the version string is correctly escaped for use in generator tags.</li>\n</ol>\n\n<p>Thank you to the reporters of these issues for practicing <a href="https://make.wordpress.org/core/handbook/testing/reporting-security-vulnerabilities/">﻿coordinated security disclosure</a>: <a href="https://profiles.wordpress.org/xknown">xknown</a> of the WordPress Security Team, <a href="https://hackerone.com/nitstorm">Nitin Venkatesh (nitstorm)</a>, and <a href="https://twitter.com/voldemortensen">Garth Mortensen</a> of the WordPress Security Team.</p>\n\n<p>Twenty-five other bugs were fixed in WordPress 4.9.5. Particularly of note were:</p>\n\n<ul>\n    <li>The previous styles on caption shortcodes have been restored.</li>\n    <li>Cropping on touch screen devices is now supported.</li>\n    <li>A variety of strings such as error messages have been updated for better clarity.</li>\n    <li>The position of an attachment placeholder during uploads has been fixed.</li>\n    <li>Custom nonce functionality in the REST API JavaScript client has been made consistent throughout the code base.</li>\n    <li>Improved compatibility with PHP 7.2.</li>\n</ul>\n\n<p><a href="https://make.wordpress.org/core/2018/04/03/wordpress-4-9-5/">This post has more information about all of the issues fixed in 4.9.5 if you&#x27;d like to learn more</a>.</p>\n\n<p><a href="https://wordpress.org/download/">Download WordPress 4.9.5</a> or venture over to Dashboard → Updates and click "Update Now." Sites that support automatic background updates are already beginning to update automatically.</p>\n\n<p>Thank you to everyone who contributed to WordPress 4.9.5:</p>\n\n<p><a href="https://profiles.wordpress.org/1265578519-1/">1265578519</a>, <a href="https://profiles.wordpress.org/jorbin/">Aaron Jorbin</a>, <a href="https://profiles.wordpress.org/adamsilverstein/">Adam Silverstein</a>, <a href="https://profiles.wordpress.org/schlessera/">Alain Schlesser</a>, <a href="https://profiles.wordpress.org/alexgso/">alexgso</a>, <a href="https://profiles.wordpress.org/afercia/">Andrea Fercia</a>, <a href="https://profiles.wordpress.org/andrei0x309/">andrei0x309</a>, <a href="https://profiles.wordpress.org/antipole/">antipole</a>, <a href="https://profiles.wordpress.org/aranwer104/">Anwer AR</a>, <a href="https://profiles.wordpress.org/birgire/">Birgir Erlendsson (birgire)</a>, <a href="https://profiles.wordpress.org/blair-jersyer/">Blair jersyer</a>, <a href="https://profiles.wordpress.org/bandonrandon/">Brooke.</a>, <a href="https://profiles.wordpress.org/chetan200891/">Chetan Prajapati</a>, <a href="https://profiles.wordpress.org/codegrau/">codegrau</a>, <a href="https://profiles.wordpress.org/conner_bw/">conner_bw</a>, <a href="https://profiles.wordpress.org/davidakennedy/">David A. Kennedy</a>, <a href="https://profiles.wordpress.org/designsimply/">designsimply</a>, <a href="https://profiles.wordpress.org/dd32/">Dion Hulse</a>, <a href="https://profiles.wordpress.org/ocean90/">Dominik Schilling (ocean90)</a>, <a href="https://profiles.wordpress.org/electricfeet/">ElectricFeet</a>, <a href="https://profiles.wordpress.org/ericmeyer/">ericmeyer</a>, <a href="https://profiles.wordpress.org/fpcsjames/">FPCSJames</a>, <a href="https://profiles.wordpress.org/garrett-eclipse/">Garrett Hyder</a>, <a href="https://profiles.wordpress.org/pento/">Gary Pendergast</a>, <a href="https://profiles.wordpress.org/soulseekah/">Gennady Kovshenin</a>, <a href="https://profiles.wordpress.org/henrywright/">Henry Wright</a>, <a href="https://profiles.wordpress.org/audrasjb/">Jb Audras</a>, <a href="https://profiles.wordpress.org/jbpaul17/">Jeffrey Paul</a>, <a href="https://profiles.wordpress.org/jipmoors/">Jip Moors</a>, <a href="https://profiles.wordpress.org/joemcgill/">Joe McGill</a>, <a href="https://profiles.wordpress.org/joen/">Joen Asmussen</a>, <a href="https://profiles.wordpress.org/johnbillion/">John Blackbourn</a>, <a href="https://profiles.wordpress.org/johnpgreen/">johnpgreen</a>, <a href="https://profiles.wordpress.org/junaidkbr/">Junaid Ahmed</a>, <a href="https://profiles.wordpress.org/kristastevens/">kristastevens</a>, <a href="https://profiles.wordpress.org/obenland/">Konstantin Obenland</a>, <a href="https://profiles.wordpress.org/lakenh/">Laken Hafner</a>, <a href="https://profiles.wordpress.org/lancewillett/">Lance Willett</a>, <a href="https://profiles.wordpress.org/leemon/">leemon</a>, <a href="https://profiles.wordpress.org/melchoyce/">Mel Choyce</a>, <a href="https://profiles.wordpress.org/mikeschroder/">Mike Schroder</a>, <a href="https://profiles.wordpress.org/mrmadhat/">mrmadhat</a>, <a href="https://profiles.wordpress.org/nandorsky/">nandorsky</a>, <a href="https://profiles.wordpress.org/jainnidhi/">Nidhi Jain</a>, <a href="https://profiles.wordpress.org/swissspidy/">Pascal Birchler</a>, <a href="https://profiles.wordpress.org/qcmiao/">qcmiao</a>, <a href="https://profiles.wordpress.org/rachelbaker/">Rachel Baker</a>, <a href="https://profiles.wordpress.org/larrach/">Rachel Peter</a>, <a href="https://profiles.wordpress.org/ravanh/">RavanH</a>, <a href="https://profiles.wordpress.org/otto42/">Samuel Wood (Otto)</a>, <a href="https://profiles.wordpress.org/sebastienthivinfocom/">Sebastien SERRE</a>, <a href="https://profiles.wordpress.org/sergeybiryukov/">Sergey Biryukov</a>, <a href="https://profiles.wordpress.org/shital-patel/">Shital Marakana</a>, <a href="https://profiles.wordpress.org/netweb/">Stephen Edgar</a>, <a href="https://profiles.wordpress.org/karmatosed/">Tammie Lister</a>, <a href="https://profiles.wordpress.org/thomas-vitale/">Thomas Vitale</a>, <a href="https://profiles.wordpress.org/kwonye/">Will Kwon</a>, and <a href="https://profiles.wordpress.org/yahil/">Yahil Madakiya</a>.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 03 Apr 2018 19:56:54 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:17:"Aaron D. Campbell";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:32;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:67:"WPTavern: ‘Try Gutenberg’ Prompt Pushed Back to A Later Release";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=79273";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:72:"https://wptavern.com/try-gutenberg-prompt-pushed-back-to-a-later-release";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:4312:"<p>Last week, <a href="https://wptavern.com/in-wordpress-4-9-5-users-will-be-two-clicks-away-from-installing-and-activating-gutenberg-from-the-dashboard">we reported</a> that WordPress 4.9.5 would ship with a call-out prompt that asks users if they want to try the new editor experience.</p>\n\n<p>Within the comments of the post, Gary Pendergast, who works for Automattic, is a WordPress core contributor, and a lead developer on the Gutenberg project, <a href="https://wptavern.com/in-wordpress-4-9-5-users-will-be-two-clicks-away-from-installing-and-activating-gutenberg-from-the-dashboard#comment-246119">informed us</a> that the prompt would not be in WordPress 4.9.5. Instead, it will ship in a later version once it has gone through a few more refinements.</p>\n\n<blockquote class="wp-block-quote">\n    <p>Change of plans, this won’t be happening in the 4.9.5 release: there are still a few issues we’d like to fix up the callout happens, they won’t be done in time for the 4.9.5 release. I expect there will be a smaller 4.9.6 release that contains this callout, and any bugfixes that happen to be ready.</p><cite>Gary Pendergast</cite></blockquote>\n\n<p>Reverting the call-out has <a href="https://core.trac.wordpress.org/ticket/41316">extended the conversation</a> surrounding its implementation. Jadon N who works for InMotion hosting and is a contributor to the <a href="https://make.wordpress.org/chat/">#hosting-community slack channel</a>, says the hosting-community group is working on ideas to help test popular plugins for Gutenberg compatibility.</p>\n\n<blockquote class="wp-block-quote">\n    <p>We have been working to expand our collection of data about how well plugins function with Gutenberg. To help with that effort, we would like to explore using feedback collected from WordPress users through the Try Gutenberg effort to add to the existing database on WordPress plugin compatibility if that could be worked out. </p>\n    <p>The goal of this project is to make sure everyone can use Gutenberg without having to worry about plugin incompatibilities.</p><cite>Jadon N</cite></blockquote>\n\n<p>The <a href="https://plugincompat.danielbachhuber.com/">Gutenberg Plugin Compatibility Database project</a> launched by Daniel Bachhuber last month attempts to determine which popular plugins are already compatible with Gutenberg by having volunteers test them in a sandboxed environment.</p>\n\n<p>Out of the 4,213 plugins in the database, 84% have an unknown compatibility status. Out of 610 plugins that have been tested, 82% don&#x27;t include editor functionality.</p>\n\n<p>Pendergast <a href="https://core.trac.wordpress.org/ticket/41316#comment:92">supports the idea</a> of hosts collecting a wide range of testing data and turning it into actionable items for the team to work on. There&#x27;s also been some discussion on creating snapshots of plugin compatibility and filtering those results into Bachhuber&#x27;s project.</p>\n\n<p>Chris Lema, Vice President of Products at LiquidWeb, <a href="https://core.trac.wordpress.org/ticket/41316#comment:98">responded</a> in the trac ticket with a suggestion that the team place as much emphasis on the Learn More and Report Issues sections as the Try Gutenberg message. He also added a prototype screenshot of what the call-out could look like. <br /></p>\n\n<img />\n    Gutenberg Call Out Prototype by Chris Lema\n\n\n<p>"The reality is that people don&#x27;t read a lot, so people may not fully grasp the &#x27;testing&#x27; part given the proposed design," Lema said. "When there are equal weight to the design, the message also carries with it the same equality."</p>\n\n<p>One of the best suggestions I&#x27;ve read comes <a href="https://core.trac.wordpress.org/ticket/41316#comment:98">from Bachhuber</a>. He suggests displaying the prompt to a small percentage of WordPress sites to prevent thousands of users from re-reporting known issues with Gutenberg. It would also help lessen the load on the support forums.</p>\n\n<p>One of my main concerns with the call-out is the lack of upfront information to the user that it is beta software and it could cause adverse affects on their site. Lema&#x27;s prototype does a great job of informing the user of this possibility and a link to known issues is a great enhancement. What do you think?</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 03 Apr 2018 00:13:16 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Jeff Chandler";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:33;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:21:"Matt: Goose-down Nape";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:22:"https://ma.tt/?p=48011";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:38:"https://ma.tt/2018/04/goose-down-nape/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1378:"<p>There was a <a href="https://www.nytimes.com/2018/03/28/magazine/poem-the-nod.html">beautiful poem by Kayo Chingonyi in the New York Magazine this week</a> titled The Nod:</p>\n\n<blockquote class="wp-block-quote">\n    <p>When we’re strangers that pass each other<br />in the street, it will come down to this tilt<br />of the head — acknowledging another<br />version of events set in a new-build<br />years from now, a mess of a place filled<br />with books and records, our kids thick as thieves<br />redefining all notions of mischief.</p>\n    <p>Perhaps our paths will cross in a city<br />of seven hills as the light draws your face<br />out from the bliss of anonymity.<br />Maybe you’ll be stroking the goose-down nape<br />of a small child with eyes the exact shade<br />of those I met across a room at the start<br />of this pain-in-the-heart, this febrile dance.</p>\n</blockquote>\n\n<p>When I hear "seven hills" my mind immediately goes to Rome, then San Francisco, but <a href="https://en.wikipedia.org/wiki/List_of_cities_claimed_to_be_built_on_seven_hills">Wikipedia has a helpful list of cities that claim to be built on seven hills</a>.</p>\n\n<p>A friend pointed out <em>The Nod</em> is a <a href="https://www.familyfriendpoems.com/poem/the-invitation-by-oriah-mountain-dreamer">fine complement to <em>The Invitation</em> by Oriah Mountain Dreamer</a>.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 03 Apr 2018 00:05:44 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:4:"Matt";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:34;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:77:"Post Status: Contextualized Learning in or around WordPress — Draft podcast";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:31:"https://poststatus.com/?p=44987";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:84:"https://poststatus.com/contextualized-learning-in-or-around-wordpress-draft-podcast/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1838:"<p>Welcome to the Post Status <a href="https://poststatus.com/category/draft">Draft podcast</a>, which you can find <a href="https://itunes.apple.com/us/podcast/post-status-draft-wordpress/id976403008">on iTunes</a>, <a href="https://play.google.com/music/m/Ih5egfxskgcec4qadr3f4zfpzzm?t=Post_Status__Draft_WordPress_Podcast">Google Play</a>, <a href="http://www.stitcher.com/podcast/krogsgard/post-status-draft-wordpress-podcast">Stitcher</a>, and <a href="http://simplecast.fm/podcasts/1061/rss">via RSS</a> for your favorite podcatcher. Post Status Draft is hosted by Brian Krogsgard and co-host Brian Richards.</p>\n<p><span>In this episode, the dynamic Brian duo discuss the highly-anticipated return of WordSesh, the different ways in which we all learn the same, and some of the problems we face in skill building. The guys also spend time finding and contacting the addressable market around WordPress, characterizing a business as WordPress-focused vs providing WordPress services in the context of a broader market, and some of the nuances of providing contextualized services (whether they be training, consulting, or otherwise).</span></p>\n<p></p>\n<h3>Links</h3>\n<ul>\n<li><a href="https://www.nbcnews.com/health/health-news/scientists-say-they-ve-discovered-unknown-human-organ-could-help-n860601">New human organ</a></li>\n<li><a href="http://wordsesh.com/">WordSesh.com</a></li>\n<li><a href="https://wpsessions.com/">WPSessions.com</a></li>\n</ul>\n<h3>Sponsor: OptinMonster</h3>\n<p><a href="http://optinmonster.com">OptinMonster</a> allows you to convert visitors into subscribers. You can easily create &amp; A/B test beautiful lead capture forms without a developer. Be sure to check out their new <a href="http://optinmonster.com/announcing-the-inactivitysensor-activity-logs-and-more/">Inactivity Sensor</a> technology.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 02 Apr 2018 19:31:14 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:14:"Katie Richards";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:35;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:44:"Dev Blog: The Month in WordPress: March 2018";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"https://wordpress.org/news/?p=5632";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:69:"https://wordpress.org/news/2018/04/the-month-in-wordpress-march-2018/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:4323:"<p>With a significant new milestone and some great improvements to WordPress as a platform, this month has been an important one for the project. Read on to find out more about what happened during the month of March.\n\n</p>\n\n<hr class="wp-block-separator" />\n\n<h2>WordPress Now Powers 30% of the Internet</h2>\n\n<p>Over the last 15 years, the popularity and usage of WordPress has been steadily growing. That growth hit a significant milestone this month when <a href="https://w3techs.com/technologies/details/cm-wordpress/all/all">W3Techs reported that WordPress now powers over 30% of sites on the web.</a></p>\n\n<p>The percentage is determined based on W3Techs’ review of the top 10 million sites on the web, and it’s a strong indicator of the popularity and flexibility of WordPress as a platform.</p>\n\n<p>If you would like to have hand in helping to grow WordPress even further, <a href="https://make.wordpress.org/">you can get involved today</a>.</p>\n\n<h2>WordPress Jargon Glossary Goes Live</h2>\n\n<p>The WordPress Marketing Team has been hard at work lately putting together <a href="https://make.wordpress.org/marketing/2018/02/28/wordpress-jargon-glossary/">a comprehensive glossary of WordPress jargon</a> to help newcomers to the project become more easily acquainted with things.</p>\n\n<p>The glossary <a href="https://make.wordpress.org/marketing/2018/02/28/wordpress-jargon-glossary/">is available here</a> along with a downloadable PDF to make it simpler to reference offline.</p>\n\n<p>Publishing this resource is part of an overall effort to make WordPress more easily accessible for people who are not so familiar with the project. If you would like to assist the Marketing Team with this, you can follow <a href="https://make.wordpress.org/marketing/">the team blog</a> and join the #marketing channel in the<a href="https://make.wordpress.org/chat/"> Making WordPress Slack group</a>.</p>\n\n<h2>Focusing on Privacy in WordPress</h2>\n\n<p>Online privacy has been in the news this month for all the wrong reasons. It has reinforced the commitment of the GDPR Compliance Team to continue working on enhancements to WordPress core that allow site owners to improve privacy standards.</p>\n\n<p>The team&#x27;s work, and the wider privacy project, spans four areas: Adding tools which will allow site administrators to collect the information they need about their sites, examining the plugin guidelines with privacy in mind, enhancing privacy standards in WordPress core, and creating documentation focused on best practices in online privacy.</p>\n\n<p>To get involved with the project, you can <a href="https://make.wordpress.org/core/2018/03/28/roadmap-tools-for-gdpr-compliance/">view the roadmap</a>, <a href="https://make.wordpress.org/core/tag/gdpr-compliance/">follow the updates</a>, <a href="https://core.trac.wordpress.org/query?status=!closed&keywords=~gdpr">submit patches</a>, and join the #gdpr-compliance channel in the <a href="https://make.wordpress.org/chat">Making WordPress Slack group</a>. Office hours are 15:00 UTC on Wednesdays.</p>\n\n<hr class="wp-block-separator" />\n\n<h2>Further Reading:</h2>\n\n<ul>\n    <li>The WordPress Foundation has published <a href="https://wordpressfoundation.org/2017-annual-report/">their annual report for 2017</a> showing just how much the community has grown over the last year.</li>\n    <li>The dates for WordCamp US <a href="https://2018.us.wordcamp.org/2018/03/13/announcing-wordcamp-us-2018/">have been announced</a> — this flagship WordCamp event will be held on 7-9 December this year in Nashville, Tennessee.</li>\n    <li>WordPress 4.9.5 is due for release on April 3 — <a href="https://make.wordpress.org/core/2018/03/21/wordpress-4-9-5-beta/">find out more here</a>.</li>\n    <li>Version 2.5 of Gutenberg, the new editor for WordPress core, <a href="https://make.wordpress.org/core/2018/03/29/whats-new-in-gutenberg-29th-march/">was released this month</a> with a host of great improvements.</li>\n    <li>WordSesh, a virtual WordPress conference, <a href="http://wordsesh.com/">is returning in July this year</a>.</li>\n</ul>\n\n<p><em>If you have a story we should consider including in the next “Month in WordPress” post, please <a href="https://make.wordpress.org/community/month-in-wordpress-submissions/">submit it here</a>.</em><br /></p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 02 Apr 2018 08:00:22 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:15:"Hugh Lashbrooke";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:36;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:77:"WPTavern: WPWeekly Episode 310 – Community Management, PHP, and Hello Dolly";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:58:"https://wptavern.com?p=79249&preview=true&preview_id=79249";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:82:"https://wptavern.com/wpweekly-episode-310-community-management-php-and-hello-dolly";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:2325:"<p>In this episode, <a href="http://jjj.me">John James Jacoby</a> and I discuss the news of the week including, the removal of offensive lyrics in Hello Dolly, a request for plugin developers to stop supporting legacy PHP versions, and changes coming in WordPress 4.9.5.</p>\n<p>We also talk about community management, the difference between comments and forums, and finally, John shares his concerns on how the Gutenberg call-out prompt is being built into core.</p>\n<h2>Stories Discussed:</h2>\n<p><a href="https://wptavern.com/a-plea-for-plugin-developers-to-stop-supporting-legacy-php-versions">A Plea For Plugin Developers to Stop Supporting Legacy PHP Versions</a><br />\n<a href="https://wptavern.com/without-context-some-lyrics-inside-the-hello-dolly-plugin-are-degrading-to-women">Without Context, Some Lyrics Inside the Hello Dolly Plugin Are Degrading to Women</a><br />\n<a href="https://wptavern.com/why-gutenberg-and-why-now">Why Gutenberg and Why Now?</a><br />\n<a href="https://wptavern.com/noteworthy-changes-coming-in-wordpress-4-9-5">Noteworthy Changes Coming in WordPress 4.9.5</a><br />\n<a href="https://wptavern.com/in-wordpress-4-9-5-users-will-be-two-clicks-away-from-installing-and-activating-gutenberg-from-the-dashboard">In WordPress 4.9.5, Users Will Be Two Clicks Away From Installing and Activating Gutenberg From the Dashboard</a></p>\n<h2>Picks of the Week:</h2>\n<p><a href="https://wptavern.com/how-to-disable-push-notification-requests-in-firefox">How to Disable Push Notification Requests in Firefox</a></p>\n<p><a href="https://addons.mozilla.org/en-US/firefox/addon/facebook-container/">Facebook Container Add-on for Firefox</a></p>\n<h2>WPWeekly Meta:</h2>\n<p><strong>Next Episode:</strong> Wednesday, April 4th 3:00 P.M. Eastern</p>\n<p>Subscribe to <a href="https://itunes.apple.com/us/podcast/wordpress-weekly/id694849738">WordPress Weekly via Itunes</a></p>\n<p>Subscribe to <a href="https://www.wptavern.com/feed/podcast">WordPress Weekly via RSS</a></p>\n<p>Subscribe to <a href="http://www.stitcher.com/podcast/wordpress-weekly-podcast?refid=stpr">WordPress Weekly via Stitcher Radio</a></p>\n<p>Subscribe to <a href="https://play.google.com/music/listen?u=0#/ps/Ir3keivkvwwh24xy7qiymurwpbe">WordPress Weekly via Google Play</a></p>\n<p><strong>Listen To Episode #310:</strong><br />\n</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 29 Mar 2018 21:07:53 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Jeff Chandler";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:37;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:42:"WPTavern: My Gutenberg Experience Thus Far";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=78991";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:53:"https://wptavern.com/my-gutenberg-experience-thus-far";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:5397:"<p>Ive used Gutenberg for several months and during that time, there have been moments where I love it and situations where I&#x27;ve had to disable the plugin because of frustrating bugs. </p>\n\n<p>One of the most frustrating aspects of using Gutenberg is the lack of support from the plugins I depend on.</p>\n\n<h2>Publish Post Preview</h2>\n\n<p>I use the <a href="https://wordpress.org/plugins/public-post-preview/">Publish Post Preview</a> plugin to generate a preview link for posts so that people can see what it looks like before it&#x27;s published.</p>\n\n<img />\n    Publish Preview Checkbox in the Current Editor\n\n\n<p>In the current editor, the checkbox to generate a link is in the Publish meta box. In Gutenberg, that option doesn&#x27;t exist. According to a <a href="https://wordpress.org/support/topic/compatibility-with-gutenberg/">recent support forum post</a>, the author does not plan on making it Gutenberg compatible until there is a finalized API to extend the sidebar.</p>\n\n<h2>Telegram for WordPress</h2>\n\n<p>We use the <a href="https://wordpress.org/plugins/telegram-for-wp/">Telegram for WordPress</a> plugin to automatically send published posts to our Telegram channel. The plugin adds a meta box that has options to send the post, configure the message structure, send a file, and display the featured image.</p>\n\n<p>In Gutenberg, the meta box is open by default which provides access to those options. However, when I edit a published post, there are times when the meta box is closed and clicking the arrow to expand it doesn&#x27;t work. <em>Since the Send this post to channel</em> option is on by default, ﻿saving changes to the post will resend the post to Telegram subscribers. Something I don&#x27;t want to happen for simple edits. <br /></p>\n\n<h2>Edit Flow</h2>\n\n<p>We use <a href="https://wordpress.org/plugins/edit-flow/">Edit Flow</a> to collaborate on posts and often use the Editorial Comments feature to provide feedback. In Gutenberg, the meta boxes for Editorial Comments and Notifications do not open when clicking the arrow. Therefor, we can&#x27;t use those features. <br /></p>\n\n<img />\n    <br /><br />Edit Flow Meta Boxes are Broken\n\n\n<h2>After the Deadline</h2>\n\n<p>I&#x27;m a fan of <a href="https://jetpack.com/support/spelling-and-grammar/">After the Deadline</a> which is a proofreading module in Jetpack. It checks posts for spelling, grammar, and misused words. When activated, a button is added to the visual editor to perform the checks. This button is not available in Gutenberg, so those features are not available as well.</p>\n\n<h2>Adding Images to Paragraphs is a Pain</h2>\n\n<p>Adding images to paragraphs in Gutenberg is more cumbersome than it needs to be. In the current editor, all I have to do is place the cursor where I want to insert an image, add media, choose image size, align it, and I&#x27;m done.</p>\n\n<p>In Gutenberg, you need to create an image block below the paragraph block, move the image block to the paragraph block, align it, and use handlebars on the corner of the image to resize it. </p>\n\n<p>I realize that there are a few workflows that I&#x27;m going to have to change because of how Gutenberg works, but this workflow doesn&#x27;t make any sense to me, especially when I can&#x27;t insert images without creating a new block. Thankfully, the Gutenberg team is on top of it and is <a href="https://github.com/WordPress/gutenberg/pull/5794">working on a solution</a> to add images within a paragraph block.</p>\n\n<h2>Random Blank Paragraph Blocks</h2>\n\n<p>I recently copied a large amount of text from a Google Doc and pasted it into Gutenberg and was surprised by how well it worked. Blocks were created in the right spots and I didn&#x27;t have to edit it much.</p>\n\n<p>I opened the post in the classic editor so that I could use the proofreading feature and it mangled the post. I opened the post in Gutenberg again and noticed a bunch of empty paragraph blocks created in-between paragraph blocks.</p>\n\n<p>This resulted in having to spend some time deleting the empty paragraph blocks and questioning whether I should avoid transferring posts between editors in the future.</p>\n\n<h2>Tags Sometimes Appear Blank in the Meta Box</h2>\n\n<p>When adding tags to posts, sometimes the tags appear blank although they show up on the front-end. Also, deleting tags sometimes doesn&#x27;t work. I click on the X and nothing happens in the back-end, but the tag will be removed from the front-end. <br /></p>\n\n<img />\n    Blank Tags in Gutenberg\n\n\n<h2>Gutenberg Has a Lot of Rough Edges</h2>\n\n<p>If this version of Gutenberg were merged into WordPress today, it would be a disaster. It&#x27;s clear that the project has a long way to go before being considered for merge into core. Most of the issues I&#x27;ve outlined in this post are known and are being addressed. <br /></p>\n\n<p>Gutenberg is supposed to make everything we do in the current editor easier and more efficient. If it doesn&#x27;t, then I have to ask, what&#x27;s the point?</p>\n\n<p>What concerns me the most about Gutenberg is plugin support. Some of the plugins I mentioned above are active on 10K sites or less but are important to the way I craft and publish content in WordPress. <br /></p>\n\n<p> Without them, using Gutenberg is not a great experience and instead, makes me want to use the current editor where things simply work. <br /></p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 29 Mar 2018 20:28:50 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Jeff Chandler";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:38;a:6:{s:4:"data";s:11:"\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:44:"HeroPress: Giving Back In Your Own Community";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://heropress.com/?p=2490";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:133:"https://heropress.com/giving-back-in-your-own-community/#utm_source=rss&utm_medium=rss&utm_campaign=giving-back-in-your-own-community";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:3298:"<img width="960" height="480" src="https://s20094.pcdn.co/wp-content/uploads/2015/11/GoodtimeToBe-HeroPress-1024x512.jpg" class="attachment-large size-large wp-post-image" alt="Pull quote: It is a good time to be part of the global WordPress community: the costs are low, the developer community is strong, and job availability is at an all time high." /><p>I was delighted to find several years ago that there&#8217;s a thriving WordPress community in Nepal. Via Slack I got to meet Sakin Shrestha, and learned all about what their group is doing in Nepal to create jobs and keep the Nepali from having to leave the country to find work.</p>\n<p>I recently found out that Sakin is finding a new way to give back to his community: <a href="https://sakinshrestha.com/events/announcing-my-new-venture-aksharaa-kindergarten/">opening a kindergarten</a>. In order for any country to grow strong it has to have good education for its children, and Sakin is working to make that happen.</p>\n<p>Read about how the Nepali WordPress community is working to build their own country.</p>\n<blockquote class="wp-embedded-content"><p><a href="https://heropress.com/essays/doing-our-part-for-the-community/">Doing Our Part for the Community</a></p></blockquote>\n<p></p>\n<div class="rtsocial-container rtsocial-container-align-right rtsocial-horizontal"><div class="rtsocial-twitter-horizontal"><div class="rtsocial-twitter-horizontal-button"><a title="Tweet: Giving Back In Your Own Community" class="rtsocial-twitter-button" href="https://twitter.com/share?text=Giving%20Back%20In%20Your%20Own%20Community&via=heropress&url=https%3A%2F%2Fheropress.com%2Fgiving-back-in-your-own-community%2F" rel="nofollow" target="_blank"></a></div></div><div class="rtsocial-fb-horizontal fb-light"><div class="rtsocial-fb-horizontal-button"><a title="Like: Giving Back In Your Own Community" class="rtsocial-fb-button rtsocial-fb-like-light" href="https://www.facebook.com/sharer.php?u=https%3A%2F%2Fheropress.com%2Fgiving-back-in-your-own-community%2F" rel="nofollow" target="_blank"></a></div></div><div class="rtsocial-linkedin-horizontal"><div class="rtsocial-linkedin-horizontal-button"><a class="rtsocial-linkedin-button" href="https://www.linkedin.com/shareArticle?mini=true&url=https%3A%2F%2Fheropress.com%2Fgiving-back-in-your-own-community%2F&title=Giving+Back+In+Your+Own+Community" rel="nofollow" target="_blank" title="Share: Giving Back In Your Own Community"></a></div></div><div class="rtsocial-pinterest-horizontal"><div class="rtsocial-pinterest-horizontal-button"><a class="rtsocial-pinterest-button" href="https://pinterest.com/pin/create/button/?url=https://heropress.com/giving-back-in-your-own-community/&media=https://heropress.com/wp-content/uploads/2015/11/GoodtimeToBe-HeroPress-150x150.jpg&description=Giving Back In Your Own Community" rel="nofollow" target="_blank" title="Pin: Giving Back In Your Own Community"></a></div></div><a rel="nofollow" class="perma-link" href="https://heropress.com/giving-back-in-your-own-community/" title="Giving Back In Your Own Community"></a></div><p>The post <a rel="nofollow" href="https://heropress.com/giving-back-in-your-own-community/">Giving Back In Your Own Community</a> appeared first on <a rel="nofollow" href="https://heropress.com">HeroPress</a>.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 28 Mar 2018 14:21:32 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:39;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:119:"WPTavern: In WordPress 4.9.5, Users Will Be Two Clicks Away From Installing and Activating Gutenberg From the Dashboard";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=78827";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:129:"https://wptavern.com/in-wordpress-4-9-5-users-will-be-two-clicks-away-from-installing-and-activating-gutenberg-from-the-dashboard";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:3920:"<p>At the end of last month, Matt Cromwell, Head of Support and Community Outreach for <a href="https://givewp.com/">GiveWP</a> and an administrator for the <a href="https://www.facebook.com/groups/advancedwp/">Advanced WordPress Facebook group</a>, hosted a <a href="https://wptavern.com/matt-cromwell-hosts-matt-mullenweg-in-qa-gutenberg-interview">question and answer session</a> about Gutenberg with Matt Mullenweg.</p>\n\n<p>Mullenweg was asked a few times if he could provide a concrete date on when Gutenberg and WordPress 5.0 would be ready. While a date was not given, Mullenweg said, "For those who want a concrete date, we will have one or two orders of magnitude more users of Gutenberg in April."</p>\n\n<p>It&#x27;s now clear what he meant by that. WordPress 4.9.5, scheduled for release in April, will <a href="https://core.trac.wordpress.org/ticket/41316">feature a call-out prompt</a> that has links to information about Gutenberg and a button to quickly install the plugin if user permissions allow. <br /></p>\n\n<img />\n    Gutenberg Call-out in WordPress 4.9.5\n\n\n<p>The core team added a Try Gutenberg prompt in October of last year but <a href="https://wptavern.com/wordpress-4-9-beta-4-removes-try-gutenberg-call-to-action">it was removed</a> in WordPress 4.9 Beta 4. After discussing the subject with Mullenweg, it was determined that Gutenberg was not ready for large-scale testing.</p>\n\n<p>The prompt in WordPress 4.9.5 changes the button text based on the following scenarios.<br /></p>\n\n<ul>\n    <li>If Gutenberg is not installed, <em>and</em> the user can install plugins, the Install Today button is displayed.<br /></li>\n    <li>If Gutenberg is installed but not activated, <em>and</em> the user can install plugins, the Activate Today button is displayed.<br /></li>\n    <li>If Gutenberg is installed and activated, <em>and</em> the user can edit posts, the Try Today button is displayed.<br /></li>\n</ul>\n\n<p>If Gutenberg is not installed and the user can not install plugins, the button is hidden from view. If you&#x27;d like to hide the prompt from users, David Decker has <a href="https://github.com/deckerweb/remove-gutenberg-panel">created a plugin</a> that&#x27;s available on GitHub that simply hides it from view.</p>\n\n<p>One of the concerns about the prompt is the lack of warning of the risks involved using beta software on a live site. Gutenberg is beta software that&#x27;s still in development that could <a href="https://core.trac.wordpress.org/ticket/41316#comment:75">adversely affect sites</a>. There is no warning on the call-out box and in two clicks, users can install and activate Gutenberg. <br /></p>\n\n<p>Whether it&#x27;s Gutenberg or some other beta software, this general advice applies. Create a full backup of your site before installing and if possible, install it on a staging site first.</p>\n\n<p>I predict that the volunteers who manage the WordPress.org support forums will have their hands full once WordPress 4.9.5 is released. The support team <a href="https://make.wordpress.org/support/2018/03/agenda-for-march-22nd-support-meeting/">is preparing</a> by brainstorming user outcomes, common questions that may be asked, and potential pitfalls users experience after installing Gutenberg. <br /></p>\n\n<p>If you&#x27;d like to give them a helping hand, check out the <a href="https://make.wordpress.org/support/handbook/">Support Handbook</a> and if you have any questions, stop by the <a href="https://wordpress.slack.com/?redir=%2Fmessages%2Fforums%2F">#forums</a> channel in <a href="https://make.wordpress.org/chat/">Slack</a>.</p>\n\n<p>The Gutenberg call-out has the potential to pave the way for large audiences to test major features in core without needing to use or install a beta branch of WordPress. However, this convenience comes with risks and while they can be reduced, WordPress needs to be up front and center to users about those risks.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 27 Mar 2018 22:55:26 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Jeff Chandler";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:40;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:36:"WPTavern: Why Gutenberg and Why Now?";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=78707";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:46:"https://wptavern.com/why-gutenberg-and-why-now";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:17921:"<img />\n\n<p>Tevya Washburn has been building websites for more than 20 years and building them on WordPress for 10. He bootstrapped his website maintenance and support company, <a href="http://wordx.press">WordXpress</a>, that he’s worked on full-time for more than seven years.</p>\n\n<p>Late last year he launched his first premium plugin, and presented at WordCamp Salt Lake City. He lives in Caldwell, ID and is the founding member of the WordPress Meetup group in Western Idaho. <br /></p>\n\n<hr />\n\n<p>It was only a few months ago that I knew almost nothing about WordPress’ new Gutenberg editor. I had a basic concept of what it was and this vague annoyance that it would mean I’d have to learn new things and probably put a lot of effort into making some sites or projects work with it.</p>\n\n<p>I kept hearing all of the frustration and issues with Gutenberg itself and the lack of information on how to integrate with it. At <a href="https://wordx.press">WordX</a>p<a href="https://wordx.press">ress</a> we recently pivoted away from designing websites. When we designed them in the past, we used premium themes. I figured Gutenberg was the theme developer’s problem.<br /></p>\n\n<p>I still had this feeling of dread though, knowing many of my favorite plugins might not add support for it. I also felt some apprehension that even if the themes we use did add support for it, they might have a lot of new bugs through the first few releases.</p>\n\n<p>Then I launched my first WordPress plugin, <a href="https://starfishwp.com">Starfish Reviews</a>, and suddenly they weren’t someone else’s problems anymore! Now I’d have to come up with a plan to integrate our plugin with Gutenberg. I installed the Gutenberg plugin on a test site where we were testing our plugin with the nightly releases of WordPress and started playing around with it.</p>\n\n<p>I was pleasantly surprised at how intuitive and easy it was to use! Now it wasn’t (and isn’t) finished, so there were bugs and annoyances, but overall I was impressed.</p>\n\n<p>Around the same time, I suggested we should have someone present on Gutenberg at our local meetup. My brief experience was more than what anyone else had, so the responsibility fell on me. Preparing for the presentation forced me to look at Gutenberg more carefully and pay more attention to the information and debate going on throughout the community.</p>\n\n<p>I started reading blog posts, paying more attention in podcasts, and even looking at what was being said on Twitter. I watched the State of the Word at WordCamp US where the general tide in the feelings toward Gutenberg, seemed to turn, though many people still remain skeptical, critical, or antagonistic toward the project as a whole.</p>\n\n<p>Today, I saw someone suggesting legal action if Gutenberg caused problems on their sites. That’s ridiculous on several levels, but shows that there’s still a lot of suspicion, frustration, and outright anger around Gutenberg.</p>\n\n<p><em>A couple notes: 1. the graphs below are for illustration purposes only, they’re not meant to be accurate to any actual data. 2. If you prefer listening, you can </em><a href="https://www.youtube.com/watch?v=S4ZqrVJ465E"><em>watch my screencast version</em></a><em> (13:12) of what follows. The message is the same, but differs in many aspects of presentation.</em></p>\n\n<h2><strong>Finding the Why</strong></h2>\n\n<p>Simon Sinek is known for <a href="https://www.youtube.com/watch?v=u4ZoJKF_VuA">his Ted talk</a> where he explains that most people explain a new product or service by talking about ‘what’ it is and ‘how’ it works, but they rarely explain the ‘why’ behind it. The ‘why’ actually resonates with people the most. They want to understand the reason and beliefs behind it.</p>\n\n<p>In my research, I couldn’t seem to find a clear answer to the most important question: “Why Gutenberg?” If I was going to present to people who knew little or nothing about it, I wanted to provide a reason why this major change was coming that might cause significant frustration, work, and pain for them.</p>\n\n<img />\n\n<p>I found a lot of ‘what’ and ‘how’ about Gutenberg. In some posts by <a href="https://ma.tt/2017/08/we-called-it-gutenberg-for-a-reason/">Matt Mullenweg</a> and <a href="https://matiasventura.com/post/gutenberg-or-the-ship-of-theseus/">Matías Ventura</a>, I found hints about ‘why’ Gutenberg existed, but no really clear, simple explanation of why this whole project was happening. Why would Matt and others want to seemingly force this major change on us all? Why does it have to be such a radical departure from the past? Why now?</p>\n\n<p>I was certain the conspiracy theorists—who seem to believe that Automattic’s sole mission is to make their lives more miserable—were wrong. But what was the purpose? Could it really just be a <strong>me too</strong> attitude that left all of these brilliant minds feeling like they had to keep up with Squarespace and Medium? That didn’t seem to fit. Especially since Gutenberg is already leagues better than Squarespace’s convoluted visual editor.</p>\n\n<h2><strong>Innovative Disruption</strong></h2>\n\n<img />\n    The Innovator&#x27;s Dilemma Book Cover\n\n\n<p>Taking cues from those hints and suggestions, I started thinking about the innovative disruption model. It was popularized in business circles, starting in 1997 when the book “<a href="https://en.wikipedia.org/wiki/The_Innovator%27s_Dilemma">The Innovator’s Dilemma</a>” was published by Clayton Christensen, a Harvard professor. His book was an expansion of an <a href="https://hbr.org/1995/01/disruptive-technologies-catching-the-wave">earlier article</a> in the Harvard Business Review.</p>\n\n<p>At the risk of oversimplifying the model, innovative disruption is what happens when an existing company who is the top dog (either in sales or market share) gets comfortable with their position at the top along with their revenue stream and quits innovating. They make small, incremental updates to their products or services to keep customers happy, but fail to look at the future of their industry.</p>\n\n<p>This makes it easier for a startup or smaller, more innovative company to bring a new product or service to market that completely disrupts the existing market because it’s better, faster, cheaper. The established company doesn’t see the disruption coming because they feel secure in their large market share and steady sales revenue. They often respond with “why would anyone want that?” when approached with the new model that is about to completely upset their business model.</p>\n\n<h2><strong>Blockbuster Gets Busted</strong></h2>\n\n<p>The classic example of this is Blockbuster Entertainment, Inc. They had over 9,000 stores at one time, allowing people to rent VHS tapes and later, DVDs. They had a huge portion of the market all to themselves and it seemed nobody could compete with this juggernaut.</p>\n\n<p>Then along came two small startups: Netflix and Redbox. Netflix comes along and says “we’re going to stream movies over the internet. That’s the future and the way everyone will want to consume movies and TV in the future. But since the internet is too slow right now, we’ll just start by mailing DVDs to people.”</p>\n\n<p>Blockbuster looked at this and said, “the internet is <em>way</em> too slow to stream movies. That’s ridiculous! Who wants to wait two weeks to get a movie in the mail?! Hahaha! Stupid startup, they’re wasting their money and energy.” In hindsight this seems ridiculous. At the time, most people would have agreed with Blockbuster.</p>\n\n<p>As you know, people started changing the way they rented movies. Once they tried it, they were happy to pay a subscription and use a queue to get DVDs delivered in the mail. Ultimately, making the decision of what to watch ahead of time was better than wandering through a cathedral of DVDs only to find the one you wanted to watch has already been checked out.</p>\n\n<p>Consumer internet bandwidth speeds quickly caught up. Netflix even invented some of the technologies that provide high quality streaming video to your home. Now, most of us can’t imagine having to go to the store to rent a physical copy of a movie. And those that can, get them from a Redbox kiosk that has a limited selection, but is much quicker and easier than a video store. Netflix now has a larger market share than Blockbuster ever did, with <em>zero</em> physical locations.</p>\n\n<img />\n\n<p>There are exactly nine Blockbuster stores still operating, mostly in Alaska. From 9,000 down to nine in only a few years! This is what failing to innovate does. This is how comfort and confidence in market share and sales blinds people and organizations to the coming innovations that will disrupt their market.</p>\n\n<h2><strong>Literacy, Disruption, and Gutenberg</strong></h2>\n\n<p>Disruptive innovation doesn’t apply just in business. I have a Bachelor’s degree in history. So one example I love to use is how literacy and education ultimately toppled monarchies and traditional power structures in favor of republics and representative democracy.</p>\n\n<p>The choice of Gutenberg as the name of the new WordPress editor seems prescient in this example as well. The name was one of the clues that led me to answer the ‘why?’ question. It was Johannes Gutenberg and his movable type printing press that was the innovative disruption that changed everything!</p>\n\n<p>Before that, the vast majority of people in Europe were illiterate and uneducated. The scarcity of books and written material made it impractical and prohibitively expensive for most people to learn to read. It also allowed the Church and aristocracy to control the opportunity to become literate. That meant the rich and powerful were the gatekeepers of knowledge. Most riots and uprisings to this point were about hunger.</p>\n\n<p>The Gutenberg press changed all that. Suddenly books could be mass-produced faster, cheaper, better than they ever could before. Literacy caught on like a wildfire. The power structures thought they could control it and maintain the status quo. They outlawed printing without state approval and did many other things to limit the spread of ideas through printed materials.</p>\n\n<p>But it was too late, the power to spread ideas that the printing press provided was much too viral. Many printing presses were operated illegally, then destroyed when they were discovered by authorities.</p>\n\n<img />\n\n<p>The tipping point had been reached though. The ability to read and spread ideas via printed documents was much more powerful than the money, soldiers, and weapons of the monarchy. Though hunger might have sparked riots and uprisings from this time on, those tiny flames were fanned into an inferno of revolution by ideas spread through printed words. <a href="https://en.wikipedia.org/wiki/Thomas_Paine">Thomas Paine</a>’s Common Sense is a great example if you want to learn more about concrete examples.</p>\n\n<h2><strong>The Pain of Disrupting Yourself</strong></h2>\n\n<p>I don’t have a business degree, but from my understanding, <em>The Innovator’s Dilemma</em> can be simplified down to this: to survive, and stay on top, a company (or software, or community) must innovate. It <em>can not</em> be incremental innovation. It <em>must</em> be innovation that disrupts the company’s core product or business model, even to the point of entirely replacing it.</p>\n\n<p>Blockbuster tried some Redbox-like and Netflix-like solutions, but they were too little, too late. The only way they could have survived would have been to disrupt their own business model and service. They would have had to say, “in five years we will close all 9k stores and completely shift our business to providing video online.”</p>\n\n<p>Who does that? Who thinks “we have built an empire, but we have to completely change it and replace it all over again”? That’s “The Innovator’s Dilemma” that the book’s title refers to: it’s incredibly difficult to think in those terms when you’re on the top. It’s nearly impossible to say, “we have to disrupt ourselves. We must compete with our own business and products and services.” But ultimately it’s the only way to survive.</p>\n\n<p>…Or you can buy an innovative company and let them disrupt your main business. Did you know Blockbuster had the <a href="http://www.businessinsider.com/blockbuster-ceo-passed-up-chance-to-buy-netflix-for-50-million-2015-7">chance to buy Netflix</a> for $50 million in 2000? It was pocket change, but they passed because it was a very small, niche business.</p>\n\n<p>Had they bought Netflix and allowed it to continue innovating and disrupting their core retail rental model, Blockbuster might still be around. It wouldn’t have 9k retail stores, but it would have an even larger market share than it ever did renting DVDs.</p>\n\n<img />\n\n<p>In either case, the process is painful. That’s why it’s called disruptive. Not because it’s a walk on the beach or small speed bump, but because it takes a lot of work and forward-thinking and causes a lot of pain to create and implement.</p>\n\n<p>If you are the market leader, you can’t rest on your previous success. You have to change everything once again, like you did to get to where you are now. Despite the pain of doing it, you have to invest yourself and your resources into hard work and difficult questions and challenging thinking that goes directly counter to our natural tendency as humans. If you want to stay on top, it’s the only way.</p>\n\n<h2><strong>WordPress is Ripe for Disruption</strong></h2>\n\n<p>WordPress has a 30% market share right now. It won’t be long before 1 out of every 3 websites is built on WordPress. No other platform is even close.</p>\n\n<p>As WordPress professionals and community members, it seems like we have all the momentum and benefits of being the leader. “Surely nothing could displace WordPress!” That’s what Blockbuster said. That’s what monarchs of past ages said. The truth is simple: “yes, something could. In fact, something will, if WordPress doesn’t innovatively disrupt itself.”</p>\n\n<img />\n\n<p>Is it going to be painful? Yes. Is it going to cause a lot of work and effort on the part of the community? Yes! Absolutely. But the alternative is to learn a totally new platform in five years when WordPress dies like Blockbuster did. You think this change is going to be difficult? Try throwing out WordPress entirely and moving your website(s) to an entirely new platform. Because that’s the alternative.</p>\n\n<h2><strong>Good Arguments Against Gutenberg</strong></h2>\n\n<p>I see many people listing a string of bugs in the Gutenberg UI/UX and concluding that Gutenberg shouldn’t exist. I see others critiquing the underlying technologies and claiming that’s evidence that Gutenberg is entirely wrong.</p>\n\n<p>I’m sorry, but those arguments are entirely invalid. They may be great arguments for how Gutenberg needs to change or improve, but they are <em>not</em> valid arguments against the existence of Gutenberg and its inclusion in core.</p>\n\n<p>Hopefully, I’ve made it clear that WordPress is in dire need of innovation. If that’s true, then as I see it, there’s only one really great argument against Gutenberg. As one person in one of the meetups I presented at put it: “is it the right innovation?”</p>\n\n<p>That&#x27;s the crux of the whole thing: <strong>WordPress must innovate to survive</strong>. Matt Mullenweg and the entire Gutenberg team have looked at the past and the future and decided that a better, faster, easier user interface and experience, are the disruptive innovations that WordPress needs to survive.</p>\n\n<p>You can argue that it’s not, that there’s some other innovation that will completely change WordPress and thereby save it from disruption by outside forces. And that&#x27;s a totally valid argument to make. But in my opinion, <strong>you can’t argue that continued, incremental changes are enough.</strong> You can’t argue that the path we’ve been on the last five years is going to keep WordPress on top for the next five years. It simply won’t.</p>\n\n<h2><strong>I Like Gutenberg, but I Love What it’s Doing</strong></h2>\n\n<p>In my experience thus far, I like Gutenberg. I believe it is the right disruptive innovation WordPress needs at this time. It will make WordPress easier to use and help its underpinnings be ready for the future. Being easy to use is what got WordPress where it is today.</p>\n\n<p>It’s not very easy to use any more. There are significantly easier options out there, that could disrupt WordPress and replace it. I think Gutenberg will allow WordPress to disrupt itself and keep ahead of other disruptive innovations. It will save WordPress and allow us all to keep using it and building our businesses on it for another 10 years into the future.</p>\n\n<p>I like Gutenberg, but I really love what Gutenberg means, what it represents, and what it&#x27;s doing. Gutenberg is bigger than just a new post editor, it shows that the leaders of the WordPress community are willing to make hard decisions and innovate even when it means disrupting their own work and previous innovations.</p>\n\n<img />\n\n<p>I have huge respect for the Gutenberg team, who have not only had to rethink everything and do all those difficult things I referred to before, but have had to do it all very publicly, while navigating a gauntlet of criticism, personal attacks, and much more.</p>\n\n<p>I hope this post shows my thanks and newfound appreciation for what they’re doing and going through. Flipping the phrase from <em>The Dark Knight,</em> the members of the Gutenberg team are “the heroes the WordPress community needs right now, even if they’re not the ones we deserve.”</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 26 Mar 2018 18:20:04 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Jeff Chandler";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:41;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:65:"Post Status: The Future of Content Distribution — Draft podcast";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:31:"https://poststatus.com/?p=44599";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:65:"https://poststatus.com/future-content-distribution-draft-podcast/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:2385:"<p>Welcome to the Post Status <a href="https://poststatus.com/category/draft">Draft podcast</a>, which you can find <a href="https://itunes.apple.com/us/podcast/post-status-draft-wordpress/id976403008">on iTunes</a>, <a href="https://play.google.com/music/m/Ih5egfxskgcec4qadr3f4zfpzzm?t=Post_Status__Draft_WordPress_Podcast">Google Play</a>, <a href="http://www.stitcher.com/podcast/krogsgard/post-status-draft-wordpress-podcast">Stitcher</a>, and <a href="http://simplecast.fm/podcasts/1061/rss">via RSS</a> for your favorite podcatcher. Post Status Draft is hosted by Brian Krogsgard and co-host Brian Richards.</p>\n<p><span>This week the Brians put their brains together and discuss content distribution across various mediums and platforms as well as subscriptions for both digital and physical products. The conversation shifts between different tooling and platforms that exist for enabling content distribution as well as some of the societal shifts that have shaped how we share and consume both content and products. </span></p>\n<p><span>This is a good episode for anyone who is developing sites and selling solutions around content distribution or subscriptions as well as anyone who is running (or looking to run) a business based around a subscriber model (paid or otherwise).</span></p>\n<p></p>\n<h3>Links</h3>\n<ul>\n<li><a href="https://make.wordpress.org/marketing/2018/02/28/wordpress-jargon-glossary/">WP Jargon Glossary</a></li>\n<li><a href="https://www.blog.google/topics/google-news-initiative/announcing-google-news-initiative/">Google News subscription initiative</a></li>\n<li><a href="https://woocommerce.com/2018/02/succeed-with-woocommerce-subscriptions-technical-tips/">Brent&#8217;s blog post</a></li>\n<li><a href="https://woocommerce.com/products/teams-woocommerce-memberships/">Teams for WooCommerce Memberships</a></li>\n<li><a href="https://www.recode.net/2017/12/13/16771646/target-shipt-acquisition-price-550-million-grocery-delivery-same-day">Target acquires Shipt</a></li>\n</ul>\n<h3>Sponsor: Pagely</h3>\n<p><a href="https://pagely.com"><span>Pagely</span></a><span> offers best in class managed WordPress hosting, powered by the Amazon Cloud, the Internet’s most reliable infrastructure. Post Status is proudly hosted by Pagely. Thank you to </span><a href="https://pagely.com"><span>Pagely</span></a><span> for being a Post Status partner</span></p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 26 Mar 2018 13:24:53 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:14:"Katie Richards";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:42;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:20:"BuddyPress: 10 years";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:32:"https://buddypress.org/?p=271550";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:40:"https://buddypress.org/2018/03/10-years/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:5437:"<p>In 2008 (just 10 short years ago) <a href="https://profiles.wordpress.org/apeatling">Andy Peatling</a> made the very first code-commit to the newly adopted BuddyPress project, joining bbPress, GlotPress, and BackPress at the time. As most of you can probably imagine, BuddyPress was a different piece of software back then, trying to solve a completely different decade&#8217;s worth of problems for a completely different version of WordPress.</p>\n<p>BuddyPress was multisite only, meaning it did not work on the regular version of WordPress that most people were accustomed to installing. It needed to completely take over the entire website experience to work, with a specific theme for the primary part of your site, and blog themes for user profiles and everything else.</p>\n<p>There was a lot to love about the original vision and version of BuddyPress. It was ambitious, but in a clever kind of way that made everyone tilt their heads, squint their eyes, and ponder what WordPress was capable of. BuddyPress knew exactly what it was trying to do, and owned it without apologies.</p>\n<p>It touted itself as a &#8220;Social Network in a box&#8221; at a time when MySpace was generating 75.9 million <em>unique</em> visitors per month, so if you couldn&#8217;t imagine how different BuddyPress may have been before, imagine how excited everyone was at the idea of owning their own MySpace.</p>\n<p>Since then, Andy invited <a href="https://profiles.wordpress.org/boonebgorges">Boone</a>, <a href="https://profiles.wordpress.org/djpaul">Paul</a>, and <a href="https://profiles.wordpress.org/johnjamesjacoby">me</a> to help lead the project forward, and in-turn we&#8217;ve invited several other prolific BuddyPress contributors to help with every aspect of the project, website, design, and so on.</p>\n<p>The BuddyPress team has grown in a few different ways. Most recently, we&#8217;ve added <a href="https://profiles.wordpress.org/espellcaste">Renato Alves</a> to the team to help with WP-CLI support. Renato is a long-time contributor who stepped up big-time to really own the WP-CLI implementation and finally see it through to the end.</p>\n<p><a href="https://profiles.wordpress.org/slaffik">Slava Abakumov</a> lead the 2.8 release, and we finally met in person for the very first time just last week at WordCamp Miami. He&#8217;s another long-time contributor who has always had the best interests of the project in mind and at heart.</p>\n<p><a href="https://profiles.wordpress.org/offereins">Laurens Offereins</a> has been helping fix BuddyPress bugs and work on evolving features since version 2.1, and while we haven&#8217;t met in person <em>yet</em>, I look forward to it someday!</p>\n<p><a href="https://profiles.wordpress.org/netweb">Stephen Edgar</a> (who you may recognize from bbPress) also works a bit on BuddyPress, largely around tooling &amp; meta related things, but he&#8217;s fully capable and will jump in and help anywhere he can, be it the forums or features.</p>\n<p><a href="https://profiles.wordpress.org/mercime">Mercime</a> would prefer I not blather on endlessly here about how important she is, or how much I appreciate her, or anything like that, so please forget I mentioned it.</p>\n<p><a href="https://profiles.wordpress.org/hnla">Hugo Ashmore</a> has spent the past 2 years completely rebuilding the default template pack. This is an absolutely huge undertaking, and everyone is really excited about sunsetting ye olde <code>bp-legacy</code>.</p>\n<p><a href="https://profiles.wordpress.org/karmatosed">Tammie Lister</a> has moved on to work on the enormously important and equally ambitious <a href="https://wordpress.org/plugins/gutenberg/">Gutenberg</a> project. Tammie is wonderful, and doing a great job crafting what the future of democratizing publishing is.</p>\n<p>Lastly, a few of our veteran team members took sabbaticals from contributing to BuddyPress in the past few years, which I see as an opportunity to return with fresh ideas and perspectives, or maybe moving onto new &amp; exciting challenges. This is a good, healthy thing to do, both for oneself and the project. Space makes the heart grow fonder, and all that.</p>\n<hr />\n<p>A small aside but worth saying here &amp; now, is that leading an open-source project is everything you think it is (or maybe have read already that it is) and like a million other things that are hard to understand until you understand. The one constant (and subsequently the hardest and funnest part) is how to provide opportunities for personal growth, without prohibiting contributions, while also doing what&#8217;s best for the greater vision of the project itself, amongst a completely remote group of bespoke volunteers. I think Paul, Boone, and I do OK at this, but we are always learning and adjusting, so please reach out to us if there is anything we can do differently or better.</p>\n<hr />\n<p>BuddyPress is my personal favorite piece of software. It&#8217;s my favorite community. I wake up excited every day because of what it can do and who it does it for. Put another way, I love what we make it do and who we make it for: ourselves, one another, each other, and you.</p>\n<p>Cheers to 10 years, and here&#8217;s to another 10!</p>\n<p><img class="alignnone wp-image-271562 size-full" src="https://buddypress.org/wp-content/uploads/1/2018/03/Screen-Shot-2018-03-25-at-4.38.12-PM.png" alt="" width="898" height="452" /></p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Sun, 25 Mar 2018 22:54:51 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:3:"JJJ";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:43;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:54:"WPTavern: Noteworthy Changes Coming in WordPress 4.9.5";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=78611";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:65:"https://wptavern.com/noteworthy-changes-coming-in-wordpress-4-9-5";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:2401:"<p>WordPress 4.9.5 Beta 1 <a href="https://make.wordpress.org/core/2018/03/21/wordpress-4-9-5-beta/">is available</a> for testing and brings with it 23 bug fixes and improvements. A release candidate is scheduled for release on March 20th and a final release on April 3rd. Here are some notable changes you can expect in the release.</p>\n\n<h3>"Cheatin’ uh?" Error Message is Replaced</h3>\n\n<p>The "Cheatin’ uh?" error message has existed in WordPress for years and for some, is insulting. The error doesn&#x27;t explain what went wrong and accuses the user of trying to cheat the system.</p>\n\n<img />\n    Cheatin&#x27; Uh Error Message<br />\n\n\n<p>Eric Meyer highlighted the error in <a href="https://wordpress.tv/2016/06/24/eric-a-meyer-design-for-real-life/">his keynote</a> at WordCamp North East Ohio in 2016, when talking about Designing for Real Life. He also <a href="https://core.trac.wordpress.org/ticket/38332#comment:11">contributed to the ticket</a> with suggestions on how to improve the wording.</p>\n\n<p>In WordPress 4.9.5, the error <a href="https://core.trac.wordpress.org/ticket/38332">has been changed</a> to more meaningful messages depending on the error that occurs.</p>\n\n<h3>Recommended PHP Version Increased to 7.2</h3>\n\n<p>Inside of the readme file in WordPress, the current recommended PHP version is 7.0. This version of PHP reached end of life last December. In 4.9.5, the recommend version is PHP 7.2. This is the same version that is <a href="https://wordpress.org/about/requirements/">recommended on WordPress.org</a>.</p>\n\n<h3>Offensive Lyrics Removed From Hello Dolly</h3>\n\n<p>As we covered <a href="https://wptavern.com/without-context-some-lyrics-inside-the-hello-dolly-plugin-are-degrading-to-women">earlier this week</a>, some of the lines displayed in the dashboard from the Hello Dolly plugin are inappropriate without context. In 4.9.5, the plugin will no longer display those lines.</p>\n\n<p>There&#x27;s a possibility that in the future, there will be a musical note icon or symbol placed next to the line to indicate it&#x27;s from a song. In addition, the lyrics are more in line with Louis Armstrong&#x27;s recording.</p>\n\n<p>To see a full list of changes in WordPress 4.9.5, you can <a href="https://core.trac.wordpress.org/query?status=closed&milestone=4.9.5&group=component">view a full list</a> of closed tickets on Trac. </p>\n\n<p><br /></p><br /></p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 22 Mar 2018 21:32:41 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Jeff Chandler";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:44;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:47:"WPTavern: WPWeekly Episode 309 – All AMPed Up";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:58:"https://wptavern.com?p=78601&preview=true&preview_id=78601";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:54:"https://wptavern.com/wpweekly-episode-309-all-amped-up";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:2180:"<p>In this episode, I&#8217;m joined by <a href="https://medinathoughts.com/">Alberto Medina</a>, Developer Advocate working with the Web Content Ecosystems Team at Google, and <a href="https://weston.ruter.net/">Weston Ruter</a>, CTO of XWP. We have a candid conversation about <a href="https://www.ampproject.org/">Google&#8217;s AMP Project</a>. We start by learning why the project was created, what its main goal is, and the technology behind it.</p>\n<p>We also dive into some of the controversy surrounding the project by discussing whether or not AMP is a threat to the Open Web. Medina and Ruter provide insight into AMP&#8217;s transformation from focusing on the mobile web to providing a great user experience across the entire web. Last but not least, we learn about the relationship between Automattic, XWP, and the AMP team and how it&#8217;s helping to shape the future of the project.</p>\n<h2>Notable Links Mentioned:</h2>\n<p><a href="https://wordpress.org/plugins/amp/">AMP for WordPress Plugin</a><br />\n<a href="https://github.com/Automattic/amp-wp">AMP for WordPress GitHub Repository</a><br />\n<a href="https://github.com/ampproject">AMP GitHub Repository</a><br />\n<a href="https://www.youtube.com/watch?v=GGS-tKTXw4Y">Video presentation from AMP Conf 2018 showcasing the work that&#8217;s gone into the AMP for WordPress plugin</a><br />\n<a href="https://www.ampproject.org/latest/blog/standardizing-lessons-learned-from-amp/">Official blog post outlining the future of the AMP Project</a></p>\n<h2>WPWeekly Meta:</h2>\n<p><strong>Next Episode:</strong> Wednesday, March 28th 3:00 P.M. Eastern</p>\n<p>Subscribe to <a href="https://itunes.apple.com/us/podcast/wordpress-weekly/id694849738">WordPress Weekly via Itunes</a></p>\n<p>Subscribe to <a href="https://www.wptavern.com/feed/podcast">WordPress Weekly via RSS</a></p>\n<p>Subscribe to <a href="http://www.stitcher.com/podcast/wordpress-weekly-podcast?refid=stpr">WordPress Weekly via Stitcher Radio</a></p>\n<p>Subscribe to <a href="https://play.google.com/music/listen?u=0#/ps/Ir3keivkvwwh24xy7qiymurwpbe">WordPress Weekly via Google Play</a></p>\n<p><strong>Listen To Episode #309:</strong><br />\n</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 22 Mar 2018 14:34:21 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Jeff Chandler";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:45;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:25:"Matt: Don’t Like Change";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:22:"https://ma.tt/?p=47998";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:39:"https://ma.tt/2018/03/dont-like-change/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:407:"<blockquote class="wp-block-quote">\n    <p>If you don&#x27;t like change, you&#x27;re going to like irrelevance even less.</p><cite>General Eric Shinseki</cite></blockquote>\n\n<p>I actually heard this on the <a href="https://www.fs.blog/2015/06/michael-lombardi/">Farnam Street podcast with Patriots coach Michael Lombardi</a>, but it seems like General Shinseki said it first so attributing it there.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 22 Mar 2018 00:01:23 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:4:"Matt";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:46;a:6:{s:4:"data";s:11:"\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:34:"HeroPress: Keeping Community Alive";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://heropress.com/?p=2487";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:113:"https://heropress.com/keeping-community-alive/#utm_source=rss&utm_medium=rss&utm_campaign=keeping-community-alive";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:3425:"<img width="960" height="480" src="https://s20094.pcdn.co/wp-content/uploads/2016/09/090716-David-Laietta-1024x512.jpg" class="attachment-large size-large wp-post-image" alt="Pull Quote: Be a pillar of support for your community." /><p>In the last year or so I&#8217;ve been a lot more involved with the business side of WordPress than the community side. The business side isn&#8217;t nearly as loving and supportive as the community side, and some of that is out of necessity. Business is business, and people need to eat.</p>\n<p>The problem comes when people get so focused on the business side of things that they forget they&#8217;re dealing with people. Recently <a href="https://twitter.com/carlhancock/status/971182969514799105">Carl Hancock mentioned on twitter</a> that there are things that happen in business in the WordPress community that would horrify people. I don&#8217;t know who those people are that do those things, and I don&#8217;t even know what the things are, but I have hope that the community can be bigger and better than that.</p>\n<p>There will always be selfish jerks who abuse the system for personal gain, but I have hope that the WordPress community can generally rise above that, and perhaps even change the hearts of poor players.</p>\n<p>This week&#8217;s HeroPress replay is from David Laietta, about how our community changes lives.</p>\n<blockquote class="wp-embedded-content"><p><a href="https://heropress.com/essays/a-community-of-acceptance/">A Community of Acceptance</a></p></blockquote>\n<p></p>\n<div class="rtsocial-container rtsocial-container-align-right rtsocial-horizontal"><div class="rtsocial-twitter-horizontal"><div class="rtsocial-twitter-horizontal-button"><a title="Tweet: Keeping Community Alive" class="rtsocial-twitter-button" href="https://twitter.com/share?text=Keeping%20Community%20Alive&via=heropress&url=https%3A%2F%2Fheropress.com%2Fkeeping-community-alive%2F" rel="nofollow" target="_blank"></a></div></div><div class="rtsocial-fb-horizontal fb-light"><div class="rtsocial-fb-horizontal-button"><a title="Like: Keeping Community Alive" class="rtsocial-fb-button rtsocial-fb-like-light" href="https://www.facebook.com/sharer.php?u=https%3A%2F%2Fheropress.com%2Fkeeping-community-alive%2F" rel="nofollow" target="_blank"></a></div></div><div class="rtsocial-linkedin-horizontal"><div class="rtsocial-linkedin-horizontal-button"><a class="rtsocial-linkedin-button" href="https://www.linkedin.com/shareArticle?mini=true&url=https%3A%2F%2Fheropress.com%2Fkeeping-community-alive%2F&title=Keeping+Community+Alive" rel="nofollow" target="_blank" title="Share: Keeping Community Alive"></a></div></div><div class="rtsocial-pinterest-horizontal"><div class="rtsocial-pinterest-horizontal-button"><a class="rtsocial-pinterest-button" href="https://pinterest.com/pin/create/button/?url=https://heropress.com/keeping-community-alive/&media=https://heropress.com/wp-content/uploads/2016/09/090716-David-Laietta-150x150.jpg&description=Keeping Community Alive" rel="nofollow" target="_blank" title="Pin: Keeping Community Alive"></a></div></div><a rel="nofollow" class="perma-link" href="https://heropress.com/keeping-community-alive/" title="Keeping Community Alive"></a></div><p>The post <a rel="nofollow" href="https://heropress.com/keeping-community-alive/">Keeping Community Alive</a> appeared first on <a rel="nofollow" href="https://heropress.com">HeroPress</a>.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 21 Mar 2018 15:39:29 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:47;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:77:"WPTavern: A Plea For Plugin Developers to Stop Supporting Legacy PHP Versions";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=78533";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:88:"https://wptavern.com/a-plea-for-plugin-developers-to-stop-supporting-legacy-php-versions";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:2188:"<p>Iain Poulson has <a href="https://deliciousbrains.com/legacy-php-version-support/">published a thoughtful request</a> on the Delicious Brains blog asking WordPress plugin developers to stop supporting legacy PHP versions. He covers some of the benefits of developing with newer versions of PHP, what Delicious Brains is doing with its plugins, and using the <a href="https://make.wordpress.org/plugins/2017/08/29/minimum-php-version-requirement/">Requires Minimum PHP Version header</a> in readme.txt.<br /></p>\n\n<blockquote class="wp-block-quote">\n    <p>While we wait for the Trac discussion to roll on and the WordPress development wheels to turn we can take action ourselves in our plugins to stop them working on installs that don’t meet our requirements. </p>\n    <p>We do this in our own plugins where it is strictly necessary (<a href="https://deliciousbrains.com/wp-offload-s3/">WP Offload S3</a> relies on the Amazon Web Services S3 SDK, which requires PHP 5.3.3+ and will we will <a href="https://deliciousbrains.com/wp-offload-s3/doc/php-version-requirements/">move to PHP 5.5</a> in the future), and the more plugins that do this out of choice will help move the needle further.</p><cite>Iain Poulson <br type="_moz" /></cite></blockquote>\n\n<p>Poulson mentions the <a href="https://github.com/WordPress/servehappy">ServeHappy project</a> in his post and it&#x27;s worth a mention here as well. The ServeHappy project was <a href="https://make.wordpress.org/core/2018/01/09/servehappy-roadmap/">launched earlier this year</a> by a group of volunteers.</p>\n\n<p>Its main goal is to reduce the number of WordPress installs running on unsupported PHP versions through education, awareness, and tools to help users update their site&#x27;s PHP versions.</p>\n\n<p>This project is in need of contributors. If you&#x27;re interested, join the #core-php channel on <a href="https://make.wordpress.org/chat/">WordPress Slack</a>. The team has meetings every Monday at 11:00 AM EDT. You can also follow the <a href="https://make.wordpress.org/core/tag/core-php/">#core-php tag</a> on the Make WordPress.org Core site where links to chat logs and meeting summaries are published. </p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 21 Mar 2018 00:31:00 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Jeff Chandler";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:48;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:62:"WPTavern: How to Disable Push Notification Requests in Firefox";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=78475";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:73:"https://wptavern.com/how-to-disable-push-notification-requests-in-firefox";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1246:"<p>Have you noticed how many sites ask if you want to enable push notifications? I&#x27;ve answered no to every request but thanks <a href="https://twitter.com/tkraftner/status/976116234365358081">to a tip</a> suggested by Thomas Kräftner, you can disable requests from appearing altogether in Firefox.</p>\n\n<p>Last week, Mozilla <a href="https://www.mozilla.org/en-US/firefox/59.0/releasenotes/">released Firefox 59.0</a> and added a <a href="https://support.mozilla.org/en-US/kb/push-notifications-firefox">new privacy feature</a> that allows users to block sites from sending push notification requests. To enable it, open the Options panel in Firefox 59.0 and click the Privacy&amp;Security tab.<br /></p>\n\n<p>Scroll down to the Permissions section. Click on the Settings button for Notifications and check the box that says <em>Block new requests asking to allow notifications.﻿</em></p>\n\n<img />\n    Settings panel for Notifications\n\n\n<p>Click the Save Changes button and enjoy one less thing interrupting your browsing experience.  To accomplish the same thing in Chrome, follow <a href="https://fieldguide.gizmodo.com/how-to-block-super-annoying-website-notification-reques-1797499616">this tutorial published by Field Guide</a>. </p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 20 Mar 2018 23:32:52 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Jeff Chandler";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:49;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:91:"WPTavern: Without Context, Some Lyrics Inside the Hello Dolly Plugin Are Degrading to Women";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=78372";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:101:"https://wptavern.com/without-context-some-lyrics-inside-the-hello-dolly-plugin-are-degrading-to-women";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:2914:"<p>There have been <a href="https://core.trac.wordpress.org/ticket/11538">many discussions</a> over the years on whether or not <a href="https://wordpress.org/plugins/hello-dolly/">Hello Dolly</a> should be unbundled with WordPress. Seven years ago, it was <a href="https://core.trac.wordpress.org/ticket/15769">argued</a> that the lyrics are copyrighted and could potentially violate the GPL license.</p>\n\n<p>The latest issue with Hello Dolly is that some lyrics that appear in users dashboards with the plugin activated can be degrading to women without context.</p>\n\n<p><blockquote class="twitter-tweet"><p lang="en" dir="ltr">Find her an empty lap, fellas.<br /><br />Wondering about my <a href="https://twitter.com/WordPress?ref_src=twsrc%5Etfw">@WordPress</a> dashboard. Apparently they''re lyrics. <img src="https://s.w.org/images/core/emoji/2.4/72x72/1f643.png" alt="🙃" class="wp-smiley" /> <img src="https://s.w.org/images/core/emoji/2.4/72x72/1f644.png" alt="🙄" class="wp-smiley" /> <a href="https://t.co/oxNU9czr5X">pic.twitter.com/oxNU9czr5X</a></p>&mdash; Michelle Felt (@michellefelt) <a href="https://twitter.com/michellefelt/status/974060334502719488?ref_src=twsrc%5Etfw">March 14, 2018</a></blockquote></p>\n\n<p>Two examples are:</p>\n\n<ul>\n    <li>Find her an empty lap, fellas</li>\n    <li>Find her a vacant knee, fellas</li>\n</ul>\n\n<p>Joe McGill has <a href="https://core.trac.wordpress.org/ticket/43555">created a trac ticket</a> proposing that those two lines be removed. "The Hello Dolly plugin has been bundled in WordPress for many years, being a simple example of how to build a plugin for WordPress while also adding a bit of whimsy to admin," he said.</p>\n\n<p>"However, there are several passages of text from this song which are inappropriate to display without any context to people using WordPress—particularly as the WordPress project seeks to promote inclusivity for all."</p>\n\n<p>The discussion within the ticket suggests creating a black list or replacing the lyrics with less offensive versions. In many of the Google search results for Hello Dolly lyrics by Jerry Herman, shows that the lyrics inside the plugin and those in the song are different.</p>\n\n<p>The lyrics say, "Find me a vacant knee, fellas." In a <a href="https://www.youtube.com/watch?v=RETJfq1U_gg">video on YouTube</a> of Hello Dolly featuring Sarah Gardner singing the lyrics, she clearly says "Find her an empty lap, fellas." In a YouTube video of <a href="https://www.youtube.com/watch?v=kmfeKUNDDYs">Louis Armstrong singing Hello Dolly live</a>, he says "Find her an empty lap, fellas."<br /></p>\n\n<p>Putting aside the debate of which version of the lyrics are used, displaying the text above without context can and is seen as degrading women. At a time when WordPress and its community are doing what it can to be more inclusive, changing or removing the lyrics seems like an easy win. </p>\n\n<p></p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 16 Mar 2018 20:45:17 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Jeff Chandler";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}}}}}}}}}}}}s:4:"type";i:128;s:7:"headers";O:42:"Requests_Utility_CaseInsensitiveDictionary":1:{s:7:"\0*\0data";a:8:{s:6:"server";s:5:"nginx";s:4:"date";s:29:"Mon, 30 Apr 2018 06:23:57 GMT";s:12:"content-type";s:8:"text/xml";s:4:"vary";s:15:"Accept-Encoding";s:13:"last-modified";s:29:"Mon, 30 Apr 2018 06:15:27 GMT";s:15:"x-frame-options";s:10:"SAMEORIGIN";s:4:"x-nc";s:9:"HIT ord 2";s:16:"content-encoding";s:4:"gzip";}}s:5:"build";s:14:"20130910220210";}', 'no');
INSERT INTO `ops_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(132, '_transient_timeout_feed_mod_d117b5738fbd35bd8c0391cda1f2b5d9', '1525112636', 'no'),
(133, '_transient_feed_mod_d117b5738fbd35bd8c0391cda1f2b5d9', '1525069436', 'no'),
(134, '_transient_timeout_dash_v2_88ae138922fe95674369b1cb3d215a2b', '1525112636', 'no'),
(135, '_transient_dash_v2_88ae138922fe95674369b1cb3d215a2b', '<div class="rss-widget"><ul><li><a class=''rsswidget'' href=''https://wordpress.org/news/2018/04/celebrate-the-wordpress-15th-anniversary-on-may-27/''>Celebrate the WordPress 15th Anniversary on May 27</a></li></ul></div><div class="rss-widget"><ul><li><a class=''rsswidget'' href=''https://ma.tt/2018/04/rent-a-family-in-japan/''>Matt: Rent-A-Family in Japan</a></li><li><a class=''rsswidget'' href=''https://poststatus.com/the-meta-episode-draft-podcast/''>Post Status: The meta episode — Draft podcast</a></li><li><a class=''rsswidget'' href=''https://wptavern.com/a-gutenberg-migration-guide-for-developers''>WPTavern: A Gutenberg Migration Guide for Developers</a></li></ul></div>', 'no'),
(139, 'current_theme', 'blank press 0.1', 'yes'),
(140, 'theme_mods_ozproduction', 'a:3:{i:0;b:0;s:18:"nav_menu_locations";a:0:{}s:18:"custom_css_post_id";i:-1;}', 'yes'),
(141, 'theme_switched', '', 'yes'),
(142, '_transient_timeout_plugin_slugs', '1525181719', 'no'),
(143, '_transient_plugin_slugs', 'a:2:{i:0;s:19:"akismet/akismet.php";i:1;s:9:"hello.php";}', 'no'),
(144, 'recently_activated', 'a:0:{}', 'yes'),
(145, '_site_transient_timeout_poptags_40cd750bba9870f18aada2478b24840a', '1525106131', 'no'),
(146, '_site_transient_poptags_40cd750bba9870f18aada2478b24840a', 'O:8:"stdClass":100:{s:6:"widget";a:3:{s:4:"name";s:6:"widget";s:4:"slug";s:6:"widget";s:5:"count";i:4459;}s:11:"woocommerce";a:3:{s:4:"name";s:11:"woocommerce";s:4:"slug";s:11:"woocommerce";s:5:"count";i:2750;}s:4:"post";a:3:{s:4:"name";s:4:"post";s:4:"slug";s:4:"post";s:5:"count";i:2556;}s:5:"admin";a:3:{s:4:"name";s:5:"admin";s:4:"slug";s:5:"admin";s:5:"count";i:2422;}s:5:"posts";a:3:{s:4:"name";s:5:"posts";s:4:"slug";s:5:"posts";s:5:"count";i:1867;}s:9:"shortcode";a:3:{s:4:"name";s:9:"shortcode";s:4:"slug";s:9:"shortcode";s:5:"count";i:1648;}s:8:"comments";a:3:{s:4:"name";s:8:"comments";s:4:"slug";s:8:"comments";s:5:"count";i:1639;}s:7:"twitter";a:3:{s:4:"name";s:7:"twitter";s:4:"slug";s:7:"twitter";s:5:"count";i:1451;}s:6:"images";a:3:{s:4:"name";s:6:"images";s:4:"slug";s:6:"images";s:5:"count";i:1387;}s:6:"google";a:3:{s:4:"name";s:6:"google";s:4:"slug";s:6:"google";s:5:"count";i:1386;}s:8:"facebook";a:3:{s:4:"name";s:8:"facebook";s:4:"slug";s:8:"facebook";s:5:"count";i:1383;}s:5:"image";a:3:{s:4:"name";s:5:"image";s:4:"slug";s:5:"image";s:5:"count";i:1310;}s:7:"sidebar";a:3:{s:4:"name";s:7:"sidebar";s:4:"slug";s:7:"sidebar";s:5:"count";i:1283;}s:3:"seo";a:3:{s:4:"name";s:3:"seo";s:4:"slug";s:3:"seo";s:5:"count";i:1200;}s:7:"gallery";a:3:{s:4:"name";s:7:"gallery";s:4:"slug";s:7:"gallery";s:5:"count";i:1099;}s:4:"page";a:3:{s:4:"name";s:4:"page";s:4:"slug";s:4:"page";s:5:"count";i:1059;}s:6:"social";a:3:{s:4:"name";s:6:"social";s:4:"slug";s:6:"social";s:5:"count";i:1022;}s:5:"email";a:3:{s:4:"name";s:5:"email";s:4:"slug";s:5:"email";s:5:"count";i:1008;}s:9:"ecommerce";a:3:{s:4:"name";s:9:"ecommerce";s:4:"slug";s:9:"ecommerce";s:5:"count";i:884;}s:5:"login";a:3:{s:4:"name";s:5:"login";s:4:"slug";s:5:"login";s:5:"count";i:875;}s:5:"links";a:3:{s:4:"name";s:5:"links";s:4:"slug";s:5:"links";s:5:"count";i:826;}s:5:"video";a:3:{s:4:"name";s:5:"video";s:4:"slug";s:5:"video";s:5:"count";i:802;}s:7:"widgets";a:3:{s:4:"name";s:7:"widgets";s:4:"slug";s:7:"widgets";s:5:"count";i:801;}s:8:"security";a:3:{s:4:"name";s:8:"security";s:4:"slug";s:8:"security";s:5:"count";i:706;}s:7:"content";a:3:{s:4:"name";s:7:"content";s:4:"slug";s:7:"content";s:5:"count";i:691;}s:3:"rss";a:3:{s:4:"name";s:3:"rss";s:4:"slug";s:3:"rss";s:5:"count";i:684;}s:10:"buddypress";a:3:{s:4:"name";s:10:"buddypress";s:4:"slug";s:10:"buddypress";s:5:"count";i:682;}s:4:"spam";a:3:{s:4:"name";s:4:"spam";s:4:"slug";s:4:"spam";s:5:"count";i:672;}s:6:"slider";a:3:{s:4:"name";s:6:"slider";s:4:"slug";s:6:"slider";s:5:"count";i:655;}s:5:"pages";a:3:{s:4:"name";s:5:"pages";s:4:"slug";s:5:"pages";s:5:"count";i:655;}s:9:"analytics";a:3:{s:4:"name";s:9:"analytics";s:4:"slug";s:9:"analytics";s:5:"count";i:641;}s:6:"jquery";a:3:{s:4:"name";s:6:"jquery";s:4:"slug";s:6:"jquery";s:5:"count";i:639;}s:5:"media";a:3:{s:4:"name";s:5:"media";s:4:"slug";s:5:"media";s:5:"count";i:634;}s:10:"e-commerce";a:3:{s:4:"name";s:10:"e-commerce";s:4:"slug";s:10:"e-commerce";s:5:"count";i:625;}s:4:"feed";a:3:{s:4:"name";s:4:"feed";s:4:"slug";s:4:"feed";s:5:"count";i:610;}s:6:"search";a:3:{s:4:"name";s:6:"search";s:4:"slug";s:6:"search";s:5:"count";i:606;}s:4:"ajax";a:3:{s:4:"name";s:4:"ajax";s:4:"slug";s:4:"ajax";s:5:"count";i:603;}s:4:"form";a:3:{s:4:"name";s:4:"form";s:4:"slug";s:4:"form";s:5:"count";i:594;}s:8:"category";a:3:{s:4:"name";s:8:"category";s:4:"slug";s:8:"category";s:5:"count";i:588;}s:4:"menu";a:3:{s:4:"name";s:4:"menu";s:4:"slug";s:4:"menu";s:5:"count";i:585;}s:5:"embed";a:3:{s:4:"name";s:5:"embed";s:4:"slug";s:5:"embed";s:5:"count";i:563;}s:10:"javascript";a:3:{s:4:"name";s:10:"javascript";s:4:"slug";s:10:"javascript";s:5:"count";i:545;}s:4:"link";a:3:{s:4:"name";s:4:"link";s:4:"slug";s:4:"link";s:5:"count";i:536;}s:3:"css";a:3:{s:4:"name";s:3:"css";s:4:"slug";s:3:"css";s:5:"count";i:531;}s:7:"youtube";a:3:{s:4:"name";s:7:"youtube";s:4:"slug";s:7:"youtube";s:5:"count";i:521;}s:5:"share";a:3:{s:4:"name";s:5:"share";s:4:"slug";s:5:"share";s:5:"count";i:519;}s:7:"comment";a:3:{s:4:"name";s:7:"comment";s:4:"slug";s:7:"comment";s:5:"count";i:511;}s:5:"theme";a:3:{s:4:"name";s:5:"theme";s:4:"slug";s:5:"theme";s:5:"count";i:505;}s:9:"dashboard";a:3:{s:4:"name";s:9:"dashboard";s:4:"slug";s:9:"dashboard";s:5:"count";i:492;}s:6:"editor";a:3:{s:4:"name";s:6:"editor";s:4:"slug";s:6:"editor";s:5:"count";i:491;}s:10:"responsive";a:3:{s:4:"name";s:10:"responsive";s:4:"slug";s:10:"responsive";s:5:"count";i:489;}s:6:"custom";a:3:{s:4:"name";s:6:"custom";s:4:"slug";s:6:"custom";s:5:"count";i:482;}s:10:"categories";a:3:{s:4:"name";s:10:"categories";s:4:"slug";s:10:"categories";s:5:"count";i:479;}s:12:"contact-form";a:3:{s:4:"name";s:12:"contact form";s:4:"slug";s:12:"contact-form";s:5:"count";i:477;}s:3:"ads";a:3:{s:4:"name";s:3:"ads";s:4:"slug";s:3:"ads";s:5:"count";i:469;}s:9:"affiliate";a:3:{s:4:"name";s:9:"affiliate";s:4:"slug";s:9:"affiliate";s:5:"count";i:465;}s:6:"button";a:3:{s:4:"name";s:6:"button";s:4:"slug";s:6:"button";s:5:"count";i:456;}s:4:"tags";a:3:{s:4:"name";s:4:"tags";s:4:"slug";s:4:"tags";s:5:"count";i:454;}s:4:"user";a:3:{s:4:"name";s:4:"user";s:4:"slug";s:4:"user";s:5:"count";i:443;}s:7:"contact";a:3:{s:4:"name";s:7:"contact";s:4:"slug";s:7:"contact";s:5:"count";i:432;}s:6:"mobile";a:3:{s:4:"name";s:6:"mobile";s:4:"slug";s:6:"mobile";s:5:"count";i:425;}s:3:"api";a:3:{s:4:"name";s:3:"api";s:4:"slug";s:3:"api";s:5:"count";i:424;}s:5:"photo";a:3:{s:4:"name";s:5:"photo";s:4:"slug";s:5:"photo";s:5:"count";i:419;}s:5:"users";a:3:{s:4:"name";s:5:"users";s:4:"slug";s:5:"users";s:5:"count";i:415;}s:9:"slideshow";a:3:{s:4:"name";s:9:"slideshow";s:4:"slug";s:9:"slideshow";s:5:"count";i:413;}s:5:"stats";a:3:{s:4:"name";s:5:"stats";s:4:"slug";s:5:"stats";s:5:"count";i:412;}s:6:"events";a:3:{s:4:"name";s:6:"events";s:4:"slug";s:6:"events";s:5:"count";i:406;}s:6:"photos";a:3:{s:4:"name";s:6:"photos";s:4:"slug";s:6:"photos";s:5:"count";i:404;}s:10:"statistics";a:3:{s:4:"name";s:10:"statistics";s:4:"slug";s:10:"statistics";s:5:"count";i:390;}s:7:"payment";a:3:{s:4:"name";s:7:"payment";s:4:"slug";s:7:"payment";s:5:"count";i:388;}s:10:"navigation";a:3:{s:4:"name";s:10:"navigation";s:4:"slug";s:10:"navigation";s:5:"count";i:384;}s:4:"news";a:3:{s:4:"name";s:4:"news";s:4:"slug";s:4:"news";s:5:"count";i:365;}s:8:"calendar";a:3:{s:4:"name";s:8:"calendar";s:4:"slug";s:8:"calendar";s:5:"count";i:363;}s:10:"shortcodes";a:3:{s:4:"name";s:10:"shortcodes";s:4:"slug";s:10:"shortcodes";s:5:"count";i:357;}s:5:"popup";a:3:{s:4:"name";s:5:"popup";s:4:"slug";s:5:"popup";s:5:"count";i:354;}s:9:"marketing";a:3:{s:4:"name";s:9:"marketing";s:4:"slug";s:9:"marketing";s:5:"count";i:348;}s:4:"chat";a:3:{s:4:"name";s:4:"chat";s:4:"slug";s:4:"chat";s:5:"count";i:347;}s:12:"social-media";a:3:{s:4:"name";s:12:"social media";s:4:"slug";s:12:"social-media";s:5:"count";i:346;}s:7:"plugins";a:3:{s:4:"name";s:7:"plugins";s:4:"slug";s:7:"plugins";s:5:"count";i:343;}s:15:"payment-gateway";a:3:{s:4:"name";s:15:"payment gateway";s:4:"slug";s:15:"payment-gateway";s:5:"count";i:341;}s:9:"multisite";a:3:{s:4:"name";s:9:"multisite";s:4:"slug";s:9:"multisite";s:5:"count";i:338;}s:4:"code";a:3:{s:4:"name";s:4:"code";s:4:"slug";s:4:"code";s:5:"count";i:337;}s:10:"newsletter";a:3:{s:4:"name";s:10:"newsletter";s:4:"slug";s:10:"newsletter";s:5:"count";i:337;}s:4:"list";a:3:{s:4:"name";s:4:"list";s:4:"slug";s:4:"list";s:5:"count";i:332;}s:3:"url";a:3:{s:4:"name";s:3:"url";s:4:"slug";s:3:"url";s:5:"count";i:331;}s:4:"meta";a:3:{s:4:"name";s:4:"meta";s:4:"slug";s:4:"meta";s:5:"count";i:329;}s:8:"redirect";a:3:{s:4:"name";s:8:"redirect";s:4:"slug";s:8:"redirect";s:5:"count";i:321;}s:5:"forms";a:3:{s:4:"name";s:5:"forms";s:4:"slug";s:5:"forms";s:5:"count";i:312;}s:6:"simple";a:3:{s:4:"name";s:6:"simple";s:4:"slug";s:6:"simple";s:5:"count";i:304;}s:16:"custom-post-type";a:3:{s:4:"name";s:16:"custom post type";s:4:"slug";s:16:"custom-post-type";s:5:"count";i:303;}s:11:"advertising";a:3:{s:4:"name";s:11:"advertising";s:4:"slug";s:11:"advertising";s:5:"count";i:303;}s:3:"tag";a:3:{s:4:"name";s:3:"tag";s:4:"slug";s:3:"tag";s:5:"count";i:300;}s:7:"adsense";a:3:{s:4:"name";s:7:"adsense";s:4:"slug";s:7:"adsense";s:5:"count";i:297;}s:4:"html";a:3:{s:4:"name";s:4:"html";s:4:"slug";s:4:"html";s:5:"count";i:295;}s:12:"notification";a:3:{s:4:"name";s:12:"notification";s:4:"slug";s:12:"notification";s:5:"count";i:294;}s:8:"tracking";a:3:{s:4:"name";s:8:"tracking";s:4:"slug";s:8:"tracking";s:5:"count";i:292;}s:6:"author";a:3:{s:4:"name";s:6:"author";s:4:"slug";s:6:"author";s:5:"count";i:290;}s:16:"google-analytics";a:3:{s:4:"name";s:16:"google analytics";s:4:"slug";s:16:"google-analytics";s:5:"count";i:290;}s:11:"performance";a:3:{s:4:"name";s:11:"performance";s:4:"slug";s:11:"performance";s:5:"count";i:290;}s:8:"lightbox";a:3:{s:4:"name";s:8:"lightbox";s:4:"slug";s:8:"lightbox";s:5:"count";i:285;}}', 'no'),
(150, '_site_transient_update_plugins', 'O:8:"stdClass":5:{s:12:"last_checked";i:1525095365;s:7:"checked";a:2:{s:19:"akismet/akismet.php";s:5:"4.0.3";s:26:"wp-easycart/wpeasycart.php";s:6:"4.0.37";}s:8:"response";a:0:{}s:12:"translations";a:0:{}s:9:"no_update";a:2:{s:19:"akismet/akismet.php";O:8:"stdClass":9:{s:2:"id";s:21:"w.org/plugins/akismet";s:4:"slug";s:7:"akismet";s:6:"plugin";s:19:"akismet/akismet.php";s:11:"new_version";s:5:"4.0.3";s:3:"url";s:38:"https://wordpress.org/plugins/akismet/";s:7:"package";s:56:"https://downloads.wordpress.org/plugin/akismet.4.0.3.zip";s:5:"icons";a:2:{s:2:"2x";s:59:"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272";s:2:"1x";s:59:"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272";}s:7:"banners";a:1:{s:2:"1x";s:61:"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904";}s:11:"banners_rtl";a:0:{}}s:26:"wp-easycart/wpeasycart.php";O:8:"stdClass":9:{s:2:"id";s:25:"w.org/plugins/wp-easycart";s:4:"slug";s:11:"wp-easycart";s:6:"plugin";s:26:"wp-easycart/wpeasycart.php";s:11:"new_version";s:6:"4.0.37";s:3:"url";s:42:"https://wordpress.org/plugins/wp-easycart/";s:7:"package";s:61:"https://downloads.wordpress.org/plugin/wp-easycart.4.0.37.zip";s:5:"icons";a:2:{s:2:"2x";s:64:"https://ps.w.org/wp-easycart/assets/icon-256x256.png?rev=1015460";s:2:"1x";s:64:"https://ps.w.org/wp-easycart/assets/icon-128x128.png?rev=1015460";}s:7:"banners";a:2:{s:2:"2x";s:67:"https://ps.w.org/wp-easycart/assets/banner-1544x500.png?rev=1738545";s:2:"1x";s:66:"https://ps.w.org/wp-easycart/assets/banner-772x250.png?rev=1738545";}s:11:"banners_rtl";a:0:{}}}}', 'no'),
(151, 'ec_option_is_installed', '1', 'yes'),
(152, 'ec_option_storepage', '5', 'yes'),
(153, 'ec_option_cartpage', '6', 'yes'),
(154, 'ec_option_accountpage', '7', 'yes'),
(155, 'ec_option_default_manufacturer', '1', 'yes'),
(156, 'ec_option_store_locale', 'US', 'yes'),
(157, 'ec_option_setup_wizard_done', '1', 'yes'),
(158, 'ec_option_setup_wizard_step', '5', 'yes'),
(159, 'ec_option_review_complete', '1', 'yes'),
(160, 'ec_option_db_version', '1_30', 'yes'),
(161, 'ec_option_show_lite_message', '1', 'yes'),
(162, 'ec_option_new_linking_setup', '0', 'yes'),
(163, 'ec_option_show_install_message', '0', 'yes'),
(164, 'ec_option_added_custom_post_type', '2', 'yes'),
(165, 'ec_option_hide_admin_notice', '0', 'yes'),
(166, 'ec_option_hide_design_help_video', '0', 'yes'),
(167, 'ec_option_design_saved', '0', 'yes'),
(168, 'ec_option_amfphp_fix', '1', 'yes'),
(169, 'ec_option_track_user_clicks', '1', 'yes'),
(170, 'ec_option_cart_use_session_support', '0', 'yes'),
(171, 'ec_option_admin_display_sales_goal', '1', 'yes'),
(172, 'ec_option_admin_sales_goal', '1', 'yes'),
(173, 'ec_option_newsletter_done', '0', 'yes'),
(174, 'ec_option_allow_tracking', '-1', 'yes'),
(175, 'ec_option_weight', 'lbs', 'yes'),
(176, 'ec_option_base_currency', 'USD', 'yes'),
(177, 'ec_option_currency', '$', 'yes'),
(178, 'ec_option_currency_symbol_location', '1', 'yes'),
(179, 'ec_option_currency_negative_location', '1', 'yes'),
(180, 'ec_option_currency_decimal_symbol', '.', 'yes'),
(181, 'ec_option_currency_decimal_places', '2', 'yes'),
(182, 'ec_option_currency_thousands_seperator', ',', 'yes'),
(183, 'ec_option_show_currency_code', '0', 'yes'),
(184, 'ec_option_default_store_filter', '0', 'yes'),
(185, 'ec_option_default_payment_type', 'manual_bill', 'yes'),
(186, 'ec_option_shipping_type', 'price', 'yes'),
(187, 'ec_option_express_shipping_price', '9.99', 'yes'),
(188, 'ec_option_reg_code', '', 'yes'),
(189, 'ec_option_order_from_email', 'youremail@url.com', 'yes'),
(190, 'ec_option_password_from_email', 'youremail@url.com', 'yes'),
(191, 'ec_option_bcc_email_addresses', 'info@ozproductionservices.com', 'yes'),
(192, 'ec_option_use_state_dropdown', '1', 'yes'),
(193, 'ec_option_use_country_dropdown', '1', 'yes'),
(194, 'ec_option_estimate_shipping_zip', '1', 'yes'),
(195, 'ec_option_estimate_shipping_country', '1', 'yes'),
(196, 'ec_option_stylesheettype', '1', 'yes'),
(197, 'ec_option_googleanalyticsid', 'UA-XXXXXXX-X', 'yes'),
(198, 'ec_option_use_rtl', '0', 'yes'),
(199, 'ec_option_allow_guest', '1', 'yes'),
(200, 'ec_option_use_shipping', '1', 'yes'),
(201, 'ec_option_user_order_notes', '0', 'yes'),
(202, 'ec_option_terms_link', 'http://yoursite.com/termsandconditions', 'yes'),
(203, 'ec_option_privacy_link', 'http://yoursite.com/privacypolicy', 'yes'),
(204, 'ec_option_email_type', 'mail', 'yes'),
(205, 'ec_option_require_account_address', '0', 'yes'),
(206, 'ec_option_use_wp_mail', '0', 'yes'),
(207, 'ec_option_product_layout_type', 'grid_only', 'yes'),
(208, 'ec_option_show_featured_categories', '0', 'yes'),
(209, 'ec_option_product_filter_0', '1', 'yes'),
(210, 'ec_option_product_filter_1', '1', 'yes'),
(211, 'ec_option_product_filter_2', '1', 'yes'),
(212, 'ec_option_product_filter_3', '1', 'yes'),
(213, 'ec_option_product_filter_4', '1', 'yes'),
(214, 'ec_option_product_filter_5', '1', 'yes'),
(215, 'ec_option_product_filter_6', '1', 'yes'),
(216, 'ec_option_product_filter_7', '1', 'yes'),
(217, 'ec_option_show_giftcards', '1', 'yes'),
(218, 'ec_option_show_coupons', '1', 'yes'),
(219, 'ec_option_match_store_meta', '0', 'yes'),
(220, 'ec_option_use_old_linking_style', '1', 'yes'),
(221, 'ec_option_no_vat_on_shipping', '0', 'yes'),
(222, 'ec_option_display_as_catalog', '0', 'yes'),
(223, 'ec_option_addtocart_return_to_product', '0', 'yes'),
(224, 'ec_option_exchange_rates', 'EUR=.73,GBP=.6,JPY=101.9', 'yes'),
(225, 'ec_option_require_email_validation', '0', 'yes'),
(226, 'ec_option_display_country_top', '1', 'yes'),
(227, 'ec_option_use_address2', '1', 'yes'),
(228, 'ec_option_use_smart_states', '1', 'yes'),
(229, 'ec_option_show_account_subscriptions_link', '1', 'yes'),
(230, 'ec_option_gift_card_shipping_allowed', '1', 'yes'),
(231, 'ec_option_collect_shipping_for_subscriptions', '0', 'yes'),
(232, 'ec_option_skip_cart_login', '0', 'yes'),
(233, 'ec_option_use_contact_name', '1', 'yes'),
(234, 'ec_option_collect_user_phone', '1', 'yes'),
(235, 'ec_option_enable_company_name', '1', 'yes'),
(236, 'ec_option_skip_shipping_page', '0', 'yes'),
(237, 'ec_option_skip_reivew_screen', '0', 'yes'),
(238, 'ec_option_require_terms_agreement', '0', 'yes'),
(239, 'ec_option_show_menu_cart_icon', '0', 'yes'),
(240, 'ec_option_cart_menu_id', '0', 'yes'),
(241, 'ec_option_amazon_key', '', 'yes'),
(242, 'ec_option_amazon_secret', '', 'yes'),
(243, 'ec_option_amazon_bucket', '', 'yes'),
(244, 'ec_option_amazon_bucket_region', '', 'yes'),
(245, 'ec_option_deconetwork_url', '', 'yes'),
(246, 'ec_option_deconetwork_password', '', 'yes'),
(247, 'ec_option_tax_cloud_api_id', '', 'yes'),
(248, 'ec_option_tax_cloud_api_key', '', 'yes'),
(249, 'ec_option_tax_cloud_address', '', 'yes'),
(250, 'ec_option_tax_cloud_city', '', 'yes'),
(251, 'ec_option_tax_cloud_state', '', 'yes'),
(252, 'ec_option_tax_cloud_zip', '', 'yes'),
(253, 'ec_option_restrict_store', '0', 'yes'),
(254, 'ec_option_enable_user_notes', '0', 'yes'),
(255, 'ec_option_enable_newsletter_popup', '0', 'yes'),
(256, 'ec_option_use_estimate_shipping', '1', 'yes'),
(257, 'ec_option_use_custom_post_theme_template', '0', 'yes'),
(258, 'ec_option_show_email_on_receipt', '0', 'yes'),
(259, 'ec_option_show_breadcrumbs', '1', 'yes'),
(260, 'ec_option_show_model_number', '1', 'yes'),
(261, 'ec_option_show_categories', '1', 'yes'),
(262, 'ec_option_show_manufacturer', '1', 'yes'),
(263, 'ec_option_send_signup_email', '0', 'yes'),
(264, 'ec_option_show_magnification', '1', 'yes'),
(265, 'ec_option_show_large_popup', '1', 'yes'),
(266, 'ec_option_enable_product_paging', '1', 'yes'),
(267, 'ec_option_show_sort_box', '1', 'yes'),
(268, 'ec_option_customer_review_require_login', '0', 'yes'),
(269, 'ec_option_customer_review_show_user_name', '0', 'yes'),
(270, 'ec_option_hide_live_editor', '0', 'yes'),
(271, 'ec_option_hide_price_seasonal', '0', 'yes'),
(272, 'ec_option_hide_price_inquiry', '0', 'yes'),
(273, 'ec_option_enable_easy_canada_tax', '0', 'yes'),
(274, 'ec_option_collect_alberta_tax', '1', 'yes'),
(275, 'ec_option_collect_british_columbia_tax', '1', 'yes'),
(276, 'ec_option_collect_manitoba_tax', '1', 'yes'),
(277, 'ec_option_collect_new_brunswick_tax', '1', 'yes'),
(278, 'ec_option_collect_newfoundland_tax', '1', 'yes'),
(279, 'ec_option_collect_northwest_territories_tax', '1', 'yes'),
(280, 'ec_option_collect_nova_scotia_tax', '1', 'yes'),
(281, 'ec_option_collect_nunavut_tax', '1', 'yes'),
(282, 'ec_option_collect_ontario_tax', '1', 'yes'),
(283, 'ec_option_collect_prince_edward_island_tax', '1', 'yes'),
(284, 'ec_option_collect_quebec_tax', '1', 'yes'),
(285, 'ec_option_collect_saskatchewan_tax', '1', 'yes'),
(286, 'ec_option_collect_yukon_tax', '1', 'yes'),
(287, 'ec_option_collect_tax_on_shipping', '0', 'yes'),
(288, 'ec_option_show_multiple_vat_pricing', '0', 'yes'),
(289, 'ec_option_hide_cart_icon_on_empty', '0', 'yes'),
(290, 'ec_option_canada_tax_options', '0', 'yes'),
(291, 'ec_option_deconetwork_allow_blank_products', '0', 'yes'),
(292, 'ec_option_ship_items_seperately', '0', 'yes'),
(293, 'ec_option_default_dynamic_sizing', '1', 'yes'),
(294, 'ec_option_subscription_one_only', '1', 'yes'),
(295, 'ec_option_enable_gateway_log', '1', 'yes'),
(296, 'ec_option_enable_metric_unit_display', '0', 'yes'),
(297, 'ec_option_use_live_search', '1', 'yes'),
(298, 'ec_option_search_title', '1', 'yes'),
(299, 'ec_option_search_model_number', '1', 'yes'),
(300, 'ec_option_search_manufacturer', '1', 'yes'),
(301, 'ec_option_search_description', '1', 'yes'),
(302, 'ec_option_search_short_description', '1', 'yes'),
(303, 'ec_option_search_menu', '1', 'yes'),
(304, 'ec_option_show_image_on_receipt', '1', 'yes'),
(305, 'ec_option_search_by_or', '1', 'yes'),
(306, 'ec_option_custom_third_party', 'Gateway Name', 'yes'),
(307, 'ec_option_show_card_holder_name', '1', 'yes'),
(308, 'ec_option_show_stock_quantity', '1', 'yes'),
(309, 'ec_option_send_low_stock_emails', '0', 'yes'),
(310, 'ec_option_send_out_of_stock_emails', '0', 'yes'),
(311, 'ec_option_low_stock_trigger_total', '5', 'yes'),
(312, 'ec_option_show_delivery_days_live_shipping', '1', 'yes'),
(313, 'ec_option_model_number_extension', '-', 'yes'),
(314, 'ec_option_collect_vat_registration_number', '0', 'yes'),
(315, 'ec_option_validate_vat_registration_number', '0', 'yes'),
(316, 'ec_option_vatlayer_api_key', '', 'yes'),
(317, 'ec_option_vat_custom_rate', '0', 'yes'),
(318, 'ec_option_fedex_use_net_charge', '1', 'yes'),
(319, 'ec_option_static_ship_items_seperately', '0', 'yes'),
(320, 'ec_option_google_adwords_conversion_id', '', 'yes'),
(321, 'ec_option_google_adwords_language', 'en', 'yes'),
(322, 'ec_option_google_adwords_format', '3', 'yes'),
(323, 'ec_option_google_adwords_color', 'ffffff', 'yes'),
(324, 'ec_option_google_adwords_label', '', 'yes'),
(325, 'ec_option_google_adwords_currency', 'USD', 'yes'),
(326, 'ec_option_google_adwords_remarketing_only', 'false', 'yes'),
(327, 'ec_option_default_country', '0', 'yes'),
(328, 'ec_option_show_subscriber_feature', '1', 'yes'),
(329, 'ec_option_use_inquiry_form', '1', 'yes'),
(330, 'ec_option_order_use_smtp', '0', 'yes'),
(331, 'ec_option_order_from_smtp_host', '', 'yes'),
(332, 'ec_option_order_from_smtp_encryption_type', 'ssl', 'yes'),
(333, 'ec_option_order_from_smtp_port', '465', 'yes'),
(334, 'ec_option_order_from_smtp_username', '', 'yes'),
(335, 'ec_option_order_from_smtp_password', '', 'yes'),
(336, 'ec_option_password_use_smtp', '0', 'yes'),
(337, 'ec_option_password_from_smtp_host', '', 'yes'),
(338, 'ec_option_password_from_smtp_encryption_type', 'ssl', 'yes'),
(339, 'ec_option_password_from_smtp_port', '465', 'yes'),
(340, 'ec_option_password_from_smtp_username', '', 'yes'),
(341, 'ec_option_password_from_smtp_password', '', 'yes'),
(342, 'ec_option_minimum_order_total', '0.00', 'yes'),
(343, 'wpeasycart_abandoned_cart_automation', '0', 'yes'),
(344, 'ec_option_tiered_price_format', '1', 'yes'),
(345, 'ec_option_demo_data_installed', '0', 'yes'),
(346, 'ec_subscriptions_use_first_order_details', '0', 'yes'),
(347, 'ec_option_packing_slip_show_pricing', '1', 'yes'),
(348, 'ec_option_fb_pixel', '', 'yes'),
(349, 'ec_option_use_direct_deposit', '1', 'yes'),
(350, 'ec_option_direct_deposit_message', 'You have selected a manual payment method.', 'yes'),
(351, 'ec_option_use_affirm', '0', 'yes'),
(352, 'ec_option_affirm_public_key', '', 'yes'),
(353, 'ec_option_affirm_private_key', '', 'yes'),
(354, 'ec_option_affirm_financial_product', '', 'yes'),
(355, 'ec_option_affirm_sandbox_account', '0', 'yes'),
(356, 'ec_option_use_visa', '1', 'yes'),
(357, 'ec_option_use_delta', '0', 'yes'),
(358, 'ec_option_use_uke', '0', 'yes'),
(359, 'ec_option_use_discover', '1', 'yes'),
(360, 'ec_option_use_mastercard', '1', 'yes'),
(361, 'ec_option_use_mcdebit', '0', 'yes'),
(362, 'ec_option_use_amex', '1', 'yes'),
(363, 'ec_option_use_jcb', '0', 'yes'),
(364, 'ec_option_use_diners', '0', 'yes'),
(365, 'ec_option_use_laser', '0', 'yes'),
(366, 'ec_option_use_maestro', '0', 'yes'),
(367, 'ec_option_payment_process_method', '0', 'yes'),
(368, 'ec_option_payment_third_party', '0', 'yes'),
(369, 'ec_option_2checkout_thirdparty_sid', '', 'yes'),
(370, 'ec_option_2checkout_thirdparty_currency_code', 'USD', 'yes'),
(371, 'ec_option_2checkout_thirdparty_lang', 'en', 'yes'),
(372, 'ec_option_2checkout_thirdparty_purchase_step', 'payment-method', 'yes'),
(373, 'ec_option_2checkout_thirdparty_demo_mode', '0', 'yes'),
(374, 'ec_option_2checkout_thirdparty_sandbox_mode', '0', 'yes'),
(375, 'ec_option_2checkout_thirdparty_secret_word', '', 'yes'),
(376, 'ec_option_authorize_login_id', '', 'yes'),
(377, 'ec_option_authorize_trans_key', '', 'yes'),
(378, 'ec_option_authorize_test_mode', '0', 'yes'),
(379, 'ec_option_authorize_developer_account', '0', 'yes'),
(380, 'ec_option_authorize_use_legacy_url', '0', 'yes'),
(381, 'ec_option_authorize_currency_code', 'USD', 'yes'),
(382, 'ec_option_beanstream_merchant_id', '', 'yes'),
(383, 'ec_option_beanstream_api_passcode', '', 'yes'),
(384, 'ec_option_braintree_merchant_id', '', 'yes'),
(385, 'ec_option_braintree_merchant_account_id', '', 'yes'),
(386, 'ec_option_braintree_public_key', '', 'yes'),
(387, 'ec_option_braintree_private_key', '', 'yes'),
(388, 'ec_option_braintree_currency', 'USD', 'yes'),
(389, 'ec_option_braintree_environment', 'sandbox', 'yes'),
(390, 'ec_option_cardinal_processor_id', '', 'yes'),
(391, 'ec_option_cardinal_merchant_id', '', 'yes'),
(392, 'ec_option_cardinal_password', '', 'yes'),
(393, 'ec_option_cardinal_currency', '840,', 'yes'),
(394, 'ec_option_cardinal_test_mode', '0', 'yes'),
(395, 'ec_option_paypoint_merchant_id', '', 'yes'),
(396, 'ec_option_paypoint_vpn_password', '0', 'yes'),
(397, 'ec_option_paypoint_test_mode', '0', 'yes'),
(398, 'ec_option_chronopay_currency', '', 'yes'),
(399, 'ec_option_chronopay_product_id', '', 'yes'),
(400, 'ec_option_chronopay_shared_secret', '', 'yes'),
(401, 'ec_option_dwolla_thirdparty_key', '', 'yes'),
(402, 'ec_option_dwolla_thirdparty_secret', '', 'yes'),
(403, 'ec_option_dwolla_thirdparty_test_mode', '0', 'yes'),
(404, 'ec_option_dwolla_thirdparty_account_id', '812-xxx-xxxx', 'yes'),
(405, 'ec_option_versapay_id', '', 'yes'),
(406, 'ec_option_versapay_password', '', 'yes'),
(407, 'ec_option_versapay_language', 'en', 'yes'),
(408, 'ec_option_eway_use_rapid_pay', '0', 'yes'),
(409, 'ec_option_eway_customer_id', '', 'yes'),
(410, 'ec_option_eway_api_key', '', 'yes'),
(411, 'ec_option_eway_api_password', '', 'yes'),
(412, 'ec_option_eway_client_key', '', 'yes'),
(413, 'ec_option_eway_test_mode', '0', 'yes'),
(414, 'ec_option_eway_test_mode_success', '1', 'yes'),
(415, 'ec_option_firstdata_login_id', '', 'yes'),
(416, 'ec_option_firstdata_pem_file', '', 'yes'),
(417, 'ec_option_firstdata_host', 'secure.linkpt.net', 'yes'),
(418, 'ec_option_firstdata_port', '1129', 'yes'),
(419, 'ec_option_firstdata_test_mode', '0', 'yes'),
(420, 'ec_option_firstdata_use_ssl_cert', '0', 'yes'),
(421, 'ec_option_firstdatae4_exact_id', '', 'yes'),
(422, 'ec_option_firstdatae4_password', '', 'yes'),
(423, 'ec_option_firstdatae4_key_id', '', 'yes'),
(424, 'ec_option_firstdatae4_key', '', 'yes'),
(425, 'ec_option_firstdatae4_language', 'EN', 'yes'),
(426, 'ec_option_firstdatae4_currency', 'USD', 'yes'),
(427, 'ec_option_firstdatae4_test_mode', '0', 'yes'),
(428, 'ec_option_goemerchant_gateway_id', '', 'yes'),
(429, 'ec_option_goemerchant_processor_id', '', 'yes'),
(430, 'ec_option_goemerchant_trans_center_id', '', 'yes'),
(431, 'ec_option_intuit_oauth_version', '1', 'yes'),
(432, 'ec_option_intuit_app_token', '', 'yes'),
(433, 'ec_option_intuit_consumer_key', '', 'yes'),
(434, 'ec_option_intuit_consumer_secret', '', 'yes'),
(435, 'ec_option_intuit_currency', 'USD', 'yes'),
(436, 'ec_option_intuit_test_mode', '0', 'yes'),
(437, 'ec_option_intuit_realm_id', '', 'yes'),
(438, 'ec_option_intuit_access_token', '', 'yes'),
(439, 'ec_option_intuit_access_token_secret', '', 'yes'),
(440, 'ec_option_intuit_last_authorized', '0', 'yes'),
(441, 'ec_option_intuit_refresh_token', '', 'yes'),
(442, 'ec_option_nmi_3ds', '0', 'yes'),
(443, 'ec_option_nmi_api_key', '', 'yes'),
(444, 'ec_option_nmi_username', '', 'yes'),
(445, 'ec_option_nmi_password', '', 'yes'),
(446, 'ec_option_nmi_currency', 'USD', 'yes'),
(447, 'ec_option_nmi_processor_id', '', 'yes'),
(448, 'ec_option_nmi_ship_from_zip', '', 'yes'),
(449, 'ec_option_nmi_commodity_code', '', 'yes'),
(450, 'ec_option_nets_merchant_id', '', 'yes'),
(451, 'ec_option_nets_token', '', 'yes'),
(452, 'ec_option_nets_currency', 'USD', 'yes'),
(453, 'ec_option_nets_test_mode', '0', 'yes'),
(454, 'ec_option_migs_signature', '', 'yes'),
(455, 'ec_option_migs_access_code', '', 'yes'),
(456, 'ec_option_migs_merchant_id', '', 'yes'),
(457, 'ec_option_moneris_ca_store_id', '', 'yes'),
(458, 'ec_option_moneris_ca_api_token', '', 'yes'),
(459, 'ec_option_moneris_ca_test_mode', '0', 'yes'),
(460, 'ec_option_moneris_us_store_id', '', 'yes'),
(461, 'ec_option_moneris_us_api_token', '', 'yes'),
(462, 'ec_option_moneris_us_test_mode', '0', 'yes'),
(463, 'ec_option_payfast_merchant_id', '', 'yes'),
(464, 'ec_option_payfast_merchant_key', '', 'yes'),
(465, 'ec_option_payfast_passphrase', '', 'yes'),
(466, 'ec_option_payfast_sandbox', '0', 'yes'),
(467, 'ec_option_payfort_access_code', '', 'yes'),
(468, 'ec_option_payfort_merchant_id', '', 'yes'),
(469, 'ec_option_payfort_request_phrase', '', 'yes'),
(470, 'ec_option_payfort_response_phrase', '', 'yes'),
(471, 'ec_option_payfort_sha_type', 'sha256', 'yes'),
(472, 'ec_option_payfort_language', 'en', 'yes'),
(473, 'ec_option_payfort_currency_code', 'USD', 'yes'),
(474, 'ec_option_payfort_use_sadad', '0', 'yes'),
(475, 'ec_option_payfort_use_naps', '0', 'yes'),
(476, 'ec_option_payfort_sadad_olp', '', 'yes'),
(477, 'ec_option_payfort_use_currency_service', '0', 'yes'),
(478, 'ec_option_payfort_test_mode', '1', 'yes'),
(479, 'ec_option_payline_username', '', 'yes'),
(480, 'ec_option_payline_password', '', 'yes'),
(481, 'ec_option_payline_currency', 'USD', 'yes'),
(482, 'ec_option_payment_express_username', '', 'yes'),
(483, 'ec_option_payment_express_password', '', 'yes'),
(484, 'ec_option_payment_express_currency', 'NZD', 'yes'),
(485, 'ec_option_payment_express_developer_account', '0', 'yes'),
(486, 'ec_option_payment_express_thirdparty_username', '', 'yes'),
(487, 'ec_option_payment_express_thirdparty_key', '', 'yes'),
(488, 'ec_option_payment_express_thirdparty_currency', 'NZD', 'yes'),
(489, 'ec_option_paypal_use_sandbox', '0', 'yes'),
(490, 'ec_option_paypal_email', '', 'yes'),
(491, 'ec_option_paypal_enable_pay_now', '0', 'yes'),
(492, 'ec_option_paypal_enable_credit', '0', 'yes'),
(493, 'ec_option_paypal_sandbox_merchant_id', '', 'yes'),
(494, 'ec_option_paypal_sandbox_app_id', '', 'yes'),
(495, 'ec_option_paypal_sandbox_secret', '', 'yes'),
(496, 'ec_option_paypal_sandbox_access_token', '', 'yes'),
(497, 'ec_option_paypal_sandbox_access_token_expires', '0', 'yes'),
(498, 'ec_option_paypal_production_merchant_id', '', 'yes'),
(499, 'ec_option_paypal_production_app_id', '', 'yes'),
(500, 'ec_option_paypal_production_secret', '', 'yes'),
(501, 'ec_option_paypal_production_access_token', '', 'yes'),
(502, 'ec_option_paypal_production_access_token_expires', '0', 'yes'),
(503, 'ec_option_paypal_marketing_solution_cid_sandbox', '', 'yes'),
(504, 'ec_option_paypal_marketing_solution_cid_production', '', 'yes'),
(505, 'ec_option_paypal_currency_code', 'USD', 'yes'),
(506, 'ec_option_paypal_use_selected_currency', '0', 'yes'),
(507, 'ec_option_paypal_charset', 'UTF-8', 'yes'),
(508, 'ec_option_paypal_lc', 'US', 'yes'),
(509, 'ec_option_paypal_weight_unit', 'kgs', 'yes'),
(510, 'ec_option_paypal_collect_shipping', '0', 'yes'),
(511, 'ec_option_paypal_send_shipping_address', '0', 'yes'),
(512, 'ec_option_paypal_advanced_test_mode', '0', 'yes'),
(513, 'ec_option_paypal_advanced_vendor', '', 'yes'),
(514, 'ec_option_paypal_advanced_partner', '', 'yes'),
(515, 'ec_option_paypal_advanced_user', '', 'yes'),
(516, 'ec_option_paypal_advanced_password', '', 'yes'),
(517, 'ec_option_paypal_pro_test_mode', '0', 'yes'),
(518, 'ec_option_paypal_pro_vendor', '', 'yes'),
(519, 'ec_option_paypal_pro_partner', '', 'yes'),
(520, 'ec_option_paypal_pro_user', '', 'yes'),
(521, 'ec_option_paypal_pro_password', '', 'yes'),
(522, 'ec_option_paypal_pro_currency', 'USD', 'yes'),
(523, 'ec_option_paypal_payments_pro_test_mode', '0', 'yes'),
(524, 'ec_option_paypal_payments_pro_user', '', 'yes'),
(525, 'ec_option_paypal_payments_pro_password', '', 'yes'),
(526, 'ec_option_paypal_payments_pro_signature', '', 'yes'),
(527, 'ec_option_paypal_payments_pro_currency', 'USD', 'yes'),
(528, 'ec_option_skrill_merchant_id', '', 'yes'),
(529, 'ec_option_skrill_company_name', '', 'yes'),
(530, 'ec_option_skrill_email', '', 'yes'),
(531, 'ec_option_skrill_language', 'EN', 'yes'),
(532, 'ec_option_realex_thirdparty_type', 'legacy', 'yes'),
(533, 'ec_option_realex_thirdparty_merchant_id', '', 'yes'),
(534, 'ec_option_realex_thirdparty_secret', '', 'yes'),
(535, 'ec_option_realex_thirdparty_account', 'redirect', 'yes'),
(536, 'ec_option_realex_thirdparty_currency', 'GBP', 'yes'),
(537, 'ec_option_realex_merchant_id', '', 'yes'),
(538, 'ec_option_realex_secret', '', 'yes'),
(539, 'ec_option_realex_currency', 'GBP', 'yes'),
(540, 'ec_option_realex_3dsecure', '0', 'yes'),
(541, 'ec_option_realex_test_mode', '0', 'yes'),
(542, 'ec_option_redsys_merchant_code', '', 'yes'),
(543, 'ec_option_redsys_terminal', '', 'yes'),
(544, 'ec_option_redsys_currency', '978', 'yes'),
(545, 'ec_option_redsys_key', '', 'yes'),
(546, 'ec_option_redsys_test_mode', '0', 'yes'),
(547, 'ec_option_sagepay_vendor', '', 'yes'),
(548, 'ec_option_sagepay_currency', 'USD', 'yes'),
(549, 'ec_option_sagepay_simulator', '0', 'yes'),
(550, 'ec_option_sagepay_testmode', '0', 'yes'),
(551, 'ec_option_sagepayus_mid', '999999999997', 'yes'),
(552, 'ec_option_sagepayus_mkey', 'A1B2C3D4E5F6', 'yes'),
(553, 'ec_option_sagepayus_application_id', 'SAGETEST1', 'yes'),
(554, 'ec_option_sagepay_paynow_za_service_key', '', 'yes'),
(555, 'ec_option_securenet_id', '', 'yes'),
(556, 'ec_option_securenet_secure_key', '', 'yes'),
(557, 'ec_option_securenet_use_sandbox', '0', 'yes'),
(558, 'ec_option_securepay_merchant_id', '', 'yes'),
(559, 'ec_option_securepay_password', '', 'yes'),
(560, 'ec_option_securepay_currency', 'AUD', 'yes'),
(561, 'ec_option_securepay_test_mode', '0', 'yes'),
(562, 'ec_option_skrill_currency_code', 'USD', 'yes'),
(563, 'ec_option_square_access_token', '', 'yes'),
(564, 'ec_option_square_token_expires', '', 'yes'),
(565, 'ec_option_square_application_id', '', 'yes'),
(566, 'ec_option_square_location_id', '0', 'yes'),
(567, 'ec_option_square_currency', 'USD', 'yes'),
(568, 'ec_option_psigate_store_id', 'teststore', 'yes'),
(569, 'ec_option_psigate_passphrase', 'psigate1234', 'yes'),
(570, 'ec_option_psigate_test_mode', '1', 'yes'),
(571, 'ec_option_use_proxy', '0', 'yes'),
(572, 'ec_option_proxy_address', '0', 'yes'),
(573, 'ec_option_stripe_api_key', '', 'yes'),
(574, 'ec_option_stripe_public_api_key', '', 'yes'),
(575, 'ec_option_stripe_currency', 'USD', 'yes'),
(576, 'ec_option_stripe_order_create_customer', '0', 'yes'),
(577, 'ec_option_stripe_connect_use_sandbox', '0', 'yes'),
(578, 'ec_option_stripe_connect_sandbox_access_token', '', 'yes'),
(579, 'ec_option_stripe_connect_sandbox_refresh_token', '', 'yes'),
(580, 'ec_option_stripe_connect_sandbox_publishable_key', '', 'yes'),
(581, 'ec_option_stripe_connect_sandbox_user_id', '', 'yes'),
(582, 'ec_option_stripe_connect_production_access_token', '', 'yes'),
(583, 'ec_option_stripe_connect_production_refresh_token', '', 'yes'),
(584, 'ec_option_stripe_connect_production_publishable_key', '', 'yes'),
(585, 'ec_option_stripe_connect_production_user_id', '', 'yes'),
(586, 'ec_option_virtualmerchant_ssl_merchant_id', '', 'yes'),
(587, 'ec_option_virtualmerchant_ssl_user_id', '', 'yes'),
(588, 'ec_option_virtualmerchant_ssl_pin', '', 'yes'),
(589, 'ec_option_virtualmerchant_currency', 'USD', 'yes'),
(590, 'ec_option_virtualmerchant_demo_account', '0', 'yes'),
(591, 'ec_option_language', 'en-us', 'yes');
INSERT INTO `ops_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(592, 'ec_option_language_data', '{"en-us":{"label":"US English","options":{"sort_bar":{"label":"Product Page Sort Menu","options":{"sort_by_price_low":{"title":"Product Page Sort Box Item 1","value":"Price Low-High"},"sort_by_price_high":{"title":"Product Page Sort Box Item 2","value":"Price High-Low"},"sort_by_title_a":{"title":"Product Page Sort Box Item 3","value":"Title A-Z"},"sort_by_title_z":{"title":"Product Page Sort Box Item 4","value":"Title Z-A"},"sort_by_newest":{"title":"Product Page Sort Box Item 5","value":"Newest"},"sort_by_rating":{"title":"Product Page Sort Box Item 6","value":"Best Rating"},"sort_by_most_viewed":{"title":"Product Page Sort Box Item 7","value":"Most Viewed"},"sort_default":{"title":"Product Page Sort Box Default","value":"Default Sorting"}}},"product_page":{"label":"Product Page","options":{"product_quick_view":{"title":"Product Quick View Label","value":"Quickview"},"product_design_now":{"title":"Product Design Now Label","value":"DESIGN NOW"},"product_no_results":{"title":"Product Page No Results","value":"No Results Found"},"product_items_per_page":{"title":"Product Page Items per Page","value":"Items Per Page:"},"product_paging_page":{"title":"Product Page Page in Page a of b","value":"Page"},"product_paging_of":{"title":"Product Page of in Page a of b","value":"of"},"product_showing":{"title":"Product Page Num Results Showing","value":"Showing"},"product_all":{"title":"Product Page Num Results all","value":"all"},"product_results":{"title":"Product Page Num Results","value":"Results"},"product_view_cart":{"title":"Product Page View Cart","value":"View Cart"},"product_product_added_note":{"title":"Product Page Added to Cart Note","value":"Product successfully added to your cart."},"product_product_view_per_page":{"title":"Product Page View in Per Page","value":"View"},"product_page_restricted_line_1":{"title":"Restricted Product Line 1","value":"OPEN ACCESS IS NOT ALLOWED FOR THE STORE"},"product_page_restricted_line_2":{"title":"Restricted Product Line 2 Prior to Link","value":"Please log in or create an account by"},"product_page_restricted_link_text":{"title":"Restricted Product Link Text","value":"clicking here"},"product_page_restricted_line_3":{"title":"Restricted Product Line 2 Post Link","value":". Your account may need to be verified before you will be able to access the store."},"product_page_start_trial_1":{"title":"Start Your Trial Text Part 1","value":"Start Your"},"product_page_start_trial_2":{"title":"Start Your Trial Text Part 2","value":"Day Free Trial Today!"},"product_inc_vat_text":{"title":"Including VAT Text","value":"inc VAT"},"product_excluding_vat_text":{"title":"Excluding VAT Text","value":"excluding VAT"}}},"quick_view":{"label":"Quick View Panel","options":{"quick_view_gift_card_message":{"title":"Gift Card Message Title","value":"SPECIAL MESSAGE"},"quick_view_gift_card_to_name":{"title":"Gift Card To Name Title","value":"TO"},"quick_view_gift_card_from_name":{"title":"Gift Card From Name Title","value":"FROM"},"quick_view_quantity":{"title":"Quantity Title","value":"QUANTITY"},"quick_view_remaining":{"title":"label in x items remaining","value":"remaining"},"quick_view_add_to_cart":{"title":"Add to Cart Button","value":"ADD TO CART"},"quick_view_select_options":{"title":"Product details missing options error text","value":"Please select all options for this product."},"quick_view_view_full_details":{"title":"Link Text View Full Product Details","value":"View Full Product Details"}}},"product_details":{"label":"Product Details Page","options":{"product_details_donation_label":{"title":"Your Donation Label","value":"Your Donation: "},"product_details_minimum_donation":{"title":"Your Donation Minimum Label","value":"minimum donation:"},"product_details_gift_card_message":{"title":"Store Gift Card Message","value":"Include a Message"},"product_details_gift_card_to_name":{"title":"Gift Card To Name Title","value":"TO"},"product_details_gift_card_from_name":{"title":"Gift Card From Name Title","value":"FROM"},"product_details_quantity":{"title":"Quantity Label","value":"Quantity"},"product_details_remaining":{"title":"x remaining","value":"remaining"},"product_details_x_of_y":{"title":"page text divider","value":"of"},"product_details_model_number":{"title":"model number text","value":"model number"},"product_details_reduced_price":{"title":"reduced price text","value":"REDUCED PRICE YOU SAVE"},"product_details_description":{"title":"Description Title","value":"Description"},"product_details_customer_reviews":{"title":"Customer Reviews Title","value":"Customer Reviews"},"product_details_specifications":{"title":"Specifications Title","value":"Specifications"},"product_details_customer_rating":{"title":"Customer Rating","value":"Overall Customer Rating"},"product_details_review_based_on":{"title":"Review Based ON","value":"based on "},"product_details_review":{"title":"Review","value":"review"},"product_details_review_no_reviews":{"title":"Product Details, Customer Reviews, No Reviews","value":"There are no reviews yet, submit yours in the box provided."},"product_details_add_to_cart":{"title":"Add to Cart button text","value":"ADD TO CART"},"product_details_added_to_cart":{"title":"Checkout Now button text","value":"CHECKOUT NOW"},"product_details_select_options":{"title":"Select Options Button","value":"Select Options"},"product_details_out_of_stock":{"title":"Out of Stock Text","value":"OUT OF STOCK"},"product_details_sign_up_now":{"title":"Sign Up Now Text","value":"SIGN UP NOW"},"product_details_inquiry_title":{"title":"Inquiry Title","value":"Submit Product Inquiry"},"product_details_inquiry_error":{"title":"Inquiry Error Text","value":"Please fill out all required fields"},"product_details_inquiry_name":{"title":"Inquiry Name","value":"*Your Name:"},"product_details_inquiry_email":{"title":"Inquiry Email","value":"*Your Email:"},"product_details_inquiry_message":{"title":"Inquire Button Text","value":"*Your Message:"},"product_details_inquire":{"title":"Inquire Button Text","value":"SUBMIT INQUIRY"},"product_details_inquiry_send_copy":{"title":"Inquiry - Send Copy","value":"Please send me a copy"},"product_details_inquiry_thank_you":{"title":"Inquiry Thank You Note","value":"Thank you for submitting a product inquiry and we will get back to you as soon as possible."},"product_details_minimum_quantity_text1":{"title":"Min Purchase Amount Text Part 1","value":"Minimum purchase amount of"},"product_details_minimum_quantity_text2":{"title":"Min Purchase Amount Text Part 2","value":"is required"},"product_details_maximum_quantity_text1":{"title":"Max Purchase Amount Text Part 1","value":"Maximum purchase amount of"},"product_details_maximum_quantity_text2":{"title":"Max Purchase Amount Text Part 2","value":"is allowed"},"product_details_handling_fee_notice1":{"title":"One Time Handling Fee Part 1","value":"*A one-time handling fee of"},"product_details_handling_fee_notice2":{"title":"One Time Handling Fee Part 2","value":"will be added to your shipping total at checkout."},"product_details_handling_fee_each_notice1":{"title":"Handling Fee Each Part 1","value":"*A handling fee of"},"product_details_handling_fee_each_notice2":{"title":"Handling Fee Each Part 2","value":"per item will be added to your shipping total at checkout."},"product_details_signup_fee_notice1":{"title":"One Time Sign-up Fee Part 1","value":"*A one-time sign-up fee of"},"product_details_signup_fee_notice2":{"title":"One Time Sign-up Fee Part 2","value":"will be added to your first payment total."},"product_details_home_link":{"title":"Home Breadcrumbs Link","value":"Home"},"product_details_store_link":{"title":"Store Breadcrumbs Link","value":"Shop"},"product_details_reviews_text":{"title":"Store Reviews Count","value":"Reviews"},"product_details_gift_card_recipient_name":{"title":"Store Gift Card To Name","value":"Recipient''s Name"},"product_details_gift_card_recipient_email":{"title":"Store Gift Card To Email","value":"Recipient''s Email"},"product_details_gift_card_sender_name":{"title":"Store Gift Card Sender''s Name","value":"Sender''s Name"},"product_details_donation_amount":{"title":"Store Donation Amount","value":"Donation Amount"},"product_details_loading_options":{"title":"Store Loading Options","value":"Loading Available Options"},"product_details_missing_option":{"title":"Store Missing Option","value":"Missing Option:"},"product_details_maximum_quantity":{"title":"Store Maximum Quantity Exceeded","value":"Maximum quantity exceeded"},"product_details_one_time_addition1":{"title":"Store One Time Addition 1","value":"A one-time price of"},"product_details_one_time_addition2":{"title":"Store One Time Addition 2","value":"will be added to your order."},"product_details_left_in_stock":{"title":"Store Left in Stock","value":"Left in Stock"},"product_details_categories":{"title":"Store Categories","value":"Categories:"},"product_details_manufacturer":{"title":"Store Manufacturer","value":"Manufacturer:"},"product_details_related_products":{"title":"Store Related Products","value":"Related Products"},"product_details_tier_buy":{"title":"Tiered Pricing buy x at y","value":"Buy"},"product_details_tier_buy_at":{"title":"Tiered Pricing buy x at y","value":"at"},"product_details_tier_or_more":{"title":"Tiered Pricing buy x or more","value":"or more"},"product_details_tier_each":{"title":"Tiered Pricing buy x at y each","value":"each"},"product_details_gift_card_error":{"title":"Gift Card Option Error Text","value":"Missing Gift Card Options"},"product_details_donation_error":{"title":"Donation Amount Error Text","value":"Invalid donation amount entered. Please enter a minimum value of"},"product_details_your_price":{"title":"Your Price:","value":"Your Price:"},"product_details_backorder_until":{"title":"Backorder UNTIL date, until","value":"until"},"product_details_backordered":{"title":"BACKORDERED until date","value":"Backordered"},"product_details_some_backordered":{"title":"Some BACKORDERED","value":"Some items are backordered, only"},"product_details_some_backordered_remaining":{"title":"Some BACKORDERED remaining","value":"remaining."},"product_details_backordered_message":{"title":"Backordered message in cart","value":"Some items in your cart are currently out of stock and on backorder."},"product_details_vat_included":{"title":"Including VAT","value":"Including"},"product_details_vat_excluded":{"title":"Excluding VAT","value":"Excluding"},"product_details_backorder_button":{"title":"Backorder Button","value":"BACKORDER NOW"},"product_details_reviews_for_text":{"title":"Reviews for","value":"Reviews for"},"product_details_as_low_as":{"title":"As low as","value":"As low as"}}},"customer_review":{"label":"Customer Review Panel","options":{"customer_review_title":{"title":"Product Details Rating Box Title","value":"Write a Review"},"customer_review_choose_rating":{"title":"Product Details Rating Box Choose Rating","value":"choose a rating"},"customer_review_enter_title":{"title":"Product Details Rating Box Choose Title","value":"enter a review title"},"customer_review_enter_description":{"title":"Product Details Rating Box Choose Description","value":"enter a review description"},"customer_review_select_option":{"title":"Product Details Default Select Option Text","value":"select an option"},"customer_review_close_button":{"title":"Product Details Rating Box Close Button","value":"Close"},"customer_review_submit_button":{"title":"Product Details Rating Box Submit Button","value":"Submit Review"},"product_details_write_a_review":{"title":"Product Details Write a Review Button","value":"Write a Review"},"product_details_add_a_review_for":{"title":"Store Add a Review","value":"Add a review for"},"product_details_your_review_title":{"title":"Store Review title","value":"Your Review Title"},"product_details_your_review_rating":{"title":"Store Review Rating","value":"Your Rating"},"product_details_your_review_message":{"title":"Store Review Message","value":"Your Review"},"product_details_your_review_submit":{"title":"Store Review Submit","value":"SUBMIT"},"product_details_review_submitted_button":{"title":"Product Details Review Submitted Button","value":"REVIEW SUBMITTED"},"product_details_submitting_review":{"title":"Store Submitting Review","value":"Submitting Your Review, Please Wait"},"product_details_review_submitted":{"title":"Store Review Submitted","value":"Your Review Has Been Submitted Successfully"},"product_details_review_log_in_first":{"title":"Log in to Review","value":"Please sign in or create an account to submit a review for this product."},"product_details_review_anonymous_reviewer":{"title":"Anonymous Reviewer Name","value":"Anonymous"},"review_error":{"title":"Store Review Error Missing Data","value":"You must include a title, rating, and message in your review."}}},"cart":{"label":"Cart","options":{"cart_title":{"title":"Checkout Cart Title","value":"SHOPPING CART"},"cart_checkout_details_title":{"title":"Checkout Details Title","value":"CHECKOUT DETAILS"},"cart_submit_payment_title":{"title":"Submit Payment Title","value":"SUBMIT PAYMENT"},"your_cart_title":{"title":"Your Cart Title","value":"YOUR CART"},"cart_empty_cart":{"title":"Checkout, Empty Cart Message","value":"There are no items in your cart."},"cart_checkout":{"title":"Checkout Page, Checkout Button","value":"Checkout"},"cart_continue_shopping":{"title":"Checkout Page, Continue Shopping Link","value":"Continue Shopping"},"cart_header_column1":{"title":"Cart Header, Column 1","value":"Product"},"cart_header_column2":{"title":"Cart Header, Column 2","value":"Details"},"cart_header_column3":{"title":"Cart Header, Column 3","value":"Price"},"cart_header_column4":{"title":"Cart Header, Column 4","value":"Quantity"},"cart_header_column5":{"title":"Cart Header, Column 5","value":"Total"},"cart_header_column6":{"title":"Cart Header, Column 6","value":"Actions"},"cart_item_vat_text":{"title":"Cart Item, Included Vat","value":"VAT @"},"cart_item_update_button":{"title":"Cart Item, Update Button","value":"UPDATE"},"cart_item_remove_button":{"title":"Cart Item, Remove Button","value":"REMOVE"},"cart_item_adjustment":{"title":"Cart Item, Adjustment per Item","value":"per item"},"cart_item_new_price_option":{"title":"Cart Item, Price Override Text","value":"Item Price of"},"cart_menu_icon_label":{"title":"Cart in Menu, Item","value":"Item"},"cart_menu_icon_label_plural":{"title":"Cart in Menu, Items","value":"Items"},"deconetwork_edit":{"title":"Deconetwork, Edit Design","value":"Edit Design"},"cart_order_adjustment":{"title":"Cart Item, Adjustment per Order","value":"per order"},"cart_return_to_store":{"title":"Empty Cart, Return to store","value":"RETURN TO STORE"},"cartitem_max_error":{"title":"Max Quantity Exceeded Error","value":"Maximum Quantity Exceeded"},"cartitem_min_error":{"title":"Min Quantity Required Error","value":"Minimum Quantity Required"},"cart_please_wait":{"title":"Cart Please Wait","value":"Please Wait"},"cart_item_adjustment_per_character":{"title":"Cart Per Character Adjustment","value":"Per Character"},"cart_minimum_purchase_amount1":{"title":"A Minimum Order Amount of","value":"A Minimum Order Amount of"},"cart_minimum_purchase_amount2":{"title":"Min Required Text 2","value":"is Required"}}},"cart_form_notices":{"label":"Cart - Form Error Notices","options":{"cart_notice_is_required":{"title":"Cart, Notice, Is Required Field","value":"is a required field"},"cart_notice_item_must_match":{"title":"Cart, Notice, Items Must Match","value":"must match"},"cart_notice_billing_first_name":{"title":"Cart, Notice, Billing, First Name","value":"Billing First Name"},"cart_notice_billing_last_name":{"title":"Cart, Notice, Billing, Last Name","value":"Billing Last Name"},"cart_notice_billing_address":{"title":"Cart, Notice, Billing, Address","value":"Billing Address"},"cart_notice_billing_city":{"title":"Cart, Notice, Billing, City","value":"Billing City"},"cart_notice_billing_state":{"title":"Cart, Notice, Billing, State","value":"Billing State"},"cart_notice_billing_zip_code":{"title":"Cart, Notice, Billing, Zip Code","value":"Billing Zip Code"},"cart_notice_billing_country":{"title":"Cart, Notice, Billing, Country","value":"Billing Country"},"cart_notice_billing_phone":{"title":"Cart, Notice, Billing, Phone","value":"Billing Phone"},"cart_notice_shipping_first_name":{"title":"Cart, Notice, Shipping, First Name","value":"Shipping First Name"},"cart_notice_shipping_last_name":{"title":"Cart, Notice, Shipping, Last Name","value":"Shipping Last Name"},"cart_notice_shipping_address":{"title":"Cart, Notice, Shipping, Address","value":"Shipping Address"},"cart_notice_shipping_city":{"title":"Cart, Notice, Shipping, City","value":"Shipping City"},"cart_notice_shipping_state":{"title":"Cart, Notice, Shipping, State","value":"Shipping State"},"cart_notice_shipping_zip_code":{"title":"Cart, Notice, Shipping, Zip Code","value":"Shipping Zip Code"},"cart_notice_shipping_country":{"title":"Cart, Notice, Shipping, Country","value":"Shipping Country"},"cart_notice_shipping_phone":{"title":"Cart, Notice, Shipping, Phone","value":"Shipping Phone"},"cart_notice_contact_first_name":{"title":"Cart, Notice, Contact First Name","value":"Contact First Name"},"cart_notice_contact_last_name":{"title":"Cart, Notice, Contact Last Name","value":"Contact Last Name"},"cart_notice_email":{"title":"Cart, Notice, Email","value":"Email"},"cart_notice_retype_email":{"title":"Cart, Notice, Retype Email","value":"Retype Email"},"cart_notice_emails_match":{"title":"Cart, Notice, Emails Match","value":"Emails"},"cart_notice_password":{"title":"Cart, Notice, Password","value":"Password"},"cart_notice_retype_password":{"title":"Cart, Notice, Retype Password","value":"Retype Password"},"cart_notice_passwords_match":{"title":"Cart, Notice, Passwords Match","value":"Passwords"},"cart_notice_length_error":{"title":"Cart, Notice, Password Format","value":"Please enter a password of at least 6 characters"},"cart_notice_payment_card_type":{"title":"Cart, Notice, Card Type","value":"Card Type"},"cart_notice_payment_card_holder_name":{"title":"Cart, Notice, Card Holder Required","value":"Card Holder Name"},"cart_notice_payment_card_number":{"title":"Cart, Notice, Card Number Required","value":"Card Number"},"cart_notice_payment_card_number_error":{"title":"Cart, Notice, Card Number Required","value":"The credit card number entered is invalid"},"cart_notice_payment_card_exp_month":{"title":"Cart, Notice, EXP Month Required","value":"Expiration Month"},"cart_notice_payment_card_exp_year":{"title":"Cart, Notice, EXP Year Required","value":"Expiration Year"},"cart_notice_payment_card_code":{"title":"Cart, Notice, Security Code Required","value":"Security Code"},"cart_notice_please_enter_your":{"title":"Cart, Notice, Please Enter Your","value":"Please enter your"},"cart_notice_please_select_your":{"title":"Cart, Notice, Please Select Your","value":"Please select your"},"cart_notice_please_enter_valid":{"title":"Cart, Notice, Please Enter Valid","value":"Please enter a valid"},"cart_notice_emails_do_not_match":{"title":"Cart, Notice, Email Do Not Match","value":"Your email addresses do not match"},"cart_notice_passwords_do_not_match":{"title":"Cart, Notice, Passwords Do Not Match","value":"Your passwords do not match"},"cart_notice_checkout_details_errors":{"title":"Cart, Notice, Checkout Details Errors","value":"Please correct the errors in your checkout details"},"cart_notice_payment_accept_terms":{"title":"Cart, Notice, Please Accept Terms","value":"Please agree to the terms and conditions"},"cart_notice_payment_correct_errors":{"title":"Cart, Notice, Payment Correct Errors","value":"Please correct the errors to your checkout information above"}}},"cart_process":{"label":"Cart - Progress Bar","options":{"cart_process_cart_link":{"title":"Cart, Progress, Cart","value":"Cart"},"cart_process_shipping_link":{"title":"Cart, Progress, Address","value":"Address"},"cart_process_review_link":{"title":"Cart, Progress, Cart","value":"Review\\/Pay"},"cart_process_complete_link":{"title":"Cart, Progress, Complete","value":"Complete"}}},"cart_coupons":{"label":"Cart - Enter Coupons and Gift Cards","options":{"cart_coupon_title":{"title":"Cart, Coupon Title","value":"Coupon"},"cart_gift_card_title":{"title":"Cart, Gift Card Title","value":"Gift Card"},"cart_coupon_sub_title":{"title":"Cart, Coupons Sub-Title","value":"Redeem coupon codes or gift cards by entering them below."},"cart_redeem_gift_card":{"title":"Cart, Redeem Gift Card Button","value":"Redeem Gift Card"},"cart_apply_coupon":{"title":"Cart, Apply Coupon Button","value":"Apply Coupon"},"cart_invalid_coupon":{"title":"Cart, Invalid Coupon","value":"Not a valid coupon code"},"cart_not_applicable_coupon":{"title":"Cart, Not Applicable Coupon","value":"Coupon code does not apply to this product"},"cart_max_exceeded_coupon":{"title":"Cart, Max Coupon Use Exceeded","value":"Max uses exceeded"},"cart_invalid_giftcard":{"title":"Cart, Invalid Gift Card","value":"Not a valid gift card number"},"cart_enter_coupon":{"title":"Cart, Enter Coupon Hint","value":"Enter Coupon Code"},"cart_enter_gift_code":{"title":"Cart, Enter Gift Card Hint","value":"Enter Gift Card"},"cart_coupon_expired":{"title":"Cart, Coupon Code Expired","value":"Coupon Has Expired"}}},"cart_estimate_shipping":{"label":"Cart - Estimate Shipping","options":{"cart_estimate_shipping_title":{"title":"Cart, Estimate Shipping Title","value":"Estimate Shipping Costs"},"cart_estimate_shipping_sub_title":{"title":"Cart, Estimate Shipping Sub Title","value":"Please enter your zip code to estimate shipping costs."},"cart_estimate_shipping_input_country_label":{"title":"Cart, Estimate Shipping, Input Label Country","value":"Country:"},"cart_estimate_shipping_select_one":{"title":"Cart, Estimate Shipping, Select one for country box","value":"Select One"},"cart_estimate_shipping_input_label":{"title":"Cart, Estimate Shipping, Input Label Zip Code","value":"Zip Code:"},"cart_estimate_shipping_button":{"title":"Cart, Estimate Shipping Button","value":"Estimate Shipping"},"cart_estimate_shipping_standard":{"title":"Cart, Standard Shipping Label","value":"Standard Shipping"},"cart_estimate_shipping_express":{"title":"Cart, Express Shipping Label","value":"Ship Express"},"cart_estimate_shipping_error":{"title":"Cart, Mismatch Postal Code Error","value":"Your postal code does not match our store''s country. Shipping rates will be determined after you have entered your shipping information"},"cart_estimate_shipping_hint":{"title":"Cart, Estimate Shipping Hint","value":"Enter Zip Code"},"delivery_in":{"title":"Cart, Live Shipping Delivery In (Days)","value":"delivery in"},"delivery_days":{"title":"Cart, Live Shipping (Delivery In) Days","value":"days"}}},"cart_totals":{"label":"Cart - Cart Totals","options":{"cart_totals_title":{"title":"Cart, Cart Totals Title","value":"Cart Totals"},"cart_totals_subtotal":{"title":"Cart, Cart Totals, Subtotal","value":"Cart Subtotal"},"cart_totals_shipping":{"title":"Cart, Cart Totals, Shipping","value":"Shipping"},"cart_totals_tax":{"title":"Cart, Cart Totals, Tax","value":"Tax"},"cart_totals_discounts":{"title":"Cart, Cart Totals, Discounts","value":"Discounts"},"cart_totals_vat":{"title":"Cart, Cart Totals, VAT","value":"VAT"},"cart_totals_duty":{"title":"Cart, Cart Totals, Duty","value":"Duty"},"cart_totals_grand_total":{"title":"Cart, Cart Totals, Grand Total","value":"Grand Total"},"cart_totals_label":{"title":"Cart Totals Label","value":"Cart Totals"}}},"cart_login":{"label":"Cart - Cart Login","options":{"cart_login_title":{"title":"Cart, Login Title","value":"Returning Customer"},"cart_login_sub_title":{"title":"Cart, Login Sub Title","value":"To checkout with an existing account, please sign in below."},"cart_login_email_label":{"title":"Cart, Login, Email Label","value":"Email Address"},"cart_login_password_label":{"title":"Cart, Login, Password Label","value":"Password"},"cart_login_button":{"title":"Cart, Login Button","value":"SIGN IN"},"cart_login_forgot_password_link":{"title":"Cart, Login, Forgot Password Link","value":"Forgot Your Password?"},"cart_login_account_information_title":{"title":"Cart, Login Complete Title","value":"Account Information"},"cart_login_account_information_text":{"title":"Cart, Login Complete Text","value":"You are currently checking out as "},"cart_login_account_a_guest_text":{"title":"Cart, Login Complete, a guest","value":"a guest"},"cart_login_account_information_text2":{"title":"Cart, Login Complete Text Part 2","value":" to checkout with a different account."},"cart_login_account_information_logout_link":{"title":"Cart, Login Complete, Logout Link","value":"click here"}}},"cart_guest_checkout":{"label":"Cart - Guest Checkout","options":{"cart_guest_title":{"title":"Cart, Login, Guest Checkout Title","value":"Checkout Without an Account"},"cart_guest_sub_title":{"title":"Cart, Login, Guest Checkout Sub Title","value":"Not registered? Checkout without an existing account."},"cart_guest_message":{"title":"Cart, Login, Guest Checkout Message","value":"No account? Continue as a guest.<br\\/>You will have an opportunity to create an account later."},"cart_guest_button":{"title":"Cart, Login, Guest Checkout Button","value":"CONTINUE TO CHECKOUT"}}},"cart_subscription_guest_checkout":{"label":"Cart - Subscription No Account Checkout","options":{"cart_subscription_guest_title":{"title":"Cart, Login, Subscription No Account Title","value":"Create Account on Checkout"},"cart_subscription_guest_sub_title":{"title":"Cart, Login, Subscription No Account Sub Title","value":"Not registered? Checkout without an existing account."},"cart_subscription_guest_message":{"title":"Cart, Login, Subscription No Account Message","value":"No account? No problem.<br\\/>You will have an opportunity to create an account later."},"cart_subscription_guest_button":{"title":"Cart, Login, Guest Checkout Button","value":"CONTINUE TO CHECKOUT"}}},"cart_billing_information":{"label":"Cart - Billing Information","options":{"cart_billing_information_title":{"title":"Cart, Billing Information Title","value":"Billing Information"},"cart_billing_information_first_name":{"title":"Cart, Billing, First Name Label","value":"First Name"},"cart_billing_information_last_name":{"title":"Cart, Billing, Last Name Label","value":"Last Name"},"cart_billing_information_address":{"title":"Cart, Billing, Address Label","value":"Address"},"cart_billing_information_address2":{"title":"Cart, Billing, Address 2 Label","value":"Address 2"},"cart_billing_information_city":{"title":"Cart, Billing, City Label","value":"City"},"cart_billing_information_state":{"title":"Cart, Billing, State Label","value":"State"},"cart_billing_information_select_state":{"title":"Cart, Billing, Default No State Selected","value":"Select a State"},"cart_billing_information_select_province":{"title":"Cart, Billing, Default No Province Selected","value":"Select a Province"},"cart_billing_information_select_county":{"title":"Cart, Billing, Default No County Selected","value":"Select a County"},"cart_billing_information_select_other":{"title":"Cart, Billing, Default None Selected","value":"Select One"},"cart_billing_information_country":{"title":"Cart, Billing, Country Label","value":"Country"},"cart_billing_information_select_country":{"title":"Cart, Billing, Country Menu, Default None Selected","value":"Select a Country"},"cart_billing_information_zip":{"title":"Cart, Billing, Zip Label","value":"Zip Code"},"cart_billing_information_phone":{"title":"Cart, Billing, Phone Label","value":"Phone"},"cart_billing_information_ship_to_this":{"title":"Cart, Billing, Use Billing Radio","value":"Ship to this address"},"cart_billing_information_ship_to_different":{"title":"Cart, Billing, Use Shipping Radio","value":"SHIP TO DIFFERENT ADDRESS"},"cart_billing_information_company_name":{"title":"Cart, Billing, Company Name Label","value":"Company Name"},"cart_billing_information_vat_registration_number":{"title":"Cart, Billing, VAT Registration Number Label","value":"VAT Registration Number"}}},"cart_shipping_information":{"label":"Cart - Shipping Information","options":{"cart_shipping_information_title":{"title":"Cart, Shipping Information Title","value":"Shipping Information"},"cart_shipping_information_first_name":{"title":"Cart, Shipping, First Name Label","value":"First Name"},"cart_shipping_information_last_name":{"title":"Cart, Shipping, Last Name Label","value":"Last Name"},"cart_shipping_information_address":{"title":"Cart, Shipping, Address Label","value":"Address"},"cart_shipping_information_address2":{"title":"Cart, Shipping, Address 2 Label","value":"Address 2"},"cart_shipping_information_city":{"title":"Cart, Shipping, City Label","value":"City"},"cart_shipping_information_state":{"title":"Cart, Shipping, State Label","value":"State"},"cart_shipping_information_select_state":{"title":"Cart, Shipping, Default No State Selected","value":"Select a State"},"cart_shipping_information_select_province":{"title":"Cart, Shipping, Default No Province Selected","value":"Select a Province"},"cart_shipping_information_select_county":{"title":"Cart, Shipping, Default No County Selected","value":"Select a County"},"cart_shipping_information_select_other":{"title":"Cart, Shipping, Default None Selected","value":"Select One"},"cart_shipping_information_country":{"title":"Cart, Shipping, Country Label","value":"Country"},"cart_shipping_information_select_country":{"title":"Cart, Shipping, Country Menu, Default None Selected","value":"Select a Country"},"cart_shipping_information_zip":{"title":"Cart, Shipping, Zip Label","value":"Zip Code"},"cart_shipping_information_phone":{"title":"Cart, Shipping, Phone Label","value":"Phone"},"cart_shipping_information_company_name":{"title":"Cart, Shipping, Company Name Label","value":"Company Name"}}},"cart_contact_information":{"label":"Cart - Contact Information","options":{"cart_contact_information_title":{"title":"Cart, Contact Information Title","value":"Contact Information"},"cart_contact_information_first_name":{"title":"Cart, Contact Information, First Name Label","value":"First Name"},"cart_contact_information_last_name":{"title":"Cart, Contact Information, Last Name Label","value":"Last Name"},"cart_contact_information_email":{"title":"Cart, Contact Information, Email Label","value":"Email"},"cart_contact_information_retype_email":{"title":"Cart, Contact Information, Retype Email Label","value":"Retype Email"},"cart_contact_information_create_account":{"title":"Cart, Contact Information, Create Account Label","value":"Create Account"},"cart_contact_information_password":{"title":"Cart, Contact Information, Password Label","value":"Password"},"cart_contact_information_retype_password":{"title":"Cart, Contact Information, Retype Password Label","value":"Retype Password"},"cart_contact_information_subscribe":{"title":"Cart, Contact Information, Subscribe Label","value":"Would you like to receive emails and updates from us?"},"cart_contact_information_continue_button":{"title":"Cart, Contact Information, Continue Button","value":"CONTINUE"},"cart_contact_information_continue_payment":{"title":"Cart, Contact Information, Continue to Payment","value":"CONTINUE TO PAYMENT"},"cart_contact_information_continue_shipping":{"title":"Cart, Contact Information, Continue to Shipping","value":"CONTINUE TO SHIPPING"}}},"cart_shipping_method":{"label":"Cart - Shipping Method","options":{"cart_shipping_method_title":{"title":"Cart, Shipping Method, Title","value":"Shipping Method"},"cart_shipping_method_continue_button":{"title":"Cart, Shipping Method Continue Button","value":"CONTINUE"},"cart_shipping_method_please_select_one":{"title":"Cart, Shipping please select one","value":"Please select a shipping method"},"cart_shipping_update_shipping":{"title":"Cart, Update Shipping","value":"Update Shipping"},"cart_shipping_no_rates_available":{"title":"Cart, No Shipping Rates Available","value":"There are no available shipping methods in your area. Contact us to complete your order."}}},"cart_address_review":{"label":"Cart - Address Review","options":{"cart_address_review_title":{"title":"Cart, Address Information Title","value":"Address Information"},"cart_address_review_billing_title":{"title":"Cart, Address Review, Billing Title","value":"Billing Address"},"cart_address_review_shipping_title":{"title":"Cart, Address Review, Shipping Title","value":"Shipping Address"},"cart_address_review_edit_link":{"title":"Cart, Address Review, Edit Link","value":"Edit Address or Shipping Options"},"cart_address_review_edit_link2":{"title":"Cart, Address Review, Edit Link","value":"Edit Address Information"}}},"cart_payment_information":{"label":"Cart - Payment Information","options":{"cart_payment_information_title":{"title":"Cart, Payment Information Title","value":"Payment Information"},"cart_payment_information_manual_payment":{"title":"Cart, Payment Information, Manual Payment Label","value":"Pay by Direct Deposit"},"cart_payment_information_affirm":{"title":"Cart, Payment Information, Affirm Payment Label","value":"Pay with Easy Monthly Payments"},"cart_payment_information_third_party":{"title":"Cart, Payment Information, Third Party Payment Label","value":"Pay Using"},"cart_payment_information_third_party_first":{"title":"Cart, Payment Information, Third Party Text, Part 1","value":"You will be redirected to"},"cart_payment_information_third_party_second":{"title":"Cart, Payment Information, Third Party Text, Part 2","value":"when you click submit order to complete your payment."},"cart_payment_information_credit_card":{"title":"Cart, Payment Information, Credit Card Payment Label","value":"Pay by Credit Card"},"cart_payment_information_payment_method":{"title":"Cart, Payment Information, Payment Method Label","value":"Payment Method"},"cart_payment_information_card_holder_name":{"title":"Cart, Payment Information, Card Holder Label","value":"Card Holder Name"},"cart_payment_information_card_number":{"title":"Cart, Payment Information, Card Number Label","value":"Card Number"},"cart_payment_information_expiration_date":{"title":"Cart, Payment Information, Expiration Date Label","value":"Expiration Date"},"cart_payment_information_security_code":{"title":"Cart, Payment Information, Security Code Label","value":"Security Code"},"cart_payment_information_checkout_text":{"title":"Cart, Payment Information, Checkout Text","value":"By submitting your order, you are agreeing to the [terms]terms and conditions[\\/terms] and [privacy]privacy policy[\\/privacy] of our website.  Once you click submit order, the checkout process is complete and no changes can be made to your order. Thank you for shopping with us!"},"cart_payment_information_review_checkout_text":{"title":"Cart, Payment Information, Review Checkout Text","value":"Before submitting your order and making it final, you will be given a chance to review and confirm the final totals. Please click the review order button when you are ready to review, confirm, and submit your order."},"cart_payment_information_review_order_button":{"title":"Cart, Payment Information, Review Order Button","value":"REVIEW ORDER"},"cart_payment_information_submit_order_button":{"title":"Cart, Payment Information, Submit Order Button","value":"SUBMIT ORDER"},"cart_payment_information_cancel_order_button":{"title":"Cart, Payment Information, Edit Order Button","value":"EDIT ORDER"},"cart_payment_information_order_notes_title":{"title":"Cart, Payment Information, Customer Notes Title","value":"ORDER NOTES"},"cart_payment_information_order_notes_message":{"title":"Cart, Payment Information, Customer Notes Message","value":"If you have any special requests for your order please enter them here."},"cart_payment_information_review_title":{"title":"Cart, Review Cart Title","value":"REVIEW YOUR CART"},"cart_payment_information_edit_cart_link":{"title":"Cart, edit cart items link","value":"edit cart items"},"cart_payment_information_edit_billing_link":{"title":"Cart, edit billing link","value":"edit billing address"},"cart_payment_information_edit_shipping_link":{"title":"Cart, edit shipping link","value":"edit shipping address"},"cart_payment_information_edit_shipping_method_link":{"title":"Cart, edit shipping link","value":"edit shipping method"},"cart_payment_review_totals_title":{"title":"Cart, Review Total Title","value":"REVIEW CART TOTALS"},"cart_payment_review_agree":{"title":"Cart, Review Agree Text","value":"I agree to the terms and conditions of this site."},"cart_change_payment_method":{"title":"Cart, Change Payment Method","value":"change payment method"}}},"cart_success":{"label":"Order Receipt","options":{"cart_payment_receipt_order_details_link":{"title":"Email Receipt, Link to Account","value":"View Order Details"},"cart_payment_receipt_subscription_details_link":{"title":"Email Receipt, Link to Subscription","value":"View Subscription Details"},"cart_payment_receipt_title":{"title":"Email Receipt Title","value":"Order Confirmation - Order Number"},"cart_payment_complete_line_1":{"title":"Cart, Payment Complete, Line 1","value":"Dear:"},"cart_payment_complete_line_2":{"title":"Cart, Payment Complete, Line 2","value":"Thank you for your order! Your Reference Number is:"},"cart_payment_complete_line_3":{"title":"Cart, Payment Complete, Line 3","value":"Below is a summary of order. You can check the status of your order and all the details by visiting our website and logging into your account."},"cart_payment_complete_line_4":{"title":"Cart, Payment Complete, Line 4","value":"Please keep this as a record of your order!"},"cart_payment_complete_line_5":{"title":"Cart, Payment Complete, Line 5","value":"Click Here to View Your Membership Content"},"cart_payment_complete_click_here":{"title":"Cart, Payment Complete, Click Here","value":"Click Here"},"cart_payment_complete_to_view_order":{"title":"Cart, Payment Complete, To View Order","value":"to View Your Order"},"cart_payment_complete_billing_label":{"title":"Cart, Payment Complete, Billing Label","value":"Billing Address"},"cart_payment_complete_shipping_label":{"title":"Cart, Payment Complete, Shipping Label","value":"Shipping Address"},"cart_payment_complete_details_header_1":{"title":"Cart, Payment Complete, Header 1","value":"Product"},"cart_payment_complete_details_header_2":{"title":"Cart, Payment Complete, Header 2","value":"Qty"},"cart_payment_complete_details_header_3":{"title":"Cart, Payment Complete, Header 3","value":"Price"},"cart_payment_complete_details_header_4":{"title":"Cart, Payment Complete, Header 4","value":"Ext Price"},"cart_payment_complete_order_totals_subtotal":{"title":"Cart, Payment Complete, Subtotal","value":"Subtotal"},"cart_payment_complete_order_totals_shipping":{"title":"Cart, Payment Complete, Shipping","value":"Shipping"},"cart_payment_complete_order_totals_tax":{"title":"Cart, Payment Complete, Tax","value":"Tax"},"cart_payment_complete_order_totals_discount":{"title":"Cart, Payment Complete, Discount","value":"Discount"},"cart_payment_complete_order_totals_vat":{"title":"Cart, Payment Complete, VAT","value":"VAT @"},"cart_payment_complete_order_totals_duty":{"title":"Cart, Payment Complete, Duty","value":"Duty"},"cart_payment_complete_order_totals_grand_total":{"title":"Cart, Payment Complete, Order Total","value":"Order Total"},"cart_payment_complete_bottom_line_1":{"title":"Cart, Payment Complete, Bottom Line 1","value":"Please double check your order when you receive it and let us know immediately if there are any concerns or issues. We always value your business and hope you enjoy your product."},"cart_payment_complete_bottom_line_2":{"title":"Cart, Payment Complete, Bottom Line 2","value":"Thank you very much!"},"cart_success_thank_you_title":{"title":"Cart, Payment Complete, Thank You Title","value":"Thank you for your order"},"cart_success_order_number_is":{"title":"Cart, Payment Complete, Your Order Number","value":"Order number is:"},"cart_success_will_receive_email":{"title":"Cart, Payment Complete, Will receive email","value":"You will receive an email confirmation shortly at"},"cart_success_print_receipt_text":{"title":"Cart, Payment Complete, Print Receipt Link Text","value":"Print Receipt"},"cart_success_save_order_text":{"title":"Cart, Payment Complete, Save Your Info","value":"Save your information for next time"},"cart_success_create_password":{"title":"Cart, Payment Complete, Create Password","value":"Create Password:"},"cart_success_verify_password":{"title":"Cart, Payment Complete, Verify Password","value":"Verify Password:"},"cart_success_password_hint":{"title":"Cart, Payment Complete, Password Hint","value":"(6-12 Characters)"},"cart_success_create_account":{"title":"Cart, Payment Complete, Create Account","value":"Create Account"},"cart_giftcard_receipt_title":{"title":"Cart, Gift Card Email Subject","value":"Gift Card Delivery"},"cart_giftcard_receipt_header":{"title":"Cart, Gift Card Header","value":"Your Gift Card"},"cart_giftcard_receipt_to":{"title":"Cart, Gift Card To","value":"To"},"cart_giftcard_receipt_from":{"title":"Cart, Gift Card From","value":"From"},"cart_giftcard_receipt_id":{"title":"Cart, Gift Card ID Label","value":"Your Gift Card ID"},"cart_giftcard_receipt_amount":{"title":"Cart, Gift Card Amount","value":"Gift Card Total"},"cart_giftcard_receipt_message":{"title":"Cart, Gift Card Message","value":"You can redeem your gift card during checkout at"},"cart_downloads_available":{"title":"Cart Success, Downloads Available","value":"Your order includes downloads and are available in your account."},"cart_downloads_unavailable":{"title":"Cart Success, Downloads Unavailable","value":"Your order includes downloads, but your payment has not yet been approved. Once payment has been approved your downloads will be available in your account."},"cart_downloads_click_to_go":{"title":"Cart Success, Downloads Click to go Text","value":"Click to go there now."},"cart_success_view_downloads":{"title":"Cart Success, View Downloads","value":"View Downloads"}}},"subscription_trial":{"label":"Subscription Trial Email","options":{"trial_message_1":{"title":"Subscription Trial Part 1","value":"Thank you for signing up! Your"},"trial_message_2":{"title":"Subscription Trial Part 2","value":"day free trial of"},"trial_message_3":{"title":"Subscription Trial Part 3","value":"has now started! If you have any questions or concerns during your trial, please contact us, we are happy to help."},"trial_message_4":{"title":"Subscription Trial Part 4","value":"We hope you stick with us, but if you decide this isn''t for you, you may cancel at any time prior to the end of your trial to prevent being charged. To cancel, log into your account, click manage subscriptions, view your subscription, and cancel on the details page."},"trial_message_link":{"title":"View Subscription Information","value":"View Subscription Information"},"subscription_trial_email_title":{"title":"Subscription Trial Email Title","value":"Your Trial Has Started"},"trial_ending_message_1":{"title":"Subscription Trial Ending Part 1","value":"Your"},"trial_ending_message_2":{"title":"Subscription Trial Part 2","value":"day free trial of"},"trial_ending_message_3":{"title":"Subscription Trial Part 3","value":"is ending in only 3 days! If you have any questions or concerns, please contact us! We are happy to help."},"trial_ending_message_4":{"title":"Subscription Trial Part 4","value":"We hope you stick with us, but if you decide this isn''t for you, you may cancel at any time prior to the end of your trial to prevent being charged. To cancel, log into your account, click manage subscriptions, view your subscription, and cancel on the details page."},"trial_ending_message_link":{"title":"View Subscription Information","value":"View Subscription Information"},"subscription_trial_ending_email_title":{"title":"Subscription Trial Email Title","value":"Your Trial is Ending in 3 Days"}}},"subscription_ended":{"label":"Subscription Ended Email","options":{"ended_message_1":{"title":"Subscription Ended Part 1","value":"Your subscription with us has either ended, been cancelled, or expired due to too many failed payments."},"ended_message_2":{"title":"Subscription Ended Part 2","value":"We are sorry to see you go and hope you return. If this subscription was cancelled by accident, you may click the link below to purchase a new subscription."},"ended_details":{"title":"Subscription Ended Details","value":"The following subscription has ended:"},"emded_message_link":{"title":"Start a New Subscription","value":"Start a New Subscription"},"subscription_ended_email_title":{"title":"Subscription Ended Email Title","value":"Your Subscription Has Ended"}}},"account_login":{"label":"Account - Login","options":{"account_login_title":{"title":"Account, Login Title","value":"Returning Customer"},"account_login_sub_title":{"title":"Account, Login Sub Title","value":"Sign in below to access your existing account."},"account_login_email_label":{"title":"Account, Login, Email Label","value":"Email Address"},"account_login_password_label":{"title":"Account, Login, Password Label","value":"Password"},"account_login_button":{"title":"Account, Login Button","value":"SIGN IN"},"account_login_forgot_password_link":{"title":"Account, Login, Forgot Password Link","value":"Forgot Your Password?"},"account_login_account_information_title":{"title":"Account, Login Complete Title","value":"Account Information"},"account_login_account_information_text":{"title":"Account, Login Complete Text","value":"You are currently checking out as Demo User. To checkout with a different account,"},"account_login_account_information_logout_link":{"title":"Account, Login Complete, Logout Link","value":"click here"},"account_new_user_title":{"title":"Account, Login, New User Title","value":"New User"},"account_new_user_sub_title":{"title":"Account, Login, New User Sub Title","value":"Not registered? Click the button below"},"account_new_user_message":{"title":"Account, Login, New User Message","value":"No account? Create an account to take full advantage of this website."},"account_new_user_button":{"title":"Account, Login, New User Button","value":"CREATE ACCOUNT"}}},"account_forgot_password":{"label":"Account - Forgot Password","options":{"account_forgot_password_title":{"title":"Account, Retrieve Password Title","value":"Retrieve Your Password"},"account_forgot_password_email_label":{"title":"Account, Retrieve Password Email Label","value":"Email"},"account_forgot_password_button":{"title":"Account, Retrieve Password Button","value":"RETRIEVE PASSWORD"}}},"account_validation_email":{"label":"Account - Validation Email","options":{"account_validation_email_message":{"title":"Account, Short Validate Email Message","value":"To activate your account, please click the link below."},"account_validation_email_title":{"title":"Account, Activate Your Account Title","value":"Please Activate Your Account"},"account_validation_email_link":{"title":"Account, Activate Your Account Link","value":"Click Here to Activate"}}},"account_register":{"label":"Account - Register","options":{"account_register_title":{"title":"Account, Register Title","value":"Create an Account"},"account_register_first_name":{"title":"Account, Register, First Name Label","value":"First Name"},"account_register_last_name":{"title":"Account, Register, Last Name Label","value":"Last Name"},"account_register_email":{"title":"Account, Register, Email Label","value":"Email Address"},"account_register_password":{"title":"Account, Register, Password Label","value":"Password"},"account_register_retype_password":{"title":"Account, Register, Re-type Password Label","value":"Re-type Password"},"account_register_subscribe":{"title":"Account, Register, Subscribe Text","value":"Would you like to subscribe to our email list?"},"account_register_button":{"title":"Account, Register Button","value":"REGISTER"},"account_billing_information_user_notes":{"title":"Account, Billing, User Notes","value":"Additional Information about your Registration"},"account_register_email_title":{"title":"Account, Register Email Title","value":"New EasyCart Registration"},"account_register_email_message":{"title":"Account, Register Email Message","value":"New Registration with Email Address:"}}},"account_navigation":{"label":"Account - Navigation","options":{"account_navigation_title":{"title":"Account, Navigation Title","value":"Account Navigation"},"account_navigation_basic_inforamtion":{"title":"Account, Navigation, Basic Information Link","value":"basic information"},"account_navigation_orders":{"title":"Account, Navigation, View Orders Link","value":"view orders"},"account_navigation_password":{"title":"Account, Navigation, Change Password Link","value":"change password"},"account_navigation_subscriptions":{"title":"Account, Navigation, Subscriptions Link","value":"manage subscriptions"},"account_navigation_payment_methods":{"title":"Account, Navigation, Payments Link","value":"manage payment methods"},"account_navigation_sign_out":{"title":"Account, Navigation, Sign Out Link","value":"sign out"},"account_navigation_dashboard":{"title":"Account, Navigation, Dashboard Link","value":"account dashboard"},"account_navigation_billing_information":{"title":"Account, Navigation, Billing Link","value":"billing information"},"account_navigation_shipping_information":{"title":"Account, Navigation, Shipping Link","value":"shipping information"}}},"account_dashboard":{"label":"Account - Dashboard","options":{"account_dashboard_recent_orders_title":{"title":"Account, Dashboard, Recent Orders Title","value":"Recent Orders"},"account_dashboard_recent_orders_none":{"title":"Account, Dashboard, Recent Order, No Orders","value":"No Orders Have Been Placed"},"account_dashboard_all_orders_linke":{"title":"Account, Dashboard, All Orders Link","value":"VIEW ALL ORDERS"},"account_dashboard_order_placed":{"title":"Account, Dashboard, Order Placed Label","value":"ORDER PLACED"},"account_dashboard_order_total":{"title":"Account, Dashboard, Total Label","value":"TOTAL"},"account_dashboard_order_ship_to":{"title":"Account, Dashboard, Ship To Label","value":"SHIP TO"},"account_dashboard_order_order_label":{"title":"Account, Dashboard, Order Label","value":"ORDER"},"account_dashboard_order_view_details":{"title":"Account, Dashboard, View Details Link","value":"View Details"},"account_dashboard_order_buy_item_again":{"title":"Account, Dashboard, Buy Item Again","value":"Buy Item Again"},"account_dashboard_preference_title":{"title":"Account, Dashboard, Preference Title","value":"Account Preferences"},"account_dashboard_email_title":{"title":"Account, Dashboard, Email Title","value":"Your Primary Email"},"account_dashboard_email_edit_link":{"title":"Account, Dashboard, Email Edit Link","value":"edit"},"account_dashboard_email_note":{"title":"Account, Dashboard, Email Note","value":"(This is used to log in)"},"account_dashboard_billing_title":{"title":"Account, Dashboard, Billing Title","value":"Billing Address"},"account_dashboard_billing_link":{"title":"Account, Dashboard, Billing Edit Link","value":"edit billing address"},"account_dashboard_shipping_title":{"title":"Account, Dashboard, Shipping Title","value":"Shipping Address"},"account_dashboard_shipping_link":{"title":"Account, Dashboard, Shipping Edit Link","value":"edit shipping address"}}},"account_personal_information":{"label":"Account - Edit Personal Information","options":{"account_personal_information_title":{"title":"Account, Personal Information Title","value":"Personal Information"},"account_personal_information_sub_title":{"title":"Account, Personal Information Sub Title","value":"Please note: Changes made to your account information, including shipping addresses, will only affect new orders. All previously placed orders will be sent to the address listed on the date of purchase."},"account_personal_information_first_name":{"title":"Account, Personal Information, First Name Label","value":"First Name"},"account_personal_information_last_name":{"title":"Account, Personal Information, Last Name Label","value":"Last Name"},"account_personal_information_email":{"title":"Account, Personal Information, Email Label","value":"Email Address"},"account_personal_information_subscribe":{"title":"Account, Personal Information, Subscribe Label","value":"Would you like to subscribe to our newsletter?"},"account_personal_information_update_button":{"title":"Account, Personal Information, Update Button","value":"UPDATE"},"account_personal_information_cancel_link":{"title":"Account, Personal Information, Cancel Link","value":"CANCEL"}}},"account_password":{"label":"Account - Edit Password","options":{"account_password_title":{"title":"Account, Password Title","value":"Edit Your Password"},"account_password_sub_title":{"title":"Account, Password Sub Title","value":"Please note: Once you change your password you will be required to use the new password each time you log into our site."},"account_password_current_password":{"title":"Account, Password, Current Password Label","value":"Current Password"},"account_password_new_password":{"title":"Account, Password, New Password Label","value":"New Password"},"account_password_retype_new_password":{"title":"Account, Password, Retype New Password Label","value":"Retype New Password"},"account_password_update_button":{"title":"Account, Password, Update Button","value":"UPDATE"},"account_password_cancel_button":{"title":"Account, Password, Cancel Link","value":"CANCEL"}}},"account_billing_information":{"label":"Account - Edit Billing Information","options":{"account_billing_information_title":{"title":"Account, Billing Information Title","value":"Billing Information"},"account_billing_information_first_name":{"title":"Account, Billing Information, First Name Label","value":"First Name"},"account_billing_information_last_name":{"title":"Account, Billing Information, Last Name Label","value":"Last Name"},"account_billing_information_address":{"title":"Account, Billing Information, Address Label","value":"Address"},"account_billing_information_address2":{"title":"Account, Billing Information, Address 2 Label","value":"Address 2"},"account_billing_information_city":{"title":"Account, Billing Information, City Label","value":"City"},"account_billing_information_state":{"title":"Account, Billing Information, State Label","value":"State"},"account_billing_information_default_no_state":{"title":"Account, Billing Information, No State Label","value":"Select a State"},"account_billing_information_zip":{"title":"Account, Billing Information, Zip Label","value":"Zip Code"},"account_billing_information_country":{"title":"Account, Billing Information, Country Label","value":"Country"},"account_billing_information_default_no_country":{"title":"Account, Billing Information, No Country Label","value":"Select a Country"},"account_billing_information_phone":{"title":"Account, Billing Information, Phone Label","value":"Phone"},"account_billing_information_update_button":{"title":"Account, Billing Information, Update Button","value":"UPDATE"},"account_billing_information_cancel":{"title":"Account, Billing Information, Cancel Link","value":"CANCEL"}}},"account_shipping_information":{"label":"Account - Edit Shipping Information","options":{"account_shipping_information_title":{"title":"Account, Shipping Information Title","value":"Shipping Information"},"account_shipping_information_first_name":{"title":"Account, Shipping Information, First Name Label","value":"First Name"},"account_shipping_information_last_name":{"title":"Account, Shipping Information, Last Name Label","value":"Last Name"},"account_shipping_information_address":{"title":"Account, Shipping Information, Address Label","value":"Address"},"account_shipping_information_address2":{"title":"Account, Shipping Information, Address 2 Label","value":"Address 2"},"account_shipping_information_city":{"title":"Account, Shipping Information, City Label","value":"City"},"account_shipping_information_state":{"title":"Account, Shipping Information, State Label","value":"State"},"account_shipping_information_default_no_state":{"title":"Account, Shipping Information, No State Label","value":"Select a State"},"account_shipping_information_zip":{"title":"Account, Shipping Information, Zip Label","value":"Zip Code"},"account_shipping_information_country":{"title":"Account, Shipping Information, Country Label","value":"Country"},"account_shipping_information_default_no_country":{"title":"Account, Shipping Information, No Country Label","value":"Select a Country"},"account_shipping_information_phone":{"title":"Account, Shipping Information, Phone Label","value":"Phone"},"account_shipping_information_update_button":{"title":"Account, Shipping Information, Update Button","value":"UPDATE"},"account_shipping_information_cancel":{"title":"Account, Shipping Information, Cancel Link","value":"CANCEL"}}},"account_orders":{"label":"Account - Orders","options":{"account_orders_title":{"title":"Account, Orders Title","value":"Your Order History"},"account_orders_header_1":{"title":"Account, Orders, Header 1","value":"ID"},"account_orders_header_2":{"title":"Account, Orders, Header 2","value":"Date"},"account_orders_header_3":{"title":"Account, Orders, Header 3","value":"Total"},"account_orders_header_4":{"title":"Account, Orders, Header 4","value":"Status"},"account_orders_view_order_button":{"title":"Account, Orders, View Order Button","value":"VIEW ORDER"}}},"account_forgot_password_email":{"label":"Account - Forgot Password Recovery Email","options":{"account_forgot_password_email_title":{"title":"Account, Password Recovery Email, Email Title","value":"Your New Password"},"account_forgot_password_email_dear":{"title":"Account, Password Recovery Email, Dear User","value":"Dear"},"account_forgot_password_email_your_new_password":{"title":"Account, Password Recovery Email, Your New Password","value":"Your new password is:"},"account_forgot_password_email_change_password":{"title":"Account, Password Recovery Email, Change Password Text","value":"Be sure to log into your account and change your password to something you can remember."},"account_forgot_password_email_thank_you":{"title":"Account, Password Recovery Email, Thank You Text","value":"Thank You Very much!"}}},"account_order_details":{"label":"Account - Order Details","options":{"account_orders_details_order_info_title":{"title":"Account, Orders Details, Order Information Title","value":"Order Information"},"account_orders_details_your_order_title":{"title":"Account, Orders Details, Your Order Title","value":"Your Order"},"account_orders_details_order_number":{"title":"Account, Orders Details, Order Number Label","value":"Order Number:"},"account_orders_details_order_date":{"title":"Account, Orders Details, Order Date Label","value":"Order Date:"},"account_orders_details_order_status":{"title":"Account, Orders Details, Order Status Label","value":"Order Status:"},"account_orders_details_order_tracking":{"title":"Account, Orders Details, Order Tracking Label","value":"Tracking Number:"},"account_orders_details_shipping_method":{"title":"Account, Orders Details, Shipping Method Label","value":"Shipping Method:"},"account_orders_details_coupon_code":{"title":"Account, Orders Details, Coupon Code Label","value":"Coupon Code:"},"account_orders_details_gift_card":{"title":"Account, Orders Details, Gift Card Label","value":"Gift Card:"},"account_orders_details_view_subscription":{"title":"Account, Orders Details, View Subscription","value":"View Subscription Details"},"account_orders_details_billing_label":{"title":"Account, Orders Details, Billing Label","value":"Billing Address"},"account_orders_details_shipping_label":{"title":"Account, Orders Details, Shipping Label","value":"Shipping Address"},"account_orders_details_payment_method":{"title":"Account, Orders Details, Payment Method","value":"Payment Method:"},"account_order_details_payment_method_manual":{"title":"Account, Order Details, Payment Method, Manual Bill Label","value":"Direct Deposit"},"account_orders_details_subtotal":{"title":"Account, Orders Details, Subtotal","value":"Sub Total:"},"account_orders_details_tax_total":{"title":"Account, Orders Details, Tax Total","value":"Tax Total:"},"account_orders_details_shipping_total":{"title":"Account, Orders Details, Shipping Total","value":"Shipping Total:"},"account_orders_details_discount_total":{"title":"Account, Orders Details, Discount Total","value":"Discount Total:"},"account_orders_details_duty_total":{"title":"Account, Orders Details, Duty Total","value":"Duty Total:"},"account_orders_details_vat_total":{"title":"Account, Orders Details, VAT Total","value":"VAT Total:"},"account_orders_details_refund_total":{"title":"Account, Orders Details, Refund Total","value":"Refunded Amount:"},"account_orders_details_grand_total":{"title":"Account, Orders Details, Grand Total","value":"Order Total:"},"account_orders_details_header_1":{"title":"Account, Orders Details, Header 1","value":"Product"},"account_orders_details_header_2":{"title":"Account, Orders Details, Header 2","value":"Options"},"account_orders_details_header_3":{"title":"Account, Orders Details, Header 3","value":"Price"},"account_orders_details_header_4":{"title":"Account, Orders Details, Header 4","value":"Quantity"},"account_orders_details_header_5":{"title":"Account, Orders Details, Header 5","value":"Total"},"account_orders_details_gift_message":{"title":"Account, Orders Details, Gift Card Message","value":"message: "},"account_orders_details_gift_from":{"title":"Account, Orders Details, Gift Card From","value":"from: "},"account_orders_details_gift_to":{"title":"Account, Orders Details, Gift Card To","value":"to: "},"account_orders_details_gift_card_id":{"title":"Account, Orders Details, Gift Card ID","value":"Gift Card Code: "},"account_orders_details_print_online":{"title":"Account, Orders Details, Print Online Link","value":"Print Online"},"account_orders_details_download":{"title":"Account, Orders Details, Download","value":"Download"},"account_orders_details_downloads_used":{"title":"Account, Orders Details, Downloads Used","value":"Downloads Used"},"account_orders_details_downloads_expire_time":{"title":"Account, Orders Details, Download Expiration","value":"Download Expires On:"},"account_orders_details_your_codes":{"title":"Account, Orders Details, Your Codes","value":"Your Code(s):"},"complete_payment":{"title":"Account, Orders Details, Complete Payment","value":"Complete Order Payment"},"order_status_status_not_found":{"title":"Account, Orders Status, Status Not Found","value":"Status Not Found"},"order_status_order_shipped":{"title":"Account, Orders Status, Order Shipped","value":"Order Shipped"},"order_status_order_confirmed":{"title":"Account, Orders Status, Order Confirmed","value":"Order Confirmed"},"order_status_order_on_hold":{"title":"Account, Orders Status, Order on Hold","value":"Order on Hold"},"order_status_order_started":{"title":"Account, Orders Status, Order Started","value":"Order Started"},"order_status_card_approved":{"title":"Account, Orders Status, Card Approved","value":"Card Approved"},"order_status_card_denied":{"title":"Account, Orders Status, Card Denied","value":"Card Denied"},"order_status_third_party_pending":{"title":"Account, Orders Status, Third Party Pending","value":"Third Party Pending"},"order_status_third_party_error":{"title":"Account, Orders Status, Third Party Error","value":"Third Party Error"},"order_status_third_party_approved":{"title":"Account, Orders Status, Third Party Approved","value":"Third Party Approved"},"order_status_ready_for_pickup":{"title":"Account, Orders Status, Ready for Pickup","value":"Ready for Pickup"},"order_status_pending_approval":{"title":"Account, Orders Status, Pending Approval","value":"Pending Approval"},"order_status_direct_deposit_pending":{"title":"Account, Orders Status, Direct Deposit Pending","value":"Direct Deposit Pending"},"order_status_direct_deposit_received":{"title":"Account, Orders Status, Direct Deposit Received","value":"Direct Deposit Received"},"order_status_refunded_order":{"title":"Account, Orders Status, Refunded Order","value":"Refunded Order"},"order_status_partial_refund":{"title":"Account, Orders Status, Partial Refund","value":"Partial Refund"},"order_status_order_picked_up":{"title":"Account, Orders Status, Order Picked UP","value":"Order Picked Up"},"no_order_found":{"title":"Account, No Order Found","value":"Order was not found or is not associated with the account you are currently logged into"},"return_to_dashboard":{"title":"Account, Return to Dashboard","value":"Return to Account Dashboard"}}},"account_subscriptions":{"label":"Account - Subscriptions","options":{"account_subscriptions_title":{"title":"Account, Subscriptions Title","value":"Your Active Subscriptions"},"account_canceled_subscriptions_title":{"title":"Account, Canceled Subscriptions Title","value":"Your Canceled Subscriptions"},"account_subscriptions_header_1":{"title":"Account, Subscriptions, Header 1","value":"Subscription Name"},"account_subscriptions_header_2":{"title":"Account, Subscriptions, Header 2","value":"Next Bill Date"},"account_subscriptions_header_3":{"title":"Account, Subscriptions, Header 3","value":"Last Charged"},"account_subscriptions_header_4":{"title":"Account, Subscriptions, Header 4","value":"Price\\/Cycle"},"account_subscriptions_view_subscription_button":{"title":"Account, Subscriptions, View Details Button","value":"VIEW DETAILS"},"account_subscriptions_none_found":{"title":"Account, Subscriptions, None Found","value":"There are no subscriptions associated with your account."},"update_subscription_subtitle":{"title":"Account, Subscription Details, Credit Card Note","value":"You are not required to update your credit card information."},"save_changes_button":{"title":"Account, Subscription Details, Save Changes Button","value":"SAVE CHANGES"},"cancel_subscription_button":{"title":"Account, Subscription Details, Cancel Subscription Button","value":"CANCEL SUBSCRIPTION"},"cancel_subscription_confirm_text":{"title":"Account, Subscription Details, Cancel Subscription Button","value":"Are you sure you would like to cancel this subscription?"},"subscription_details_next_billing":{"title":"Account, Subscription Details, Next Billing Date","value":"Next Billing Date"},"subscription_details_last_payment":{"title":"Account, Subscription Details, Last Payment Date","value":"Last Payment Date"},"subscription_details_current_billing":{"title":"Account, Subscription Details, Current Billing Title","value":"Current Billing Method"},"subscription_details_notice":{"title":"Account, Subscription Details, User Notice","value":"*NOTICE: any changes to the billing method or subscription plan will take effect in the next billing cycle. Pricing changes will be prorated beginning immediately and will be reflected on your next bill."},"subscription_details_past_payments":{"title":"Account, Subscription Details, Past Payments Title","value":"Past Payments"}}},"ec_errors":{"label":"Store Error Text","options":{"login_failed":{"title":"Account, User Login Failed","value":"The username or password you entered is incorrect."},"register_email_error":{"title":"Account Registration Email In Use Error","value":"The email you have entered already has an account."},"no_reset_email_found":{"title":"No Email Found Error","value":"The email address you entered was not found."},"personal_information_update_error":{"title":"Account, updating personal info error","value":"An error occurred while updating your personal information."},"password_wrong_current":{"title":"Wrong password entered Error","value":"The current password you entered did not match your account password."},"password_no_match":{"title":"Passwords do not match Error","value":"The new password and the retype new password values did not match."},"password_update_error":{"title":"password update error","value":"An error occurred while updating your password."},"billing_information_error":{"title":"Account, billing information update error","value":"An error occurred while updating your billing information."},"shipping_information_error":{"title":"Account, shipping information update error","value":"An error occurred while updating your shipping information."},"email_exists_error":{"title":"Cart, create account on checkout, email exists","value":"This email already has an account. Please login or use a different email address to continue."},"3dsecure_failed":{"title":"Cart, Optional, Error message when 3D Secure Fails, Gateway Dependent","value":"Your payment could not completed because the 3D Secure method failed. Please try your payment again. If your problem persists, please contact us to have the issue resolved."},"manualbill_failed":{"title":"Cart, Optional, Manual billing failed, Gateway Dependent","value":"Your payment could not completed, manual billing failed. Please try again."},"thirdparty_failed":{"title":"Cart, Optional, Third Party Failed, Gateway Dependent","value":"Your payment could not completed, third party failed. Please try again."},"payment_failed":{"title":"Cart, Optional, Credit Card Failed, Gateway Dependent","value":"Your payment could not completed because your credit card could not be verified. Please check your credit card information and try again. If your problem persists, please contact us to complete payment."},"already_subscribed":{"title":"Cart, Optional, Already Subscribed, Gateway Dependent","value":"You have already subscribed to this product. Please visit your account to manage your active subscriptions."},"subscription_not_found":{"title":"Account, Subscription Not Found Error","value":"An unknown error occurred in which the subscription you are trying to purchase was not found in our system. Please try again and contact us if you have continued difficulties."},"invalid_address":{"title":"Cart, Invalid Live Shipping Address","value":"Something is wrong with the address you have entered. Please check your city, state, zip, and country to make sure the values are correct."},"activation_error":{"title":"Account, Activation Error","value":"An error has occurred while attempting to activate your account. Please contact us to have the issue resolved."},"not_activated":{"title":"Account, Not Activated","value":"The account you are attempting to access has not been activated. Please activate your account through the activation email sent when you signed up for an account or contact us if you are having problems."},"subscription_update_failed":{"title":"Account, Subscription Update Error","value":"There was a problem while updating your subscription, please try again or contact us if you continue to have problems."},"subscription_cancel_failed":{"title":"Account, Subscription Cancel Error","value":"There was a problem while cancelling your subscription, please try again or contact us if you continue to have problems."},"user_insert_error":{"title":"Account, Subscription User Insert Error","value":"There was a problem creating your user account in our subscription system. Please contact us to complete your transaction."},"subscription_added_failed":{"title":"Account, Subscription Plan Added Error","value":"There was an internal problem while creating this product in our subscription service. Please contact us to complete your transaction."},"subscription_failed":{"title":"Account, Subscription Creation Error","value":"There was a problem while creating your subscription in our system. Please contact us to complete your transaction."},"nets_processing":{"title":"Account, Nets Error Level Authorize","value":"While completing your order at Nets Netaxept, an error occurred. You may complete your order at any time by clicking the button below."},"nets_processing_payment":{"title":"Account, Nets Error Level Capture","value":"Your payment has been authorized at Nets Netaxept, but there seems to have been an issue in our system. Please contact us to complete your order."},"subscription_payment_failed_title":{"title":"Cart, Subscription Payment Failed","value":"Payment Failed"},"subscription_payment_failed_text":{"title":"Cart, Subscription Payment Failed Text","value":"The payment has failed for one of your subscriptions. Please click the link below to update your credit card information."},"subscription_payment_failed_link":{"title":"Cart, Subscription Payment Failed Link","value":"Click to Update Billing"},"minquantity":{"title":"Store, Minimum Quantity Error","value":"This product requires a minimum purchase quantity."},"missing_gift_card_options":{"title":"Store, Missing Gift Card Options","value":"Missing Gift Card Options"},"missing_inquiry_options":{"title":"Store, Missing Inquiry Options","value":"Missing Inquiry Options"},"session_expired":{"title":"Store, Session Expired","value":"Your session has expired, please enter your checkout information again to complete your order."},"invalid_vat_number":{"title":"Cart, Invalid VAT Number Entered","value":"Your VAT registration number is invalid. Please leave this field empty or correct the error to continue."}}},"ec_success":{"label":"Store Success Text","options":{"personal_information_updated":{"title":"Account Personal Information Update Success","value":"Your personal information was updated successfully."},"password_updated":{"title":"Account Personal Information Update Success","value":"Your password was updated successfully."},"billing_information_updated":{"title":"Account Billing Update Success","value":"Your billing information was updated successfully."},"shipping_information_updated":{"title":"Account Shipping Update Success","value":"Your shipping information was updated successfully."},"reset_email_sent":{"title":"Account Email Sent Success","value":"Your new password has been sent to your email address."},"cart_account_created":{"title":"Cart, Create Account, Account Created Success Text","value":"Your account has been created, all orders will now be associated with your new account."},"cart_account_free_order":{"title":"Cart, Order Type, Gift Card, Free Order","value":"Free Order"},"store_added_to_cart":{"title":"Store, Added Item to Cart","value":"You have successfully added [prod_title] to your cart."},"activation_success":{"title":"Account, Activation Success","value":"You have successfully activated your account"},"validation_required":{"title":"Account, Validation Required","value":"You have successfully created an account, but your email needs to be validated. An email has been sent to your account with instructions on how to complete the registration process. Contact us if you have any questions."},"subscription_updated":{"title":"Account, Subscription Updated","value":"Your subscription has been updated successfully."},"subscription_canceled":{"title":"Account, Subscription Canceled","value":"Your subscription has been canceled."},"inquiry_sent":{"title":"Store, Inquiry Sent","value":"Your product inquiry has been sent successfully."},"add_to_cart_success":{"title":"Store, Added to Cart","value":"Successfully Added to your Shopping Cart"},"adding_to_cart":{"title":"Store, Adding to Cart","value":"Adding to Cart..."}}},"ec_shipping_email":{"label":"Shipping Emailer","options":{"shipping_email_title":{"title":"Shipping Email Title","value":"Shipping Confirmation - Order Number"},"shipping_dear":{"title":"Shipping Dear Customer","value":"Dear"},"shipping_subtitle1":{"title":"Order Shipped First Half","value":"Your recent order  with the number"},"shipping_subtitle2":{"title":"Order Shipped Second Half","value":"has been shipped! You should be receiving it within a short time period."},"shipping_description":{"title":"Order Shipped Description","value":"You may check the status of your order by visiting your carrier''s website and using the following tracking number."},"shipping_carrier":{"title":"Order Shipped Carrier","value":"Package Carrier:"},"shipping_tracking":{"title":"Order Shipped Tracking Number","value":"Package Tracking Number:"},"shipping_billing_label":{"title":"Billing Label","value":"Billing Address"},"shipping_shipping_label":{"title":"Shipping Label","value":"Shipping Address"},"shipping_product":{"title":"Product Label","value":"Product"},"shipping_quantity":{"title":"Quantity Label","value":"Qty"},"shipping_unit_price":{"title":"Unit Price Label","value":"Unit Price"},"shipping_total_price":{"title":"Total Price Label","value":"Ext Price"},"shipping_final_note1":{"title":"Shipping Emailer Final Note Line 1","value":"Please double check your order when you receive it and let us know immediately if there are any concerns or issues. We always value your business and hope you enjoy your product."},"shipping_final_note2":{"title":"Shipping Emailer Final Note Line 2","value":"Thank you very much!"}}},"ec_login_widget":{"label":"Login Widget","options":{"hello_text":{"title":"Hello","value":"Hello"},"dashboard_text":{"title":"Dashboard","value":"Dashboard"},"order_history_text":{"title":"Order History","value":"Order History"},"billing_info_text":{"title":"Billing Information","value":"Billing Information"},"shipping_info_text":{"title":"Shipping Information","value":"Shipping Information"},"change_password_text":{"title":"Change Password","value":"Change Password"},"sign_out_text":{"title":"Sign Out","value":"Sign Out"}}},"ec_minicart_widget":{"label":"Minicart Widget","options":{"subtotal_text":{"title":"Minicart Widget, Subtotal Text","value":"SUBTOTAL"},"checkout_button_text":{"title":"Minicart Widget, Checkout Button Text","value":"CHECKOUT"}}},"ec_pricepoint_widget":{"label":"Price Point Widget","options":{"less_than":{"title":"Less Than Text","value":"Less Than"},"greater_than":{"title":"Greater Than Text","value":"Greater Than"}}},"ec_newsletter_popup":{"label":"Newsletter Popup","options":{"signup_form_title":{"title":"Signup Form Title","value":"Newsletter Signup"},"signup_form_subtitle":{"title":"Signup Form Subtitle","value":"Sign up now and never miss a thing!"},"signup_form_email_placeholder":{"title":"Signup Form Email Placeholder","value":"email address"},"signup_form_name_placeholder":{"title":"Signup Form Name Placeholder","value":"your name"},"signup_form_button_text":{"title":"Signup Form Button","value":"SUBMIT"},"signup_form_success_title":{"title":"Signup Success Title","value":"You''re Signed Up!"},"signup_form_success_subtitle":{"title":"Signup Success Subtitle","value":"Now that you are signed up, we will send you exclusive offers periodically."}}},"ec_abandoned_cart_email":{"label":"Abandoned Cart Email","options":{"email_title":{"title":"Email Title","value":"You Left Items in Your Cart"},"something_in_cart":{"title":"Main Title, Something in Cart","value":"THERE''S SOMETHING IN YOUR CART."},"complete_question":{"title":"Informational Text","value":"Would you like to complete your purchase?"},"complete_checkout":{"title":"Complete Checkout Button","value":"Complete Checkout"}}},"language_code":{"label":"Language Code","options":{"code":{"title":"Do Not Change","value":"EN"}}}}}}', 'yes');
INSERT INTO `ops_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(593, 'ec_option_use_seperate_language_forms', '1', 'yes'),
(594, 'ec_option_base_theme', '0', 'yes'),
(595, 'ec_option_base_layout', '0', 'yes'),
(596, 'ec_option_latest_theme', 'base-responsive-v3', 'yes'),
(597, 'ec_option_latest_layout', 'base-responsive-v3', 'yes'),
(598, 'ec_option_caching_on', '1', 'yes'),
(599, 'ec_option_cache_update_period', '2', 'yes'),
(600, 'ec_option_cached_date', '0', 'yes'),
(601, 'ec_option_custom_css', '', 'yes'),
(602, 'ec_option_css_replacements', 'main_color=#242424,second_color=#6b6b6b,third_color=#adadad,title_color=#0f0f0f,text_color=#141414,link_color=#242424,link_hover_color=#121212,sale_color=#900,backdrop_color=#333,content_bg=#FFF,error_text=#900,error_color=#F1D9D9,error_color2=#FF0606,success_text=#333,success_color=#E6FFE6,success_color2=#6FFF47', 'yes'),
(603, 'ec_option_font_replacements', 'title_font=Arial, Helvetica, sans-serif:::subtitle_font=Arial, Helvetica, sans-serif:::content_font=Arial, Helvetica, sans-serif', 'yes'),
(604, 'ec_option_responsive_sizes', 'size_level1_high=479:::size_level2_low=480:::size_level2_high=767:::size_level3_low=768:::size_level3_high=960:::size_level4_low=961:::size_level4_high=1300:::size_level5_low=1301', 'yes'),
(605, 'ec_option_details_main_color', '#222222', 'yes'),
(606, 'ec_option_details_second_color', '#666666', 'yes'),
(607, 'ec_option_use_dark_bg', '0', 'yes'),
(608, 'ec_option_default_product_type', '1', 'yes'),
(609, 'ec_option_default_product_image_hover_type', '3', 'yes'),
(610, 'ec_option_default_product_image_effect_type', 'none', 'yes'),
(611, 'ec_option_default_quick_view', '0', 'yes'),
(612, 'ec_option_default_desktop_columns', '3', 'yes'),
(613, 'ec_option_default_desktop_image_height', '310px', 'yes'),
(614, 'ec_option_default_laptop_columns', '3', 'yes'),
(615, 'ec_option_default_laptop_image_height', '310px', 'yes'),
(616, 'ec_option_default_tablet_wide_columns', '2', 'yes'),
(617, 'ec_option_default_tablet_wide_image_height', '310px', 'yes'),
(618, 'ec_option_default_tablet_columns', '2', 'yes'),
(619, 'ec_option_default_tablet_image_height', '380px', 'yes'),
(620, 'ec_option_default_smartphone_columns', '1', 'yes'),
(621, 'ec_option_default_smartphone_image_height', '270px', 'yes'),
(622, 'ec_option_details_columns_desktop', '2', 'yes'),
(623, 'ec_option_details_columns_laptop', '2', 'yes'),
(624, 'ec_option_details_columns_tablet_wide', '1', 'yes'),
(625, 'ec_option_details_columns_tablet', '1', 'yes'),
(626, 'ec_option_details_columns_smartphone', '1', 'yes'),
(627, 'ec_option_cart_columns_desktop', '2', 'yes'),
(628, 'ec_option_cart_columns_laptop', '2', 'yes'),
(629, 'ec_option_cart_columns_tablet_wide', '1', 'yes'),
(630, 'ec_option_cart_columns_tablet', '1', 'yes'),
(631, 'ec_option_cart_columns_smartphone', '1', 'yes'),
(632, 'ec_option_email_logo', '', 'yes'),
(633, 'ec_option_use_facebook_icon', '1', 'yes'),
(634, 'ec_option_use_twitter_icon', '1', 'yes'),
(635, 'ec_option_use_delicious_icon', '1', 'yes'),
(636, 'ec_option_use_myspace_icon', '1', 'yes'),
(637, 'ec_option_use_linkedin_icon', '1', 'yes'),
(638, 'ec_option_use_email_icon', '1', 'yes'),
(639, 'ec_option_use_digg_icon', '1', 'yes'),
(640, 'ec_option_use_googleplus_icon', '1', 'yes'),
(641, 'ec_option_use_pinterest_icon', '1', 'yes'),
(642, 'ec_option_checklist_state', '', 'yes'),
(643, 'ec_option_checklist_currency', '', 'yes'),
(644, 'ec_option_checklist_default_payment', '', 'yes'),
(645, 'ec_option_checklist_guest', '', 'yes'),
(646, 'ec_option_checklist_shipping_enabled', '', 'yes'),
(647, 'ec_option_checklist_checkout_notes', '', 'yes'),
(648, 'ec_option_checklist_billing_registration', '', 'yes'),
(649, 'ec_option_checklist_google_analytics', '', 'yes'),
(650, 'ec_option_checklist_manual_billing', '', 'yes'),
(651, 'ec_option_checklist_third_party_complete', '', 'yes'),
(652, 'ec_option_checklist_third_party', '', 'yes'),
(653, 'ec_option_checklist_has_paypal', '', 'yes'),
(654, 'ec_option_checklist_has_skrill', '', 'yes'),
(655, 'ec_option_checklist_has_paymentexpress_thirdparty', '', 'yes'),
(656, 'ec_option_checklist_has_realex_thirdparty', '', 'yes'),
(657, 'ec_option_checklist_credit_cart_complete', '', 'yes'),
(658, 'ec_option_checklist_credit_card', '', 'yes'),
(659, 'ec_option_checklist_credit_card_location', '', 'yes'),
(660, 'ec_option_checklist_tax_complete', '', 'yes'),
(661, 'ec_option_checklist_tax_choice', '', 'yes'),
(662, 'ec_option_checklist_shipping_complete', '', 'yes'),
(663, 'ec_option_checklist_shipping_choice', '', 'yes'),
(664, 'ec_checklist_shipping_use_ups', '', 'yes'),
(665, 'ec_checklist_shipping_use_usps', '', 'yes'),
(666, 'ec_checklist_shipping_use_fedex', '', 'yes'),
(667, 'ec_checklist_shipping_use_auspost', '', 'yes'),
(668, 'ec_checklist_shipping_use_dhl', '', 'yes'),
(669, 'ec_option_checklist_language_complete', '', 'yes'),
(670, 'ec_option_checklist_theme_complete', '', 'yes'),
(671, 'ec_option_checklist_colorization_complete', '', 'yes'),
(672, 'ec_option_checklist_logo_added_complete', '', 'yes'),
(673, 'ec_option_checklist_admin_embedded_complete', '', 'yes'),
(674, 'ec_option_checklist_admin_consoles_complete', '', 'yes'),
(675, 'ec_option_checklist_page', '', 'yes'),
(676, 'ec_option_quickbooks_user', '', 'yes'),
(677, 'ec_option_quickbooks_password', '', 'yes'),
(678, 'ec_option_wpoptions_version', '4_0_37', 'yes'),
(679, 'ec_option_db_insert_v4', '1', 'yes'),
(680, 'ec_option_db_new_version', '57', 'yes'),
(681, 'ec_option_data_folders_installed', '4_0_37', 'yes'),
(682, 'widget_ec_categorywidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(683, 'widget_ec_cartwidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(684, 'widget_ec_colorwidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(685, 'widget_ec_currencywidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(686, 'widget_ec_donationwidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(687, 'widget_ec_groupwidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(688, 'widget_ec_languagewidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(689, 'widget_ec_loginwidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(690, 'widget_ec_manufacturerwidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(691, 'widget_ec_menuwidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(692, 'widget_ec_newsletterwidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(693, 'widget_ec_pricepointwidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(694, 'widget_ec_productwidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(695, 'widget_ec_searchwidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(696, 'widget_ec_specialswidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(697, 'ec_option_v3_fix', '1', 'yes'),
(698, 'ec_option_published_check', '4_0_37', 'yes'),
(700, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:"auto_add";a:0:{}}', 'yes'),
(701, '_site_transient_timeout_theme_roots', '1525101420', 'no'),
(702, '_site_transient_theme_roots', 'a:4:{s:12:"ozproduction";s:7:"/themes";s:13:"twentyfifteen";s:7:"/themes";s:15:"twentyseventeen";s:7:"/themes";s:13:"twentysixteen";s:7:"/themes";}', 'no'),
(703, '_site_transient_timeout_available_translations', '1525110980', 'no'),
(704, '_site_transient_available_translations', 'a:113:{s:2:"af";a:8:{s:8:"language";s:2:"af";s:7:"version";s:5:"4.9.4";s:7:"updated";s:19:"2018-02-06 13:56:09";s:12:"english_name";s:9:"Afrikaans";s:11:"native_name";s:9:"Afrikaans";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.9.4/af.zip";s:3:"iso";a:2:{i:1;s:2:"af";i:2;s:3:"afr";}s:7:"strings";a:1:{s:8:"continue";s:10:"Gaan voort";}}s:2:"ar";a:8:{s:8:"language";s:2:"ar";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-03-24 19:38:49";s:12:"english_name";s:6:"Arabic";s:11:"native_name";s:14:"العربية";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.9.5/ar.zip";s:3:"iso";a:2:{i:1;s:2:"ar";i:2;s:3:"ara";}s:7:"strings";a:1:{s:8:"continue";s:16:"المتابعة";}}s:3:"ary";a:8:{s:8:"language";s:3:"ary";s:7:"version";s:5:"4.7.7";s:7:"updated";s:19:"2017-01-26 15:42:35";s:12:"english_name";s:15:"Moroccan Arabic";s:11:"native_name";s:31:"العربية المغربية";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.7.7/ary.zip";s:3:"iso";a:2:{i:1;s:2:"ar";i:3;s:3:"ary";}s:7:"strings";a:1:{s:8:"continue";s:16:"المتابعة";}}s:2:"as";a:8:{s:8:"language";s:2:"as";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-11-22 18:59:07";s:12:"english_name";s:8:"Assamese";s:11:"native_name";s:21:"অসমীয়া";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/as.zip";s:3:"iso";a:3:{i:1;s:2:"as";i:2;s:3:"asm";i:3;s:3:"asm";}s:7:"strings";a:1:{s:8:"continue";s:0:"";}}s:2:"az";a:8:{s:8:"language";s:2:"az";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-11-06 00:09:27";s:12:"english_name";s:11:"Azerbaijani";s:11:"native_name";s:16:"Azərbaycan dili";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/az.zip";s:3:"iso";a:2:{i:1;s:2:"az";i:2;s:3:"aze";}s:7:"strings";a:1:{s:8:"continue";s:5:"Davam";}}s:3:"azb";a:8:{s:8:"language";s:3:"azb";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-09-12 20:34:31";s:12:"english_name";s:17:"South Azerbaijani";s:11:"native_name";s:29:"گؤنئی آذربایجان";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.7.2/azb.zip";s:3:"iso";a:2:{i:1;s:2:"az";i:3;s:3:"azb";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:3:"bel";a:8:{s:8:"language";s:3:"bel";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-04-04 08:43:29";s:12:"english_name";s:10:"Belarusian";s:11:"native_name";s:29:"Беларуская мова";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.9.5/bel.zip";s:3:"iso";a:2:{i:1;s:2:"be";i:2;s:3:"bel";}s:7:"strings";a:1:{s:8:"continue";s:20:"Працягнуць";}}s:5:"bg_BG";a:8:{s:8:"language";s:5:"bg_BG";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-04-25 15:24:22";s:12:"english_name";s:9:"Bulgarian";s:11:"native_name";s:18:"Български";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.5/bg_BG.zip";s:3:"iso";a:2:{i:1;s:2:"bg";i:2;s:3:"bul";}s:7:"strings";a:1:{s:8:"continue";s:12:"Напред";}}s:5:"bn_BD";a:8:{s:8:"language";s:5:"bn_BD";s:7:"version";s:5:"4.8.6";s:7:"updated";s:19:"2017-10-01 12:57:10";s:12:"english_name";s:7:"Bengali";s:11:"native_name";s:15:"বাংলা";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.8.6/bn_BD.zip";s:3:"iso";a:1:{i:1;s:2:"bn";}s:7:"strings";a:1:{s:8:"continue";s:23:"এগিয়ে চল.";}}s:2:"bo";a:8:{s:8:"language";s:2:"bo";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-03-22 03:44:52";s:12:"english_name";s:7:"Tibetan";s:11:"native_name";s:21:"བོད་ཡིག";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.9.5/bo.zip";s:3:"iso";a:2:{i:1;s:2:"bo";i:2;s:3:"tib";}s:7:"strings";a:1:{s:8:"continue";s:24:"མུ་མཐུད།";}}s:5:"bs_BA";a:8:{s:8:"language";s:5:"bs_BA";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-09-04 20:20:28";s:12:"english_name";s:7:"Bosnian";s:11:"native_name";s:8:"Bosanski";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/bs_BA.zip";s:3:"iso";a:2:{i:1;s:2:"bs";i:2;s:3:"bos";}s:7:"strings";a:1:{s:8:"continue";s:7:"Nastavi";}}s:2:"ca";a:8:{s:8:"language";s:2:"ca";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-04-16 18:12:49";s:12:"english_name";s:7:"Catalan";s:11:"native_name";s:7:"Català";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.9.5/ca.zip";s:3:"iso";a:2:{i:1;s:2:"ca";i:2;s:3:"cat";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continua";}}s:3:"ceb";a:8:{s:8:"language";s:3:"ceb";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-03-02 17:25:51";s:12:"english_name";s:7:"Cebuano";s:11:"native_name";s:7:"Cebuano";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.7.2/ceb.zip";s:3:"iso";a:2:{i:2;s:3:"ceb";i:3;s:3:"ceb";}s:7:"strings";a:1:{s:8:"continue";s:7:"Padayun";}}s:5:"cs_CZ";a:8:{s:8:"language";s:5:"cs_CZ";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-04-30 07:04:50";s:12:"english_name";s:5:"Czech";s:11:"native_name";s:9:"Čeština";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.5/cs_CZ.zip";s:3:"iso";a:2:{i:1;s:2:"cs";i:2;s:3:"ces";}s:7:"strings";a:1:{s:8:"continue";s:11:"Pokračovat";}}s:2:"cy";a:8:{s:8:"language";s:2:"cy";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-03-11 09:40:36";s:12:"english_name";s:5:"Welsh";s:11:"native_name";s:7:"Cymraeg";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.9.5/cy.zip";s:3:"iso";a:2:{i:1;s:2:"cy";i:2;s:3:"cym";}s:7:"strings";a:1:{s:8:"continue";s:6:"Parhau";}}s:5:"da_DK";a:8:{s:8:"language";s:5:"da_DK";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-03-19 17:34:31";s:12:"english_name";s:6:"Danish";s:11:"native_name";s:5:"Dansk";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.5/da_DK.zip";s:3:"iso";a:2:{i:1;s:2:"da";i:2;s:3:"dan";}s:7:"strings";a:1:{s:8:"continue";s:8:"Fortsæt";}}s:12:"de_DE_formal";a:8:{s:8:"language";s:12:"de_DE_formal";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-04-30 07:11:01";s:12:"english_name";s:15:"German (Formal)";s:11:"native_name";s:13:"Deutsch (Sie)";s:7:"package";s:71:"https://downloads.wordpress.org/translation/core/4.9.5/de_DE_formal.zip";s:3:"iso";a:1:{i:1;s:2:"de";}s:7:"strings";a:1:{s:8:"continue";s:6:"Weiter";}}s:5:"de_DE";a:8:{s:8:"language";s:5:"de_DE";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-04-30 07:12:45";s:12:"english_name";s:6:"German";s:11:"native_name";s:7:"Deutsch";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.5/de_DE.zip";s:3:"iso";a:1:{i:1;s:2:"de";}s:7:"strings";a:1:{s:8:"continue";s:6:"Weiter";}}s:14:"de_CH_informal";a:8:{s:8:"language";s:14:"de_CH_informal";s:7:"version";s:5:"4.9.2";s:7:"updated";s:19:"2017-11-22 15:43:53";s:12:"english_name";s:30:"German (Switzerland, Informal)";s:11:"native_name";s:21:"Deutsch (Schweiz, Du)";s:7:"package";s:73:"https://downloads.wordpress.org/translation/core/4.9.2/de_CH_informal.zip";s:3:"iso";a:1:{i:1;s:2:"de";}s:7:"strings";a:1:{s:8:"continue";s:6:"Weiter";}}s:5:"de_CH";a:8:{s:8:"language";s:5:"de_CH";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-02-12 10:10:36";s:12:"english_name";s:20:"German (Switzerland)";s:11:"native_name";s:17:"Deutsch (Schweiz)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.5/de_CH.zip";s:3:"iso";a:1:{i:1;s:2:"de";}s:7:"strings";a:1:{s:8:"continue";s:6:"Weiter";}}s:3:"dzo";a:8:{s:8:"language";s:3:"dzo";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-06-29 08:59:03";s:12:"english_name";s:8:"Dzongkha";s:11:"native_name";s:18:"རྫོང་ཁ";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.7.2/dzo.zip";s:3:"iso";a:2:{i:1;s:2:"dz";i:2;s:3:"dzo";}s:7:"strings";a:1:{s:8:"continue";s:0:"";}}s:2:"el";a:8:{s:8:"language";s:2:"el";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-04-05 12:41:56";s:12:"english_name";s:5:"Greek";s:11:"native_name";s:16:"Ελληνικά";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.9.5/el.zip";s:3:"iso";a:2:{i:1;s:2:"el";i:2;s:3:"ell";}s:7:"strings";a:1:{s:8:"continue";s:16:"Συνέχεια";}}s:5:"en_CA";a:8:{s:8:"language";s:5:"en_CA";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-03-29 16:28:34";s:12:"english_name";s:16:"English (Canada)";s:11:"native_name";s:16:"English (Canada)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.5/en_CA.zip";s:3:"iso";a:3:{i:1;s:2:"en";i:2;s:3:"eng";i:3;s:3:"eng";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:5:"en_NZ";a:8:{s:8:"language";s:5:"en_NZ";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-03-15 20:17:27";s:12:"english_name";s:21:"English (New Zealand)";s:11:"native_name";s:21:"English (New Zealand)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.5/en_NZ.zip";s:3:"iso";a:3:{i:1;s:2:"en";i:2;s:3:"eng";i:3;s:3:"eng";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:5:"en_AU";a:8:{s:8:"language";s:5:"en_AU";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-04-30 07:39:22";s:12:"english_name";s:19:"English (Australia)";s:11:"native_name";s:19:"English (Australia)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.5/en_AU.zip";s:3:"iso";a:3:{i:1;s:2:"en";i:2;s:3:"eng";i:3;s:3:"eng";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:5:"en_ZA";a:8:{s:8:"language";s:5:"en_ZA";s:7:"version";s:5:"4.9.2";s:7:"updated";s:19:"2017-11-15 09:54:30";s:12:"english_name";s:22:"English (South Africa)";s:11:"native_name";s:22:"English (South Africa)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.2/en_ZA.zip";s:3:"iso";a:3:{i:1;s:2:"en";i:2;s:3:"eng";i:3;s:3:"eng";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:5:"en_GB";a:8:{s:8:"language";s:5:"en_GB";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-04-30 07:40:51";s:12:"english_name";s:12:"English (UK)";s:11:"native_name";s:12:"English (UK)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.5/en_GB.zip";s:3:"iso";a:3:{i:1;s:2:"en";i:2;s:3:"eng";i:3;s:3:"eng";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:2:"eo";a:8:{s:8:"language";s:2:"eo";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-04-25 16:53:32";s:12:"english_name";s:9:"Esperanto";s:11:"native_name";s:9:"Esperanto";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.9.5/eo.zip";s:3:"iso";a:2:{i:1;s:2:"eo";i:2;s:3:"epo";}s:7:"strings";a:1:{s:8:"continue";s:8:"Daŭrigi";}}s:5:"es_AR";a:8:{s:8:"language";s:5:"es_AR";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-02-27 05:22:44";s:12:"english_name";s:19:"Spanish (Argentina)";s:11:"native_name";s:21:"Español de Argentina";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.5/es_AR.zip";s:3:"iso";a:3:{i:1;s:2:"es";i:2;s:3:"spa";i:3;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_ES";a:8:{s:8:"language";s:5:"es_ES";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-04-30 07:59:33";s:12:"english_name";s:15:"Spanish (Spain)";s:11:"native_name";s:8:"Español";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.5/es_ES.zip";s:3:"iso";a:3:{i:1;s:2:"es";i:2;s:3:"spa";i:3;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_VE";a:8:{s:8:"language";s:5:"es_VE";s:7:"version";s:5:"4.9.4";s:7:"updated";s:19:"2018-02-23 18:34:33";s:12:"english_name";s:19:"Spanish (Venezuela)";s:11:"native_name";s:21:"Español de Venezuela";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.4/es_VE.zip";s:3:"iso";a:3:{i:1;s:2:"es";i:2;s:3:"spa";i:3;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_CO";a:8:{s:8:"language";s:5:"es_CO";s:7:"version";s:5:"4.9.2";s:7:"updated";s:19:"2017-11-15 23:17:08";s:12:"english_name";s:18:"Spanish (Colombia)";s:11:"native_name";s:20:"Español de Colombia";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.2/es_CO.zip";s:3:"iso";a:3:{i:1;s:2:"es";i:2;s:3:"spa";i:3;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_GT";a:8:{s:8:"language";s:5:"es_GT";s:7:"version";s:5:"4.9.2";s:7:"updated";s:19:"2017-11-15 15:03:42";s:12:"english_name";s:19:"Spanish (Guatemala)";s:11:"native_name";s:21:"Español de Guatemala";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.2/es_GT.zip";s:3:"iso";a:3:{i:1;s:2:"es";i:2;s:3:"spa";i:3;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_MX";a:8:{s:8:"language";s:5:"es_MX";s:7:"version";s:5:"4.8.6";s:7:"updated";s:19:"2017-07-31 15:12:02";s:12:"english_name";s:16:"Spanish (Mexico)";s:11:"native_name";s:19:"Español de México";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.8.6/es_MX.zip";s:3:"iso";a:3:{i:1;s:2:"es";i:2;s:3:"spa";i:3;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_CR";a:8:{s:8:"language";s:5:"es_CR";s:7:"version";s:5:"4.8.3";s:7:"updated";s:19:"2017-10-01 17:54:52";s:12:"english_name";s:20:"Spanish (Costa Rica)";s:11:"native_name";s:22:"Español de Costa Rica";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.8.3/es_CR.zip";s:3:"iso";a:3:{i:1;s:2:"es";i:2;s:3:"spa";i:3;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_PE";a:8:{s:8:"language";s:5:"es_PE";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-09-09 09:36:22";s:12:"english_name";s:14:"Spanish (Peru)";s:11:"native_name";s:17:"Español de Perú";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/es_PE.zip";s:3:"iso";a:3:{i:1;s:2:"es";i:2;s:3:"spa";i:3;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_CL";a:8:{s:8:"language";s:5:"es_CL";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-11-28 20:09:49";s:12:"english_name";s:15:"Spanish (Chile)";s:11:"native_name";s:17:"Español de Chile";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/es_CL.zip";s:3:"iso";a:3:{i:1;s:2:"es";i:2;s:3:"spa";i:3;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:2:"et";a:8:{s:8:"language";s:2:"et";s:7:"version";s:5:"4.9.2";s:7:"updated";s:19:"2017-11-19 14:11:29";s:12:"english_name";s:8:"Estonian";s:11:"native_name";s:5:"Eesti";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.9.2/et.zip";s:3:"iso";a:2:{i:1;s:2:"et";i:2;s:3:"est";}s:7:"strings";a:1:{s:8:"continue";s:6:"Jätka";}}s:2:"eu";a:8:{s:8:"language";s:2:"eu";s:7:"version";s:5:"4.9.2";s:7:"updated";s:19:"2017-12-09 21:12:23";s:12:"english_name";s:6:"Basque";s:11:"native_name";s:7:"Euskara";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.9.2/eu.zip";s:3:"iso";a:2:{i:1;s:2:"eu";i:2;s:3:"eus";}s:7:"strings";a:1:{s:8:"continue";s:8:"Jarraitu";}}s:5:"fa_IR";a:8:{s:8:"language";s:5:"fa_IR";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-03-30 07:44:25";s:12:"english_name";s:7:"Persian";s:11:"native_name";s:10:"فارسی";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.5/fa_IR.zip";s:3:"iso";a:2:{i:1;s:2:"fa";i:2;s:3:"fas";}s:7:"strings";a:1:{s:8:"continue";s:10:"ادامه";}}s:2:"fi";a:8:{s:8:"language";s:2:"fi";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-04-20 11:27:31";s:12:"english_name";s:7:"Finnish";s:11:"native_name";s:5:"Suomi";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.9.5/fi.zip";s:3:"iso";a:2:{i:1;s:2:"fi";i:2;s:3:"fin";}s:7:"strings";a:1:{s:8:"continue";s:5:"Jatka";}}s:5:"fr_BE";a:8:{s:8:"language";s:5:"fr_BE";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-01-31 11:16:06";s:12:"english_name";s:16:"French (Belgium)";s:11:"native_name";s:21:"Français de Belgique";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.5/fr_BE.zip";s:3:"iso";a:2:{i:1;s:2:"fr";i:2;s:3:"fra";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuer";}}s:5:"fr_FR";a:8:{s:8:"language";s:5:"fr_FR";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-04-30 07:13:07";s:12:"english_name";s:15:"French (France)";s:11:"native_name";s:9:"Français";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.5/fr_FR.zip";s:3:"iso";a:1:{i:1;s:2:"fr";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuer";}}s:5:"fr_CA";a:8:{s:8:"language";s:5:"fr_CA";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-04-30 02:03:31";s:12:"english_name";s:15:"French (Canada)";s:11:"native_name";s:19:"Français du Canada";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.5/fr_CA.zip";s:3:"iso";a:2:{i:1;s:2:"fr";i:2;s:3:"fra";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuer";}}s:3:"fur";a:8:{s:8:"language";s:3:"fur";s:7:"version";s:5:"4.8.6";s:7:"updated";s:19:"2018-01-29 17:32:35";s:12:"english_name";s:8:"Friulian";s:11:"native_name";s:8:"Friulian";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.8.6/fur.zip";s:3:"iso";a:2:{i:2;s:3:"fur";i:3;s:3:"fur";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:2:"gd";a:8:{s:8:"language";s:2:"gd";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-08-23 17:41:37";s:12:"english_name";s:15:"Scottish Gaelic";s:11:"native_name";s:9:"Gàidhlig";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/gd.zip";s:3:"iso";a:3:{i:1;s:2:"gd";i:2;s:3:"gla";i:3;s:3:"gla";}s:7:"strings";a:1:{s:8:"continue";s:15:"Lean air adhart";}}s:5:"gl_ES";a:8:{s:8:"language";s:5:"gl_ES";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-03-10 18:19:59";s:12:"english_name";s:8:"Galician";s:11:"native_name";s:6:"Galego";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.5/gl_ES.zip";s:3:"iso";a:2:{i:1;s:2:"gl";i:2;s:3:"glg";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:2:"gu";a:8:{s:8:"language";s:2:"gu";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-02-14 06:16:04";s:12:"english_name";s:8:"Gujarati";s:11:"native_name";s:21:"ગુજરાતી";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.9.5/gu.zip";s:3:"iso";a:2:{i:1;s:2:"gu";i:2;s:3:"guj";}s:7:"strings";a:1:{s:8:"continue";s:31:"ચાલુ રાખવું";}}s:3:"haz";a:8:{s:8:"language";s:3:"haz";s:7:"version";s:5:"4.4.2";s:7:"updated";s:19:"2015-12-05 00:59:09";s:12:"english_name";s:8:"Hazaragi";s:11:"native_name";s:15:"هزاره گی";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.4.2/haz.zip";s:3:"iso";a:1:{i:3;s:3:"haz";}s:7:"strings";a:1:{s:8:"continue";s:10:"ادامه";}}s:5:"he_IL";a:8:{s:8:"language";s:5:"he_IL";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-04-15 08:49:46";s:12:"english_name";s:6:"Hebrew";s:11:"native_name";s:16:"עִבְרִית";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.5/he_IL.zip";s:3:"iso";a:1:{i:1;s:2:"he";}s:7:"strings";a:1:{s:8:"continue";s:8:"המשך";}}s:5:"hi_IN";a:8:{s:8:"language";s:5:"hi_IN";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-03-31 18:09:34";s:12:"english_name";s:5:"Hindi";s:11:"native_name";s:18:"हिन्दी";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.5/hi_IN.zip";s:3:"iso";a:2:{i:1;s:2:"hi";i:2;s:3:"hin";}s:7:"strings";a:1:{s:8:"continue";s:12:"जारी";}}s:2:"hr";a:8:{s:8:"language";s:2:"hr";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-03-26 21:01:10";s:12:"english_name";s:8:"Croatian";s:11:"native_name";s:8:"Hrvatski";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.9.5/hr.zip";s:3:"iso";a:2:{i:1;s:2:"hr";i:2;s:3:"hrv";}s:7:"strings";a:1:{s:8:"continue";s:7:"Nastavi";}}s:5:"hu_HU";a:8:{s:8:"language";s:5:"hu_HU";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-04-02 12:51:15";s:12:"english_name";s:9:"Hungarian";s:11:"native_name";s:6:"Magyar";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.5/hu_HU.zip";s:3:"iso";a:2:{i:1;s:2:"hu";i:2;s:3:"hun";}s:7:"strings";a:1:{s:8:"continue";s:10:"Folytatás";}}s:2:"hy";a:8:{s:8:"language";s:2:"hy";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-12-03 16:21:10";s:12:"english_name";s:8:"Armenian";s:11:"native_name";s:14:"Հայերեն";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/hy.zip";s:3:"iso";a:2:{i:1;s:2:"hy";i:2;s:3:"hye";}s:7:"strings";a:1:{s:8:"continue";s:20:"Շարունակել";}}s:5:"id_ID";a:8:{s:8:"language";s:5:"id_ID";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-04-13 11:16:25";s:12:"english_name";s:10:"Indonesian";s:11:"native_name";s:16:"Bahasa Indonesia";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.5/id_ID.zip";s:3:"iso";a:2:{i:1;s:2:"id";i:2;s:3:"ind";}s:7:"strings";a:1:{s:8:"continue";s:9:"Lanjutkan";}}s:5:"is_IS";a:8:{s:8:"language";s:5:"is_IS";s:7:"version";s:5:"4.7.7";s:7:"updated";s:19:"2017-04-13 13:55:54";s:12:"english_name";s:9:"Icelandic";s:11:"native_name";s:9:"Íslenska";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.7/is_IS.zip";s:3:"iso";a:2:{i:1;s:2:"is";i:2;s:3:"isl";}s:7:"strings";a:1:{s:8:"continue";s:6:"Áfram";}}s:5:"it_IT";a:8:{s:8:"language";s:5:"it_IT";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-04-27 09:48:26";s:12:"english_name";s:7:"Italian";s:11:"native_name";s:8:"Italiano";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.5/it_IT.zip";s:3:"iso";a:2:{i:1;s:2:"it";i:2;s:3:"ita";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continua";}}s:2:"ja";a:8:{s:8:"language";s:2:"ja";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-03-16 18:46:39";s:12:"english_name";s:8:"Japanese";s:11:"native_name";s:9:"日本語";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.9.5/ja.zip";s:3:"iso";a:1:{i:1;s:2:"ja";}s:7:"strings";a:1:{s:8:"continue";s:9:"続ける";}}s:5:"jv_ID";a:8:{s:8:"language";s:5:"jv_ID";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-03-24 13:53:29";s:12:"english_name";s:8:"Javanese";s:11:"native_name";s:9:"Basa Jawa";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.5/jv_ID.zip";s:3:"iso";a:2:{i:1;s:2:"jv";i:2;s:3:"jav";}s:7:"strings";a:1:{s:8:"continue";s:9:"Nerusaké";}}s:5:"ka_GE";a:8:{s:8:"language";s:5:"ka_GE";s:7:"version";s:5:"4.9.4";s:7:"updated";s:19:"2018-02-08 06:01:48";s:12:"english_name";s:8:"Georgian";s:11:"native_name";s:21:"ქართული";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.4/ka_GE.zip";s:3:"iso";a:2:{i:1;s:2:"ka";i:2;s:3:"kat";}s:7:"strings";a:1:{s:8:"continue";s:30:"გაგრძელება";}}s:3:"kab";a:8:{s:8:"language";s:3:"kab";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-03-22 22:24:38";s:12:"english_name";s:6:"Kabyle";s:11:"native_name";s:9:"Taqbaylit";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.9.5/kab.zip";s:3:"iso";a:2:{i:2;s:3:"kab";i:3;s:3:"kab";}s:7:"strings";a:1:{s:8:"continue";s:6:"Kemmel";}}s:2:"kk";a:8:{s:8:"language";s:2:"kk";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-03-12 08:08:32";s:12:"english_name";s:6:"Kazakh";s:11:"native_name";s:19:"Қазақ тілі";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.9.5/kk.zip";s:3:"iso";a:2:{i:1;s:2:"kk";i:2;s:3:"kaz";}s:7:"strings";a:1:{s:8:"continue";s:20:"Жалғастыру";}}s:2:"km";a:8:{s:8:"language";s:2:"km";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-12-07 02:07:59";s:12:"english_name";s:5:"Khmer";s:11:"native_name";s:27:"ភាសាខ្មែរ";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/km.zip";s:3:"iso";a:2:{i:1;s:2:"km";i:2;s:3:"khm";}s:7:"strings";a:1:{s:8:"continue";s:12:"បន្ត";}}s:5:"ko_KR";a:8:{s:8:"language";s:5:"ko_KR";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-03-15 02:27:09";s:12:"english_name";s:6:"Korean";s:11:"native_name";s:9:"한국어";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.5/ko_KR.zip";s:3:"iso";a:2:{i:1;s:2:"ko";i:2;s:3:"kor";}s:7:"strings";a:1:{s:8:"continue";s:6:"계속";}}s:3:"ckb";a:8:{s:8:"language";s:3:"ckb";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:48:25";s:12:"english_name";s:16:"Kurdish (Sorani)";s:11:"native_name";s:13:"كوردی‎";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.7.2/ckb.zip";s:3:"iso";a:2:{i:1;s:2:"ku";i:3;s:3:"ckb";}s:7:"strings";a:1:{s:8:"continue";s:30:"به‌رده‌وام به‌";}}s:2:"lo";a:8:{s:8:"language";s:2:"lo";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-11-12 09:59:23";s:12:"english_name";s:3:"Lao";s:11:"native_name";s:21:"ພາສາລາວ";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/lo.zip";s:3:"iso";a:2:{i:1;s:2:"lo";i:2;s:3:"lao";}s:7:"strings";a:1:{s:8:"continue";s:18:"ຕໍ່​ໄປ";}}s:5:"lt_LT";a:8:{s:8:"language";s:5:"lt_LT";s:7:"version";s:5:"4.9.2";s:7:"updated";s:19:"2017-11-15 19:40:23";s:12:"english_name";s:10:"Lithuanian";s:11:"native_name";s:15:"Lietuvių kalba";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.2/lt_LT.zip";s:3:"iso";a:2:{i:1;s:2:"lt";i:2;s:3:"lit";}s:7:"strings";a:1:{s:8:"continue";s:6:"Tęsti";}}s:2:"lv";a:8:{s:8:"language";s:2:"lv";s:7:"version";s:5:"4.7.7";s:7:"updated";s:19:"2017-03-17 20:40:40";s:12:"english_name";s:7:"Latvian";s:11:"native_name";s:16:"Latviešu valoda";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.7/lv.zip";s:3:"iso";a:2:{i:1;s:2:"lv";i:2;s:3:"lav";}s:7:"strings";a:1:{s:8:"continue";s:9:"Turpināt";}}s:5:"mk_MK";a:8:{s:8:"language";s:5:"mk_MK";s:7:"version";s:5:"4.7.7";s:7:"updated";s:19:"2017-01-26 15:54:41";s:12:"english_name";s:10:"Macedonian";s:11:"native_name";s:31:"Македонски јазик";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.7/mk_MK.zip";s:3:"iso";a:2:{i:1;s:2:"mk";i:2;s:3:"mkd";}s:7:"strings";a:1:{s:8:"continue";s:16:"Продолжи";}}s:5:"ml_IN";a:8:{s:8:"language";s:5:"ml_IN";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-27 03:43:32";s:12:"english_name";s:9:"Malayalam";s:11:"native_name";s:18:"മലയാളം";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/ml_IN.zip";s:3:"iso";a:2:{i:1;s:2:"ml";i:2;s:3:"mal";}s:7:"strings";a:1:{s:8:"continue";s:18:"തുടരുക";}}s:2:"mn";a:8:{s:8:"language";s:2:"mn";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-12 07:29:35";s:12:"english_name";s:9:"Mongolian";s:11:"native_name";s:12:"Монгол";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/mn.zip";s:3:"iso";a:2:{i:1;s:2:"mn";i:2;s:3:"mon";}s:7:"strings";a:1:{s:8:"continue";s:24:"Үргэлжлүүлэх";}}s:2:"mr";a:8:{s:8:"language";s:2:"mr";s:7:"version";s:5:"4.8.6";s:7:"updated";s:19:"2018-02-13 07:38:55";s:12:"english_name";s:7:"Marathi";s:11:"native_name";s:15:"मराठी";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.8.6/mr.zip";s:3:"iso";a:2:{i:1;s:2:"mr";i:2;s:3:"mar";}s:7:"strings";a:1:{s:8:"continue";s:25:"सुरु ठेवा";}}s:5:"ms_MY";a:8:{s:8:"language";s:5:"ms_MY";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-02-28 10:55:13";s:12:"english_name";s:5:"Malay";s:11:"native_name";s:13:"Bahasa Melayu";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.5/ms_MY.zip";s:3:"iso";a:2:{i:1;s:2:"ms";i:2;s:3:"msa";}s:7:"strings";a:1:{s:8:"continue";s:8:"Teruskan";}}s:5:"my_MM";a:8:{s:8:"language";s:5:"my_MM";s:7:"version";s:6:"4.1.20";s:7:"updated";s:19:"2015-03-26 15:57:42";s:12:"english_name";s:17:"Myanmar (Burmese)";s:11:"native_name";s:15:"ဗမာစာ";s:7:"package";s:65:"https://downloads.wordpress.org/translation/core/4.1.20/my_MM.zip";s:3:"iso";a:2:{i:1;s:2:"my";i:2;s:3:"mya";}s:7:"strings";a:1:{s:8:"continue";s:54:"ဆက်လက်လုပ်ဆောင်ပါ။";}}s:5:"nb_NO";a:8:{s:8:"language";s:5:"nb_NO";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-04-12 10:26:07";s:12:"english_name";s:19:"Norwegian (Bokmål)";s:11:"native_name";s:13:"Norsk bokmål";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.5/nb_NO.zip";s:3:"iso";a:2:{i:1;s:2:"nb";i:2;s:3:"nob";}s:7:"strings";a:1:{s:8:"continue";s:8:"Fortsett";}}s:5:"ne_NP";a:8:{s:8:"language";s:5:"ne_NP";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-03-27 10:30:26";s:12:"english_name";s:6:"Nepali";s:11:"native_name";s:18:"नेपाली";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.5/ne_NP.zip";s:3:"iso";a:2:{i:1;s:2:"ne";i:2;s:3:"nep";}s:7:"strings";a:1:{s:8:"continue";s:43:"जारी राख्नुहोस्";}}s:12:"nl_NL_formal";a:8:{s:8:"language";s:12:"nl_NL_formal";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-03-26 08:04:00";s:12:"english_name";s:14:"Dutch (Formal)";s:11:"native_name";s:20:"Nederlands (Formeel)";s:7:"package";s:71:"https://downloads.wordpress.org/translation/core/4.9.5/nl_NL_formal.zip";s:3:"iso";a:2:{i:1;s:2:"nl";i:2;s:3:"nld";}s:7:"strings";a:1:{s:8:"continue";s:8:"Doorgaan";}}s:5:"nl_BE";a:8:{s:8:"language";s:5:"nl_BE";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-03-23 12:42:00";s:12:"english_name";s:15:"Dutch (Belgium)";s:11:"native_name";s:20:"Nederlands (België)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.5/nl_BE.zip";s:3:"iso";a:2:{i:1;s:2:"nl";i:2;s:3:"nld";}s:7:"strings";a:1:{s:8:"continue";s:8:"Doorgaan";}}s:5:"nl_NL";a:8:{s:8:"language";s:5:"nl_NL";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-04-24 14:07:15";s:12:"english_name";s:5:"Dutch";s:11:"native_name";s:10:"Nederlands";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.5/nl_NL.zip";s:3:"iso";a:2:{i:1;s:2:"nl";i:2;s:3:"nld";}s:7:"strings";a:1:{s:8:"continue";s:8:"Doorgaan";}}s:5:"nn_NO";a:8:{s:8:"language";s:5:"nn_NO";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-03-22 09:27:50";s:12:"english_name";s:19:"Norwegian (Nynorsk)";s:11:"native_name";s:13:"Norsk nynorsk";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.5/nn_NO.zip";s:3:"iso";a:2:{i:1;s:2:"nn";i:2;s:3:"nno";}s:7:"strings";a:1:{s:8:"continue";s:9:"Hald fram";}}s:3:"oci";a:8:{s:8:"language";s:3:"oci";s:7:"version";s:5:"4.8.3";s:7:"updated";s:19:"2017-08-25 10:03:08";s:12:"english_name";s:7:"Occitan";s:11:"native_name";s:7:"Occitan";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.8.3/oci.zip";s:3:"iso";a:2:{i:1;s:2:"oc";i:2;s:3:"oci";}s:7:"strings";a:1:{s:8:"continue";s:9:"Contunhar";}}s:5:"pa_IN";a:8:{s:8:"language";s:5:"pa_IN";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-16 05:19:43";s:12:"english_name";s:7:"Punjabi";s:11:"native_name";s:18:"ਪੰਜਾਬੀ";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/pa_IN.zip";s:3:"iso";a:2:{i:1;s:2:"pa";i:2;s:3:"pan";}s:7:"strings";a:1:{s:8:"continue";s:25:"ਜਾਰੀ ਰੱਖੋ";}}s:5:"pl_PL";a:8:{s:8:"language";s:5:"pl_PL";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-04-19 19:40:03";s:12:"english_name";s:6:"Polish";s:11:"native_name";s:6:"Polski";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.5/pl_PL.zip";s:3:"iso";a:2:{i:1;s:2:"pl";i:2;s:3:"pol";}s:7:"strings";a:1:{s:8:"continue";s:9:"Kontynuuj";}}s:2:"ps";a:8:{s:8:"language";s:2:"ps";s:7:"version";s:6:"4.1.20";s:7:"updated";s:19:"2015-03-29 22:19:48";s:12:"english_name";s:6:"Pashto";s:11:"native_name";s:8:"پښتو";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.1.20/ps.zip";s:3:"iso";a:2:{i:1;s:2:"ps";i:2;s:3:"pus";}s:7:"strings";a:1:{s:8:"continue";s:19:"دوام ورکړه";}}s:5:"pt_BR";a:8:{s:8:"language";s:5:"pt_BR";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-03-22 18:30:41";s:12:"english_name";s:19:"Portuguese (Brazil)";s:11:"native_name";s:20:"Português do Brasil";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.5/pt_BR.zip";s:3:"iso";a:2:{i:1;s:2:"pt";i:2;s:3:"por";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:10:"pt_PT_ao90";a:8:{s:8:"language";s:10:"pt_PT_ao90";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-03-09 09:30:48";s:12:"english_name";s:27:"Portuguese (Portugal, AO90)";s:11:"native_name";s:17:"Português (AO90)";s:7:"package";s:69:"https://downloads.wordpress.org/translation/core/4.9.5/pt_PT_ao90.zip";s:3:"iso";a:1:{i:1;s:2:"pt";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"pt_PT";a:8:{s:8:"language";s:5:"pt_PT";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-04-27 09:56:39";s:12:"english_name";s:21:"Portuguese (Portugal)";s:11:"native_name";s:10:"Português";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.5/pt_PT.zip";s:3:"iso";a:1:{i:1;s:2:"pt";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:3:"rhg";a:8:{s:8:"language";s:3:"rhg";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-03-16 13:03:18";s:12:"english_name";s:8:"Rohingya";s:11:"native_name";s:8:"Ruáinga";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.7.2/rhg.zip";s:3:"iso";a:1:{i:3;s:3:"rhg";}s:7:"strings";a:1:{s:8:"continue";s:0:"";}}s:5:"ro_RO";a:8:{s:8:"language";s:5:"ro_RO";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-04-30 06:02:55";s:12:"english_name";s:8:"Romanian";s:11:"native_name";s:8:"Română";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.5/ro_RO.zip";s:3:"iso";a:2:{i:1;s:2:"ro";i:2;s:3:"ron";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuă";}}s:5:"ru_RU";a:8:{s:8:"language";s:5:"ru_RU";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-04-30 03:01:28";s:12:"english_name";s:7:"Russian";s:11:"native_name";s:14:"Русский";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.5/ru_RU.zip";s:3:"iso";a:2:{i:1;s:2:"ru";i:2;s:3:"rus";}s:7:"strings";a:1:{s:8:"continue";s:20:"Продолжить";}}s:3:"sah";a:8:{s:8:"language";s:3:"sah";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-21 02:06:41";s:12:"english_name";s:5:"Sakha";s:11:"native_name";s:14:"Сахалыы";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.7.2/sah.zip";s:3:"iso";a:2:{i:2;s:3:"sah";i:3;s:3:"sah";}s:7:"strings";a:1:{s:8:"continue";s:12:"Салҕаа";}}s:5:"si_LK";a:8:{s:8:"language";s:5:"si_LK";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-11-12 06:00:52";s:12:"english_name";s:7:"Sinhala";s:11:"native_name";s:15:"සිංහල";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/si_LK.zip";s:3:"iso";a:2:{i:1;s:2:"si";i:2;s:3:"sin";}s:7:"strings";a:1:{s:8:"continue";s:44:"දිගටම කරගෙන යන්න";}}s:5:"sk_SK";a:8:{s:8:"language";s:5:"sk_SK";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-04-29 09:16:43";s:12:"english_name";s:6:"Slovak";s:11:"native_name";s:11:"Slovenčina";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.5/sk_SK.zip";s:3:"iso";a:2:{i:1;s:2:"sk";i:2;s:3:"slk";}s:7:"strings";a:1:{s:8:"continue";s:12:"Pokračovať";}}s:5:"sl_SI";a:8:{s:8:"language";s:5:"sl_SI";s:7:"version";s:5:"4.9.2";s:7:"updated";s:19:"2018-01-04 13:33:13";s:12:"english_name";s:9:"Slovenian";s:11:"native_name";s:13:"Slovenščina";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.2/sl_SI.zip";s:3:"iso";a:2:{i:1;s:2:"sl";i:2;s:3:"slv";}s:7:"strings";a:1:{s:8:"continue";s:8:"Nadaljuj";}}s:2:"sq";a:8:{s:8:"language";s:2:"sq";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-03-25 10:30:04";s:12:"english_name";s:8:"Albanian";s:11:"native_name";s:5:"Shqip";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.9.5/sq.zip";s:3:"iso";a:2:{i:1;s:2:"sq";i:2;s:3:"sqi";}s:7:"strings";a:1:{s:8:"continue";s:6:"Vazhdo";}}s:5:"sr_RS";a:8:{s:8:"language";s:5:"sr_RS";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-03-25 20:12:50";s:12:"english_name";s:7:"Serbian";s:11:"native_name";s:23:"Српски језик";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.5/sr_RS.zip";s:3:"iso";a:2:{i:1;s:2:"sr";i:2;s:3:"srp";}s:7:"strings";a:1:{s:8:"continue";s:14:"Настави";}}s:5:"sv_SE";a:8:{s:8:"language";s:5:"sv_SE";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-04-30 06:40:27";s:12:"english_name";s:7:"Swedish";s:11:"native_name";s:7:"Svenska";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.5/sv_SE.zip";s:3:"iso";a:2:{i:1;s:2:"sv";i:2;s:3:"swe";}s:7:"strings";a:1:{s:8:"continue";s:9:"Fortsätt";}}s:3:"szl";a:8:{s:8:"language";s:3:"szl";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-09-24 19:58:14";s:12:"english_name";s:8:"Silesian";s:11:"native_name";s:17:"Ślōnskŏ gŏdka";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.7.2/szl.zip";s:3:"iso";a:1:{i:3;s:3:"szl";}s:7:"strings";a:1:{s:8:"continue";s:13:"Kōntynuować";}}s:5:"ta_IN";a:8:{s:8:"language";s:5:"ta_IN";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-27 03:22:47";s:12:"english_name";s:5:"Tamil";s:11:"native_name";s:15:"தமிழ்";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/ta_IN.zip";s:3:"iso";a:2:{i:1;s:2:"ta";i:2;s:3:"tam";}s:7:"strings";a:1:{s:8:"continue";s:24:"தொடரவும்";}}s:2:"te";a:8:{s:8:"language";s:2:"te";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:47:39";s:12:"english_name";s:6:"Telugu";s:11:"native_name";s:18:"తెలుగు";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/te.zip";s:3:"iso";a:2:{i:1;s:2:"te";i:2;s:3:"tel";}s:7:"strings";a:1:{s:8:"continue";s:30:"కొనసాగించు";}}s:2:"th";a:8:{s:8:"language";s:2:"th";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-03-02 17:08:41";s:12:"english_name";s:4:"Thai";s:11:"native_name";s:9:"ไทย";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.9.5/th.zip";s:3:"iso";a:2:{i:1;s:2:"th";i:2;s:3:"tha";}s:7:"strings";a:1:{s:8:"continue";s:15:"ต่อไป";}}s:2:"tl";a:8:{s:8:"language";s:2:"tl";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-12-30 02:38:08";s:12:"english_name";s:7:"Tagalog";s:11:"native_name";s:7:"Tagalog";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/tl.zip";s:3:"iso";a:2:{i:1;s:2:"tl";i:2;s:3:"tgl";}s:7:"strings";a:1:{s:8:"continue";s:10:"Magpatuloy";}}s:5:"tr_TR";a:8:{s:8:"language";s:5:"tr_TR";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-04-04 21:51:10";s:12:"english_name";s:7:"Turkish";s:11:"native_name";s:8:"Türkçe";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.5/tr_TR.zip";s:3:"iso";a:2:{i:1;s:2:"tr";i:2;s:3:"tur";}s:7:"strings";a:1:{s:8:"continue";s:5:"Devam";}}s:5:"tt_RU";a:8:{s:8:"language";s:5:"tt_RU";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-11-20 20:20:50";s:12:"english_name";s:5:"Tatar";s:11:"native_name";s:19:"Татар теле";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/tt_RU.zip";s:3:"iso";a:2:{i:1;s:2:"tt";i:2;s:3:"tat";}s:7:"strings";a:1:{s:8:"continue";s:17:"дәвам итү";}}s:3:"tah";a:8:{s:8:"language";s:3:"tah";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-03-06 18:39:39";s:12:"english_name";s:8:"Tahitian";s:11:"native_name";s:10:"Reo Tahiti";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.7.2/tah.zip";s:3:"iso";a:3:{i:1;s:2:"ty";i:2;s:3:"tah";i:3;s:3:"tah";}s:7:"strings";a:1:{s:8:"continue";s:0:"";}}s:5:"ug_CN";a:8:{s:8:"language";s:5:"ug_CN";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-04-12 12:31:53";s:12:"english_name";s:6:"Uighur";s:11:"native_name";s:16:"ئۇيغۇرچە";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.5/ug_CN.zip";s:3:"iso";a:2:{i:1;s:2:"ug";i:2;s:3:"uig";}s:7:"strings";a:1:{s:8:"continue";s:26:"داۋاملاشتۇرۇش";}}s:2:"uk";a:8:{s:8:"language";s:2:"uk";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-04-06 20:34:06";s:12:"english_name";s:9:"Ukrainian";s:11:"native_name";s:20:"Українська";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.9.5/uk.zip";s:3:"iso";a:2:{i:1;s:2:"uk";i:2;s:3:"ukr";}s:7:"strings";a:1:{s:8:"continue";s:20:"Продовжити";}}s:2:"ur";a:8:{s:8:"language";s:2:"ur";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-03-13 08:24:25";s:12:"english_name";s:4:"Urdu";s:11:"native_name";s:8:"اردو";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.9.5/ur.zip";s:3:"iso";a:2:{i:1;s:2:"ur";i:2;s:3:"urd";}s:7:"strings";a:1:{s:8:"continue";s:19:"جاری رکھیں";}}s:5:"uz_UZ";a:8:{s:8:"language";s:5:"uz_UZ";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-03-09 10:37:43";s:12:"english_name";s:5:"Uzbek";s:11:"native_name";s:11:"O‘zbekcha";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.5/uz_UZ.zip";s:3:"iso";a:2:{i:1;s:2:"uz";i:2;s:3:"uzb";}s:7:"strings";a:1:{s:8:"continue";s:11:"Davom etish";}}s:2:"vi";a:8:{s:8:"language";s:2:"vi";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-04-11 05:09:29";s:12:"english_name";s:10:"Vietnamese";s:11:"native_name";s:14:"Tiếng Việt";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.9.5/vi.zip";s:3:"iso";a:2:{i:1;s:2:"vi";i:2;s:3:"vie";}s:7:"strings";a:1:{s:8:"continue";s:12:"Tiếp tục";}}s:5:"zh_HK";a:8:{s:8:"language";s:5:"zh_HK";s:7:"version";s:5:"4.9.5";s:7:"updated";s:19:"2018-04-09 00:56:52";s:12:"english_name";s:19:"Chinese (Hong Kong)";s:11:"native_name";s:16:"香港中文版	";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.5/zh_HK.zip";s:3:"iso";a:2:{i:1;s:2:"zh";i:2;s:3:"zho";}s:7:"strings";a:1:{s:8:"continue";s:6:"繼續";}}s:5:"zh_TW";a:8:{s:8:"language";s:5:"zh_TW";s:7:"version";s:5:"4.9.4";s:7:"updated";s:19:"2018-02-13 02:41:15";s:12:"english_name";s:16:"Chinese (Taiwan)";s:11:"native_name";s:12:"繁體中文";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.4/zh_TW.zip";s:3:"iso";a:2:{i:1;s:2:"zh";i:2;s:3:"zho";}s:7:"strings";a:1:{s:8:"continue";s:6:"繼續";}}s:5:"zh_CN";a:8:{s:8:"language";s:5:"zh_CN";s:7:"version";s:5:"4.9.2";s:7:"updated";s:19:"2017-11-17 22:20:52";s:12:"english_name";s:15:"Chinese (China)";s:11:"native_name";s:12:"简体中文";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.9.2/zh_CN.zip";s:3:"iso";a:2:{i:1;s:2:"zh";i:2;s:3:"zho";}s:7:"strings";a:1:{s:8:"continue";s:6:"继续";}}}', 'no'),
(705, 'WPLANG', '', 'yes'),
(706, 'new_admin_email', 'info@ozproductionservices.com', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `ops_postmeta`
--

CREATE TABLE IF NOT EXISTS `ops_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=94 ;

--
-- Dumping data for table `ops_postmeta`
--

INSERT INTO `ops_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 8, '_wp_attached_file', '2018/04/chair.png'),
(3, 8, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1000;s:6:"height";i:1000;s:4:"file";s:17:"2018/04/chair.png";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"chair-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:17:"chair-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:12:"medium_large";a:4:{s:4:"file";s:17:"chair-768x768.png";s:5:"width";i:768;s:6:"height";i:768;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(4, 2, '_wp_trash_meta_status', 'publish'),
(5, 2, '_wp_trash_meta_time', '1525098299'),
(6, 2, '_wp_desired_post_slug', 'sample-page'),
(7, 11, '_edit_last', '1'),
(8, 11, 'wpeasycart_restrict_redirect_url', ''),
(9, 11, '_edit_lock', '1525099477:1'),
(10, 13, '_edit_last', '1'),
(11, 13, '_edit_lock', '1525101007:1'),
(12, 13, 'wpeasycart_restrict_redirect_url', ''),
(13, 15, '_edit_last', '1'),
(14, 15, '_edit_lock', '1525098204:1'),
(15, 15, 'wpeasycart_restrict_redirect_url', ''),
(16, 17, '_edit_last', '1'),
(17, 17, 'wpeasycart_restrict_redirect_url', ''),
(18, 17, '_edit_lock', '1525098522:1'),
(19, 19, '_menu_item_type', 'custom'),
(20, 19, '_menu_item_menu_item_parent', '0'),
(21, 19, '_menu_item_object_id', '19'),
(22, 19, '_menu_item_object', 'custom'),
(23, 19, '_menu_item_target', ''),
(24, 19, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(25, 19, '_menu_item_xfn', ''),
(26, 19, '_menu_item_url', 'http://localhost/oz_production/public_html/'),
(27, 19, '_menu_item_orphaned', '1525098707'),
(28, 20, '_menu_item_type', 'post_type'),
(29, 20, '_menu_item_menu_item_parent', '0'),
(30, 20, '_menu_item_object_id', '13'),
(31, 20, '_menu_item_object', 'page'),
(32, 20, '_menu_item_target', ''),
(33, 20, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(34, 20, '_menu_item_xfn', ''),
(35, 20, '_menu_item_url', ''),
(37, 21, '_menu_item_type', 'post_type'),
(38, 21, '_menu_item_menu_item_parent', '0'),
(39, 21, '_menu_item_object_id', '7'),
(40, 21, '_menu_item_object', 'page'),
(41, 21, '_menu_item_target', ''),
(42, 21, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(43, 21, '_menu_item_xfn', ''),
(44, 21, '_menu_item_url', ''),
(46, 22, '_menu_item_type', 'post_type'),
(47, 22, '_menu_item_menu_item_parent', '0'),
(48, 22, '_menu_item_object_id', '6'),
(49, 22, '_menu_item_object', 'page'),
(50, 22, '_menu_item_target', ''),
(51, 22, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(52, 22, '_menu_item_xfn', ''),
(53, 22, '_menu_item_url', ''),
(55, 23, '_menu_item_type', 'post_type'),
(56, 23, '_menu_item_menu_item_parent', '0'),
(57, 23, '_menu_item_object_id', '17'),
(58, 23, '_menu_item_object', 'page'),
(59, 23, '_menu_item_target', ''),
(60, 23, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(61, 23, '_menu_item_xfn', ''),
(62, 23, '_menu_item_url', ''),
(64, 24, '_menu_item_type', 'post_type'),
(65, 24, '_menu_item_menu_item_parent', '0'),
(66, 24, '_menu_item_object_id', '11'),
(67, 24, '_menu_item_object', 'page'),
(68, 24, '_menu_item_target', ''),
(69, 24, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(70, 24, '_menu_item_xfn', ''),
(71, 24, '_menu_item_url', ''),
(73, 25, '_menu_item_type', 'post_type'),
(74, 25, '_menu_item_menu_item_parent', '0'),
(75, 25, '_menu_item_object_id', '15'),
(76, 25, '_menu_item_object', 'page'),
(77, 25, '_menu_item_target', ''),
(78, 25, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(79, 25, '_menu_item_xfn', ''),
(80, 25, '_menu_item_url', ''),
(81, 25, '_menu_item_orphaned', '1525098709'),
(82, 26, '_menu_item_type', 'post_type'),
(83, 26, '_menu_item_menu_item_parent', '0'),
(84, 26, '_menu_item_object_id', '5'),
(85, 26, '_menu_item_object', 'page'),
(86, 26, '_menu_item_target', ''),
(87, 26, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(88, 26, '_menu_item_xfn', ''),
(89, 26, '_menu_item_url', ''),
(91, 11, '_wp_page_template', 'home.php'),
(92, 27, '_wp_attached_file', '2018/04/table.png'),
(93, 27, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:500;s:6:"height";i:282;s:4:"file";s:17:"2018/04/table.png";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"table-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:17:"table-300x169.png";s:5:"width";i:300;s:6:"height";i:169;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}');

-- --------------------------------------------------------

--
-- Table structure for table `ops_posts`
--

CREATE TABLE IF NOT EXISTS `ops_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=29 ;

--
-- Dumping data for table `ops_posts`
--

INSERT INTO `ops_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2018-04-30 06:23:21', '2018-04-30 06:23:21', 'Welcome to WordPress. This is your first post. Edit or delete it, then start writing!', 'Hello world!', '', 'publish', 'open', 'open', '', 'hello-world', '', '', '2018-04-30 06:23:21', '2018-04-30 06:23:21', '', 0, 'http://localhost/oz_production/public_html/?p=1', 0, 'post', '', 1),
(2, 1, '2018-04-30 06:23:21', '2018-04-30 06:23:21', 'This is an example page. It''s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\n\n<blockquote>Hi there! I''m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin'' caught in the rain.)</blockquote>\n\n...or something like this:\n\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\n\nAs a new WordPress user, you should go to <a href="http://localhost/oz_production/public_html/wp-admin/">your dashboard</a> to delete this page and create new pages for your content. Have fun!', 'Sample Page', '', 'trash', 'closed', 'open', '', 'sample-page__trashed', '', '', '2018-04-30 14:24:59', '2018-04-30 14:24:59', '', 0, 'http://localhost/oz_production/public_html/?page_id=2', 0, 'page', '', 0),
(3, 1, '2018-04-30 06:23:51', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2018-04-30 06:23:51', '0000-00-00 00:00:00', '', 0, 'http://localhost/oz_production/public_html/?p=3', 0, 'post', '', 0),
(4, 1, '2018-04-30 13:37:27', '2018-04-30 13:37:27', '[ec_store manufacturerid="1"]', 'Oz Production Event Services, LLC', '', 'publish', 'closed', 'closed', '', 'oz-production-event-services-llc', '', '', '2018-04-30 13:37:27', '2018-04-30 13:37:27', '', 0, 'http://localhost/oz_production/public_html/2018/04/30/oz-production-event-services-llc/', 0, 'ec_store', '', 0),
(5, 1, '2018-04-30 13:37:27', '2018-04-30 13:37:27', '[ec_store]', 'Store', '', 'publish', 'closed', 'closed', '', 'store', '', '', '2018-04-30 13:37:27', '2018-04-30 13:37:27', '', 0, 'http://localhost/oz_production/public_html/store/', 0, 'page', '', 0),
(6, 1, '2018-04-30 13:37:28', '2018-04-30 13:37:28', '[ec_cart]', 'Cart', '', 'publish', 'closed', 'closed', '', 'cart', '', '', '2018-04-30 13:37:28', '2018-04-30 13:37:28', '', 0, 'http://localhost/oz_production/public_html/cart/', 0, 'page', '', 0),
(7, 1, '2018-04-30 13:37:28', '2018-04-30 13:37:28', '[ec_account]', 'Account', '', 'publish', 'closed', 'closed', '', 'account', '', '', '2018-04-30 13:37:28', '2018-04-30 13:37:28', '', 0, 'http://localhost/oz_production/public_html/account/', 0, 'page', '', 0),
(8, 1, '2018-04-30 14:22:55', '2018-04-30 14:22:55', '', 'chair', '', 'inherit', 'open', 'closed', '', 'chair', '', '', '2018-04-30 14:22:55', '2018-04-30 14:22:55', '', 0, 'http://localhost/oz_production/public_html/wp-content/uploads/2018/04/chair.png', 0, 'attachment', 'image/png', 0),
(9, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[ec_store modelnumber="Chair"]', 'Chair', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis mattis fringilla lectus et blandit. Integer sit amet nulla sed mi porttitor tincidunt a non ligula. Quisque blandit dui odio, sed consectetur erat malesuada ac. Curabitur placerat orci magna, id malesuada massa iaculis id. Sed a felis id enim euismod tristique varius in libero. Sed tempus tristique felis ac vehicula. Vestibulum viverra, velit non aliquet rhoncus, odio dolor placerat dui, vel porttitor sem arcu ac massa. Mauris elementum, lacus sollicitudin dictum faucibus, magna enim aliquam libero, at pharetra justo quam eget arcu. Donec ut porttitor metus, luctus egestas dolor. Cras lacinia condimentum nunc fermentum vulputate. Donec feugiat auctor purus. Suspendisse lobortis diam eget pellentesque cursus. Proin non dictum lorem, porttitor tempor nisi.</p><p>Integer auctor lobortis nibh, a imperdiet dolor pulvinar a. Donec tempor aliquet mauris, vitae feugiat sapien dapibus sit amet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget libero sed arcu efficitur iaculis in ut quam. Fusce nisl tellus, ultrices vitae ex id, interdum porttitor diam. Aliquam et velit tellus. Vivamus varius nec lacus id interdum. Mauris sollicitudin orci eget lectus porta tincidunt. Phasellus suscipit, justo et ultricies tristique, eros libero maximus lacus, in pulvinar lectus arcu eu eros. Mauris nec convallis ligula, dictum dignissim lacus.</p>', 'publish', 'open', 'open', '', 'chair', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 0, 'http://localhost/oz_production/public_html/store/chair/', 0, 'ec_store', '', 0),
(10, 1, '2018-04-30 14:24:59', '2018-04-30 14:24:59', 'This is an example page. It''s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\n\n<blockquote>Hi there! I''m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin'' caught in the rain.)</blockquote>\n\n...or something like this:\n\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\n\nAs a new WordPress user, you should go to <a href="http://localhost/oz_production/public_html/wp-admin/">your dashboard</a> to delete this page and create new pages for your content. Have fun!', 'Sample Page', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2018-04-30 14:24:59', '2018-04-30 14:24:59', '', 2, 'http://localhost/oz_production/public_html/2018/04/30/2-revision-v1/', 0, 'revision', '', 0),
(11, 1, '2018-04-30 14:25:07', '2018-04-30 14:25:07', '', 'Home', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2018-04-30 14:38:56', '2018-04-30 14:38:56', '', 0, 'http://localhost/oz_production/public_html/?page_id=11', 0, 'page', '', 0),
(12, 1, '2018-04-30 14:25:07', '2018-04-30 14:25:07', '', 'Home', '', 'inherit', 'closed', 'closed', '', '11-revision-v1', '', '', '2018-04-30 14:25:07', '2018-04-30 14:25:07', '', 11, 'http://localhost/oz_production/public_html/2018/04/30/11-revision-v1/', 0, 'revision', '', 0),
(13, 1, '2018-04-30 14:25:26', '2018-04-30 14:25:26', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis mattis fringilla lectus et blandit. Integer sit amet nulla sed mi porttitor tincidunt a non ligula. Quisque blandit dui odio, sed consectetur erat malesuada ac. Curabitur placerat orci magna, id malesuada massa iaculis id. Sed a felis id enim euismod tristique varius in libero. Sed tempus tristique felis ac vehicula. Vestibulum viverra, velit non aliquet rhoncus, odio dolor placerat dui, vel porttitor sem arcu ac massa. Mauris elementum, lacus sollicitudin dictum faucibus, magna enim aliquam libero, at pharetra justo quam eget arcu. Donec ut porttitor metus, luctus egestas dolor. Cras lacinia condimentum nunc fermentum vulputate. Donec feugiat auctor purus. Suspendisse lobortis diam eget pellentesque cursus. Proin non dictum lorem, porttitor tempor nisi.\r\n\r\nInteger auctor lobortis nibh, a imperdiet dolor pulvinar a. Donec tempor aliquet mauris, vitae feugiat sapien dapibus sit amet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget libero sed arcu efficitur iaculis in ut quam. Fusce nisl tellus, ultrices vitae ex id, interdum porttitor diam. Aliquam et velit tellus. Vivamus varius nec lacus id interdum. Mauris sollicitudin orci eget lectus porta tincidunt. Phasellus suscipit, justo et ultricies tristique, eros libero maximus lacus, in pulvinar lectus arcu eu eros. Mauris nec convallis ligula, dictum dignissim lacus.', 'About Us', '', 'publish', 'closed', 'closed', '', 'about-us', '', '', '2018-04-30 14:25:26', '2018-04-30 14:25:26', '', 0, 'http://localhost/oz_production/public_html/?page_id=13', 0, 'page', '', 0),
(14, 1, '2018-04-30 14:25:26', '2018-04-30 14:25:26', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis mattis fringilla lectus et blandit. Integer sit amet nulla sed mi porttitor tincidunt a non ligula. Quisque blandit dui odio, sed consectetur erat malesuada ac. Curabitur placerat orci magna, id malesuada massa iaculis id. Sed a felis id enim euismod tristique varius in libero. Sed tempus tristique felis ac vehicula. Vestibulum viverra, velit non aliquet rhoncus, odio dolor placerat dui, vel porttitor sem arcu ac massa. Mauris elementum, lacus sollicitudin dictum faucibus, magna enim aliquam libero, at pharetra justo quam eget arcu. Donec ut porttitor metus, luctus egestas dolor. Cras lacinia condimentum nunc fermentum vulputate. Donec feugiat auctor purus. Suspendisse lobortis diam eget pellentesque cursus. Proin non dictum lorem, porttitor tempor nisi.\r\n\r\nInteger auctor lobortis nibh, a imperdiet dolor pulvinar a. Donec tempor aliquet mauris, vitae feugiat sapien dapibus sit amet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget libero sed arcu efficitur iaculis in ut quam. Fusce nisl tellus, ultrices vitae ex id, interdum porttitor diam. Aliquam et velit tellus. Vivamus varius nec lacus id interdum. Mauris sollicitudin orci eget lectus porta tincidunt. Phasellus suscipit, justo et ultricies tristique, eros libero maximus lacus, in pulvinar lectus arcu eu eros. Mauris nec convallis ligula, dictum dignissim lacus.', 'About Us', '', 'inherit', 'closed', 'closed', '', '13-revision-v1', '', '', '2018-04-30 14:25:26', '2018-04-30 14:25:26', '', 13, 'http://localhost/oz_production/public_html/2018/04/30/13-revision-v1/', 0, 'revision', '', 0),
(15, 1, '2018-04-30 14:25:45', '2018-04-30 14:25:45', '', 'Services', '', 'publish', 'closed', 'closed', '', 'services', '', '', '2018-04-30 14:25:45', '2018-04-30 14:25:45', '', 0, 'http://localhost/oz_production/public_html/?page_id=15', 0, 'page', '', 0),
(16, 1, '2018-04-30 14:25:45', '2018-04-30 14:25:45', '', 'Services', '', 'inherit', 'closed', 'closed', '', '15-revision-v1', '', '', '2018-04-30 14:25:45', '2018-04-30 14:25:45', '', 15, 'http://localhost/oz_production/public_html/2018/04/30/15-revision-v1/', 0, 'revision', '', 0),
(17, 1, '2018-04-30 14:25:55', '2018-04-30 14:25:55', '', 'Contact Us', '', 'publish', 'closed', 'closed', '', 'contact-us', '', '', '2018-04-30 14:31:01', '2018-04-30 14:31:01', '', 0, 'http://localhost/oz_production/public_html/?page_id=17', 0, 'page', '', 0),
(18, 1, '2018-04-30 14:25:55', '2018-04-30 14:25:55', '', 'Contact Us', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2018-04-30 14:25:55', '2018-04-30 14:25:55', '', 17, 'http://localhost/oz_production/public_html/2018/04/30/17-revision-v1/', 0, 'revision', '', 0),
(19, 1, '2018-04-30 14:31:47', '0000-00-00 00:00:00', '', 'Home', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-04-30 14:31:47', '0000-00-00 00:00:00', '', 0, 'http://localhost/oz_production/public_html/?p=19', 1, 'nav_menu_item', '', 0),
(20, 1, '2018-04-30 14:32:48', '2018-04-30 14:32:48', ' ', '', '', 'publish', 'closed', 'closed', '', '20', '', '', '2018-04-30 15:08:03', '2018-04-30 15:08:03', '', 0, 'http://localhost/oz_production/public_html/?p=20', 2, 'nav_menu_item', '', 0),
(21, 1, '2018-04-30 14:32:48', '2018-04-30 14:32:48', ' ', '', '', 'publish', 'closed', 'closed', '', '21', '', '', '2018-04-30 15:08:03', '2018-04-30 15:08:03', '', 0, 'http://localhost/oz_production/public_html/?p=21', 4, 'nav_menu_item', '', 0),
(22, 1, '2018-04-30 14:32:48', '2018-04-30 14:32:48', ' ', '', '', 'publish', 'closed', 'closed', '', '22', '', '', '2018-04-30 15:08:03', '2018-04-30 15:08:03', '', 0, 'http://localhost/oz_production/public_html/?p=22', 5, 'nav_menu_item', '', 0),
(23, 1, '2018-04-30 14:32:48', '2018-04-30 14:32:48', ' ', '', '', 'publish', 'closed', 'closed', '', '23', '', '', '2018-04-30 15:08:03', '2018-04-30 15:08:03', '', 0, 'http://localhost/oz_production/public_html/?p=23', 6, 'nav_menu_item', '', 0),
(24, 1, '2018-04-30 14:32:47', '2018-04-30 14:32:47', ' ', '', '', 'publish', 'closed', 'closed', '', '24', '', '', '2018-04-30 15:08:03', '2018-04-30 15:08:03', '', 0, 'http://localhost/oz_production/public_html/?p=24', 1, 'nav_menu_item', '', 0),
(25, 1, '2018-04-30 14:31:49', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-04-30 14:31:49', '0000-00-00 00:00:00', '', 0, 'http://localhost/oz_production/public_html/?p=25', 1, 'nav_menu_item', '', 0),
(26, 1, '2018-04-30 14:32:48', '2018-04-30 14:32:48', ' ', '', '', 'publish', 'closed', 'closed', '', 'services', '', '', '2018-04-30 15:08:03', '2018-04-30 15:08:03', '', 0, 'http://localhost/oz_production/public_html/?p=26', 3, 'nav_menu_item', '', 0),
(27, 1, '2018-04-30 15:09:51', '2018-04-30 15:09:51', '', 'table', '', 'inherit', 'open', 'closed', '', 'table', '', '', '2018-04-30 15:09:51', '2018-04-30 15:09:51', '', 0, 'http://localhost/oz_production/public_html/wp-content/uploads/2018/04/table.png', 0, 'attachment', 'image/png', 0),
(28, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[ec_store modelnumber="table"]', 'Table', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis mattis fringilla lectus et blandit. Integer sit amet nulla sed mi porttitor tincidunt a non ligula. Quisque blandit dui odio, sed consectetur erat malesuada ac. Curabitur placerat orci magna, id malesuada massa iaculis id. Sed a felis id enim euismod tristique varius in libero. Sed tempus tristique felis ac vehicula. Vestibulum viverra, velit non aliquet rhoncus, odio dolor placerat dui, vel porttitor sem arcu ac massa. Mauris elementum, lacus sollicitudin dictum faucibus, magna enim aliquam libero, at pharetra justo quam eget arcu. Donec ut porttitor metus, luctus egestas dolor. Cras lacinia condimentum nunc fermentum vulputate. Donec feugiat auctor purus. Suspendisse lobortis diam eget pellentesque cursus. Proin non dictum lorem, porttitor tempor nisi.</p><p>Integer auctor lobortis nibh, a imperdiet dolor pulvinar a. Donec tempor aliquet mauris, vitae feugiat sapien dapibus sit amet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget libero sed arcu efficitur iaculis in ut quam. Fusce nisl tellus, ultrices vitae ex id, interdum porttitor diam. Aliquam et velit tellus. Vivamus varius nec lacus id interdum. Mauris sollicitudin orci eget lectus porta tincidunt. Phasellus suscipit, justo et ultricies tristique, eros libero maximus lacus, in pulvinar lectus arcu eu eros. Mauris nec convallis ligula, dictum dignissim lacus.</p>', 'publish', 'open', 'open', '', 'table', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 0, 'http://localhost/oz_production/public_html/store/table/', 0, 'ec_store', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ops_termmeta`
--

CREATE TABLE IF NOT EXISTS `ops_termmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ops_terms`
--

CREATE TABLE IF NOT EXISTS `ops_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `ops_terms`
--

INSERT INTO `ops_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'Main_Menu', 'main_menu', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ops_term_relationships`
--

CREATE TABLE IF NOT EXISTS `ops_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `ops_term_relationships`
--

INSERT INTO `ops_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(20, 2, 0),
(21, 2, 0),
(22, 2, 0),
(23, 2, 0),
(24, 2, 0),
(26, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ops_term_taxonomy`
--

CREATE TABLE IF NOT EXISTS `ops_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `ops_term_taxonomy`
--

INSERT INTO `ops_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'nav_menu', '', 0, 6);

-- --------------------------------------------------------

--
-- Table structure for table `ops_usermeta`
--

CREATE TABLE IF NOT EXISTS `ops_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=24 ;

--
-- Dumping data for table `ops_usermeta`
--

INSERT INTO `ops_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'false'),
(11, 1, 'locale', ''),
(12, 1, 'ops_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(13, 1, 'ops_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', ''),
(15, 1, 'show_welcome_panel', '1'),
(16, 1, 'session_tokens', 'a:1:{s:64:"1472437afdd6763a30fc91673b6435bcc31f749e7bd8c064853499fa4e1b09d3";a:4:{s:10:"expiration";i:1525242229;s:2:"ip";s:3:"::1";s:2:"ua";s:115:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36";s:5:"login";i:1525069429;}}'),
(17, 1, 'ops_dashboard_quick_press_last_post_id', '3'),
(18, 1, 'community-events-location', 'a:1:{s:2:"ip";s:2:"::";}'),
(19, 1, 'ops_user-settings', 'libraryContent=browse'),
(20, 1, 'ops_user-settings-time', '1525098202'),
(21, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:"link-target";i:1;s:11:"css-classes";i:2;s:3:"xfn";i:3;s:11:"description";i:4;s:15:"title-attribute";}'),
(22, 1, 'metaboxhidden_nav-menus', 'a:2:{i:0;s:22:"add-post-type-ec_store";i:1;s:12:"add-post_tag";}'),
(23, 1, 'nav_menu_recently_edited', '2');

-- --------------------------------------------------------

--
-- Table structure for table `ops_users`
--

CREATE TABLE IF NOT EXISTS `ops_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ops_users`
--

INSERT INTO `ops_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$P$BdXlqYsrFsajH/3QZZ24vJg29ZrhUn0', 'admin', 'info@ozproductionservices.com', '', '2018-04-30 06:23:21', '', 0, 'admin');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
