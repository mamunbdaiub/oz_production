-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 01, 2018 at 03:58 PM
-- Server version: 5.6.17
-- PHP Version: 5.6.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `oz_production`
--

-- --------------------------------------------------------

--
-- Table structure for table `ec_address`
--

CREATE TABLE IF NOT EXISTS `ec_address` (
  `address_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `first_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `last_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `address_line_1` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `address_line_2` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT '',
  `city` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `state` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `zip` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `country` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `phone` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `company_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`address_id`),
  UNIQUE KEY `address_id` (`address_id`),
  KEY `ec_address_idx1` (`address_id`),
  KEY `ec_address_idx2` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=15 ;

--
-- Dumping data for table `ec_address`
--

INSERT INTO `ec_address` (`address_id`, `user_id`, `first_name`, `last_name`, `address_line_1`, `address_line_2`, `city`, `state`, `zip`, `country`, `phone`, `company_name`) VALUES
(1, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'KS', '10991', 'US', '43535353', ''),
(2, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'KS', '10991', 'US', '43535353', ''),
(3, 0, 'Test', 'Abdul', 'NY, Newyork, USA', '', 'Kansas', 'KS', '10991', 'US', '43535353', ''),
(4, 0, 'Test', 'Abdul', 'NY, Newyork, USA', '', 'Kansas', 'KS', '10991', 'US', '43535353', ''),
(5, 0, 'Mamun', 'Abdullah', 'NY, Newyork, USA', '', 'Kansas', 'IN', '10991', 'US', '43535353', ''),
(6, 0, 'Mamun', 'Abdullah', 'NY, Newyork, USA', '', 'Kansas', 'IN', '10991', 'US', '43535353', ''),
(7, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'IA', '10991', 'US', '43535353', ''),
(8, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'IA', '10991', 'US', '43535353', ''),
(9, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'IL', '10991', 'US', '43535353', ''),
(10, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'IL', '10991', 'US', '43535353', ''),
(11, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'KS', '10991', 'US', '43535353', ''),
(12, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'KS', '10991', 'US', '43535353', ''),
(13, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'AL', '10991', 'US', '43535353', ''),
(14, 0, 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'AL', '10991', 'US', '43535353', '');

-- --------------------------------------------------------

--
-- Table structure for table `ec_affiliate_rule`
--

CREATE TABLE IF NOT EXISTS `ec_affiliate_rule` (
  `affiliate_rule_id` int(11) NOT NULL AUTO_INCREMENT,
  `rule_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `rule_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `rule_amount` float(15,3) NOT NULL DEFAULT '0.000',
  `rule_limit` int(11) NOT NULL DEFAULT '0',
  `rule_active` tinyint(1) NOT NULL DEFAULT '1',
  `rule_recurring` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`affiliate_rule_id`),
  UNIQUE KEY `affiliate_rule_id` (`affiliate_rule_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_affiliate_rule_to_affiliate`
--

CREATE TABLE IF NOT EXISTS `ec_affiliate_rule_to_affiliate` (
  `rule_to_account_id` int(11) NOT NULL AUTO_INCREMENT,
  `affiliate_rule_id` int(11) NOT NULL DEFAULT '0',
  `affiliate_id` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`rule_to_account_id`),
  UNIQUE KEY `rule_to_account_id` (`rule_to_account_id`),
  KEY `affiliate_rule_id` (`affiliate_rule_id`),
  KEY `affiliate_id` (`affiliate_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_affiliate_rule_to_product`
--

CREATE TABLE IF NOT EXISTS `ec_affiliate_rule_to_product` (
  `rule_to_product_id` int(11) NOT NULL AUTO_INCREMENT,
  `affiliate_rule_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`rule_to_product_id`),
  UNIQUE KEY `rule_to_product_id` (`rule_to_product_id`),
  KEY `affiliate_rule_id` (`affiliate_rule_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_bundle`
--

CREATE TABLE IF NOT EXISTS `ec_bundle` (
  `bundle_id` int(11) NOT NULL AUTO_INCREMENT,
  `key_product_id` int(11) NOT NULL DEFAULT '0',
  `bundled_product_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`bundle_id`),
  UNIQUE KEY `bundle_id` (`bundle_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_category`
--

CREATE TABLE IF NOT EXISTS `ec_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_demo_item` tinyint(1) NOT NULL DEFAULT '0',
  `category_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_id` int(11) NOT NULL DEFAULT '0',
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `short_description` text COLLATE utf8mb4_unicode_520_ci,
  `image` text COLLATE utf8mb4_unicode_520_ci,
  `featured_category` tinyint(1) NOT NULL DEFAULT '0',
  `priority` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `category_id` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_categoryitem`
--

CREATE TABLE IF NOT EXISTS `ec_categoryitem` (
  `categoryitem_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`categoryitem_id`),
  UNIQUE KEY `categoryitem_id` (`categoryitem_id`),
  KEY `product_id` (`product_id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_code`
--

CREATE TABLE IF NOT EXISTS `ec_code` (
  `code_id` int(11) NOT NULL AUTO_INCREMENT,
  `code_val` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT '',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `orderdetail_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`code_id`),
  UNIQUE KEY `code_id` (`code_id`),
  KEY `product_id` (`product_id`),
  KEY `orderdetail_id` (`orderdetail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_country`
--

CREATE TABLE IF NOT EXISTS `ec_country` (
  `id_cnt` int(11) NOT NULL AUTO_INCREMENT,
  `name_cnt` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `iso2_cnt` varchar(10) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `iso3_cnt` varchar(10) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `sort_order` int(11) NOT NULL,
  `vat_rate_cnt` float(9,3) NOT NULL DEFAULT '0.000',
  `ship_to_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_cnt`),
  KEY `iso2_cnt` (`iso2_cnt`),
  KEY `iso3_cnt` (`iso3_cnt`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=241 ;

--
-- Dumping data for table `ec_country`
--

INSERT INTO `ec_country` (`id_cnt`, `name_cnt`, `iso2_cnt`, `iso3_cnt`, `sort_order`, `vat_rate_cnt`, `ship_to_active`) VALUES
(1, 'Afghanistan', 'AF', 'AFG', 10, 0.000, 1),
(2, 'Albania', 'AL', 'ALB', 11, 0.000, 1),
(3, 'Algeria', 'DZ', 'DZA', 12, 0.000, 1),
(4, 'American Samoa', 'AS', 'ASM', 13, 0.000, 1),
(5, 'Andorra', 'AD', 'AND', 14, 0.000, 1),
(6, 'Angola', 'AO', 'AGO', 15, 0.000, 1),
(7, 'Anguilla', 'AI', 'AIA', 16, 0.000, 1),
(8, 'Antarctica', 'AQ', 'ATA', 17, 0.000, 1),
(9, 'Antigua and Barbuda', 'AG', 'ATG', 18, 0.000, 1),
(10, 'Argentina', 'AR', 'ARG', 19, 0.000, 1),
(11, 'Armenia', 'AM', 'ARM', 20, 0.000, 1),
(12, 'Aruba', 'AW', 'ABW', 21, 0.000, 1),
(13, 'Australia', 'AU', 'AUS', 3, 0.000, 1),
(14, 'Austria', 'AT', 'AUT', 23, 0.000, 1),
(15, 'Azerbaijan', 'AZ', 'AZE', 24, 0.000, 1),
(16, 'Bahamas', 'BS', 'BHS', 25, 0.000, 1),
(17, 'Bahrain', 'BH', 'BHR', 26, 0.000, 1),
(18, 'Bangladesh', 'BD', 'BGD', 27, 0.000, 1),
(19, 'Barbados', 'BB', 'BRB', 28, 0.000, 1),
(20, 'Belarus', 'BY', 'BLR', 29, 0.000, 1),
(21, 'Belgium', 'BE', 'BEL', 30, 0.000, 1),
(22, 'Belize', 'BZ', 'BLZ', 31, 0.000, 1),
(23, 'Benin', 'BJ', 'BEN', 32, 0.000, 1),
(24, 'Bermuda', 'BM', 'BMU', 33, 0.000, 1),
(25, 'Bhutan', 'BT', 'BTN', 34, 0.000, 1),
(26, 'Bolivia', 'BO', 'BOL', 35, 0.000, 1),
(28, 'Botswana', 'BW', 'BWA', 36, 0.000, 1),
(29, 'Bouvet Island', 'BV', 'BVT', 37, 0.000, 1),
(30, 'Brazil', 'BR', 'BRA', 38, 0.000, 1),
(32, 'Brunei Darussalam', 'BN', 'BRN', 39, 0.000, 1),
(33, 'Bulgaria', 'BG', 'BGR', 40, 0.000, 1),
(34, 'Burkina Faso', 'BF', 'BFA', 41, 0.000, 1),
(35, 'Burundi', 'BI', 'BDI', 42, 0.000, 1),
(36, 'Cambodia', 'KH', 'KHM', 43, 0.000, 1),
(37, 'Cameroon', 'CM', 'CMR', 44, 0.000, 1),
(38, 'Canada', 'CA', 'CAN', 2, 0.000, 1),
(39, 'Cape Verde', 'CV', 'CPV', 46, 0.000, 1),
(40, 'Cayman Islands', 'KY', 'CYM', 47, 0.000, 1),
(42, 'Chad', 'TD', 'TCD', 48, 0.000, 1),
(43, 'Chile', 'CL', 'CHL', 49, 0.000, 1),
(44, 'China', 'CN', 'CHN', 50, 0.000, 1),
(45, 'Christmas Island', 'CX', 'CXR', 51, 0.000, 1),
(47, 'Colombia', 'CO', 'COL', 52, 0.000, 1),
(48, 'Comoros', 'KM', 'COM', 53, 0.000, 1),
(49, 'Congo', 'CG', 'COG', 54, 0.000, 1),
(50, 'Cook Islands', 'CK', 'COK', 55, 0.000, 1),
(51, 'Costa Rica', 'CR', 'CRI', 56, 0.000, 1),
(52, 'Cote D''''Ivoire', 'CI', 'CIV', 57, 0.000, 1),
(53, 'Croatia', 'HR', 'HRV', 58, 0.000, 1),
(54, 'Cuba', 'CU', 'CUB', 59, 0.000, 1),
(55, 'Cyprus', 'CY', 'CYP', 60, 0.000, 1),
(56, 'Czech Republic', 'CZ', 'CZE', 61, 0.000, 1),
(57, 'Denmark', 'DK', 'DNK', 62, 0.000, 1),
(58, 'Djibouti', 'DJ', 'DJI', 63, 0.000, 1),
(59, 'Dominica', 'DM', 'DMA', 64, 0.000, 1),
(60, 'Dominican Republic', 'DO', 'DOM', 65, 0.000, 1),
(61, 'East Timor', 'TP', 'TMP', 66, 0.000, 1),
(62, 'Ecuador', 'EC', 'ECU', 67, 0.000, 1),
(63, 'Egypt', 'EG', 'EGY', 68, 0.000, 1),
(64, 'El Salvador', 'SV', 'SLV', 69, 0.000, 1),
(65, 'Equatorial Guinea', 'GQ', 'GNQ', 70, 0.000, 1),
(66, 'Eritrea', 'ER', 'ERI', 71, 0.000, 1),
(67, 'Estonia', 'EE', 'EST', 72, 0.000, 1),
(68, 'Ethiopia', 'ET', 'ETH', 73, 0.000, 1),
(70, 'Faroe Islands', 'FO', 'FRO', 74, 0.000, 1),
(71, 'Fiji', 'FJ', 'FJI', 75, 0.000, 1),
(72, 'Finland', 'FI', 'FIN', 76, 0.000, 1),
(73, 'France', 'FR', 'FRA', 77, 0.000, 1),
(74, 'France, Metropolitan', 'FX', 'FXX', 78, 0.000, 1),
(75, 'French Guiana', 'GF', 'GUF', 79, 0.000, 1),
(76, 'French Polynesia', 'PF', 'PYF', 80, 0.000, 1),
(78, 'Gabon', 'GA', 'GAB', 81, 0.000, 1),
(79, 'Gambia', 'GM', 'GMB', 82, 0.000, 1),
(80, 'Georgia', 'GE', 'GEO', 83, 0.000, 1),
(81, 'Germany', 'DE', 'DEU', 84, 0.000, 1),
(82, 'Ghana', 'GH', 'GHA', 85, 0.000, 1),
(83, 'Gibraltar', 'GI', 'GIB', 86, 0.000, 1),
(84, 'Greece', 'GR', 'GRC', 87, 0.000, 1),
(85, 'Greenland', 'GL', 'GRL', 88, 0.000, 1),
(86, 'Grenada', 'GD', 'GRD', 89, 0.000, 1),
(87, 'Guadeloupe', 'GP', 'GLP', 90, 0.000, 1),
(88, 'Guam', 'GU', 'GUM', 91, 0.000, 1),
(89, 'Guatemala', 'GT', 'GTM', 92, 0.000, 1),
(90, 'Guinea', 'GN', 'GIN', 93, 0.000, 1),
(91, 'Guinea-bissau', 'GW', 'GNB', 94, 0.000, 1),
(92, 'Guyana', 'GY', 'GUY', 95, 0.000, 1),
(93, 'Haiti', 'HT', 'HTI', 96, 0.000, 1),
(95, 'Honduras', 'HN', 'HND', 97, 0.000, 1),
(96, 'Hong Kong', 'HK', 'HKG', 98, 0.000, 1),
(97, 'Hungary', 'HU', 'HUN', 99, 0.000, 1),
(98, 'Iceland', 'IS', 'ISL', 100, 0.000, 1),
(99, 'India', 'IN', 'IND', 101, 0.000, 1),
(100, 'Indonesia', 'ID', 'IDN', 102, 0.000, 1),
(102, 'Iraq', 'IQ', 'IRQ', 103, 0.000, 1),
(103, 'Ireland', 'IE', 'IRL', 104, 0.000, 1),
(104, 'Israel', 'IL', 'ISR', 105, 0.000, 1),
(105, 'Italy', 'IT', 'ITA', 106, 0.000, 1),
(106, 'Jamaica', 'JM', 'JAM', 107, 0.000, 1),
(107, 'Japan', 'JP', 'JPN', 108, 0.000, 1),
(108, 'Jordan', 'JO', 'JOR', 109, 0.000, 1),
(109, 'Kazakhstan', 'KZ', 'KAZ', 110, 0.000, 1),
(110, 'Kenya', 'KE', 'KEN', 111, 0.000, 1),
(111, 'Kiribati', 'KI', 'KIR', 112, 0.000, 1),
(113, 'Korea, Republic of', 'KR', 'KOR', 113, 0.000, 1),
(114, 'Kuwait', 'KW', 'KWT', 114, 0.000, 1),
(115, 'Kyrgyzstan', 'KG', 'KGZ', 115, 0.000, 1),
(117, 'Latvia', 'LV', 'LVA', 116, 0.000, 1),
(118, 'Lebanon', 'LB', 'LBN', 117, 0.000, 1),
(119, 'Lesotho', 'LS', 'LSO', 118, 0.000, 1),
(120, 'Liberia', 'LR', 'LBR', 119, 0.000, 1),
(122, 'Liechtenstein', 'LI', 'LIE', 120, 0.000, 1),
(123, 'Lithuania', 'LT', 'LTU', 121, 0.000, 1),
(124, 'Luxembourg', 'LU', 'LUX', 122, 0.000, 1),
(125, 'Macau', 'MO', 'MAC', 123, 0.000, 1),
(127, 'Madagascar', 'MG', 'MDG', 124, 0.000, 1),
(128, 'Malawi', 'MW', 'MWI', 125, 0.000, 1),
(129, 'Malaysia', 'MY', 'MYS', 126, 0.000, 1),
(130, 'Maldives', 'MV', 'MDV', 127, 0.000, 1),
(131, 'Mali', 'ML', 'MLI', 128, 0.000, 1),
(132, 'Malta', 'MT', 'MLT', 129, 0.000, 1),
(133, 'Marshall Islands', 'MH', 'MHL', 130, 0.000, 1),
(134, 'Martinique', 'MQ', 'MTQ', 131, 0.000, 1),
(135, 'Mauritania', 'MR', 'MRT', 132, 0.000, 1),
(136, 'Mauritius', 'MU', 'MUS', 133, 0.000, 1),
(137, 'Mayotte', 'YT', 'MYT', 134, 0.000, 1),
(138, 'Mexico', 'MX', 'MEX', 135, 0.000, 1),
(141, 'Monaco', 'MC', 'MCO', 136, 0.000, 1),
(142, 'Mongolia', 'MN', 'MNG', 137, 0.000, 1),
(143, 'Montserrat', 'MS', 'MSR', 138, 0.000, 1),
(144, 'Morocco', 'MA', 'MAR', 139, 0.000, 1),
(145, 'Mozambique', 'MZ', 'MOZ', 140, 0.000, 1),
(146, 'Myanmar', 'MM', 'MMR', 141, 0.000, 1),
(147, 'Namibia', 'NA', 'NAM', 142, 0.000, 1),
(148, 'Nauru', 'NR', 'NRU', 143, 0.000, 1),
(149, 'Nepal', 'NP', 'NPL', 144, 0.000, 1),
(150, 'Netherlands', 'NL', 'NLD', 145, 0.000, 1),
(151, 'Netherlands Antilles', 'AN', 'ANT', 146, 0.000, 1),
(152, 'New Caledonia', 'NC', 'NCL', 147, 0.000, 1),
(153, 'New Zealand', 'NZ', 'NZL', 148, 0.000, 1),
(154, 'Nicaragua', 'NI', 'NIC', 149, 0.000, 1),
(155, 'Niger', 'NE', 'NER', 150, 0.000, 1),
(156, 'Nigeria', 'NG', 'NGA', 151, 0.000, 1),
(157, 'Niue', 'NU', 'NIU', 152, 0.000, 1),
(158, 'Norfolk Island', 'NF', 'NFK', 153, 0.000, 1),
(160, 'Norway', 'NO', 'NOR', 154, 0.000, 1),
(161, 'Oman', 'OM', 'OMN', 155, 0.000, 1),
(162, 'Pakistan', 'PK', 'PAK', 156, 0.000, 1),
(163, 'Palau', 'PW', 'PLW', 157, 0.000, 1),
(164, 'Panama', 'PA', 'PAN', 158, 0.000, 1),
(165, 'Papua New Guinea', 'PG', 'PNG', 159, 0.000, 1),
(166, 'Paraguay', 'PY', 'PRY', 160, 0.000, 1),
(167, 'Peru', 'PE', 'PER', 161, 0.000, 1),
(168, 'Philippines', 'PH', 'PHL', 162, 0.000, 1),
(169, 'Pitcairn', 'PN', 'PCN', 163, 0.000, 1),
(170, 'Poland', 'PL', 'POL', 164, 0.000, 1),
(171, 'Portugal', 'PT', 'PRT', 165, 0.000, 1),
(172, 'Puerto Rico', 'PR', 'PRI', 166, 0.000, 1),
(173, 'Qatar', 'QA', 'QAT', 167, 0.000, 1),
(174, 'Reunion', 'RE', 'REU', 168, 0.000, 1),
(175, 'Romania', 'RO', 'ROM', 169, 0.000, 1),
(176, 'Russian Federation', 'RU', 'RUS', 170, 0.000, 1),
(177, 'Rwanda', 'RW', 'RWA', 171, 0.000, 1),
(178, 'Saint Kitts and Nevis', 'KN', 'KNA', 172, 0.000, 1),
(179, 'Saint Lucia', 'LC', 'LCA', 173, 0.000, 1),
(181, 'Samoa', 'WS', 'WSM', 174, 0.000, 1),
(182, 'San Marino', 'SM', 'SMR', 175, 0.000, 1),
(183, 'Sao Tome and Principe', 'ST', 'STP', 176, 0.000, 1),
(184, 'Saudi Arabia', 'SA', 'SAU', 177, 0.000, 1),
(185, 'Senegal', 'SN', 'SEN', 178, 0.000, 1),
(186, 'Seychelles', 'SC', 'SYC', 179, 0.000, 1),
(187, 'Sierra Leone', 'SL', 'SLE', 180, 0.000, 1),
(188, 'Singapore', 'SG', 'SGP', 181, 0.000, 1),
(190, 'Slovenia', 'SI', 'SVN', 182, 0.000, 1),
(191, 'Solomon Islands', 'SB', 'SLB', 183, 0.000, 1),
(192, 'Somalia', 'SO', 'SOM', 184, 0.000, 1),
(193, 'South Africa', 'ZA', 'ZAF', 185, 0.000, 1),
(195, 'Spain', 'ES', 'ESP', 186, 0.000, 1),
(196, 'Sri Lanka', 'LK', 'LKA', 187, 0.000, 1),
(197, 'St. Helena', 'SH', 'SHN', 188, 0.000, 1),
(198, 'St. Pierre and Miquelon', 'PM', 'SPM', 189, 0.000, 1),
(199, 'Sudan', 'SD', 'SDN', 190, 0.000, 1),
(200, 'Suriname', 'SR', 'SUR', 191, 0.000, 1),
(202, 'Swaziland', 'SZ', 'SWZ', 192, 0.000, 1),
(203, 'Sweden', 'SE', 'SWE', 193, 0.000, 1),
(204, 'Switzerland', 'CH', 'CHE', 194, 0.000, 1),
(205, 'Syrian Arab Republic', 'SY', 'SYR', 195, 0.000, 1),
(206, 'Taiwan', 'TW', 'TWN', 196, 0.000, 1),
(207, 'Tajikistan', 'TJ', 'TJK', 197, 0.000, 1),
(209, 'Thailand', 'TH', 'THA', 198, 0.000, 1),
(210, 'Togo', 'TG', 'TGO', 199, 0.000, 1),
(211, 'Tokelau', 'TK', 'TKL', 200, 0.000, 1),
(212, 'Tonga', 'TO', 'TON', 201, 0.000, 1),
(213, 'Trinidad and Tobago', 'TT', 'TTO', 202, 0.000, 1),
(214, 'Tunisia', 'TN', 'TUN', 203, 0.000, 1),
(215, 'Turkey', 'TR', 'TUR', 204, 0.000, 1),
(216, 'Turkmenistan', 'TM', 'TKM', 205, 0.000, 1),
(217, 'Turks and Caicos Islands', 'TC', 'TCA', 206, 0.000, 1),
(218, 'Tuvalu', 'TV', 'TUV', 207, 0.000, 1),
(219, 'Uganda', 'UG', 'UGA', 208, 0.000, 1),
(220, 'Ukraine', 'UA', 'UKR', 209, 0.000, 1),
(221, 'United Arab Emirates', 'AE', 'ARE', 210, 0.000, 1),
(222, 'United Kingdom', 'GB', 'GBR', 211, 0.000, 1),
(223, 'United States', 'US', 'USA', 1, 0.000, 1),
(224, 'US Minor Outlying Islands', 'UM', 'UMI', 213, 0.000, 1),
(225, 'Uruguay', 'UY', 'URY', 214, 0.000, 1),
(226, 'Uzbekistan', 'UZ', 'UZB', 215, 0.000, 1),
(227, 'Vanuatu', 'VU', 'VUT', 216, 0.000, 1),
(229, 'Venezuela', 'VE', 'VEN', 217, 0.000, 1),
(230, 'Viet Nam', 'VN', 'VNM', 218, 0.000, 1),
(231, 'Virgin Islands (British)', 'VG', 'VGB', 219, 0.000, 1),
(232, 'Virgin Islands (U.S.)', 'VI', 'VIR', 220, 0.000, 1),
(233, 'Wallis and Futuna Islands', 'WF', 'WLF', 221, 0.000, 1),
(234, 'Western Sahara', 'EH', 'ESH', 222, 0.000, 1),
(235, 'Yemen', 'YE', 'YEM', 223, 0.000, 1),
(236, 'Yugoslavia', 'YU', 'YUG', 224, 0.000, 1),
(237, 'Zaire', 'ZR', 'ZAR', 225, 0.000, 1),
(238, 'Zambia', 'ZM', 'ZMB', 226, 0.000, 1),
(239, 'Zimbabwe', 'ZW', 'ZWE', 227, 0.000, 1),
(240, 'Slovakia', 'SK', 'SVK', 182, 0.000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ec_customfield`
--

CREATE TABLE IF NOT EXISTS `ec_customfield` (
  `customfield_id` int(11) NOT NULL AUTO_INCREMENT,
  `table_name` varchar(30) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `field_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `field_label` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`customfield_id`),
  UNIQUE KEY `customfield_id` (`customfield_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_customfielddata`
--

CREATE TABLE IF NOT EXISTS `ec_customfielddata` (
  `customfielddata_id` int(11) NOT NULL AUTO_INCREMENT,
  `customfield_id` int(11) DEFAULT NULL,
  `table_id` int(11) NOT NULL,
  `data` blob NOT NULL,
  PRIMARY KEY (`customfielddata_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_download`
--

CREATE TABLE IF NOT EXISTS `ec_download` (
  `download_id` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `download_count` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `download_file_name` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `is_amazon_download` tinyint(1) NOT NULL DEFAULT '0',
  `amazon_key` varchar(1024) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`download_id`),
  KEY `download_order_id` (`order_id`),
  KEY `download_product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ec_giftcard`
--

CREATE TABLE IF NOT EXISTS `ec_giftcard` (
  `giftcard_id` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `amount` float(15,3) NOT NULL DEFAULT '0.000',
  `message` text COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`giftcard_id`),
  UNIQUE KEY `giftcard_id` (`giftcard_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ec_live_rate_cache`
--

CREATE TABLE IF NOT EXISTS `ec_live_rate_cache` (
  `live_rate_cache_id` int(11) NOT NULL AUTO_INCREMENT,
  `ec_cart_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `rate_data` text COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`live_rate_cache_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_manufacturer`
--

CREATE TABLE IF NOT EXISTS `ec_manufacturer` (
  `manufacturer_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_demo_item` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `clicks` int(11) NOT NULL DEFAULT '0',
  `post_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`manufacturer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ec_manufacturer`
--

INSERT INTO `ec_manufacturer` (`manufacturer_id`, `is_demo_item`, `name`, `clicks`, `post_id`) VALUES
(1, 0, 'Oz Production Event Services, LLC', 0, 4);

-- --------------------------------------------------------

--
-- Table structure for table `ec_menulevel1`
--

CREATE TABLE IF NOT EXISTS `ec_menulevel1` (
  `menulevel1_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_demo_item` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `clicks` int(11) NOT NULL DEFAULT '0',
  `seo_keywords` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `seo_description` blob,
  `banner_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`menulevel1_id`),
  UNIQUE KEY `menu1_menulevel1_id` (`menulevel1_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_menulevel2`
--

CREATE TABLE IF NOT EXISTS `ec_menulevel2` (
  `menulevel2_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_demo_item` tinyint(1) NOT NULL DEFAULT '0',
  `menulevel1_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `clicks` int(11) NOT NULL DEFAULT '0',
  `seo_keywords` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `seo_description` blob,
  `banner_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`menulevel2_id`),
  UNIQUE KEY `menu2_menulevel2_id` (`menulevel2_id`),
  KEY `menu2_menulevel1_id` (`menulevel1_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_menulevel3`
--

CREATE TABLE IF NOT EXISTS `ec_menulevel3` (
  `menulevel3_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_demo_item` tinyint(1) NOT NULL DEFAULT '0',
  `menulevel2_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `clicks` int(11) NOT NULL DEFAULT '0',
  `seo_keywords` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `seo_description` blob,
  `banner_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`menulevel3_id`),
  UNIQUE KEY `menu3_menulevel3_id` (`menulevel3_id`),
  KEY `menu3_menulevel2_id` (`menulevel2_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_option`
--

CREATE TABLE IF NOT EXISTS `ec_option` (
  `option_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_demo_item` tinyint(1) NOT NULL DEFAULT '0',
  `option_name` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_label` text COLLATE utf8mb4_unicode_520_ci,
  `option_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'combo',
  `option_required` tinyint(1) NOT NULL DEFAULT '1',
  `option_error_text` text COLLATE utf8mb4_unicode_520_ci,
  `option_meta` text COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_option_id` (`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_optionitem`
--

CREATE TABLE IF NOT EXISTS `ec_optionitem` (
  `optionitem_id` int(11) NOT NULL AUTO_INCREMENT,
  `option_id` int(11) NOT NULL DEFAULT '0',
  `optionitem_name` text COLLATE utf8mb4_unicode_520_ci,
  `optionitem_price` float(15,3) NOT NULL DEFAULT '0.000',
  `optionitem_price_onetime` float(15,3) NOT NULL DEFAULT '0.000',
  `optionitem_price_override` float(15,3) NOT NULL DEFAULT '-1.000',
  `optionitem_price_multiplier` float(15,3) NOT NULL DEFAULT '0.000',
  `optionitem_price_per_character` float(15,3) NOT NULL DEFAULT '0.000',
  `optionitem_weight` float(15,3) NOT NULL DEFAULT '0.000',
  `optionitem_weight_onetime` float(15,3) NOT NULL DEFAULT '0.000',
  `optionitem_weight_override` float(15,3) NOT NULL DEFAULT '-1.000',
  `optionitem_weight_multiplier` float(15,3) NOT NULL DEFAULT '0.000',
  `optionitem_order` int(11) NOT NULL DEFAULT '1',
  `optionitem_icon` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_initial_value` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_model_number` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_allow_download` tinyint(1) NOT NULL DEFAULT '1',
  `optionitem_disallow_shipping` tinyint(1) NOT NULL DEFAULT '0',
  `optionitem_initially_selected` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`optionitem_id`),
  KEY `option_id` (`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_optionitemimage`
--

CREATE TABLE IF NOT EXISTS `ec_optionitemimage` (
  `optionitemimage_id` int(11) NOT NULL AUTO_INCREMENT,
  `optionitem_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `image1` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `image2` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `image3` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `image4` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `image5` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`optionitemimage_id`),
  KEY `optionitem_id` (`optionitem_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_optionitemquantity`
--

CREATE TABLE IF NOT EXISTS `ec_optionitemquantity` (
  `optionitemquantity_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(17) NOT NULL DEFAULT '0',
  `optionitem_id_1` int(11) NOT NULL DEFAULT '0',
  `optionitem_id_2` int(11) NOT NULL DEFAULT '0',
  `optionitem_id_3` int(11) NOT NULL DEFAULT '0',
  `optionitem_id_4` int(11) NOT NULL DEFAULT '0',
  `optionitem_id_5` int(11) NOT NULL DEFAULT '0',
  `quantity` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`optionitemquantity_id`),
  UNIQUE KEY `optionitemquantity_id` (`optionitemquantity_id`),
  KEY `product_id` (`product_id`),
  KEY `optionitem_id_1` (`optionitem_id_1`),
  KEY `optionitem_id_2` (`optionitem_id_2`),
  KEY `optionitem_id_3` (`optionitem_id_3`),
  KEY `optionitem_id_4` (`optionitem_id_4`),
  KEY `optionitem_id_5` (`optionitem_id_5`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_option_to_product`
--

CREATE TABLE IF NOT EXISTS `ec_option_to_product` (
  `option_to_product_id` int(11) NOT NULL AUTO_INCREMENT,
  `option_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `role_label` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'all',
  `option_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`option_to_product_id`),
  UNIQUE KEY `option_to_product_id` (`option_to_product_id`),
  KEY `option_id` (`option_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_order`
--

CREATE TABLE IF NOT EXISTS `ec_order` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_demo_item` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `user_email` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_level` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'shopper',
  `last_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `order_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `orderstatus_id` int(11) NOT NULL DEFAULT '5',
  `order_weight` float(15,3) NOT NULL DEFAULT '0.000',
  `sub_total` float(15,3) NOT NULL DEFAULT '0.000',
  `tax_total` float(15,3) NOT NULL DEFAULT '0.000',
  `shipping_total` float(15,3) NOT NULL DEFAULT '0.000',
  `discount_total` float(15,3) NOT NULL DEFAULT '0.000',
  `vat_total` float(15,3) NOT NULL DEFAULT '0.000',
  `vat_rate` float(15,3) NOT NULL DEFAULT '0.000',
  `duty_total` float(15,3) NOT NULL DEFAULT '0.000',
  `gst_total` float(15,3) NOT NULL DEFAULT '0.000',
  `gst_rate` float(15,3) NOT NULL DEFAULT '0.000',
  `pst_total` float(15,3) NOT NULL DEFAULT '0.000',
  `pst_rate` float(15,3) NOT NULL DEFAULT '0.000',
  `hst_total` float(15,3) NOT NULL DEFAULT '0.000',
  `hst_rate` float(15,3) NOT NULL DEFAULT '0.000',
  `grand_total` float(15,3) NOT NULL DEFAULT '0.000',
  `refund_total` float(15,3) NOT NULL DEFAULT '0.000',
  `promo_code` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `giftcard_id` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `use_expedited_shipping` tinyint(1) NOT NULL DEFAULT '0',
  `shipping_method` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_carrier` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_service_code` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tracking_number` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_first_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_last_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_address_line_1` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_address_line_2` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_city` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_state` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_country` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_zip` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_phone` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_first_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_last_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_address_line_1` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_address_line_2` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_city` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_state` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_country` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_zip` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_phone` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `vat_registration_number` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `payment_method` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `paypal_email_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `paypal_transaction_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `paypal_payer_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `order_viewed` tinyint(1) NOT NULL DEFAULT '0',
  `order_notes` text COLLATE utf8mb4_unicode_520_ci,
  `order_customer_notes` blob,
  `txn_id` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `payment_txn_id` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `edit_sequence` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `quickbooks_status` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Not Queued',
  `credit_memo_txn_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `card_holder_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `creditcard_digits` varchar(4) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `cc_exp_month` varchar(2) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `cc_exp_year` varchar(4) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fraktjakt_order_id` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fraktjakt_shipment_id` varchar(20) COLLATE utf8mb4_unicode_520_ci DEFAULT '',
  `stripe_charge_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `nets_transaction_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `subscription_id` int(11) NOT NULL DEFAULT '0',
  `order_gateway` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `affirm_charge_id` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_company_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_company_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `guest_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `agreed_to_terms` tinyint(1) NOT NULL DEFAULT '0',
  `order_ip_address` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `gateway_transaction_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`order_id`),
  UNIQUE KEY `order_id` (`order_id`),
  KEY `user_id` (`user_id`),
  KEY `giftcard_id` (`giftcard_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ec_order`
--

INSERT INTO `ec_order` (`order_id`, `is_demo_item`, `user_id`, `user_email`, `user_level`, `last_updated`, `order_date`, `orderstatus_id`, `order_weight`, `sub_total`, `tax_total`, `shipping_total`, `discount_total`, `vat_total`, `vat_rate`, `duty_total`, `gst_total`, `gst_rate`, `pst_total`, `pst_rate`, `hst_total`, `hst_rate`, `grand_total`, `refund_total`, `promo_code`, `giftcard_id`, `use_expedited_shipping`, `shipping_method`, `shipping_carrier`, `shipping_service_code`, `tracking_number`, `billing_first_name`, `billing_last_name`, `billing_address_line_1`, `billing_address_line_2`, `billing_city`, `billing_state`, `billing_country`, `billing_zip`, `billing_phone`, `shipping_first_name`, `shipping_last_name`, `shipping_address_line_1`, `shipping_address_line_2`, `shipping_city`, `shipping_state`, `shipping_country`, `shipping_zip`, `shipping_phone`, `vat_registration_number`, `payment_method`, `paypal_email_id`, `paypal_transaction_id`, `paypal_payer_id`, `order_viewed`, `order_notes`, `order_customer_notes`, `txn_id`, `payment_txn_id`, `edit_sequence`, `quickbooks_status`, `credit_memo_txn_id`, `card_holder_name`, `creditcard_digits`, `cc_exp_month`, `cc_exp_year`, `fraktjakt_order_id`, `fraktjakt_shipment_id`, `stripe_charge_id`, `nets_transaction_id`, `subscription_id`, `order_gateway`, `affirm_charge_id`, `billing_company_name`, `shipping_company_name`, `guest_key`, `agreed_to_terms`, `order_ip_address`, `gateway_transaction_id`) VALUES
(1, 0, 0, 'mamunbdaiub@gmail.com', '', '2018-05-01 13:56:41', '2018-05-01 13:56:41', 14, 0.000, 120.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 120.000, 0.000, '', '', 0, '', '', '', '', 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'AL', 'US', '10991', '43535353', 'Abdullah', 'Mamun', 'NY, Newyork, USA', '', 'Kansas', 'AL', 'US', '10991', '43535353', '', 'manual_bill', '', '', '', 1, NULL, '', '', '', '', 'Not Queued', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', 'LPTLRAKCVGDMOAFCWOOWLPMIHIVJCM', 0, '::1', '');

-- --------------------------------------------------------

--
-- Table structure for table `ec_orderdetail`
--

CREATE TABLE IF NOT EXISTS `ec_orderdetail` (
  `orderdetail_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `model_number` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `order_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `unit_price` float(15,3) NOT NULL DEFAULT '0.000',
  `total_price` float(15,3) NOT NULL DEFAULT '0.000',
  `quantity` int(11) NOT NULL DEFAULT '0',
  `image1` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `optionitem_id_1` int(11) NOT NULL DEFAULT '0',
  `optionitem_id_2` int(11) NOT NULL DEFAULT '0',
  `optionitem_id_3` int(11) NOT NULL DEFAULT '0',
  `optionitem_id_4` int(11) NOT NULL DEFAULT '0',
  `optionitem_id_5` int(11) NOT NULL DEFAULT '0',
  `optionitem_name_1` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_name_2` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_name_3` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_name_4` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_name_5` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_label_1` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_label_2` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_label_3` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_label_4` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_label_5` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_price_1` float(15,3) NOT NULL DEFAULT '0.000',
  `optionitem_price_2` float(15,3) NOT NULL DEFAULT '0.000',
  `optionitem_price_3` float(15,3) NOT NULL DEFAULT '0.000',
  `optionitem_price_4` float(15,3) NOT NULL DEFAULT '0.000',
  `optionitem_price_5` float(15,3) NOT NULL DEFAULT '0.000',
  `use_advanced_optionset` tinyint(1) NOT NULL DEFAULT '0',
  `giftcard_id` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipper_id` int(11) DEFAULT '0',
  `shipper_first_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipper_last_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `gift_card_message` text COLLATE utf8mb4_unicode_520_ci,
  `gift_card_from_name` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `gift_card_to_name` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `is_download` tinyint(1) NOT NULL DEFAULT '0',
  `is_giftcard` tinyint(1) NOT NULL DEFAULT '0',
  `is_taxable` tinyint(1) NOT NULL DEFAULT '1',
  `is_shippable` tinyint(1) NOT NULL DEFAULT '1',
  `download_file_name` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `download_key` text COLLATE utf8mb4_unicode_520_ci,
  `maximum_downloads_allowed` int(11) NOT NULL DEFAULT '0',
  `download_timelimit_seconds` int(11) DEFAULT '0',
  `is_amazon_download` tinyint(1) NOT NULL DEFAULT '0',
  `amazon_key` varchar(1024) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `is_deconetwork` tinyint(1) NOT NULL DEFAULT '0',
  `deconetwork_id` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_product_code` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_options` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_color_code` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_product_id` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_image_link` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `gift_card_email` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `include_code` tinyint(1) NOT NULL DEFAULT '0',
  `subscription_signup_fee` float(15,3) NOT NULL DEFAULT '0.000',
  `stock_adjusted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`orderdetail_id`),
  UNIQUE KEY `orderdetail_id` (`orderdetail_id`),
  KEY `orderdetail_order_id` (`order_id`),
  KEY `orderdetail_product_id` (`product_id`),
  KEY `orderdetail_giftcard_id` (`giftcard_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ec_orderdetail`
--

INSERT INTO `ec_orderdetail` (`orderdetail_id`, `order_id`, `product_id`, `title`, `model_number`, `order_date`, `unit_price`, `total_price`, `quantity`, `image1`, `optionitem_id_1`, `optionitem_id_2`, `optionitem_id_3`, `optionitem_id_4`, `optionitem_id_5`, `optionitem_name_1`, `optionitem_name_2`, `optionitem_name_3`, `optionitem_name_4`, `optionitem_name_5`, `optionitem_label_1`, `optionitem_label_2`, `optionitem_label_3`, `optionitem_label_4`, `optionitem_label_5`, `optionitem_price_1`, `optionitem_price_2`, `optionitem_price_3`, `optionitem_price_4`, `optionitem_price_5`, `use_advanced_optionset`, `giftcard_id`, `shipper_id`, `shipper_first_name`, `shipper_last_name`, `gift_card_message`, `gift_card_from_name`, `gift_card_to_name`, `is_download`, `is_giftcard`, `is_taxable`, `is_shippable`, `download_file_name`, `download_key`, `maximum_downloads_allowed`, `download_timelimit_seconds`, `is_amazon_download`, `amazon_key`, `is_deconetwork`, `deconetwork_id`, `deconetwork_name`, `deconetwork_product_code`, `deconetwork_options`, `deconetwork_color_code`, `deconetwork_product_id`, `deconetwork_image_link`, `gift_card_email`, `include_code`, `subscription_signup_fee`, `stock_adjusted`) VALUES
(1, 1, 2, 'Table', 'table', '0000-00-00 00:00:00', 10.000, 120.000, 3, 'http://localhost/oz_production/public_html/wp-content/uploads/2018/04/table.png', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', 0.000, 0.000, 0.000, 0.000, 0.000, 0, '0', 0, '', '', '', '0', '', 0, 0, 0, 1, '', '0', 0, 0, 0, '', 0, '', '', '', '', '', '', '', '', 0, 0.000, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ec_orderstatus`
--

CREATE TABLE IF NOT EXISTS `ec_orderstatus` (
  `status_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_status` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `is_approved` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`status_id`),
  UNIQUE KEY `orderstatus_status_id` (`status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=20 ;

--
-- Dumping data for table `ec_orderstatus`
--

INSERT INTO `ec_orderstatus` (`status_id`, `order_status`, `is_approved`) VALUES
(1, 'Status Not Found', 0),
(2, 'Order Shipped', 1),
(3, 'Order Confirmed', 1),
(4, 'Order on Hold', 0),
(5, 'Order Started', 0),
(6, 'Card Approved', 1),
(7, 'Card Denied', 0),
(8, 'Third Party Pending', 0),
(9, 'Third Party Error', 0),
(10, 'Third Party Approved', 1),
(11, 'Ready for Pickup', 1),
(12, 'Pending Approval', 0),
(14, 'Direct Deposit Pending', 0),
(15, 'Direct Deposit Received', 1),
(16, 'Refunded Order', 0),
(17, 'Partial Refund', 1),
(18, 'Order Picked Up', 1),
(19, 'Order Cancelled', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ec_order_option`
--

CREATE TABLE IF NOT EXISTS `ec_order_option` (
  `order_option_id` int(11) NOT NULL AUTO_INCREMENT,
  `orderdetail_id` int(11) NOT NULL DEFAULT '0',
  `option_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_name` text COLLATE utf8mb4_unicode_520_ci,
  `option_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'combo',
  `option_value` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `option_price_change` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_allow_download` tinyint(1) NOT NULL DEFAULT '1',
  `option_label` text COLLATE utf8mb4_unicode_520_ci,
  `option_to_product_id` int(11) NOT NULL DEFAULT '0',
  `option_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`order_option_id`),
  UNIQUE KEY `order_option_id` (`order_option_id`),
  KEY `orderdetail_id` (`orderdetail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_pageoption`
--

CREATE TABLE IF NOT EXISTS `ec_pageoption` (
  `pageoption_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL DEFAULT '0',
  `option_type` varchar(155) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`pageoption_id`),
  UNIQUE KEY `pageoption_id` (`pageoption_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_perpage`
--

CREATE TABLE IF NOT EXISTS `ec_perpage` (
  `perpage_id` int(11) NOT NULL AUTO_INCREMENT,
  `perpage` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`perpage_id`),
  UNIQUE KEY `perpageid` (`perpage_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `ec_perpage`
--

INSERT INTO `ec_perpage` (`perpage_id`, `perpage`) VALUES
(1, 50),
(2, 25),
(3, 10);

-- --------------------------------------------------------

--
-- Table structure for table `ec_pricepoint`
--

CREATE TABLE IF NOT EXISTS `ec_pricepoint` (
  `pricepoint_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_less_than` tinyint(1) NOT NULL DEFAULT '0',
  `is_greater_than` tinyint(1) NOT NULL DEFAULT '0',
  `low_point` float(15,3) NOT NULL DEFAULT '0.000',
  `high_point` float(15,3) DEFAULT '0.000',
  `pricepoint_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pricepoint_id`),
  UNIQUE KEY `pricepoint_pricepoint_id` (`pricepoint_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `ec_pricepoint`
--

INSERT INTO `ec_pricepoint` (`pricepoint_id`, `is_less_than`, `is_greater_than`, `low_point`, `high_point`, `pricepoint_order`) VALUES
(1, 1, 0, 0.000, 10.000, 0),
(2, 0, 0, 25.000, 49.990, 4),
(3, 0, 0, 50.000, 99.990, 5),
(4, 0, 0, 100.000, 299.990, 6),
(5, 0, 2, 299.990, 0.000, 7),
(6, 0, 0, 10.000, 14.990, 1),
(7, 0, 0, 15.000, 19.990, 2),
(8, 0, 0, 20.000, 24.990, 3);

-- --------------------------------------------------------

--
-- Table structure for table `ec_pricetier`
--

CREATE TABLE IF NOT EXISTS `ec_pricetier` (
  `pricetier_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `price` float(15,3) NOT NULL DEFAULT '0.000',
  `quantity` int(11) NOT NULL DEFAULT '10',
  PRIMARY KEY (`pricetier_id`),
  UNIQUE KEY `pricetier_id` (`pricetier_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_product`
--

CREATE TABLE IF NOT EXISTS `ec_product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_demo_item` tinyint(1) NOT NULL DEFAULT '0',
  `model_number` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_id` int(11) NOT NULL DEFAULT '0',
  `activate_in_store` tinyint(1) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8mb4_unicode_520_ci,
  `specifications` text COLLATE utf8mb4_unicode_520_ci,
  `order_completed_note` text COLLATE utf8mb4_unicode_520_ci,
  `order_completed_email_note` text COLLATE utf8mb4_unicode_520_ci,
  `order_completed_details_note` text COLLATE utf8mb4_unicode_520_ci,
  `price` float(15,3) NOT NULL DEFAULT '0.000',
  `list_price` float(15,3) NOT NULL DEFAULT '0.000',
  `product_cost` float(15,3) NOT NULL DEFAULT '0.000',
  `vat_rate` float(15,3) NOT NULL DEFAULT '0.000',
  `handling_price` float(15,3) NOT NULL DEFAULT '0.000',
  `handling_price_each` float(15,3) NOT NULL DEFAULT '0.000',
  `stock_quantity` int(7) NOT NULL DEFAULT '0',
  `min_purchase_quantity` int(11) NOT NULL DEFAULT '0',
  `max_purchase_quantity` int(11) NOT NULL DEFAULT '0',
  `weight` float(15,3) NOT NULL DEFAULT '0.000',
  `width` double(15,3) NOT NULL DEFAULT '1.000',
  `height` double(15,3) NOT NULL DEFAULT '1.000',
  `length` double(15,3) NOT NULL DEFAULT '1.000',
  `seo_description` text COLLATE utf8mb4_unicode_520_ci,
  `seo_keywords` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `use_specifications` tinyint(1) NOT NULL DEFAULT '0',
  `use_customer_reviews` tinyint(1) NOT NULL DEFAULT '0',
  `manufacturer_id` int(11) NOT NULL DEFAULT '0',
  `download_file_name` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `image1` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `image2` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `image3` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `image4` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `image5` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_id_1` int(11) NOT NULL DEFAULT '0',
  `option_id_2` int(11) NOT NULL DEFAULT '0',
  `option_id_3` int(11) NOT NULL DEFAULT '0',
  `option_id_4` int(11) NOT NULL DEFAULT '0',
  `option_id_5` int(11) NOT NULL DEFAULT '0',
  `use_advanced_optionset` tinyint(1) NOT NULL DEFAULT '0',
  `menulevel1_id_1` int(11) NOT NULL DEFAULT '0',
  `menulevel1_id_2` int(11) NOT NULL DEFAULT '0',
  `menulevel1_id_3` int(11) NOT NULL DEFAULT '0',
  `menulevel2_id_1` int(11) NOT NULL DEFAULT '0',
  `menulevel2_id_2` int(11) NOT NULL DEFAULT '0',
  `menulevel2_id_3` int(11) NOT NULL DEFAULT '0',
  `menulevel3_id_1` int(11) NOT NULL DEFAULT '0',
  `menulevel3_id_2` int(11) NOT NULL DEFAULT '0',
  `menulevel3_id_3` int(11) NOT NULL DEFAULT '0',
  `featured_product_id_1` int(11) NOT NULL DEFAULT '0',
  `featured_product_id_2` int(11) NOT NULL DEFAULT '0',
  `featured_product_id_3` int(11) NOT NULL DEFAULT '0',
  `featured_product_id_4` int(11) NOT NULL DEFAULT '0',
  `is_giftcard` tinyint(1) NOT NULL DEFAULT '0',
  `is_download` tinyint(1) NOT NULL DEFAULT '0',
  `is_donation` tinyint(1) NOT NULL DEFAULT '0',
  `is_special` tinyint(1) NOT NULL DEFAULT '0',
  `is_taxable` tinyint(1) NOT NULL DEFAULT '1',
  `is_shippable` tinyint(1) NOT NULL DEFAULT '1',
  `is_subscription_item` tinyint(1) NOT NULL DEFAULT '0',
  `is_preorder` tinyint(1) NOT NULL DEFAULT '0',
  `role_id` tinyint(1) NOT NULL DEFAULT '0',
  `added_to_db_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `show_on_startup` tinyint(1) NOT NULL DEFAULT '0',
  `use_optionitem_images` tinyint(1) NOT NULL DEFAULT '0',
  `use_optionitem_quantity_tracking` tinyint(1) NOT NULL DEFAULT '0',
  `views` int(11) NOT NULL DEFAULT '0',
  `last_viewed` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `show_stock_quantity` tinyint(1) NOT NULL DEFAULT '1',
  `maximum_downloads_allowed` int(11) NOT NULL DEFAULT '0',
  `download_timelimit_seconds` int(11) NOT NULL DEFAULT '0',
  `list_id` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `edit_sequence` varchar(55) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `quickbooks_status` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Not Queued',
  `income_account_ref` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Online Sales',
  `cogs_account_ref` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Cost of Goods Sold',
  `asset_account_ref` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Inventory Asset',
  `quickbooks_parent_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `quickbooks_parent_list_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `subscription_bill_length` int(11) NOT NULL DEFAULT '1',
  `subscription_bill_period` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'M',
  `subscription_bill_duration` int(11) NOT NULL DEFAULT '0',
  `trial_period_days` int(11) NOT NULL DEFAULT '0',
  `stripe_plan_added` tinyint(1) NOT NULL DEFAULT '0',
  `subscription_plan_id` int(11) NOT NULL DEFAULT '0',
  `allow_multiple_subscription_purchases` tinyint(1) NOT NULL DEFAULT '1',
  `membership_page` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `is_amazon_download` tinyint(1) NOT NULL DEFAULT '0',
  `amazon_key` varchar(1024) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `catalog_mode` tinyint(1) NOT NULL DEFAULT '0',
  `catalog_mode_phrase` varchar(1024) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `inquiry_mode` tinyint(1) NOT NULL DEFAULT '0',
  `inquiry_url` varchar(1024) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `is_deconetwork` tinyint(1) NOT NULL DEFAULT '0',
  `deconetwork_mode` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'designer',
  `deconetwork_product_id` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_size_id` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_color_id` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_design_id` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `short_description` text COLLATE utf8mb4_unicode_520_ci,
  `display_type` int(11) NOT NULL DEFAULT '1',
  `image_hover_type` int(11) NOT NULL DEFAULT '3',
  `tag_type` int(11) NOT NULL DEFAULT '0',
  `tag_bg_color` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tag_text_color` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tag_text` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `image_effect_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'none',
  `include_code` tinyint(1) NOT NULL DEFAULT '0',
  `TIC` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '00000',
  `subscription_signup_fee` float(15,3) NOT NULL DEFAULT '0.000',
  `subscription_unique_id` int(11) NOT NULL DEFAULT '0',
  `subscription_prorate` tinyint(1) NOT NULL DEFAULT '1',
  `allow_backorders` tinyint(1) NOT NULL DEFAULT '0',
  `backorder_fill_date` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_class_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_id`),
  UNIQUE KEY `product_product_id` (`product_id`),
  UNIQUE KEY `product_model_number` (`model_number`(191)),
  KEY `product_menulevel1_id_1` (`menulevel1_id_1`,`menulevel2_id_1`,`menulevel3_id_1`),
  KEY `product_menulevel1_id_2` (`menulevel1_id_2`,`menulevel2_id_2`,`menulevel3_id_2`),
  KEY `product_menulevel1_id_3` (`menulevel1_id_3`,`menulevel2_id_3`,`menulevel3_id_3`),
  KEY `product_manufacturer_id` (`manufacturer_id`),
  KEY `product_option_id_1` (`option_id_1`),
  KEY `product_option_id_2` (`option_id_2`),
  KEY `product_option_id_3` (`option_id_3`),
  KEY `product_option_id_4` (`option_id_4`),
  KEY `product_option_id_5` (`option_id_5`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `ec_product`
--

INSERT INTO `ec_product` (`product_id`, `is_demo_item`, `model_number`, `post_id`, `activate_in_store`, `title`, `description`, `specifications`, `order_completed_note`, `order_completed_email_note`, `order_completed_details_note`, `price`, `list_price`, `product_cost`, `vat_rate`, `handling_price`, `handling_price_each`, `stock_quantity`, `min_purchase_quantity`, `max_purchase_quantity`, `weight`, `width`, `height`, `length`, `seo_description`, `seo_keywords`, `use_specifications`, `use_customer_reviews`, `manufacturer_id`, `download_file_name`, `image1`, `image2`, `image3`, `image4`, `image5`, `option_id_1`, `option_id_2`, `option_id_3`, `option_id_4`, `option_id_5`, `use_advanced_optionset`, `menulevel1_id_1`, `menulevel1_id_2`, `menulevel1_id_3`, `menulevel2_id_1`, `menulevel2_id_2`, `menulevel2_id_3`, `menulevel3_id_1`, `menulevel3_id_2`, `menulevel3_id_3`, `featured_product_id_1`, `featured_product_id_2`, `featured_product_id_3`, `featured_product_id_4`, `is_giftcard`, `is_download`, `is_donation`, `is_special`, `is_taxable`, `is_shippable`, `is_subscription_item`, `is_preorder`, `role_id`, `added_to_db_date`, `show_on_startup`, `use_optionitem_images`, `use_optionitem_quantity_tracking`, `views`, `last_viewed`, `show_stock_quantity`, `maximum_downloads_allowed`, `download_timelimit_seconds`, `list_id`, `edit_sequence`, `quickbooks_status`, `income_account_ref`, `cogs_account_ref`, `asset_account_ref`, `quickbooks_parent_name`, `quickbooks_parent_list_id`, `subscription_bill_length`, `subscription_bill_period`, `subscription_bill_duration`, `trial_period_days`, `stripe_plan_added`, `subscription_plan_id`, `allow_multiple_subscription_purchases`, `membership_page`, `is_amazon_download`, `amazon_key`, `catalog_mode`, `catalog_mode_phrase`, `inquiry_mode`, `inquiry_url`, `is_deconetwork`, `deconetwork_mode`, `deconetwork_product_id`, `deconetwork_size_id`, `deconetwork_color_id`, `deconetwork_design_id`, `short_description`, `display_type`, `image_hover_type`, `tag_type`, `tag_bg_color`, `tag_text_color`, `tag_text`, `image_effect_type`, `include_code`, `TIC`, `subscription_signup_fee`, `subscription_unique_id`, `subscription_prorate`, `allow_backorders`, `backorder_fill_date`, `shipping_class_id`) VALUES
(1, 0, 'Chair', 9, 1, 'Chair', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis mattis fringilla lectus et blandit. Integer sit amet nulla sed mi porttitor tincidunt a non ligula. Quisque blandit dui odio, sed consectetur erat malesuada ac. Curabitur placerat orci magna, id malesuada massa iaculis id. Sed a felis id enim euismod tristique varius in libero. Sed tempus tristique felis ac vehicula. Vestibulum viverra, velit non aliquet rhoncus, odio dolor placerat dui, vel porttitor sem arcu ac massa. Mauris elementum, lacus sollicitudin dictum faucibus, magna enim aliquam libero, at pharetra justo quam eget arcu. Donec ut porttitor metus, luctus egestas dolor. Cras lacinia condimentum nunc fermentum vulputate. Donec feugiat auctor purus. Suspendisse lobortis diam eget pellentesque cursus. Proin non dictum lorem, porttitor tempor nisi.</p><p>Integer auctor lobortis nibh, a imperdiet dolor pulvinar a. Donec tempor aliquet mauris, vitae feugiat sapien dapibus sit amet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget libero sed arcu efficitur iaculis in ut quam. Fusce nisl tellus, ultrices vitae ex id, interdum porttitor diam. Aliquam et velit tellus. Vivamus varius nec lacus id interdum. Mauris sollicitudin orci eget lectus porta tincidunt. Phasellus suscipit, justo et ultricies tristique, eros libero maximus lacus, in pulvinar lectus arcu eu eros. Mauris nec convallis ligula, dictum dignissim lacus.</p>', NULL, NULL, NULL, NULL, 1.750, 0.000, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0.000, 1.000, 1.000, 1.000, NULL, '', 0, 0, 1, '', 'http://localhost/oz_production/public_html/wp-content/uploads/2018/04/chair.png', '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, '2018-04-30 14:23:26', 1, 0, 0, 11, '0000-00-00 00:00:00', 0, 0, 0, '', '', 'Not Queued', 'Online Sales', 'Cost of Goods Sold', 'Inventory Asset', '', '', 1, 'M', 0, 0, 0, 0, 1, '', 0, '', 0, NULL, 0, NULL, 0, 'designer', '', '', '', '', NULL, 1, 3, 0, '', '', '', 'none', 0, '00000', 0.000, 0, 1, 0, '', 0),
(2, 0, 'table', 28, 1, 'Table', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis mattis fringilla lectus et blandit. Integer sit amet nulla sed mi porttitor tincidunt a non ligula. Quisque blandit dui odio, sed consectetur erat malesuada ac. Curabitur placerat orci magna, id malesuada massa iaculis id. Sed a felis id enim euismod tristique varius in libero. Sed tempus tristique felis ac vehicula. Vestibulum viverra, velit non aliquet rhoncus, odio dolor placerat dui, vel porttitor sem arcu ac massa. Mauris elementum, lacus sollicitudin dictum faucibus, magna enim aliquam libero, at pharetra justo quam eget arcu. Donec ut porttitor metus, luctus egestas dolor. Cras lacinia condimentum nunc fermentum vulputate. Donec feugiat auctor purus. Suspendisse lobortis diam eget pellentesque cursus. Proin non dictum lorem, porttitor tempor nisi.</p><p>Integer auctor lobortis nibh, a imperdiet dolor pulvinar a. Donec tempor aliquet mauris, vitae feugiat sapien dapibus sit amet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget libero sed arcu efficitur iaculis in ut quam. Fusce nisl tellus, ultrices vitae ex id, interdum porttitor diam. Aliquam et velit tellus. Vivamus varius nec lacus id interdum. Mauris sollicitudin orci eget lectus porta tincidunt. Phasellus suscipit, justo et ultricies tristique, eros libero maximus lacus, in pulvinar lectus arcu eu eros. Mauris nec convallis ligula, dictum dignissim lacus.</p>', NULL, NULL, NULL, NULL, 10.000, 0.000, 0.000, 0.000, 0.000, 0.000, -5, 0, 0, 0.000, 1.000, 1.000, 1.000, NULL, '', 0, 0, 1, '', 'http://localhost/oz_production/public_html/wp-content/uploads/2018/04/table.png', '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, '2018-04-30 15:09:54', 1, 0, 0, 14, '0000-00-00 00:00:00', 0, 0, 0, '', '', 'Not Queued', 'Online Sales', 'Cost of Goods Sold', 'Inventory Asset', '', '', 1, 'M', 0, 0, 0, 0, 1, '', 0, '', 0, NULL, 0, NULL, 0, 'designer', '', '', '', '', NULL, 1, 3, 0, '', '', '', 'none', 0, '00000', 0.000, 0, 1, 0, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ec_product_google_attributes`
--

CREATE TABLE IF NOT EXISTS `ec_product_google_attributes` (
  `product_google_attribute_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `attribute_value` text COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`product_google_attribute_id`),
  UNIQUE KEY `product_google_attribute_id` (`product_google_attribute_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_promocode`
--

CREATE TABLE IF NOT EXISTS `ec_promocode` (
  `promocode_id` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `is_dollar_based` tinyint(1) NOT NULL DEFAULT '0',
  `is_percentage_based` tinyint(1) NOT NULL DEFAULT '0',
  `is_shipping_based` tinyint(1) NOT NULL DEFAULT '0',
  `is_free_item_based` tinyint(1) NOT NULL DEFAULT '0',
  `is_for_me_based` tinyint(1) NOT NULL DEFAULT '0',
  `by_manufacturer_id` tinyint(1) NOT NULL DEFAULT '0',
  `by_category_id` tinyint(1) NOT NULL DEFAULT '0',
  `by_product_id` tinyint(1) NOT NULL DEFAULT '0',
  `by_all_products` int(11) NOT NULL DEFAULT '0',
  `promo_dollar` float(15,3) NOT NULL DEFAULT '0.000',
  `promo_percentage` float(15,3) NOT NULL DEFAULT '0.000',
  `promo_shipping` float(15,3) NOT NULL DEFAULT '0.000',
  `promo_free_item` float(15,3) NOT NULL DEFAULT '0.000',
  `promo_for_me` float(15,3) NOT NULL DEFAULT '0.000',
  `manufacturer_id` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `message` blob NOT NULL,
  `max_redemptions` int(11) NOT NULL DEFAULT '999',
  `times_redeemed` int(11) NOT NULL DEFAULT '0',
  `expiration_date` datetime DEFAULT NULL,
  `duration` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'forever',
  `duration_in_months` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`promocode_id`),
  KEY `promo_manufacturer_id` (`manufacturer_id`),
  KEY `promo_product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ec_promotion`
--

CREATE TABLE IF NOT EXISTS `ec_promotion` (
  `promotion_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `type` int(11) NOT NULL DEFAULT '0',
  `start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_date` datetime DEFAULT '0000-00-00 00:00:00',
  `product_id_1` int(11) NOT NULL DEFAULT '0',
  `product_id_2` int(11) NOT NULL DEFAULT '0',
  `product_id_3` int(11) NOT NULL DEFAULT '0',
  `manufacturer_id_1` int(11) NOT NULL DEFAULT '0',
  `manufacturer_id_2` int(11) NOT NULL DEFAULT '0',
  `manufacturer_id_3` int(11) NOT NULL DEFAULT '0',
  `category_id_1` int(11) NOT NULL DEFAULT '0',
  `category_id_2` int(11) NOT NULL DEFAULT '0',
  `category_id_3` int(11) NOT NULL DEFAULT '0',
  `price1` float(15,3) NOT NULL DEFAULT '0.000',
  `price2` float(15,3) NOT NULL DEFAULT '0.000',
  `price3` float(15,3) NOT NULL DEFAULT '0.000',
  `percentage1` float(15,3) NOT NULL DEFAULT '0.000',
  `percentage2` float(15,3) NOT NULL DEFAULT '0.000',
  `percentage3` float(15,3) NOT NULL DEFAULT '0.000',
  `number1` int(11) NOT NULL DEFAULT '0',
  `number2` int(11) NOT NULL DEFAULT '0',
  `number3` int(11) NOT NULL DEFAULT '0',
  `promo_limit` int(11) NOT NULL DEFAULT '3',
  PRIMARY KEY (`promotion_id`),
  UNIQUE KEY `promotion_promotion_id` (`promotion_id`),
  KEY `promotion_product_id_1` (`product_id_1`),
  KEY `promotion_product_id_2` (`product_id_2`),
  KEY `promotion_product_id_3` (`product_id_3`),
  KEY `promotion_manufacturer_id_1` (`manufacturer_id_1`),
  KEY `promotion_manufacturer_id_2` (`manufacturer_id_2`),
  KEY `promotion_manufacturer_id_3` (`manufacturer_id_3`),
  KEY `promotion_category_id_1` (`category_id_1`),
  KEY `promotion_category_id_2` (`category_id_2`),
  KEY `promotion_category_id_3` (`category_id_3`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_response`
--

CREATE TABLE IF NOT EXISTS `ec_response` (
  `response_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_error` tinyint(1) NOT NULL DEFAULT '0',
  `processor` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `response_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `response_text` text COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`response_id`),
  KEY `order_id` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_review`
--

CREATE TABLE IF NOT EXISTS `ec_review` (
  `review_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `rating` int(2) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` mediumblob NOT NULL,
  `date_submitted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`review_id`),
  UNIQUE KEY `review_id` (`review_id`),
  KEY `product_id` (`product_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_role`
--

CREATE TABLE IF NOT EXISTS `ec_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_label` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `admin_access` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`role_id`),
  UNIQUE KEY `role_id` (`role_id`),
  KEY `role_label` (`role_label`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `ec_role`
--

INSERT INTO `ec_role` (`role_id`, `role_label`, `admin_access`) VALUES
(1, 'admin', 1),
(2, 'shopper', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ec_roleaccess`
--

CREATE TABLE IF NOT EXISTS `ec_roleaccess` (
  `roleaccess_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_label` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `admin_panel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`roleaccess_id`),
  UNIQUE KEY `roleaccess_id` (`roleaccess_id`),
  KEY `role_label` (`role_label`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_roleprice`
--

CREATE TABLE IF NOT EXISTS `ec_roleprice` (
  `roleprice_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `role_label` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `role_price` float(15,3) NOT NULL DEFAULT '0.000',
  PRIMARY KEY (`roleprice_id`),
  UNIQUE KEY `roleprice_id` (`roleprice_id`),
  KEY `product_id` (`product_id`),
  KEY `role_label` (`role_label`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_setting`
--

CREATE TABLE IF NOT EXISTS `ec_setting` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `site_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `reg_code` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `storeversion` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `storetype` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'wordpress',
  `storepage` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'store',
  `cartpage` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'cart',
  `accountpage` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'account',
  `timezone` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Europe/London',
  `shipping_method` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'method',
  `shipping_expedite_rate` float(11,2) NOT NULL DEFAULT '0.00',
  `shipping_handling_rate` float(11,2) NOT NULL DEFAULT '0.00',
  `ups_access_license_number` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `ups_user_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `ups_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `ups_ship_from_zip` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `ups_shipper_number` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `ups_country_code` varchar(9) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'US',
  `ups_weight_type` varchar(19) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'LBS',
  `ups_conversion_rate` float(9,3) NOT NULL DEFAULT '1.000',
  `usps_user_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `usps_ship_from_zip` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fedex_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fedex_account_number` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fedex_meter_number` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fedex_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fedex_ship_from_zip` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fedex_weight_units` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'LB',
  `fedex_country_code` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'US',
  `fedex_conversion_rate` float(9,3) NOT NULL DEFAULT '1.000',
  `fedex_test_account` tinyint(1) NOT NULL DEFAULT '0',
  `auspost_api_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `auspost_ship_from_zip` varchar(55) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `dhl_site_id` varchar(155) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `dhl_password` varchar(155) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `dhl_ship_from_country` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'US',
  `dhl_ship_from_zip` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `dhl_weight_unit` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'LB',
  `dhl_test_mode` tinyint(1) NOT NULL DEFAULT '0',
  `fraktjakt_customer_id` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fraktjakt_login_key` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fraktjakt_conversion_rate` double(15,3) NOT NULL DEFAULT '1.000',
  `fraktjakt_test_mode` tinyint(1) NOT NULL DEFAULT '0',
  `fraktjakt_address` varchar(120) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fraktjakt_city` varchar(55) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fraktjakt_state` varchar(2) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fraktjakt_zip` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fraktjakt_country` varchar(2) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `ups_ship_from_state` varchar(2) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `ups_negotiated_rates` tinyint(1) NOT NULL DEFAULT '0',
  `canadapost_username` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `canadapost_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `canadapost_customer_number` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `canadapost_contract_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `canadapost_test_mode` tinyint(1) NOT NULL DEFAULT '0',
  `canadapost_ship_from_zip` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`setting_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ec_setting`
--

INSERT INTO `ec_setting` (`setting_id`, `site_url`, `reg_code`, `storeversion`, `storetype`, `storepage`, `cartpage`, `accountpage`, `timezone`, `shipping_method`, `shipping_expedite_rate`, `shipping_handling_rate`, `ups_access_license_number`, `ups_user_id`, `ups_password`, `ups_ship_from_zip`, `ups_shipper_number`, `ups_country_code`, `ups_weight_type`, `ups_conversion_rate`, `usps_user_name`, `usps_ship_from_zip`, `fedex_key`, `fedex_account_number`, `fedex_meter_number`, `fedex_password`, `fedex_ship_from_zip`, `fedex_weight_units`, `fedex_country_code`, `fedex_conversion_rate`, `fedex_test_account`, `auspost_api_key`, `auspost_ship_from_zip`, `dhl_site_id`, `dhl_password`, `dhl_ship_from_country`, `dhl_ship_from_zip`, `dhl_weight_unit`, `dhl_test_mode`, `fraktjakt_customer_id`, `fraktjakt_login_key`, `fraktjakt_conversion_rate`, `fraktjakt_test_mode`, `fraktjakt_address`, `fraktjakt_city`, `fraktjakt_state`, `fraktjakt_zip`, `fraktjakt_country`, `ups_ship_from_state`, `ups_negotiated_rates`, `canadapost_username`, `canadapost_password`, `canadapost_customer_number`, `canadapost_contract_id`, `canadapost_test_mode`, `canadapost_ship_from_zip`) VALUES
(1, 'localhost/oz_production/public_html', '', '1.0.0', 'wordpress', '6', '7', '8', 'America/Los_Angeles', 'method', 0.00, 0.00, '', '', '', '', '', '', '', 1.000, '', '', '', '', '', '', '', 'LB', 'US', 1.000, 0, '', '', '', '', '', '', '', 0, '', '', 1.000, 0, '', '', '', '', '', '', 0, '', '', '', '', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `ec_shippingrate`
--

CREATE TABLE IF NOT EXISTS `ec_shippingrate` (
  `shippingrate_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_demo_item` tinyint(1) NOT NULL DEFAULT '0',
  `zone_id` int(11) NOT NULL DEFAULT '0',
  `is_price_based` tinyint(1) NOT NULL DEFAULT '0',
  `is_weight_based` tinyint(1) NOT NULL DEFAULT '0',
  `is_method_based` tinyint(1) NOT NULL DEFAULT '0',
  `is_quantity_based` tinyint(1) NOT NULL DEFAULT '0',
  `is_percentage_based` tinyint(1) NOT NULL DEFAULT '0',
  `is_ups_based` tinyint(1) NOT NULL DEFAULT '0',
  `is_usps_based` tinyint(1) NOT NULL DEFAULT '0',
  `is_fedex_based` tinyint(1) NOT NULL DEFAULT '0',
  `is_auspost_based` tinyint(1) NOT NULL DEFAULT '0',
  `is_dhl_based` tinyint(1) NOT NULL DEFAULT '0',
  `is_canadapost_based` tinyint(1) NOT NULL DEFAULT '0',
  `trigger_rate` float(15,3) NOT NULL DEFAULT '0.000',
  `shipping_rate` float(15,3) NOT NULL DEFAULT '0.000',
  `shipping_label` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_order` int(11) NOT NULL DEFAULT '0',
  `shipping_code` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_override_rate` float(11,3) DEFAULT NULL,
  `free_shipping_at` float(15,3) NOT NULL DEFAULT '-1.000',
  PRIMARY KEY (`shippingrate_id`),
  UNIQUE KEY `shippingrate_id` (`shippingrate_id`),
  KEY `zone_id` (`zone_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=55 ;

--
-- Dumping data for table `ec_shippingrate`
--

INSERT INTO `ec_shippingrate` (`shippingrate_id`, `is_demo_item`, `zone_id`, `is_price_based`, `is_weight_based`, `is_method_based`, `is_quantity_based`, `is_percentage_based`, `is_ups_based`, `is_usps_based`, `is_fedex_based`, `is_auspost_based`, `is_dhl_based`, `is_canadapost_based`, `trigger_rate`, `shipping_rate`, `shipping_label`, `shipping_order`, `shipping_code`, `shipping_override_rate`, `free_shipping_at`) VALUES
(51, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 5.000, '', 0, '', 0.000, -1.000),
(52, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 7.990, 'Standard Shipping 7-10 Days', 1, '', NULL, -1.000),
(53, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 14.990, 'Priority 3 Day Shipping', 2, '', NULL, -1.000),
(54, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 19.990, 'Priority 2 Day Shipping', 3, '', NULL, -1.000);

-- --------------------------------------------------------

--
-- Table structure for table `ec_shipping_class`
--

CREATE TABLE IF NOT EXISTS `ec_shipping_class` (
  `shipping_class_id` int(11) NOT NULL AUTO_INCREMENT,
  `class_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`shipping_class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_shipping_class_to_rate`
--

CREATE TABLE IF NOT EXISTS `ec_shipping_class_to_rate` (
  `shipping_class_to_rate_id` int(11) NOT NULL AUTO_INCREMENT,
  `shipping_class_id` int(11) NOT NULL DEFAULT '0',
  `shipping_rate_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`shipping_class_to_rate_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_state`
--

CREATE TABLE IF NOT EXISTS `ec_state` (
  `id_sta` int(11) NOT NULL AUTO_INCREMENT,
  `idcnt_sta` int(11) NOT NULL DEFAULT '0',
  `code_sta` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `name_sta` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `group_sta` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `ship_to_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_sta`),
  KEY `idcnt_sta` (`idcnt_sta`),
  KEY `code_sta` (`code_sta`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=312 ;

--
-- Dumping data for table `ec_state`
--

INSERT INTO `ec_state` (`id_sta`, `idcnt_sta`, `code_sta`, `name_sta`, `sort_order`, `group_sta`, `ship_to_active`) VALUES
(1, 223, 'AL', 'Alabama', 9, '', 1),
(2, 223, 'AK', 'Alaska', 10, '', 1),
(4, 223, 'AZ', 'Arizona', 11, '', 1),
(5, 223, 'AR', 'Arkansas', 12, '', 1),
(12, 223, 'CA', 'California', 13, '', 1),
(13, 223, 'CO', 'Colorado', 14, '', 1),
(14, 223, 'CT', 'Connecticut', 15, '', 1),
(15, 223, 'DE', 'Delaware', 16, '', 1),
(16, 223, 'DC', 'District of Columbia', 17, '', 1),
(18, 223, 'FL', 'Florida', 18, '', 1),
(19, 223, 'GA', 'Georgia', 19, '', 1),
(21, 223, 'HI', 'Hawaii', 21, '', 1),
(22, 223, 'ID', 'Idaho', 22, '', 1),
(23, 223, 'IL', 'Illinois', 23, '', 1),
(24, 223, 'IN', 'Indiana', 24, '', 1),
(25, 223, 'IA', 'Iowa', 25, '', 1),
(26, 223, 'KS', 'Kansas', 26, '', 1),
(27, 223, 'KY', 'Kentucky', 27, '', 1),
(28, 223, 'LA', 'Louisiana', 28, '', 1),
(29, 223, 'ME', 'Maine', 29, '', 1),
(31, 223, 'MD', 'Maryland', 30, '', 1),
(32, 223, 'MA', 'Massachusetts', 31, '', 1),
(33, 223, 'MI', 'Michigan', 32, '', 1),
(34, 223, 'MN', 'Minnesota', 33, '', 1),
(35, 223, 'MS', 'Mississippi', 34, '', 1),
(36, 223, 'MO', 'Missouri', 35, '', 1),
(37, 223, 'MT', 'Montana', 36, '', 1),
(38, 223, 'NE', 'Nebraska', 37, '', 1),
(39, 223, 'NV', 'Nevada', 38, '', 1),
(40, 223, 'NH', 'New Hampshire', 39, '', 1),
(41, 223, 'NJ', 'New Jersey', 40, '', 1),
(42, 223, 'NM', 'New Mexico', 41, '', 1),
(43, 223, 'NY', 'New York', 42, '', 1),
(44, 223, 'NC', 'North Carolina', 43, '', 1),
(45, 223, 'ND', 'North Dakota', 44, '', 1),
(47, 223, 'OH', 'Ohio', 45, '', 1),
(48, 223, 'OK', 'Oklahoma', 46, '', 1),
(49, 223, 'OR', 'Oregon', 47, '', 1),
(51, 223, 'PA', 'Pennsylvania', 48, '', 1),
(52, 223, 'PR', 'Puerto Rico', 49, '', 1),
(53, 223, 'RI', 'Rhode Island', 50, '', 1),
(54, 223, 'SC', 'South Carolina', 51, '', 1),
(55, 223, 'SD', 'South Dakota', 52, '', 1),
(56, 223, 'TN', 'Tennessee', 53, '', 1),
(57, 223, 'TX', 'Texas', 54, '', 1),
(58, 223, 'UT', 'Utah', 55, '', 1),
(59, 223, 'VT', 'Vermont', 56, '', 1),
(60, 223, 'VI', 'Virgin Islands', 57, '', 1),
(61, 223, 'VA', 'Virginia', 58, '', 1),
(62, 223, 'WA', 'Washington', 59, '', 1),
(63, 223, 'WV', 'West Virginia', 60, '', 1),
(64, 223, 'WI', 'Wisconsin', 61, '', 1),
(65, 223, 'WY', 'Wyoming', 62, '', 1),
(66, 38, 'AB', 'Alberta', 100, '', 1),
(67, 38, 'BC', 'British Columbia', 101, '', 1),
(68, 38, 'MB', 'Manitoba', 102, '', 1),
(69, 38, 'NF', 'Newfoundland', 103, '', 1),
(70, 38, 'NB', 'New Brunswick', 104, '', 1),
(71, 38, 'NS', 'Nova Scotia', 105, '', 1),
(72, 38, 'NT', 'Northwest Territories', 106, '', 1),
(73, 38, 'NU', 'Nunavut', 107, '', 1),
(74, 38, 'ON', 'Ontario', 108, '', 1),
(75, 38, 'PE', 'Prince Edward Island', 109, '', 1),
(76, 38, 'QC', 'Quebec', 110, '', 1),
(77, 38, 'SK', 'Saskatchewan', 111, '', 1),
(78, 38, 'YT', 'Yukon Territory', 112, '', 1),
(79, 13, 'ACT', 'Australian Capital Territory', 113, '', 1),
(80, 13, 'CX', 'Christmas Island', 114, '', 1),
(81, 13, 'CC', 'Cocos Islands', 115, '', 1),
(82, 13, 'HM', 'Heard Island and McDonald Islands', 116, '', 1),
(83, 13, 'NSW', 'New South Wales', 117, '', 1),
(84, 13, 'NF', 'Norfolk Island', 118, '', 1),
(85, 13, 'NT', 'Northern Territory', 119, '', 1),
(86, 13, 'QLD', 'Queensland', 120, '', 1),
(87, 13, 'SA', 'South Australia', 121, '', 1),
(88, 13, 'TAS', 'Tasmania', 122, '', 1),
(89, 13, 'VIC', 'Victoria', 123, '', 1),
(90, 13, 'WA', 'Western Australia', 124, '', 1),
(91, 222, 'Avon', 'Avon', 125, 'England', 1),
(92, 222, 'Bedfordshire', 'Bedfordshire', 126, 'England', 1),
(93, 222, 'Berkshire', 'Berkshire', 127, 'England', 1),
(94, 222, 'Buckinghamshire', 'Buckinghamshire', 128, 'England', 1),
(95, 222, 'Cambridgeshire', 'Cambridgeshire', 129, 'England', 1),
(96, 222, 'Cheshire', 'Cheshire', 130, 'England', 1),
(97, 222, 'Cleveland', 'Cleveland', 131, 'England', 1),
(98, 222, 'Cornwall', 'Cornwall', 132, 'England', 1),
(99, 222, 'Cumbria', 'Cumbria', 133, 'England', 1),
(100, 222, 'Derbyshire', 'Derbyshire', 134, 'England', 1),
(101, 222, 'Devon', 'Devon', 135, 'England', 1),
(102, 222, 'Dorset', 'Dorset', 136, 'England', 1),
(103, 222, 'Durham', 'Durham', 137, 'England', 1),
(104, 222, 'East Sussex', 'East Sussex', 138, 'England', 1),
(105, 222, 'Essex', 'Essex', 139, 'England', 1),
(106, 222, 'Gloucestershire', 'Gloucestershire', 140, 'England', 1),
(107, 222, 'Hampshire', 'Hampshire', 141, 'England', 1),
(108, 222, 'Herefordshire', 'Herefordshire', 142, 'England', 1),
(109, 222, 'Hertfordshire', 'Hertfordshire', 143, 'England', 1),
(110, 222, 'Isle of Wight', 'Isle of Wight', 144, 'England', 1),
(111, 222, 'Kent', 'Kent', 145, 'England', 1),
(112, 222, 'Lancashire', 'Lancashire', 146, 'England', 1),
(113, 222, 'Leicestershire', 'Leicestershire', 147, 'England', 1),
(114, 222, 'Lincolnshire', 'Lincolnshire', 148, 'England', 1),
(115, 222, 'London', 'London', 149, 'England', 1),
(116, 222, 'Merseyside', 'Merseyside', 150, 'England', 1),
(117, 222, 'Middlesex', 'Middlesex', 151, 'England', 1),
(118, 222, 'Norfolk', 'Norfolk', 152, 'England', 1),
(119, 222, 'Northamptonshire', 'Northamptonshire', 153, 'England', 1),
(120, 222, 'Northumberland', 'Northumberland', 154, 'England', 1),
(121, 222, 'North Humberside', 'North Humberside', 155, 'England', 1),
(122, 222, 'North Yorkshire', 'North Yorkshire', 156, 'England', 1),
(123, 222, 'Nottinghamshire', 'Nottinghamshire', 157, 'England', 1),
(124, 222, 'Oxfordshire', 'Oxfordshire', 158, 'England', 1),
(125, 222, 'Rutland', 'Rutland', 159, 'England', 1),
(126, 222, 'Shropshire', 'Shropshire', 160, 'England', 1),
(127, 222, 'Somerset', 'Somerset', 161, 'England', 1),
(128, 222, 'South Humberside', 'South Humberside', 162, 'England', 1),
(129, 222, 'South Yorkshire', 'South Yorkshire', 163, 'England', 1),
(130, 222, 'Staffordshire', 'Staffordshire', 164, 'England', 1),
(131, 222, 'Suffolk', 'Suffolk', 165, 'England', 1),
(132, 222, 'Surrey', 'Surrey', 166, 'England', 1),
(133, 222, 'Tyne and Wear', 'Tyne and Wear', 167, 'England', 1),
(134, 222, 'Warwickshire', 'Warwickshire', 168, 'England', 1),
(135, 222, 'West Midlands', 'West Midlands', 169, 'England', 1),
(136, 222, 'West Sussex', 'West Sussex', 170, 'England', 1),
(137, 222, 'West Yorkshire', 'West Yorkshire', 171, 'England', 1),
(138, 222, 'Wiltshire', 'Wiltshire', 172, 'England', 1),
(139, 222, 'Worcestershire', 'Worcestershire', 173, 'England', 1),
(140, 222, 'Clwyd', 'Clwyd', 174, 'Wales', 1),
(141, 222, 'Dyfed', 'Dyfed', 175, 'Wales', 1),
(142, 222, 'Gwent', 'Gwent', 176, 'Wales', 1),
(143, 222, 'Gwynedd', 'Gwynedd', 177, 'Wales', 1),
(144, 222, 'Mid Glamorgan', 'Mid Glamorgan', 178, 'Wales', 1),
(145, 222, 'Powys', 'Powys', 179, 'Wales', 1),
(146, 222, 'South Glamorgan', 'South Glamorgan', 180, 'Wales', 1),
(147, 222, 'West Glamorgan', 'West Glamorgan', 181, 'Wales', 1),
(148, 222, 'Aberdeenshire', 'Aberdeenshire', 182, 'Scotland', 1),
(149, 222, 'Angus', 'Angus', 183, 'Scotland', 1),
(150, 222, 'Argyll', 'Argyll', 184, 'Scotland', 1),
(151, 222, 'Ayrshire', 'Ayrshire', 185, 'Scotland', 1),
(152, 222, 'Banffshire', 'Banffshire', 186, 'Scotland', 1),
(153, 222, 'Berwickshire', 'Berwickshire', 187, 'Scotland', 1),
(154, 222, 'Bute', 'Bute', 188, 'Scotland', 1),
(155, 222, 'Caithness', 'Caithness', 189, 'Scotland', 1),
(156, 222, 'Clackmannanshire', 'Clackmannanshire', 190, 'Scotland', 1),
(157, 222, 'Dumfriesshire', 'Dumfriesshire', 191, 'Scotland', 1),
(158, 222, 'Dunbartonshire', 'Dunbartonshire', 192, 'Scotland', 1),
(159, 222, 'East Lothian', 'East Lothian', 193, 'Scotland', 1),
(160, 222, 'Fife', 'Fife', 194, 'Scotland', 1),
(161, 222, 'Inverness-shire', 'Inverness-shire', 195, 'Scotland', 1),
(162, 222, 'Kincardineshire', 'Kincardineshire', 196, 'Scotland', 1),
(163, 222, 'Kinross-shire', 'Kinross-shire', 197, 'Scotland', 1),
(164, 222, 'Kirkcudbrightshire', 'Kirkcudbrightshire', 198, 'Scotland', 1),
(165, 222, 'Lanarkshire', 'Lanarkshire', 199, 'Scotland', 1),
(166, 222, 'Midlothian', 'Midlothian', 200, 'Scotland', 1),
(167, 222, 'Moray', 'Moray', 201, 'Scotland', 1),
(168, 222, 'Nairnshire', 'Nairnshire', 202, 'Scotland', 1),
(169, 222, 'Orkney', 'Orkney', 203, 'Scotland', 1),
(170, 222, 'Peeblesshire', 'Peeblesshire', 204, 'Scotland', 1),
(171, 222, 'Perthshire', 'Perthshire', 205, 'Scotland', 1),
(172, 222, 'Renfrewshire', 'Renfrewshire', 206, 'Scotland', 1),
(173, 222, 'Ross-shire', 'Ross-shire', 207, 'Scotland', 1),
(174, 222, 'Roxburghshire', 'Roxburghshire', 208, 'Scotland', 1),
(175, 222, 'Selkirkshire', 'Selkirkshire', 209, 'Scotland', 1),
(176, 222, 'Shetland', 'Shetland', 210, 'Scotland', 1),
(177, 222, 'Stirlingshire', 'Stirlingshire', 211, 'Scotland', 1),
(178, 222, 'Sutherland', 'Sutherland', 212, 'Scotland', 1),
(179, 222, 'West Lothian', 'West Lothian', 213, 'Scotland', 1),
(180, 222, 'Wigtownshire', 'Wigtownshire', 214, 'Scotland', 1),
(181, 222, 'Antrim', 'Antrim', 215, 'Northern Ireland', 1),
(182, 222, 'Down', 'Down', 217, 'Northern Ireland', 1),
(183, 222, 'Armagh', 'Armagh', 216, 'Northern Ireland', 1),
(184, 222, 'Fermanagh', 'Fermanagh', 218, 'Northern Ireland', 1),
(185, 222, 'Londonderry', 'Londonderry', 219, 'Northern Ireland', 1),
(186, 222, 'Tyrone', 'Tyrone', 220, 'Northern Ireland', 1),
(187, 30, 'AL', 'Alagoas', 221, '', 1),
(188, 30, 'AM', 'Amazonas', 222, '', 1),
(189, 30, 'BA', 'Bahia', 223, '', 1),
(190, 30, 'CE', 'Cearà', 224, '', 1),
(191, 30, 'DF', 'Distrito Federal', 225, '', 1),
(192, 30, 'ES', 'Espìrito Santo', 226, '', 1),
(193, 30, 'GO', 'Goias', 227, '', 1),
(194, 30, 'MA', 'Maranhao', 228, '', 1),
(195, 30, 'MT', 'Mato Grosso', 229, '', 1),
(196, 30, 'MS', 'Mato Grosso Do Sul', 230, '', 1),
(197, 30, 'MG', 'Minas Gerais', 231, '', 1),
(198, 30, 'PA', 'Parà', 232, '', 1),
(199, 30, 'PB', 'Paraìba', 233, '', 1),
(200, 30, 'PR', 'Paranà', 234, '', 1),
(201, 30, 'PE', 'Pernambuco', 235, '', 1),
(202, 30, 'PI', 'Piauì', 236, '', 1),
(203, 30, 'RJ', 'Rio de Janeiro', 237, '', 1),
(204, 30, 'RN', 'Rio Grande do Norte', 238, '', 1),
(205, 30, 'RS', 'Dio Grande do Sul', 239, '', 1),
(206, 30, 'RO', 'Rondônia', 240, '', 1),
(207, 30, 'SC', 'Santa Catarina', 241, '', 1),
(208, 30, 'SP', 'Sao Paulo', 242, '', 1),
(209, 30, 'SE', 'Sergipe', 243, '', 1),
(210, 44, 'ANH', 'Anhui', 244, '', 1),
(211, 44, 'BEI', 'Beijing', 245, '', 1),
(212, 44, 'CHO', 'Chongqing', 246, '', 1),
(213, 44, 'FUJ', 'Fujian', 247, '', 1),
(214, 44, 'GAN', 'Gansu', 248, '', 1),
(215, 44, 'GDG', 'Guangdong', 249, '', 1),
(216, 44, 'GXI', 'Guangxi', 250, '', 1),
(217, 44, 'GUI', 'Guizhou', 251, '', 1),
(218, 44, 'HAI', 'Hainan', 252, '', 1),
(219, 44, 'HEB', 'Hebei', 253, '', 1),
(220, 44, 'HEI', 'Heilongjiang', 254, '', 1),
(221, 44, 'HEN', 'Henan', 255, '', 1),
(222, 44, 'HUB', 'Hubei', 256, '', 1),
(223, 44, 'HUN', 'Hunan', 257, '', 1),
(224, 44, 'JSU', 'Jiangsu', 258, '', 1),
(225, 44, 'JXI', 'Jiangxi', 259, '', 1),
(226, 44, 'JIL', 'Jilin', 260, '', 1),
(227, 44, 'LIA', 'Liaoning', 261, '', 1),
(228, 44, 'MON', 'Nei Mongol', 262, '', 1),
(229, 44, 'NIN', 'Ningxia', 263, '', 1),
(230, 44, 'QIN', 'Qinghai', 264, '', 1),
(231, 44, 'SHA', 'Shaanxi', 265, '', 1),
(232, 44, 'SHD', 'Shandong', 266, '', 1),
(233, 44, 'SHH', 'Shanghai', 267, '', 1),
(234, 44, 'SHX', 'Shanxi', 268, '', 1),
(235, 44, 'SIC', 'Sichuan', 269, '', 1),
(236, 44, 'TIA', 'TIanjin', 270, '', 1),
(237, 44, 'XIN', 'Xinjiang', 271, '', 1),
(238, 44, 'XIZ', 'Xizang', 272, '', 1),
(239, 44, 'YUN', 'Yunnan', 273, '', 1),
(240, 44, 'ZHE', 'Zhejiang', 274, '', 1),
(241, 99, 'AND', 'Andhra Pradesh', 275, '', 1),
(242, 99, 'ASS', 'Assam', 276, '', 1),
(243, 99, 'BIH', 'Bihar', 277, '', 1),
(244, 99, 'CHH', 'Chhattisgarh', 278, '', 1),
(245, 99, 'DEL', 'Delhi', 279, '', 1),
(246, 99, 'GOA', 'Goa', 280, '', 1),
(247, 99, 'GUJ', 'Gujarat', 281, '', 1),
(248, 99, 'HAR', 'Haryana', 282, '', 1),
(249, 99, 'HIM', 'Himachal Pradesh', 283, '', 1),
(250, 99, 'JAM', 'Jammu & Kashmir', 284, '', 1),
(251, 99, 'JHA', 'Jharkhand', 285, '', 1),
(252, 99, 'KAR', 'Karnataka', 286, '', 1),
(253, 99, 'KER', 'Kerala', 287, '', 1),
(254, 99, 'MAD', 'Madhya Pradesh', 288, '', 1),
(255, 99, 'MAH', 'Maharashtra', 289, '', 1),
(256, 99, 'MEG', 'Meghalaya', 290, '', 1),
(257, 99, 'ORI', 'Orissa', 291, '', 1),
(258, 99, 'PON', 'Pondicherry', 292, '', 1),
(259, 99, 'PUN', 'Punjab', 293, '', 1),
(260, 99, 'RAJ', 'Rajasthan', 294, '', 1),
(261, 99, 'TAM', 'Tamil Nadu', 295, '', 1),
(262, 99, 'UTT', 'Uttar Pradesh', 296, '', 1),
(263, 99, 'UTR', 'Uttaranchal', 297, '', 1),
(264, 99, 'WES', 'West Bengal', 298, '', 1),
(265, 107, 'AIC', 'Aichi', 299, '', 1),
(266, 107, 'AKT', 'Akita', 300, '', 1),
(267, 107, 'AMR', 'Aomori', 301, '', 1),
(268, 107, 'CHB', 'Chiba', 302, '', 1),
(269, 107, 'EHM', 'Ehime', 303, '', 1),
(270, 107, 'FKI', 'Fukui', 304, '', 1),
(271, 107, 'FKO', 'Fukuoka', 305, '', 1),
(272, 107, 'FSM', 'Fukushima', 306, '', 1),
(273, 107, 'GFU', 'Gifu', 307, '', 1),
(274, 107, 'GUM', 'Gunma', 308, '', 1),
(275, 107, 'HRS', 'Hiroshima', 309, '', 1),
(276, 107, 'HKD', 'Hokkaido', 310, '', 1),
(277, 107, 'HYG', 'Hyogo', 311, '', 1),
(278, 107, 'IBR', 'Ibaraki', 312, '', 1),
(279, 107, 'IKW', 'Ishikawa', 313, '', 1),
(280, 107, 'IWT', 'Iwate', 314, '', 1),
(281, 107, 'KGW', 'Kagawa', 315, '', 1),
(282, 107, 'KGS', 'Kagoshima', 316, '', 1),
(283, 107, 'KNG', 'Kanagawa', 317, '', 1),
(284, 107, 'KCH', 'Kochi', 318, '', 1),
(285, 107, 'KMM', 'Kumamoto', 319, '', 1),
(286, 107, 'KYT', 'Kyoto', 320, '', 1),
(287, 107, 'MIE', 'Mie', 321, '', 1),
(288, 107, 'MYG', 'Miyagi', 322, '', 1),
(289, 107, 'MYZ', 'Miyazaki', 323, '', 1),
(290, 107, 'NGN', 'Nagano', 324, '', 1),
(291, 107, 'NGS', 'Nagasaki', 325, '', 1),
(292, 107, 'NRA', 'Nara', 326, '', 1),
(293, 107, 'NGT', 'Niigata', 327, '', 1),
(294, 107, 'OTA', 'Oita', 328, '', 1),
(295, 107, 'OKY', 'Okayama', 329, '', 1),
(296, 107, 'OKN', 'Okinawa', 330, '', 1),
(297, 107, 'OSK', 'Osaka', 331, '', 1),
(298, 107, 'SAG', 'Saga', 332, '', 1),
(299, 107, 'STM', 'Saitama', 333, '', 1),
(300, 107, 'SHG', 'Shiga', 334, '', 1),
(301, 107, 'SMN', 'Shimane', 335, '', 1),
(302, 107, 'SZK', 'Shizuoka', 336, '', 1),
(303, 107, 'TOC', 'Tochigi', 337, '', 1),
(304, 107, 'TKS', 'Tokushima', 338, '', 1),
(305, 107, 'TKY', 'Tokyo', 335, '', 1),
(306, 107, 'TTR', 'Tottori', 336, '', 1),
(307, 107, 'TYM', 'Toyama', 337, '', 1),
(308, 107, 'WKY', 'Wakayama', 338, '', 1),
(309, 107, 'YGT', 'Yamagata', 339, '', 1),
(310, 107, 'YGC', 'Yamaguchi', 340, '', 1),
(311, 107, 'YNS', 'Yamanashi', 341, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ec_subscriber`
--

CREATE TABLE IF NOT EXISTS `ec_subscriber` (
  `subscriber_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `last_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`subscriber_id`),
  UNIQUE KEY `subscriber_email` (`email`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_subscription`
--

CREATE TABLE IF NOT EXISTS `ec_subscription` (
  `subscription_id` int(11) NOT NULL AUTO_INCREMENT,
  `subscription_type` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'paypal',
  `subscription_status` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Active',
  `title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `email` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `last_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_country` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'US',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `model_number` varchar(510) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `price` double(21,3) NOT NULL DEFAULT '0.000',
  `payment_length` int(11) NOT NULL DEFAULT '1',
  `payment_period` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `payment_duration` int(11) NOT NULL DEFAULT '0',
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_payment_date` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `next_payment_date` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `number_payments_completed` int(11) NOT NULL DEFAULT '1',
  `paypal_txn_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `paypal_txn_type` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `paypal_subscr_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `paypal_username` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `paypal_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `stripe_subscription_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `quantity` int(11) NOT NULL DEFAULT '1',
  `num_failed_payment` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`subscription_id`),
  UNIQUE KEY `subscription_id` (`subscription_id`),
  KEY `user_id` (`user_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_subscription_plan`
--

CREATE TABLE IF NOT EXISTS `ec_subscription_plan` (
  `subscription_plan_id` int(11) NOT NULL AUTO_INCREMENT,
  `plan_title` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `can_downgrade` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`subscription_plan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_taxrate`
--

CREATE TABLE IF NOT EXISTS `ec_taxrate` (
  `taxrate_id` int(11) NOT NULL AUTO_INCREMENT,
  `tax_by_state` tinyint(1) NOT NULL DEFAULT '0',
  `tax_by_country` tinyint(1) NOT NULL DEFAULT '0',
  `tax_by_duty` tinyint(1) NOT NULL DEFAULT '0',
  `tax_by_vat` tinyint(1) NOT NULL DEFAULT '0',
  `tax_by_single_vat` tinyint(1) NOT NULL DEFAULT '0',
  `tax_by_all` tinyint(1) NOT NULL DEFAULT '0',
  `state_rate` float(15,3) NOT NULL DEFAULT '0.000',
  `country_rate` float(15,3) NOT NULL DEFAULT '0.000',
  `duty_rate` float(15,3) NOT NULL DEFAULT '0.000',
  `vat_rate` float(15,3) NOT NULL DEFAULT '0.000',
  `vat_added` tinyint(1) NOT NULL DEFAULT '0',
  `vat_included` tinyint(1) NOT NULL DEFAULT '0',
  `all_rate` float(15,3) NOT NULL DEFAULT '0.000',
  `state_code` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `country_code` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `vat_country_code` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `duty_exempt_country_code` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`taxrate_id`),
  UNIQUE KEY `taxrate_id` (`taxrate_id`),
  KEY `state_code` (`state_code`),
  KEY `country_code` (`country_code`),
  KEY `vat_country_code` (`vat_country_code`),
  KEY `duty_exempt_country_code` (`duty_exempt_country_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_tempcart`
--

CREATE TABLE IF NOT EXISTS `ec_tempcart` (
  `tempcart_id` int(11) NOT NULL AUTO_INCREMENT,
  `session_id` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `quantity` int(11) DEFAULT '0',
  `days` int(11) DEFAULT '0',
  `grid_quantity` int(11) DEFAULT '0',
  `grid_days` int(11) DEFAULT '0',
  `gift_card_message` blob,
  `gift_card_from_name` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `gift_card_to_name` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `optionitem_id_1` int(11) NOT NULL DEFAULT '0',
  `optionitem_id_2` int(11) NOT NULL DEFAULT '0',
  `optionitem_id_3` int(11) NOT NULL DEFAULT '0',
  `optionitem_id_4` int(11) NOT NULL DEFAULT '0',
  `optionitem_id_5` int(11) NOT NULL DEFAULT '0',
  `donation_price` float(15,3) NOT NULL DEFAULT '0.000',
  `last_changed_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_deconetwork` tinyint(1) NOT NULL DEFAULT '0',
  `deconetwork_id` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_product_code` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_options` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_edit_link` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_color_code` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_product_id` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_image_link` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `deconetwork_discount` float(15,3) NOT NULL DEFAULT '0.000',
  `deconetwork_tax` float(15,3) NOT NULL DEFAULT '0.000',
  `deconetwork_total` float(15,3) NOT NULL DEFAULT '0.000',
  `deconetwork_version` int(11) NOT NULL DEFAULT '1',
  `gift_card_email` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `abandoned_cart_email_sent` int(11) NOT NULL DEFAULT '0',
  `hide_from_admin` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tempcart_id`),
  UNIQUE KEY `tempcart_tempcart_id` (`tempcart_id`),
  KEY `tempcart_session_id` (`session_id`),
  KEY `tempcart_product_id` (`product_id`),
  KEY `tempcart_optionitem_id_1` (`optionitem_id_1`),
  KEY `tempcart_optionitem_id_2` (`optionitem_id_2`),
  KEY `tempcart_optionitem_id_3` (`optionitem_id_3`),
  KEY `tempcart_optionitem_id_4` (`optionitem_id_4`),
  KEY `tempcart_optionitem_id_5` (`optionitem_id_5`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_tempcart_data`
--

CREATE TABLE IF NOT EXISTS `ec_tempcart_data` (
  `tempcart_data_id` int(11) NOT NULL AUTO_INCREMENT,
  `tempcart_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `session_id` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `email` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `username` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `first_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `last_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `coupon_code` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `giftcard` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_first_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_last_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_company_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_address_line_1` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_address_line_2` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_city` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_state` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_zip` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_country` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `billing_phone` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_selector` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_first_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_last_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_company_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_address_line_2` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_address_line_1` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_city` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_state` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_zip` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_country` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `shipping_phone` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `create_account` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `order_notes` text COLLATE utf8mb4_unicode_520_ci,
  `shipping_method` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `estimate_shipping_zip` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `expedited_shipping` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `estimate_shipping_country` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `is_guest` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `guest_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `subscription_option1` text COLLATE utf8mb4_unicode_520_ci,
  `subscription_option2` text COLLATE utf8mb4_unicode_520_ci,
  `subscription_option3` text COLLATE utf8mb4_unicode_520_ci,
  `subscription_option4` text COLLATE utf8mb4_unicode_520_ci,
  `subscription_option5` text COLLATE utf8mb4_unicode_520_ci,
  `subscription_advanced_option` text COLLATE utf8mb4_unicode_520_ci,
  `subscription_quantity` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `convert_to` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `translate_to` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `taxcloud_tax_amount` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `taxcloud_address_verified` tinyint(1) NOT NULL DEFAULT '0',
  `perpage` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `vat_registration_number` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`tempcart_data_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=30 ;

--
-- Dumping data for table `ec_tempcart_data`
--

INSERT INTO `ec_tempcart_data` (`tempcart_data_id`, `tempcart_time`, `session_id`, `user_id`, `email`, `username`, `first_name`, `last_name`, `coupon_code`, `giftcard`, `billing_first_name`, `billing_last_name`, `billing_company_name`, `billing_address_line_1`, `billing_address_line_2`, `billing_city`, `billing_state`, `billing_zip`, `billing_country`, `billing_phone`, `shipping_selector`, `shipping_first_name`, `shipping_last_name`, `shipping_company_name`, `shipping_address_line_2`, `shipping_address_line_1`, `shipping_city`, `shipping_state`, `shipping_zip`, `shipping_country`, `shipping_phone`, `create_account`, `order_notes`, `shipping_method`, `estimate_shipping_zip`, `expedited_shipping`, `estimate_shipping_country`, `is_guest`, `guest_key`, `subscription_option1`, `subscription_option2`, `subscription_option3`, `subscription_option4`, `subscription_option5`, `subscription_advanced_option`, `subscription_quantity`, `convert_to`, `translate_to`, `taxcloud_tax_amount`, `taxcloud_address_verified`, `perpage`, `vat_registration_number`) VALUES
(29, '2018-05-01 13:56:43', 'BWTHMDAMIFJSUWTOQNSCAYDXTNNCYC', '', 'mamunbdaiub@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', '', '', '1', 'LPTLRAKCVGDMOAFCWOOWLPMIHIVJCM', NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `ec_tempcart_optionitem`
--

CREATE TABLE IF NOT EXISTS `ec_tempcart_optionitem` (
  `tempcart_optionitem_id` int(11) NOT NULL AUTO_INCREMENT,
  `tempcart_id` int(11) NOT NULL DEFAULT '0',
  `option_id` int(11) NOT NULL DEFAULT '0',
  `optionitem_id` int(11) NOT NULL DEFAULT '0',
  `optionitem_value` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `session_id` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `optionitem_model_number` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`tempcart_optionitem_id`),
  UNIQUE KEY `tempcart_optionitem_id` (`tempcart_optionitem_id`),
  KEY `tempcart_id` (`tempcart_id`),
  KEY `option_id` (`option_id`),
  KEY `optionitem_id` (`optionitem_id`),
  KEY `session_id` (`session_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ec_tempcart_optionitem`
--

INSERT INTO `ec_tempcart_optionitem` (`tempcart_optionitem_id`, `tempcart_id`, `option_id`, `optionitem_id`, `optionitem_value`, `session_id`, `optionitem_model_number`) VALUES
(1, 999999999, 3, 3, 'test', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `ec_timezone`
--

CREATE TABLE IF NOT EXISTS `ec_timezone` (
  `timezone_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `identifier` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`timezone_id`),
  UNIQUE KEY `timezone_id` (`timezone_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=141 ;

--
-- Dumping data for table `ec_timezone`
--

INSERT INTO `ec_timezone` (`timezone_id`, `name`, `identifier`) VALUES
(1, '(GMT-12:00) International Date Line West', 'Pacific/Wake'),
(2, '(GMT-11:00) Midway Island', 'Pacific/Apia'),
(3, '(GMT-11:00) Samoa', 'Pacific/Apia'),
(4, '(GMT-10:00) Hawaii', 'Pacific/Honolulu'),
(5, '(GMT-09:00) Alaska', 'America/Anchorage'),
(6, '(GMT-08:00) Pacific Time (US & Canada) Tijuana', 'America/Los_Angeles'),
(7, '(GMT-07:00) Arizona', 'America/Phoenix'),
(8, '(GMT-07:00) Chihuahua', 'America/Chihuahua'),
(9, '(GMT-07:00) La Paz', 'America/Chihuahua'),
(10, '(GMT-07:00) Mazatlan', 'America/Chihuahua'),
(11, '(GMT-07:00) Mountain Time (US & Canada)', 'America/Denver'),
(12, '(GMT-06:00) Central America', 'America/Managua'),
(13, '(GMT-06:00) Central Time (US & Canada)', 'America/Chicago'),
(14, '(GMT-06:00) Guadalajara', 'America/Mexico_City'),
(15, '(GMT-06:00) Mexico City', 'America/Mexico_City'),
(16, '(GMT-06:00) Monterrey', 'America/Mexico_City'),
(17, '(GMT-06:00) Saskatchewan', 'America/Regina'),
(18, '(GMT-05:00) Bogota', 'America/Bogota'),
(19, '(GMT-05:00) Eastern Time (US & Canada)', 'America/New_York'),
(20, '(GMT-05:00) Indiana (East)', 'America/Indiana/Indianapolis'),
(21, '(GMT-05:00) Lima', 'America/Bogota'),
(22, '(GMT-05:00) Quito', 'America/Bogota'),
(23, '(GMT-04:00) Atlantic Time (Canada)', 'America/Halifax'),
(24, '(GMT-04:00) Caracas', 'America/Caracas'),
(25, '(GMT-04:00) La Paz', 'America/Caracas'),
(26, '(GMT-04:00) Santiago', 'America/Santiago'),
(27, '(GMT-03:30) Newfoundland', 'America/St_Johns'),
(28, '(GMT-03:00) Brasilia', 'America/Sao_Paulo'),
(29, '(GMT-03:00) Buenos Aires', 'America/Argentina/Buenos_Aires'),
(30, '(GMT-03:00) Georgetown', 'America/Argentina/Buenos_Aires'),
(31, '(GMT-03:00) Greenland', 'America/Godthab'),
(32, '(GMT-02:00) Mid-Atlantic', 'America/Noronha'),
(33, '(GMT-01:00) Azores', 'Atlantic/Azores'),
(34, '(GMT-01:00) Cape Verde Is.', 'Atlantic/Cape_Verde'),
(35, '(GMT) Casablanca', 'Africa/Casablanca'),
(36, '(GMT) Edinburgh', 'Europe/London'),
(37, '(GMT) Greenwich Mean Time : Dublin', 'Europe/London'),
(38, '(GMT) Lisbon', 'Europe/London'),
(39, '(GMT) London', 'Europe/London'),
(40, '(GMT) Monrovia', 'Africa/Casablanca'),
(41, '(GMT+01:00) Amsterdam', 'Europe/Berlin'),
(42, '(GMT+01:00) Belgrade', 'Europe/Belgrade'),
(43, '(GMT+01:00) Berlin', 'Europe/Berlin'),
(44, '(GMT+01:00) Bern', 'Europe/Berlin'),
(45, '(GMT+01:00) Bratislava', 'Europe/Belgrade'),
(46, '(GMT+01:00) Brussels', 'Europe/Paris'),
(47, '(GMT+01:00) Budapest', 'Europe/Belgrade'),
(48, '(GMT+01:00) Copenhagen', 'Europe/Paris'),
(49, '(GMT+01:00) Ljubljana', 'Europe/Belgrade'),
(50, '(GMT+01:00) Madrid', 'Europe/Paris'),
(51, '(GMT+01:00) Paris', 'Europe/Paris'),
(52, '(GMT+01:00) Prague', 'Europe/Belgrade'),
(53, '(GMT+01:00) Rome', 'Europe/Berlin'),
(54, '(GMT+01:00) Sarajevo', 'Europe/Sarajevo'),
(55, '(GMT+01:00) Skopje', 'Europe/Sarajevo'),
(56, '(GMT+01:00) Stockholm', 'Europe/Berlin'),
(57, '(GMT+01:00) Vienna', 'Europe/Berlin'),
(58, '(GMT+01:00) Warsaw', 'Europe/Sarajevo'),
(59, '(GMT+01:00) West Central Africa', 'Africa/Lagos'),
(60, '(GMT+01:00) Zagreb', 'Europe/Sarajevo'),
(61, '(GMT+02:00) Athens', 'Europe/Istanbul'),
(62, '(GMT+02:00) Bucharest', 'Europe/Bucharest'),
(63, '(GMT+02:00) Cairo', 'Africa/Cairo'),
(64, '(GMT+02:00) Harare', 'Africa/Johannesburg'),
(65, '(GMT+02:00) Helsinki', 'Europe/Helsinki'),
(66, '(GMT+02:00) Istanbul', 'Europe/Istanbul'),
(67, '(GMT+02:00) Jerusalem', 'Asia/Jerusalem'),
(68, '(GMT+02:00) Kyiv', 'Europe/Helsinki'),
(69, '(GMT+02:00) Minsk', 'Europe/Istanbul'),
(70, '(GMT+02:00) Pretoria', 'Africa/Johannesburg'),
(71, '(GMT+02:00) Riga', 'Europe/Helsinki'),
(72, '(GMT+02:00) Sofia', 'Europe/Helsinki'),
(73, '(GMT+02:00) Tallinn', 'Europe/Helsinki'),
(74, '(GMT+02:00) Vilnius', 'Europe/Helsinki'),
(75, '(GMT+03:00) Baghdad', 'Asia/Baghdad'),
(76, '(GMT+03:00) Kuwait', 'Asia/Riyadh'),
(77, '(GMT+03:00) Moscow', 'Europe/Moscow'),
(78, '(GMT+03:00) Nairobi', 'Africa/Nairobi'),
(79, '(GMT+03:00) Riyadh', 'Asia/Riyadh'),
(80, '(GMT+03:00) St. Petersburg', 'Europe/Moscow'),
(81, '(GMT+03:00) Volgograd', 'Europe/Moscow'),
(82, '(GMT+03:30) Tehran', 'Asia/Tehran'),
(83, '(GMT+04:00) Abu Dhabi', 'Asia/Muscat'),
(84, '(GMT+04:00) Baku', 'Asia/Tbilisi'),
(85, '(GMT+04:00) Muscat', 'Asia/Muscat'),
(86, '(GMT+04:00) Tbilisi', 'Asia/Tbilisi'),
(87, '(GMT+04:00) Yerevan', 'Asia/Tbilisi'),
(88, '(GMT+04:30) Kabul', 'Asia/Kabul'),
(89, '(GMT+05:00) Ekaterinburg', 'Asia/Yekaterinburg'),
(90, '(GMT+05:00) Islamabad', 'Asia/Karachi'),
(91, '(GMT+05:00) Karachi', 'Asia/Karachi'),
(92, '(GMT+05:00) Tashkent', 'Asia/Karachi'),
(93, '(GMT+05:30) Chennai', 'Asia/Calcutta'),
(94, '(GMT+05:30) Kolkata', 'Asia/Calcutta'),
(95, '(GMT+05:30) Mumbai', 'Asia/Calcutta'),
(96, '(GMT+05:30) New Delhi', 'Asia/Calcutta'),
(97, '(GMT+05:45) Kathmandu', 'Asia/Katmandu'),
(98, '(GMT+06:00) Almaty', 'Asia/Novosibirsk'),
(99, '(GMT+06:00) Astana', 'Asia/Dhaka'),
(100, '(GMT+06:00) Dhaka', 'Asia/Dhaka'),
(101, '(GMT+06:00) Novosibirsk', 'Asia/Novosibirsk'),
(102, '(GMT+06:00) Sri Jayawardenepura', 'Asia/Colombo'),
(103, '(GMT+06:30) Rangoon', 'Asia/Rangoon'),
(104, '(GMT+07:00) Bangkok', 'Asia/Bangkok'),
(105, '(GMT+07:00) Hanoi', 'Asia/Bangkok'),
(106, '(GMT+07:00) Jakarta', 'Asia/Bangkok'),
(107, '(GMT+07:00) Krasnoyarsk', 'Asia/Krasnoyarsk'),
(108, '(GMT+08:00) Beijing', 'Asia/Hong_Kong'),
(109, '(GMT+08:00) Chongqing', 'Asia/Hong_Kong'),
(110, '(GMT+08:00) Hong Kong', 'Asia/Hong_Kong'),
(111, '(GMT+08:00) Irkutsk', 'Asia/Irkutsk'),
(112, '(GMT+08:00) Kuala Lumpur', 'Asia/Singapore'),
(113, '(GMT+08:00) Perth', 'Australia/Perth'),
(114, '(GMT+08:00) Singapore', 'Asia/Singapore'),
(115, '(GMT+08:00) Taipei', 'Asia/Taipei'),
(116, '(GMT+08:00) Ulaan Bataar', 'Asia/Irkutsk'),
(117, '(GMT+08:00) Urumqi', 'Asia/Hong_Kong'),
(118, '(GMT+09:00) Osaka', 'Asia/Tokyo'),
(119, '(GMT+09:00) Sapporo', 'Asia/Tokyo'),
(120, '(GMT+09:00) Seoul', 'Asia/Seoul'),
(121, '(GMT+09:00) Tokyo', 'Asia/Tokyo'),
(122, '(GMT+09:00) Yakutsk', 'Asia/Yakutsk'),
(123, '(GMT+09:30) Adelaide', 'Australia/Adelaide'),
(124, '(GMT+09:30) Darwin', 'Australia/Darwin'),
(125, '(GMT+10:00) Brisbane', 'Australia/Brisbane'),
(126, '(GMT+10:00) Canberra', 'Australia/Sydney'),
(127, '(GMT+10:00) Guam', 'Pacific/Guam'),
(128, '(GMT+10:00) Hobart', 'Australia/Hobart'),
(129, '(GMT+10:00) Melbourne', 'Australia/Sydney'),
(130, '(GMT+10:00) Port Moresby', 'Pacific/Guam'),
(131, '(GMT+10:00) Sydney', 'Australia/Sydney'),
(132, '(GMT+10:00) Vladivostok', 'Asia/Vladivostok'),
(133, '(GMT+11:00) Magadan', 'Asia/Magadan'),
(134, '(GMT+11:00) New Caledonia', 'Asia/Magadan'),
(135, '(GMT+11:00) Solomon Is.', 'Asia/Magadan'),
(136, '(GMT+12:00) Auckland', 'Pacific/Auckland'),
(137, '(GMT+12:00) Fiji', 'Pacific/Fiji'),
(138, '(GMT+12:00) Kamchatka', 'Pacific/Fiji'),
(139, '(GMT+12:00) Marshall Is.', 'Pacific/Fiji'),
(140, '(GMT+12:00) Wellington', 'Pacific/Auckland');

-- --------------------------------------------------------

--
-- Table structure for table `ec_user`
--

CREATE TABLE IF NOT EXISTS `ec_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_demo_item` tinyint(1) NOT NULL DEFAULT '0',
  `email` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `list_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `edit_sequence` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `quickbooks_status` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Not Queued',
  `first_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `last_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `default_billing_address_id` int(11) NOT NULL DEFAULT '0',
  `default_shipping_address_id` int(11) NOT NULL DEFAULT '0',
  `user_level` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'shopper',
  `is_subscriber` tinyint(1) NOT NULL DEFAULT '0',
  `realauth_registered` tinyint(1) NOT NULL DEFAULT '0',
  `stripe_customer_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `default_card_type` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `default_card_last4` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `exclude_tax` tinyint(1) NOT NULL DEFAULT '0',
  `exclude_shipping` tinyint(1) NOT NULL DEFAULT '0',
  `user_notes` text COLLATE utf8mb4_unicode_520_ci,
  `vat_registration_number` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_user_id` (`user_id`),
  UNIQUE KEY `user_email` (`email`(191)),
  KEY `user_password` (`password`(191)),
  KEY `user_default_billing_address_id` (`default_billing_address_id`),
  KEY `user_default_shipping_address_id` (`default_shipping_address_id`),
  KEY `user_user_level` (`user_level`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ec_webhook`
--

CREATE TABLE IF NOT EXISTS `ec_webhook` (
  `webhook_id` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `webhook_type` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `webhook_data` blob,
  PRIMARY KEY (`webhook_id`),
  UNIQUE KEY `webhook_id` (`webhook_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ec_zone`
--

CREATE TABLE IF NOT EXISTS `ec_zone` (
  `zone_id` int(11) NOT NULL AUTO_INCREMENT,
  `zone_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`zone_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `ec_zone`
--

INSERT INTO `ec_zone` (`zone_id`, `zone_name`) VALUES
(1, 'North America'),
(2, 'South America'),
(3, 'Europe'),
(4, 'Africa'),
(5, 'Asia'),
(6, 'Australia'),
(7, 'Oceania'),
(8, 'Lower 48 States'),
(9, 'Alaska and Hawaii');

-- --------------------------------------------------------

--
-- Table structure for table `ec_zone_to_location`
--

CREATE TABLE IF NOT EXISTS `ec_zone_to_location` (
  `zone_to_location_id` int(11) NOT NULL AUTO_INCREMENT,
  `zone_id` int(11) NOT NULL DEFAULT '0',
  `iso2_cnt` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `code_sta` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`zone_to_location_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=259 ;

--
-- Dumping data for table `ec_zone_to_location`
--

INSERT INTO `ec_zone_to_location` (`zone_to_location_id`, `zone_id`, `iso2_cnt`, `code_sta`) VALUES
(1, 1, 'AI', ''),
(2, 1, 'AQ', ''),
(3, 1, 'AW', ''),
(4, 1, 'BS', ''),
(5, 1, 'BB', ''),
(6, 1, 'BM', ''),
(7, 1, 'BZ', ''),
(8, 1, 'CA', ''),
(9, 1, 'KY', ''),
(10, 1, 'CR', ''),
(11, 1, 'CU', ''),
(12, 1, 'DM', ''),
(13, 1, 'DO', ''),
(14, 1, 'SV', ''),
(15, 1, 'GL', ''),
(16, 1, 'GD', ''),
(17, 1, 'GP', ''),
(18, 1, 'GT', ''),
(19, 1, 'HT', ''),
(20, 1, 'HN', ''),
(21, 1, 'JM', ''),
(22, 1, 'MQ', ''),
(23, 1, 'MX', ''),
(24, 1, 'MS', ''),
(25, 1, 'NI', ''),
(26, 1, 'PA', ''),
(27, 1, 'PR', ''),
(28, 1, 'KN', ''),
(29, 1, 'LC', ''),
(30, 1, 'TT', ''),
(31, 1, 'TC', ''),
(32, 1, 'US', ''),
(33, 1, 'VI', ''),
(34, 2, 'AR', ''),
(35, 2, 'BO', ''),
(36, 2, 'BR', ''),
(37, 2, 'CL', ''),
(38, 2, 'CO', ''),
(39, 2, 'EC', ''),
(40, 2, 'GF', ''),
(41, 2, 'GY', ''),
(42, 2, 'PY', ''),
(43, 2, 'PE', ''),
(44, 2, 'SR', ''),
(45, 2, 'UY', ''),
(46, 2, 'VE', ''),
(47, 6, 'AU', ''),
(48, 7, 'AS', ''),
(49, 7, 'AU', ''),
(50, 7, 'CK', ''),
(51, 7, 'FJ', ''),
(52, 7, 'PF', ''),
(53, 7, 'GU', ''),
(54, 7, 'KI', ''),
(55, 7, 'MH', ''),
(56, 7, 'NR', ''),
(57, 7, 'NC', ''),
(58, 7, 'NZ', ''),
(59, 7, 'NU', ''),
(60, 7, 'NF', ''),
(61, 7, 'PW', ''),
(62, 7, 'PG', ''),
(63, 7, 'PN', ''),
(64, 7, 'WS', ''),
(65, 7, 'SB', ''),
(66, 7, 'TK', ''),
(67, 7, 'TO', ''),
(68, 7, 'TV', ''),
(69, 7, 'VU', ''),
(70, 7, 'WF', ''),
(71, 3, 'AL', ''),
(72, 3, 'AD', ''),
(73, 3, 'AT', ''),
(74, 3, 'BY', ''),
(75, 3, 'BE', ''),
(76, 3, 'BG', ''),
(77, 3, 'HR', ''),
(78, 3, 'CZ', ''),
(79, 3, 'DK', ''),
(80, 3, 'EE', ''),
(81, 3, 'FO', ''),
(82, 3, 'FI', ''),
(83, 3, 'FR', ''),
(84, 3, 'DE', ''),
(85, 3, 'GI', ''),
(86, 3, 'GR', ''),
(87, 3, 'HU', ''),
(88, 3, 'IS', ''),
(89, 3, 'IE', ''),
(90, 3, 'IT', ''),
(91, 3, 'LV', ''),
(92, 3, 'LI', ''),
(93, 3, 'LT', ''),
(94, 3, 'LU', ''),
(95, 3, 'MT', ''),
(96, 3, 'MC', ''),
(97, 3, 'NL', ''),
(98, 3, 'NO', ''),
(99, 3, 'PL', ''),
(100, 3, 'PT', ''),
(101, 3, 'RO', ''),
(102, 3, 'RU', ''),
(103, 3, 'SM', ''),
(104, 3, 'SI', ''),
(105, 3, 'ES', ''),
(106, 3, 'SE', ''),
(107, 3, 'CH', ''),
(108, 3, 'UA', ''),
(109, 3, 'GB', ''),
(110, 4, 'DZ', ''),
(111, 4, 'AO', ''),
(112, 4, 'BJ', ''),
(113, 4, 'BW', ''),
(114, 4, 'BF', ''),
(115, 4, 'BI', ''),
(116, 4, 'CM', ''),
(117, 4, 'CV', ''),
(118, 4, 'TD', ''),
(119, 4, 'KM', ''),
(120, 4, 'CG', ''),
(121, 4, 'CI', ''),
(122, 4, 'DJ', ''),
(123, 4, 'EG', ''),
(124, 4, 'GQ', ''),
(125, 4, 'ER', ''),
(126, 4, 'ET', ''),
(127, 4, 'GA', ''),
(128, 4, 'GM', ''),
(129, 4, 'GH', ''),
(130, 4, 'GN', ''),
(131, 4, 'GW', ''),
(132, 4, 'KE', ''),
(133, 4, 'LS', ''),
(134, 4, 'LR', ''),
(135, 4, 'MG', ''),
(136, 4, 'MW', ''),
(137, 4, 'ML', ''),
(138, 4, 'MR', ''),
(139, 4, 'MU', ''),
(140, 4, 'YT', ''),
(141, 4, 'MA', ''),
(142, 4, 'MZ', ''),
(143, 4, 'NA', ''),
(144, 4, 'NE', ''),
(145, 4, 'NG', ''),
(146, 4, 'RE', ''),
(147, 4, 'RW', ''),
(148, 4, 'ST', ''),
(149, 4, 'SN', ''),
(150, 4, 'SC', ''),
(151, 4, 'SL', ''),
(152, 4, 'SO', ''),
(153, 4, 'ZA', ''),
(154, 4, 'SD', ''),
(155, 4, 'SZ', ''),
(156, 4, 'TG', ''),
(157, 4, 'TN', ''),
(158, 4, 'UG', ''),
(159, 4, 'ZM', ''),
(160, 4, 'ZW', ''),
(161, 5, 'AF', ''),
(162, 5, 'AM', ''),
(163, 5, 'AZ', ''),
(164, 5, 'BH', ''),
(165, 5, 'BD', ''),
(166, 5, 'BT', ''),
(167, 5, 'BN', ''),
(168, 5, 'KH', ''),
(169, 5, 'CN', ''),
(170, 5, 'CX', ''),
(171, 5, 'CY', ''),
(172, 5, 'TP', ''),
(173, 5, 'GE', ''),
(174, 5, 'HK', ''),
(175, 5, 'IN', ''),
(176, 5, 'ID', ''),
(177, 5, 'IQ', ''),
(178, 5, 'IL', ''),
(179, 5, 'JP', ''),
(180, 5, 'JO', ''),
(181, 5, 'KZ', ''),
(182, 5, 'KW', ''),
(183, 5, 'KG', ''),
(184, 5, 'LB', ''),
(185, 5, 'MO', ''),
(186, 5, 'MY', ''),
(187, 5, 'MV', ''),
(188, 5, 'MN', ''),
(189, 5, 'MM', ''),
(190, 5, 'NP', ''),
(191, 5, 'OM', ''),
(192, 5, 'PK', ''),
(193, 5, 'PH', ''),
(194, 5, 'QA', ''),
(195, 5, 'SA', ''),
(196, 5, 'SG', ''),
(197, 5, 'LK', ''),
(198, 5, 'TW', ''),
(199, 5, 'TJ', ''),
(200, 5, 'TH', ''),
(201, 5, 'TR', ''),
(202, 5, 'TM', ''),
(203, 5, 'AE', ''),
(204, 5, 'UZ', ''),
(205, 5, 'VN', ''),
(206, 5, 'YE', ''),
(207, 9, 'US', 'HI'),
(208, 9, 'US', 'AK'),
(209, 8, 'US', 'AL'),
(210, 8, 'US', 'AZ'),
(211, 8, 'US', 'AR'),
(212, 8, 'US', 'CA'),
(213, 8, 'US', 'CO'),
(214, 8, 'US', 'CT'),
(215, 8, 'US', 'DE'),
(216, 8, 'US', 'FL'),
(217, 8, 'US', 'GA'),
(218, 8, 'US', 'ID'),
(219, 8, 'US', 'IL'),
(220, 8, 'US', 'IN'),
(221, 8, 'US', 'IA'),
(222, 8, 'US', 'KS'),
(223, 8, 'US', 'KY'),
(224, 8, 'US', 'LA'),
(225, 8, 'US', 'ME'),
(226, 8, 'US', 'MD'),
(227, 8, 'US', 'MA'),
(228, 8, 'US', 'MI'),
(229, 8, 'US', 'MN'),
(230, 8, 'US', 'MS'),
(231, 8, 'US', 'MO'),
(232, 8, 'US', 'MT'),
(233, 8, 'US', 'NE'),
(234, 8, 'US', 'NV'),
(235, 8, 'US', 'NH'),
(236, 8, 'US', 'NJ'),
(237, 8, 'US', 'NM'),
(238, 8, 'US', 'NY'),
(239, 8, 'US', 'NC'),
(240, 8, 'US', 'ND'),
(241, 8, 'US', 'OH'),
(242, 8, 'US', 'OK'),
(243, 8, 'US', 'OR'),
(244, 8, 'US', 'PA'),
(245, 8, 'US', 'RI'),
(246, 8, 'US', 'SC'),
(247, 8, 'US', 'SD'),
(248, 8, 'US', 'TN'),
(249, 8, 'US', 'TX'),
(250, 8, 'US', 'UT'),
(251, 8, 'US', 'VT'),
(252, 8, 'US', 'VA'),
(253, 8, 'US', 'WA'),
(254, 8, 'US', 'WV'),
(255, 8, 'US', 'WI'),
(256, 8, 'US', 'WY'),
(257, 3, 'DC', ''),
(258, 8, 'US', 'DC');

-- --------------------------------------------------------

--
-- Table structure for table `ops_commentmeta`
--

CREATE TABLE IF NOT EXISTS `ops_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ops_comments`
--

CREATE TABLE IF NOT EXISTS `ops_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ops_comments`
--

INSERT INTO `ops_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2018-04-30 06:23:21', '2018-04-30 06:23:21', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href="https://gravatar.com">Gravatar</a>.', 0, '1', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ops_links`
--

CREATE TABLE IF NOT EXISTS `ops_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ops_options`
--

CREATE TABLE IF NOT EXISTS `ops_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=717 ;

--
-- Dumping data for table `ops_options`
--

INSERT INTO `ops_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/oz_production/public_html', 'yes'),
(2, 'home', 'http://localhost/oz_production/public_html', 'yes'),
(3, 'blogname', 'Oz Production', 'yes'),
(4, 'blogdescription', 'Oz Production Event Services, LLC', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'info@ozproductionservices.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:106:{s:11:"^wp-json/?$";s:22:"index.php?rest_route=/";s:14:"^wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:21:"^index.php/wp-json/?$";s:22:"index.php?rest_route=/";s:24:"^index.php/wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:17:"^store/([^/]*)/?$";s:30:"index.php?ec_store=$matches[1]";s:47:"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:42:"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:23:"category/(.+?)/embed/?$";s:46:"index.php?category_name=$matches[1]&embed=true";s:35:"category/(.+?)/page/?([0-9]{1,})/?$";s:53:"index.php?category_name=$matches[1]&paged=$matches[2]";s:17:"category/(.+?)/?$";s:35:"index.php?category_name=$matches[1]";s:44:"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:39:"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:20:"tag/([^/]+)/embed/?$";s:36:"index.php?tag=$matches[1]&embed=true";s:32:"tag/([^/]+)/page/?([0-9]{1,})/?$";s:43:"index.php?tag=$matches[1]&paged=$matches[2]";s:14:"tag/([^/]+)/?$";s:25:"index.php?tag=$matches[1]";s:45:"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:40:"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:21:"type/([^/]+)/embed/?$";s:44:"index.php?post_format=$matches[1]&embed=true";s:33:"type/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?post_format=$matches[1]&paged=$matches[2]";s:15:"type/([^/]+)/?$";s:33:"index.php?post_format=$matches[1]";s:33:"store/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:43:"store/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:63:"store/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:58:"store/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:58:"store/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:39:"store/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:22:"store/([^/]+)/embed/?$";s:41:"index.php?ec_store=$matches[1]&embed=true";s:26:"store/([^/]+)/trackback/?$";s:35:"index.php?ec_store=$matches[1]&tb=1";s:46:"store/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?ec_store=$matches[1]&feed=$matches[2]";s:41:"store/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?ec_store=$matches[1]&feed=$matches[2]";s:34:"store/([^/]+)/page/?([0-9]{1,})/?$";s:48:"index.php?ec_store=$matches[1]&paged=$matches[2]";s:41:"store/([^/]+)/comment-page-([0-9]{1,})/?$";s:48:"index.php?ec_store=$matches[1]&cpage=$matches[2]";s:30:"store/([^/]+)(?:/([0-9]+))?/?$";s:47:"index.php?ec_store=$matches[1]&page=$matches[2]";s:22:"store/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:32:"store/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:52:"store/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:47:"store/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:47:"store/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:28:"store/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:48:".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$";s:18:"index.php?feed=old";s:20:".*wp-app\\.php(/.*)?$";s:19:"index.php?error=403";s:18:".*wp-register.php$";s:23:"index.php?register=true";s:32:"feed/(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:27:"(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:8:"embed/?$";s:21:"index.php?&embed=true";s:20:"page/?([0-9]{1,})/?$";s:28:"index.php?&paged=$matches[1]";s:41:"comments/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:36:"comments/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:17:"comments/embed/?$";s:21:"index.php?&embed=true";s:44:"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:39:"search/(.+)/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:20:"search/(.+)/embed/?$";s:34:"index.php?s=$matches[1]&embed=true";s:32:"search/(.+)/page/?([0-9]{1,})/?$";s:41:"index.php?s=$matches[1]&paged=$matches[2]";s:14:"search/(.+)/?$";s:23:"index.php?s=$matches[1]";s:47:"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:42:"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:23:"author/([^/]+)/embed/?$";s:44:"index.php?author_name=$matches[1]&embed=true";s:35:"author/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?author_name=$matches[1]&paged=$matches[2]";s:17:"author/([^/]+)/?$";s:33:"index.php?author_name=$matches[1]";s:69:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:45:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$";s:74:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]";s:39:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$";s:63:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]";s:56:"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:51:"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:32:"([0-9]{4})/([0-9]{1,2})/embed/?$";s:58:"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true";s:44:"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]";s:26:"([0-9]{4})/([0-9]{1,2})/?$";s:47:"index.php?year=$matches[1]&monthnum=$matches[2]";s:43:"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:38:"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:19:"([0-9]{4})/embed/?$";s:37:"index.php?year=$matches[1]&embed=true";s:31:"([0-9]{4})/page/?([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&paged=$matches[2]";s:13:"([0-9]{4})/?$";s:26:"index.php?year=$matches[1]";s:27:".?.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:".?.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:".?.+?/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"(.?.+?)/embed/?$";s:41:"index.php?pagename=$matches[1]&embed=true";s:20:"(.?.+?)/trackback/?$";s:35:"index.php?pagename=$matches[1]&tb=1";s:40:"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:35:"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:28:"(.?.+?)/page/?([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&paged=$matches[2]";s:35:"(.?.+?)/comment-page-([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&cpage=$matches[2]";s:24:"(.?.+?)(?:/([0-9]+))?/?$";s:47:"index.php?pagename=$matches[1]&page=$matches[2]";s:27:"[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:"[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:"[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"([^/]+)/embed/?$";s:37:"index.php?name=$matches[1]&embed=true";s:20:"([^/]+)/trackback/?$";s:31:"index.php?name=$matches[1]&tb=1";s:40:"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:35:"([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:28:"([^/]+)/page/?([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&paged=$matches[2]";s:35:"([^/]+)/comment-page-([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&cpage=$matches[2]";s:24:"([^/]+)(?:/([0-9]+))?/?$";s:43:"index.php?name=$matches[1]&page=$matches[2]";s:16:"[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:26:"[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:46:"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:22:"[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:1:{i:0;s:26:"wp-easycart/wpeasycart.php";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'ozproduction', 'yes'),
(41, 'stylesheet', 'ozproduction', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '38590', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'posts', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:"title";s:0:"";s:5:"count";i:0;s:12:"hierarchical";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(79, 'widget_text', 'a:0:{}', 'yes'),
(80, 'widget_rss', 'a:0:{}', 'yes'),
(81, 'uninstall_plugins', 'a:1:{s:26:"wp-easycart/wpeasycart.php";s:12:"ec_uninstall";}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '0', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'initial_db_version', '38590', 'yes'),
(92, 'ops_user_roles', 'a:5:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:61:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:34:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:10:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:5:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:2:{s:4:"read";b:1;s:7:"level_0";b:1;}}}', 'yes'),
(93, 'fresh_site', '0', 'yes'),
(94, 'widget_search', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(95, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(96, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(97, 'widget_archives', 'a:2:{i:2;a:3:{s:5:"title";s:0:"";s:5:"count";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(98, 'widget_meta', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(99, 'sidebars_widgets', 'a:4:{s:19:"wp_inactive_widgets";a:0:{}s:19:"primary-widget-area";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:21:"secondary-widget-area";a:0:{}s:13:"array_version";i:3;}', 'yes'),
(100, 'widget_pages', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(101, 'widget_calendar', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(102, 'widget_media_audio', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(103, 'widget_media_image', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(104, 'widget_media_gallery', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(105, 'widget_media_video', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(106, 'widget_tag_cloud', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(107, 'widget_nav_menu', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(108, 'widget_custom_html', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(109, 'cron', 'a:4:{i:1525184702;a:1:{s:30:"wp_scheduled_auto_draft_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1525199003;a:3:{s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1525242229;a:2:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}s:25:"delete_expired_transients";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}s:7:"version";i:2;}', 'yes'),
(110, 'theme_mods_twentyseventeen', 'a:2:{s:18:"custom_css_post_id";i:-1;s:16:"sidebars_widgets";a:2:{s:4:"time";i:1525072201;s:4:"data";a:4:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:9:"sidebar-2";a:0:{}s:9:"sidebar-3";a:0:{}}}}', 'yes'),
(114, '_site_transient_update_core', 'O:8:"stdClass":4:{s:7:"updates";a:1:{i:0;O:8:"stdClass":10:{s:8:"response";s:6:"latest";s:8:"download";s:59:"https://downloads.wordpress.org/release/wordpress-4.9.5.zip";s:6:"locale";s:5:"en_US";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:59:"https://downloads.wordpress.org/release/wordpress-4.9.5.zip";s:10:"no_content";s:70:"https://downloads.wordpress.org/release/wordpress-4.9.5-no-content.zip";s:11:"new_bundled";s:71:"https://downloads.wordpress.org/release/wordpress-4.9.5-new-bundled.zip";s:7:"partial";b:0;s:8:"rollback";b:0;}s:7:"current";s:5:"4.9.5";s:7:"version";s:5:"4.9.5";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.7";s:15:"partial_version";s:0:"";}}s:12:"last_checked";i:1525155837;s:15:"version_checked";s:5:"4.9.5";s:12:"translations";a:0:{}}', 'no'),
(119, '_site_transient_update_themes', 'O:8:"stdClass":4:{s:12:"last_checked";i:1525155307;s:7:"checked";a:4:{s:12:"ozproduction";s:3:"1.0";s:13:"twentyfifteen";s:3:"1.9";s:15:"twentyseventeen";s:3:"1.5";s:13:"twentysixteen";s:3:"1.4";}s:8:"response";a:0:{}s:12:"translations";a:0:{}}', 'no'),
(120, '_site_transient_timeout_browser_efc56fe28520bcd166ef136f44025003', '1525674231', 'no'),
(121, '_site_transient_browser_efc56fe28520bcd166ef136f44025003', 'a:10:{s:4:"name";s:6:"Chrome";s:7:"version";s:13:"65.0.3325.181";s:8:"platform";s:7:"Windows";s:10:"update_url";s:29:"https://www.google.com/chrome";s:7:"img_src";s:43:"http://s.w.org/images/browsers/chrome.png?1";s:11:"img_src_ssl";s:44:"https://s.w.org/images/browsers/chrome.png?1";s:15:"current_version";s:2:"18";s:7:"upgrade";b:0;s:8:"insecure";b:0;s:6:"mobile";b:0;}', 'no'),
(124, 'can_compress_scripts', '1', 'no'),
(139, 'current_theme', 'blank press 0.1', 'yes'),
(140, 'theme_mods_ozproduction', 'a:3:{i:0;b:0;s:18:"nav_menu_locations";a:0:{}s:18:"custom_css_post_id";i:-1;}', 'yes'),
(141, 'theme_switched', '', 'yes'),
(142, '_transient_timeout_plugin_slugs', '1525181719', 'no'),
(143, '_transient_plugin_slugs', 'a:2:{i:0;s:19:"akismet/akismet.php";i:1;s:9:"hello.php";}', 'no'),
(144, 'recently_activated', 'a:0:{}', 'yes'),
(150, '_site_transient_update_plugins', 'O:8:"stdClass":5:{s:12:"last_checked";i:1525155308;s:7:"checked";a:2:{s:19:"akismet/akismet.php";s:5:"4.0.3";s:26:"wp-easycart/wpeasycart.php";s:6:"4.0.37";}s:8:"response";a:0:{}s:12:"translations";a:0:{}s:9:"no_update";a:2:{s:19:"akismet/akismet.php";O:8:"stdClass":9:{s:2:"id";s:21:"w.org/plugins/akismet";s:4:"slug";s:7:"akismet";s:6:"plugin";s:19:"akismet/akismet.php";s:11:"new_version";s:5:"4.0.3";s:3:"url";s:38:"https://wordpress.org/plugins/akismet/";s:7:"package";s:56:"https://downloads.wordpress.org/plugin/akismet.4.0.3.zip";s:5:"icons";a:2:{s:2:"2x";s:59:"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272";s:2:"1x";s:59:"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272";}s:7:"banners";a:1:{s:2:"1x";s:61:"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904";}s:11:"banners_rtl";a:0:{}}s:26:"wp-easycart/wpeasycart.php";O:8:"stdClass":9:{s:2:"id";s:25:"w.org/plugins/wp-easycart";s:4:"slug";s:11:"wp-easycart";s:6:"plugin";s:26:"wp-easycart/wpeasycart.php";s:11:"new_version";s:6:"4.0.37";s:3:"url";s:42:"https://wordpress.org/plugins/wp-easycart/";s:7:"package";s:61:"https://downloads.wordpress.org/plugin/wp-easycart.4.0.37.zip";s:5:"icons";a:2:{s:2:"2x";s:64:"https://ps.w.org/wp-easycart/assets/icon-256x256.png?rev=1015460";s:2:"1x";s:64:"https://ps.w.org/wp-easycart/assets/icon-128x128.png?rev=1015460";}s:7:"banners";a:2:{s:2:"2x";s:67:"https://ps.w.org/wp-easycart/assets/banner-1544x500.png?rev=1738545";s:2:"1x";s:66:"https://ps.w.org/wp-easycart/assets/banner-772x250.png?rev=1738545";}s:11:"banners_rtl";a:0:{}}}}', 'no'),
(151, 'ec_option_is_installed', '1', 'yes'),
(152, 'ec_option_storepage', '5', 'yes'),
(153, 'ec_option_cartpage', '6', 'yes'),
(154, 'ec_option_accountpage', '7', 'yes'),
(155, 'ec_option_default_manufacturer', '1', 'yes'),
(156, 'ec_option_store_locale', 'US', 'yes'),
(157, 'ec_option_setup_wizard_done', '1', 'yes'),
(158, 'ec_option_setup_wizard_step', '5', 'yes'),
(159, 'ec_option_review_complete', '1', 'yes'),
(160, 'ec_option_db_version', '1_30', 'yes'),
(161, 'ec_option_show_lite_message', '1', 'yes'),
(162, 'ec_option_new_linking_setup', '0', 'yes'),
(163, 'ec_option_show_install_message', '0', 'yes'),
(164, 'ec_option_added_custom_post_type', '2', 'yes'),
(165, 'ec_option_hide_admin_notice', '0', 'yes'),
(166, 'ec_option_hide_design_help_video', '0', 'yes'),
(167, 'ec_option_design_saved', '0', 'yes'),
(168, 'ec_option_amfphp_fix', '1', 'yes'),
(169, 'ec_option_track_user_clicks', '1', 'yes'),
(170, 'ec_option_cart_use_session_support', '0', 'yes'),
(171, 'ec_option_admin_display_sales_goal', '1', 'yes'),
(172, 'ec_option_admin_sales_goal', '1', 'yes'),
(173, 'ec_option_newsletter_done', '0', 'yes'),
(174, 'ec_option_allow_tracking', '-1', 'yes'),
(175, 'ec_option_weight', 'lbs', 'yes'),
(176, 'ec_option_base_currency', 'USD', 'yes'),
(177, 'ec_option_currency', '$', 'yes'),
(178, 'ec_option_currency_symbol_location', '1', 'yes'),
(179, 'ec_option_currency_negative_location', '1', 'yes'),
(180, 'ec_option_currency_decimal_symbol', '.', 'yes'),
(181, 'ec_option_currency_decimal_places', '2', 'yes'),
(182, 'ec_option_currency_thousands_seperator', ',', 'yes'),
(183, 'ec_option_show_currency_code', '0', 'yes'),
(184, 'ec_option_default_store_filter', '0', 'yes'),
(185, 'ec_option_default_payment_type', 'manual_bill', 'yes'),
(186, 'ec_option_shipping_type', 'price', 'yes'),
(187, 'ec_option_express_shipping_price', '9.99', 'yes'),
(188, 'ec_option_reg_code', '', 'yes'),
(189, 'ec_option_order_from_email', 'youremail@url.com', 'yes'),
(190, 'ec_option_password_from_email', 'youremail@url.com', 'yes'),
(191, 'ec_option_bcc_email_addresses', 'info@ozproductionservices.com', 'yes'),
(192, 'ec_option_use_state_dropdown', '1', 'yes'),
(193, 'ec_option_use_country_dropdown', '1', 'yes'),
(194, 'ec_option_estimate_shipping_zip', '1', 'yes'),
(195, 'ec_option_estimate_shipping_country', '1', 'yes'),
(196, 'ec_option_stylesheettype', '1', 'yes'),
(197, 'ec_option_googleanalyticsid', 'UA-XXXXXXX-X', 'yes'),
(198, 'ec_option_use_rtl', '0', 'yes'),
(199, 'ec_option_allow_guest', '1', 'yes'),
(200, 'ec_option_use_shipping', '0', 'yes'),
(201, 'ec_option_user_order_notes', '0', 'yes'),
(202, 'ec_option_terms_link', 'http://yoursite.com/termsandconditions', 'yes'),
(203, 'ec_option_privacy_link', 'http://yoursite.com/privacypolicy', 'yes'),
(204, 'ec_option_email_type', 'mail', 'yes'),
(205, 'ec_option_require_account_address', '0', 'yes'),
(206, 'ec_option_use_wp_mail', '0', 'yes'),
(207, 'ec_option_product_layout_type', 'grid_only', 'yes'),
(208, 'ec_option_show_featured_categories', '0', 'yes'),
(209, 'ec_option_product_filter_0', '1', 'yes'),
(210, 'ec_option_product_filter_1', '1', 'yes'),
(211, 'ec_option_product_filter_2', '1', 'yes'),
(212, 'ec_option_product_filter_3', '1', 'yes'),
(213, 'ec_option_product_filter_4', '1', 'yes'),
(214, 'ec_option_product_filter_5', '1', 'yes'),
(215, 'ec_option_product_filter_6', '1', 'yes'),
(216, 'ec_option_product_filter_7', '1', 'yes'),
(217, 'ec_option_show_giftcards', '0', 'yes'),
(218, 'ec_option_show_coupons', '0', 'yes'),
(219, 'ec_option_match_store_meta', '0', 'yes'),
(220, 'ec_option_use_old_linking_style', '1', 'yes'),
(221, 'ec_option_no_vat_on_shipping', '0', 'yes'),
(222, 'ec_option_display_as_catalog', '0', 'yes'),
(223, 'ec_option_addtocart_return_to_product', '0', 'yes'),
(224, 'ec_option_exchange_rates', 'EUR=.73,GBP=.6,JPY=101.9', 'yes'),
(225, 'ec_option_require_email_validation', '0', 'yes'),
(226, 'ec_option_display_country_top', '1', 'yes'),
(227, 'ec_option_use_address2', '1', 'yes'),
(228, 'ec_option_use_smart_states', '1', 'yes'),
(229, 'ec_option_show_account_subscriptions_link', '1', 'yes'),
(230, 'ec_option_gift_card_shipping_allowed', '0', 'yes'),
(231, 'ec_option_collect_shipping_for_subscriptions', '0', 'yes'),
(232, 'ec_option_skip_cart_login', '0', 'yes'),
(233, 'ec_option_use_contact_name', '1', 'yes'),
(234, 'ec_option_collect_user_phone', '1', 'yes'),
(235, 'ec_option_enable_company_name', '1', 'yes'),
(236, 'ec_option_skip_shipping_page', '0', 'yes'),
(237, 'ec_option_skip_reivew_screen', '0', 'yes'),
(238, 'ec_option_require_terms_agreement', '0', 'yes'),
(239, 'ec_option_show_menu_cart_icon', '0', 'yes'),
(240, 'ec_option_cart_menu_id', '0', 'yes'),
(241, 'ec_option_amazon_key', '', 'yes'),
(242, 'ec_option_amazon_secret', '', 'yes'),
(243, 'ec_option_amazon_bucket', '', 'yes'),
(244, 'ec_option_amazon_bucket_region', '', 'yes'),
(245, 'ec_option_deconetwork_url', '', 'yes'),
(246, 'ec_option_deconetwork_password', '', 'yes'),
(247, 'ec_option_tax_cloud_api_id', '', 'yes'),
(248, 'ec_option_tax_cloud_api_key', '', 'yes'),
(249, 'ec_option_tax_cloud_address', '', 'yes'),
(250, 'ec_option_tax_cloud_city', '', 'yes'),
(251, 'ec_option_tax_cloud_state', '', 'yes'),
(252, 'ec_option_tax_cloud_zip', '', 'yes'),
(253, 'ec_option_restrict_store', '0', 'yes'),
(254, 'ec_option_enable_user_notes', '0', 'yes'),
(255, 'ec_option_enable_newsletter_popup', '0', 'yes'),
(256, 'ec_option_use_estimate_shipping', '0', 'yes'),
(257, 'ec_option_use_custom_post_theme_template', '0', 'yes'),
(258, 'ec_option_show_email_on_receipt', '0', 'yes'),
(259, 'ec_option_show_breadcrumbs', '1', 'yes'),
(260, 'ec_option_show_model_number', '1', 'yes'),
(261, 'ec_option_show_categories', '1', 'yes'),
(262, 'ec_option_show_manufacturer', '1', 'yes'),
(263, 'ec_option_send_signup_email', '0', 'yes'),
(264, 'ec_option_show_magnification', '1', 'yes'),
(265, 'ec_option_show_large_popup', '1', 'yes'),
(266, 'ec_option_enable_product_paging', '1', 'yes'),
(267, 'ec_option_show_sort_box', '1', 'yes'),
(268, 'ec_option_customer_review_require_login', '0', 'yes'),
(269, 'ec_option_customer_review_show_user_name', '0', 'yes'),
(270, 'ec_option_hide_live_editor', '0', 'yes'),
(271, 'ec_option_hide_price_seasonal', '0', 'yes'),
(272, 'ec_option_hide_price_inquiry', '0', 'yes'),
(273, 'ec_option_enable_easy_canada_tax', '0', 'yes'),
(274, 'ec_option_collect_alberta_tax', '1', 'yes'),
(275, 'ec_option_collect_british_columbia_tax', '1', 'yes'),
(276, 'ec_option_collect_manitoba_tax', '1', 'yes'),
(277, 'ec_option_collect_new_brunswick_tax', '1', 'yes'),
(278, 'ec_option_collect_newfoundland_tax', '1', 'yes'),
(279, 'ec_option_collect_northwest_territories_tax', '1', 'yes'),
(280, 'ec_option_collect_nova_scotia_tax', '1', 'yes'),
(281, 'ec_option_collect_nunavut_tax', '1', 'yes'),
(282, 'ec_option_collect_ontario_tax', '1', 'yes'),
(283, 'ec_option_collect_prince_edward_island_tax', '1', 'yes'),
(284, 'ec_option_collect_quebec_tax', '1', 'yes'),
(285, 'ec_option_collect_saskatchewan_tax', '1', 'yes'),
(286, 'ec_option_collect_yukon_tax', '1', 'yes'),
(287, 'ec_option_collect_tax_on_shipping', '0', 'yes'),
(288, 'ec_option_show_multiple_vat_pricing', '0', 'yes'),
(289, 'ec_option_hide_cart_icon_on_empty', '0', 'yes'),
(290, 'ec_option_canada_tax_options', '0', 'yes'),
(291, 'ec_option_deconetwork_allow_blank_products', '0', 'yes'),
(292, 'ec_option_ship_items_seperately', '0', 'yes'),
(293, 'ec_option_default_dynamic_sizing', '1', 'yes'),
(294, 'ec_option_subscription_one_only', '1', 'yes'),
(295, 'ec_option_enable_gateway_log', '1', 'yes'),
(296, 'ec_option_enable_metric_unit_display', '0', 'yes'),
(297, 'ec_option_use_live_search', '1', 'yes'),
(298, 'ec_option_search_title', '1', 'yes'),
(299, 'ec_option_search_model_number', '1', 'yes'),
(300, 'ec_option_search_manufacturer', '1', 'yes'),
(301, 'ec_option_search_description', '1', 'yes'),
(302, 'ec_option_search_short_description', '1', 'yes'),
(303, 'ec_option_search_menu', '1', 'yes'),
(304, 'ec_option_show_image_on_receipt', '1', 'yes'),
(305, 'ec_option_search_by_or', '1', 'yes'),
(306, 'ec_option_custom_third_party', 'Gateway Name', 'yes'),
(307, 'ec_option_show_card_holder_name', '1', 'yes'),
(308, 'ec_option_show_stock_quantity', '1', 'yes'),
(309, 'ec_option_send_low_stock_emails', '0', 'yes'),
(310, 'ec_option_send_out_of_stock_emails', '0', 'yes'),
(311, 'ec_option_low_stock_trigger_total', '5', 'yes'),
(312, 'ec_option_show_delivery_days_live_shipping', '0', 'yes'),
(313, 'ec_option_model_number_extension', '-', 'yes'),
(314, 'ec_option_collect_vat_registration_number', '0', 'yes'),
(315, 'ec_option_validate_vat_registration_number', '0', 'yes'),
(316, 'ec_option_vatlayer_api_key', '', 'yes'),
(317, 'ec_option_vat_custom_rate', '0', 'yes'),
(318, 'ec_option_fedex_use_net_charge', '0', 'yes'),
(319, 'ec_option_static_ship_items_seperately', '0', 'yes'),
(320, 'ec_option_google_adwords_conversion_id', '', 'yes'),
(321, 'ec_option_google_adwords_language', 'en', 'yes'),
(322, 'ec_option_google_adwords_format', '3', 'yes'),
(323, 'ec_option_google_adwords_color', 'ffffff', 'yes'),
(324, 'ec_option_google_adwords_label', '', 'yes'),
(325, 'ec_option_google_adwords_currency', 'USD', 'yes'),
(326, 'ec_option_google_adwords_remarketing_only', 'false', 'yes'),
(327, 'ec_option_default_country', '0', 'yes'),
(328, 'ec_option_show_subscriber_feature', '0', 'yes'),
(329, 'ec_option_use_inquiry_form', '1', 'yes'),
(330, 'ec_option_order_use_smtp', '0', 'yes'),
(331, 'ec_option_order_from_smtp_host', '', 'yes'),
(332, 'ec_option_order_from_smtp_encryption_type', 'ssl', 'yes'),
(333, 'ec_option_order_from_smtp_port', '465', 'yes'),
(334, 'ec_option_order_from_smtp_username', '', 'yes'),
(335, 'ec_option_order_from_smtp_password', '', 'yes'),
(336, 'ec_option_password_use_smtp', '0', 'yes'),
(337, 'ec_option_password_from_smtp_host', '', 'yes'),
(338, 'ec_option_password_from_smtp_encryption_type', 'ssl', 'yes'),
(339, 'ec_option_password_from_smtp_port', '465', 'yes'),
(340, 'ec_option_password_from_smtp_username', '', 'yes'),
(341, 'ec_option_password_from_smtp_password', '', 'yes'),
(342, 'ec_option_minimum_order_total', '0.00', 'yes'),
(343, 'wpeasycart_abandoned_cart_automation', '0', 'yes'),
(344, 'ec_option_tiered_price_format', '1', 'yes'),
(345, 'ec_option_demo_data_installed', '0', 'yes'),
(346, 'ec_subscriptions_use_first_order_details', '0', 'yes'),
(347, 'ec_option_packing_slip_show_pricing', '1', 'yes'),
(348, 'ec_option_fb_pixel', '', 'yes'),
(349, 'ec_option_use_direct_deposit', '1', 'yes'),
(350, 'ec_option_direct_deposit_message', 'You have selected a manual payment method.', 'yes'),
(351, 'ec_option_use_affirm', '0', 'yes'),
(352, 'ec_option_affirm_public_key', '', 'yes'),
(353, 'ec_option_affirm_private_key', '', 'yes'),
(354, 'ec_option_affirm_financial_product', '', 'yes'),
(355, 'ec_option_affirm_sandbox_account', '0', 'yes'),
(356, 'ec_option_use_visa', '1', 'yes'),
(357, 'ec_option_use_delta', '0', 'yes'),
(358, 'ec_option_use_uke', '0', 'yes'),
(359, 'ec_option_use_discover', '1', 'yes'),
(360, 'ec_option_use_mastercard', '1', 'yes'),
(361, 'ec_option_use_mcdebit', '0', 'yes'),
(362, 'ec_option_use_amex', '1', 'yes'),
(363, 'ec_option_use_jcb', '0', 'yes'),
(364, 'ec_option_use_diners', '0', 'yes'),
(365, 'ec_option_use_laser', '0', 'yes'),
(366, 'ec_option_use_maestro', '0', 'yes'),
(367, 'ec_option_payment_process_method', '0', 'yes'),
(368, 'ec_option_payment_third_party', '0', 'yes'),
(369, 'ec_option_2checkout_thirdparty_sid', '', 'yes'),
(370, 'ec_option_2checkout_thirdparty_currency_code', 'USD', 'yes'),
(371, 'ec_option_2checkout_thirdparty_lang', 'en', 'yes'),
(372, 'ec_option_2checkout_thirdparty_purchase_step', 'payment-method', 'yes'),
(373, 'ec_option_2checkout_thirdparty_demo_mode', '0', 'yes'),
(374, 'ec_option_2checkout_thirdparty_sandbox_mode', '0', 'yes'),
(375, 'ec_option_2checkout_thirdparty_secret_word', '', 'yes'),
(376, 'ec_option_authorize_login_id', '', 'yes'),
(377, 'ec_option_authorize_trans_key', '', 'yes'),
(378, 'ec_option_authorize_test_mode', '0', 'yes'),
(379, 'ec_option_authorize_developer_account', '0', 'yes'),
(380, 'ec_option_authorize_use_legacy_url', '0', 'yes'),
(381, 'ec_option_authorize_currency_code', 'USD', 'yes'),
(382, 'ec_option_beanstream_merchant_id', '', 'yes'),
(383, 'ec_option_beanstream_api_passcode', '', 'yes'),
(384, 'ec_option_braintree_merchant_id', '', 'yes'),
(385, 'ec_option_braintree_merchant_account_id', '', 'yes'),
(386, 'ec_option_braintree_public_key', '', 'yes'),
(387, 'ec_option_braintree_private_key', '', 'yes'),
(388, 'ec_option_braintree_currency', 'USD', 'yes'),
(389, 'ec_option_braintree_environment', 'sandbox', 'yes'),
(390, 'ec_option_cardinal_processor_id', '', 'yes'),
(391, 'ec_option_cardinal_merchant_id', '', 'yes'),
(392, 'ec_option_cardinal_password', '', 'yes'),
(393, 'ec_option_cardinal_currency', '840,', 'yes'),
(394, 'ec_option_cardinal_test_mode', '0', 'yes'),
(395, 'ec_option_paypoint_merchant_id', '', 'yes'),
(396, 'ec_option_paypoint_vpn_password', '0', 'yes'),
(397, 'ec_option_paypoint_test_mode', '0', 'yes'),
(398, 'ec_option_chronopay_currency', '', 'yes'),
(399, 'ec_option_chronopay_product_id', '', 'yes'),
(400, 'ec_option_chronopay_shared_secret', '', 'yes'),
(401, 'ec_option_dwolla_thirdparty_key', '', 'yes'),
(402, 'ec_option_dwolla_thirdparty_secret', '', 'yes'),
(403, 'ec_option_dwolla_thirdparty_test_mode', '0', 'yes'),
(404, 'ec_option_dwolla_thirdparty_account_id', '812-xxx-xxxx', 'yes'),
(405, 'ec_option_versapay_id', '', 'yes'),
(406, 'ec_option_versapay_password', '', 'yes'),
(407, 'ec_option_versapay_language', 'en', 'yes'),
(408, 'ec_option_eway_use_rapid_pay', '0', 'yes'),
(409, 'ec_option_eway_customer_id', '', 'yes'),
(410, 'ec_option_eway_api_key', '', 'yes'),
(411, 'ec_option_eway_api_password', '', 'yes'),
(412, 'ec_option_eway_client_key', '', 'yes'),
(413, 'ec_option_eway_test_mode', '0', 'yes'),
(414, 'ec_option_eway_test_mode_success', '1', 'yes'),
(415, 'ec_option_firstdata_login_id', '', 'yes'),
(416, 'ec_option_firstdata_pem_file', '', 'yes'),
(417, 'ec_option_firstdata_host', 'secure.linkpt.net', 'yes'),
(418, 'ec_option_firstdata_port', '1129', 'yes'),
(419, 'ec_option_firstdata_test_mode', '0', 'yes'),
(420, 'ec_option_firstdata_use_ssl_cert', '0', 'yes'),
(421, 'ec_option_firstdatae4_exact_id', '', 'yes'),
(422, 'ec_option_firstdatae4_password', '', 'yes'),
(423, 'ec_option_firstdatae4_key_id', '', 'yes'),
(424, 'ec_option_firstdatae4_key', '', 'yes'),
(425, 'ec_option_firstdatae4_language', 'EN', 'yes'),
(426, 'ec_option_firstdatae4_currency', 'USD', 'yes'),
(427, 'ec_option_firstdatae4_test_mode', '0', 'yes'),
(428, 'ec_option_goemerchant_gateway_id', '', 'yes'),
(429, 'ec_option_goemerchant_processor_id', '', 'yes'),
(430, 'ec_option_goemerchant_trans_center_id', '', 'yes'),
(431, 'ec_option_intuit_oauth_version', '1', 'yes'),
(432, 'ec_option_intuit_app_token', '', 'yes'),
(433, 'ec_option_intuit_consumer_key', '', 'yes'),
(434, 'ec_option_intuit_consumer_secret', '', 'yes'),
(435, 'ec_option_intuit_currency', 'USD', 'yes'),
(436, 'ec_option_intuit_test_mode', '0', 'yes'),
(437, 'ec_option_intuit_realm_id', '', 'yes'),
(438, 'ec_option_intuit_access_token', '', 'yes'),
(439, 'ec_option_intuit_access_token_secret', '', 'yes'),
(440, 'ec_option_intuit_last_authorized', '0', 'yes'),
(441, 'ec_option_intuit_refresh_token', '', 'yes'),
(442, 'ec_option_nmi_3ds', '0', 'yes'),
(443, 'ec_option_nmi_api_key', '', 'yes'),
(444, 'ec_option_nmi_username', '', 'yes'),
(445, 'ec_option_nmi_password', '', 'yes'),
(446, 'ec_option_nmi_currency', 'USD', 'yes'),
(447, 'ec_option_nmi_processor_id', '', 'yes'),
(448, 'ec_option_nmi_ship_from_zip', '', 'yes'),
(449, 'ec_option_nmi_commodity_code', '', 'yes'),
(450, 'ec_option_nets_merchant_id', '', 'yes'),
(451, 'ec_option_nets_token', '', 'yes'),
(452, 'ec_option_nets_currency', 'USD', 'yes'),
(453, 'ec_option_nets_test_mode', '0', 'yes'),
(454, 'ec_option_migs_signature', '', 'yes'),
(455, 'ec_option_migs_access_code', '', 'yes'),
(456, 'ec_option_migs_merchant_id', '', 'yes'),
(457, 'ec_option_moneris_ca_store_id', '', 'yes'),
(458, 'ec_option_moneris_ca_api_token', '', 'yes'),
(459, 'ec_option_moneris_ca_test_mode', '0', 'yes'),
(460, 'ec_option_moneris_us_store_id', '', 'yes'),
(461, 'ec_option_moneris_us_api_token', '', 'yes'),
(462, 'ec_option_moneris_us_test_mode', '0', 'yes'),
(463, 'ec_option_payfast_merchant_id', '', 'yes'),
(464, 'ec_option_payfast_merchant_key', '', 'yes'),
(465, 'ec_option_payfast_passphrase', '', 'yes'),
(466, 'ec_option_payfast_sandbox', '0', 'yes'),
(467, 'ec_option_payfort_access_code', '', 'yes'),
(468, 'ec_option_payfort_merchant_id', '', 'yes'),
(469, 'ec_option_payfort_request_phrase', '', 'yes'),
(470, 'ec_option_payfort_response_phrase', '', 'yes'),
(471, 'ec_option_payfort_sha_type', 'sha256', 'yes'),
(472, 'ec_option_payfort_language', 'en', 'yes'),
(473, 'ec_option_payfort_currency_code', 'USD', 'yes'),
(474, 'ec_option_payfort_use_sadad', '0', 'yes'),
(475, 'ec_option_payfort_use_naps', '0', 'yes'),
(476, 'ec_option_payfort_sadad_olp', '', 'yes'),
(477, 'ec_option_payfort_use_currency_service', '0', 'yes'),
(478, 'ec_option_payfort_test_mode', '1', 'yes'),
(479, 'ec_option_payline_username', '', 'yes'),
(480, 'ec_option_payline_password', '', 'yes'),
(481, 'ec_option_payline_currency', 'USD', 'yes'),
(482, 'ec_option_payment_express_username', '', 'yes'),
(483, 'ec_option_payment_express_password', '', 'yes'),
(484, 'ec_option_payment_express_currency', 'NZD', 'yes'),
(485, 'ec_option_payment_express_developer_account', '0', 'yes'),
(486, 'ec_option_payment_express_thirdparty_username', '', 'yes'),
(487, 'ec_option_payment_express_thirdparty_key', '', 'yes'),
(488, 'ec_option_payment_express_thirdparty_currency', 'NZD', 'yes'),
(489, 'ec_option_paypal_use_sandbox', '0', 'yes'),
(490, 'ec_option_paypal_email', '', 'yes'),
(491, 'ec_option_paypal_enable_pay_now', '0', 'yes'),
(492, 'ec_option_paypal_enable_credit', '0', 'yes'),
(493, 'ec_option_paypal_sandbox_merchant_id', '', 'yes'),
(494, 'ec_option_paypal_sandbox_app_id', '', 'yes'),
(495, 'ec_option_paypal_sandbox_secret', '', 'yes'),
(496, 'ec_option_paypal_sandbox_access_token', '', 'yes'),
(497, 'ec_option_paypal_sandbox_access_token_expires', '0', 'yes'),
(498, 'ec_option_paypal_production_merchant_id', '', 'yes'),
(499, 'ec_option_paypal_production_app_id', '', 'yes'),
(500, 'ec_option_paypal_production_secret', '', 'yes'),
(501, 'ec_option_paypal_production_access_token', '', 'yes'),
(502, 'ec_option_paypal_production_access_token_expires', '0', 'yes'),
(503, 'ec_option_paypal_marketing_solution_cid_sandbox', '', 'yes'),
(504, 'ec_option_paypal_marketing_solution_cid_production', '', 'yes'),
(505, 'ec_option_paypal_currency_code', 'USD', 'yes'),
(506, 'ec_option_paypal_use_selected_currency', '0', 'yes'),
(507, 'ec_option_paypal_charset', 'UTF-8', 'yes'),
(508, 'ec_option_paypal_lc', 'US', 'yes'),
(509, 'ec_option_paypal_weight_unit', 'kgs', 'yes'),
(510, 'ec_option_paypal_collect_shipping', '0', 'yes'),
(511, 'ec_option_paypal_send_shipping_address', '0', 'yes'),
(512, 'ec_option_paypal_advanced_test_mode', '0', 'yes'),
(513, 'ec_option_paypal_advanced_vendor', '', 'yes'),
(514, 'ec_option_paypal_advanced_partner', '', 'yes'),
(515, 'ec_option_paypal_advanced_user', '', 'yes'),
(516, 'ec_option_paypal_advanced_password', '', 'yes'),
(517, 'ec_option_paypal_pro_test_mode', '0', 'yes'),
(518, 'ec_option_paypal_pro_vendor', '', 'yes'),
(519, 'ec_option_paypal_pro_partner', '', 'yes'),
(520, 'ec_option_paypal_pro_user', '', 'yes'),
(521, 'ec_option_paypal_pro_password', '', 'yes'),
(522, 'ec_option_paypal_pro_currency', 'USD', 'yes'),
(523, 'ec_option_paypal_payments_pro_test_mode', '0', 'yes'),
(524, 'ec_option_paypal_payments_pro_user', '', 'yes'),
(525, 'ec_option_paypal_payments_pro_password', '', 'yes'),
(526, 'ec_option_paypal_payments_pro_signature', '', 'yes'),
(527, 'ec_option_paypal_payments_pro_currency', 'USD', 'yes'),
(528, 'ec_option_skrill_merchant_id', '', 'yes'),
(529, 'ec_option_skrill_company_name', '', 'yes'),
(530, 'ec_option_skrill_email', '', 'yes'),
(531, 'ec_option_skrill_language', 'EN', 'yes'),
(532, 'ec_option_realex_thirdparty_type', 'legacy', 'yes'),
(533, 'ec_option_realex_thirdparty_merchant_id', '', 'yes'),
(534, 'ec_option_realex_thirdparty_secret', '', 'yes'),
(535, 'ec_option_realex_thirdparty_account', 'redirect', 'yes'),
(536, 'ec_option_realex_thirdparty_currency', 'GBP', 'yes'),
(537, 'ec_option_realex_merchant_id', '', 'yes'),
(538, 'ec_option_realex_secret', '', 'yes'),
(539, 'ec_option_realex_currency', 'GBP', 'yes'),
(540, 'ec_option_realex_3dsecure', '0', 'yes'),
(541, 'ec_option_realex_test_mode', '0', 'yes'),
(542, 'ec_option_redsys_merchant_code', '', 'yes'),
(543, 'ec_option_redsys_terminal', '', 'yes'),
(544, 'ec_option_redsys_currency', '978', 'yes'),
(545, 'ec_option_redsys_key', '', 'yes'),
(546, 'ec_option_redsys_test_mode', '0', 'yes'),
(547, 'ec_option_sagepay_vendor', '', 'yes'),
(548, 'ec_option_sagepay_currency', 'USD', 'yes'),
(549, 'ec_option_sagepay_simulator', '0', 'yes'),
(550, 'ec_option_sagepay_testmode', '0', 'yes'),
(551, 'ec_option_sagepayus_mid', '999999999997', 'yes'),
(552, 'ec_option_sagepayus_mkey', 'A1B2C3D4E5F6', 'yes'),
(553, 'ec_option_sagepayus_application_id', 'SAGETEST1', 'yes'),
(554, 'ec_option_sagepay_paynow_za_service_key', '', 'yes'),
(555, 'ec_option_securenet_id', '', 'yes'),
(556, 'ec_option_securenet_secure_key', '', 'yes'),
(557, 'ec_option_securenet_use_sandbox', '0', 'yes'),
(558, 'ec_option_securepay_merchant_id', '', 'yes'),
(559, 'ec_option_securepay_password', '', 'yes'),
(560, 'ec_option_securepay_currency', 'AUD', 'yes'),
(561, 'ec_option_securepay_test_mode', '0', 'yes'),
(562, 'ec_option_skrill_currency_code', 'USD', 'yes'),
(563, 'ec_option_square_access_token', '', 'yes'),
(564, 'ec_option_square_token_expires', '', 'yes'),
(565, 'ec_option_square_application_id', '', 'yes'),
(566, 'ec_option_square_location_id', '0', 'yes'),
(567, 'ec_option_square_currency', 'USD', 'yes'),
(568, 'ec_option_psigate_store_id', 'teststore', 'yes'),
(569, 'ec_option_psigate_passphrase', 'psigate1234', 'yes'),
(570, 'ec_option_psigate_test_mode', '1', 'yes'),
(571, 'ec_option_use_proxy', '0', 'yes'),
(572, 'ec_option_proxy_address', '0', 'yes'),
(573, 'ec_option_stripe_api_key', '', 'yes'),
(574, 'ec_option_stripe_public_api_key', '', 'yes'),
(575, 'ec_option_stripe_currency', 'USD', 'yes'),
(576, 'ec_option_stripe_order_create_customer', '0', 'yes'),
(577, 'ec_option_stripe_connect_use_sandbox', '0', 'yes'),
(578, 'ec_option_stripe_connect_sandbox_access_token', '', 'yes'),
(579, 'ec_option_stripe_connect_sandbox_refresh_token', '', 'yes'),
(580, 'ec_option_stripe_connect_sandbox_publishable_key', '', 'yes'),
(581, 'ec_option_stripe_connect_sandbox_user_id', '', 'yes'),
(582, 'ec_option_stripe_connect_production_access_token', '', 'yes'),
(583, 'ec_option_stripe_connect_production_refresh_token', '', 'yes'),
(584, 'ec_option_stripe_connect_production_publishable_key', '', 'yes'),
(585, 'ec_option_stripe_connect_production_user_id', '', 'yes'),
(586, 'ec_option_virtualmerchant_ssl_merchant_id', '', 'yes'),
(587, 'ec_option_virtualmerchant_ssl_user_id', '', 'yes'),
(588, 'ec_option_virtualmerchant_ssl_pin', '', 'yes'),
(589, 'ec_option_virtualmerchant_currency', 'USD', 'yes'),
(590, 'ec_option_virtualmerchant_demo_account', '0', 'yes'),
(591, 'ec_option_language', 'en-us', 'yes');
INSERT INTO `ops_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(592, 'ec_option_language_data', '{"en-us":{"label":"US English","options":{"sort_bar":{"label":"Product Page Sort Menu","options":{"sort_by_price_low":{"title":"Product Page Sort Box Item 1","value":"Price Low-High"},"sort_by_price_high":{"title":"Product Page Sort Box Item 2","value":"Price High-Low"},"sort_by_title_a":{"title":"Product Page Sort Box Item 3","value":"Title A-Z"},"sort_by_title_z":{"title":"Product Page Sort Box Item 4","value":"Title Z-A"},"sort_by_newest":{"title":"Product Page Sort Box Item 5","value":"Newest"},"sort_by_rating":{"title":"Product Page Sort Box Item 6","value":"Best Rating"},"sort_by_most_viewed":{"title":"Product Page Sort Box Item 7","value":"Most Viewed"},"sort_default":{"title":"Product Page Sort Box Default","value":"Default Sorting"}}},"product_page":{"label":"Product Page","options":{"product_quick_view":{"title":"Product Quick View Label","value":"Quickview"},"product_design_now":{"title":"Product Design Now Label","value":"DESIGN NOW"},"product_no_results":{"title":"Product Page No Results","value":"No Results Found"},"product_items_per_page":{"title":"Product Page Items per Page","value":"Items Per Page:"},"product_paging_page":{"title":"Product Page Page in Page a of b","value":"Page"},"product_paging_of":{"title":"Product Page of in Page a of b","value":"of"},"product_showing":{"title":"Product Page Num Results Showing","value":"Showing"},"product_all":{"title":"Product Page Num Results all","value":"all"},"product_results":{"title":"Product Page Num Results","value":"Results"},"product_view_cart":{"title":"Product Page View Cart","value":"View Cart"},"product_product_added_note":{"title":"Product Page Added to Cart Note","value":"Product successfully added to your cart."},"product_product_view_per_page":{"title":"Product Page View in Per Page","value":"View"},"product_page_restricted_line_1":{"title":"Restricted Product Line 1","value":"OPEN ACCESS IS NOT ALLOWED FOR THE STORE"},"product_page_restricted_line_2":{"title":"Restricted Product Line 2 Prior to Link","value":"Please log in or create an account by"},"product_page_restricted_link_text":{"title":"Restricted Product Link Text","value":"clicking here"},"product_page_restricted_line_3":{"title":"Restricted Product Line 2 Post Link","value":". Your account may need to be verified before you will be able to access the store."},"product_page_start_trial_1":{"title":"Start Your Trial Text Part 1","value":"Start Your"},"product_page_start_trial_2":{"title":"Start Your Trial Text Part 2","value":"Day Free Trial Today!"},"product_inc_vat_text":{"title":"Including VAT Text","value":"inc VAT"},"product_excluding_vat_text":{"title":"Excluding VAT Text","value":"excluding VAT"}}},"quick_view":{"label":"Quick View Panel","options":{"quick_view_gift_card_message":{"title":"Gift Card Message Title","value":"SPECIAL MESSAGE"},"quick_view_gift_card_to_name":{"title":"Gift Card To Name Title","value":"TO"},"quick_view_gift_card_from_name":{"title":"Gift Card From Name Title","value":"FROM"},"quick_view_quantity":{"title":"Quantity Title","value":"QUANTITY"},"quick_view_remaining":{"title":"label in x items remaining","value":"remaining"},"quick_view_add_to_cart":{"title":"Add to Cart Button","value":"ADD TO CART"},"quick_view_select_options":{"title":"Product details missing options error text","value":"Please select all options for this product."},"quick_view_view_full_details":{"title":"Link Text View Full Product Details","value":"View Full Product Details"}}},"product_details":{"label":"Product Details Page","options":{"product_details_donation_label":{"title":"Your Donation Label","value":"Your Donation: "},"product_details_minimum_donation":{"title":"Your Donation Minimum Label","value":"minimum donation:"},"product_details_gift_card_message":{"title":"Store Gift Card Message","value":"Include a Message"},"product_details_gift_card_to_name":{"title":"Gift Card To Name Title","value":"TO"},"product_details_gift_card_from_name":{"title":"Gift Card From Name Title","value":"FROM"},"product_details_quantity":{"title":"Quantity Label","value":"Quantity"},"product_details_remaining":{"title":"x remaining","value":"remaining"},"product_details_x_of_y":{"title":"page text divider","value":"of"},"product_details_model_number":{"title":"model number text","value":"model number"},"product_details_reduced_price":{"title":"reduced price text","value":"REDUCED PRICE YOU SAVE"},"product_details_description":{"title":"Description Title","value":"Description"},"product_details_customer_reviews":{"title":"Customer Reviews Title","value":"Customer Reviews"},"product_details_specifications":{"title":"Specifications Title","value":"Specifications"},"product_details_customer_rating":{"title":"Customer Rating","value":"Overall Customer Rating"},"product_details_review_based_on":{"title":"Review Based ON","value":"based on "},"product_details_review":{"title":"Review","value":"review"},"product_details_review_no_reviews":{"title":"Product Details, Customer Reviews, No Reviews","value":"There are no reviews yet, submit yours in the box provided."},"product_details_add_to_cart":{"title":"Add to Cart button text","value":"ADD TO CART"},"product_details_added_to_cart":{"title":"Checkout Now button text","value":"CHECKOUT NOW"},"product_details_select_options":{"title":"Select Options Button","value":"Select Options"},"product_details_out_of_stock":{"title":"Out of Stock Text","value":"OUT OF STOCK"},"product_details_sign_up_now":{"title":"Sign Up Now Text","value":"SIGN UP NOW"},"product_details_inquiry_title":{"title":"Inquiry Title","value":"Submit Product Inquiry"},"product_details_inquiry_error":{"title":"Inquiry Error Text","value":"Please fill out all required fields"},"product_details_inquiry_name":{"title":"Inquiry Name","value":"*Your Name:"},"product_details_inquiry_email":{"title":"Inquiry Email","value":"*Your Email:"},"product_details_inquiry_message":{"title":"Inquire Button Text","value":"*Your Message:"},"product_details_inquire":{"title":"Inquire Button Text","value":"SUBMIT INQUIRY"},"product_details_inquiry_send_copy":{"title":"Inquiry - Send Copy","value":"Please send me a copy"},"product_details_inquiry_thank_you":{"title":"Inquiry Thank You Note","value":"Thank you for submitting a product inquiry and we will get back to you as soon as possible."},"product_details_minimum_quantity_text1":{"title":"Min Purchase Amount Text Part 1","value":"Minimum purchase amount of"},"product_details_minimum_quantity_text2":{"title":"Min Purchase Amount Text Part 2","value":"is required"},"product_details_maximum_quantity_text1":{"title":"Max Purchase Amount Text Part 1","value":"Maximum purchase amount of"},"product_details_maximum_quantity_text2":{"title":"Max Purchase Amount Text Part 2","value":"is allowed"},"product_details_handling_fee_notice1":{"title":"One Time Handling Fee Part 1","value":"*A one-time handling fee of"},"product_details_handling_fee_notice2":{"title":"One Time Handling Fee Part 2","value":"will be added to your shipping total at checkout."},"product_details_handling_fee_each_notice1":{"title":"Handling Fee Each Part 1","value":"*A handling fee of"},"product_details_handling_fee_each_notice2":{"title":"Handling Fee Each Part 2","value":"per item will be added to your shipping total at checkout."},"product_details_signup_fee_notice1":{"title":"One Time Sign-up Fee Part 1","value":"*A one-time sign-up fee of"},"product_details_signup_fee_notice2":{"title":"One Time Sign-up Fee Part 2","value":"will be added to your first payment total."},"product_details_home_link":{"title":"Home Breadcrumbs Link","value":"Home"},"product_details_store_link":{"title":"Store Breadcrumbs Link","value":"Shop"},"product_details_reviews_text":{"title":"Store Reviews Count","value":"Reviews"},"product_details_gift_card_recipient_name":{"title":"Store Gift Card To Name","value":"Recipient''s Name"},"product_details_gift_card_recipient_email":{"title":"Store Gift Card To Email","value":"Recipient''s Email"},"product_details_gift_card_sender_name":{"title":"Store Gift Card Sender''s Name","value":"Sender''s Name"},"product_details_donation_amount":{"title":"Store Donation Amount","value":"Donation Amount"},"product_details_loading_options":{"title":"Store Loading Options","value":"Loading Available Options"},"product_details_missing_option":{"title":"Store Missing Option","value":"Missing Option:"},"product_details_maximum_quantity":{"title":"Store Maximum Quantity Exceeded","value":"Maximum quantity exceeded"},"product_details_one_time_addition1":{"title":"Store One Time Addition 1","value":"A one-time price of"},"product_details_one_time_addition2":{"title":"Store One Time Addition 2","value":"will be added to your order."},"product_details_left_in_stock":{"title":"Store Left in Stock","value":"Left in Stock"},"product_details_categories":{"title":"Store Categories","value":"Categories:"},"product_details_manufacturer":{"title":"Store Manufacturer","value":"Manufacturer:"},"product_details_related_products":{"title":"Store Related Products","value":"Related Products"},"product_details_tier_buy":{"title":"Tiered Pricing buy x at y","value":"Buy"},"product_details_tier_buy_at":{"title":"Tiered Pricing buy x at y","value":"at"},"product_details_tier_or_more":{"title":"Tiered Pricing buy x or more","value":"or more"},"product_details_tier_each":{"title":"Tiered Pricing buy x at y each","value":"each"},"product_details_gift_card_error":{"title":"Gift Card Option Error Text","value":"Missing Gift Card Options"},"product_details_donation_error":{"title":"Donation Amount Error Text","value":"Invalid donation amount entered. Please enter a minimum value of"},"product_details_your_price":{"title":"Your Price:","value":"Your Price:"},"product_details_backorder_until":{"title":"Backorder UNTIL date, until","value":"until"},"product_details_backordered":{"title":"BACKORDERED until date","value":"Backordered"},"product_details_some_backordered":{"title":"Some BACKORDERED","value":"Some items are backordered, only"},"product_details_some_backordered_remaining":{"title":"Some BACKORDERED remaining","value":"remaining."},"product_details_backordered_message":{"title":"Backordered message in cart","value":"Some items in your cart are currently out of stock and on backorder."},"product_details_vat_included":{"title":"Including VAT","value":"Including"},"product_details_vat_excluded":{"title":"Excluding VAT","value":"Excluding"},"product_details_backorder_button":{"title":"Backorder Button","value":"BACKORDER NOW"},"product_details_reviews_for_text":{"title":"Reviews for","value":"Reviews for"},"product_details_as_low_as":{"title":"As low as","value":"As low as"}}},"customer_review":{"label":"Customer Review Panel","options":{"customer_review_title":{"title":"Product Details Rating Box Title","value":"Write a Review"},"customer_review_choose_rating":{"title":"Product Details Rating Box Choose Rating","value":"choose a rating"},"customer_review_enter_title":{"title":"Product Details Rating Box Choose Title","value":"enter a review title"},"customer_review_enter_description":{"title":"Product Details Rating Box Choose Description","value":"enter a review description"},"customer_review_select_option":{"title":"Product Details Default Select Option Text","value":"select an option"},"customer_review_close_button":{"title":"Product Details Rating Box Close Button","value":"Close"},"customer_review_submit_button":{"title":"Product Details Rating Box Submit Button","value":"Submit Review"},"product_details_write_a_review":{"title":"Product Details Write a Review Button","value":"Write a Review"},"product_details_add_a_review_for":{"title":"Store Add a Review","value":"Add a review for"},"product_details_your_review_title":{"title":"Store Review title","value":"Your Review Title"},"product_details_your_review_rating":{"title":"Store Review Rating","value":"Your Rating"},"product_details_your_review_message":{"title":"Store Review Message","value":"Your Review"},"product_details_your_review_submit":{"title":"Store Review Submit","value":"SUBMIT"},"product_details_review_submitted_button":{"title":"Product Details Review Submitted Button","value":"REVIEW SUBMITTED"},"product_details_submitting_review":{"title":"Store Submitting Review","value":"Submitting Your Review, Please Wait"},"product_details_review_submitted":{"title":"Store Review Submitted","value":"Your Review Has Been Submitted Successfully"},"product_details_review_log_in_first":{"title":"Log in to Review","value":"Please sign in or create an account to submit a review for this product."},"product_details_review_anonymous_reviewer":{"title":"Anonymous Reviewer Name","value":"Anonymous"},"review_error":{"title":"Store Review Error Missing Data","value":"You must include a title, rating, and message in your review."}}},"cart":{"label":"Cart","options":{"cart_title":{"title":"Checkout Cart Title","value":"SHOPPING CART"},"cart_checkout_details_title":{"title":"Checkout Details Title","value":"CHECKOUT DETAILS"},"cart_submit_payment_title":{"title":"Submit Payment Title","value":"SUBMIT PAYMENT"},"your_cart_title":{"title":"Your Cart Title","value":"YOUR CART"},"cart_empty_cart":{"title":"Checkout, Empty Cart Message","value":"There are no items in your cart."},"cart_checkout":{"title":"Checkout Page, Checkout Button","value":"Checkout"},"cart_continue_shopping":{"title":"Checkout Page, Continue Shopping Link","value":"Continue Shopping"},"cart_header_column1":{"title":"Cart Header, Column 1","value":"Product"},"cart_header_column2":{"title":"Cart Header, Column 2","value":"Details"},"cart_header_column3":{"title":"Cart Header, Column 3","value":"Price"},"cart_header_column4":{"title":"Cart Header, Column 4","value":"Quantity"},"cart_header_column5":{"title":"Cart Header, Column 5","value":"Total"},"cart_header_column6":{"title":"Cart Header, Column 6","value":"Actions"},"cart_item_vat_text":{"title":"Cart Item, Included Vat","value":"VAT @"},"cart_item_update_button":{"title":"Cart Item, Update Button","value":"UPDATE"},"cart_item_remove_button":{"title":"Cart Item, Remove Button","value":"REMOVE"},"cart_item_adjustment":{"title":"Cart Item, Adjustment per Item","value":"per item"},"cart_item_new_price_option":{"title":"Cart Item, Price Override Text","value":"Item Price of"},"cart_menu_icon_label":{"title":"Cart in Menu, Item","value":"Item"},"cart_menu_icon_label_plural":{"title":"Cart in Menu, Items","value":"Items"},"deconetwork_edit":{"title":"Deconetwork, Edit Design","value":"Edit Design"},"cart_order_adjustment":{"title":"Cart Item, Adjustment per Order","value":"per order"},"cart_return_to_store":{"title":"Empty Cart, Return to store","value":"RETURN TO STORE"},"cartitem_max_error":{"title":"Max Quantity Exceeded Error","value":"Maximum Quantity Exceeded"},"cartitem_min_error":{"title":"Min Quantity Required Error","value":"Minimum Quantity Required"},"cart_please_wait":{"title":"Cart Please Wait","value":"Please Wait"},"cart_item_adjustment_per_character":{"title":"Cart Per Character Adjustment","value":"Per Character"},"cart_minimum_purchase_amount1":{"title":"A Minimum Order Amount of","value":"A Minimum Order Amount of"},"cart_minimum_purchase_amount2":{"title":"Min Required Text 2","value":"is Required"}}},"cart_form_notices":{"label":"Cart - Form Error Notices","options":{"cart_notice_is_required":{"title":"Cart, Notice, Is Required Field","value":"is a required field"},"cart_notice_item_must_match":{"title":"Cart, Notice, Items Must Match","value":"must match"},"cart_notice_billing_first_name":{"title":"Cart, Notice, Billing, First Name","value":"Billing First Name"},"cart_notice_billing_last_name":{"title":"Cart, Notice, Billing, Last Name","value":"Billing Last Name"},"cart_notice_billing_address":{"title":"Cart, Notice, Billing, Address","value":"Billing Address"},"cart_notice_billing_city":{"title":"Cart, Notice, Billing, City","value":"Billing City"},"cart_notice_billing_state":{"title":"Cart, Notice, Billing, State","value":"Billing State"},"cart_notice_billing_zip_code":{"title":"Cart, Notice, Billing, Zip Code","value":"Billing Zip Code"},"cart_notice_billing_country":{"title":"Cart, Notice, Billing, Country","value":"Billing Country"},"cart_notice_billing_phone":{"title":"Cart, Notice, Billing, Phone","value":"Billing Phone"},"cart_notice_shipping_first_name":{"title":"Cart, Notice, Shipping, First Name","value":"Shipping First Name"},"cart_notice_shipping_last_name":{"title":"Cart, Notice, Shipping, Last Name","value":"Shipping Last Name"},"cart_notice_shipping_address":{"title":"Cart, Notice, Shipping, Address","value":"Shipping Address"},"cart_notice_shipping_city":{"title":"Cart, Notice, Shipping, City","value":"Shipping City"},"cart_notice_shipping_state":{"title":"Cart, Notice, Shipping, State","value":"Shipping State"},"cart_notice_shipping_zip_code":{"title":"Cart, Notice, Shipping, Zip Code","value":"Shipping Zip Code"},"cart_notice_shipping_country":{"title":"Cart, Notice, Shipping, Country","value":"Shipping Country"},"cart_notice_shipping_phone":{"title":"Cart, Notice, Shipping, Phone","value":"Shipping Phone"},"cart_notice_contact_first_name":{"title":"Cart, Notice, Contact First Name","value":"Contact First Name"},"cart_notice_contact_last_name":{"title":"Cart, Notice, Contact Last Name","value":"Contact Last Name"},"cart_notice_email":{"title":"Cart, Notice, Email","value":"Email"},"cart_notice_retype_email":{"title":"Cart, Notice, Retype Email","value":"Retype Email"},"cart_notice_emails_match":{"title":"Cart, Notice, Emails Match","value":"Emails"},"cart_notice_password":{"title":"Cart, Notice, Password","value":"Password"},"cart_notice_retype_password":{"title":"Cart, Notice, Retype Password","value":"Retype Password"},"cart_notice_passwords_match":{"title":"Cart, Notice, Passwords Match","value":"Passwords"},"cart_notice_length_error":{"title":"Cart, Notice, Password Format","value":"Please enter a password of at least 6 characters"},"cart_notice_payment_card_type":{"title":"Cart, Notice, Card Type","value":"Card Type"},"cart_notice_payment_card_holder_name":{"title":"Cart, Notice, Card Holder Required","value":"Card Holder Name"},"cart_notice_payment_card_number":{"title":"Cart, Notice, Card Number Required","value":"Card Number"},"cart_notice_payment_card_number_error":{"title":"Cart, Notice, Card Number Required","value":"The credit card number entered is invalid"},"cart_notice_payment_card_exp_month":{"title":"Cart, Notice, EXP Month Required","value":"Expiration Month"},"cart_notice_payment_card_exp_year":{"title":"Cart, Notice, EXP Year Required","value":"Expiration Year"},"cart_notice_payment_card_code":{"title":"Cart, Notice, Security Code Required","value":"Security Code"},"cart_notice_please_enter_your":{"title":"Cart, Notice, Please Enter Your","value":"Please enter your"},"cart_notice_please_select_your":{"title":"Cart, Notice, Please Select Your","value":"Please select your"},"cart_notice_please_enter_valid":{"title":"Cart, Notice, Please Enter Valid","value":"Please enter a valid"},"cart_notice_emails_do_not_match":{"title":"Cart, Notice, Email Do Not Match","value":"Your email addresses do not match"},"cart_notice_passwords_do_not_match":{"title":"Cart, Notice, Passwords Do Not Match","value":"Your passwords do not match"},"cart_notice_checkout_details_errors":{"title":"Cart, Notice, Checkout Details Errors","value":"Please correct the errors in your checkout details"},"cart_notice_payment_accept_terms":{"title":"Cart, Notice, Please Accept Terms","value":"Please agree to the terms and conditions"},"cart_notice_payment_correct_errors":{"title":"Cart, Notice, Payment Correct Errors","value":"Please correct the errors to your checkout information above"}}},"cart_process":{"label":"Cart - Progress Bar","options":{"cart_process_cart_link":{"title":"Cart, Progress, Cart","value":"Cart"},"cart_process_shipping_link":{"title":"Cart, Progress, Address","value":"Address"},"cart_process_review_link":{"title":"Cart, Progress, Cart","value":"Review\\/Pay"},"cart_process_complete_link":{"title":"Cart, Progress, Complete","value":"Complete"}}},"cart_coupons":{"label":"Cart - Enter Coupons and Gift Cards","options":{"cart_coupon_title":{"title":"Cart, Coupon Title","value":"Coupon"},"cart_gift_card_title":{"title":"Cart, Gift Card Title","value":"Gift Card"},"cart_coupon_sub_title":{"title":"Cart, Coupons Sub-Title","value":"Redeem coupon codes or gift cards by entering them below."},"cart_redeem_gift_card":{"title":"Cart, Redeem Gift Card Button","value":"Redeem Gift Card"},"cart_apply_coupon":{"title":"Cart, Apply Coupon Button","value":"Apply Coupon"},"cart_invalid_coupon":{"title":"Cart, Invalid Coupon","value":"Not a valid coupon code"},"cart_not_applicable_coupon":{"title":"Cart, Not Applicable Coupon","value":"Coupon code does not apply to this product"},"cart_max_exceeded_coupon":{"title":"Cart, Max Coupon Use Exceeded","value":"Max uses exceeded"},"cart_invalid_giftcard":{"title":"Cart, Invalid Gift Card","value":"Not a valid gift card number"},"cart_enter_coupon":{"title":"Cart, Enter Coupon Hint","value":"Enter Coupon Code"},"cart_enter_gift_code":{"title":"Cart, Enter Gift Card Hint","value":"Enter Gift Card"},"cart_coupon_expired":{"title":"Cart, Coupon Code Expired","value":"Coupon Has Expired"}}},"cart_estimate_shipping":{"label":"Cart - Estimate Shipping","options":{"cart_estimate_shipping_title":{"title":"Cart, Estimate Shipping Title","value":"Estimate Shipping Costs"},"cart_estimate_shipping_sub_title":{"title":"Cart, Estimate Shipping Sub Title","value":"Please enter your zip code to estimate shipping costs."},"cart_estimate_shipping_input_country_label":{"title":"Cart, Estimate Shipping, Input Label Country","value":"Country:"},"cart_estimate_shipping_select_one":{"title":"Cart, Estimate Shipping, Select one for country box","value":"Select One"},"cart_estimate_shipping_input_label":{"title":"Cart, Estimate Shipping, Input Label Zip Code","value":"Zip Code:"},"cart_estimate_shipping_button":{"title":"Cart, Estimate Shipping Button","value":"Estimate Shipping"},"cart_estimate_shipping_standard":{"title":"Cart, Standard Shipping Label","value":"Standard Shipping"},"cart_estimate_shipping_express":{"title":"Cart, Express Shipping Label","value":"Ship Express"},"cart_estimate_shipping_error":{"title":"Cart, Mismatch Postal Code Error","value":"Your postal code does not match our store''s country. Shipping rates will be determined after you have entered your shipping information"},"cart_estimate_shipping_hint":{"title":"Cart, Estimate Shipping Hint","value":"Enter Zip Code"},"delivery_in":{"title":"Cart, Live Shipping Delivery In (Days)","value":"delivery in"},"delivery_days":{"title":"Cart, Live Shipping (Delivery In) Days","value":"days"}}},"cart_totals":{"label":"Cart - Cart Totals","options":{"cart_totals_title":{"title":"Cart, Cart Totals Title","value":"Cart Totals"},"cart_totals_subtotal":{"title":"Cart, Cart Totals, Subtotal","value":"Cart Subtotal"},"cart_totals_shipping":{"title":"Cart, Cart Totals, Shipping","value":"Shipping"},"cart_totals_tax":{"title":"Cart, Cart Totals, Tax","value":"Tax"},"cart_totals_discounts":{"title":"Cart, Cart Totals, Discounts","value":"Discounts"},"cart_totals_vat":{"title":"Cart, Cart Totals, VAT","value":"VAT"},"cart_totals_duty":{"title":"Cart, Cart Totals, Duty","value":"Duty"},"cart_totals_grand_total":{"title":"Cart, Cart Totals, Grand Total","value":"Grand Total"},"cart_totals_label":{"title":"Cart Totals Label","value":"Cart Totals"}}},"cart_login":{"label":"Cart - Cart Login","options":{"cart_login_title":{"title":"Cart, Login Title","value":"Returning Customer"},"cart_login_sub_title":{"title":"Cart, Login Sub Title","value":"To checkout with an existing account, please sign in below."},"cart_login_email_label":{"title":"Cart, Login, Email Label","value":"Email Address"},"cart_login_password_label":{"title":"Cart, Login, Password Label","value":"Password"},"cart_login_button":{"title":"Cart, Login Button","value":"SIGN IN"},"cart_login_forgot_password_link":{"title":"Cart, Login, Forgot Password Link","value":"Forgot Your Password?"},"cart_login_account_information_title":{"title":"Cart, Login Complete Title","value":"Account Information"},"cart_login_account_information_text":{"title":"Cart, Login Complete Text","value":"You are currently checking out as "},"cart_login_account_a_guest_text":{"title":"Cart, Login Complete, a guest","value":"a guest"},"cart_login_account_information_text2":{"title":"Cart, Login Complete Text Part 2","value":" to checkout with a different account."},"cart_login_account_information_logout_link":{"title":"Cart, Login Complete, Logout Link","value":"click here"}}},"cart_guest_checkout":{"label":"Cart - Guest Checkout","options":{"cart_guest_title":{"title":"Cart, Login, Guest Checkout Title","value":"Checkout Without an Account"},"cart_guest_sub_title":{"title":"Cart, Login, Guest Checkout Sub Title","value":"Not registered? Checkout without an existing account."},"cart_guest_message":{"title":"Cart, Login, Guest Checkout Message","value":"No account? Continue as a guest.<br\\/>You will have an opportunity to create an account later."},"cart_guest_button":{"title":"Cart, Login, Guest Checkout Button","value":"CONTINUE TO CHECKOUT"}}},"cart_subscription_guest_checkout":{"label":"Cart - Subscription No Account Checkout","options":{"cart_subscription_guest_title":{"title":"Cart, Login, Subscription No Account Title","value":"Create Account on Checkout"},"cart_subscription_guest_sub_title":{"title":"Cart, Login, Subscription No Account Sub Title","value":"Not registered? Checkout without an existing account."},"cart_subscription_guest_message":{"title":"Cart, Login, Subscription No Account Message","value":"No account? No problem.<br\\/>You will have an opportunity to create an account later."},"cart_subscription_guest_button":{"title":"Cart, Login, Guest Checkout Button","value":"CONTINUE TO CHECKOUT"}}},"cart_billing_information":{"label":"Cart - Billing Information","options":{"cart_billing_information_title":{"title":"Cart, Billing Information Title","value":"Billing Information"},"cart_billing_information_first_name":{"title":"Cart, Billing, First Name Label","value":"First Name"},"cart_billing_information_last_name":{"title":"Cart, Billing, Last Name Label","value":"Last Name"},"cart_billing_information_address":{"title":"Cart, Billing, Address Label","value":"Address"},"cart_billing_information_address2":{"title":"Cart, Billing, Address 2 Label","value":"Address 2"},"cart_billing_information_city":{"title":"Cart, Billing, City Label","value":"City"},"cart_billing_information_state":{"title":"Cart, Billing, State Label","value":"State"},"cart_billing_information_select_state":{"title":"Cart, Billing, Default No State Selected","value":"Select a State"},"cart_billing_information_select_province":{"title":"Cart, Billing, Default No Province Selected","value":"Select a Province"},"cart_billing_information_select_county":{"title":"Cart, Billing, Default No County Selected","value":"Select a County"},"cart_billing_information_select_other":{"title":"Cart, Billing, Default None Selected","value":"Select One"},"cart_billing_information_country":{"title":"Cart, Billing, Country Label","value":"Country"},"cart_billing_information_select_country":{"title":"Cart, Billing, Country Menu, Default None Selected","value":"Select a Country"},"cart_billing_information_zip":{"title":"Cart, Billing, Zip Label","value":"Zip Code"},"cart_billing_information_phone":{"title":"Cart, Billing, Phone Label","value":"Phone"},"cart_billing_information_ship_to_this":{"title":"Cart, Billing, Use Billing Radio","value":"Ship to this address"},"cart_billing_information_ship_to_different":{"title":"Cart, Billing, Use Shipping Radio","value":"SHIP TO DIFFERENT ADDRESS"},"cart_billing_information_company_name":{"title":"Cart, Billing, Company Name Label","value":"Company Name"},"cart_billing_information_vat_registration_number":{"title":"Cart, Billing, VAT Registration Number Label","value":"VAT Registration Number"}}},"cart_shipping_information":{"label":"Cart - Shipping Information","options":{"cart_shipping_information_title":{"title":"Cart, Shipping Information Title","value":"Shipping Information"},"cart_shipping_information_first_name":{"title":"Cart, Shipping, First Name Label","value":"First Name"},"cart_shipping_information_last_name":{"title":"Cart, Shipping, Last Name Label","value":"Last Name"},"cart_shipping_information_address":{"title":"Cart, Shipping, Address Label","value":"Address"},"cart_shipping_information_address2":{"title":"Cart, Shipping, Address 2 Label","value":"Address 2"},"cart_shipping_information_city":{"title":"Cart, Shipping, City Label","value":"City"},"cart_shipping_information_state":{"title":"Cart, Shipping, State Label","value":"State"},"cart_shipping_information_select_state":{"title":"Cart, Shipping, Default No State Selected","value":"Select a State"},"cart_shipping_information_select_province":{"title":"Cart, Shipping, Default No Province Selected","value":"Select a Province"},"cart_shipping_information_select_county":{"title":"Cart, Shipping, Default No County Selected","value":"Select a County"},"cart_shipping_information_select_other":{"title":"Cart, Shipping, Default None Selected","value":"Select One"},"cart_shipping_information_country":{"title":"Cart, Shipping, Country Label","value":"Country"},"cart_shipping_information_select_country":{"title":"Cart, Shipping, Country Menu, Default None Selected","value":"Select a Country"},"cart_shipping_information_zip":{"title":"Cart, Shipping, Zip Label","value":"Zip Code"},"cart_shipping_information_phone":{"title":"Cart, Shipping, Phone Label","value":"Phone"},"cart_shipping_information_company_name":{"title":"Cart, Shipping, Company Name Label","value":"Company Name"}}},"cart_contact_information":{"label":"Cart - Contact Information","options":{"cart_contact_information_title":{"title":"Cart, Contact Information Title","value":"Contact Information"},"cart_contact_information_first_name":{"title":"Cart, Contact Information, First Name Label","value":"First Name"},"cart_contact_information_last_name":{"title":"Cart, Contact Information, Last Name Label","value":"Last Name"},"cart_contact_information_email":{"title":"Cart, Contact Information, Email Label","value":"Email"},"cart_contact_information_retype_email":{"title":"Cart, Contact Information, Retype Email Label","value":"Retype Email"},"cart_contact_information_create_account":{"title":"Cart, Contact Information, Create Account Label","value":"Create Account"},"cart_contact_information_password":{"title":"Cart, Contact Information, Password Label","value":"Password"},"cart_contact_information_retype_password":{"title":"Cart, Contact Information, Retype Password Label","value":"Retype Password"},"cart_contact_information_subscribe":{"title":"Cart, Contact Information, Subscribe Label","value":"Would you like to receive emails and updates from us?"},"cart_contact_information_continue_button":{"title":"Cart, Contact Information, Continue Button","value":"CONTINUE"},"cart_contact_information_continue_payment":{"title":"Cart, Contact Information, Continue to Payment","value":"CONTINUE TO PAYMENT"},"cart_contact_information_continue_shipping":{"title":"Cart, Contact Information, Continue to Shipping","value":"CONTINUE TO SHIPPING"}}},"cart_shipping_method":{"label":"Cart - Shipping Method","options":{"cart_shipping_method_title":{"title":"Cart, Shipping Method, Title","value":"Shipping Method"},"cart_shipping_method_continue_button":{"title":"Cart, Shipping Method Continue Button","value":"CONTINUE"},"cart_shipping_method_please_select_one":{"title":"Cart, Shipping please select one","value":"Please select a shipping method"},"cart_shipping_update_shipping":{"title":"Cart, Update Shipping","value":"Update Shipping"},"cart_shipping_no_rates_available":{"title":"Cart, No Shipping Rates Available","value":"There are no available shipping methods in your area. Contact us to complete your order."}}},"cart_address_review":{"label":"Cart - Address Review","options":{"cart_address_review_title":{"title":"Cart, Address Information Title","value":"Address Information"},"cart_address_review_billing_title":{"title":"Cart, Address Review, Billing Title","value":"Billing Address"},"cart_address_review_shipping_title":{"title":"Cart, Address Review, Shipping Title","value":"Shipping Address"},"cart_address_review_edit_link":{"title":"Cart, Address Review, Edit Link","value":"Edit Address or Shipping Options"},"cart_address_review_edit_link2":{"title":"Cart, Address Review, Edit Link","value":"Edit Address Information"}}},"cart_payment_information":{"label":"Cart - Payment Information","options":{"cart_payment_information_title":{"title":"Cart, Payment Information Title","value":"Payment Information"},"cart_payment_information_manual_payment":{"title":"Cart, Payment Information, Manual Payment Label","value":"Pay by Direct Deposit"},"cart_payment_information_affirm":{"title":"Cart, Payment Information, Affirm Payment Label","value":"Pay with Easy Monthly Payments"},"cart_payment_information_third_party":{"title":"Cart, Payment Information, Third Party Payment Label","value":"Pay Using"},"cart_payment_information_third_party_first":{"title":"Cart, Payment Information, Third Party Text, Part 1","value":"You will be redirected to"},"cart_payment_information_third_party_second":{"title":"Cart, Payment Information, Third Party Text, Part 2","value":"when you click submit order to complete your payment."},"cart_payment_information_credit_card":{"title":"Cart, Payment Information, Credit Card Payment Label","value":"Pay by Credit Card"},"cart_payment_information_payment_method":{"title":"Cart, Payment Information, Payment Method Label","value":"Payment Method"},"cart_payment_information_card_holder_name":{"title":"Cart, Payment Information, Card Holder Label","value":"Card Holder Name"},"cart_payment_information_card_number":{"title":"Cart, Payment Information, Card Number Label","value":"Card Number"},"cart_payment_information_expiration_date":{"title":"Cart, Payment Information, Expiration Date Label","value":"Expiration Date"},"cart_payment_information_security_code":{"title":"Cart, Payment Information, Security Code Label","value":"Security Code"},"cart_payment_information_checkout_text":{"title":"Cart, Payment Information, Checkout Text","value":"By submitting your order, you are agreeing to the [terms]terms and conditions[\\/terms] and [privacy]privacy policy[\\/privacy] of our website.  Once you click submit order, the checkout process is complete and no changes can be made to your order. Thank you for shopping with us!"},"cart_payment_information_review_checkout_text":{"title":"Cart, Payment Information, Review Checkout Text","value":"Before submitting your order and making it final, you will be given a chance to review and confirm the final totals. Please click the review order button when you are ready to review, confirm, and submit your order."},"cart_payment_information_review_order_button":{"title":"Cart, Payment Information, Review Order Button","value":"REVIEW ORDER"},"cart_payment_information_submit_order_button":{"title":"Cart, Payment Information, Submit Order Button","value":"SUBMIT ORDER"},"cart_payment_information_cancel_order_button":{"title":"Cart, Payment Information, Edit Order Button","value":"EDIT ORDER"},"cart_payment_information_order_notes_title":{"title":"Cart, Payment Information, Customer Notes Title","value":"ORDER NOTES"},"cart_payment_information_order_notes_message":{"title":"Cart, Payment Information, Customer Notes Message","value":"If you have any special requests for your order please enter them here."},"cart_payment_information_review_title":{"title":"Cart, Review Cart Title","value":"REVIEW YOUR CART"},"cart_payment_information_edit_cart_link":{"title":"Cart, edit cart items link","value":"edit cart items"},"cart_payment_information_edit_billing_link":{"title":"Cart, edit billing link","value":"edit billing address"},"cart_payment_information_edit_shipping_link":{"title":"Cart, edit shipping link","value":"edit shipping address"},"cart_payment_information_edit_shipping_method_link":{"title":"Cart, edit shipping link","value":"edit shipping method"},"cart_payment_review_totals_title":{"title":"Cart, Review Total Title","value":"REVIEW CART TOTALS"},"cart_payment_review_agree":{"title":"Cart, Review Agree Text","value":"I agree to the terms and conditions of this site."},"cart_change_payment_method":{"title":"Cart, Change Payment Method","value":"change payment method"}}},"cart_success":{"label":"Order Receipt","options":{"cart_payment_receipt_order_details_link":{"title":"Email Receipt, Link to Account","value":"View Order Details"},"cart_payment_receipt_subscription_details_link":{"title":"Email Receipt, Link to Subscription","value":"View Subscription Details"},"cart_payment_receipt_title":{"title":"Email Receipt Title","value":"Order Confirmation - Order Number"},"cart_payment_complete_line_1":{"title":"Cart, Payment Complete, Line 1","value":"Dear:"},"cart_payment_complete_line_2":{"title":"Cart, Payment Complete, Line 2","value":"Thank you for your order! Your Reference Number is:"},"cart_payment_complete_line_3":{"title":"Cart, Payment Complete, Line 3","value":"Below is a summary of order. You can check the status of your order and all the details by visiting our website and logging into your account."},"cart_payment_complete_line_4":{"title":"Cart, Payment Complete, Line 4","value":"Please keep this as a record of your order!"},"cart_payment_complete_line_5":{"title":"Cart, Payment Complete, Line 5","value":"Click Here to View Your Membership Content"},"cart_payment_complete_click_here":{"title":"Cart, Payment Complete, Click Here","value":"Click Here"},"cart_payment_complete_to_view_order":{"title":"Cart, Payment Complete, To View Order","value":"to View Your Order"},"cart_payment_complete_billing_label":{"title":"Cart, Payment Complete, Billing Label","value":"Billing Address"},"cart_payment_complete_shipping_label":{"title":"Cart, Payment Complete, Shipping Label","value":"Shipping Address"},"cart_payment_complete_details_header_1":{"title":"Cart, Payment Complete, Header 1","value":"Product"},"cart_payment_complete_details_header_2":{"title":"Cart, Payment Complete, Header 2","value":"Qty"},"cart_payment_complete_details_header_3":{"title":"Cart, Payment Complete, Header 3","value":"Price"},"cart_payment_complete_details_header_4":{"title":"Cart, Payment Complete, Header 4","value":"Ext Price"},"cart_payment_complete_order_totals_subtotal":{"title":"Cart, Payment Complete, Subtotal","value":"Subtotal"},"cart_payment_complete_order_totals_shipping":{"title":"Cart, Payment Complete, Shipping","value":"Shipping"},"cart_payment_complete_order_totals_tax":{"title":"Cart, Payment Complete, Tax","value":"Tax"},"cart_payment_complete_order_totals_discount":{"title":"Cart, Payment Complete, Discount","value":"Discount"},"cart_payment_complete_order_totals_vat":{"title":"Cart, Payment Complete, VAT","value":"VAT @"},"cart_payment_complete_order_totals_duty":{"title":"Cart, Payment Complete, Duty","value":"Duty"},"cart_payment_complete_order_totals_grand_total":{"title":"Cart, Payment Complete, Order Total","value":"Order Total"},"cart_payment_complete_bottom_line_1":{"title":"Cart, Payment Complete, Bottom Line 1","value":"Please double check your order when you receive it and let us know immediately if there are any concerns or issues. We always value your business and hope you enjoy your product."},"cart_payment_complete_bottom_line_2":{"title":"Cart, Payment Complete, Bottom Line 2","value":"Thank you very much!"},"cart_success_thank_you_title":{"title":"Cart, Payment Complete, Thank You Title","value":"Thank you for your order"},"cart_success_order_number_is":{"title":"Cart, Payment Complete, Your Order Number","value":"Order number is:"},"cart_success_will_receive_email":{"title":"Cart, Payment Complete, Will receive email","value":"You will receive an email confirmation shortly at"},"cart_success_print_receipt_text":{"title":"Cart, Payment Complete, Print Receipt Link Text","value":"Print Receipt"},"cart_success_save_order_text":{"title":"Cart, Payment Complete, Save Your Info","value":"Save your information for next time"},"cart_success_create_password":{"title":"Cart, Payment Complete, Create Password","value":"Create Password:"},"cart_success_verify_password":{"title":"Cart, Payment Complete, Verify Password","value":"Verify Password:"},"cart_success_password_hint":{"title":"Cart, Payment Complete, Password Hint","value":"(6-12 Characters)"},"cart_success_create_account":{"title":"Cart, Payment Complete, Create Account","value":"Create Account"},"cart_giftcard_receipt_title":{"title":"Cart, Gift Card Email Subject","value":"Gift Card Delivery"},"cart_giftcard_receipt_header":{"title":"Cart, Gift Card Header","value":"Your Gift Card"},"cart_giftcard_receipt_to":{"title":"Cart, Gift Card To","value":"To"},"cart_giftcard_receipt_from":{"title":"Cart, Gift Card From","value":"From"},"cart_giftcard_receipt_id":{"title":"Cart, Gift Card ID Label","value":"Your Gift Card ID"},"cart_giftcard_receipt_amount":{"title":"Cart, Gift Card Amount","value":"Gift Card Total"},"cart_giftcard_receipt_message":{"title":"Cart, Gift Card Message","value":"You can redeem your gift card during checkout at"},"cart_downloads_available":{"title":"Cart Success, Downloads Available","value":"Your order includes downloads and are available in your account."},"cart_downloads_unavailable":{"title":"Cart Success, Downloads Unavailable","value":"Your order includes downloads, but your payment has not yet been approved. Once payment has been approved your downloads will be available in your account."},"cart_downloads_click_to_go":{"title":"Cart Success, Downloads Click to go Text","value":"Click to go there now."},"cart_success_view_downloads":{"title":"Cart Success, View Downloads","value":"View Downloads"}}},"subscription_trial":{"label":"Subscription Trial Email","options":{"trial_message_1":{"title":"Subscription Trial Part 1","value":"Thank you for signing up! Your"},"trial_message_2":{"title":"Subscription Trial Part 2","value":"day free trial of"},"trial_message_3":{"title":"Subscription Trial Part 3","value":"has now started! If you have any questions or concerns during your trial, please contact us, we are happy to help."},"trial_message_4":{"title":"Subscription Trial Part 4","value":"We hope you stick with us, but if you decide this isn''t for you, you may cancel at any time prior to the end of your trial to prevent being charged. To cancel, log into your account, click manage subscriptions, view your subscription, and cancel on the details page."},"trial_message_link":{"title":"View Subscription Information","value":"View Subscription Information"},"subscription_trial_email_title":{"title":"Subscription Trial Email Title","value":"Your Trial Has Started"},"trial_ending_message_1":{"title":"Subscription Trial Ending Part 1","value":"Your"},"trial_ending_message_2":{"title":"Subscription Trial Part 2","value":"day free trial of"},"trial_ending_message_3":{"title":"Subscription Trial Part 3","value":"is ending in only 3 days! If you have any questions or concerns, please contact us! We are happy to help."},"trial_ending_message_4":{"title":"Subscription Trial Part 4","value":"We hope you stick with us, but if you decide this isn''t for you, you may cancel at any time prior to the end of your trial to prevent being charged. To cancel, log into your account, click manage subscriptions, view your subscription, and cancel on the details page."},"trial_ending_message_link":{"title":"View Subscription Information","value":"View Subscription Information"},"subscription_trial_ending_email_title":{"title":"Subscription Trial Email Title","value":"Your Trial is Ending in 3 Days"}}},"subscription_ended":{"label":"Subscription Ended Email","options":{"ended_message_1":{"title":"Subscription Ended Part 1","value":"Your subscription with us has either ended, been cancelled, or expired due to too many failed payments."},"ended_message_2":{"title":"Subscription Ended Part 2","value":"We are sorry to see you go and hope you return. If this subscription was cancelled by accident, you may click the link below to purchase a new subscription."},"ended_details":{"title":"Subscription Ended Details","value":"The following subscription has ended:"},"emded_message_link":{"title":"Start a New Subscription","value":"Start a New Subscription"},"subscription_ended_email_title":{"title":"Subscription Ended Email Title","value":"Your Subscription Has Ended"}}},"account_login":{"label":"Account - Login","options":{"account_login_title":{"title":"Account, Login Title","value":"Returning Customer"},"account_login_sub_title":{"title":"Account, Login Sub Title","value":"Sign in below to access your existing account."},"account_login_email_label":{"title":"Account, Login, Email Label","value":"Email Address"},"account_login_password_label":{"title":"Account, Login, Password Label","value":"Password"},"account_login_button":{"title":"Account, Login Button","value":"SIGN IN"},"account_login_forgot_password_link":{"title":"Account, Login, Forgot Password Link","value":"Forgot Your Password?"},"account_login_account_information_title":{"title":"Account, Login Complete Title","value":"Account Information"},"account_login_account_information_text":{"title":"Account, Login Complete Text","value":"You are currently checking out as Demo User. To checkout with a different account,"},"account_login_account_information_logout_link":{"title":"Account, Login Complete, Logout Link","value":"click here"},"account_new_user_title":{"title":"Account, Login, New User Title","value":"New User"},"account_new_user_sub_title":{"title":"Account, Login, New User Sub Title","value":"Not registered? Click the button below"},"account_new_user_message":{"title":"Account, Login, New User Message","value":"No account? Create an account to take full advantage of this website."},"account_new_user_button":{"title":"Account, Login, New User Button","value":"CREATE ACCOUNT"}}},"account_forgot_password":{"label":"Account - Forgot Password","options":{"account_forgot_password_title":{"title":"Account, Retrieve Password Title","value":"Retrieve Your Password"},"account_forgot_password_email_label":{"title":"Account, Retrieve Password Email Label","value":"Email"},"account_forgot_password_button":{"title":"Account, Retrieve Password Button","value":"RETRIEVE PASSWORD"}}},"account_validation_email":{"label":"Account - Validation Email","options":{"account_validation_email_message":{"title":"Account, Short Validate Email Message","value":"To activate your account, please click the link below."},"account_validation_email_title":{"title":"Account, Activate Your Account Title","value":"Please Activate Your Account"},"account_validation_email_link":{"title":"Account, Activate Your Account Link","value":"Click Here to Activate"}}},"account_register":{"label":"Account - Register","options":{"account_register_title":{"title":"Account, Register Title","value":"Create an Account"},"account_register_first_name":{"title":"Account, Register, First Name Label","value":"First Name"},"account_register_last_name":{"title":"Account, Register, Last Name Label","value":"Last Name"},"account_register_email":{"title":"Account, Register, Email Label","value":"Email Address"},"account_register_password":{"title":"Account, Register, Password Label","value":"Password"},"account_register_retype_password":{"title":"Account, Register, Re-type Password Label","value":"Re-type Password"},"account_register_subscribe":{"title":"Account, Register, Subscribe Text","value":"Would you like to subscribe to our email list?"},"account_register_button":{"title":"Account, Register Button","value":"REGISTER"},"account_billing_information_user_notes":{"title":"Account, Billing, User Notes","value":"Additional Information about your Registration"},"account_register_email_title":{"title":"Account, Register Email Title","value":"New EasyCart Registration"},"account_register_email_message":{"title":"Account, Register Email Message","value":"New Registration with Email Address:"}}},"account_navigation":{"label":"Account - Navigation","options":{"account_navigation_title":{"title":"Account, Navigation Title","value":"Account Navigation"},"account_navigation_basic_inforamtion":{"title":"Account, Navigation, Basic Information Link","value":"basic information"},"account_navigation_orders":{"title":"Account, Navigation, View Orders Link","value":"view orders"},"account_navigation_password":{"title":"Account, Navigation, Change Password Link","value":"change password"},"account_navigation_subscriptions":{"title":"Account, Navigation, Subscriptions Link","value":"manage subscriptions"},"account_navigation_payment_methods":{"title":"Account, Navigation, Payments Link","value":"manage payment methods"},"account_navigation_sign_out":{"title":"Account, Navigation, Sign Out Link","value":"sign out"},"account_navigation_dashboard":{"title":"Account, Navigation, Dashboard Link","value":"account dashboard"},"account_navigation_billing_information":{"title":"Account, Navigation, Billing Link","value":"billing information"},"account_navigation_shipping_information":{"title":"Account, Navigation, Shipping Link","value":"shipping information"}}},"account_dashboard":{"label":"Account - Dashboard","options":{"account_dashboard_recent_orders_title":{"title":"Account, Dashboard, Recent Orders Title","value":"Recent Orders"},"account_dashboard_recent_orders_none":{"title":"Account, Dashboard, Recent Order, No Orders","value":"No Orders Have Been Placed"},"account_dashboard_all_orders_linke":{"title":"Account, Dashboard, All Orders Link","value":"VIEW ALL ORDERS"},"account_dashboard_order_placed":{"title":"Account, Dashboard, Order Placed Label","value":"ORDER PLACED"},"account_dashboard_order_total":{"title":"Account, Dashboard, Total Label","value":"TOTAL"},"account_dashboard_order_ship_to":{"title":"Account, Dashboard, Ship To Label","value":"SHIP TO"},"account_dashboard_order_order_label":{"title":"Account, Dashboard, Order Label","value":"ORDER"},"account_dashboard_order_view_details":{"title":"Account, Dashboard, View Details Link","value":"View Details"},"account_dashboard_order_buy_item_again":{"title":"Account, Dashboard, Buy Item Again","value":"Buy Item Again"},"account_dashboard_preference_title":{"title":"Account, Dashboard, Preference Title","value":"Account Preferences"},"account_dashboard_email_title":{"title":"Account, Dashboard, Email Title","value":"Your Primary Email"},"account_dashboard_email_edit_link":{"title":"Account, Dashboard, Email Edit Link","value":"edit"},"account_dashboard_email_note":{"title":"Account, Dashboard, Email Note","value":"(This is used to log in)"},"account_dashboard_billing_title":{"title":"Account, Dashboard, Billing Title","value":"Billing Address"},"account_dashboard_billing_link":{"title":"Account, Dashboard, Billing Edit Link","value":"edit billing address"},"account_dashboard_shipping_title":{"title":"Account, Dashboard, Shipping Title","value":"Shipping Address"},"account_dashboard_shipping_link":{"title":"Account, Dashboard, Shipping Edit Link","value":"edit shipping address"}}},"account_personal_information":{"label":"Account - Edit Personal Information","options":{"account_personal_information_title":{"title":"Account, Personal Information Title","value":"Personal Information"},"account_personal_information_sub_title":{"title":"Account, Personal Information Sub Title","value":"Please note: Changes made to your account information, including shipping addresses, will only affect new orders. All previously placed orders will be sent to the address listed on the date of purchase."},"account_personal_information_first_name":{"title":"Account, Personal Information, First Name Label","value":"First Name"},"account_personal_information_last_name":{"title":"Account, Personal Information, Last Name Label","value":"Last Name"},"account_personal_information_email":{"title":"Account, Personal Information, Email Label","value":"Email Address"},"account_personal_information_subscribe":{"title":"Account, Personal Information, Subscribe Label","value":"Would you like to subscribe to our newsletter?"},"account_personal_information_update_button":{"title":"Account, Personal Information, Update Button","value":"UPDATE"},"account_personal_information_cancel_link":{"title":"Account, Personal Information, Cancel Link","value":"CANCEL"}}},"account_password":{"label":"Account - Edit Password","options":{"account_password_title":{"title":"Account, Password Title","value":"Edit Your Password"},"account_password_sub_title":{"title":"Account, Password Sub Title","value":"Please note: Once you change your password you will be required to use the new password each time you log into our site."},"account_password_current_password":{"title":"Account, Password, Current Password Label","value":"Current Password"},"account_password_new_password":{"title":"Account, Password, New Password Label","value":"New Password"},"account_password_retype_new_password":{"title":"Account, Password, Retype New Password Label","value":"Retype New Password"},"account_password_update_button":{"title":"Account, Password, Update Button","value":"UPDATE"},"account_password_cancel_button":{"title":"Account, Password, Cancel Link","value":"CANCEL"}}},"account_billing_information":{"label":"Account - Edit Billing Information","options":{"account_billing_information_title":{"title":"Account, Billing Information Title","value":"Billing Information"},"account_billing_information_first_name":{"title":"Account, Billing Information, First Name Label","value":"First Name"},"account_billing_information_last_name":{"title":"Account, Billing Information, Last Name Label","value":"Last Name"},"account_billing_information_address":{"title":"Account, Billing Information, Address Label","value":"Address"},"account_billing_information_address2":{"title":"Account, Billing Information, Address 2 Label","value":"Address 2"},"account_billing_information_city":{"title":"Account, Billing Information, City Label","value":"City"},"account_billing_information_state":{"title":"Account, Billing Information, State Label","value":"State"},"account_billing_information_default_no_state":{"title":"Account, Billing Information, No State Label","value":"Select a State"},"account_billing_information_zip":{"title":"Account, Billing Information, Zip Label","value":"Zip Code"},"account_billing_information_country":{"title":"Account, Billing Information, Country Label","value":"Country"},"account_billing_information_default_no_country":{"title":"Account, Billing Information, No Country Label","value":"Select a Country"},"account_billing_information_phone":{"title":"Account, Billing Information, Phone Label","value":"Phone"},"account_billing_information_update_button":{"title":"Account, Billing Information, Update Button","value":"UPDATE"},"account_billing_information_cancel":{"title":"Account, Billing Information, Cancel Link","value":"CANCEL"}}},"account_shipping_information":{"label":"Account - Edit Shipping Information","options":{"account_shipping_information_title":{"title":"Account, Shipping Information Title","value":"Shipping Information"},"account_shipping_information_first_name":{"title":"Account, Shipping Information, First Name Label","value":"First Name"},"account_shipping_information_last_name":{"title":"Account, Shipping Information, Last Name Label","value":"Last Name"},"account_shipping_information_address":{"title":"Account, Shipping Information, Address Label","value":"Address"},"account_shipping_information_address2":{"title":"Account, Shipping Information, Address 2 Label","value":"Address 2"},"account_shipping_information_city":{"title":"Account, Shipping Information, City Label","value":"City"},"account_shipping_information_state":{"title":"Account, Shipping Information, State Label","value":"State"},"account_shipping_information_default_no_state":{"title":"Account, Shipping Information, No State Label","value":"Select a State"},"account_shipping_information_zip":{"title":"Account, Shipping Information, Zip Label","value":"Zip Code"},"account_shipping_information_country":{"title":"Account, Shipping Information, Country Label","value":"Country"},"account_shipping_information_default_no_country":{"title":"Account, Shipping Information, No Country Label","value":"Select a Country"},"account_shipping_information_phone":{"title":"Account, Shipping Information, Phone Label","value":"Phone"},"account_shipping_information_update_button":{"title":"Account, Shipping Information, Update Button","value":"UPDATE"},"account_shipping_information_cancel":{"title":"Account, Shipping Information, Cancel Link","value":"CANCEL"}}},"account_orders":{"label":"Account - Orders","options":{"account_orders_title":{"title":"Account, Orders Title","value":"Your Order History"},"account_orders_header_1":{"title":"Account, Orders, Header 1","value":"ID"},"account_orders_header_2":{"title":"Account, Orders, Header 2","value":"Date"},"account_orders_header_3":{"title":"Account, Orders, Header 3","value":"Total"},"account_orders_header_4":{"title":"Account, Orders, Header 4","value":"Status"},"account_orders_view_order_button":{"title":"Account, Orders, View Order Button","value":"VIEW ORDER"}}},"account_forgot_password_email":{"label":"Account - Forgot Password Recovery Email","options":{"account_forgot_password_email_title":{"title":"Account, Password Recovery Email, Email Title","value":"Your New Password"},"account_forgot_password_email_dear":{"title":"Account, Password Recovery Email, Dear User","value":"Dear"},"account_forgot_password_email_your_new_password":{"title":"Account, Password Recovery Email, Your New Password","value":"Your new password is:"},"account_forgot_password_email_change_password":{"title":"Account, Password Recovery Email, Change Password Text","value":"Be sure to log into your account and change your password to something you can remember."},"account_forgot_password_email_thank_you":{"title":"Account, Password Recovery Email, Thank You Text","value":"Thank You Very much!"}}},"account_order_details":{"label":"Account - Order Details","options":{"account_orders_details_order_info_title":{"title":"Account, Orders Details, Order Information Title","value":"Order Information"},"account_orders_details_your_order_title":{"title":"Account, Orders Details, Your Order Title","value":"Your Order"},"account_orders_details_order_number":{"title":"Account, Orders Details, Order Number Label","value":"Order Number:"},"account_orders_details_order_date":{"title":"Account, Orders Details, Order Date Label","value":"Order Date:"},"account_orders_details_order_status":{"title":"Account, Orders Details, Order Status Label","value":"Order Status:"},"account_orders_details_order_tracking":{"title":"Account, Orders Details, Order Tracking Label","value":"Tracking Number:"},"account_orders_details_shipping_method":{"title":"Account, Orders Details, Shipping Method Label","value":"Shipping Method:"},"account_orders_details_coupon_code":{"title":"Account, Orders Details, Coupon Code Label","value":"Coupon Code:"},"account_orders_details_gift_card":{"title":"Account, Orders Details, Gift Card Label","value":"Gift Card:"},"account_orders_details_view_subscription":{"title":"Account, Orders Details, View Subscription","value":"View Subscription Details"},"account_orders_details_billing_label":{"title":"Account, Orders Details, Billing Label","value":"Billing Address"},"account_orders_details_shipping_label":{"title":"Account, Orders Details, Shipping Label","value":"Shipping Address"},"account_orders_details_payment_method":{"title":"Account, Orders Details, Payment Method","value":"Payment Method:"},"account_order_details_payment_method_manual":{"title":"Account, Order Details, Payment Method, Manual Bill Label","value":"Direct Deposit"},"account_orders_details_subtotal":{"title":"Account, Orders Details, Subtotal","value":"Sub Total:"},"account_orders_details_tax_total":{"title":"Account, Orders Details, Tax Total","value":"Tax Total:"},"account_orders_details_shipping_total":{"title":"Account, Orders Details, Shipping Total","value":"Shipping Total:"},"account_orders_details_discount_total":{"title":"Account, Orders Details, Discount Total","value":"Discount Total:"},"account_orders_details_duty_total":{"title":"Account, Orders Details, Duty Total","value":"Duty Total:"},"account_orders_details_vat_total":{"title":"Account, Orders Details, VAT Total","value":"VAT Total:"},"account_orders_details_refund_total":{"title":"Account, Orders Details, Refund Total","value":"Refunded Amount:"},"account_orders_details_grand_total":{"title":"Account, Orders Details, Grand Total","value":"Order Total:"},"account_orders_details_header_1":{"title":"Account, Orders Details, Header 1","value":"Product"},"account_orders_details_header_2":{"title":"Account, Orders Details, Header 2","value":"Options"},"account_orders_details_header_3":{"title":"Account, Orders Details, Header 3","value":"Price"},"account_orders_details_header_4":{"title":"Account, Orders Details, Header 4","value":"Quantity"},"account_orders_details_header_5":{"title":"Account, Orders Details, Header 5","value":"Total"},"account_orders_details_gift_message":{"title":"Account, Orders Details, Gift Card Message","value":"message: "},"account_orders_details_gift_from":{"title":"Account, Orders Details, Gift Card From","value":"from: "},"account_orders_details_gift_to":{"title":"Account, Orders Details, Gift Card To","value":"to: "},"account_orders_details_gift_card_id":{"title":"Account, Orders Details, Gift Card ID","value":"Gift Card Code: "},"account_orders_details_print_online":{"title":"Account, Orders Details, Print Online Link","value":"Print Online"},"account_orders_details_download":{"title":"Account, Orders Details, Download","value":"Download"},"account_orders_details_downloads_used":{"title":"Account, Orders Details, Downloads Used","value":"Downloads Used"},"account_orders_details_downloads_expire_time":{"title":"Account, Orders Details, Download Expiration","value":"Download Expires On:"},"account_orders_details_your_codes":{"title":"Account, Orders Details, Your Codes","value":"Your Code(s):"},"complete_payment":{"title":"Account, Orders Details, Complete Payment","value":"Complete Order Payment"},"order_status_status_not_found":{"title":"Account, Orders Status, Status Not Found","value":"Status Not Found"},"order_status_order_shipped":{"title":"Account, Orders Status, Order Shipped","value":"Order Shipped"},"order_status_order_confirmed":{"title":"Account, Orders Status, Order Confirmed","value":"Order Confirmed"},"order_status_order_on_hold":{"title":"Account, Orders Status, Order on Hold","value":"Order on Hold"},"order_status_order_started":{"title":"Account, Orders Status, Order Started","value":"Order Started"},"order_status_card_approved":{"title":"Account, Orders Status, Card Approved","value":"Card Approved"},"order_status_card_denied":{"title":"Account, Orders Status, Card Denied","value":"Card Denied"},"order_status_third_party_pending":{"title":"Account, Orders Status, Third Party Pending","value":"Third Party Pending"},"order_status_third_party_error":{"title":"Account, Orders Status, Third Party Error","value":"Third Party Error"},"order_status_third_party_approved":{"title":"Account, Orders Status, Third Party Approved","value":"Third Party Approved"},"order_status_ready_for_pickup":{"title":"Account, Orders Status, Ready for Pickup","value":"Ready for Pickup"},"order_status_pending_approval":{"title":"Account, Orders Status, Pending Approval","value":"Pending Approval"},"order_status_direct_deposit_pending":{"title":"Account, Orders Status, Direct Deposit Pending","value":"Direct Deposit Pending"},"order_status_direct_deposit_received":{"title":"Account, Orders Status, Direct Deposit Received","value":"Direct Deposit Received"},"order_status_refunded_order":{"title":"Account, Orders Status, Refunded Order","value":"Refunded Order"},"order_status_partial_refund":{"title":"Account, Orders Status, Partial Refund","value":"Partial Refund"},"order_status_order_picked_up":{"title":"Account, Orders Status, Order Picked UP","value":"Order Picked Up"},"no_order_found":{"title":"Account, No Order Found","value":"Order was not found or is not associated with the account you are currently logged into"},"return_to_dashboard":{"title":"Account, Return to Dashboard","value":"Return to Account Dashboard"}}},"account_subscriptions":{"label":"Account - Subscriptions","options":{"account_subscriptions_title":{"title":"Account, Subscriptions Title","value":"Your Active Subscriptions"},"account_canceled_subscriptions_title":{"title":"Account, Canceled Subscriptions Title","value":"Your Canceled Subscriptions"},"account_subscriptions_header_1":{"title":"Account, Subscriptions, Header 1","value":"Subscription Name"},"account_subscriptions_header_2":{"title":"Account, Subscriptions, Header 2","value":"Next Bill Date"},"account_subscriptions_header_3":{"title":"Account, Subscriptions, Header 3","value":"Last Charged"},"account_subscriptions_header_4":{"title":"Account, Subscriptions, Header 4","value":"Price\\/Cycle"},"account_subscriptions_view_subscription_button":{"title":"Account, Subscriptions, View Details Button","value":"VIEW DETAILS"},"account_subscriptions_none_found":{"title":"Account, Subscriptions, None Found","value":"There are no subscriptions associated with your account."},"update_subscription_subtitle":{"title":"Account, Subscription Details, Credit Card Note","value":"You are not required to update your credit card information."},"save_changes_button":{"title":"Account, Subscription Details, Save Changes Button","value":"SAVE CHANGES"},"cancel_subscription_button":{"title":"Account, Subscription Details, Cancel Subscription Button","value":"CANCEL SUBSCRIPTION"},"cancel_subscription_confirm_text":{"title":"Account, Subscription Details, Cancel Subscription Button","value":"Are you sure you would like to cancel this subscription?"},"subscription_details_next_billing":{"title":"Account, Subscription Details, Next Billing Date","value":"Next Billing Date"},"subscription_details_last_payment":{"title":"Account, Subscription Details, Last Payment Date","value":"Last Payment Date"},"subscription_details_current_billing":{"title":"Account, Subscription Details, Current Billing Title","value":"Current Billing Method"},"subscription_details_notice":{"title":"Account, Subscription Details, User Notice","value":"*NOTICE: any changes to the billing method or subscription plan will take effect in the next billing cycle. Pricing changes will be prorated beginning immediately and will be reflected on your next bill."},"subscription_details_past_payments":{"title":"Account, Subscription Details, Past Payments Title","value":"Past Payments"}}},"ec_errors":{"label":"Store Error Text","options":{"login_failed":{"title":"Account, User Login Failed","value":"The username or password you entered is incorrect."},"register_email_error":{"title":"Account Registration Email In Use Error","value":"The email you have entered already has an account."},"no_reset_email_found":{"title":"No Email Found Error","value":"The email address you entered was not found."},"personal_information_update_error":{"title":"Account, updating personal info error","value":"An error occurred while updating your personal information."},"password_wrong_current":{"title":"Wrong password entered Error","value":"The current password you entered did not match your account password."},"password_no_match":{"title":"Passwords do not match Error","value":"The new password and the retype new password values did not match."},"password_update_error":{"title":"password update error","value":"An error occurred while updating your password."},"billing_information_error":{"title":"Account, billing information update error","value":"An error occurred while updating your billing information."},"shipping_information_error":{"title":"Account, shipping information update error","value":"An error occurred while updating your shipping information."},"email_exists_error":{"title":"Cart, create account on checkout, email exists","value":"This email already has an account. Please login or use a different email address to continue."},"3dsecure_failed":{"title":"Cart, Optional, Error message when 3D Secure Fails, Gateway Dependent","value":"Your payment could not completed because the 3D Secure method failed. Please try your payment again. If your problem persists, please contact us to have the issue resolved."},"manualbill_failed":{"title":"Cart, Optional, Manual billing failed, Gateway Dependent","value":"Your payment could not completed, manual billing failed. Please try again."},"thirdparty_failed":{"title":"Cart, Optional, Third Party Failed, Gateway Dependent","value":"Your payment could not completed, third party failed. Please try again."},"payment_failed":{"title":"Cart, Optional, Credit Card Failed, Gateway Dependent","value":"Your payment could not completed because your credit card could not be verified. Please check your credit card information and try again. If your problem persists, please contact us to complete payment."},"already_subscribed":{"title":"Cart, Optional, Already Subscribed, Gateway Dependent","value":"You have already subscribed to this product. Please visit your account to manage your active subscriptions."},"subscription_not_found":{"title":"Account, Subscription Not Found Error","value":"An unknown error occurred in which the subscription you are trying to purchase was not found in our system. Please try again and contact us if you have continued difficulties."},"invalid_address":{"title":"Cart, Invalid Live Shipping Address","value":"Something is wrong with the address you have entered. Please check your city, state, zip, and country to make sure the values are correct."},"activation_error":{"title":"Account, Activation Error","value":"An error has occurred while attempting to activate your account. Please contact us to have the issue resolved."},"not_activated":{"title":"Account, Not Activated","value":"The account you are attempting to access has not been activated. Please activate your account through the activation email sent when you signed up for an account or contact us if you are having problems."},"subscription_update_failed":{"title":"Account, Subscription Update Error","value":"There was a problem while updating your subscription, please try again or contact us if you continue to have problems."},"subscription_cancel_failed":{"title":"Account, Subscription Cancel Error","value":"There was a problem while cancelling your subscription, please try again or contact us if you continue to have problems."},"user_insert_error":{"title":"Account, Subscription User Insert Error","value":"There was a problem creating your user account in our subscription system. Please contact us to complete your transaction."},"subscription_added_failed":{"title":"Account, Subscription Plan Added Error","value":"There was an internal problem while creating this product in our subscription service. Please contact us to complete your transaction."},"subscription_failed":{"title":"Account, Subscription Creation Error","value":"There was a problem while creating your subscription in our system. Please contact us to complete your transaction."},"nets_processing":{"title":"Account, Nets Error Level Authorize","value":"While completing your order at Nets Netaxept, an error occurred. You may complete your order at any time by clicking the button below."},"nets_processing_payment":{"title":"Account, Nets Error Level Capture","value":"Your payment has been authorized at Nets Netaxept, but there seems to have been an issue in our system. Please contact us to complete your order."},"subscription_payment_failed_title":{"title":"Cart, Subscription Payment Failed","value":"Payment Failed"},"subscription_payment_failed_text":{"title":"Cart, Subscription Payment Failed Text","value":"The payment has failed for one of your subscriptions. Please click the link below to update your credit card information."},"subscription_payment_failed_link":{"title":"Cart, Subscription Payment Failed Link","value":"Click to Update Billing"},"minquantity":{"title":"Store, Minimum Quantity Error","value":"This product requires a minimum purchase quantity."},"missing_gift_card_options":{"title":"Store, Missing Gift Card Options","value":"Missing Gift Card Options"},"missing_inquiry_options":{"title":"Store, Missing Inquiry Options","value":"Missing Inquiry Options"},"session_expired":{"title":"Store, Session Expired","value":"Your session has expired, please enter your checkout information again to complete your order."},"invalid_vat_number":{"title":"Cart, Invalid VAT Number Entered","value":"Your VAT registration number is invalid. Please leave this field empty or correct the error to continue."}}},"ec_success":{"label":"Store Success Text","options":{"personal_information_updated":{"title":"Account Personal Information Update Success","value":"Your personal information was updated successfully."},"password_updated":{"title":"Account Personal Information Update Success","value":"Your password was updated successfully."},"billing_information_updated":{"title":"Account Billing Update Success","value":"Your billing information was updated successfully."},"shipping_information_updated":{"title":"Account Shipping Update Success","value":"Your shipping information was updated successfully."},"reset_email_sent":{"title":"Account Email Sent Success","value":"Your new password has been sent to your email address."},"cart_account_created":{"title":"Cart, Create Account, Account Created Success Text","value":"Your account has been created, all orders will now be associated with your new account."},"cart_account_free_order":{"title":"Cart, Order Type, Gift Card, Free Order","value":"Free Order"},"store_added_to_cart":{"title":"Store, Added Item to Cart","value":"You have successfully added [prod_title] to your cart."},"activation_success":{"title":"Account, Activation Success","value":"You have successfully activated your account"},"validation_required":{"title":"Account, Validation Required","value":"You have successfully created an account, but your email needs to be validated. An email has been sent to your account with instructions on how to complete the registration process. Contact us if you have any questions."},"subscription_updated":{"title":"Account, Subscription Updated","value":"Your subscription has been updated successfully."},"subscription_canceled":{"title":"Account, Subscription Canceled","value":"Your subscription has been canceled."},"inquiry_sent":{"title":"Store, Inquiry Sent","value":"Your product inquiry has been sent successfully."},"add_to_cart_success":{"title":"Store, Added to Cart","value":"Successfully Added to your Shopping Cart"},"adding_to_cart":{"title":"Store, Adding to Cart","value":"Adding to Cart..."}}},"ec_shipping_email":{"label":"Shipping Emailer","options":{"shipping_email_title":{"title":"Shipping Email Title","value":"Shipping Confirmation - Order Number"},"shipping_dear":{"title":"Shipping Dear Customer","value":"Dear"},"shipping_subtitle1":{"title":"Order Shipped First Half","value":"Your recent order  with the number"},"shipping_subtitle2":{"title":"Order Shipped Second Half","value":"has been shipped! You should be receiving it within a short time period."},"shipping_description":{"title":"Order Shipped Description","value":"You may check the status of your order by visiting your carrier''s website and using the following tracking number."},"shipping_carrier":{"title":"Order Shipped Carrier","value":"Package Carrier:"},"shipping_tracking":{"title":"Order Shipped Tracking Number","value":"Package Tracking Number:"},"shipping_billing_label":{"title":"Billing Label","value":"Billing Address"},"shipping_shipping_label":{"title":"Shipping Label","value":"Shipping Address"},"shipping_product":{"title":"Product Label","value":"Product"},"shipping_quantity":{"title":"Quantity Label","value":"Qty"},"shipping_unit_price":{"title":"Unit Price Label","value":"Unit Price"},"shipping_total_price":{"title":"Total Price Label","value":"Ext Price"},"shipping_final_note1":{"title":"Shipping Emailer Final Note Line 1","value":"Please double check your order when you receive it and let us know immediately if there are any concerns or issues. We always value your business and hope you enjoy your product."},"shipping_final_note2":{"title":"Shipping Emailer Final Note Line 2","value":"Thank you very much!"}}},"ec_login_widget":{"label":"Login Widget","options":{"hello_text":{"title":"Hello","value":"Hello"},"dashboard_text":{"title":"Dashboard","value":"Dashboard"},"order_history_text":{"title":"Order History","value":"Order History"},"billing_info_text":{"title":"Billing Information","value":"Billing Information"},"shipping_info_text":{"title":"Shipping Information","value":"Shipping Information"},"change_password_text":{"title":"Change Password","value":"Change Password"},"sign_out_text":{"title":"Sign Out","value":"Sign Out"}}},"ec_minicart_widget":{"label":"Minicart Widget","options":{"subtotal_text":{"title":"Minicart Widget, Subtotal Text","value":"SUBTOTAL"},"checkout_button_text":{"title":"Minicart Widget, Checkout Button Text","value":"CHECKOUT"}}},"ec_pricepoint_widget":{"label":"Price Point Widget","options":{"less_than":{"title":"Less Than Text","value":"Less Than"},"greater_than":{"title":"Greater Than Text","value":"Greater Than"}}},"ec_newsletter_popup":{"label":"Newsletter Popup","options":{"signup_form_title":{"title":"Signup Form Title","value":"Newsletter Signup"},"signup_form_subtitle":{"title":"Signup Form Subtitle","value":"Sign up now and never miss a thing!"},"signup_form_email_placeholder":{"title":"Signup Form Email Placeholder","value":"email address"},"signup_form_name_placeholder":{"title":"Signup Form Name Placeholder","value":"your name"},"signup_form_button_text":{"title":"Signup Form Button","value":"SUBMIT"},"signup_form_success_title":{"title":"Signup Success Title","value":"You''re Signed Up!"},"signup_form_success_subtitle":{"title":"Signup Success Subtitle","value":"Now that you are signed up, we will send you exclusive offers periodically."}}},"ec_abandoned_cart_email":{"label":"Abandoned Cart Email","options":{"email_title":{"title":"Email Title","value":"You Left Items in Your Cart"},"something_in_cart":{"title":"Main Title, Something in Cart","value":"THERE''S SOMETHING IN YOUR CART."},"complete_question":{"title":"Informational Text","value":"Would you like to complete your purchase?"},"complete_checkout":{"title":"Complete Checkout Button","value":"Complete Checkout"}}},"language_code":{"label":"Language Code","options":{"code":{"title":"Do Not Change","value":"EN"}}}}}}', 'yes');
INSERT INTO `ops_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(593, 'ec_option_use_seperate_language_forms', '1', 'yes'),
(594, 'ec_option_base_theme', '0', 'yes'),
(595, 'ec_option_base_layout', '0', 'yes'),
(596, 'ec_option_latest_theme', 'base-responsive-v3', 'yes'),
(597, 'ec_option_latest_layout', 'base-responsive-v3', 'yes'),
(598, 'ec_option_caching_on', '1', 'yes'),
(599, 'ec_option_cache_update_period', '2', 'yes'),
(600, 'ec_option_cached_date', '0', 'yes'),
(601, 'ec_option_custom_css', '', 'yes'),
(602, 'ec_option_css_replacements', 'main_color=#242424,second_color=#6b6b6b,third_color=#adadad,title_color=#0f0f0f,text_color=#141414,link_color=#242424,link_hover_color=#121212,sale_color=#900,backdrop_color=#333,content_bg=#FFF,error_text=#900,error_color=#F1D9D9,error_color2=#FF0606,success_text=#333,success_color=#E6FFE6,success_color2=#6FFF47', 'yes'),
(603, 'ec_option_font_replacements', 'title_font=Arial, Helvetica, sans-serif:::subtitle_font=Arial, Helvetica, sans-serif:::content_font=Arial, Helvetica, sans-serif', 'yes'),
(604, 'ec_option_responsive_sizes', 'size_level1_high=479:::size_level2_low=480:::size_level2_high=767:::size_level3_low=768:::size_level3_high=960:::size_level4_low=961:::size_level4_high=1300:::size_level5_low=1301', 'yes'),
(605, 'ec_option_details_main_color', '#222222', 'yes'),
(606, 'ec_option_details_second_color', '#666666', 'yes'),
(607, 'ec_option_use_dark_bg', '0', 'yes'),
(608, 'ec_option_default_product_type', '1', 'yes'),
(609, 'ec_option_default_product_image_hover_type', '3', 'yes'),
(610, 'ec_option_default_product_image_effect_type', 'none', 'yes'),
(611, 'ec_option_default_quick_view', '0', 'yes'),
(612, 'ec_option_default_desktop_columns', '3', 'yes'),
(613, 'ec_option_default_desktop_image_height', '310px', 'yes'),
(614, 'ec_option_default_laptop_columns', '3', 'yes'),
(615, 'ec_option_default_laptop_image_height', '310px', 'yes'),
(616, 'ec_option_default_tablet_wide_columns', '2', 'yes'),
(617, 'ec_option_default_tablet_wide_image_height', '310px', 'yes'),
(618, 'ec_option_default_tablet_columns', '2', 'yes'),
(619, 'ec_option_default_tablet_image_height', '380px', 'yes'),
(620, 'ec_option_default_smartphone_columns', '1', 'yes'),
(621, 'ec_option_default_smartphone_image_height', '270px', 'yes'),
(622, 'ec_option_details_columns_desktop', '2', 'yes'),
(623, 'ec_option_details_columns_laptop', '2', 'yes'),
(624, 'ec_option_details_columns_tablet_wide', '1', 'yes'),
(625, 'ec_option_details_columns_tablet', '1', 'yes'),
(626, 'ec_option_details_columns_smartphone', '1', 'yes'),
(627, 'ec_option_cart_columns_desktop', '2', 'yes'),
(628, 'ec_option_cart_columns_laptop', '2', 'yes'),
(629, 'ec_option_cart_columns_tablet_wide', '1', 'yes'),
(630, 'ec_option_cart_columns_tablet', '1', 'yes'),
(631, 'ec_option_cart_columns_smartphone', '1', 'yes'),
(632, 'ec_option_email_logo', '', 'yes'),
(633, 'ec_option_use_facebook_icon', '1', 'yes'),
(634, 'ec_option_use_twitter_icon', '1', 'yes'),
(635, 'ec_option_use_delicious_icon', '1', 'yes'),
(636, 'ec_option_use_myspace_icon', '1', 'yes'),
(637, 'ec_option_use_linkedin_icon', '1', 'yes'),
(638, 'ec_option_use_email_icon', '1', 'yes'),
(639, 'ec_option_use_digg_icon', '1', 'yes'),
(640, 'ec_option_use_googleplus_icon', '1', 'yes'),
(641, 'ec_option_use_pinterest_icon', '1', 'yes'),
(642, 'ec_option_checklist_state', '', 'yes'),
(643, 'ec_option_checklist_currency', '', 'yes'),
(644, 'ec_option_checklist_default_payment', '', 'yes'),
(645, 'ec_option_checklist_guest', '', 'yes'),
(646, 'ec_option_checklist_shipping_enabled', '', 'yes'),
(647, 'ec_option_checklist_checkout_notes', '', 'yes'),
(648, 'ec_option_checklist_billing_registration', '', 'yes'),
(649, 'ec_option_checklist_google_analytics', '', 'yes'),
(650, 'ec_option_checklist_manual_billing', '', 'yes'),
(651, 'ec_option_checklist_third_party_complete', '', 'yes'),
(652, 'ec_option_checklist_third_party', '', 'yes'),
(653, 'ec_option_checklist_has_paypal', '', 'yes'),
(654, 'ec_option_checklist_has_skrill', '', 'yes'),
(655, 'ec_option_checklist_has_paymentexpress_thirdparty', '', 'yes'),
(656, 'ec_option_checklist_has_realex_thirdparty', '', 'yes'),
(657, 'ec_option_checklist_credit_cart_complete', '', 'yes'),
(658, 'ec_option_checklist_credit_card', '', 'yes'),
(659, 'ec_option_checklist_credit_card_location', '', 'yes'),
(660, 'ec_option_checklist_tax_complete', '', 'yes'),
(661, 'ec_option_checklist_tax_choice', '', 'yes'),
(662, 'ec_option_checklist_shipping_complete', '', 'yes'),
(663, 'ec_option_checklist_shipping_choice', '', 'yes'),
(664, 'ec_checklist_shipping_use_ups', '', 'yes'),
(665, 'ec_checklist_shipping_use_usps', '', 'yes'),
(666, 'ec_checklist_shipping_use_fedex', '', 'yes'),
(667, 'ec_checklist_shipping_use_auspost', '', 'yes'),
(668, 'ec_checklist_shipping_use_dhl', '', 'yes'),
(669, 'ec_option_checklist_language_complete', '', 'yes'),
(670, 'ec_option_checklist_theme_complete', '', 'yes'),
(671, 'ec_option_checklist_colorization_complete', '', 'yes'),
(672, 'ec_option_checklist_logo_added_complete', '', 'yes'),
(673, 'ec_option_checklist_admin_embedded_complete', '', 'yes'),
(674, 'ec_option_checklist_admin_consoles_complete', '', 'yes'),
(675, 'ec_option_checklist_page', '', 'yes'),
(676, 'ec_option_quickbooks_user', '', 'yes'),
(677, 'ec_option_quickbooks_password', '', 'yes'),
(678, 'ec_option_wpoptions_version', '4_0_37', 'yes'),
(679, 'ec_option_db_insert_v4', '1', 'yes'),
(680, 'ec_option_db_new_version', '57', 'yes'),
(681, 'ec_option_data_folders_installed', '4_0_37', 'yes'),
(682, 'widget_ec_categorywidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(683, 'widget_ec_cartwidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(684, 'widget_ec_colorwidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(685, 'widget_ec_currencywidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(686, 'widget_ec_donationwidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(687, 'widget_ec_groupwidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(688, 'widget_ec_languagewidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(689, 'widget_ec_loginwidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(690, 'widget_ec_manufacturerwidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(691, 'widget_ec_menuwidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(692, 'widget_ec_newsletterwidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(693, 'widget_ec_pricepointwidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(694, 'widget_ec_productwidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(695, 'widget_ec_searchwidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(696, 'widget_ec_specialswidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(697, 'ec_option_v3_fix', '1', 'yes'),
(698, 'ec_option_published_check', '4_0_37', 'yes'),
(700, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:"auto_add";a:0:{}}', 'yes'),
(705, 'WPLANG', '', 'yes'),
(706, 'new_admin_email', 'info@ozproductionservices.com', 'yes'),
(713, '_site_transient_timeout_theme_roots', '1525157104', 'no'),
(714, '_site_transient_theme_roots', 'a:4:{s:12:"ozproduction";s:7:"/themes";s:13:"twentyfifteen";s:7:"/themes";s:15:"twentyseventeen";s:7:"/themes";s:13:"twentysixteen";s:7:"/themes";}', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `ops_postmeta`
--

CREATE TABLE IF NOT EXISTS `ops_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=108 ;

--
-- Dumping data for table `ops_postmeta`
--

INSERT INTO `ops_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 8, '_wp_attached_file', '2018/04/chair.png'),
(3, 8, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1000;s:6:"height";i:1000;s:4:"file";s:17:"2018/04/chair.png";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"chair-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:17:"chair-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:12:"medium_large";a:4:{s:4:"file";s:17:"chair-768x768.png";s:5:"width";i:768;s:6:"height";i:768;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(4, 2, '_wp_trash_meta_status', 'publish'),
(5, 2, '_wp_trash_meta_time', '1525098299'),
(6, 2, '_wp_desired_post_slug', 'sample-page'),
(7, 11, '_edit_last', '1'),
(8, 11, 'wpeasycart_restrict_redirect_url', ''),
(9, 11, '_edit_lock', '1525099477:1'),
(10, 13, '_edit_last', '1'),
(11, 13, '_edit_lock', '1525182866:1'),
(12, 13, 'wpeasycart_restrict_redirect_url', ''),
(13, 15, '_edit_last', '1'),
(14, 15, '_edit_lock', '1525098204:1'),
(15, 15, 'wpeasycart_restrict_redirect_url', ''),
(16, 17, '_edit_last', '1'),
(17, 17, 'wpeasycart_restrict_redirect_url', ''),
(18, 17, '_edit_lock', '1525098522:1'),
(19, 19, '_menu_item_type', 'custom'),
(20, 19, '_menu_item_menu_item_parent', '0'),
(21, 19, '_menu_item_object_id', '19'),
(22, 19, '_menu_item_object', 'custom'),
(23, 19, '_menu_item_target', ''),
(24, 19, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(25, 19, '_menu_item_xfn', ''),
(26, 19, '_menu_item_url', 'http://localhost/oz_production/public_html/'),
(27, 19, '_menu_item_orphaned', '1525098707'),
(28, 20, '_menu_item_type', 'post_type'),
(29, 20, '_menu_item_menu_item_parent', '0'),
(30, 20, '_menu_item_object_id', '13'),
(31, 20, '_menu_item_object', 'page'),
(32, 20, '_menu_item_target', ''),
(33, 20, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(34, 20, '_menu_item_xfn', ''),
(35, 20, '_menu_item_url', ''),
(37, 21, '_menu_item_type', 'post_type'),
(38, 21, '_menu_item_menu_item_parent', '0'),
(39, 21, '_menu_item_object_id', '7'),
(40, 21, '_menu_item_object', 'page'),
(41, 21, '_menu_item_target', ''),
(42, 21, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(43, 21, '_menu_item_xfn', ''),
(44, 21, '_menu_item_url', ''),
(46, 22, '_menu_item_type', 'post_type'),
(47, 22, '_menu_item_menu_item_parent', '0'),
(48, 22, '_menu_item_object_id', '6'),
(49, 22, '_menu_item_object', 'page'),
(50, 22, '_menu_item_target', ''),
(51, 22, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(52, 22, '_menu_item_xfn', ''),
(53, 22, '_menu_item_url', ''),
(55, 23, '_menu_item_type', 'post_type'),
(56, 23, '_menu_item_menu_item_parent', '0'),
(57, 23, '_menu_item_object_id', '17'),
(58, 23, '_menu_item_object', 'page'),
(59, 23, '_menu_item_target', ''),
(60, 23, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(61, 23, '_menu_item_xfn', ''),
(62, 23, '_menu_item_url', ''),
(64, 24, '_menu_item_type', 'post_type'),
(65, 24, '_menu_item_menu_item_parent', '0'),
(66, 24, '_menu_item_object_id', '11'),
(67, 24, '_menu_item_object', 'page'),
(68, 24, '_menu_item_target', ''),
(69, 24, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(70, 24, '_menu_item_xfn', ''),
(71, 24, '_menu_item_url', ''),
(73, 25, '_menu_item_type', 'post_type'),
(74, 25, '_menu_item_menu_item_parent', '0'),
(75, 25, '_menu_item_object_id', '15'),
(76, 25, '_menu_item_object', 'page'),
(77, 25, '_menu_item_target', ''),
(78, 25, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(79, 25, '_menu_item_xfn', ''),
(80, 25, '_menu_item_url', ''),
(81, 25, '_menu_item_orphaned', '1525098709'),
(91, 11, '_wp_page_template', 'home.php'),
(92, 27, '_wp_attached_file', '2018/04/table.png'),
(93, 27, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:500;s:6:"height";i:282;s:4:"file";s:17:"2018/04/table.png";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"table-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:17:"table-300x169.png";s:5:"width";i:300;s:6:"height";i:169;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(94, 13, '_wp_page_template', 'default'),
(95, 13, 'about_project_completed', '39'),
(96, 13, 'about_team_member', '14'),
(97, 13, 'about_on_going_project', '29'),
(98, 13, 'about_weekly_event', '5'),
(99, 30, '_menu_item_type', 'post_type'),
(100, 30, '_menu_item_menu_item_parent', '0'),
(101, 30, '_menu_item_object_id', '15'),
(102, 30, '_menu_item_object', 'page'),
(103, 30, '_menu_item_target', ''),
(104, 30, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(105, 30, '_menu_item_xfn', ''),
(106, 30, '_menu_item_url', '');

-- --------------------------------------------------------

--
-- Table structure for table `ops_posts`
--

CREATE TABLE IF NOT EXISTS `ops_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=31 ;

--
-- Dumping data for table `ops_posts`
--

INSERT INTO `ops_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2018-04-30 06:23:21', '2018-04-30 06:23:21', 'Welcome to WordPress. This is your first post. Edit or delete it, then start writing!', 'Hello world!', '', 'publish', 'open', 'open', '', 'hello-world', '', '', '2018-04-30 06:23:21', '2018-04-30 06:23:21', '', 0, 'http://localhost/oz_production/public_html/?p=1', 0, 'post', '', 1),
(2, 1, '2018-04-30 06:23:21', '2018-04-30 06:23:21', 'This is an example page. It''s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\n\n<blockquote>Hi there! I''m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin'' caught in the rain.)</blockquote>\n\n...or something like this:\n\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\n\nAs a new WordPress user, you should go to <a href="http://localhost/oz_production/public_html/wp-admin/">your dashboard</a> to delete this page and create new pages for your content. Have fun!', 'Sample Page', '', 'trash', 'closed', 'open', '', 'sample-page__trashed', '', '', '2018-04-30 14:24:59', '2018-04-30 14:24:59', '', 0, 'http://localhost/oz_production/public_html/?page_id=2', 0, 'page', '', 0),
(3, 1, '2018-04-30 06:23:51', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2018-04-30 06:23:51', '0000-00-00 00:00:00', '', 0, 'http://localhost/oz_production/public_html/?p=3', 0, 'post', '', 0),
(4, 1, '2018-04-30 13:37:27', '2018-04-30 13:37:27', '[ec_store manufacturerid="1"]', 'Oz Production Event Services, LLC', '', 'publish', 'closed', 'closed', '', 'oz-production-event-services-llc', '', '', '2018-04-30 13:37:27', '2018-04-30 13:37:27', '', 0, 'http://localhost/oz_production/public_html/2018/04/30/oz-production-event-services-llc/', 0, 'ec_store', '', 0),
(5, 1, '2018-04-30 13:37:27', '2018-04-30 13:37:27', '[ec_store]', 'Store', '', 'publish', 'closed', 'closed', '', 'store', '', '', '2018-04-30 13:37:27', '2018-04-30 13:37:27', '', 0, 'http://localhost/oz_production/public_html/store/', 0, 'page', '', 0),
(6, 1, '2018-04-30 13:37:28', '2018-04-30 13:37:28', '[ec_cart]', 'Cart', '', 'publish', 'closed', 'closed', '', 'cart', '', '', '2018-04-30 13:37:28', '2018-04-30 13:37:28', '', 0, 'http://localhost/oz_production/public_html/cart/', 0, 'page', '', 0),
(7, 1, '2018-04-30 13:37:28', '2018-04-30 13:37:28', '[ec_account]', 'Account', '', 'publish', 'closed', 'closed', '', 'account', '', '', '2018-04-30 13:37:28', '2018-04-30 13:37:28', '', 0, 'http://localhost/oz_production/public_html/account/', 0, 'page', '', 0),
(8, 1, '2018-04-30 14:22:55', '2018-04-30 14:22:55', '', 'chair', '', 'inherit', 'open', 'closed', '', 'chair', '', '', '2018-04-30 14:22:55', '2018-04-30 14:22:55', '', 0, 'http://localhost/oz_production/public_html/wp-content/uploads/2018/04/chair.png', 0, 'attachment', 'image/png', 0),
(9, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[ec_store modelnumber="Chair"]', 'Chair', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis mattis fringilla lectus et blandit. Integer sit amet nulla sed mi porttitor tincidunt a non ligula. Quisque blandit dui odio, sed consectetur erat malesuada ac. Curabitur placerat orci magna, id malesuada massa iaculis id. Sed a felis id enim euismod tristique varius in libero. Sed tempus tristique felis ac vehicula. Vestibulum viverra, velit non aliquet rhoncus, odio dolor placerat dui, vel porttitor sem arcu ac massa. Mauris elementum, lacus sollicitudin dictum faucibus, magna enim aliquam libero, at pharetra justo quam eget arcu. Donec ut porttitor metus, luctus egestas dolor. Cras lacinia condimentum nunc fermentum vulputate. Donec feugiat auctor purus. Suspendisse lobortis diam eget pellentesque cursus. Proin non dictum lorem, porttitor tempor nisi.</p><p>Integer auctor lobortis nibh, a imperdiet dolor pulvinar a. Donec tempor aliquet mauris, vitae feugiat sapien dapibus sit amet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget libero sed arcu efficitur iaculis in ut quam. Fusce nisl tellus, ultrices vitae ex id, interdum porttitor diam. Aliquam et velit tellus. Vivamus varius nec lacus id interdum. Mauris sollicitudin orci eget lectus porta tincidunt. Phasellus suscipit, justo et ultricies tristique, eros libero maximus lacus, in pulvinar lectus arcu eu eros. Mauris nec convallis ligula, dictum dignissim lacus.</p>', 'publish', 'open', 'open', '', 'chair', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 0, 'http://localhost/oz_production/public_html/store/chair/', 0, 'ec_store', '', 0),
(10, 1, '2018-04-30 14:24:59', '2018-04-30 14:24:59', 'This is an example page. It''s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\n\n<blockquote>Hi there! I''m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin'' caught in the rain.)</blockquote>\n\n...or something like this:\n\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\n\nAs a new WordPress user, you should go to <a href="http://localhost/oz_production/public_html/wp-admin/">your dashboard</a> to delete this page and create new pages for your content. Have fun!', 'Sample Page', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2018-04-30 14:24:59', '2018-04-30 14:24:59', '', 2, 'http://localhost/oz_production/public_html/2018/04/30/2-revision-v1/', 0, 'revision', '', 0),
(11, 1, '2018-04-30 14:25:07', '2018-04-30 14:25:07', '', 'Home', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2018-04-30 14:38:56', '2018-04-30 14:38:56', '', 0, 'http://localhost/oz_production/public_html/?page_id=11', 0, 'page', '', 0),
(12, 1, '2018-04-30 14:25:07', '2018-04-30 14:25:07', '', 'Home', '', 'inherit', 'closed', 'closed', '', '11-revision-v1', '', '', '2018-04-30 14:25:07', '2018-04-30 14:25:07', '', 11, 'http://localhost/oz_production/public_html/2018/04/30/11-revision-v1/', 0, 'revision', '', 0),
(13, 1, '2018-04-30 14:25:26', '2018-04-30 14:25:26', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis mattis fringilla lectus et blandit. Integer sit amet nulla sed mi porttitor tincidunt a non ligula. Quisque blandit dui odio, sed consectetur erat malesuada ac. Curabitur placerat orci magna, id malesuada massa iaculis id. Sed a felis id enim euismod tristique varius in libero. Sed tempus tristique felis ac vehicula. Vestibulum viverra, velit non aliquet rhoncus, odio dolor placerat dui, vel porttitor sem arcu ac massa. Mauris elementum, lacus sollicitudin dictum faucibus, magna enim aliquam libero, at pharetra justo quam eget arcu. Donec ut porttitor metus, luctus egestas dolor.', 'About Us', '', 'publish', 'closed', 'closed', '', 'about-us', '', '', '2018-05-01 06:49:59', '2018-05-01 06:49:59', '', 0, 'http://localhost/oz_production/public_html/?page_id=13', 0, 'page', '', 0),
(14, 1, '2018-04-30 14:25:26', '2018-04-30 14:25:26', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis mattis fringilla lectus et blandit. Integer sit amet nulla sed mi porttitor tincidunt a non ligula. Quisque blandit dui odio, sed consectetur erat malesuada ac. Curabitur placerat orci magna, id malesuada massa iaculis id. Sed a felis id enim euismod tristique varius in libero. Sed tempus tristique felis ac vehicula. Vestibulum viverra, velit non aliquet rhoncus, odio dolor placerat dui, vel porttitor sem arcu ac massa. Mauris elementum, lacus sollicitudin dictum faucibus, magna enim aliquam libero, at pharetra justo quam eget arcu. Donec ut porttitor metus, luctus egestas dolor. Cras lacinia condimentum nunc fermentum vulputate. Donec feugiat auctor purus. Suspendisse lobortis diam eget pellentesque cursus. Proin non dictum lorem, porttitor tempor nisi.\r\n\r\nInteger auctor lobortis nibh, a imperdiet dolor pulvinar a. Donec tempor aliquet mauris, vitae feugiat sapien dapibus sit amet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget libero sed arcu efficitur iaculis in ut quam. Fusce nisl tellus, ultrices vitae ex id, interdum porttitor diam. Aliquam et velit tellus. Vivamus varius nec lacus id interdum. Mauris sollicitudin orci eget lectus porta tincidunt. Phasellus suscipit, justo et ultricies tristique, eros libero maximus lacus, in pulvinar lectus arcu eu eros. Mauris nec convallis ligula, dictum dignissim lacus.', 'About Us', '', 'inherit', 'closed', 'closed', '', '13-revision-v1', '', '', '2018-04-30 14:25:26', '2018-04-30 14:25:26', '', 13, 'http://localhost/oz_production/public_html/2018/04/30/13-revision-v1/', 0, 'revision', '', 0),
(15, 1, '2018-04-30 14:25:45', '2018-04-30 14:25:45', '', 'Services', '', 'publish', 'closed', 'closed', '', 'services', '', '', '2018-04-30 14:25:45', '2018-04-30 14:25:45', '', 0, 'http://localhost/oz_production/public_html/?page_id=15', 0, 'page', '', 0),
(16, 1, '2018-04-30 14:25:45', '2018-04-30 14:25:45', '', 'Services', '', 'inherit', 'closed', 'closed', '', '15-revision-v1', '', '', '2018-04-30 14:25:45', '2018-04-30 14:25:45', '', 15, 'http://localhost/oz_production/public_html/2018/04/30/15-revision-v1/', 0, 'revision', '', 0),
(17, 1, '2018-04-30 14:25:55', '2018-04-30 14:25:55', '', 'Contact Us', '', 'publish', 'closed', 'closed', '', 'contact-us', '', '', '2018-04-30 14:31:01', '2018-04-30 14:31:01', '', 0, 'http://localhost/oz_production/public_html/?page_id=17', 0, 'page', '', 0),
(18, 1, '2018-04-30 14:25:55', '2018-04-30 14:25:55', '', 'Contact Us', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2018-04-30 14:25:55', '2018-04-30 14:25:55', '', 17, 'http://localhost/oz_production/public_html/2018/04/30/17-revision-v1/', 0, 'revision', '', 0),
(19, 1, '2018-04-30 14:31:47', '0000-00-00 00:00:00', '', 'Home', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-04-30 14:31:47', '0000-00-00 00:00:00', '', 0, 'http://localhost/oz_production/public_html/?p=19', 1, 'nav_menu_item', '', 0),
(20, 1, '2018-04-30 14:32:48', '2018-04-30 14:32:48', ' ', '', '', 'publish', 'closed', 'closed', '', '20', '', '', '2018-05-01 06:59:17', '2018-05-01 06:59:17', '', 0, 'http://localhost/oz_production/public_html/?p=20', 2, 'nav_menu_item', '', 0),
(21, 1, '2018-04-30 14:32:48', '2018-04-30 14:32:48', ' ', '', '', 'publish', 'closed', 'closed', '', '21', '', '', '2018-05-01 06:59:17', '2018-05-01 06:59:17', '', 0, 'http://localhost/oz_production/public_html/?p=21', 4, 'nav_menu_item', '', 0),
(22, 1, '2018-04-30 14:32:48', '2018-04-30 14:32:48', ' ', '', '', 'publish', 'closed', 'closed', '', '22', '', '', '2018-05-01 06:59:17', '2018-05-01 06:59:17', '', 0, 'http://localhost/oz_production/public_html/?p=22', 5, 'nav_menu_item', '', 0),
(23, 1, '2018-04-30 14:32:48', '2018-04-30 14:32:48', ' ', '', '', 'publish', 'closed', 'closed', '', '23', '', '', '2018-05-01 06:59:17', '2018-05-01 06:59:17', '', 0, 'http://localhost/oz_production/public_html/?p=23', 6, 'nav_menu_item', '', 0),
(24, 1, '2018-04-30 14:32:47', '2018-04-30 14:32:47', ' ', '', '', 'publish', 'closed', 'closed', '', '24', '', '', '2018-05-01 06:59:17', '2018-05-01 06:59:17', '', 0, 'http://localhost/oz_production/public_html/?p=24', 1, 'nav_menu_item', '', 0),
(25, 1, '2018-04-30 14:31:49', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-04-30 14:31:49', '0000-00-00 00:00:00', '', 0, 'http://localhost/oz_production/public_html/?p=25', 1, 'nav_menu_item', '', 0),
(27, 1, '2018-04-30 15:09:51', '2018-04-30 15:09:51', '', 'table', '', 'inherit', 'open', 'closed', '', 'table', '', '', '2018-04-30 15:09:51', '2018-04-30 15:09:51', '', 0, 'http://localhost/oz_production/public_html/wp-content/uploads/2018/04/table.png', 0, 'attachment', 'image/png', 0),
(28, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[ec_store modelnumber="table"]', 'Table', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis mattis fringilla lectus et blandit. Integer sit amet nulla sed mi porttitor tincidunt a non ligula. Quisque blandit dui odio, sed consectetur erat malesuada ac. Curabitur placerat orci magna, id malesuada massa iaculis id. Sed a felis id enim euismod tristique varius in libero. Sed tempus tristique felis ac vehicula. Vestibulum viverra, velit non aliquet rhoncus, odio dolor placerat dui, vel porttitor sem arcu ac massa. Mauris elementum, lacus sollicitudin dictum faucibus, magna enim aliquam libero, at pharetra justo quam eget arcu. Donec ut porttitor metus, luctus egestas dolor. Cras lacinia condimentum nunc fermentum vulputate. Donec feugiat auctor purus. Suspendisse lobortis diam eget pellentesque cursus. Proin non dictum lorem, porttitor tempor nisi.</p><p>Integer auctor lobortis nibh, a imperdiet dolor pulvinar a. Donec tempor aliquet mauris, vitae feugiat sapien dapibus sit amet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget libero sed arcu efficitur iaculis in ut quam. Fusce nisl tellus, ultrices vitae ex id, interdum porttitor diam. Aliquam et velit tellus. Vivamus varius nec lacus id interdum. Mauris sollicitudin orci eget lectus porta tincidunt. Phasellus suscipit, justo et ultricies tristique, eros libero maximus lacus, in pulvinar lectus arcu eu eros. Mauris nec convallis ligula, dictum dignissim lacus.</p>', 'publish', 'open', 'open', '', 'table', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 0, 'http://localhost/oz_production/public_html/store/table/', 0, 'ec_store', '', 0),
(29, 1, '2018-05-01 06:29:11', '2018-05-01 06:29:11', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis mattis fringilla lectus et blandit. Integer sit amet nulla sed mi porttitor tincidunt a non ligula. Quisque blandit dui odio, sed consectetur erat malesuada ac. Curabitur placerat orci magna, id malesuada massa iaculis id. Sed a felis id enim euismod tristique varius in libero. Sed tempus tristique felis ac vehicula. Vestibulum viverra, velit non aliquet rhoncus, odio dolor placerat dui, vel porttitor sem arcu ac massa. Mauris elementum, lacus sollicitudin dictum faucibus, magna enim aliquam libero, at pharetra justo quam eget arcu. Donec ut porttitor metus, luctus egestas dolor.', 'About Us', '', 'inherit', 'closed', 'closed', '', '13-revision-v1', '', '', '2018-05-01 06:29:11', '2018-05-01 06:29:11', '', 13, 'http://localhost/oz_production/public_html/13-revision-v1/', 0, 'revision', '', 0),
(30, 1, '2018-05-01 06:59:17', '2018-05-01 06:59:17', ' ', '', '', 'publish', 'closed', 'closed', '', '30', '', '', '2018-05-01 06:59:17', '2018-05-01 06:59:17', '', 0, 'http://localhost/oz_production/public_html/?p=30', 3, 'nav_menu_item', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ops_termmeta`
--

CREATE TABLE IF NOT EXISTS `ops_termmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ops_terms`
--

CREATE TABLE IF NOT EXISTS `ops_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `ops_terms`
--

INSERT INTO `ops_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'Main_Menu', 'main_menu', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ops_term_relationships`
--

CREATE TABLE IF NOT EXISTS `ops_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `ops_term_relationships`
--

INSERT INTO `ops_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(20, 2, 0),
(21, 2, 0),
(22, 2, 0),
(23, 2, 0),
(24, 2, 0),
(30, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ops_term_taxonomy`
--

CREATE TABLE IF NOT EXISTS `ops_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `ops_term_taxonomy`
--

INSERT INTO `ops_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'nav_menu', '', 0, 6);

-- --------------------------------------------------------

--
-- Table structure for table `ops_usermeta`
--

CREATE TABLE IF NOT EXISTS `ops_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=24 ;

--
-- Dumping data for table `ops_usermeta`
--

INSERT INTO `ops_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'false'),
(11, 1, 'locale', ''),
(12, 1, 'ops_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(13, 1, 'ops_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', ''),
(15, 1, 'show_welcome_panel', '1'),
(16, 1, 'session_tokens', 'a:1:{s:64:"1472437afdd6763a30fc91673b6435bcc31f749e7bd8c064853499fa4e1b09d3";a:4:{s:10:"expiration";i:1525242229;s:2:"ip";s:3:"::1";s:2:"ua";s:115:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36";s:5:"login";i:1525069429;}}'),
(17, 1, 'ops_dashboard_quick_press_last_post_id', '3'),
(18, 1, 'community-events-location', 'a:1:{s:2:"ip";s:2:"::";}'),
(19, 1, 'ops_user-settings', 'libraryContent=browse'),
(20, 1, 'ops_user-settings-time', '1525098202'),
(21, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:"link-target";i:1;s:11:"css-classes";i:2;s:3:"xfn";i:3;s:11:"description";i:4;s:15:"title-attribute";}'),
(22, 1, 'metaboxhidden_nav-menus', 'a:2:{i:0;s:22:"add-post-type-ec_store";i:1;s:12:"add-post_tag";}'),
(23, 1, 'nav_menu_recently_edited', '2');

-- --------------------------------------------------------

--
-- Table structure for table `ops_users`
--

CREATE TABLE IF NOT EXISTS `ops_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ops_users`
--

INSERT INTO `ops_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$P$BdXlqYsrFsajH/3QZZ24vJg29ZrhUn0', 'admin', 'info@ozproductionservices.com', '', '2018-04-30 06:23:21', '', 0, 'admin');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
