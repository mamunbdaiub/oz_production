<?php
/*
  * Template Name: Home
  */
get_header(); ?>
    <!-- Header BEGIN -->
<?php get_template_part('templates/header_tpl', 'none'); ?>
    <!-- Header END -->

    <!-- Promo block BEGIN -->
<?php get_template_part('templates/home_tpl', 'none'); ?>
    <!-- Promo block END -->

    <!-- About block BEGIN -->
<?php get_template_part('templates/about_tpl', 'none'); ?>
    <!-- About block END -->

    <!-- Services block BEGIN -->
<?php get_template_part('templates/service_tpl', 'none'); ?>
    <!-- Services block END -->

    <!-- Testimonials block BEGIN -->
<?php get_template_part('templates/testimonials_tpl', 'none'); ?>
    <!-- Testimonials block END -->

    <!-- Partners block BEGIN -->
<?php //get_template_part('templates/partners_tpl', 'none'); ?>
    <!-- Partners block END -->

    <!-- BEGIN PRE-FOOTER -->
<?php get_template_part('templates/contact_tpl', 'none'); ?>
    <!-- END PRE-FOOTER -->
<?php get_footer(); ?>