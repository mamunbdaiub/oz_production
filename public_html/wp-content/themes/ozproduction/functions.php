<?php
require_once(TEMPLATEPATH . "/inc/sc_banner.php");
require_once(TEMPLATEPATH . "/inc/sc_testimonial.php");
function pr($result)
{
    echo "<pre>";
    var_dump($result);
    echo "</pre>";
}

function has_children()
{
    global $post;
    $pages = get_pages('child_of=' . $post->ID);
    return count($pages);
}

add_theme_support('post-thumbnails');
function shorten_string($string, $wordsreturned)
{
    $retval = $string; //    Just in case of a problem
    $array = explode(" ", $string);

    array_splice($array, $wordsreturned);
    $retval = implode(" ", $array);

    return $retval;
}

// Add RSS links to <head> section
automatic_feed_links();


// Clean up the <head>
function removeHeadLinks()
{
    remove_action('wp_head', 'rsd_link');
    remove_action('wp_head', 'wlwmanifest_link');
}

add_action('init', 'removeHeadLinks');
remove_action('wp_head', 'wp_generator');
// add sidebar widget


function blankpress_widgets_init()
{

    register_sidebar(array(
        'name' => __('Primary Widget Area', 'Blankpress'),
        'id' => 'primary-widget-area',
        'description' => __('The primary widget area', 'Blankpress'),
        'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</li>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));

    // Area 2, located below the Primary Widget Area in the sidebar. Empty by default.
    register_sidebar(array(
        'name' => __('Secondary Widget Area', 'Blankpress'),
        'id' => 'secondary-widget-area',
        'description' => __('The secondary widget area', 'Blankpress'),
        'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</li>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));
}

add_action('widgets_init', 'blankpress_widgets_init');

/**
 * @param $post_ID
 */
function restrict_post_deletion($post_ID)
{
    $restricted_pages = array(13, 15);
    if (in_array($post_ID, $restricted_pages)) {
        echo "You are not authorized to delete this page.";
        exit;
    }
}


add_action('trash_post', 'restrict_post_deletion', 10, 1);
add_action('delete_post', 'restrict_post_deletion', 10, 1);
add_action('wp_trash_post', 'restrict_post_deletion');

function about_meta_box_markup($object)
{
    wp_nonce_field(basename(__FILE__), "meta-box-nonce");
    ?>
    <table>
        <tr>
            <td><label for="about_project_completed">Project Completed</label></td>
            <td><input id="about_project_completed" name="about_project_completed" type="text" value="<?php echo get_post_meta($object->ID, "about_project_completed", true); ?>"></td>
        </tr>
        <tr>
            <td><label for="about_team_member">Team Member</label></td>
            <td><input id="about_team_member" name="about_team_member" type="text" value="<?php echo get_post_meta($object->ID, "about_team_member", true); ?>"></td>
        </tr>
        <tr>
            <td><label for="about_on_going_project">On Going Project</label></td>
            <td><input id="about_on_going_project" name="about_on_going_project" type="text" value="<?php echo get_post_meta($object->ID, "about_on_going_project", true); ?>"></td>
        </tr>
        <tr>
            <td><label for="about_weekly_event">Weekly Event</label></td>
            <td><input id="about_weekly_event" name="about_weekly_event" type="text" value="<?php echo get_post_meta($object->ID, "about_weekly_event", true); ?>"></td>
        </tr>
    </table>
    <?php
}

function add_about_meta_box()
{
    if (!empty($GLOBALS['pagenow']) && 'post.php' === $GLOBALS['pagenow']) {
        $arr = array(13);
        if (isset($_GET['post']) && $_GET['post'] == 13 && get_post_type() == 'page' && in_array(get_the_ID(), $arr)) {
            add_meta_box("about-meta-box", "Some Facts About Us", "about_meta_box_markup", "page", "normal", "low", null);
        }
    }
}

add_action("add_meta_boxes", "add_about_meta_box");

function save_about_meta_box()
{
    global $post;

    $about_name_box_text_value = "";
    $about_age_box_text_value = "";
    $about_location_box_text_value = "";

    if (isset($_POST["about_project_completed"])) {
        $about_project_completed = $_POST['about_project_completed'];
    }
    update_post_meta($post->ID, "about_project_completed", $about_project_completed);


    if (isset($_POST["about_team_member"])) {
        $about_team_member = $_POST["about_team_member"];
    }
    update_post_meta($post->ID, "about_team_member", $about_team_member);

    if (isset($_POST["about_on_going_project"])) {
        $about_on_going_project = $_POST["about_on_going_project"];
    }
    update_post_meta($post->ID, "about_on_going_project", $about_on_going_project);

    if (isset($_POST["about_weekly_event"])) {
        $about_weekly_event = $_POST["about_weekly_event"];
    }
    update_post_meta($post->ID, "about_weekly_event", $about_weekly_event);

}

add_action("save_post", "save_about_meta_box");

function service_meta_box_markup($object)
{
    wp_nonce_field(basename(__FILE__), "meta-box-nonce");
    ?>
    <table width="100%">
        <tr>
            <td width="10%"><label for="service_slogan_1">Slogan 1 </label></td>
            <td width="90%"><input id="service_slogan_1" name="service_slogan_1" type="text" style="width: 400px;" value="<?php echo get_post_meta($object->ID, "service_slogan_1", true); ?>"></td>
        </tr>
        <tr>
            <td><label for="service_slogan_2">Slogan 2 </label></td>
            <td><input id="service_slogan_2" name="service_slogan_2" type="text" style="width: 400px;" value="<?php echo get_post_meta($object->ID, "service_slogan_2", true); ?>"></td>
        </tr>
    </table>
    <?php
}

function add_service_meta_box()
{
    if (!empty($GLOBALS['pagenow']) && 'post.php' === $GLOBALS['pagenow']) {
        $arr = array(15);
        if (isset($_GET['post']) && $_GET['post'] == 15 && get_post_type() == 'page' && in_array(get_the_ID(), $arr)) {
            add_meta_box("service-meta-box", "Some Facts About Us", "service_meta_box_markup", "page", "normal", "low", null);
        }
    }
}

add_action("add_meta_boxes", "add_service_meta_box");

function save_service_meta_box()
{
    global $post;

    $service_name_box_text_value = "";
    $service_age_box_text_value = "";
    $service_location_box_text_value = "";

    if (isset($_POST["service_slogan_1"])) {
        $service_project_completed = $_POST['service_slogan_1'];
    }
    update_post_meta($post->ID, "service_slogan_1", $service_project_completed);


    if (isset($_POST["service_slogan_2"])) {
        $service_team_member = $_POST["service_slogan_2"];
    }
    update_post_meta($post->ID, "service_slogan_2", $service_team_member);

}

add_action("save_post", "save_service_meta_box");

add_action('wp_enqueue_scripts', 'my_register_javascript', 100);

function my_register_javascript()
{
    wp_register_script('mediaelement', plugins_url('wp-mediaelement.min.js', __FILE__), array('jquery'), '4.8.2', true);
    wp_enqueue_script('mediaelement');
}


function themename_custom_logo_setup()
{
    $defaults = array(
        'height' => 100,
        'width' => 400,
        'flex-height' => true,
        'flex-width' => true,
        'header-text' => array('site-title', 'site-description'),
    );
    add_theme_support('custom-logo', $defaults);
}

add_action('after_setup_theme', 'themename_custom_logo_setup');

function displayBanner($item)
{
    $html = '';
    if (!empty($item)) {

        if ($item['radio'] == 'Yes') {
            $html .= '<li data-transition="fadefromright" data-slotamount="5" data-masterspeed="700" data-delay="9400" class="slider-item-2">
                                                <img src="' . $item['full_img'] . '" alt="slidebg2" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">';

            for ($i = 1; $i <= 2; $i++) {
                if ($i == 1) {
                    $html .= '<div class="caption lft start" data-y="center" data-voffset="40" data-x="center" data-hoffset="-250" data-speed="600" data-start="500" data-easing="easeOutBack"><img src="' . $item['images'][$i] . '" alt=""> </div>
                ';
                } else {
                    $html .= '<div class="caption lft start" data-y="center" data-voffset="130" data-x="center" data-hoffset="170" data-speed="600"  data-start="1200" data-easing="easeOutBack"><img src="' . $item['images'][$i] . '" alt=""> </div>';
                }
            }
            $html .= '<div class="tp-caption large_bold_white fade" data-x="center" data-y="40" data-speed="300" data-start="1700" data-easing="Power4.easeOut" data-endspeed="500" data-endeasing="Power1.easeIn" data-captionhidden="off"style="z-index: 6">' . $item['title'] . ' </div></li>';
        } else {
            $html .= '
             <li data-transition="fade" data-slotamount="5" data-masterspeed="700" data-delay="9400" class="slider-item-1">
                                                <img src="' . $item['full_img'] . '" alt="" data-bgfit="cover" style="opacity:0.4 !important;" data-bgposition="center center" data-bgrepeat="no-repeat">
                                                <div class="tp-caption large_text customin customout start"
                                                     data-x="center"
                                                     data-hoffset="0"
                                                     data-y="center"
                                                     data-voffset="60"
                                                     data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                                                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                                     data-speed="1000"
                                                     data-start="500"
                                                     data-easing="Back.easeInOut"
                                                     data-endspeed="300">
                                                    <div class="promo-like"><i class="fa fa-thumbs-up"></i></div>
                                                    <div class="promo-like-text">
                                                        <p>' . $item['content'] . '</p>
                                                    </div>
                                                </div>
                                                <div class="tp-caption large_bold_white fade"
                                                     data-x="center"
                                                     data-y="center"
                                                     data-voffset="-110"
                                                     data-speed="300"
                                                     data-start="1700"
                                                     data-easing="Power4.easeOut"
                                                     data-endspeed="500"
                                                     data-endeasing="Power1.easeIn"
                                                     data-captionhidden="off"
                                                     style="z-index: 6"> <span>' . $item['title'] . '</span>
                                                </div>
                                            </li>
            ';
        }

    }

    return $html;
}

function display_image_url($productId)
{
    $url = '';
    if (!empty($productId)) {
        global $wpdb;
        $imgUrl = $wpdb->get_results("SELECT image1 FROM ec_product WHERE product_id =" . $productId);

        if (!empty($imgUrl[0]->image1)) {
            $imgPath = explode('wp-content', $imgUrl[0]->image1);
            $fileUrl = WP_CONTENT_DIR . $imgPath[1];
            if (file_exists($fileUrl) && !is_dir($fileUrl)) {
                $url = $imgUrl[0]->image1;
            }
        }
    }

    return $url;
}

remove_action('wp_head', 'wp_generator');
add_filter('login_errors',create_function('$a', "return null;"));

?>