<?php
$args = array('p' => 15, 'post_type' => 'page');
$the_query = new WP_Query($args);

if ($the_query->have_posts()) {
    while ($the_query->have_posts()) {
        $the_query->the_post();
        $featureWorkImg = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full', false, '');

        global $wpdb;
        $results = $wpdb->get_results("SELECT * FROM ec_product WHERE activate_in_store = 1", OBJECT);
        ?>
        <div class="services-block content content-center" id="<?php echo strtolower(str_replace(' ', '_', get_the_title())); ?>">
            <div class="container">
                <h2>Our <strong>services</strong></h2>
                <p><?php echo get_the_content(); ?></p>
                <?php
                if (!empty($results)) {
                    $total = $wpdb->num_rows;;
                    $i = 0;
                    foreach ($results as $result) {
                        $i++;
                        if ($i == 1) {
                            echo '<div class="row">';
                        }
                        ?>
                        <div class="col-md-3 col-sm-3 col-xs-12 item">
                            <i class="fa"><img class="service-img" src="<?php echo $result->image1; ?>" alt="<?php echo $result->title; ?>"/> </i>
                            <h3><a href="<?php echo site_url() ?>/store/?model_number=<?php echo $result->model_number; ?>"><?php echo $result->title; ?></a></h3>
                            <p><?php echo shorten_string($result->description, 20); ?>...</p>
                        </div>
                        <?php
                        if ($i == 4 || $i == $total) {
                            echo '</div>';
                            $i = 0;
                        }
                    }
                }
                ?>

            </div>
        </div>
        <!-- Message block BEGIN -->
        <div class="message-block content content-center valign-center" id="message-block" style="background: #292F34 url(<?php echo !empty($featureWorkImg[0]) ? $featureWorkImg[0] : get_bloginfo('template_url') . '/assets/img/quote.jpg'; ?>) no-repeat fixed">
            <div class="valign-center-elem">
                <h2><?php echo strtoupper(get_post_meta(get_the_ID(), "service_slogan_1", true)); ?> <strong><?php echo strtoupper(get_post_meta(get_the_ID(), "service_slogan_2", true)); ?></strong></h2>
            </div>
        </div>
        <!-- Message block END -->

        <?php
    }
    /* Restore original Post Data */
    wp_reset_postdata();
} else {
    // no posts found
}
?>