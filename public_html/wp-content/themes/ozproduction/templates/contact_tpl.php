<?php
$args = array('p' => 17, 'post_type' => 'page');
$the_query = new WP_Query($args);

if ($the_query->have_posts()) {
    while ($the_query->have_posts()) {
        $the_query->the_post();
        $featureWorkImg = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full', false, '');
        ?>
        <div class="pre-footer" id="<?php echo strtolower(str_replace(' ', '_', get_the_title())); ?>">
            <div class="black-bg">
                <div id="maps"></div>

                <div class="container">
                    <div class="row">
                        <div class="offset-top">
                            <div class="col-xs-12 col-md-12 wow fadeInUp" data-wow-delay="0.2s">
                                <div class="well well-lg">
                                    <h3>Get in Touch</h3>
                                    <div class="space-20"></div>
                                    <?php echo do_shortcode('[contact-form-7 id="98" title="Contact form"]'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="container">
                <div class="row">
                    <!-- BEGIN BOTTOM ABOUT BLOCK -->
                    <div class="col-md-4 col-sm-6 pre-footer-col">
                        <h2>About us</h2>
                        <?php
                        $contactArgs = array('p' => 44, 'post_type' => 'post');
                        $the_query_contact = new WP_Query($contactArgs);

                        if ($the_query_contact->have_posts()) {
                            while ($the_query_contact->have_posts()) {
                                $the_query_contact->the_post();
                                ?>
                                <address class="margin-bottom-20">
                                    <?php echo get_the_content(); ?>
                                </address>
                                <?php
                            }
                            /* Restore original Post Data */
                            wp_reset_postdata();
                        } else {
                            // no posts found
                        }
                        ?>
                    </div>
                    <!-- END BOTTOM ABOUT BLOCK -->
                    <!-- BEGIN TWITTER BLOCK -->
                    <div class="col-md-4 col-sm-6 pre-footer-col">

                    </div>
                    <!-- END TWITTER BLOCK -->
                    <div class="col-md-4 col-sm-6 pre-footer-col">
                        <!-- BEGIN BOTTOM CONTACTS -->
                        <h2>Our Contacts</h2>
                        <?php
                        $contactArgs = array('p' => 46, 'post_type' => 'post');
                        $the_query_contact = new WP_Query($contactArgs);

                        if ($the_query_contact->have_posts()) {
                            while ($the_query_contact->have_posts()) {
                                $the_query_contact->the_post();
                                ?>
                                <address class="margin-bottom-20">
                                    <?php echo get_the_content(); ?>
                                </address>
                                <?php
                            }
                            /* Restore original Post Data */
                            wp_reset_postdata();
                        } else {
                            // no posts found
                        }
                        ?>

                        <!-- END BOTTOM CONTACTS -->
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
    /* Restore original Post Data */
    wp_reset_postdata();
} else {
    // no posts found
}
?>