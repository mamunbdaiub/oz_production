<?php
$args = array('p' => 40, 'post_type' => 'page');
$the_query = new WP_Query($args);

if ($the_query->have_posts()) {
    while ($the_query->have_posts()) {
        $the_query->the_post();
        $featureWorkImg = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full', false, '');

        $about_project_completed = get_post_meta(get_the_ID(), "about_project_completed", true);
        $about_team_member = get_post_meta(get_the_ID(), "about_team_member", true);
        $about_on_going_project = get_post_meta(get_the_ID(), "about_on_going_project", true);
        $about_weekly_event = get_post_meta(get_the_ID(), "about_weekly_event", true);
        ?>
        <div class="testimonials-block content content-center margin-bottom-65">
            <div class="container">
                <h2>Customer <strong>testimonials</strong></h2>
                <h4><?php the_content(); ?></h4>
                <div class="carousel slide" data-ride="carousel" id="testimonials-block">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <?php
                        $testimonialArgs = array(
                            'post_type' => 'testimonial',
                            'post_status' => 'publish',
                            'orderby' => 'meta_value',
                            'order' => 'ASC',
                            'posts_per_page' => -1
                        );
                        $i = 0;
                        $testimonials = new WP_Query($testimonialArgs);
                        if (!empty($testimonials->posts)) {
                            while ($testimonials->have_posts()) : $testimonials->the_post();
                                $i++;
                                $post_author_id = get_post_field('post_author', get_the_ID());
                                $author_img_url = get_avatar_url($post_author_id);
                                $authorName = get_post_meta(get_the_ID(), 'author', true);
                                $designation = get_post_meta(get_the_ID(), 'designation', true);
                                $companyName = get_post_meta(get_the_ID(), 'company_name', true);
                                ?>
                                <!-- Carousel items -->
                                <div class="<?php echo $i == 1 ? 'active' : ''; ?> item">
                                    <blockquote>
                                        <p><?php echo get_the_content(); ?></p>
                                    </blockquote>
                                    <span class="testimonials-name"><?php echo !empty($authorName) ? $authorName : ''; ?></span>
                                </div>
                                <!-- Carousel items -->
                                <?php

                            endwhile;
                        }
                        wp_reset_postdata();
                        ?>
                        <!-- Carousel items -->
                    </div>
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <?php
                        $testimonialArgs = array(
                            'post_type' => 'testimonial',
                            'post_status' => 'publish',
                            'order' => 'ASC',
                            'posts_per_page' => -1
                        );
                        $j = 0;
                        $testimonials = new WP_Query($testimonialArgs);
                        if (!empty($testimonials->posts)) {
                            while ($testimonials->have_posts()) : $testimonials->the_post();

                                ?>
                                <li data-target="#testimonials-block" data-slide-to="<?php echo $j; ?>" class="<?php echo $j == 0 ? 'active' : ''; ?>"></li>
                                <?php
                                $j++;
                            endwhile;
                        }
                        wp_reset_postdata();
                        ?>
                    </ol>
                </div>
            </div>
        </div>
        <?php
    }
    /* Restore original Post Data */
    wp_reset_postdata();
} else {
    // no posts found
}
?>