<?php
$args = array('p' => 13, 'post_type' => 'page');
$the_query = new WP_Query($args);

if ($the_query->have_posts()) {
    while ($the_query->have_posts()) {
        $the_query->the_post();
        $featureWorkImg = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full', false, '');

        $about_project_completed = get_post_meta(get_the_ID(), "about_project_completed", true);
        $about_team_member = get_post_meta(get_the_ID(), "about_team_member", true);
        $about_on_going_project = get_post_meta(get_the_ID(), "about_on_going_project", true);
        $about_weekly_event = get_post_meta(get_the_ID(), "about_weekly_event", true);
        ?>
        <div class="about-block content content-center" id="<?php echo strtolower(str_replace(' ', '_', get_the_title())); ?>">
            <div class="container">
                <h2><strong>About</strong> Us</h2>
                <p><?php echo get_the_content(); ?></p>
            </div>
            <!-- Facts block BEGIN -->
            <div class="facts-block content content-center" id="facts-block">
                <h2>Some facts about us</h2>
                <div class="container">
                    <div class="row">
                        <?php
                        if (!empty($about_project_completed)) {
                            ?>
                            <div class="col-md-3 col-sm-3 col-xs-6">
                                <div class="item">
                                    <strong><?php echo $about_project_completed; ?></strong>
                                    Projects Completed
                                </div>
                            </div>
                            <?php
                        }
                        ?>

                        <?php
                        if (!empty($about_team_member)) {
                            ?>
                            <div class="col-md-3 col-sm-3 col-xs-6">
                                <div class="item ab-cirle-blue">
                                    <strong><?php echo $about_team_member; ?></strong>
                                    Team Members
                                </div>
                            </div>
                            <?php
                        }
                        ?>

                        <?php
                        if (!empty($about_on_going_project)) {
                            ?>
                            <div class="col-md-3 col-sm-3 col-xs-6">
                                <div class="item ab-cirle-green">
                                    <strong><?php echo $about_on_going_project; ?></strong>
                                    Products Sold
                                </div>
                            </div>
                            <?php
                        }
                        ?>

                        <?php
                        if (!empty($about_weekly_event)) {
                            ?>
                            <div class="col-md-3 col-sm-3 col-xs-6">
                                <div class="item ab-cirle-red">
                                    <strong><?php echo $about_weekly_event; ?></strong>
                                    Weekly Event
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
            <!-- Facts block END -->
        </div>
        <?php
    }
    /* Restore original Post Data */
    wp_reset_postdata();
} else {
    // no posts found
}
?>