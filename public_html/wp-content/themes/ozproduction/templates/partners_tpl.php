<div class="partners-block">
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-sm-3 col-xs-6">
                <img src="<?php bloginfo('template_url') ?>/assets/img/partners/cisco.png" alt="cisco">
            </div>
            <div class="col-md-2 col-sm-3 col-xs-6">
                <img src="<?php bloginfo('template_url') ?>/assets/img/partners/walmart.png" alt="walmart">
            </div>
            <div class="col-md-2 col-sm-3 col-xs-6">
                <img src="<?php bloginfo('template_url') ?>/assets/img/partners/gamescast.png" alt="gamescast">
            </div>
            <div class="col-md-2 col-sm-3 col-xs-6">
                <img src="<?php bloginfo('template_url') ?>/assets/img/partners/spinwokrx.png" alt="spinwokrx">
            </div>
            <div class="col-md-2 col-sm-3 col-xs-6">
                <img src="<?php bloginfo('template_url') ?>/assets/img/partners/ngreen.png" alt="ngreen">
            </div>
            <div class="col-md-2 col-sm-3 col-xs-6">
                <img src="<?php bloginfo('template_url') ?>/assets/img/partners/vimeo.png" alt="vimeo">
            </div>
        </div>
    </div>
</div>