<?php
$args = array('p' => 11, 'post_type' => 'page');
$the_query = new WP_Query($args);

if ($the_query->have_posts()) {
    while ($the_query->have_posts()) {
        $the_query->the_post();
        $featureWorkImg = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full', false, '');
        ?>
        <div class="promo-block" id="<?php echo strtolower(str_replace(' ', '_', get_the_title())); ?>">
            <div class="tp-banner-container">
                <div class="tp-banner">
                    <ul>
                        <?php
                        $bannerArgs = array(
                            'post_type' => 'banner',
                            'post_status' => 'publish',
                            'order' => 'ASC',
                            'posts_per_page' => -1
                        );
                        $j = $i = 0;
                        $banners = new WP_Query($bannerArgs);
                        if (!empty($banners->posts)) {
                            $item = [];
                            while ($banners->have_posts()) : $banners->the_post();
                                $i++;
                                $featureWorkImg = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full', false, '');
                                $fields = get_field_objects(get_the_ID());
                                $item[$j]['full_img'] = !empty($featureWorkImg[0]) ? $featureWorkImg[0] : get_bloginfo('template_url') . '/images/event/event_' . $i . '.png';
                                $item[$j]['title'] = get_the_title();
                                $item[$j]['content'] = get_the_content();

                                if (!empty($fields)) {
                                    $m = 0;
                                    foreach ($fields as $field_name => $field) {
                                        if (in_array($field['type'], array('image', 'radio'))) {
                                            if ($field['type'] == 'radio') {
                                                $item[$j]['radio'] = $field['value'];
                                            } elseif ($field['type'] == 'image') {
                                                $item[$j]['images'][$m] = $field['value'];
                                            }
                                        }
                                        $m++;
                                    }
                                }
                                echo displayBanner($item[$j]);
                                ?>
                                <?php
                                $j++;
                            endwhile;
                        }
                        wp_reset_postdata();
                        ?>
                    </ul>
                </div>
            </div>
        </div>
        <?php
    }
    /* Restore original Post Data */
    wp_reset_postdata();
} else {
    // no posts found
}
?>