<div class="header header-mobi-ext">
    <div class="container">
        <div class="row">
            <!-- Logo BEGIN -->
            <div class="col-md-2 col-sm-2">
                <a class="scroll site-logo" href="#home">
                    <?php
                    $custom_logo_id = get_theme_mod('custom_logo');
                    $logo = wp_get_attachment_image_src($custom_logo_id, 'full');
                    if (has_custom_logo()) {
                        echo '<img src="' . esc_url($logo[0]) . '">';
                    } else {
                        echo '<span class="text-white logo-text">' . get_bloginfo('name') . '</span>';
                    }
                    ?>
                </a>
            </div>
            <!-- Logo END -->
            <a href="javascript:void(0);" class="mobi-toggler"><i class="fa fa-bars"></i></a>
            <!-- Navigation BEGIN -->
            <div class="col-md-10 pull-right">
                <ul class="header-navigation">
                    <?php
                    $menu_name = 'Main_Menu';
                    $navMenus = wp_get_nav_menu_items($menu_name);
                    foreach ($navMenus as $menu) {
                        if ($menu->object_id == 6 || $menu->object_id == 7 || $menu->object_id == 5) {
                            echo '<li><a href="' . $menu->url . '">' . $menu->title . '</a></li>';
                        } else {
                            echo '<li><a href="#' . strtolower(str_replace(' ', '_', $menu->title)) . '">' . $menu->title . '</a></li>';
                        }
                    }
                    ?>
                </ul>
            </div>
            <!-- Navigation END -->
        </div>
    </div>
</div>