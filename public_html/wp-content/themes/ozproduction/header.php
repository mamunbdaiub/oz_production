<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->

    <!-- Head BEGIN -->
    <head profile="http://gmpg.org/xfn/11">
        <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>"/>

        <title><?php wp_title('&laquo;', true, 'right'); ?><?php bloginfo('name'); ?></title>

        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <meta content="Metronic Shop UI description" name="description">
        <meta content="Metronic Shop UI keywords" name="keywords">
        <meta content="keenthemes" name="author">

        <meta property="og:site_name" content="Oz Production">
        <meta property="og:title" content="Oz Production">
        <meta property="og:description" content="Oz Production Event Services, LLC">
        <meta property="og:type" content="website">
        <meta property="og:image" content=""><!-- link to image for socio -->
        <meta property="og:url" content="http://www.ozproductionservices.com/">

        <link rel="shortcut icon" href="favicon.ico">
        <!-- Fonts START -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|Pathway+Gothic+One|PT+Sans+Narrow:400+700|Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all" rel="stylesheet" type="text/css">
        <!-- Fonts END -->
        <!-- Global styles BEGIN -->
        <link href="<?php bloginfo('template_url') ?>/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php bloginfo('template_url') ?>/vendor/bootstrap/css/bootstrap-datetimepicker.css" rel="stylesheet">
        <link href="<?php bloginfo('template_url') ?>/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php bloginfo('template_url') ?>/vendor/slider-revolution-slider/rs-plugin/css/settings.css" rel="stylesheet">
        <!-- Global styles END -->
        <!-- Page level plugin styles BEGIN -->
        <link href="<?php bloginfo('template_url') ?>/vendor/fancybox/source/jquery.fancybox.css" rel="stylesheet">
        <!-- Page level plugin styles END -->
        <!-- Theme styles BEGIN -->
        <link href="<?php bloginfo('template_url') ?>/css/components.css" rel="stylesheet">
        <link href="<?php bloginfo('template_url') ?>/assets/css/style.css" rel="stylesheet">
        <link href="<?php bloginfo('template_url') ?>/assets/css/style-responsive.css" rel="stylesheet">
        <link href="<?php bloginfo('template_url') ?>/assets/css/themes/red.css" rel="stylesheet" id="style-color">
        <link href="<?php bloginfo('template_url') ?>/assets/css/custom.css" rel="stylesheet">
        <link href="<?php bloginfo('template_url') ?>/css/overright.css" rel="stylesheet">
        <link href="<?php bloginfo('template_url') ?>/css/responsive.css" rel="stylesheet">
        <!-- Theme styles END -->

        <script type="text/javascript">
            var BASEURL = "<?php echo esc_url(get_template_directory_uri()) ?>";
            var IMAGEPATH = "<?php echo esc_url(get_template_directory_uri()); ?>/images/";
        </script>

        <?php if (is_singular()) wp_enqueue_script('comment-reply'); ?>

        <?php wp_head(); ?>
    </head>
    <!--DOC: menu-always-on-top class to the body element to set menu on top -->
    <body <?php body_class(); ?>>





