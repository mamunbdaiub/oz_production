<?php get_header(); ?>
    <!-- Header BEGIN -->
<?php get_template_part('templates/inner_header_tpl', 'none'); ?>
    <!-- Header END -->
    <div class="main">
        <div class="container inner-container">
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <!-- BEGIN SIDEBAR & CONTENT -->
                <div class="row margin-bottom-40">
                    <!-- BEGIN CONTENT -->
                    <div class="col-md-12 col-sm-12">
                        <h1><?php the_title(); ?></h1>
                        <div class="content-page">
                            <?php the_content(); ?>
                        </div>
                    </div>
                    <!-- END CONTENT -->
                </div>
                <!-- END SIDEBAR & CONTENT -->
            <?php endwhile; endif; ?>
        </div>
    </div>

<?php get_footer(); ?>