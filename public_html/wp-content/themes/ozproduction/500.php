<?php get_header(); ?>
    <!-- Header BEGIN -->
<?php get_template_part('templates/inner_header_tpl', 'none'); ?>
    <!-- Header END -->
    <div class="main">
        <div class="container inner-container">
            <!-- BEGIN SIDEBAR & CONTENT -->
            <div class="row margin-bottom-40">
                <!-- BEGIN CONTENT -->
                <div class="col-md-12 col-sm-12">
                    <div class="content-page">
                        <div class="col-md-12 col-sm-12">
                            <div class="content-page page-404">
                                <div class="number">
                                    500
                                </div>
                                <div class="details">
                                    <h3>Oops! You're lost.</h3>
                                    <p>
                                        We can not find the page you're looking for.<br>
                                        <a href="<?php echo site_url(); ?>" class="link">Return home</a> or try the search bar below.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END CONTENT -->
            </div>
            <!-- END SIDEBAR & CONTENT -->
        </div>
    </div>

<?php get_footer(); ?>