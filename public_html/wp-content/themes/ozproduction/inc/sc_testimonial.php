<?php
function sc_testimonial()
{
    $labels = array(
        'name' => _x('Testimonials', 'post type general name'),
        'singular_name' => _x('Testimonial', 'post type singular name'),
        'add_new' => _x('Add New', 'equipment'),
        'add_new_item' => __('Add New Testimonial'),
        'edit_item' => __('Edit New Testimonial'),
        'new_item' => __('New Testimonial'),
        'all_items' => __('All Testimonials'),
        'view_item' => __('View Testimonial'),
        'search_items' => __('Search Testimonials'),
        'not_found' => __('No testimonials found'),
        'not_found_in_trash' => __('No testimonials found in the Trash'),
        'parent_item_colon' => '',
        'menu_name' => 'Testimonials'
    );
    $args = array(
        'labels' => $labels,
        'description' => 'Holds our testimonials and testimonial specific data',
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'menu_position' => 5,
        'supports' => array('title', 'editor', 'thumbnail', 'comments'),
        'has_archive' => true,
        'rewrite' => array('slug' => 'testimonials', 'with_front' => true),
        'capability_type' => 'post',
        'hierarchical' => false,
        'taxonomies' => array('post_tag')
    );
    register_post_type('testimonial', $args);
    flush_rewrite_rules(TRUE);
}

add_action('init', 'sc_testimonial');

//Custom category
function sc_taxonomies_testimonial()
{
    $labels = array(
        'name' => _x('Testimonial Categories', 'taxonomy general name'),
        'singular_name' => _x('Testimonial Category', 'taxonomy singular name'),
        'search_items' => __('Search Testimonials Categories'),
        'all_items' => __('All Testimonials Categories'),
        'parent_item' => __('Parent Testimonials Category'),
        'parent_item_colon' => __('Parent Testimonial Category:'),
        'edit_item' => __('Edit Testimonial Category'),
        'update_item' => __('Update Testimonial Category'),
        'add_new_item' => __('Add New Testimonial Category'),
        'new_item_name' => __('New Testimonial Category'),
        'menu_name' => __('Testimonial Categories'),
    );
    $args = array(
        'labels' => $labels,
        'hierarchical' => true,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'sc-testimonial', 'hierarchical' => false),
    );
    register_taxonomy('sc-testimonial', 'testimonial', $args);
}

add_action('init', 'sc_taxonomies_testimonial', 0);

// add meta fields 
$prefix = 'country_';

$meta_box_testimonial = array(
    'id' => 'country-testimonial-meta-box-id',
    'title' => 'Testimonial Summary Information',
    'page' => 'testimonial',
    'context' => 'normal',
    'priority' => 'low',
    'fields' => array(
        array(
            'name' => 'Short Description',
            'desc' => '',
            'id' => $prefix . 'testimonial_short_description',
            'type' => 'textarea'
        )
    )
);

add_action('add_meta_boxes', 'country_testimonial_meta_box_add');

function country_testimonial_meta_box_add()
{
    global $meta_box_testimonial;
    add_meta_box($meta_box_testimonial['id'], $meta_box_testimonial['title'], 'country_testimonial_meta_box_view', $meta_box_testimonial['page'], $meta_box_testimonial['context'], $meta_box_testimonial['priority']);
}

function country_testimonial_meta_box_view()
{
    global $meta_box_testimonial, $post_testimonial;
    // Use nonce for verification
    echo '<input type="hidden" name="country_testimonial_meta_box_nonce" value="' . wp_create_nonce(basename(__FILE__)) . '" />';

    echo '<table class="form-table">';
    // pr($meta_box_testimonial);
    foreach ($meta_box_testimonial['fields'] as $field) {
        //pr($field);
        // get current post meta data
        $meta = get_post_meta($_GET['post'], $field['id'], true);;
        //var_dump($meta);
        echo '<tr>';
        echo '<th style="width:20%"><label for="' . $field['id'] . '">' . $field['name'] . ':</label></th>';
        echo '<td>';
        switch ($field['type']) {
            case 'text':
                echo '<input type="text" name="' . $field['id'] . '" id="' . $field['id'] . '" value="' . $meta . '" size="30" /> 
                            <br /><span class="description">' . $field['desc'] . '</span>';
                break;
            case 'date':
                echo '<input type="text" name="' . $field['id'] . '" id="' . $field['id'] . '" value="' . $meta . '" size="30" /> 
                            <br /><span class="description">' . $field['desc'] . '</span>';
                echo '<script type="text/javascript">
                                jQuery(function() {
                                    jQuery( "#country_testimonial_date" ).datepicker({dateFormat: "yy-mm-dd"});
                                  });
                                </script>  ';
                break;
            // textarea  
            case 'textarea':
                $args = array("textarea_rows" => 10, "textarea_name" => "country_testimonial_short_description", "editor_class" => "my_editor_custom");
                wp_editor($meta, "country_testimonial_short_description", $args);
                break;
            case 'checkbox':
                echo '<input type="checkbox" name="' . $field['id'] . '" id="' . $field['id'] . '" ', $meta ? ' checked="checked"' : '', '/> 
                            <label for="' . $field['id'] . '">' . $field['desc'] . '</label>';
                break;
        } //end switch          
        echo '</td>';
        echo '</tr>';
    }
    echo '</table>';
}

add_action('save_post', 'country_testimonial_meta_box_save');

function country_testimonial_meta_box_save($post_id)
{
    global $meta_box_testimonial;
    global $post_id;

    // verify nonce
    if (!wp_verify_nonce($_POST['country_testimonial_meta_box_nonce'], basename(__FILE__))) {
        return $post_id;
    }

    // check autosave
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return $post_id;
    }

    // check permissions
    if ('page' == $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id)) {
            return $post_id;
        }
    } elseif (!current_user_can('edit_post', $post_id)) {
        return $post_id;
    }

    foreach ($meta_box_testimonial['fields'] as $field) {
        $old = get_post_meta($post_id, $field['id'], true);
        $new = $_POST[$field['id']];

        if ($new && $new != $old) {
            update_post_meta($post_id, $field['id'], $new);
        } elseif ('' == $new && $old) {
            delete_post_meta($post_id, $field['id'], $old);
        }
    }
}

add_action('admin_init', 'testimonial_editor_admin_init');
add_action('admin_head', 'testimonial_editor_admin_head');

function testimonial_editor_admin_init()
{
    wp_enqueue_script('word-count');
    wp_enqueue_script('post');
    wp_enqueue_script('editor');
    wp_enqueue_script('media-upload');
}

function testimonial_editor_admin_head()
{
    wp_tiny_mce();
}

?>