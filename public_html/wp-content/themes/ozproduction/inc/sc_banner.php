<?php
function sc_banner()
{
    $labels = array(
        'name' => _x('Home Banners', 'post type general name'),
        'singular_name' => _x('Home Banner', 'post type singular name'),
        'add_new' => _x('Add New Banner', 'equipment'),
        'add_new_item' => __('Add New Home Banner'),
        'edit_item' => __('Edit New Home Banner'),
        'new_item' => __('New Banner'),
        'all_items' => __('All Home Banners'),
        'view_item' => __('View Home Banner'),
        'search_items' => __('Search Home Banners'),
        'not_found' => __('No home banners found'),
        'not_found_in_trash' => __('No home banners found in the Trash'),
        'parent_item_colon' => '',
        'menu_name' => 'Home Banners'
    );
    $args = array(
        'labels' => $labels,
        'description' => 'Holds our banners and banner specific data',
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'menu_position' => 5,
        'supports' => array('title', 'editor', 'thumbnail', 'comments'),
        'has_archive' => true,
        'rewrite' => array('slug' => 'banners', 'with_front' => true),
        'capability_type' => 'post',
        'hierarchical' => false,
        'taxonomies' => array('post_tag')
    );
    register_post_type('banner', $args);
    flush_rewrite_rules(TRUE);
}

add_action('init', 'sc_banner');

//Custom category
function sc_taxonomies_banner()
{
    $labels = array(
        'name' => _x('Banner Categories', 'taxonomy general name'),
        'singular_name' => _x('Banner Category', 'taxonomy singular name'),
        'search_items' => __('Search Banners Categories'),
        'all_items' => __('All Banners Categories'),
        'parent_item' => __('Parent Banners Category'),
        'parent_item_colon' => __('Parent Banner Category:'),
        'edit_item' => __('Edit Banner Category'),
        'update_item' => __('Update Banner Category'),
        'add_new_item' => __('Add New Banner Category'),
        'new_item_name' => __('New Banner Category'),
        'menu_name' => __('Banner Categories'),
    );
    $args = array(
        'labels' => $labels,
        'hierarchical' => true,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'sc-banner', 'hierarchical' => false),
    );
    register_taxonomy('sc-banner', 'banner', $args);
}

add_action('init', 'sc_taxonomies_banner', 0);

// add meta fields 
$prefix = 'country_';

$meta_box_banner = array(
    'id' => 'country-banner-meta-box-id',
    'title' => 'Banner Summary Information',
    'page' => 'banner',
    'context' => 'normal',
    'priority' => 'low',
    'fields' => array(
        array(
            'name' => 'Short Description',
            'desc' => '',
            'id' => $prefix . 'banner_short_description',
            'type' => 'textarea'
        )
    )
);

add_action('add_meta_boxes', 'country_banner_meta_box_add');

function country_banner_meta_box_add()
{
    global $meta_box_banner;
//    add_meta_box($meta_box_banner['id'], $meta_box_banner['title'], 'country_banner_meta_box_view', $meta_box_banner['page'], $meta_box_banner['context'], $meta_box_banner['priority']);
}

function country_banner_meta_box_view()
{
    global $meta_box_banner, $post_banner;
    // Use nonce for verification
    echo '<input type="hidden" name="country_banner_meta_box_nonce" value="' . wp_create_nonce(basename(__FILE__)) . '" />';

    echo '<table class="form-table">';
    // pr($meta_box_banner);
    foreach ($meta_box_banner['fields'] as $field) {
        //pr($field);
        // get current post meta data
        $meta = get_post_meta($_GET['post'], $field['id'], true);;
        //var_dump($meta);
        echo '<tr>';
        echo '<th style="width:20%"><label for="' . $field['id'] . '">' . $field['name'] . ':</label></th>';
        echo '<td>';
        switch ($field['type']) {
            case 'text':
                echo '<input type="text" name="' . $field['id'] . '" id="' . $field['id'] . '" value="' . $meta . '" size="30" /> 
                            <br /><span class="description">' . $field['desc'] . '</span>';
                break;
            case 'date':
                echo '<input type="text" name="' . $field['id'] . '" id="' . $field['id'] . '" value="' . $meta . '" size="30" /> 
                            <br /><span class="description">' . $field['desc'] . '</span>';
                echo '<script type="text/javascript">
                                jQuery(function() {
                                    jQuery( "#country_banner_date" ).datepicker({dateFormat: "yy-mm-dd"});
                                  });
                                </script>  ';
                break;
            // textarea  
            case 'textarea':
                $args = array("textarea_rows" => 10, "textarea_name" => "country_banner_short_description", "editor_class" => "my_editor_custom");
                wp_editor($meta, "country_banner_short_description", $args);
                break;
            case 'checkbox':
                echo '<input type="checkbox" name="' . $field['id'] . '" id="' . $field['id'] . '" ', $meta ? ' checked="checked"' : '', '/> 
                            <label for="' . $field['id'] . '">' . $field['desc'] . '</label>';
                break;
        } //end switch          
        echo '</td>';
        echo '</tr>';
    }
    echo '</table>';
}

add_action('save_post', 'country_banner_meta_box_save');

function country_banner_meta_box_save($post_id)
{
    global $meta_box_banner;
    global $post_id;

    // verify nonce
    if (!wp_verify_nonce($_POST['country_banner_meta_box_nonce'], basename(__FILE__))) {
        return $post_id;
    }

    // check autosave
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return $post_id;
    }

    // check permissions
    if ('page' == $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id)) {
            return $post_id;
        }
    } elseif (!current_user_can('edit_post', $post_id)) {
        return $post_id;
    }

    foreach ($meta_box_banner['fields'] as $field) {
        $old = get_post_meta($post_id, $field['id'], true);
        $new = $_POST[$field['id']];

        if ($new && $new != $old) {
            update_post_meta($post_id, $field['id'], $new);
        } elseif ('' == $new && $old) {
            delete_post_meta($post_id, $field['id'], $old);
        }
    }
}

add_action('admin_init', 'banner_editor_admin_init');
add_action('admin_head', 'banner_editor_admin_head');

function banner_editor_admin_init()
{
    wp_enqueue_script('word-count');
    wp_enqueue_script('post');
    wp_enqueue_script('editor');
    wp_enqueue_script('media-upload');
}

function banner_editor_admin_head()
{
    wp_tiny_mce();
}

?>