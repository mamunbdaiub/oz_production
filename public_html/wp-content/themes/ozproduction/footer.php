<!-- BEGIN FOOTER -->
<!--Footer-area-->
<div class="footer">
    <div class="container">
        <div class="row">
            <!-- BEGIN COPYRIGHT -->
            <div class="col-md-6 col-sm-6">
                <div class="copyright"><?php echo date('Y'); ?> © <?php bloginfo( 'description' ); ?>. ALL Rights Reserved.</div>
            </div>
            <!-- END COPYRIGHT -->
            <!-- BEGIN SOCIAL ICONS -->
            <div class="col-md-6 col-sm-6 pull-right">
                <ul class="social-icons">
<!--                    <li><a class="rss" data-original-title="rss" href="javascript:void(0);"></a></li>-->
<!--                    <li><a class="facebook" data-original-title="facebook" href="javascript:void(0);"></a></li>-->
<!--                    <li><a class="twitter" data-original-title="twitter" href="javascript:void(0);"></a></li>-->
<!--                    <li><a class="googleplus" data-original-title="googleplus" href="javascript:void(0);"></a></li>-->
<!--                    <li><a class="linkedin" data-original-title="linkedin" href="javascript:void(0);"></a></li>-->
<!--                    <li><a class="youtube" data-original-title="youtube" href="javascript:void(0);"></a></li>-->
<!--                    <li><a class="vimeo" data-original-title="vimeo" href="javascript:void(0);"></a></li>-->
<!--                    <li><a class="skype" data-original-title="skype" href="javascript:void(0);"></a></li>-->
                </ul>
            </div>
            <!-- END SOCIAL ICONS -->
        </div>
    </div>
</div>
<!-- END FOOTER -->
<a href="#home" class="go2top scroll"><i class="fa fa-arrow-up"></i></a>
<!--[if lt IE 9]>
<script src="<?php bloginfo('template_url') ?>/vendor/respond.min.js"></script>
<![endif]-->
<!-- Load JavaScripts at the bottom, because it will reduce page load time -->
<!-- Core plugins BEGIN (For ALL pages) -->
<script src="<?php bloginfo('template_url') ?>/vendor/jquery.min.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_url') ?>/vendor/jquery-migrate.min.js" type="text/javascript"></script>
<script src="http://code.jquery.com/ui/1.8.21/jquery-ui.min.js"></script>
<script src="<?php bloginfo('template_url') ?>/vendor/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_url') ?>/vendor/bootstrap/js/moment-with-locales.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_url') ?>/vendor/bootstrap/js/bootstrap-datetimepicker.js" type="text/javascript"></script>
<!-- Core plugins END (For ALL pages) -->
<!-- BEGIN RevolutionSlider -->
<script src="<?php bloginfo('template_url') ?>/vendor/slider-revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_url') ?>/vendor/slider-revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_url') ?>/assets/scripts/revo-ini.js" type="text/javascript"></script>
<!-- END RevolutionSlider -->
<!-- Core plugins BEGIN (required only for current page) -->
<script src="<?php bloginfo('template_url') ?>/vendor/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script><!-- pop up -->
<script src="<?php bloginfo('template_url') ?>/vendor/jquery.easing.js"></script>
<script src="<?php bloginfo('template_url') ?>/vendor/jquery.parallax.js"></script>
<script src="<?php bloginfo('template_url') ?>/vendor/jquery.scrollTo.min.js"></script>
<script src="<?php bloginfo('template_url') ?>/assets/scripts/jquery.nav.js"></script>
<!-- Core plugins END (required only for current page) -->

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBTS_KEDfHXYBslFTI_qPJIybDP3eceE-A&sensor=false"></script>
<script src="<?php bloginfo('template_url') ?>/js/maps.js"></script>
<!-- Global js BEGIN -->
<script src='https://www.google.com/recaptcha/api.js'></script>
<script src="<?php bloginfo('template_url') ?>/assets/scripts/layout.js" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        Layout.init();
        $('#datetimepicker1').datetimepicker({
            format: 'YYYY/MM/DD hh:mm A',
            minDate: moment()
        });
        $('#datetimepicker2').datetimepicker({
            format: 'YYYY/MM/DD hh:mm A',
            minDate: moment()
        });

    });
</script>
<!-- Global js END -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119386524-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-119386524-1');
</script>
<?php wp_footer(); ?>
</body>
</html>