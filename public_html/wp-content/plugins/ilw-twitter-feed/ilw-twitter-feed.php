<?php

/**
 * Plugin Name: ILW Twitter Feed
 * Plugin URI: http://www.instalogic.com
 * Description: This plugin fetch the feed from twitter.
 * Version: 1.0
 * Author: Instalogic
 * Author URI: http://www.instalogic.com
 * License: GPL2
 */
/*  Copyright 2014  ILW Twiiter Feed  (email : info@instalogic.com)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License, version 2, as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

class ILWTwitterFeed {

    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct() {
        add_action('admin_menu', array($this, 'add_plugin_page'));
        add_action('admin_init', array($this, 'page_init'));
    }
    
    private function str_replace_word($needle, $replacement, $haystack) {
        $pattern = "/\b$needle\b/i";
        $haystack = preg_replace($pattern, $replacement, $haystack);
        return $haystack;
    }

    /**
     * Add options page
     */
    public function add_plugin_page() {
        // This page will be under "Settings"
        add_options_page(
                'Instalogic Twitter Feed Admin', 'Instalogic Twitter Feed', 'manage_options', 'ilw-tw-feed-admin', array($this, 'create_admin_page')
        );
    }

    /**
     * Options page callback
     */
    public function create_admin_page() {
        // Set class property
        $this->options = get_option('ilw_tw_feed_option_name');
        ?>
        <div class="wrap">
        <?php screen_icon(); ?>
            <h2>Instalogic Twitter Feed</h2>           
            <form method="post" action="options.php">
        <?php
        // This prints out all hidden setting fields
        settings_fields('ilw_tw_feed_option_group');
        do_settings_sections('ilw-tw-feed-admin');
        submit_button();
        ?>
            </form>
            <br/>
            <h2>Documentation</h2>
            <br/>
            1. Use the below shortcode to show the twitter feed in page where you want to show.<br/><br/>
            [ilw_tw_widget]
            <br/><br/><br/>
            2. Or put below function in the php file where you want to show.<br/><br/>
            get_ilw_feed_widget(); 

            <br/><br/><br/>
            Click <a target="_blank" href="<?php echo plugin_dir_url(__FILE__); ?>docs/documentation-twitter.pdf">here</a> to download full documentation.
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init() {
        register_setting(
                'ilw_tw_feed_option_group', // Option group
                'ilw_tw_feed_option_name', // Option name
                array($this, 'sanitize') // Sanitize
        );

        add_settings_section(
                'ilw_tw_feed_section_id', // ID
                'Twitter Feed Settings', // Title
                array($this, 'print_section_info'), // Callback
                'ilw-tw-feed-admin' // Page
        );

        add_settings_field(
                'ilw_tw_consumer_key', // Consumer Key
                'Consumer Key', // Title 
                array($this, 'ilw_tw_consumer_key_callback'), // Callback
                'ilw-tw-feed-admin', // Page
                'ilw_tw_feed_section_id' // Section           
        );

        add_settings_field(
                'ilw_tw_consumer_screet', //Consumer Secreet
                'Consumer Screet', array($this, 'ilw_tw_consumer_screet_callback'), 'ilw-tw-feed-admin', // Page
                'ilw_tw_feed_section_id' // Section  
        );

        add_settings_field(
                'ilw_tw_access_token', //Access Token 
                'Access Token', array($this, 'ilw_tw_access_token_callback'), 'ilw-tw-feed-admin', // Page
                'ilw_tw_feed_section_id' // Section  
        );

        add_settings_field(
                'ilw_tw_access_token_screet', //Access Token Screet
                'Access Token Screet', array($this, 'ilw_tw_access_token_screet_callback'), 'ilw-tw-feed-admin', // Page
                'ilw_tw_feed_section_id' // Section  
        );

        add_settings_field(
                'ilw_tw_post_limit', 'Post Limit', array($this, 'ilw_tw_post_limit'), 'ilw-tw-feed-admin', // Page
                'ilw_tw_feed_section_id' // Section  
        );

        add_settings_field(
                'ilw_tw_widget_width', 'Width of Widget', array($this, 'ilw_tw_widget_width'), 'ilw-tw-feed-admin', // Page
                'ilw_tw_feed_section_id' // Section  
        );

        add_settings_field(
                'ilw_tw_widget_height', 'Width of Height', array($this, 'ilw_tw_widget_height'), 'ilw-tw-feed-admin', // Page
                'ilw_tw_feed_section_id' // Section  
        );
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize($input) {
        $new_input = array();
        if (isset($input['ilw_tw_consumer_key']))
            $new_input['ilw_tw_consumer_key'] = sanitize_text_field($input['ilw_tw_consumer_key']);

        if (isset($input['ilw_tw_consumer_screet']))
            $new_input['ilw_tw_consumer_screet'] = sanitize_text_field($input['ilw_tw_consumer_screet']);

        if (isset($input['ilw_tw_access_token']))
            $new_input['ilw_tw_access_token'] = sanitize_text_field($input['ilw_tw_access_token']);

        if (isset($input['ilw_tw_access_token_screet']))
            $new_input['ilw_tw_access_token_screet'] = sanitize_text_field($input['ilw_tw_access_token_screet']);

        if (isset($input['ilw_tw_post_limit']))
            $new_input['ilw_tw_post_limit'] = sanitize_text_field($input['ilw_tw_post_limit']);

        if (isset($input['ilw_tw_widget_width']))
            $new_input['ilw_tw_widget_width'] = sanitize_text_field($input['ilw_tw_widget_width']);

        if (isset($input['ilw_tw_widget_height']))
            $new_input['ilw_tw_widget_height'] = sanitize_text_field($input['ilw_tw_widget_height']);

        return $new_input;
    }

    /**
     * Print the Section text
     */
    public function print_section_info() {
        print 'Enter your settings below:';
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function ilw_tw_consumer_key_callback() {
        printf(
                '<input type="text" id="ilw_tw_consumer_key" size="50" name="ilw_tw_feed_option_name[ilw_tw_consumer_key]" value="%s" />', isset($this->options['ilw_tw_consumer_key']) ? esc_attr($this->options['ilw_tw_consumer_key']) : ''
        );
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function ilw_tw_consumer_screet_callback() {
        printf(
                '<input type="text" id="ilw_tw_consumer_screet" size="50" name="ilw_tw_feed_option_name[ilw_tw_consumer_screet]" value="%s" />', isset($this->options['ilw_tw_consumer_screet']) ? esc_attr($this->options['ilw_tw_consumer_screet']) : ''
        );
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function ilw_tw_access_token_callback() {
        printf(
                '<input type="text" id="ilw_tw_access_token" size="50" name="ilw_tw_feed_option_name[ilw_tw_access_token]" value="%s" />', isset($this->options['ilw_tw_access_token']) ? esc_attr($this->options['ilw_tw_access_token']) : ''
        );
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function ilw_tw_access_token_screet_callback() {
        printf(
                '<input type="text" id="ilw_tw_access_token_screet" size="50" name="ilw_tw_feed_option_name[ilw_tw_access_token_screet]" value="%s" />', isset($this->options['ilw_tw_access_token_screet']) ? esc_attr($this->options['ilw_tw_access_token_screet']) : ''
        );
    }

    public function ilw_tw_post_limit() {
        printf(
                '<input type="text" id="ilw_tw_post_limit" size="50" name="ilw_tw_feed_option_name[ilw_tw_post_limit]" value="%s" /><br/> <i> i.e., 10 | default: 5</i>', isset($this->options['ilw_tw_post_limit']) ? esc_attr($this->options['ilw_tw_post_limit']) : '5'
        );
    }

    public function ilw_tw_widget_width() {
        printf(
                '<input type="text" id="ilw_tw_widget_width" size="50" name="ilw_tw_feed_option_name[ilw_tw_widget_width]" value="%s" />px<br/> <i> i.e., 400 | default: 300</i>', isset($this->options['ilw_tw_widget_width']) ? esc_attr($this->options['ilw_tw_widget_width']) : '300'
        );
    }

    public function ilw_tw_widget_height() {
        printf(
                '<input type="text" id="ilw_tw_widget_height" size="50" name="ilw_tw_feed_option_name[ilw_tw_widget_height]" value="%s" />px<br/> <i> i.e., 450 | default: auto</i>', isset($this->options['ilw_tw_widget_height']) ? esc_attr($this->options['ilw_tw_widget_height']) : 'auto'
        );
    }

    public function show_feed() {
        // echo plugins_url();
        wp_enqueue_style('ilw_tw_css', plugin_dir_url('') . '/ilw-twitter-feed/css/style.css');
        /**
         * This is the link to my page graph
         * I've included it here so i can copy an paste for quick reference
         *
         * Copying and pasting this into the browser url bar gives you a full graph of the feed
         * which is very handy for browsing and seeing what exists in the array
         *
         * Change the values to suit your own needs, and when your script is final, remove this
         * comment block
         *
         * Typing this into the url will get you the super array (graph) to analyze
         * https://graph.facebook.com/YOUR_PAGE_ID/feed?access_token=APP_ACCESS_TOKEN
         */
        $this->options = get_option('ilw_tw_feed_option_name');

        $consumer_key = isset($this->options['ilw_tw_consumer_key']) ? esc_attr($this->options['ilw_tw_consumer_key']) : '';
        $consumer_screet = isset($this->options['ilw_tw_consumer_screet']) ? esc_attr($this->options['ilw_tw_consumer_screet']) : '';
        $access_token = isset($this->options['ilw_tw_access_token']) ? esc_attr($this->options['ilw_tw_access_token']) : '';
        $access_token_screet = isset($this->options['ilw_tw_access_token_screet']) ? esc_attr($this->options['ilw_tw_access_token_screet']) : '';

//    echo "<br>Consumer Key: ".$consumer_key;
//    echo "<br>Consumer Screet: ".$consumer_screet;
//    echo "<br>Access Token: ".$access_token;
//    echo "<br>Access Token Screet: ".$access_token_screet;

        $post_limit = !empty($this->options['ilw_tw_post_limit']) ? esc_attr($this->options['ilw_tw_post_limit']) : 5;
        $widget_width = !empty($this->options['ilw_tw_widget_width']) ? esc_attr($this->options['ilw_tw_widget_width']) : 300;
        $widget_height = !empty($this->options['ilw_tw_widget_width']) ? esc_attr($this->options['ilw_tw_widget_height']) : 'auto';
        $widget_height = ( $widget_height == 'auto' ) ? $widget_height : $widget_height . "px";

        $oauth_hash = '';
        $oauth_hash .= 'oauth_consumer_key=' . $consumer_key . '&';
        $oauth_hash .= 'oauth_nonce=' . time() . '&';
        $oauth_hash .= 'oauth_signature_method=HMAC-SHA1&';
        $oauth_hash .= 'oauth_timestamp=' . time() . '&';
        $oauth_hash .= 'oauth_token=' . $access_token . '&';
        $oauth_hash .= 'oauth_version=1.0';
        $base = '';
        $base .= 'GET';
        $base .= '&';
        $base .= rawurlencode('https://api.twitter.com/1.1/statuses/user_timeline.json');
        $base .= '&';
        $base .= rawurlencode($oauth_hash);
        $key = '';
        $key .= rawurlencode($consumer_screet);
        $key .= '&';
        $key .= rawurlencode($access_token_screet);
        $signature = base64_encode(hash_hmac('sha1', $base, $key, true));
        $signature = rawurlencode($signature);

        $oauth_header = '';
        $oauth_header .= 'oauth_consumer_key="' . $consumer_key . '", ';
        $oauth_header .= 'oauth_nonce="' . time() . '", ';
        $oauth_header .= 'oauth_signature="' . $signature . '", ';
        $oauth_header .= 'oauth_signature_method="HMAC-SHA1", ';
        $oauth_header .= 'oauth_timestamp="' . time() . '", ';
        $oauth_header .= 'oauth_token="' . $access_token . '", ';
        $oauth_header .= 'oauth_version="1.0", ';
        $curl_header = array("Authorization: Oauth {$oauth_header}", 'Expect:');

        $curl_request = curl_init();
        curl_setopt($curl_request, CURLOPT_HTTPHEADER, $curl_header);
        curl_setopt($curl_request, CURLOPT_HEADER, false);
        curl_setopt($curl_request, CURLOPT_URL, 'https://api.twitter.com/1.1/statuses/user_timeline.json');
        curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_request, CURLOPT_SSL_VERIFYPEER, false);
        $json = curl_exec($curl_request);
        curl_close($curl_request);

        $datas = json_decode($json);

        $i = 0;
        if (!empty($datas)) {
            echo '<div style="width:' . $widget_width . 'px;"  id="ilw-twitter-feed">';
            echo '<div style="height:'.$widget_height.';" class="content">';
            foreach ($datas as $data) {
                $i++;

                $created_time = new DateTime($data->created_at);
                $created_time = date_format($created_time, 'M j');

                $urls = array();
                if (!empty($data->entities->urls)) {
                    foreach ($data->entities->urls as $url) {
                        $ldata = $url->url;
                        $urls[] = array(
                            'ldata' => $ldata,
                            'ldataa' => '<a target="_blank" href="' . $ldata . '">' . $url->display_url . '</a>'
                        );
                    }
                }


                $hashtags = array();
                if (!empty($data->entities->hashtags)) {
                    foreach ($data->entities->hashtags as $hashtag) {
                        $ldata = $hashtag->text;
                        $hashtags[] = array(
                            'ldata' => '#' . $ldata,
                            'ldataa' => '<a target="_blank" href="https://twitter.com/hashtag/' . $ldata . '?src=hash">#' . $ldata . '</a>'
                        );
                    }
                }

                $user_mentions = array();
                if (!empty($data->entities->user_mentions)) {
                    foreach ($data->entities->user_mentions as $user_mention) {
                        $ldata = $user_mention->screen_name;
                        $user_mentions[] = array(
                            'ldata' => '@' . $ldata,
                            'ldataa' => '<a target="_blank" href="https://twitter.com/' . $ldata . '">@' . $ldata . '</a>'
                        );
                    }
                }
                ?>
                <div id="feed-<?php echo $i; ?>" class="feed">
                    <?php $photoUrl = $data->user->profile_image_url;
                    $input = 'https://example.com/example/https.php';
                    $photoNewUrl =  preg_replace('/^http(?=:\/\/)/i','https',$photoUrl);
                    ?>
                    <a target="_blank" href="http://www.twitter.com/<?php echo $data->user->screen_name; ?>">
                        <img class="profile-img" src="<?php echo $photoNewUrl; ?>" />

                        <div class="profile-name"><?php echo $data->user->name; ?></div>
                    </a>
                <?php
                ?>
                    <div class="date"><a target="_blank" href="http://www.twitter.com/<?php echo $data->user->name; ?>">@<?php echo $data->user->screen_name; ?> </a> &nbsp;&nbsp;<?php echo $created_time; ?></div>
                    <div class="clear"></div>
                <?php
                if (!empty($urls)) {
                    foreach ($urls as $url) {
                        $data->text = str_replace($url['ldata'], $url['ldataa'], $data->text);
                    }
                }

                if (!empty($hashtags)) {
                    foreach ($hashtags as $hashtag) {
                        //$data->text = str_replace($hashtag['ldata'], $hashtag['ldataa'], $data->text);
                        $data->text = preg_replace('/'.$hashtag["ldata"].'\b/u', $hashtag['ldataa'], $data->text);
                    }
                }

                if (!empty($user_mentions)) {
                    foreach ($user_mentions as $user_mention) {
                        $data->text = str_replace($user_mention['ldata'], $user_mention['ldataa'], $data->text);
                    }
                }
                ?>
                    <div class="text"><?php echo $data->text; ?></div>
                    <?php
                            echo '<div class="summary">';
                                if( empty( $data->retweet_count ) && empty( $data->favorite_count ) )  echo '&nbsp;';
                                echo !empty( $data->retweet_count ) ? 'Retweet: '.$data->retweet_count : '';
                                echo !empty( $data->favorite_count ) ? 'Favorite: '.$data->favorite_count : '';
                                echo '<a target="_blank" class="right" href="https://www.twitter.com/'.$data->user->screen_name.'/status/'.$data->id_str.'">View Summary</a>';
                                echo '<span class="clear"></span>';
                            echo '</div>';
                        
                    ?>
                </div>
                <?php
                if ($i == $post_limit) {
                    break;
                }
            }
            echo '</div>';
            echo '</div>';
        }
    }
}

if (is_admin())
    $objILWFacebookFeed = new ILWTwitterFeed();

add_shortcode('ilw_feed_widget', 'get_ilw_feed_widget');

function get_ilw_feed_widget() {
    $objILWTwitterFeed = new ILWTwitterFeed();
    $objILWTwitterFeed->show_feed();

}