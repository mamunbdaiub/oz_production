<div class="ec_admin_slider_row">
    <h1>Free Edition Gateways</h1>
    <div style="text-align:center;">PayPal Standard and Bill Later are aboslutely free to use, but Stripe and Square each include a 2% application fee and the standard processing fees on all transactions. You may use Square or Stripe as your live payment method.</div>
    <div style="text-align:center; margin:15px 0 0; font-size:18px;"><strong>Enable Bill Later, PayPal, Stripe/Square, or All 3 Methods!</strong></div>
</div>