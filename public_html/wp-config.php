<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'oz_production');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', '');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'hg;H-HJDDH}vC|3gb)Eaq<gDfd+`aFH@Zu)X7LIaiIXOnbasPP3CL1Zz16GN25mQ');
define('SECURE_AUTH_KEY',  'N=(@k}+Y{k|J82[CjRXC# WW-<%y`i>DA+/fTyK+wb~@A8Fa<E[Yxk;>k+-]+#D*');
define('LOGGED_IN_KEY',    '?/O9UfW{//}hh@QdywV-XLm<6jG]X64T.).NCu %jS-,bOn0>Y<AM` xpW%G,vun');
define('NONCE_KEY',        'A5o,kNsyve{7Q,vsPE&qXwjLKJ;1tz++b?X,j]v?iu*(_EeL}GB6=iIiO1U-J}@&');
define('AUTH_SALT',        'PPf]yGyMgsm{^ijYh?*?J&h1&#*uiJSz|f,aZSvV)nMllMA5}B4A~!n/I$izk|+x');
define('SECURE_AUTH_SALT', 'J+.O?L=&^Q}wO~Yp~:N&?qSmQrwj?7jP5/tOkhklg8fad$|sD5)e?;3_Q@wwwJR!');
define('LOGGED_IN_SALT',   '097]cijzCSG/Y74HotrZBDFslx%mpWP{i*JU&c7T4VJJXKk`z]YQ],}JZ&GJk4fK');
define('NONCE_SALT',       'aw~Md$6+A@YwM=`w48zsY `x0ld+D5N%QJ?c$`y/Bi61Ffzc=F~!fLr$D=c^Kw1a');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'ops_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
